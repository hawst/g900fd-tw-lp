.class public Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field c:Z

.field d:I

.field e:I

.field f:Z

.field public final synthetic g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

.field private h:I

.field private i:I

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/ar;",
            ">;"
        }
    .end annotation
.end field

.field private k:[Z

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:[Lcom/google/android/apps/gmm/base/views/an;

.field private o:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 877
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 878
    invoke-direct {p0, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 843
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->c:Z

    .line 849
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->d:I

    .line 850
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->e:I

    .line 865
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->l:Ljava/util/List;

    .line 879
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    .line 880
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->o:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->requestLayout()V

    .line 881
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;)Lcom/google/b/a/an;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 834
    const/4 v3, -0x1

    const v2, 0x7fffffff

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getScrollY()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingTop()I

    move-result v1

    sub-int v7, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v8

    move v4, v5

    move v1, v5

    :goto_0
    if-ge v4, v8, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    if-nez v6, :cond_0

    const/4 v6, 0x1

    :goto_1
    if-nez v6, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    iget v6, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    if-ge v6, v2, :cond_2

    iget v1, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    iget v2, v0, Lcom/google/android/apps/gmm/base/views/ar;->b:I

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    sub-int v0, v7, v0

    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v6, v5

    goto :goto_1

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2
.end method

.method private a(Z)V
    .locals 13

    .prologue
    .line 1231
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->m:Z

    if-eqz v0, :cond_0

    .line 1420
    :goto_0
    return-void

    .line 1235
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->m:Z

    .line 1237
    sget v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->d:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->d:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "layout-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1242
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->d:I

    if-ltz v0, :cond_2

    .line 1243
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1244
    if-ltz v0, :cond_1

    .line 1245
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ar;

    .line 1248
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->m:Z

    .line 1249
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->e:I

    add-int/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->scrollTo(II)V

    .line 1250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->m:Z

    .line 1252
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->d:I

    .line 1253
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->e:I

    .line 1256
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getScrollY()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingTop()I

    move-result v1

    sub-int v4, v0, v1

    .line 1257
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getHeight()I

    move-result v0

    add-int v5, v4, v0

    .line 1261
    const/4 v1, 0x0

    .line 1264
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 1266
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1267
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([ZZ)V

    .line 1268
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_8

    .line 1269
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1270
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    .line 1271
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_7

    .line 1272
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    .line 1276
    iget v0, v3, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    if-gt v0, v5, :cond_3

    iget v0, v3, Lcom/google/android/apps/gmm/base/views/ar;->g:I

    if-ge v0, v4, :cond_6

    .line 1277
    :cond_3
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    move v1, v0

    :goto_3
    if-ge v1, v9, :cond_5

    .line 1278
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/aq;

    const/4 v10, 0x0

    invoke-interface {v0, v8, v10}, Lcom/google/android/apps/gmm/base/views/aq;->a(Landroid/view/View;Z)V

    .line 1277
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1271
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 1280
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    iget v1, v3, Lcom/google/android/apps/gmm/base/views/ar;->c:I

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/base/views/aw;->a(ILandroid/view/View;)V

    .line 1281
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {p0, v0, v1, v3, v8}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->invalidate(IIII)V

    .line 1282
    const/4 v0, 0x1

    .line 1268
    :goto_4
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 1284
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    iget v3, v3, Lcom/google/android/apps/gmm/base/views/ar;->b:I

    const/4 v8, 0x1

    aput-boolean v8, v0, v3

    :cond_7
    move v0, v1

    goto :goto_4

    .line 1288
    :cond_8
    if-eqz v1, :cond_9

    .line 1289
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long/2addr v0, v6

    long-to-float v0, v0

    const v1, 0x49742400    # 1000000.0f

    div-float/2addr v0, v1

    .line 1290
    sget-object v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onLayout() remove time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1296
    :cond_9
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1298
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 1300
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    move v2, v0

    :goto_5
    if-ge v2, v8, :cond_12

    .line 1301
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ar;

    .line 1303
    iget v1, v0, Lcom/google/android/apps/gmm/base/views/ar;->b:I

    .line 1304
    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    array-length v9, v9

    if-lt v1, v9, :cond_a

    .line 1305
    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    add-int/lit8 v10, v1, 0x1

    invoke-static {v9, v10}, Ljava/util/Arrays;->copyOf([ZI)[Z

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    .line 1308
    :cond_a
    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    aget-boolean v9, v9, v1

    if-nez v9, :cond_d

    .line 1309
    iget v9, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    if-gt v9, v5, :cond_d

    iget v9, v0, Lcom/google/android/apps/gmm/base/views/ar;->g:I

    if-lt v9, v4, :cond_d

    .line 1313
    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v9, v9, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    iget v10, v0, Lcom/google/android/apps/gmm/base/views/ar;->c:I

    invoke-virtual {v9, v10}, Lcom/google/android/apps/gmm/base/views/aw;->a(I)Landroid/view/View;

    move-result-object v9

    .line 1317
    iget-object v10, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v10, v10, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    iget-object v11, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v11, v11, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-interface {v10, v1, v9, v11}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 1318
    if-eqz v9, :cond_c

    if-eq v9, v10, :cond_c

    .line 1319
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v11, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v12, v1, Landroid/view/ViewGroup;

    if-eqz v12, :cond_b

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_b
    iget-object v1, v11, Lcom/google/android/apps/gmm/base/views/aw;->b:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1321
    :cond_c
    if-eqz v10, :cond_d

    .line 1322
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 1328
    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1330
    new-instance v1, Lcom/google/android/apps/gmm/base/views/ap;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/ap;-><init>(Lcom/google/android/apps/gmm/base/views/ar;)V

    .line 1331
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/views/ap;->b:Z

    .line 1332
    invoke-virtual {v10}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1333
    if-ne v0, p0, :cond_e

    .line 1335
    invoke-virtual {v10, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1353
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1300
    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1337
    :cond_e
    instance-of v9, v0, Landroid/view/ViewGroup;

    if-eqz v9, :cond_f

    .line 1340
    if-eqz p1, :cond_10

    .line 1341
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 1346
    :cond_f
    :goto_7
    if-eqz p1, :cond_11

    .line 1347
    const/4 v0, -0x1

    invoke-virtual {p0, v10, v0, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    goto :goto_6

    .line 1343
    :cond_10
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_7

    .line 1349
    :cond_11
    const/4 v0, -0x1

    invoke-virtual {p0, v10, v0, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    .line 1357
    :cond_12
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long/2addr v0, v6

    long-to-float v0, v0

    const v1, 0x49742400    # 1000000.0f

    div-float/2addr v0, v1

    .line 1358
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    .line 1359
    sget-object v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onLayout() add time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1368
    :cond_13
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v4

    move v3, v0

    :goto_8
    if-ge v3, v4, :cond_18

    .line 1369
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1370
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    .line 1371
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    if-nez v1, :cond_16

    const/4 v1, 0x1

    :goto_9
    if-nez v1, :cond_15

    .line 1372
    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    .line 1376
    iget v1, v6, Lcom/google/android/apps/gmm/base/views/ar;->e:I

    if-eqz v1, :cond_15

    .line 1377
    if-nez p1, :cond_14

    invoke-virtual {v5}, Landroid/view/View;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1382
    :cond_14
    iget v1, v6, Lcom/google/android/apps/gmm/base/views/ar;->a:I

    const/4 v2, -0x3

    if-ne v1, v2, :cond_17

    .line 1383
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, v1, Lcom/google/android/apps/gmm/base/views/an;->b:I

    .line 1384
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    aget-object v1, v1, v7

    iget v1, v1, Lcom/google/android/apps/gmm/base/views/an;->c:I

    .line 1391
    :goto_a
    const/high16 v7, 0x40000000    # 2.0f

    sub-int v8, v1, v2

    .line 1392
    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    iget v8, v6, Lcom/google/android/apps/gmm/base/views/ar;->e:I

    const/high16 v9, 0x40000000    # 2.0f

    .line 1393
    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 1391
    invoke-virtual {v5, v7, v8}, Landroid/view/View;->measure(II)V

    .line 1394
    iget v7, v6, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    iget v6, v6, Lcom/google/android/apps/gmm/base/views/ar;->g:I

    invoke-virtual {v5, v2, v7, v1, v6}, Landroid/view/View;->layout(IIII)V

    .line 1396
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/ap;->b:Z

    if-eqz v1, :cond_15

    .line 1397
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {p0, v1, v2, v6, v5}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->invalidate(IIII)V

    .line 1398
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/ap;->b:Z

    .line 1368
    :cond_15
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    .line 1371
    :cond_16
    const/4 v1, 0x0

    goto :goto_9

    .line 1386
    :cond_17
    iget-object v1, v6, Lcom/google/android/apps/gmm/base/views/ar;->h:Lcom/google/android/apps/gmm/base/views/an;

    .line 1387
    iget v2, v1, Lcom/google/android/apps/gmm/base/views/an;->b:I

    .line 1388
    iget v1, v1, Lcom/google/android/apps/gmm/base/views/an;->c:I

    goto :goto_a

    .line 1404
    :cond_18
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    :goto_b
    if-ge v3, v4, :cond_1a

    .line 1405
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/aq;

    .line 1406
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    move v2, v1

    :goto_c
    if-ge v2, v5, :cond_19

    .line 1407
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->l:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v6, 0x1

    invoke-interface {v0, v1, v6}, Lcom/google/android/apps/gmm/base/views/aq;->a(Landroid/view/View;Z)V

    .line 1406
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_c

    .line 1404
    :cond_19
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    .line 1413
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->m:Z

    goto/16 :goto_0
.end method

.method private a(ZLjava/util/IdentityHashMap;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/IdentityHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 996
    sget v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->c:I

    add-int/lit8 v3, v2, 0x1

    sput v3, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "measure-all-contents-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1002
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    .line 1005
    if-eqz p1, :cond_0

    .line 1007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1010
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 1011
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v2, v2, Lcom/google/android/apps/gmm/base/views/an;->c:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/google/android/apps/gmm/base/views/an;->b:I

    sub-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    .line 1012
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1013
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/google/android/apps/gmm/base/views/an;->c:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/google/android/apps/gmm/base/views/an;->b:I

    sub-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 1027
    const/4 v4, 0x0

    .line 1028
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v16

    .line 1029
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 1030
    :goto_0
    move/from16 v0, v16

    if-ge v4, v0, :cond_1

    if-ge v4, v3, :cond_1

    .line 1031
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/views/ar;

    .line 1035
    iget-wide v6, v2, Lcom/google/android/apps/gmm/base/views/ar;->d:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    iget-wide v6, v2, Lcom/google/android/apps/gmm/base/views/ar;->d:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1036
    iget-object v5, v5, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v5, v4}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    iget v2, v2, Lcom/google/android/apps/gmm/base/views/ar;->c:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1037
    iget-object v5, v5, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v5, v4}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v5

    if-ne v2, v5, :cond_1

    .line 1039
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1046
    :cond_1
    add-int/lit8 v2, v3, -0x1

    :goto_1
    if-lt v2, v4, :cond_2

    .line 1047
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1046
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 1051
    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "# of ListItems recycled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1052
    sget-object v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    sub-int v2, v16, v4

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x28

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "# of ListItems NOT recycled: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1055
    :goto_2
    move/from16 v0, v16

    if-ge v4, v0, :cond_7

    .line 1057
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v2, v4}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v17

    .line 1058
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/views/aw;->a(I)Landroid/view/View;

    move-result-object v3

    .line 1059
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-interface {v2, v4, v3, v5}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v18

    .line 1060
    if-eqz v3, :cond_4

    move-object/from16 v0, v18

    if-eq v3, v0, :cond_4

    .line 1061
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v5, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v6, v2, Landroid/view/ViewGroup;

    if-eqz v6, :cond_3

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/views/aw;->b:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1068
    :cond_4
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/IdentityHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070
    const/4 v5, -0x1

    .line 1071
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    instance-of v2, v2, Lcom/google/android/apps/gmm/base/b/j;

    if-eqz v2, :cond_5

    .line 1072
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1073
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    check-cast v2, Lcom/google/android/apps/gmm/base/b/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v3, v3

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/gmm/base/b/j;->a(II)I

    move-result v5

    .line 1077
    :cond_5
    const/4 v2, -0x3

    if-ne v5, v2, :cond_6

    move v2, v10

    .line 1080
    :goto_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v13}, Landroid/view/View;->measure(II)V

    .line 1082
    new-instance v3, Lcom/google/android/apps/gmm/base/views/ar;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1084
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v2, v4}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v6

    .line 1085
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1086
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v2, v4}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v8

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/gmm/base/views/ar;-><init>(IIIIJ)V

    .line 1087
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1090
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/base/views/aw;->a(ILandroid/view/View;)V

    .line 1055
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_6
    move v2, v11

    .line 1077
    goto :goto_3

    .line 1095
    :cond_7
    invoke-virtual/range {p2 .. p2}, Ljava/util/IdentityHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1096
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->removeView(Landroid/view/View;)V

    .line 1097
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    goto :goto_4

    .line 1101
    :cond_8
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v14

    long-to-float v2, v2

    const v3, 0x49742400    # 1000000.0f

    div-float/2addr v2, v3

    .line 1102
    sget-object v3, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "measureAllViews() trace: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1107
    return-void
.end method

.method private c()I
    .locals 14

    .prologue
    .line 1115
    sget v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->b:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->b:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "layout-contents-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1121
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 1125
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1126
    const/4 v5, 0x0

    iput v5, v3, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1129
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    const/4 v1, 0x0

    aget-object v2, v0, v1

    .line 1130
    const/4 v1, 0x0

    .line 1131
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_8

    .line 1132
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ar;

    .line 1134
    iget v8, v0, Lcom/google/android/apps/gmm/base/views/ar;->e:I

    if-eqz v8, :cond_b

    .line 1135
    iget v8, v0, Lcom/google/android/apps/gmm/base/views/ar;->a:I

    .line 1139
    const/4 v9, -0x3

    if-ne v8, v9, :cond_3

    .line 1140
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->e()Lcom/google/android/apps/gmm/base/views/an;

    move-result-object v2

    .line 1159
    :cond_1
    :goto_2
    const/4 v9, -0x2

    if-eq v8, v9, :cond_2

    iget v9, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    if-lez v9, :cond_2

    .line 1161
    iget v9, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    iget v10, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a:I

    add-int/2addr v9, v10

    iput v9, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1163
    :cond_2
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/ar;->h:Lcom/google/android/apps/gmm/base/views/an;

    .line 1164
    iget v9, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    iput v9, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    .line 1165
    iget v9, v0, Lcom/google/android/apps/gmm/base/views/ar;->f:I

    iget v10, v0, Lcom/google/android/apps/gmm/base/views/ar;->e:I

    add-int/2addr v9, v10

    iput v9, v0, Lcom/google/android/apps/gmm/base/views/ar;->g:I

    .line 1168
    iget v9, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/ar;->e:I

    add-int/2addr v0, v9

    iput v0, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1169
    const/4 v0, -0x3

    if-ne v8, v0, :cond_7

    .line 1172
    const/4 v0, 0x0

    :goto_3
    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v9, v9

    if-ge v0, v9, :cond_7

    .line 1173
    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v9, v9, v0

    iget v10, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    iput v10, v9, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1172
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1141
    :cond_3
    iget-object v9, v0, Lcom/google/android/apps/gmm/base/views/ar;->h:Lcom/google/android/apps/gmm/base/views/an;

    if-eqz v9, :cond_4

    .line 1144
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ar;->h:Lcom/google/android/apps/gmm/base/views/an;

    goto :goto_2

    .line 1145
    :cond_4
    if-ltz v8, :cond_5

    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v9, v9

    if-ge v8, v9, :cond_5

    .line 1146
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v2, v2, v8

    goto :goto_2

    .line 1147
    :cond_5
    const/4 v9, -0x1

    if-ne v8, v9, :cond_6

    .line 1148
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->d()Lcom/google/android/apps/gmm/base/views/an;

    move-result-object v2

    goto :goto_2

    .line 1149
    :cond_6
    const/4 v9, -0x2

    if-eq v8, v9, :cond_1

    const/4 v9, -0x4

    if-eq v8, v9, :cond_1

    .line 1153
    sget-object v9, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    const-string v10, "Illegal column preference: %d/%d"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 1154
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v13, v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    .line 1153
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1178
    :cond_7
    iget v0, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    if-le v0, v1, :cond_a

    .line 1179
    iget v0, v2, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1185
    :goto_4
    const/4 v1, -0x3

    if-ne v8, v1, :cond_9

    .line 1186
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 1131
    :goto_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move v1, v0

    goto/16 :goto_1

    .line 1191
    :cond_8
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v6

    long-to-float v0, v2

    const v2, 0x49742400    # 1000000.0f

    div-float/2addr v0, v2

    .line 1192
    sget-object v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "layoutContents() trace: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1198
    return v1

    :cond_9
    move-object v1, v2

    goto :goto_5

    :cond_a
    move v0, v1

    goto :goto_4

    :cond_b
    move v0, v1

    move-object v1, v2

    goto :goto_5
.end method

.method private d()Lcom/google/android/apps/gmm/base/views/an;
    .locals 6

    .prologue
    .line 1202
    const v2, 0x7fffffff

    .line 1203
    const/4 v1, 0x0

    .line 1204
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v3, v3

    move v5, v0

    move-object v0, v1

    move v1, v2

    move v2, v5

    :goto_0
    if-ge v2, v3, :cond_1

    .line 1205
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v4, v4, v2

    iget v4, v4, Lcom/google/android/apps/gmm/base/views/an;->a:I

    if-ge v4, v1, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v0, v0, v2

    iget v1, v0, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1207
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v0, v0, v2

    .line 1204
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1210
    :cond_1
    return-object v0
.end method

.method private e()Lcom/google/android/apps/gmm/base/views/an;
    .locals 6

    .prologue
    .line 1214
    const/high16 v2, -0x80000000

    .line 1215
    const/4 v1, 0x0

    .line 1216
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v3, v3

    move v5, v0

    move-object v0, v1

    move v1, v2

    move v2, v5

    :goto_0
    if-ge v2, v3, :cond_1

    .line 1217
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v4, v4, v2

    iget v4, v4, Lcom/google/android/apps/gmm/base/views/an;->a:I

    if-le v4, v1, :cond_0

    .line 1218
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v0, v0, v2

    iget v1, v0, Lcom/google/android/apps/gmm/base/views/an;->a:I

    .line 1219
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v0, v0, v2

    .line 1216
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1222
    :cond_1
    return-object v0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 896
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a(Z)V

    .line 897
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    .line 1443
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    .line 1444
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1445
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    .line 1446
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    .line 1447
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/ar;->c:I

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/gmm/base/views/aw;->a(ILandroid/view/View;)V

    .line 1443
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 1446
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1450
    :cond_2
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1424
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    .line 1425
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    .line 1428
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1425
    goto :goto_0

    .line 1428
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_1
.end method

.method protected findViewTraversal(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1508
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getId()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1523
    :goto_0
    return-object p0

    .line 1511
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 1512
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1513
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 1514
    goto :goto_0

    .line 1511
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1517
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1518
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 1519
    if-eqz p0, :cond_3

    goto :goto_0

    .line 1523
    :cond_4
    const/4 p0, 0x0

    goto :goto_0
.end method

.method protected findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1535
    const/4 v0, 0x0

    .line 1536
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1538
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 1539
    if-eqz v0, :cond_0

    .line 1544
    :cond_1
    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1227
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a(Z)V

    .line 1228
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 901
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->o:I

    .line 902
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->f:Z

    if-eqz v1, :cond_f

    .line 903
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 904
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    int-to-float v0, v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->b(Landroid/content/Context;F)I

    move-result v0

    .line 905
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;I)I

    move-result v0

    move v1, v0

    .line 908
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->h:I

    if-eq p1, v0, :cond_1

    move v2, v3

    .line 909
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v0, v0

    if-ne v0, v1, :cond_0

    if-nez v2, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->i:I

    if-eq p2, v0, :cond_2

    :cond_0
    move v8, v3

    .line 914
    :goto_2
    if-nez v8, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v9

    move v7, v4

    move v5, v4

    :goto_3
    if-ge v7, v9, :cond_4

    invoke-virtual {p0, v7}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    if-nez v6, :cond_3

    move v6, v3

    :goto_4
    if-nez v6, :cond_e

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v10, v5, v6}, Landroid/view/View;->measure(II)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    iput v5, v0, Lcom/google/android/apps/gmm/base/views/ar;->e:I

    move v0, v3

    :goto_5
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v0

    goto :goto_3

    :cond_1
    move v2, v4

    .line 908
    goto :goto_1

    :cond_2
    move v8, v4

    .line 909
    goto :goto_2

    :cond_3
    move v6, v4

    .line 914
    goto :goto_4

    :cond_4
    if-eqz v5, :cond_5

    .line 917
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->c()I

    move-result v0

    .line 918
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->setMeasuredDimension(II)V

    .line 962
    :goto_6
    iput p2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->i:I

    .line 963
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->h:I

    .line 964
    return-void

    .line 919
    :cond_5
    if-eqz v8, :cond_d

    .line 922
    new-instance v3, Ljava/util/IdentityHashMap;

    invoke-direct {v3}, Ljava/util/IdentityHashMap;-><init>()V

    .line 923
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->c:Z

    if-eqz v0, :cond_a

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    array-length v0, v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v5}, Landroid/widget/Adapter;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_7

    .line 925
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->k:[Z

    .line 927
    :cond_7
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->c:Z

    .line 928
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildCount()I

    move-result v5

    move v0, v4

    :goto_7
    if-ge v0, v5, :cond_8

    .line 929
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 930
    invoke-virtual {v3, v6, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 932
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/aw;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 938
    :goto_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v0, v0

    if-eq v0, v1, :cond_b

    .line 939
    :cond_9
    new-array v0, v1, [Lcom/google/android/apps/gmm/base/views/an;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    move v0, v4

    .line 940
    :goto_9
    if-ge v0, v1, :cond_b

    .line 941
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    new-instance v6, Lcom/google/android/apps/gmm/base/views/an;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/base/views/an;-><init>()V

    aput-object v6, v5, v0

    .line 940
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 934
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->b()V

    goto :goto_8

    .line 944
    :cond_b
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 945
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->b:I

    add-int/lit8 v6, v1, -0x1

    mul-int/2addr v0, v6

    .line 946
    sub-int v0, v5, v0

    int-to-float v0, v0

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 947
    const/4 v0, 0x0

    .line 948
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    array-length v6, v6

    :goto_a
    if-ge v4, v6, :cond_c

    .line 949
    iget-object v7, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v7, v7, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    iput v8, v7, Lcom/google/android/apps/gmm/base/views/an;->b:I

    .line 950
    iget-object v7, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v7, v7, v4

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->n:[Lcom/google/android/apps/gmm/base/views/an;

    aget-object v8, v8, v4

    iget v8, v8, Lcom/google/android/apps/gmm/base/views/an;->b:I

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v7, Lcom/google/android/apps/gmm/base/views/an;->c:I

    .line 951
    iget v7, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->b:I

    int-to-float v7, v7

    add-float/2addr v7, v1

    add-float/2addr v0, v7

    .line 948
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 954
    :cond_c
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a(ZLjava/util/IdentityHashMap;)V

    .line 956
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->c()I

    move-result v0

    .line 957
    invoke-virtual {p0, v5, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->setMeasuredDimension(II)V

    goto/16 :goto_6

    .line 959
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->setMeasuredDimension(II)V

    goto/16 :goto_6

    :cond_e
    move v0, v5

    goto/16 :goto_5

    :cond_f
    move v1, v0

    goto/16 :goto_0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1434
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->m:Z

    if-nez v0, :cond_0

    .line 1438
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1440
    :cond_0
    return-void
.end method
