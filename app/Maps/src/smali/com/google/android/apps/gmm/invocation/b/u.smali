.class public Lcom/google/android/apps/gmm/invocation/b/u;
.super Lcom/google/android/apps/gmm/invocation/b/e;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected final b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 34
    .line 37
    invoke-static {}, Lcom/google/r/b/a/fr;->d()Lcom/google/r/b/a/fr;

    move-result-object v1

    .line 34
    if-eqz p1, :cond_1

    const/16 v0, 0x19

    invoke-virtual {p1, v4, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    :goto_1
    check-cast v0, Lcom/google/r/b/a/fr;

    .line 38
    iget v1, v0, Lcom/google/r/b/a/fr;->b:I

    invoke-static {v1}, Lcom/google/r/b/a/ft;->a(I)Lcom/google/r/b/a/ft;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    move-object v2, v1

    .line 39
    :goto_2
    iget-object v1, v0, Lcom/google/r/b/a/fr;->c:Ljava/lang/Object;

    instance-of v5, v1, Ljava/lang/String;

    if-eqz v5, :cond_4

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 40
    :goto_3
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    :cond_0
    move v0, v4

    :goto_4
    if-eqz v0, :cond_7

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v1, "No redirection url in response."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v2, v1

    .line 38
    goto :goto_2

    .line 39
    :cond_4
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    iput-object v5, v0, Lcom/google/r/b/a/fr;->c:Ljava/lang/Object;

    :cond_5
    move-object v1, v5

    goto :goto_3

    :cond_6
    move v0, v3

    .line 40
    goto :goto_4

    .line 44
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/invocation/b/z;->a:[I

    invoke-virtual {v2}, Lcom/google/r/b/a/ft;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v5, "Non matching actiontype for (%s, %s) "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v3

    aput-object v2, v6, v4

    .line 92
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/v;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/v;-><init>(Lcom/google/android/apps/gmm/invocation/b/u;Ljava/lang/String;)V

    .line 83
    :goto_5
    return-object v0

    .line 65
    :pswitch_1
    invoke-static {v1}, Lcom/google/android/apps/gmm/invocation/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/w;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/w;-><init>(Lcom/google/android/apps/gmm/invocation/b/u;Ljava/lang/String;)V

    goto :goto_5

    .line 72
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/invocation/b/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 73
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->j()Lcom/google/android/apps/gmm/shared/net/a/i;

    move-result-object v0

    .line 72
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/invocation/a/c;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/net/a/i;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/x;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/x;-><init>(Lcom/google/android/apps/gmm/invocation/b/u;Ljava/lang/String;)V

    goto :goto_5

    .line 83
    :cond_9
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/y;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/y;-><init>(Lcom/google/android/apps/gmm/invocation/b/u;Ljava/lang/String;)V

    goto :goto_5

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
