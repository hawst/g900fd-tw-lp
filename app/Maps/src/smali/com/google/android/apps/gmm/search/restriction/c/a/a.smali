.class public abstract Lcom/google/android/apps/gmm/search/restriction/c/a/a;
.super Lcom/google/android/apps/gmm/base/l/w;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;


# instance fields
.field private final d:Lcom/google/android/libraries/curvular/bi;

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/bi;Lcom/google/android/libraries/curvular/bi;Lcom/google/b/f/t;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/gmm/base/l/w;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/bi;Lcom/google/b/f/t;)V

    .line 43
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/restriction/c/a/a;->d:Lcom/google/android/libraries/curvular/bi;

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a/a;->e:Landroid/content/Context;

    .line 45
    return-void
.end method


# virtual methods
.method public final ag_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a/a;->d:Lcom/google/android/libraries/curvular/bi;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a/a;->e:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/bi;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
