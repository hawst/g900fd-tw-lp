.class public Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"


# instance fields
.field private c:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final b()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/apps/gmm/m;->b:I

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 40
    sget v0, Lcom/google/android/apps/gmm/h;->ap:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->c:Landroid/view/ViewGroup;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->c:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/gmm/l;->la:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->a(Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;->c:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/gmm/g;->aX:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 35
    :cond_0
    return-void
.end method
