.class public Lcom/google/android/apps/gmm/map/internal/d/af;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/bc;

.field final b:I

.field c:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/bc;IJ)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 40
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    .line 41
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->c:J

    .line 42
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p0, p1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/d/af;

    if-eqz v2, :cond_3

    .line 67
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/af;

    .line 68
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/bc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 71
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->b:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/af;->a:Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/bc;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
