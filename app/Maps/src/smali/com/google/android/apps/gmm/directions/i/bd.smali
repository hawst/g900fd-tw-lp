.class public Lcom/google/android/apps/gmm/directions/i/bd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/v;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/directions/i/bd;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/directions/bc;

.field private final c:Ljava/lang/Boolean;

.field private final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/bd;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v1, v2, v2}, Lcom/google/android/apps/gmm/directions/i/bd;-><init>(Lcom/google/android/apps/gmm/directions/bc;ZLjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/bd;->a:Lcom/google/android/apps/gmm/directions/i/bd;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/directions/bc;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/bd;->b:Lcom/google/android/apps/gmm/directions/bc;

    .line 35
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bd;->c:Ljava/lang/Boolean;

    .line 36
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/bd;->d:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/i/bd;->e:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/bi;Lcom/google/android/apps/gmm/map/r/a/h;)Lcom/google/android/apps/gmm/directions/i/bd;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-boolean v3, p0, Lcom/google/maps/g/a/bi;->j:Z

    .line 46
    new-instance v4, Lcom/google/android/apps/gmm/directions/i/bd;

    new-instance v5, Lcom/google/android/apps/gmm/directions/bc;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/gmm/directions/bc;-><init>(Lcom/google/maps/g/a/bi;Lcom/google/android/apps/gmm/map/r/a/h;)V

    if-eqz v3, :cond_0

    move-object v2, v1

    .line 49
    :goto_0
    if-eqz v3, :cond_1

    move-object v0, v1

    .line 50
    :goto_1
    invoke-direct {v4, v5, v3, v2, v0}, Lcom/google/android/apps/gmm/directions/i/bd;-><init>(Lcom/google/android/apps/gmm/directions/bc;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ai;->d()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/bi;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ai;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bd;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bd;->b:Lcom/google/android/apps/gmm/directions/bc;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bd;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bd;->e:Ljava/lang/String;

    return-object v0
.end method
