.class public Lcom/google/android/apps/gmm/search/aj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field A:Lcom/google/r/b/a/ado;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field B:Lcom/google/r/b/a/aky;

.field C:Lcom/google/r/b/a/acc;

.field private D:Lcom/google/r/b/a/agd;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private E:Z

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/akl;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/akl;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Lcom/google/maps/a/a;

.field public e:[B
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/map/internal/d/bd;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public g:Lcom/google/e/a/a/a/b;

.field public h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field i:I

.field j:I

.field k:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field l:Lcom/google/android/apps/gmm/place/ck;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public m:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public n:Lcom/google/maps/g/hy;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field o:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field p:Lcom/google/maps/g/qm;

.field public q:Lcom/google/r/b/a/alh;

.field r:Lcom/google/maps/g/qm;

.field public s:Lcom/google/r/b/a/alc;

.field public t:Lcom/google/e/a/a/a/b;

.field u:Z

.field v:Z

.field w:[B
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field x:Z

.field y:Lcom/google/android/apps/gmm/map/internal/d/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field z:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    .line 73
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/apps/gmm/search/aj;->b:I

    .line 74
    iput v2, p0, Lcom/google/android/apps/gmm/search/aj;->c:I

    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/search/ai;->c:Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    .line 78
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    .line 79
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->h:Ljava/lang/String;

    .line 83
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->l:Lcom/google/android/apps/gmm/place/ck;

    .line 84
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    .line 85
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 86
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->o:Lcom/google/e/a/a/a/b;

    .line 87
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->p:Lcom/google/maps/g/qm;

    .line 89
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    .line 90
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->r:Lcom/google/maps/g/qm;

    .line 91
    invoke-static {}, Lcom/google/r/b/a/alc;->d()Lcom/google/r/b/a/alc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->s:Lcom/google/r/b/a/alc;

    .line 92
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->t:Lcom/google/e/a/a/a/b;

    .line 93
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/aj;->u:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/aj;->v:Z

    .line 97
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->w:[B

    .line 98
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/aj;->x:Z

    .line 99
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->y:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 101
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->z:Ljava/lang/String;

    .line 102
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->A:Lcom/google/r/b/a/ado;

    .line 103
    invoke-static {}, Lcom/google/r/b/a/aky;->d()Lcom/google/r/b/a/aky;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->B:Lcom/google/r/b/a/aky;

    .line 104
    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aj;->D:Lcom/google/r/b/a/agd;

    .line 105
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/aj;->E:Z

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->F:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->G:Ljava/util/List;

    .line 108
    sget-object v0, Lcom/google/r/b/a/acc;->a:Lcom/google/r/b/a/acc;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->C:Lcom/google/r/b/a/acc;

    .line 111
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/search/ai;)V
    .locals 10

    .prologue
    const/16 v9, 0x15

    const/16 v8, 0x1a

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    .line 73
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/apps/gmm/search/aj;->b:I

    .line 74
    iput v1, p0, Lcom/google/android/apps/gmm/search/aj;->c:I

    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/search/ai;->c:Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    .line 78
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    .line 79
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->h:Ljava/lang/String;

    .line 83
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->l:Lcom/google/android/apps/gmm/place/ck;

    .line 84
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    .line 85
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 86
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->o:Lcom/google/e/a/a/a/b;

    .line 87
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->p:Lcom/google/maps/g/qm;

    .line 89
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    .line 90
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->r:Lcom/google/maps/g/qm;

    .line 91
    invoke-static {}, Lcom/google/r/b/a/alc;->d()Lcom/google/r/b/a/alc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->s:Lcom/google/r/b/a/alc;

    .line 92
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->t:Lcom/google/e/a/a/a/b;

    .line 93
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/aj;->u:Z

    .line 94
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/aj;->v:Z

    .line 97
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->w:[B

    .line 98
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/aj;->x:Z

    .line 99
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->y:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 101
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->z:Ljava/lang/String;

    .line 102
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->A:Lcom/google/r/b/a/ado;

    .line 103
    invoke-static {}, Lcom/google/r/b/a/aky;->d()Lcom/google/r/b/a/aky;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->B:Lcom/google/r/b/a/aky;

    .line 104
    iput-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->D:Lcom/google/r/b/a/agd;

    .line 105
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/aj;->E:Z

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->F:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->G:Ljava/util/List;

    .line 108
    sget-object v0, Lcom/google/r/b/a/acc;->a:Lcom/google/r/b/a/acc;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->C:Lcom/google/r/b/a/acc;

    .line 114
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1c

    invoke-virtual {v0, v4, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    .line 115
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    iput v0, p0, Lcom/google/android/apps/gmm/search/aj;->c:I

    .line 116
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    iput v0, p0, Lcom/google/android/apps/gmm/search/aj;->b:I

    .line 117
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0xc

    const/16 v5, 0x19

    invoke-virtual {v0, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->e:[B

    .line 118
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v5, 0xe

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_e

    move v0, v4

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    :cond_0
    move v0, v4

    :goto_1
    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v4, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    :goto_2
    iput v0, p0, Lcom/google/android/apps/gmm/search/aj;->i:I

    .line 119
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x10

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_11

    move v0, v4

    :goto_3
    if-nez v0, :cond_1

    invoke-virtual {v2, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_12

    :cond_1
    move v0, v4

    :goto_4
    if-eqz v0, :cond_13

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x10

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v4, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    :goto_5
    iput v0, p0, Lcom/google/android/apps/gmm/search/aj;->j:I

    .line 120
    iget-object v5, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 121
    if-eqz v5, :cond_7

    .line 122
    const/4 v2, 0x2

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_14

    move v0, v4

    :goto_6
    if-nez v0, :cond_2

    invoke-virtual {v5, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_15

    :cond_2
    move v0, v4

    :goto_7
    if-eqz v0, :cond_3

    .line 123
    const/4 v0, 0x2

    .line 124
    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v2

    .line 123
    if-eqz v5, :cond_16

    const/16 v6, 0x19

    invoke-virtual {v5, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_8
    if-eqz v0, :cond_17

    :goto_9
    check-cast v0, Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    .line 126
    :cond_3
    const/16 v2, 0xb

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_18

    move v0, v4

    :goto_a
    if-nez v0, :cond_4

    invoke-virtual {v5, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_19

    :cond_4
    move v0, v4

    :goto_b
    if-eqz v0, :cond_5

    .line 127
    const/16 v0, 0xb

    .line 128
    invoke-virtual {v5, v0, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    .line 130
    :cond_5
    const/16 v2, 0xf

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1a

    move v0, v4

    :goto_c
    if-nez v0, :cond_6

    invoke-virtual {v5, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1b

    :cond_6
    move v0, v4

    :goto_d
    if-eqz v0, :cond_7

    .line 131
    const/16 v0, 0xf

    .line 132
    invoke-virtual {v5, v0, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->k:Lcom/google/e/a/a/a/b;

    .line 135
    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x20

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_1c

    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    :goto_e
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->p:Lcom/google/maps/g/qm;

    .line 136
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->a()Lcom/google/maps/g/qm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->r:Lcom/google/maps/g/qm;

    .line 137
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->c()Lcom/google/r/b/a/alh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    .line 138
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v9, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/alc;->d()Lcom/google/r/b/a/alc;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_1e

    :goto_f
    check-cast v0, Lcom/google/r/b/a/alc;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->s:Lcom/google/r/b/a/alc;

    .line 139
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0xa

    const/16 v5, 0x1c

    invoke-virtual {v0, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->h:Ljava/lang/String;

    .line 140
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->d()Lcom/google/android/apps/gmm/place/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->l:Lcom/google/android/apps/gmm/place/ck;

    .line 141
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x14

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1f

    move v0, v4

    :goto_10
    if-nez v0, :cond_8

    invoke-virtual {v2, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_20

    :cond_8
    move v0, v4

    :goto_11
    if-eqz v0, :cond_23

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x14

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_21

    move v2, v4

    :goto_12
    if-nez v2, :cond_9

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_22

    :cond_9
    move v2, v4

    :goto_13
    if-eqz v2, :cond_23

    invoke-virtual {v0, v4, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    :goto_14
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    .line 142
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->e()Lcom/google/maps/g/hy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 143
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x1b

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_24

    move v0, v4

    :goto_15
    if-nez v0, :cond_a

    invoke-virtual {v2, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_25

    :cond_a
    move v0, v4

    :goto_16
    if-eqz v0, :cond_26

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1b

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    :goto_17
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->t:Lcom/google/e/a/a/a/b;

    .line 144
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1f

    const/16 v5, 0x18

    invoke-virtual {v0, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->u:Z

    .line 145
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x7

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->v:Z

    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x21

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_27

    move-object v0, v3

    :goto_18
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->w:[B

    .line 147
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/search/ai;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->x:Z

    .line 148
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->y:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 149
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x23

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_28

    move v0, v4

    :goto_19
    if-nez v0, :cond_b

    invoke-virtual {v2, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_29

    :cond_b
    move v0, v4

    :goto_1a
    if-eqz v0, :cond_2a

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x23

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1c

    invoke-virtual {v0, v4, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1b
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->z:Ljava/lang/String;

    .line 150
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x24

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/ado;->d()Lcom/google/r/b/a/ado;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_2b

    :goto_1c
    check-cast v0, Lcom/google/r/b/a/ado;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->A:Lcom/google/r/b/a/ado;

    .line 151
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x25

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/aky;->d()Lcom/google/r/b/a/aky;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_2c

    :goto_1d
    check-cast v0, Lcom/google/r/b/a/aky;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->B:Lcom/google/r/b/a/aky;

    .line 152
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x26

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->D:Lcom/google/r/b/a/agd;

    .line 153
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x18

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->E:Z

    .line 154
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->f()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->F:Ljava/util/List;

    .line 155
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->G:Ljava/util/List;

    .line 156
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v3, 0x8

    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2d

    move v0, v4

    :goto_1e
    if-nez v0, :cond_c

    invoke-virtual {v2, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    :cond_c
    move v1, v4

    :cond_d
    if-eqz v1, :cond_2e

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/r/b/a/acc;->a:Lcom/google/r/b/a/acc;

    :goto_1f
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->C:Lcom/google/r/b/a/acc;

    .line 157
    return-void

    :cond_e
    move v0, v1

    .line 118
    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto/16 :goto_1

    :cond_10
    move v0, v1

    goto/16 :goto_2

    :cond_11
    move v0, v1

    .line 119
    goto/16 :goto_3

    :cond_12
    move v0, v1

    goto/16 :goto_4

    :cond_13
    move v0, v1

    goto/16 :goto_5

    :cond_14
    move v0, v1

    .line 122
    goto/16 :goto_6

    :cond_15
    move v0, v1

    goto/16 :goto_7

    :cond_16
    move-object v0, v3

    .line 123
    goto/16 :goto_8

    :cond_17
    move-object v0, v2

    goto/16 :goto_9

    :cond_18
    move v0, v1

    .line 126
    goto/16 :goto_a

    :cond_19
    move v0, v1

    goto/16 :goto_b

    :cond_1a
    move v0, v1

    .line 130
    goto/16 :goto_c

    :cond_1b
    move v0, v1

    goto/16 :goto_d

    .line 135
    :cond_1c
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_1d

    :goto_20
    check-cast v0, Lcom/google/maps/g/qm;

    goto/16 :goto_e

    :cond_1d
    move-object v0, v2

    goto :goto_20

    :cond_1e
    move-object v0, v2

    .line 138
    goto/16 :goto_f

    :cond_1f
    move v0, v1

    .line 141
    goto/16 :goto_10

    :cond_20
    move v0, v1

    goto/16 :goto_11

    :cond_21
    move v2, v1

    goto/16 :goto_12

    :cond_22
    move v2, v1

    goto/16 :goto_13

    :cond_23
    move-object v0, v3

    goto/16 :goto_14

    :cond_24
    move v0, v1

    .line 143
    goto/16 :goto_15

    :cond_25
    move v0, v1

    goto/16 :goto_16

    :cond_26
    move-object v0, v3

    goto/16 :goto_17

    .line 146
    :cond_27
    const/16 v2, 0x19

    invoke-virtual {v0, v4, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto/16 :goto_18

    :cond_28
    move v0, v1

    .line 149
    goto/16 :goto_19

    :cond_29
    move v0, v1

    goto/16 :goto_1a

    :cond_2a
    move-object v0, v3

    goto/16 :goto_1b

    :cond_2b
    move-object v0, v2

    .line 150
    goto/16 :goto_1c

    :cond_2c
    move-object v0, v2

    .line 151
    goto/16 :goto_1d

    :cond_2d
    move v0, v1

    .line 156
    goto/16 :goto_1e

    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/acc;->c:Lcom/google/r/b/a/acc;

    goto :goto_1f

    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/acc;->b:Lcom/google/r/b/a/acc;

    goto :goto_1f

    :cond_2e
    sget-object v0, Lcom/google/r/b/a/acc;->a:Lcom/google/r/b/a/acc;

    goto/16 :goto_1f

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/search/ai;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->o:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->o:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/e/a/a/a/b;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_17

    move v0, v2

    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_0
    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_18

    move v0, v2

    :goto_2
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    sget-object v4, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_1
    :goto_3
    const/16 v0, 0x8

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1a

    move v0, v2

    :goto_4
    if-nez v0, :cond_2

    const/16 v0, 0x8

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->C:Lcom/google/r/b/a/acc;

    iget v4, v4, Lcom/google/r/b/a/acc;->d:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->e:[B

    if-eqz v0, :cond_3

    const/16 v0, 0xc

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1b

    move v0, v2

    :goto_5
    if-nez v0, :cond_3

    const/16 v0, 0xc

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->e:[B

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_3
    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1c

    move v0, v2

    :goto_6
    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/gmm/search/aj;->c:I

    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v6, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_4
    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1d

    move v0, v2

    :goto_7
    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/gmm/search/aj;->b:I

    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v7, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/search/aj;->i:I

    if-lez v0, :cond_6

    const/16 v0, 0xe

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1e

    move v0, v2

    :goto_8
    if-nez v0, :cond_6

    const/16 v0, 0xe

    iget v4, p0, Lcom/google/android/apps/gmm/search/aj;->i:I

    invoke-static {v4}, Lcom/google/android/apps/gmm/search/bc;->a(I)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/search/aj;->j:I

    if-lez v0, :cond_7

    const/16 v0, 0x10

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1f

    move v0, v2

    :goto_9
    if-nez v0, :cond_7

    const/16 v0, 0x10

    iget v4, p0, Lcom/google/android/apps/gmm/search/aj;->j:I

    invoke-static {v4}, Lcom/google/android/apps/gmm/search/bc;->a(I)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_8

    const/16 v0, 0xb

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_20

    move v0, v2

    :goto_a
    if-nez v0, :cond_8

    const/16 v0, 0xb

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->h:Ljava/lang/String;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_21

    move v0, v2

    :goto_b
    if-nez v0, :cond_9

    const/16 v0, 0xa

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->h:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->k:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_a

    const/16 v0, 0xf

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_22

    move v0, v2

    :goto_c
    if-nez v0, :cond_a

    const/16 v0, 0xf

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->k:Lcom/google/e/a/a/a/b;

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_a
    const/16 v0, 0x12

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_23

    move v0, v2

    :goto_d
    if-nez v0, :cond_b

    const/16 v0, 0x12

    sget-object v4, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    iget v4, v4, Lcom/google/r/b/a/acy;->t:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    :cond_b
    const/16 v0, 0x1a

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_24

    move v0, v2

    :goto_e
    if-nez v0, :cond_c

    const/16 v4, 0x1a

    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->l:Lcom/google/android/apps/gmm/place/ck;

    if-eqz v0, :cond_d

    const/16 v0, 0xd

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_25

    move v0, v2

    :goto_f
    if-nez v0, :cond_d

    const/16 v0, 0xd

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->l:Lcom/google/android/apps/gmm/place/ck;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/place/ck;->a()Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_d
    const/16 v0, 0x14

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_26

    move v0, v2

    :goto_10
    if-nez v0, :cond_f

    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/g/b/ac;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_e
    invoke-static {}, Lcom/google/maps/g/pj;->newBuilder()Lcom/google/maps/g/pl;

    move-result-object v0

    iget v5, v0, Lcom/google/maps/g/pl;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/maps/g/pl;->a:I

    iput-boolean v3, v0, Lcom/google/maps/g/pl;->b:Z

    iget v5, v0, Lcom/google/maps/g/pl;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v0, Lcom/google/maps/g/pl;->a:I

    iput-boolean v2, v0, Lcom/google/maps/g/pl;->c:Z

    invoke-virtual {v0}, Lcom/google/maps/g/pl;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pj;

    const/4 v5, 0x5

    sget-object v6, Lcom/google/maps/g/b/ah;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v6, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v6, v5, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/16 v0, 0x14

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_f
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->r:Lcom/google/maps/g/qm;

    invoke-interface {v4}, Lcom/google/n/at;->l()[B

    move-result-object v4

    invoke-interface {v0}, Lcom/google/n/at;->l()[B

    move-result-object v0

    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_10

    const/16 v0, 0x1e

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_27

    move v0, v2

    :goto_11
    if-nez v0, :cond_10

    const/16 v0, 0x1e

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->r:Lcom/google/maps/g/qm;

    sget-object v5, Lcom/google/maps/g/b/al;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_10
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    invoke-interface {v4}, Lcom/google/n/at;->l()[B

    move-result-object v4

    invoke-interface {v0}, Lcom/google/n/at;->l()[B

    move-result-object v0

    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x19

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_28

    move v0, v2

    :goto_12
    if-nez v0, :cond_11

    const/16 v0, 0x19

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    sget-object v5, Lcom/google/r/b/a/b/bd;->k:Lcom/google/e/a/a/a/d;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_11
    invoke-static {}, Lcom/google/r/b/a/alc;->d()Lcom/google/r/b/a/alc;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->s:Lcom/google/r/b/a/alc;

    invoke-interface {v4}, Lcom/google/n/at;->l()[B

    move-result-object v4

    invoke-interface {v0}, Lcom/google/n/at;->l()[B

    move-result-object v0

    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_12

    const/16 v0, 0x15

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_29

    move v0, v2

    :goto_13
    if-nez v0, :cond_12

    const/16 v0, 0x15

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->s:Lcom/google/r/b/a/alc;

    sget-object v5, Lcom/google/r/b/a/b/bd;->b:Lcom/google/e/a/a/a/d;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    if-eqz v0, :cond_13

    const/16 v0, 0x13

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2a

    move v0, v2

    :goto_14
    if-nez v0, :cond_13

    const/16 v0, 0x13

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    sget-object v5, Lcom/google/maps/g/b/r;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->t:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_14

    const/16 v0, 0x1b

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2b

    move v0, v2

    :goto_15
    if-nez v0, :cond_14

    const/16 v0, 0x1b

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->t:Lcom/google/e/a/a/a/b;

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_14
    const/16 v0, 0x17

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2c

    move v0, v2

    :goto_16
    if-nez v0, :cond_15

    invoke-static {v1}, Lcom/google/android/apps/gmm/util/a;->a(Lcom/google/e/a/a/a/b;)V

    :cond_15
    const/16 v0, 0x20

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2d

    move v0, v2

    :goto_17
    if-nez v0, :cond_2f

    invoke-static {}, Lcom/google/r/b/a/akn;->newBuilder()Lcom/google/r/b/a/akp;

    move-result-object v0

    iget v4, v0, Lcom/google/r/b/a/akp;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/akp;->a:I

    iput-boolean v2, v0, Lcom/google/r/b/a/akp;->b:Z

    iget v4, v0, Lcom/google/r/b/a/akp;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/r/b/a/akp;->a:I

    iput-boolean v2, v0, Lcom/google/r/b/a/akp;->c:Z

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->p:Lcom/google/maps/g/qm;

    if-nez v4, :cond_2e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bd;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_17
    move v0, v3

    goto/16 :goto_1

    :cond_18
    move v0, v3

    goto/16 :goto_2

    :cond_19
    const-string v0, "SearchParameters"

    const-string v4, "inputCamera not set."

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_1a
    move v0, v3

    goto/16 :goto_4

    :cond_1b
    move v0, v3

    goto/16 :goto_5

    :cond_1c
    move v0, v3

    goto/16 :goto_6

    :cond_1d
    move v0, v3

    goto/16 :goto_7

    :cond_1e
    move v0, v3

    goto/16 :goto_8

    :cond_1f
    move v0, v3

    goto/16 :goto_9

    :cond_20
    move v0, v3

    goto/16 :goto_a

    :cond_21
    move v0, v3

    goto/16 :goto_b

    :cond_22
    move v0, v3

    goto/16 :goto_c

    :cond_23
    move v0, v3

    goto/16 :goto_d

    :cond_24
    move v0, v3

    goto/16 :goto_e

    :cond_25
    move v0, v3

    goto/16 :goto_f

    :cond_26
    move v0, v3

    goto/16 :goto_10

    :cond_27
    move v0, v3

    goto/16 :goto_11

    :cond_28
    move v0, v3

    goto/16 :goto_12

    :cond_29
    move v0, v3

    goto/16 :goto_13

    :cond_2a
    move v0, v3

    goto/16 :goto_14

    :cond_2b
    move v0, v3

    goto/16 :goto_15

    :cond_2c
    move v0, v3

    goto/16 :goto_16

    :cond_2d
    move v0, v3

    goto :goto_17

    :cond_2e
    iget-object v5, v0, Lcom/google/r/b/a/akp;->d:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v4, 0x0

    iput-object v4, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v5, Lcom/google/n/ao;->d:Z

    iget v4, v0, Lcom/google/r/b/a/akp;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/akp;->a:I

    const/16 v4, 0x20

    invoke-virtual {v0}, Lcom/google/r/b/a/akp;->g()Lcom/google/n/t;

    move-result-object v0

    sget-object v5, Lcom/google/r/b/a/b/bd;->l:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->z:Ljava/lang/String;

    if-eqz v0, :cond_32

    const/16 v0, 0x23

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_30

    move v0, v2

    :goto_18
    if-nez v0, :cond_32

    invoke-static {}, Lcom/google/r/b/a/aac;->newBuilder()Lcom/google/r/b/a/aae;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->z:Ljava/lang/String;

    if-nez v4, :cond_31

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_30
    move v0, v3

    goto :goto_18

    :cond_31
    iget v5, v0, Lcom/google/r/b/a/aae;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/r/b/a/aae;->a:I

    iput-object v4, v0, Lcom/google/r/b/a/aae;->b:Ljava/lang/Object;

    const/16 v4, 0x23

    invoke-virtual {v0}, Lcom/google/r/b/a/aae;->g()Lcom/google/n/t;

    move-result-object v0

    sget-object v5, Lcom/google/r/b/a/b/at;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_32
    const/16 v0, 0x1f

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_39

    move v0, v2

    :goto_19
    if-nez v0, :cond_33

    const/16 v4, 0x1f

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->u:Z

    if-eqz v0, :cond_3a

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_1a
    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_33
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->u:Z

    if-eqz v0, :cond_34

    const/16 v0, 0x22

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3b

    move v0, v2

    :goto_1b
    if-nez v0, :cond_34

    const/16 v4, 0x22

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_34
    const/4 v0, 0x7

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3c

    move v0, v2

    :goto_1c
    if-nez v0, :cond_35

    const/4 v4, 0x7

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->v:Z

    if-eqz v0, :cond_3d

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_1d
    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_35
    const/16 v0, 0x24

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3e

    move v0, v2

    :goto_1e
    if-nez v0, :cond_36

    const/16 v0, 0x24

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/aj;->A:Lcom/google/r/b/a/ado;

    sget-object v5, Lcom/google/r/b/a/b/aw;->r:Lcom/google/e/a/a/a/d;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v4

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_36
    const/16 v0, 0x25

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3f

    move v0, v2

    :goto_1f
    if-nez v0, :cond_37

    const/16 v0, 0x25

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/aj;->B:Lcom/google/r/b/a/aky;

    sget-object v3, Lcom/google/r/b/a/b/bd;->c:Lcom/google/e/a/a/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 343
    :cond_37
    new-instance v2, Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/search/ai;-><init>(Lcom/google/e/a/a/a/b;)V

    .line 347
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->x:Z

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/search/ai;->a:Z

    .line 350
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/aj;->x:Z

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->y:Lcom/google/android/apps/gmm/map/internal/d/b;

    if-eqz v0, :cond_40

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->y:Lcom/google/android/apps/gmm/map/internal/d/b;

    iput-object v0, v2, Lcom/google/android/apps/gmm/search/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 356
    const/16 v0, 0x9

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->y:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 357
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v3

    .line 356
    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 368
    :cond_38
    :goto_20
    return-object v2

    :cond_39
    move v0, v3

    .line 342
    goto/16 :goto_19

    :cond_3a
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto/16 :goto_1a

    :cond_3b
    move v0, v3

    goto/16 :goto_1b

    :cond_3c
    move v0, v3

    goto :goto_1c

    :cond_3d
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_1d

    :cond_3e
    move v0, v3

    goto :goto_1e

    :cond_3f
    move v0, v3

    goto :goto_1f

    .line 359
    :cond_40
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aj;->f:Lcom/google/android/apps/gmm/map/internal/d/bd;

    if-eqz v0, :cond_38

    .line 360
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/aj;->f:Lcom/google/android/apps/gmm/map/internal/d/bd;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 361
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_41

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_41
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/a;

    .line 362
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/a;->a()Lcom/google/android/apps/gmm/map/internal/d/b;

    move-result-object v0

    .line 360
    iput-object v0, v2, Lcom/google/android/apps/gmm/search/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 363
    const/16 v0, 0x9

    .line 364
    iget-object v3, v2, Lcom/google/android/apps/gmm/search/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/b;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v3

    .line 363
    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    goto :goto_20
.end method
