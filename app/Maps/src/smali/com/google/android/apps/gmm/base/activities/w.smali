.class public Lcom/google/android/apps/gmm/base/activities/w;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/activities/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/p;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/activities/p;
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x2

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_0

    .line 558
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    if-gez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Floating bar visibility must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Slider view must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Full screen view must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t display the slider and full screen view at the same time."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/f;->f:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    :cond_5
    :goto_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/f;->f:Lcom/google/android/apps/gmm/map/b/a/f;

    if-eq v1, v2, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t show compass on a full screen view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0

    :cond_7
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    if-eqz v1, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t set compass position on a full screen view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    if-ne v1, v3, :cond_9

    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    if-ne v1, v4, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If the floating bar is shown, a size must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    if-eq v1, v4, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If the floating bar is going to be hidden, don\'t change the size."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    if-ne v1, v3, :cond_c

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->c:Z

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    if-nez v1, :cond_c

    :cond_b
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If the floating bar is shown, a floating bar must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    iget v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    if-ne v1, v3, :cond_d

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->d:Lcom/google/android/apps/gmm/base/activities/ac;

    if-eq v1, v2, :cond_d

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->e:Lcom/google/android/apps/gmm/base/activities/ac;

    if-eq v0, v1, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t show both the floating bar and a fixed header at the same time."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 561
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    .line 562
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    .line 563
    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/base/activities/w;
    .locals 1

    .prologue
    .line 250
    packed-switch p1, :pswitch_data_0

    .line 256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 258
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput p1, v0, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    .line 259
    return-object p0

    .line 250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/activities/w;
    .locals 2

    .prologue
    .line 408
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/x;-><init>()V

    .line 409
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 410
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    .line 411
    return-object p0
.end method

.method public final a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput p2, v0, Lcom/google/android/apps/gmm/base/activities/p;->o:I

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    return-object p0
.end method

.method public final a(Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/ac;)Lcom/google/android/apps/gmm/base/activities/w;
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p2, v0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 394
    return-object p0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)Lcom/google/android/apps/gmm/base/activities/w;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p2, v0, Lcom/google/android/apps/gmm/base/activities/p;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 346
    return-object p0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)Lcom/google/android/apps/gmm/base/activities/w;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/activities/p;->K:Ljava/lang/String;

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p2, v0, Lcom/google/android/apps/gmm/base/activities/p;->L:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 536
    return-object p0
.end method
