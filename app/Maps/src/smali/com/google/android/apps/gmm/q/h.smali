.class Lcom/google/android/apps/gmm/q/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/q/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/q/g;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/q/h;->a:Lcom/google/android/apps/gmm/q/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 14
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/gmm/q/h;->a:Lcom/google/android/apps/gmm/q/g;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/q/g;->d:Z

    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/q/g;->c:Z

    :cond_0
    :goto_0
    iget-object v0, v2, Lcom/google/android/apps/gmm/q/g;->b:Lcom/google/android/apps/gmm/q/a;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/apps/gmm/q/g;->b:Lcom/google/android/apps/gmm/q/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 88
    :cond_1
    return-void

    .line 87
    :cond_2
    iget-object v3, v2, Lcom/google/android/apps/gmm/q/g;->a:Lcom/google/android/apps/gmm/q/e;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v3, Lcom/google/android/apps/gmm/q/e;->c:J

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0x1d4c0

    cmp-long v1, v6, v8

    if-gez v1, :cond_3

    iget-object v1, v3, Lcom/google/android/apps/gmm/q/e;->d:Landroid/location/Location;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    iget-object v1, v3, Lcom/google/android/apps/gmm/q/e;->d:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v1, v6, v8

    if-gez v1, :cond_3

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    iget-object v1, v3, Lcom/google/android/apps/gmm/q/e;->d:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v1, v6, v8

    if-gez v1, :cond_3

    iget-object v1, v3, Lcom/google/android/apps/gmm/q/e;->e:Lcom/google/android/apps/gmm/q/f;

    :goto_1
    sget-object v3, Lcom/google/android/apps/gmm/q/f;->a:Lcom/google/android/apps/gmm/q/f;

    if-ne v1, v3, :cond_9

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/q/g;->d:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/q/g;->c:Z

    goto :goto_0

    :cond_3
    iput-object v0, v3, Lcom/google/android/apps/gmm/q/e;->d:Landroid/location/Location;

    iput-wide v4, v3, Lcom/google/android/apps/gmm/q/e;->c:J

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    double-to-float v1, v6

    const v6, 0x40c90fdb

    mul-float/2addr v1, v6

    const/high16 v6, 0x43b40000    # 360.0f

    div-float/2addr v1, v6

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    double-to-float v6, v6

    const v7, 0x40c90fdb

    mul-float/2addr v6, v7

    const/high16 v7, 0x43b40000    # 360.0f

    div-float/2addr v6, v7

    float-to-double v8, v1

    const-wide v10, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v7, v8, v10

    if-lez v7, :cond_4

    float-to-double v8, v1

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    cmpl-double v7, v8, v10

    if-gez v7, :cond_4

    const v7, -0x3f36f025

    cmpg-float v7, v6, v7

    if-ltz v7, :cond_4

    const v7, 0x40c90fdb

    cmpl-float v7, v6, v7

    if-gtz v7, :cond_4

    const-wide v8, 0xdc6d62da00L

    cmp-long v7, v4, v8

    if-gez v7, :cond_5

    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/q/f;->b:Lcom/google/android/apps/gmm/q/f;

    :goto_3
    iput-object v1, v3, Lcom/google/android/apps/gmm/q/e;->e:Lcom/google/android/apps/gmm/q/f;

    iget-object v1, v3, Lcom/google/android/apps/gmm/q/e;->e:Lcom/google/android/apps/gmm/q/f;

    goto :goto_1

    :cond_5
    const-wide v8, 0xdc6d62da00L

    sub-long/2addr v4, v8

    long-to-float v4, v4

    const v5, 0x4ca4cb80    # 8.64E7f

    div-float/2addr v4, v5

    const v5, 0x40c7ae92

    const v7, 0x40c90fdb

    mul-float/2addr v7, v4

    const v8, 0x43b6a0d1

    div-float/2addr v7, v8

    add-float/2addr v5, v7

    const v7, 0x3d08e2fe

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v7, v5

    const v8, 0x39b702d8

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v5

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    const v8, 0x36afb0e6    # 5.236E-6f

    const/high16 v9, 0x40400000    # 3.0f

    mul-float/2addr v9, v5

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    const v8, 0x3fe5f6fd    # 1.7966f

    add-float/2addr v7, v8

    const v8, 0x40490fdb    # (float)Math.PI

    add-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    sget v10, Lcom/google/android/apps/gmm/q/e;->a:F

    float-to-double v10, v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->asin(D)D

    move-result-wide v8

    double-to-float v8, v8

    iget v9, v3, Lcom/google/android/apps/gmm/q/e;->b:F

    float-to-double v10, v1

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    float-to-double v12, v8

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    double-to-float v10, v10

    sub-float/2addr v9, v10

    float-to-double v10, v1

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    float-to-double v12, v8

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    double-to-float v1, v10

    div-float v1, v9, v1

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v8, v1, v8

    if-ltz v8, :cond_6

    sget-object v1, Lcom/google/android/apps/gmm/q/f;->a:Lcom/google/android/apps/gmm/q/f;

    goto :goto_3

    :cond_6
    const/high16 v8, -0x40800000    # -1.0f

    cmpg-float v8, v1, v8

    if-gtz v8, :cond_7

    sget-object v1, Lcom/google/android/apps/gmm/q/f;->b:Lcom/google/android/apps/gmm/q/f;

    goto/16 :goto_3

    :cond_7
    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->acos(D)D

    move-result-wide v8

    double-to-float v1, v8

    neg-float v6, v6

    const v8, 0x40c90fdb

    div-float/2addr v6, v8

    const v8, 0x3a6bedfa    # 9.0E-4f

    add-float/2addr v6, v8

    const v8, 0x3badab9f    # 0.0053f

    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v5, v10

    mul-float/2addr v5, v8

    add-float/2addr v5, v6

    const v6, -0x441de69b    # -0.0069f

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    neg-float v5, v1

    const v6, 0x40c90fdb

    div-float/2addr v5, v6

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_8

    const v5, 0x40c90fdb

    div-float/2addr v1, v5

    cmpg-float v1, v4, v1

    if-gtz v1, :cond_8

    sget-object v1, Lcom/google/android/apps/gmm/q/f;->b:Lcom/google/android/apps/gmm/q/f;

    goto/16 :goto_3

    :cond_8
    sget-object v1, Lcom/google/android/apps/gmm/q/f;->a:Lcom/google/android/apps/gmm/q/f;

    goto/16 :goto_3

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_a
    iget-boolean v1, v2, Lcom/google/android/apps/gmm/q/g;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->b()Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v1, :cond_c

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_5
    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/q/g;->c:Z

    goto/16 :goto_0

    :cond_b
    const/4 v1, 0x0

    goto :goto_4

    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    :cond_d
    const/4 v0, 0x0

    goto :goto_6
.end method
