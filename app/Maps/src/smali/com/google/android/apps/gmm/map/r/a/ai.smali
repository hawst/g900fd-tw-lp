.class public Lcom/google/android/apps/gmm/map/r/a/ai;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/maps/g/a/ac;

.field public b:Lcom/google/android/apps/gmm/map/r/a/ag;


# direct methods
.method private constructor <init>(Lcom/google/maps/g/a/ac;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    .line 27
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/ac;)Lcom/google/android/apps/gmm/map/r/a/ai;
    .locals 2
    .param p0    # Lcom/google/maps/g/a/ac;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 37
    if-eqz p0, :cond_0

    iget v1, p0, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_1

    :goto_0
    if-nez v0, :cond_2

    .line 38
    :cond_0
    const/4 v0, 0x0

    .line 40
    :goto_1
    return-object v0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/r/a/ai;-><init>(Lcom/google/maps/g/a/ac;)V

    goto :goto_1
.end method

.method public static a(Lcom/google/maps/g/a/ae;)Lcom/google/android/apps/gmm/map/r/a/ai;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/maps/g/a/ae;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ac;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a(Lcom/google/maps/g/a/ac;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget-object v0, v2, Lcom/google/maps/g/a/ac;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v0, v0, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget-object v0, v2, Lcom/google/maps/g/a/ac;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->g:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v0, v0, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget-object v0, v2, Lcom/google/maps/g/a/ac;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->h:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v0, v0, Lcom/google/maps/g/a/ac;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget-object v0, v2, Lcom/google/maps/g/a/ac;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v2, Lcom/google/maps/g/a/ac;->i:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v0, v0, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    :cond_0
    sget-object v1, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "StepCue{"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v0, v0, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    :cond_0
    invoke-virtual {v0}, Lcom/google/maps/g/a/af;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
