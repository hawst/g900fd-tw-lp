.class Lcom/google/android/apps/gmm/map/util/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/util/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 101
    const/16 v0, 0x50

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/util/a/c;->onTrimMemory(I)V

    .line 103
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x36

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ComponentCallbacks2: onTrimMemory() level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    const/16 v0, 0x50

    if-lt p1, v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    goto :goto_0

    .line 115
    :cond_2
    const/16 v0, 0x28

    if-lt p1, v0, :cond_3

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    goto :goto_0

    .line 117
    :cond_3
    const/16 v0, 0x14

    if-lt p1, v0, :cond_4

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    goto :goto_0

    .line 119
    :cond_4
    const/16 v0, 0xf

    if-lt p1, v0, :cond_5

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    goto :goto_0

    .line 121
    :cond_5
    const/16 v0, 0xa

    if-lt p1, v0, :cond_6

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    goto :goto_0

    .line 123
    :cond_6
    const/4 v0, 0x5

    if-lt p1, v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/c;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    goto :goto_0
.end method
