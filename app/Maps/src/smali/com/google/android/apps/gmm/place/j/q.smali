.class public Lcom/google/android/apps/gmm/place/j/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/q;
.implements Lcom/google/android/apps/gmm/place/br;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private b:Z

.field private c:Lcom/google/android/apps/gmm/place/j/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 128
    iput p1, p0, Lcom/google/android/apps/gmm/place/j/q;->d:F

    .line 129
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 42
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_1

    move-object v0, v2

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    .line 43
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 44
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-eqz v0, :cond_0

    move v6, v7

    :cond_0
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/place/j/q;->b:Z

    .line 45
    return-void

    .line 42
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->p:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ajk;->d()Lcom/google/r/b/a/ajk;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/ajk;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/base/i/f;->a(Lcom/google/r/b/a/ajk;Lcom/google/android/apps/gmm/map/h/f;)Lcom/google/r/b/a/aje;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v0, v2

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/place/j/r;->a:[I

    iget v1, v3, Lcom/google/r/b/a/aje;->d:I

    invoke-static {v1}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    :cond_3
    invoke-virtual {v1}, Lcom/google/r/b/a/aiy;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    const-string v4, ""

    :goto_1
    sget-object v1, Lcom/google/android/apps/gmm/place/j/r;->a:[I

    iget v0, v3, Lcom/google/r/b/a/aje;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    :cond_4
    invoke-virtual {v0}, Lcom/google/r/b/a/aiy;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->d:Lcom/google/android/apps/gmm/place/j/x;

    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/v;-><init>(Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/r/b/a/aje;Ljava/lang/String;Lcom/google/android/apps/gmm/place/j/x;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->nk:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->hs:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    if-nez v1, :cond_5

    move v1, v6

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->J()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/j;->D:I

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    iget v1, v1, Lcom/google/r/b/a/ade;->f:I

    goto :goto_3

    :pswitch_3
    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->a:Lcom/google/android/apps/gmm/place/j/x;

    goto :goto_2

    :pswitch_4
    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->b:Lcom/google/android/apps/gmm/place/j/x;

    goto :goto_2

    :pswitch_5
    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->c:Lcom/google/android/apps/gmm/place/j/x;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 67
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    .line 69
    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final b(F)V
    .locals 0

    .prologue
    .line 138
    iput p1, p0, Lcom/google/android/apps/gmm/place/j/q;->e:F

    .line 139
    return-void
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 6

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    if-eqz v0, :cond_1

    .line 88
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    .line 89
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    invoke-virtual {v0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    .line 90
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 92
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x10e0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    move-object v0, v1

    .line 94
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(F)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/google/android/apps/gmm/place/j/q;->f:F

    .line 50
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    if-nez v0, :cond_0

    .line 101
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 103
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/place/j/r;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    :cond_1
    invoke-virtual {v0}, Lcom/google/r/b/a/aiy;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 109
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 107
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->c:Lcom/google/android/apps/gmm/place/j/v;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->a()V

    .line 118
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/Float;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/q;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    const/high16 v0, 0x40a00000    # 5.0f

    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method public final h()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/apps/gmm/place/j/q;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/apps/gmm/place/j/q;->e:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/apps/gmm/place/j/q;->f:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
