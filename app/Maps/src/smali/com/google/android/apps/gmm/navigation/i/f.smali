.class public Lcom/google/android/apps/gmm/navigation/i/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/u/a/b;

.field final b:Lcom/google/android/apps/gmm/shared/net/a/n;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/shared/net/a/n;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/f;->a:Lcom/google/android/apps/gmm/map/u/a/b;

    .line 81
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 82
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/r/a/w;II)Lcom/google/android/apps/gmm/map/r/a/ad;
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 283
    int-to-double v0, p3

    invoke-virtual {p1, p0, v0, v1, v4}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/b/a/y;DZ)[Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v6

    .line 286
    const/4 v3, 0x0

    .line 287
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 288
    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v2, v6, v5

    .line 289
    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    cmpg-double v8, v8, v0

    if-gez v8, :cond_4

    .line 291
    iget-wide v0, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    .line 288
    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto :goto_0

    .line 294
    :cond_0
    if-eqz v3, :cond_1

    .line 297
    iget-wide v0, v3, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v8

    double-to-int v0, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 296
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 298
    const-string v0, "EndpointFinder"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Search radius: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " min: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " max: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    array-length v7, v6

    move v0, v4

    move-object v2, v3

    move v3, v4

    :goto_2
    if-ge v3, v7, :cond_2

    aget-object v1, v6, v3

    .line 304
    iget v4, v1, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    .line 305
    if-lt v4, v0, :cond_3

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    int-to-double v10, v5

    cmpg-double v4, v8, v10

    if-gez v4, :cond_3

    .line 307
    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    .line 303
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_2

    :cond_1
    move-object v2, v3

    .line 311
    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    goto :goto_3

    :cond_4
    move-object v2, v3

    goto :goto_1
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/w;DDDDD)Ljava/util/List;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "DDDDD)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 221
    const-wide/16 v2, 0x0

    sub-double v4, p1, p3

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    .line 222
    add-double v16, p1, p5

    .line 223
    move-wide/from16 v0, p7

    neg-double v4, v0

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 228
    iget-object v2, v13, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v18, v2, 0x3

    .line 229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    invoke-static {v2, v14, v15}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v2

    if-gez v2, :cond_0

    add-int/lit8 v2, v2, 0x2

    neg-int v2, v2

    .line 231
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    move-wide/from16 v0, v16

    invoke-static {v3, v0, v1}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v3

    if-gez v3, :cond_1

    add-int/lit8 v3, v3, 0x2

    neg-int v3, v3

    :cond_1
    add-int/lit8 v6, v18, -0x2

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 232
    add-int/lit8 v3, v19, 0x1

    .line 233
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v6, v6, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v20

    .line 235
    new-instance v22, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct/range {v22 .. v22}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 236
    new-instance v23, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct/range {v23 .. v23}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    move/from16 v24, v2

    move-wide v2, v4

    move/from16 v5, v24

    .line 237
    :goto_0
    move/from16 v0, v19

    if-gt v5, v0, :cond_6

    .line 238
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v10, v4, v5

    .line 239
    cmpl-double v4, v10, v14

    if-ltz v4, :cond_3

    cmpg-double v4, v10, v16

    if-gtz v4, :cond_3

    const/4 v4, 0x1

    move v7, v4

    .line 241
    :goto_1
    sub-double v8, v10, v2

    cmpg-double v4, v8, p7

    if-gez v4, :cond_4

    const/4 v4, 0x1

    .line 244
    :goto_2
    invoke-virtual {v13, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->b(I)F

    move-result v6

    float-to-double v8, v6

    cmpg-double v6, v8, p9

    if-gez v6, :cond_5

    const/4 v6, 0x1

    .line 245
    :goto_3
    if-eqz v7, :cond_2

    if-nez v4, :cond_2

    if-nez v6, :cond_2

    .line 247
    move-object/from16 v0, v22

    invoke-virtual {v13, v5, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 248
    add-int/lit8 v2, v5, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v13, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v2, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v2

    sub-double v2, v2, v20

    .line 255
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v9, v2

    .line 257
    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/g;

    const/high16 v2, 0x3f000000    # 0.5f

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 258
    invoke-virtual {v13, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v13, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v6

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/gmm/navigation/i/g;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;IDZI)V

    .line 257
    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v2, v10

    .line 237
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 239
    :cond_3
    const/4 v4, 0x0

    move v7, v4

    goto :goto_1

    .line 241
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    .line 244
    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    .line 262
    :cond_6
    add-int/lit8 v2, v18, -0x2

    move/from16 v0, v19

    if-ne v0, v2, :cond_7

    .line 263
    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/g;

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->c()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    add-int/lit8 v5, v18, -0x1

    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/gmm/navigation/i/g;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;IDZI)V

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    :cond_7
    return-object v12
.end method

.method static a(ZLcom/google/android/apps/gmm/map/b/a/y;Ljava/util/Iterator;DIIDI)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            ">;DIIDI)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    new-instance v13, Ljava/util/LinkedList;

    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    .line 335
    mul-double v14, p7, p7

    .line 336
    new-instance v16, Lcom/google/android/apps/gmm/navigation/i/h;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/apps/gmm/navigation/i/h;-><init>()V

    .line 337
    :cond_0
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 338
    invoke-interface/range {p2 .. p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/gmm/map/u/a/e;

    .line 339
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/gmm/navigation/i/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/navigation/i/h;)V

    .line 340
    const/16 v2, 0x168

    move/from16 v0, p5

    if-eq v0, v2, :cond_2

    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/i/h;->b:I

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v6, v2, v3}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v2, v2, 0x1

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v6, v2, v4}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    sub-double v2, p3, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_1

    const-wide v4, 0x4076800000000000L    # 360.0

    sub-double v2, v4, v2

    :cond_1
    move/from16 v0, p5

    int-to-double v4, v0

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_0

    .line 342
    :cond_2
    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/i/h;->c:F

    float-to-double v2, v2

    cmpg-double v2, v2, v14

    if-gez v2, :cond_0

    .line 346
    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/e;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/h;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    move-object/from16 v0, v16

    iget v7, v0, Lcom/google/android/apps/gmm/navigation/i/h;->b:I

    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/i/h;->c:F

    float-to-double v8, v2

    .line 348
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    move/from16 v4, p0

    move/from16 v8, p9

    move-object/from16 v9, p1

    move/from16 v12, p6

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/gmm/navigation/i/e;-><init>(ZLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/u/a/e;IILcom/google/android/apps/gmm/map/b/a/y;DI)V

    .line 346
    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 340
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 351
    :cond_4
    return-object v13
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/navigation/i/h;)V
    .locals 7

    .prologue
    .line 388
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 389
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 390
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 391
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p2, Lcom/google/android/apps/gmm/navigation/i/h;->c:F

    .line 392
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v4, v0, 0x3

    .line 393
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v5, v4, -0x1

    if-ge v0, v5, :cond_1

    .line 394
    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 395
    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p1, v5, v2}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 396
    invoke-static {v1, v2, p0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v5

    .line 398
    iget v6, p2, Lcom/google/android/apps/gmm/navigation/i/h;->c:F

    cmpg-float v6, v5, v6

    if-gez v6, :cond_0

    .line 399
    iput v5, p2, Lcom/google/android/apps/gmm/navigation/i/h;->c:F

    .line 400
    iput v0, p2, Lcom/google/android/apps/gmm/navigation/i/h;->b:I

    .line 401
    iget-object v5, p2, Lcom/google/android/apps/gmm/navigation/i/h;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 393
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 404
    :cond_1
    return-void
.end method

.method static a(Ljava/util/List;D)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;D)V"
        }
    .end annotation

    .prologue
    .line 417
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 418
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 419
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/e;

    .line 420
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/i/e;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/i/e;->a:Z

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    const/4 v4, 0x0

    .line 421
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v2, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 422
    :cond_2
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 423
    :cond_3
    iget-wide v2, v0, Lcom/google/android/apps/gmm/navigation/i/e;->g:D

    cmpl-double v0, v2, p1

    if-lez v0, :cond_0

    .line 424
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 427
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;)Ljava/util/Collection;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/b/a;",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 146
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v14

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 148
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->c:I

    int-to-double v4, v3

    mul-double/2addr v4, v14

    .line 149
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 150
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->d:I

    int-to-double v6, v3

    mul-double v16, v14, v6

    .line 151
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 152
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->i:I

    int-to-double v6, v3

    mul-double/2addr v6, v14

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 154
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->j:I

    int-to-double v8, v3

    mul-double/2addr v8, v14

    .line 155
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 156
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->k:I

    int-to-double v10, v3

    mul-double/2addr v10, v14

    .line 157
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 158
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v3, v3, Lcom/google/r/b/a/qz;->l:I

    int-to-double v12, v3

    mul-double/2addr v12, v14

    .line 159
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 160
    double-to-int v3, v4

    move-wide/from16 v0, v16

    double-to-int v4, v0

    move-object/from16 v0, p2

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/navigation/i/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/r/a/w;II)Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v3

    .line 162
    if-eqz v3, :cond_7

    .line 163
    const-string v4, "EndpointFinder"

    const-string v5, "Projection: "

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_1

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v2, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/ad;)D

    move-result-wide v4

    move-object/from16 v3, p2

    .line 165
    invoke-static/range {v3 .. v13}, Lcom/google/android/apps/gmm/navigation/i/f;->a(Lcom/google/android/apps/gmm/map/r/a/w;DDDDD)Ljava/util/List;

    move-result-object v2

    .line 168
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double v10, v14, v4

    .line 170
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/g;

    .line 171
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->a:Lcom/google/android/apps/gmm/map/u/a/b;

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/i/g;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v5, 0x0

    .line 172
    invoke-interface {v3, v4, v10, v11, v5}, Lcom/google/android/apps/gmm/map/u/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/y;DLcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/Iterator;

    move-result-object v5

    .line 173
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/navigation/i/g;->d:Z

    if-eqz v3, :cond_2

    const/16 v8, 0x168

    .line 175
    :goto_2
    const/4 v3, 0x0

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/i/g;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/navigation/i/g;->c:D

    iget v9, v2, Lcom/google/android/apps/gmm/navigation/i/g;->e:I

    iget v12, v2, Lcom/google/android/apps/gmm/navigation/i/g;->b:I

    invoke-static/range {v3 .. v12}, Lcom/google/android/apps/gmm/navigation/i/f;->a(ZLcom/google/android/apps/gmm/map/b/a/y;Ljava/util/Iterator;DIIDI)Ljava/util/List;

    move-result-object v4

    .line 179
    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/i/g;->d:Z

    if-eqz v2, :cond_3

    .line 180
    invoke-static {v4, v10, v11}, Lcom/google/android/apps/gmm/navigation/i/f;->a(Ljava/util/List;D)V

    .line 184
    :cond_0
    :goto_3
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 163
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/f;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 174
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v8, v3, Lcom/google/r/b/a/qz;->f:I

    goto :goto_2

    .line 182
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/e;

    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/navigation/i/e;

    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v2, "EndpointFinder"

    const-string v3, "More than 2 endpoints. Dropping all."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto :goto_3

    :cond_4
    iget-object v6, v2, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget-object v7, v3, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    const/4 v8, 0x0

    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v6, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    const/4 v6, 0x0

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v2, v6, v7}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {v7, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    invoke-interface {v5}, Ljava/util/ListIterator;->remove()V

    goto :goto_3

    :cond_6
    const-string v2, "EndpointFinder"

    const-string v3, "Endpoints don\'t line up. Dropping all."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto/16 :goto_3

    .line 191
    :cond_7
    return-object v18
.end method
