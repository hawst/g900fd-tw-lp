.class public Lcom/google/android/apps/gmm/shared/c/c/l;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:I

.field private static final e:I

.field private static final f:I


# instance fields
.field a:I

.field b:I

.field c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 57
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/shared/c/c/l;->d:I

    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/shared/c/c/l;->e:I

    .line 59
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/shared/c/c/l;->f:I

    return-void
.end method

.method constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    add-int/lit8 v0, p1, 0x3b

    .line 65
    sget v1, Lcom/google/android/apps/gmm/shared/c/c/l;->f:I

    div-int v1, v0, v1

    iput v1, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    .line 66
    sget v1, Lcom/google/android/apps/gmm/shared/c/c/l;->f:I

    rem-int/2addr v0, v1

    .line 67
    sget v1, Lcom/google/android/apps/gmm/shared/c/c/l;->e:I

    div-int v1, v0, v1

    iput v1, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    .line 68
    sget v1, Lcom/google/android/apps/gmm/shared/c/c/l;->e:I

    rem-int/2addr v0, v1

    .line 69
    sget v1, Lcom/google/android/apps/gmm/shared/c/c/l;->d:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->c:I

    .line 73
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    if-lez v0, :cond_1

    .line 74
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->c:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_0

    .line 75
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    .line 76
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    .line 77
    iput v2, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->b:I

    .line 78
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->a:I

    .line 81
    :cond_0
    iput v2, p0, Lcom/google/android/apps/gmm/shared/c/c/l;->c:I

    .line 83
    :cond_1
    return-void
.end method
