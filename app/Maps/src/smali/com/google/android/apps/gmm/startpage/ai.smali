.class Lcom/google/android/apps/gmm/startpage/ai;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/v/cp;

.field b:J

.field final synthetic c:Lcom/google/android/apps/gmm/startpage/ag;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/startpage/ag;Lcom/google/android/apps/gmm/v/cp;)V
    .locals 0

    .prologue
    .line 755
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/ai;->c:Lcom/google/android/apps/gmm/startpage/ag;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 756
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/ai;->a:Lcom/google/android/apps/gmm/v/cp;

    .line 757
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->a:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 762
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 4

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->c:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->c:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->a:Lcom/google/android/apps/gmm/v/cp;

    .line 771
    iget-wide v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->b:J

    const-wide/16 v2, 0x12c

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ai;->c:Lcom/google/android/apps/gmm/startpage/ag;

    .line 772
    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 773
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->c:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->b:J

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->c:Lcom/google/android/apps/gmm/startpage/ag;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/ao;->a:Lcom/google/android/apps/gmm/startpage/ao;

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/startpage/ah;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/startpage/ah;-><init>(Lcom/google/android/apps/gmm/startpage/ag;Lcom/google/android/apps/gmm/startpage/ao;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 777
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ai;->a:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 778
    return-void
.end method
