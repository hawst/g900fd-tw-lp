.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# static fields
.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .prologue
    .line 182
    const-string v1, ""

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget v2, v2, v3

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget v3, v3, v4

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aget v4, v4, v5

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v6, 0x1

    aget-object v5, v5, v6

    const/4 v6, 0x1

    aget v5, v5, v6

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const/4 v8, 0x1

    aget v7, v7, v8

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v9, 0x3

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aget v8, v8, v9

    sget-object v9, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v10, 0x3

    aget-object v9, v9, v10

    const/4 v10, 0x1

    aget v9, v9, v10

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v11, 0x4

    aget-object v10, v10, v11

    const/4 v11, 0x0

    aget v10, v10, v11

    sget-object v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v12, 0x4

    aget-object v11, v11, v12

    const/4 v12, 0x1

    aget v11, v11, v12

    sget-object v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v13, 0x5

    aget-object v12, v12, v13

    const/4 v13, 0x0

    aget v12, v12, v13

    sget-object v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v14, 0x5

    aget-object v13, v13, v14

    const/4 v14, 0x1

    aget v13, v13, v14

    sget-object v14, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v15, 0x6

    aget-object v14, v14, v15

    const/4 v15, 0x0

    aget v14, v14, v15

    sget-object v15, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v16, 0x6

    aget-object v15, v15, v16

    const/16 v16, 0x1

    aget v15, v15, v16

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x5b4

    move/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "attribute vec4 userData0;\n#ifdef DASHES_IN_STYLE_TEXTURE\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "attribute float userData1;\n#endif\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying float strokeIndex;\nvarying float styleIndex;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying vec2 texCoord;\nuniform float uRoadAlpha;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying float vRoadAlpha;\n#ifdef DASHES_IN_STYLE_TEXTURE\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying float vDistanceAlongRoad;\n#endif\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "void main() {\n  float texCoordIndex = aPosition.z;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0.0, 1.0);\n  styleIndex = (256.0 * userData0.x) + userData0.y;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "  strokeIndex = userData0.z;\n  if (strokeIndex < 0.5 && uRoadAlpha < 1.0)\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "    vRoadAlpha = 0.0;\n  else\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "    vRoadAlpha = uRoadAlpha;\n  if (texCoordIndex < 1.0) {\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "    texCoord.x = "

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 2.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    texCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 3.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    texCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 4.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    texCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 5.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    texCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 6.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    texCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 7.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    texCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    texCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  }\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  texCoord -= vec2(0.5);\n#ifdef DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vDistanceAlongRoad = userData1;\n#endif\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->g:Ljava/lang/String;

    .line 235
    const-string v1, "precision mediump float;\n"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    const-string v3, "float convertBase255Vec4ToFloat(vec4 inVec4) {;\n  float result = inVec4[3];\n  result /= 255.0;\n  result += inVec4[2];\n  result /= 255.0;\n  result += inVec4[1];\n  result /= 255.0;\n  result += inVec4[0];\n  return result;\n}\n"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit16 v5, v5, 0x8a4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#define ROAD_EXTRUSION_SCALE 0.5"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n#define LOD_BIAS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-0.25\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform vec2 uTextureInfo;\nvarying float styleIndex;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform vec3 uStrokeInfo;\nvarying float strokeIndex;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform float reciprocalBitmapWidth;\nvarying vec2 texCoord;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform sampler2D sTexture0;\nuniform sampler2D sStyleTexture;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "varying float vRoadAlpha;\nuniform float brightnessScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#ifdef DASHES_IN_STYLE_TEXTURE\nuniform float uDashZoomScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "varying float vDistanceAlongRoad;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\nvoid main() {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float texHeight = uTextureInfo[0];\n  float textureScaleX = uTextureInfo[1];\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float widthVecIndex = strokeIndex;\n  if (strokeIndex > 2.5)\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    widthVecIndex = strokeIndex - 3.0;\n  float widthPixel = 4.0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  if (strokeIndex > 2.5)\n    widthPixel = 5.0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#ifdef DASHES_IN_STYLE_TEXTURE\n  widthPixel += 4.0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#else\n  float colorTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\n  float widthTexX = (widthPixel + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#ifdef DASHES_IN_STYLE_TEXTURE\n  float dashScaleTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\n#ifdef DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float row = styleIndex * 5.0;\n#else\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float row = styleIndex;\n#endif\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float texY = (row + 0.5) / texHeight;\n  vec4 roadWidthVec = texture2D(sStyleTexture, vec2(widthTexX, texY));\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#ifdef DASHES_IN_STYLE_TEXTURE\n  vec4 dashScaleVec = texture2D(sStyleTexture, vec2(dashScaleTexX, texY));\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float dashScale = convertBase255Vec4ToFloat(dashScaleVec);\n  dashScale *= uDashZoomScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vec2 dashTextureCoord = vec2(vDistanceAlongRoad * dashScale,\n      (row + strokeIndex + 1.5) / texHeight);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\n  float roadScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  if (widthVecIndex < 0.5)\n    roadScale = roadWidthVec[0];\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else if (widthVecIndex < 1.5)\n    roadScale = roadWidthVec[1];\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else if (widthVecIndex < 2.5)\n    roadScale = roadWidthVec[2];\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else\n    roadScale = 0.0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  roadScale /= textureScaleX * ROAD_EXTRUSION_SCALE;\n  vec2 texCoordTmp = (texCoord / roadScale) + vec2(0.5);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#ifdef DASHES_IN_STYLE_TEXTURE\n  vec4 vColor = texture2D(sStyleTexture, dashTextureCoord);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#else\n  vec4 vColor = texture2D(sStyleTexture, vec2(colorTexX, texY));\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\n  float t = texture2D(sTexture0, texCoordTmp, LOD_BIAS).a;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  t *= vRoadAlpha * vColor.a;\n  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 325
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->g:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 330
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 331
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->a:I

    .line 332
    const-string v0, "LegacyRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->a:I

    if-ne v0, v2, :cond_0

    .line 334
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->a:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 340
    const-string v0, "sStyleTexture"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->b:I

    .line 345
    const-string v0, "LegacyRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->b:I

    if-ne v0, v2, :cond_1

    .line 347
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sStyleTexture handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->b:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 353
    const-string v0, "uTextureInfo"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->c:I

    .line 358
    const-string v0, "LegacyRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->c:I

    if-ne v0, v2, :cond_2

    .line 360
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uTextureInfo handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 363
    :cond_2
    const-string v0, "uRoadAlpha"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->d:I

    .line 364
    const-string v0, "LegacyRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->d:I

    if-ne v0, v2, :cond_3

    .line 366
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uRoadAlpha handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369
    :cond_3
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->e:I

    .line 370
    const-string v0, "LegacyRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->e:I

    if-ne v0, v2, :cond_4

    .line 372
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get brightnessScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_4
    const-string v0, "reciprocalBitmapWidth"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->f:I

    .line 376
    const-string v0, "LegacyRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->f:I

    if-ne v0, v2, :cond_5

    .line 378
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get reciprocalBitmapWidth handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_5
    return-void
.end method

.method protected final b(I)V
    .locals 2

    .prologue
    .line 392
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->b(I)V

    .line 393
    const/4 v0, 0x5

    const-string v1, "userData0"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 394
    return-void
.end method
