.class public Lcom/google/android/apps/gmm/shared/net/a/s;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/maps/g/aw;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 19
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 20
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    :cond_0
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    move v0, v2

    :goto_2
    if-eqz v0, :cond_1

    .line 24
    iget-object v0, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    :goto_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 26
    :cond_1
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a

    move v0, v2

    :goto_4
    if-eqz v0, :cond_2

    .line 27
    iget-object v0, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    :goto_5
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 29
    :cond_2
    iget v0, p0, Lcom/google/maps/g/aw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_d

    move v0, v2

    :goto_6
    if-eqz v0, :cond_3

    .line 30
    iget v0, p0, Lcom/google/maps/g/aw;->e:I

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 32
    :cond_3
    return-object v4

    :cond_4
    move v0, v3

    .line 20
    goto :goto_0

    .line 21
    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v1, p0, Lcom/google/maps/g/aw;->b:Ljava/lang/Object;

    :cond_6
    move-object v0, v1

    goto :goto_1

    :cond_7
    move v0, v3

    .line 23
    goto :goto_2

    .line 24
    :cond_8
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    iput-object v1, p0, Lcom/google/maps/g/aw;->c:Ljava/lang/Object;

    :cond_9
    move-object v0, v1

    goto :goto_3

    :cond_a
    move v0, v3

    .line 26
    goto :goto_4

    .line 27
    :cond_b
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    iput-object v1, p0, Lcom/google/maps/g/aw;->d:Ljava/lang/Object;

    :cond_c
    move-object v0, v1

    goto :goto_5

    :cond_d
    move v0, v3

    .line 29
    goto :goto_6
.end method
