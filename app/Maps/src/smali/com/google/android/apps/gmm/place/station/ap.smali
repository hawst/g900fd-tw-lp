.class public final enum Lcom/google/android/apps/gmm/place/station/ap;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/place/station/ap;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/place/station/ap;

.field public static final enum b:Lcom/google/android/apps/gmm/place/station/ap;

.field public static final enum c:Lcom/google/android/apps/gmm/place/station/ap;

.field public static final enum d:Lcom/google/android/apps/gmm/place/station/ap;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/place/station/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ap;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/place/station/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ap;->a:Lcom/google/android/apps/gmm/place/station/ap;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ap;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/place/station/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ap;->b:Lcom/google/android/apps/gmm/place/station/ap;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ap;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/place/station/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ap;->c:Lcom/google/android/apps/gmm/place/station/ap;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ap;

    const-string v1, "LATER"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/place/station/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ap;->d:Lcom/google/android/apps/gmm/place/station/ap;

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/place/station/ap;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ap;->a:Lcom/google/android/apps/gmm/place/station/ap;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ap;->b:Lcom/google/android/apps/gmm/place/station/ap;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ap;->c:Lcom/google/android/apps/gmm/place/station/ap;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ap;->d:Lcom/google/android/apps/gmm/place/station/ap;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ap;->e:[Lcom/google/android/apps/gmm/place/station/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/place/station/ap;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/place/station/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/ap;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/place/station/ap;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/gmm/place/station/ap;->e:[Lcom/google/android/apps/gmm/place/station/ap;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/place/station/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/place/station/ap;

    return-object v0
.end method
