.class public Lcom/google/android/apps/gmm/place/c/x;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/ae;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/place/ae;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;Z)V
    .locals 6

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/place/c/a;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/gmm/place/c/a;-><init>(Lcom/google/android/apps/gmm/place/ae;Lcom/google/android/libraries/curvular/bc;Landroid/content/Context;)V

    .line 60
    if-eqz p3, :cond_0

    .line 61
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->t()Lcom/google/android/apps/gmm/place/i/b;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/f;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->t()Lcom/google/android/apps/gmm/place/i/b;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 64
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/gmm/place/ae;->G()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->f()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->b()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v2

    .line 69
    iget-object v0, v2, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/b;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    iget-object v1, v2, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/station/b/b;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    :cond_1
    iget-object v4, v2, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v5, Lcom/google/android/apps/gmm/place/station/q;

    iget-object v0, v2, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/b;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {v4, v5, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 71
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/station/b/b;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/place/station/f;->a(Lcom/google/android/apps/gmm/place/station/b/b;Lcom/google/android/libraries/curvular/bc;)V

    .line 72
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v1

    .line 73
    iget-object v0, v1, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/e;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/place/station/n;->a(Lcom/google/android/apps/gmm/place/station/b/e;Lcom/google/android/libraries/curvular/bc;)V

    goto :goto_1

    .line 74
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/av;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->p()Lcom/google/android/apps/gmm/base/l/v;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 76
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 77
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/station/b/b;->g()Lcom/google/android/apps/gmm/directions/h/a;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->s()Lcom/google/android/apps/gmm/place/station/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/station/b/b;->g()Lcom/google/android/apps/gmm/directions/h/a;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v3, Lcom/google/android/apps/gmm/place/station/t;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/h/a;->b()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    .line 78
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->h()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    .line 84
    :goto_2
    return-void

    .line 82
    :cond_8
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_2
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 30
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 31
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v8

    .line 32
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v9

    .line 34
    invoke-static {}, Lcom/google/android/apps/gmm/place/c/c;->a()Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v10

    const/4 v2, 0x3

    const-class v3, Lcom/google/android/apps/gmm/base/f/aq;

    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->C()Lcom/google/android/apps/gmm/base/l/a/o;

    move-result-object v0

    new-instance v4, Lcom/google/android/libraries/curvular/ao;

    const/4 v5, 0x0

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-direct {v4, v3, v0}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v4, v1, v2

    const/4 v0, 0x4

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/x;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Lcom/google/android/libraries/curvular/cu;

    sget-object v4, Lcom/google/android/apps/gmm/place/t;->a:Lcom/google/android/libraries/curvular/bk;

    .line 40
    sget-object v5, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v8

    .line 42
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v9

    .line 43
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x3

    sget v5, Lcom/google/android/apps/gmm/d;->aR:I

    .line 45
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v3, v4

    .line 37
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/base/k/ao;->a(Lcom/google/android/libraries/curvular/am;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v1, v0

    .line 30
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.FrameLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lcom/google/android/apps/gmm/place/ae;

    const/4 v0, 0x1

    invoke-static {p2, p3, p4, v0}, Lcom/google/android/apps/gmm/place/c/x;->a(Lcom/google/android/apps/gmm/place/ae;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;Z)V

    return-void
.end method
