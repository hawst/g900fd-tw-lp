.class public Lcom/google/android/apps/gmm/streetview/b/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/streetview/b/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:F

.field public b:F

.field public c:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/b/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/b/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 28
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 36
    iput p1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 37
    iput p2, p0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    .line 38
    const/high16 v0, 0x41700000    # 15.0f

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-static {v0, p3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/high16 v0, 0x42700000    # 60.0f

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 42
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 43
    return-void
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 131
    invoke-static {p1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 99
    instance-of v1, p1, Lcom/google/android/apps/gmm/streetview/b/a;

    if-eqz v1, :cond_0

    .line 100
    check-cast p1, Lcom/google/android/apps/gmm/streetview/b/a;

    .line 101
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 103
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UserOrientation["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    .line 92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 114
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 115
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 116
    return-void
.end method
