.class public Lcom/google/android/apps/gmm/car/ad;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/car/m/f;

.field public final b:Lcom/google/android/apps/gmm/car/m/m;

.field public final c:Lcom/google/android/apps/gmm/car/drawer/j;

.field public final d:Landroid/view/LayoutInflater;

.field public final e:Lcom/google/android/libraries/curvular/bd;

.field public final f:Lcom/google/android/apps/gmm/base/a;

.field public final g:Lcom/google/android/apps/gmm/map/t;

.field public final h:Lcom/google/android/apps/gmm/car/at;

.field public final i:Lcom/google/android/apps/gmm/mylocation/b/f;

.field public final j:Lcom/google/android/apps/gmm/directions/a/a;

.field public final k:Lcom/google/android/apps/gmm/search/ah;

.field public final l:Lcom/google/android/apps/gmm/car/bg;

.field public final m:Lcom/google/android/apps/gmm/o/a/c;

.field public final n:Lcom/google/android/apps/gmm/car/e/m;

.field public final o:Z

.field public final p:Lcom/google/android/apps/gmm/car/c;

.field public final q:Lcom/google/android/apps/gmm/car/ao;

.field public final r:Lcom/google/android/apps/gmm/car/k/h;

.field public final s:Lcom/google/android/apps/gmm/car/d/a;

.field public final t:Lcom/google/android/apps/gmm/car/v;

.field public final u:Lcom/google/android/apps/gmm/car/q;

.field public v:Lcom/google/android/apps/gmm/car/c/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public w:Lcom/google/android/apps/gmm/car/c/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public x:Lcom/google/android/apps/gmm/car/c/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public y:Lcom/google/android/apps/gmm/car/c/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public z:Lcom/google/android/apps/gmm/car/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/m/f;Lcom/google/android/apps/gmm/car/m/m;Lcom/google/android/apps/gmm/car/drawer/j;Landroid/view/LayoutInflater;Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/car/at;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/directions/a/a;Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/car/bg;Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/car/e/m;ZLcom/google/android/apps/gmm/car/c;Lcom/google/android/apps/gmm/car/ao;Lcom/google/android/apps/gmm/car/k/h;Lcom/google/android/apps/gmm/car/d/a;)V
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/m/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    .line 106
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/m/m;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    .line 107
    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/drawer/j;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    .line 108
    if-nez p4, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_3
    move-object v1, p4

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    .line 109
    if-nez p5, :cond_4

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_4
    move-object v1, p5

    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    .line 110
    if-nez p6, :cond_5

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_5
    check-cast p6, Lcom/google/android/apps/gmm/base/a;

    iput-object p6, p0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 111
    if-nez p7, :cond_6

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_6
    check-cast p7, Lcom/google/android/apps/gmm/map/t;

    iput-object p7, p0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    .line 112
    if-nez p8, :cond_7

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_7
    check-cast p8, Lcom/google/android/apps/gmm/car/at;

    iput-object p8, p0, Lcom/google/android/apps/gmm/car/ad;->h:Lcom/google/android/apps/gmm/car/at;

    .line 113
    if-nez p9, :cond_8

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_8
    check-cast p9, Lcom/google/android/apps/gmm/mylocation/b/f;

    iput-object p9, p0, Lcom/google/android/apps/gmm/car/ad;->i:Lcom/google/android/apps/gmm/mylocation/b/f;

    .line 114
    if-nez p10, :cond_9

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_9
    check-cast p10, Lcom/google/android/apps/gmm/directions/a/a;

    iput-object p10, p0, Lcom/google/android/apps/gmm/car/ad;->j:Lcom/google/android/apps/gmm/directions/a/a;

    .line 115
    if-nez p11, :cond_a

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_a
    check-cast p11, Lcom/google/android/apps/gmm/search/ah;

    iput-object p11, p0, Lcom/google/android/apps/gmm/car/ad;->k:Lcom/google/android/apps/gmm/search/ah;

    .line 116
    if-nez p12, :cond_b

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_b
    check-cast p12, Lcom/google/android/apps/gmm/car/bg;

    iput-object p12, p0, Lcom/google/android/apps/gmm/car/ad;->l:Lcom/google/android/apps/gmm/car/bg;

    .line 117
    if-nez p13, :cond_c

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_c
    check-cast p13, Lcom/google/android/apps/gmm/o/a/c;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ad;->m:Lcom/google/android/apps/gmm/o/a/c;

    .line 118
    if-nez p14, :cond_d

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_d
    check-cast p14, Lcom/google/android/apps/gmm/car/e/m;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ad;->n:Lcom/google/android/apps/gmm/car/e/m;

    .line 119
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/ad;->o:Z

    .line 120
    if-nez p16, :cond_e

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_e
    check-cast p16, Lcom/google/android/apps/gmm/car/c;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ad;->p:Lcom/google/android/apps/gmm/car/c;

    .line 121
    if-nez p17, :cond_f

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_f
    check-cast p17, Lcom/google/android/apps/gmm/car/ao;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    .line 122
    if-nez p18, :cond_10

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_10
    check-cast p18, Lcom/google/android/apps/gmm/car/k/h;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ad;->r:Lcom/google/android/apps/gmm/car/k/h;

    .line 123
    if-nez p19, :cond_11

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_11
    move-object/from16 v1, p19

    check-cast v1, Lcom/google/android/apps/gmm/car/d/a;

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    .line 124
    new-instance v1, Lcom/google/android/apps/gmm/car/v;

    invoke-virtual {p4}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/v;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    .line 125
    new-instance v1, Lcom/google/android/apps/gmm/car/q;

    move-object/from16 v0, p19

    invoke-direct {v1, p5, v0}, Lcom/google/android/apps/gmm/car/q;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/car/d/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/ad;->u:Lcom/google/android/apps/gmm/car/q;

    .line 126
    return-void
.end method
