.class public Lcom/google/android/apps/gmm/map/internal/c/bp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/bp;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Lcom/google/android/apps/gmm/map/internal/c/cd;

.field public e:I

.field public f:I

.field public g:I

.field private h:Lcom/google/android/apps/gmm/map/internal/c/bp;


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 74
    return-void
.end method

.method public constructor <init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 67
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(III)V

    .line 68
    if-nez p4, :cond_0

    new-instance p4, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {p4}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    :cond_0
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 70
    return-void
.end method

.method private static a(IIILcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 4

    .prologue
    const/high16 v3, 0x20000000

    .line 119
    if-ltz p0, :cond_0

    const/16 v0, 0x1e

    if-gt p0, v0, :cond_0

    const/high16 v0, -0x20000000

    if-le p2, v0, :cond_0

    if-le p2, v3, :cond_1

    .line 122
    :cond_0
    const/4 p3, 0x0

    .line 138
    :goto_0
    return-object p3

    .line 125
    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    .line 126
    add-int v0, p1, v3

    shr-int/2addr v0, v1

    .line 128
    neg-int v2, p2

    add-int/2addr v2, v3

    shr-int v1, v2, v1

    .line 131
    const/4 v2, 0x1

    shl-int/2addr v2, p0

    .line 132
    if-gez v0, :cond_3

    .line 133
    add-int/2addr v0, v2

    .line 137
    :cond_2
    :goto_1
    invoke-virtual {p3, p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(III)V

    goto :goto_0

    .line 134
    :cond_3
    if-lt v0, v2, :cond_2

    .line 135
    sub-int/2addr v0, v2

    goto :goto_1
.end method

.method public static a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 5

    .prologue
    const/high16 v4, 0x20000000

    const/16 v1, 0x1e

    const/4 v0, 0x0

    .line 183
    if-gtz p0, :cond_0

    .line 185
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v1, v0, v0, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    move-object v0, v1

    .line 209
    :goto_0
    return-object v0

    .line 186
    :cond_0
    if-le p0, v1, :cond_1

    move p0, v1

    .line 189
    :cond_1
    rsub-int/lit8 v1, p0, 0x1e

    .line 190
    add-int v2, p1, v4

    shr-int/2addr v2, v1

    .line 192
    neg-int v3, p2

    add-int/2addr v3, v4

    shr-int v1, v3, v1

    .line 194
    const/4 v3, 0x1

    shl-int/2addr v3, p0

    .line 197
    if-gez v2, :cond_3

    .line 198
    add-int/2addr v2, v3

    .line 204
    :cond_2
    :goto_1
    if-gez v1, :cond_4

    .line 209
    :goto_2
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v1, p0, v2, v0, p3}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    move-object v0, v1

    goto :goto_0

    .line 199
    :cond_3
    if-lt v2, v3, :cond_2

    .line 200
    sub-int/2addr v2, v3

    goto :goto_1

    .line 206
    :cond_4
    if-lt v1, v3, :cond_5

    .line 207
    add-int/lit8 v0, v3, -0x1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static a(ILcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v2, v3, v3, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 2

    .prologue
    .line 168
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {p0, v0, v1, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 4

    .prologue
    .line 423
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    .line 424
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 425
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 426
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    return-object v3
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/bc;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/bc;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 431
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 432
    invoke-static {p0, p1, v1, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V

    .line 434
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V
    .locals 9
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/bc;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/bc;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/cd;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/bc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 452
    if-gez p1, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 459
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 457
    invoke-static {p1, v0, v1, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    .line 461
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/lit8 v0, v0, -0x1

    .line 462
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/lit8 v1, v1, 0x1

    .line 460
    invoke-static {p1, v0, v1, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v4

    .line 463
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    .line 464
    iget v1, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    .line 465
    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    .line 466
    iget v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    .line 468
    const/4 v0, 0x1

    shl-int v7, v0, p1

    .line 470
    if-le v3, v5, :cond_3

    .line 471
    sub-int v0, v7, v3

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    sub-int v8, v6, v1

    add-int/lit8 v8, v8, 0x1

    mul-int/2addr v0, v8

    .line 477
    :goto_1
    if-ltz v0, :cond_0

    .line 481
    const/4 v8, 0x2

    if-gt v0, v8, :cond_4

    .line 482
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 483
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 484
    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    :cond_2
    if-eqz p4, :cond_0

    .line 518
    rsub-int/lit8 v0, p1, 0x1e

    .line 519
    const/high16 v2, 0x40000000    # 2.0f

    shr-int/2addr v2, p1

    .line 520
    shl-int/2addr v3, v0

    const/high16 v4, 0x20000000

    sub-int/2addr v3, v4

    shl-int v4, v6, v0

    neg-int v4, v4

    const/high16 v6, 0x20000000

    add-int/2addr v4, v6

    sub-int/2addr v4, v2

    shl-int/2addr v5, v0

    const/high16 v6, 0x20000000

    sub-int/2addr v5, v6

    add-int/2addr v2, v5

    shl-int v0, v1, v0

    neg-int v0, v0

    const/high16 v1, 0x20000000

    add-int/2addr v0, v1

    iget-object v1, p4, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v1, v3, v4, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(IIII)V

    iget-object v0, p4, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {p4, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    goto :goto_0

    .line 473
    :cond_3
    sub-int v0, v5, v3

    add-int/lit8 v0, v0, 0x1

    sub-int v8, v6, v1

    add-int/lit8 v8, v8, 0x1

    mul-int/2addr v0, v8

    goto :goto_1

    .line 486
    :cond_4
    if-le v3, v5, :cond_8

    move v2, v3

    .line 487
    :goto_2
    if-ge v2, v7, :cond_6

    move v0, v1

    .line 488
    :goto_3
    if-gt v0, v6, :cond_5

    .line 489
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v4, p1, v2, v0, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 487
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 492
    :cond_6
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-gt v2, v5, :cond_2

    move v0, v1

    .line 493
    :goto_5
    if-gt v0, v6, :cond_7

    .line 494
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v4, p1, v2, v0, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 493
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 492
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 497
    :cond_8
    if-ne v3, v5, :cond_b

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v2

    const/high16 v2, 0x20000000

    if-gt v0, v2, :cond_9

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v0, v2, v0

    const/high16 v2, 0x20000000

    if-le v0, v2, :cond_b

    .line 503
    :cond_9
    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v7, :cond_2

    move v0, v1

    .line 504
    :goto_7
    if-gt v0, v6, :cond_a

    .line 505
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v4, p1, v2, v0, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 504
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 503
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_b
    move v2, v3

    .line 509
    :goto_8
    if-gt v2, v5, :cond_2

    move v0, v1

    .line 510
    :goto_9
    if-gt v0, v6, :cond_c

    .line 511
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v4, p1, v2, v0, p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 509
    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 705
    if-ltz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 706
    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    shr-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 5

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 4

    .prologue
    .line 326
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int/2addr v0, p1

    .line 327
    if-gtz v0, :cond_0

    .line 330
    :goto_0
    return-object p0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    shr-int/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    shr-int/2addr v2, v0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 5

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    return-object v1
.end method

.method public final a(III)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x20000000

    .line 88
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 89
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    .line 90
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    .line 92
    rsub-int/lit8 v0, p1, 0x12

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 93
    const/high16 v0, 0x40000000    # 2.0f

    shr-int/2addr v0, p1

    .line 94
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    mul-int/2addr v1, v0

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    .line 95
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    sub-int/2addr v0, v2

    neg-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    .line 97
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    if-eqz v0, :cond_1

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v2

    .line 102
    :cond_1
    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ILcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 3

    .prologue
    .line 340
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int v1, v0, p1

    .line 341
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 345
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    shr-int/2addr v0, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    shr-int v1, v2, v1

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(III)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iput-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 347
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 691
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v2, v2, v0

    invoke-interface {v2, p2}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a(Lcom/google/e/a/a/a/b;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 692
    :cond_1
    return-void
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 6

    .prologue
    .line 299
    const/high16 v0, 0x40000000    # 2.0f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int/2addr v0, v1

    .line 300
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v4, v0

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v0, v5

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/cd;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 589
    if-ne p0, p1, :cond_1

    .line 590
    const/4 v0, 0x1

    .line 607
    :cond_0
    :goto_0
    return v0

    .line 592
    :cond_1
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eqz v1, :cond_0

    .line 596
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 598
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-ne v1, v2, :cond_0

    .line 601
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-ne v1, v2, :cond_0

    .line 604
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-ne v1, v2, :cond_0

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 612
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 613
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    add-int/2addr v0, v1

    .line 614
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    add-int/2addr v0, v1

    .line 615
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 616
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 618
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 627
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 628
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    .line 629
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
