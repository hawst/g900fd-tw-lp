.class public Lcom/google/android/apps/gmm/map/internal/c/az;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# static fields
.field private static final p:[Lcom/google/android/apps/gmm/map/internal/c/z;

.field private static final q:[I


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public final b:[Lcom/google/android/apps/gmm/map/internal/c/z;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/google/android/apps/gmm/map/internal/c/bi;

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:Z

.field public final j:[I

.field private final k:J

.field private final l:I

.field private final m:I

.field private final n:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    new-array v0, v1, [Lcom/google/android/apps/gmm/map/internal/c/z;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/az;->p:[Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 144
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/az;->q:[I

    return-void
.end method

.method public constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bi;IIIIIIZ[I)V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->k:J

    .line 164
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->l:I

    .line 165
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->m:I

    .line 166
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->n:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 167
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 168
    if-nez p7, :cond_0

    sget-object p7, Lcom/google/android/apps/gmm/map/internal/c/az;->p:[Lcom/google/android/apps/gmm/map/internal/c/z;

    :cond_0
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 169
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    .line 170
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 171
    iput p11, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    .line 173
    iput p12, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->o:I

    .line 174
    iput p13, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    .line 175
    iput p14, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->g:I

    .line 176
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->h:I

    .line 177
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->i:Z

    .line 178
    if-nez p17, :cond_1

    sget-object p17, Lcom/google/android/apps/gmm/map/internal/c/az;->q:[I

    :cond_1
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->j:[I

    .line 179
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/ay;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    const/4 v4, 0x1

    .line 200
    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v6, 0xc

    if-lt v5, v6, :cond_0

    .line 201
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 203
    :cond_0
    new-array v0, v4, [Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v24, v0

    .line 204
    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_1

    .line 205
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v6

    aput-object v6, v24, v5

    .line 204
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 209
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v6

    new-instance v25, Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-object/from16 v0, v25

    invoke-direct {v0, v6, v5}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 212
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 213
    const/4 v12, 0x0

    .line 214
    if-lez v6, :cond_2

    .line 215
    new-array v12, v6, [Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 216
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_2

    .line 217
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v7

    aput-object v7, v12, v5

    .line 216
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 222
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v16

    .line 225
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v17

    .line 228
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v6

    .line 231
    const/4 v10, 0x0

    .line 232
    and-int/lit8 v5, v6, 0x1

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_5

    .line 233
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v10

    .line 239
    :cond_3
    :goto_3
    ushr-int/lit8 v18, v6, 0x2

    .line 242
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 243
    const/16 v22, 0x0

    .line 244
    if-lez v6, :cond_7

    .line 245
    new-array v0, v6, [I

    move-object/from16 v22, v0

    .line 246
    const/4 v5, 0x0

    :goto_4
    if-ge v5, v6, :cond_7

    .line 247
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v7

    aput v7, v22, v5

    .line 246
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 232
    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    .line 234
    :cond_5
    and-int/lit8 v5, v6, 0x2

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_5
    if-eqz v5, :cond_3

    .line 235
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->e()Lcom/google/android/apps/gmm/map/b/a/k;

    move-result-object v10

    goto :goto_3

    .line 234
    :cond_6
    const/4 v5, 0x0

    goto :goto_5

    .line 251
    :cond_7
    const/4 v5, 0x0

    move/from16 v23, v5

    :goto_6
    move/from16 v0, v23

    if-ge v0, v4, :cond_8

    .line 252
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 253
    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v6, v6

    .line 254
    move-object/from16 v0, p2

    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 255
    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    aget-object v11, v24, v23

    const/4 v13, 0x0

    .line 260
    move-object/from16 v0, v25

    iget-object v14, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 261
    move-object/from16 v0, v25

    iget v15, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1

    invoke-direct/range {v5 .. v22}, Lcom/google/android/apps/gmm/map/internal/c/az;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bi;IIIIIIZ[I)V

    .line 269
    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/map/internal/c/bi;->a(Z)Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 270
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 251
    add-int/lit8 v5, v23, 0x1

    move/from16 v23, v5

    goto :goto_6

    .line 272
    :cond_8
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/ba;Lcom/google/android/apps/gmm/map/internal/c/bs;)[Lcom/google/android/apps/gmm/map/internal/c/az;
    .locals 23

    .prologue
    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ba;->a()Lcom/google/maps/b/a/cu;

    move-result-object v2

    .line 277
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/ba;->a:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/b/a/ba;->b:Lcom/google/maps/b/a/cy;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 276
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Lcom/google/maps/b/a/cu;ILcom/google/maps/b/a/cy;Lcom/google/android/apps/gmm/map/b/a/aw;)[Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v21

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ba;->f:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v12

    .line 279
    move-object/from16 v0, v21

    array-length v2, v0

    new-array v0, v2, [Lcom/google/android/apps/gmm/map/internal/c/az;

    move-object/from16 v22, v0

    .line 280
    const/4 v14, 0x0

    .line 281
    const/4 v11, 0x0

    .line 282
    const/4 v2, 0x0

    .line 283
    const/16 v19, 0x0

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/ba;->i:Lcom/google/maps/b/a/cr;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v3, :cond_3

    .line 285
    const/16 v19, 0x1

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ba;->b()Lcom/google/maps/b/a/r;

    move-result-object v3

    .line 287
    invoke-virtual {v3}, Lcom/google/maps/b/a/r;->a()Ljava/lang/String;

    move-result-object v11

    .line 288
    iget-object v4, v3, Lcom/google/maps/b/a/r;->a:Lcom/google/maps/b/a/cz;

    iget v14, v4, Lcom/google/maps/b/a/cz;->b:I

    .line 289
    iget-object v3, v3, Lcom/google/maps/b/a/r;->b:Lcom/google/maps/b/a/cz;

    iget v4, v3, Lcom/google/maps/b/a/cz;->b:I

    .line 290
    and-int/lit8 v3, v4, 0x1

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_0

    .line 292
    const/4 v2, 0x1

    .line 294
    :cond_0
    and-int/lit8 v3, v4, 0x2

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_1

    .line 296
    or-int/lit8 v2, v2, 0x2

    .line 298
    :cond_1
    and-int/lit8 v3, v4, 0x4

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_2

    .line 300
    or-int/lit8 v2, v2, 0x4

    .line 302
    :cond_2
    and-int/lit8 v3, v4, 0x8

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_3

    .line 304
    or-int/lit8 v2, v2, 0x10

    .line 307
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/ba;->e:Lcom/google/maps/b/a/cz;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v3, :cond_a

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/ba;->e:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    .line 308
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_a

    .line 310
    or-int/lit8 v2, v2, 0x40

    move/from16 v16, v2

    .line 316
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ba;->h:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v3, -0x80000000

    xor-int v7, v2, v3

    .line 318
    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, v21

    array-length v3, v0

    if-ge v2, v3, :cond_9

    .line 319
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/az;

    const-wide/16 v4, 0x0

    .line 321
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/b/a/ba;->g:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    const/4 v8, 0x0

    aget-object v9, v21, v2

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/c/az;->p:[Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v13, 0x0

    const/4 v15, 0x0

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/b/a/ba;->c:Lcom/google/maps/b/a/cz;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    move/from16 v17, v0

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/b/a/ba;->d:Lcom/google/maps/b/a/cz;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    move/from16 v18, v0

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v20, v0

    invoke-direct/range {v3 .. v20}, Lcom/google/android/apps/gmm/map/internal/c/az;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bi;IIIIIIZ[I)V

    aput-object v3, v22, v2

    .line 336
    aget-object v3, v22, v2

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/bi;->a(Z)Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 318
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 290
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 294
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 298
    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    .line 302
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 308
    :cond_8
    const/4 v3, 0x0

    goto :goto_4

    .line 338
    :cond_9
    return-object v22

    :cond_a
    move/from16 v16, v2

    goto :goto_5
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 343
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->k:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_0
    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->l:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 358
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->m:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->n:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 477
    const/4 v0, 0x2

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->o:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->j:[I

    return-object v0
.end method

.method public final k()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    add-int/lit16 v0, v0, 0xa0

    add-int/lit8 v4, v0, 0x3c

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    if-eqz v0, :cond_0

    .line 599
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v6, v5

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v6, :cond_1

    aget-object v3, v5, v2

    .line 600
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/z;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 599
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    .line 603
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->n:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 604
    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 605
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/c/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 606
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    .line 607
    if-nez v2, :cond_4

    :goto_3
    add-int/2addr v0, v1

    add-int/2addr v0, v4

    .line 609
    return v0

    .line 604
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->d()I

    move-result v2

    goto :goto_1

    .line 606
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0x2

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x28

    goto :goto_2

    .line 607
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_3
.end method

.method public final l()Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->d:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v3, v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v0, :cond_3

    move v0, v1

    .line 526
    :goto_1
    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v3, 0x20

    and-int/2addr v0, v3

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v0, v4

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_a

    .line 527
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_7

    move v0, v1

    :goto_5
    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/4 v3, 0x2

    and-int/2addr v0, v3

    if-eqz v0, :cond_8

    move v0, v1

    :goto_6
    if-eqz v0, :cond_a

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    const/16 v3, 0x80

    if-ge v0, v3, :cond_a

    .line 529
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    and-int/2addr v0, v4

    if-eqz v0, :cond_9

    move v0, v1

    :goto_7
    if-nez v0, :cond_a

    move v0, v1

    :goto_8
    return v0

    .line 525
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v0, v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    .line 526
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v0, v2

    .line 527
    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v0, v2

    .line 529
    goto :goto_7

    :cond_a
    move v0, v2

    goto :goto_8
.end method

.method public final m()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 537
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v3, 0x20

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v3, 0x8

    .line 538
    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 537
    goto :goto_0

    :cond_1
    move v2, v1

    .line 538
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 388
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 389
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-ge v0, v2, :cond_4

    .line 390
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 391
    const-string v2, ", "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    if-nez v2, :cond_2

    move v2, v1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v2, v2

    goto :goto_1

    .line 393
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    goto :goto_2

    .line 396
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v1

    .line 397
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    if-ge v2, v0, :cond_a

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v6, :cond_8

    move-object v0, v3

    .line 399
    :goto_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_6

    .line 400
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_5

    .line 401
    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    :cond_5
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v0, v0

    goto :goto_4

    .line 398
    :cond_8
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_5

    :cond_9
    move-object v0, v3

    goto :goto_5

    .line 407
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 408
    const-string v1, "Road{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_b

    .line 410
    const-string v1, "names = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 412
    :cond_b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_d

    .line 413
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_c

    .line 414
    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    :cond_c
    const-string v1, "iconUrls = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 418
    :cond_d
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
