.class public Lcom/google/android/apps/gmm/map/o/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/o/as;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/o/d;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/m;

.field final c:Lcom/google/android/apps/gmm/map/o/ak;

.field public final d:I

.field public final e:Z

.field private final f:Lcom/google/android/apps/gmm/map/o/af;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Z)V
    .locals 7
    .param p2    # Lcom/google/android/apps/gmm/map/o/ak;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 54
    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/o/af;->a(Lcom/google/android/apps/gmm/map/internal/c/m;)Lcom/google/android/apps/gmm/map/o/af;

    move-result-object v4

    .line 55
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->i()I

    move-result v5

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v6, p3

    .line 54
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/o/d;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/af;IZ)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 7

    .prologue
    .line 42
    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->b()Lcom/google/android/apps/gmm/map/o/ak;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->e()Lcom/google/android/apps/gmm/map/o/af;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/o/d;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/af;IZ)V

    .line 44
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/o/d;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/af;IZ)V
    .locals 6
    .param p1    # Lcom/google/android/apps/gmm/map/o/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/internal/c/m;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/o/ak;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-ne v2, v0, :cond_2

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "label: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " feature: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    .line 62
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 67
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/as;->a:Lcom/google/android/apps/gmm/map/o/d;

    .line 68
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 69
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/as;->c:Lcom/google/android/apps/gmm/map/o/ak;

    .line 70
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    .line 71
    iput p5, p0, Lcom/google/android/apps/gmm/map/o/as;->d:I

    .line 72
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/map/o/as;->e:Z

    .line 73
    return-void
.end method

.method private b()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 224
    :goto_0
    if-eqz v0, :cond_1

    .line 225
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->e()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 227
    :goto_1
    return-object v0

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/d;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v0

    goto :goto_0

    .line 227
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/as;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 149
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 150
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/d;)Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 118
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    .line 127
    :goto_0
    return v0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/az;

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 121
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ad;

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ad;

    .line 124
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    goto :goto_0

    .line 127
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 24
    check-cast p1, Lcom/google/android/apps/gmm/map/o/as;

    if-ne p0, p1, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eq v3, v4, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eqz v2, :cond_0

    :cond_2
    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    if-eq v3, v4, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-eq v3, v4, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/as;->f:Lcom/google/android/apps/gmm/map/o/af;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-nez v2, :cond_0

    goto :goto_1

    :cond_5
    iget v3, p0, Lcom/google/android/apps/gmm/map/o/as;->d:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/as;->d:I

    if-eq v3, v4, :cond_7

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/as;->d:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/as;->d:I

    if-ge v3, v4, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    if-gt v3, v4, :cond_2

    move v1, v2

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/as;->b()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v5

    invoke-direct {p1}, Lcom/google/android/apps/gmm/map/o/as;->b()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v6

    if-nez v5, :cond_8

    move v4, v0

    :goto_2
    if-nez v6, :cond_9

    move v3, v0

    :goto_3
    if-eq v4, v3, :cond_a

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_8
    move v4, v2

    goto :goto_2

    :cond_9
    move v3, v2

    goto :goto_3

    :cond_a
    if-eqz v5, :cond_b

    if-eqz v6, :cond_b

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Lcom/google/android/apps/gmm/map/b/a/j;)I

    move-result v3

    if-eqz v3, :cond_b

    move v1, v3

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_c

    move v1, v0

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_1
.end method
