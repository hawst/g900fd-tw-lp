.class Lcom/google/android/apps/gmm/myplaces/c/c;
.super Lcom/google/android/apps/gmm/myplaces/c/b;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/myplaces/c/b",
        "<",
        "Lcom/google/android/apps/gmm/myplaces/c/a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 25
    const/16 v0, 0x8

    return v0
.end method

.method public final synthetic a(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 21
    check-cast p1, Lcom/google/android/apps/gmm/myplaces/c/a;

    invoke-static {p1}, Lcom/google/android/apps/gmm/myplaces/c/c;->b(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/y;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/y;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget v3, p1, Lcom/google/android/apps/gmm/myplaces/c/a;->a:I

    int-to-long v4, v3

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v6, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    invoke-static {v8, v9}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v7, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v6, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-wide v2, p1, Lcom/google/android/apps/gmm/myplaces/c/f;->f:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_0

    const/4 v2, 0x5

    iget-wide v4, p1, Lcom/google/android/apps/gmm/myplaces/c/f;->f:J

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/myplaces/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/myplaces/c/a;->c:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v7, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, p1, Lcom/google/android/apps/gmm/myplaces/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/q;->a()Lcom/google/e/a/a/a/b;

    move-result-object v3

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_2
    const/4 v2, 0x6

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)Z
    .locals 2

    .prologue
    .line 37
    const/4 v0, 0x6

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 38
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/myplaces/c/f;
    .locals 12

    .prologue
    .line 21
    const/4 v0, 0x2

    const/16 v1, 0x1c

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v0, 0x7

    const-wide/16 v2, 0x0

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v4

    const/4 v0, 0x6

    const/16 v2, 0x1a

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/4 v2, 0x5

    const-wide/16 v6, 0x0

    invoke-static {v0, v2, v6, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v2

    const/4 v0, 0x6

    const/16 v6, 0x1a

    invoke-virtual {p1, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/4 v6, 0x1

    const/16 v7, 0x1a

    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/e/a/a/a/b;

    const/4 v7, 0x1

    const/16 v8, 0x15

    invoke-virtual {v6, v7, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x1

    const/16 v8, 0x1a

    invoke-virtual {v0, v7, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/e/a/a/a/b;

    const/4 v8, 0x2

    const/16 v9, 0x13

    invoke-virtual {v7, v8, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/l;

    const-string v1, "An alias with non-zero sub-ID is not supported."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/myplaces/c/l;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v7, 0x3

    invoke-static {v0, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v7

    const/4 v8, 0x2

    invoke-static {v0, v8}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v11, 0x4

    iget-object v10, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v11}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v10

    invoke-static {v10}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v10

    if-lez v10, :cond_3

    const/4 v10, 0x1

    :goto_0
    if-nez v10, :cond_1

    invoke-virtual {v0, v11}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_4

    :cond_1
    const/4 v10, 0x1

    :goto_1
    if-eqz v10, :cond_2

    const/4 v9, 0x4

    const/16 v10, 0x1a

    invoke-virtual {v0, v9, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v9

    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/a;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/myplaces/c/a;-><init>(Ljava/lang/String;JJILcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)V

    return-object v0

    :cond_3
    const/4 v10, 0x0

    goto :goto_0

    :cond_4
    const/4 v10, 0x0

    goto :goto_1
.end method

.method public final c(Lcom/google/e/a/a/a/b;)J
    .locals 4

    .prologue
    .line 69
    const/4 v0, 0x6

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 70
    const/4 v1, 0x5

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x1a

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 31
    const/4 v5, 0x4

    if-eqz v0, :cond_3

    iget-object v4, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_2

    move v4, v3

    :goto_0
    if-nez v4, :cond_0

    invoke-virtual {v0, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_0
    move v2, v3

    :cond_1
    if-eqz v2, :cond_3

    invoke-virtual {v0, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 32
    :goto_1
    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    move v4, v2

    .line 31
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 32
    goto :goto_2
.end method
