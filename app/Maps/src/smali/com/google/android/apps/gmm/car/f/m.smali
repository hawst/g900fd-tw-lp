.class Lcom/google/android/apps/gmm/car/f/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/f/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/f/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/f/d;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    .line 415
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/f/d;->a(Lcom/google/android/apps/gmm/car/f/d;)V

    .line 420
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 425
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->n()Ljava/lang/String;

    move-result-object v0

    .line 434
    sget-object v1, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    const-string v1, "GMM starting a call to "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 435
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/r/b/a/ru;->b:Lcom/google/r/b/a/ru;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    .line 437
    iget-object v5, v5, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v5, v5, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 436
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/z/d;->a(Lcom/google/r/b/a/ru;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/z/d;

    move-result-object v4

    aput-object v4, v2, v3

    .line 435
    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 438
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    const-string v3, "tel: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 439
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 441
    return-void

    .line 434
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 438
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/m;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/d;->e()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 446
    :cond_0
    return-void
.end method
