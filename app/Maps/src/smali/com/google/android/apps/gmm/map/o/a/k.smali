.class public Lcom/google/android/apps/gmm/map/o/a/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/n;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/o/a/d;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/a/d;I)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "intersectionFinder"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/o/a/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/a/k;->a:Lcom/google/android/apps/gmm/map/o/a/d;

    .line 28
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/a/k;->b:I

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ay;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 34
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/k;->b:I

    if-ne v0, v1, :cond_0

    move v0, v2

    .line 42
    :goto_0
    return v0

    .line 37
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/k;->a:Lcom/google/android/apps/gmm/map/o/a/d;

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/k;->b:I

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    invoke-static {v0, v3}, Lcom/google/b/a/aq;->a(II)I

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v5, p1, v0, v3}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    :cond_1
    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/k;->b:I

    .line 39
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/k;->b:I

    if-ne v0, v1, :cond_8

    move v0, v2

    .line 40
    goto :goto_0

    .line 37
    :cond_2
    iget-object v3, v5, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v5, p1, v0, v3, p2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)I

    move-result v0

    goto :goto_1

    :cond_3
    move v3, v2

    :goto_2
    const/4 v4, 0x5

    if-ge v3, v4, :cond_7

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/a/d;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    add-int/lit8 v4, v0, 0x1

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v5, p1, v4, v6}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-nez v4, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/a/d;->c:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v6, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-int/lit8 v0, v0, 0x1

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v5, p1, v0, v3, p2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)I

    move-result v0

    goto :goto_1

    :cond_6
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v5, v4, v6, p2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-nez v4, :cond_1

    add-int/lit8 v4, v0, 0x1

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/o/a/d;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/a/d;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v7, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v6, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_1

    .line 42
    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
