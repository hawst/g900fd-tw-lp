.class public final Lcom/google/android/apps/gmm/navigation/commonui/a/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I

.field public static final b:I

.field private static final c:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->c:Lcom/google/android/libraries/curvular/bk;

    .line 59
    sget v0, Lcom/google/android/apps/gmm/g;->aM:I

    sput v0, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a:I

    .line 61
    sget v0, Lcom/google/android/apps/gmm/g;->bQ:I

    sput v0, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->b:I

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/commonui/c/a;)Lcom/google/android/libraries/curvular/cs;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 125
    .line 126
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v1

    const/4 v2, 0x6

    new-array v2, v2, [Lcom/google/android/libraries/curvular/cu;

    const/16 v3, 0x14

    .line 127
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v4, v3, v0, v7}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x1

    .line 128
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->e()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    if-eqz v4, :cond_0

    new-array v0, v7, [Ljava/lang/Class;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v4

    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v4, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    sget-object v3, Lcom/google/b/f/t;->g:Lcom/google/b/f/t;

    .line 129
    invoke-static {v3}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    .line 130
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->d()Ljava/lang/Boolean;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->et:I

    .line 131
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/f;->es:I

    .line 132
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    .line 130
    invoke-static {v3, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    sget v3, Lcom/google/android/apps/gmm/l;->s:I

    .line 134
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    .line 135
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/navigation/commonui/c/f;->a()Ljava/lang/Boolean;

    move-result-object v3

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    .line 125
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Lcom/google/android/apps/gmm/navigation/commonui/c/f;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Lcom/google/android/apps/gmm/navigation/commonui/c/a;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 106
    const/4 v0, 0x7

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 107
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/f;->c()Lcom/google/android/libraries/curvular/cf;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_0

    new-array v0, v3, [Ljava/lang/Class;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v2

    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v2, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v3

    .line 108
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->k()Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v5

    const/4 v0, 0x2

    .line 109
    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v3, Lcom/google/android/apps/gmm/base/h/c;->i:Lcom/google/android/apps/gmm/base/h/c;

    new-instance v4, Lcom/google/android/apps/gmm/base/k/an;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    sget-object v2, Lcom/google/b/f/t;->bO:Lcom/google/b/f/t;

    .line 111
    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 110
    sget-object v3, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    const/4 v2, -0x1

    .line 113
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    sget v2, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a:I

    .line 115
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    .line 116
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 106
    invoke-static {p1, v1}, Lcom/google/android/libraries/curvular/br;->a([Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)[Lcom/google/android/libraries/curvular/cu;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.RelativeLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Lcom/google/android/apps/gmm/navigation/commonui/c/f;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 5

    .prologue
    .line 190
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->c:Lcom/google/android/libraries/curvular/bk;

    .line 191
    sget-object v3, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, -0x2

    .line 192
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, -0x1

    .line 193
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 194
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->c()Lcom/google/android/libraries/curvular/b;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 195
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->c()Lcom/google/android/libraries/curvular/b;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 196
    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->a:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v3, Lcom/google/android/apps/gmm/base/h/c;->h:Lcom/google/android/apps/gmm/base/h/c;

    new-instance v4, Lcom/google/android/apps/gmm/base/k/an;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 199
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/f;->b()Ljava/lang/Float;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->f:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    .line 190
    invoke-static {p1, v0}, Lcom/google/android/libraries/curvular/br;->a([Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)[Lcom/google/android/libraries/curvular/cu;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.ImageView"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/cs;Lcom/google/android/libraries/curvular/cs;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cs;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, -0x1

    const/4 v7, 0x3

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 76
    new-array v0, v9, [Lcom/google/android/libraries/curvular/cu;

    sget v1, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->b:I

    .line 77
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v6

    .line 78
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v2, v1, v3, v6}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v1, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v5

    .line 76
    invoke-virtual {p1, v0, v5}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    .line 80
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 81
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, -0x2

    .line 82
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v5

    .line 83
    sget v1, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->au:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/base/k/am;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/base/k/am;-><init>(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)V

    sget-object v1, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v9

    aput-object p0, v0, v7

    const/4 v1, 0x4

    aput-object p1, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/google/android/libraries/curvular/cu;

    .line 90
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v6

    .line 91
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v5

    .line 92
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v4, v3, v5, v6}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v9

    sget v3, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->b:I

    .line 93
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->X:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x4

    .line 94
    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->ai:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v4, p2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v2, v3

    .line 89
    new-instance v3, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v3, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-class v2, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v0, v1

    .line 80
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.RelativeLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;)Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 205
    sget-object v1, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->c:Lcom/google/android/libraries/curvular/bk;

    invoke-static {p0, v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;Ljava/util/Collection;)V

    .line 206
    new-instance v1, Lcom/google/android/apps/gmm/navigation/commonui/a/d;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/commonui/a/d;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public static b(Lcom/google/android/apps/gmm/navigation/commonui/c/a;)Lcom/google/android/libraries/curvular/cs;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 144
    .line 145
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v1

    const/4 v2, 0x6

    new-array v2, v2, [Lcom/google/android/libraries/curvular/cu;

    const/16 v3, 0x15

    .line 146
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v4, v3, v0, v7}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x1

    .line 147
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->h()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    if-eqz v4, :cond_0

    new-array v0, v7, [Ljava/lang/Class;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v4

    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v4, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    sget-object v3, Lcom/google/b/f/t;->i:Lcom/google/b/f/t;

    .line 148
    invoke-static {v3}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    .line 149
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->d()Ljava/lang/Boolean;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->ev:I

    .line 150
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/f;->eu:I

    .line 151
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    .line 149
    invoke-static {v3, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    sget v3, Lcom/google/android/apps/gmm/l;->Y:I

    .line 153
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    .line 154
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/navigation/commonui/c/f;->a()Ljava/lang/Boolean;

    move-result-object v3

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    .line 144
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Lcom/google/android/apps/gmm/navigation/commonui/c/f;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/google/android/apps/gmm/navigation/commonui/c/a;)Lcom/google/android/libraries/curvular/cs;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const v11, 0xffffff

    const/4 v10, 0x0

    .line 163
    .line 164
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v2

    const/4 v0, 0x7

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    const/16 v0, 0x15

    .line 165
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v4, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v4, v0, v1, v10}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v10

    const/4 v4, 0x1

    const-wide/high16 v6, 0x404c000000000000L    # 56.0

    .line 166
    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v6, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v11

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v4, 0x2

    .line 167
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->Z_()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    if-eqz v0, :cond_1

    new-array v1, v10, [Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v1

    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :goto_1
    sget-object v1, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x3

    sget-object v1, Lcom/google/b/f/t;->bL:Lcom/google/b/f/t;

    .line 168
    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v4, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x4

    .line 170
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->d()Ljava/lang/Boolean;

    move-result-object v1

    sget v4, Lcom/google/android/apps/gmm/f;->er:I

    .line 171
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/f;->eq:I

    .line 172
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    .line 170
    invoke-static {v1, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x5

    sget v1, Lcom/google/android/apps/gmm/l;->iA:I

    .line 174
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v1

    sget-object v4, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x6

    .line 177
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->f()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 178
    invoke-interface {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/a;->j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/navigation/commonui/c/f;->a()Ljava/lang/Boolean;

    move-result-object v4

    .line 176
    invoke-static {v1, v4}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 175
    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v3, v0

    .line 163
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Lcom/google/android/apps/gmm/navigation/commonui/c/f;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 166
    :cond_0
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v11

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    goto/16 :goto_1
.end method
