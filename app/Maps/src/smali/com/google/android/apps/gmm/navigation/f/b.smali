.class public Lcom/google/android/apps/gmm/navigation/f/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/e;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/f/s;

.field final b:Lcom/google/android/apps/gmm/navigation/f/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/navigation/f/a;

.field public final d:Lcom/google/android/apps/gmm/navigation/f/c/b;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/commonui/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/f/s;Lcom/google/android/apps/gmm/navigation/f/l;Lcom/google/android/apps/gmm/navigation/f/a;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/navigation/f/c/b;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/navigation/f/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/commonui/e;",
            ">;",
            "Lcom/google/android/apps/gmm/navigation/f/s;",
            "Lcom/google/android/apps/gmm/navigation/f/l;",
            "Lcom/google/android/apps/gmm/navigation/f/a;",
            "Lcom/google/android/apps/gmm/navigation/c/f;",
            "Lcom/google/android/apps/gmm/navigation/f/c/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/f/b;->a:Lcom/google/android/apps/gmm/navigation/f/s;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/f/b;->b:Lcom/google/android/apps/gmm/navigation/f/l;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/f/b;->c:Lcom/google/android/apps/gmm/navigation/f/a;

    .line 57
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/f/b;->d:Lcom/google/android/apps/gmm/navigation/f/c/b;

    .line 59
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/e;

    .line 153
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/e;->G_()V

    goto :goto_0

    .line 155
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/e;

    .line 167
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/e;->a(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 169
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/e;

    .line 138
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/e;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 140
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/e;

    .line 145
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/e;->b()V

    goto :goto_0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->a:Lcom/google/android/apps/gmm/navigation/f/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/f/s;->e()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/f/s;->c:Lcom/google/android/apps/gmm/navigation/f/b/a;

    const-string v2, "currentState"

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/f/s;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/f/s;->c:Lcom/google/android/apps/gmm/navigation/f/b/a;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/navigation/f/q;->a(Lcom/google/android/apps/gmm/navigation/f/b/a;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->c:Z

    .line 148
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/e;

    .line 160
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/e;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/commonui/e;

    .line 174
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/e;->g()V

    goto :goto_0

    .line 176
    :cond_0
    return-void
.end method
