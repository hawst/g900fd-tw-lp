.class public Lcom/google/android/apps/gmm/search/restriction/c/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/b;


# static fields
.field private static final b:Ljava/util/Calendar;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/search/restriction/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/gmm/search/restriction/c/h;

.field private final e:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

.field private final f:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

.field private final g:Lcom/google/android/apps/gmm/search/restriction/c/b/a;

.field private final h:Lcom/google/android/apps/gmm/search/restriction/c/j;

.field private final i:Lcom/google/android/apps/gmm/search/restriction/c/a;

.field private final j:Lcom/google/android/apps/gmm/search/restriction/c/g;

.field private final k:Lcom/google/android/apps/gmm/search/restriction/c/e;

.field private final l:Lcom/google/android/apps/gmm/search/restriction/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/k;->b:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->c:Landroid/content/Context;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/b/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->g:Lcom/google/android/apps/gmm/search/restriction/c/b/a;

    .line 78
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/h;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/h;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->d:Lcom/google/android/apps/gmm/search/restriction/c/h;

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/j;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/j;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->h:Lcom/google/android/apps/gmm/search/restriction/c/j;

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/a;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/a;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->i:Lcom/google/android/apps/gmm/search/restriction/c/a;

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/a/b;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/a/b;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->e:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/a/c;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/a/c;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->f:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/e;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/e;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->k:Lcom/google/android/apps/gmm/search/restriction/c/e;

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/g;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/g;-><init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->j:Lcom/google/android/apps/gmm/search/restriction/c/g;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/b;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/k;->b:Ljava/util/Calendar;

    invoke-direct {v0, p1, v1, p3, p4}, Lcom/google/android/apps/gmm/search/restriction/c/b;-><init>(Landroid/content/Context;Ljava/util/Calendar;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->l:Lcom/google/android/apps/gmm/search/restriction/c/b;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->i:Lcom/google/android/apps/gmm/search/restriction/c/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->l:Lcom/google/android/apps/gmm/search/restriction/c/b;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->j:Lcom/google/android/apps/gmm/search/restriction/c/g;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->h:Lcom/google/android/apps/gmm/search/restriction/c/j;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->d:Lcom/google/android/apps/gmm/search/restriction/c/h;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->k:Lcom/google/android/apps/gmm/search/restriction/c/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->g:Lcom/google/android/apps/gmm/search/restriction/c/b/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->f:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->e:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/alh;
    .locals 4

    .prologue
    .line 126
    invoke-static {}, Lcom/google/r/b/a/alh;->newBuilder()Lcom/google/r/b/a/alm;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/d/a;

    .line 130
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/a;->a()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/d/a;->a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 133
    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {v1}, Lcom/google/r/b/a/alm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alh;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/d/a;

    .line 146
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/search/restriction/d/a;->a(Lcom/google/r/b/a/alh;)V

    goto :goto_0

    .line 148
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/d/a;

    .line 118
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/search/restriction/d/a;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 120
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/d/a;

    .line 173
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/a;->ag_()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 176
    :cond_2
    const-string v0, " \u00b7 "

    new-instance v1, Lcom/google/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/b/a/ab;->a()Lcom/google/b/a/ab;

    move-result-object v0

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2, v1}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 178
    :goto_2
    return-object v0

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->c:Landroid/content/Context;

    .line 178
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->mH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 184
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/d/a;

    .line 186
    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/a;->ai_()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/restriction/d/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 189
    goto :goto_0

    .line 192
    :cond_0
    if-lez v1, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/l;->lI:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_2
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->lH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final d()Lcom/google/android/apps/gmm/search/restriction/d/c;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->g:Lcom/google/android/apps/gmm/search/restriction/c/b/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/search/restriction/d/d;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->d:Lcom/google/android/apps/gmm/search/restriction/c/h;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/search/restriction/d/d;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->k:Lcom/google/android/apps/gmm/search/restriction/c/e;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/h;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->l:Lcom/google/android/apps/gmm/search/restriction/c/b;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/base/l/a/f;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->e:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/base/l/a/f;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->f:Lcom/google/android/apps/gmm/search/restriction/c/a/a;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/search/restriction/d/d;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->h:Lcom/google/android/apps/gmm/search/restriction/c/j;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/gmm/search/restriction/d/d;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->i:Lcom/google/android/apps/gmm/search/restriction/c/a;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/search/restriction/d/d;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/k;->j:Lcom/google/android/apps/gmm/search/restriction/c/g;

    return-object v0
.end method
