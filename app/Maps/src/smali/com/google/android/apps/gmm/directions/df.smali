.class public Lcom/google/android/apps/gmm/directions/df;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/base/activities/c;

.field c:Lcom/google/android/apps/gmm/directions/h/z;

.field final d:Lcom/google/android/apps/gmm/directions/al;

.field e:Landroid/widget/ListView;

.field f:Landroid/view/View;

.field g:Landroid/view/View;

.field h:Lcom/google/android/apps/gmm/directions/dj;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field i:Lcom/google/android/apps/gmm/map/r/a/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field j:[Lcom/google/android/apps/gmm/map/r/a/w;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 80
    const-class v0, Lcom/google/android/apps/gmm/directions/df;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/df;->a:Ljava/lang/String;

    .line 82
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->g:Lcom/google/maps/g/a/hm;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/directions/al;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/directions/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/df;->d:Lcom/google/android/apps/gmm/directions/al;

    .line 109
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/a/jm;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/suggest/e/c;",
            "Lcom/google/maps/g/a/jm;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/didyoumean/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 424
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p1, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 425
    iget-object v0, p1, Lcom/google/maps/g/a/jm;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    .line 426
    new-instance v3, Lcom/google/android/apps/gmm/directions/z;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/directions/z;-><init>(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/a/ja;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 428
    :cond_0
    return-object v2
.end method


# virtual methods
.method varargs a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;
    .locals 4

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 392
    invoke-virtual {v2, p3, p4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/df;->d:Lcom/google/android/apps/gmm/directions/al;

    invoke-direct {v1, v2, p1, v3}, Lcom/google/android/apps/gmm/directions/i/c;-><init>(Ljava/lang/String;ZLcom/google/android/apps/gmm/directions/al;)V

    .line 389
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/b/e;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    return-object v0
.end method

.method a()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    if-nez v0, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 258
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 265
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_4

    :cond_2
    move v1, v3

    .line 267
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v5}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    move v0, v2

    .line 268
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v5

    .line 270
    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    array-length v6, v6

    if-eqz v6, :cond_0

    .line 276
    if-nez v1, :cond_3

    sget-object v6, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v5, v6, :cond_3

    move v2, v3

    .line 279
    :cond_3
    if-eqz v1, :cond_6

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->c:Lcom/google/android/apps/gmm/map/ag;

    .line 281
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 282
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 283
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v4

    new-instance v6, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    .line 285
    invoke-static {v0, v7}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    sget-object v0, Lcom/google/android/apps/gmm/map/i/t;->a:Lcom/google/android/apps/gmm/map/i/t;

    .line 286
    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v0

    .line 287
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v5, v2

    .line 288
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 289
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    .line 290
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    .line 291
    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->j:Lcom/google/android/apps/gmm/map/ag;

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/v;->a:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 292
    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 293
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    .line 283
    invoke-interface {v4, v1}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 265
    goto :goto_1

    .line 267
    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0, v2}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 279
    :cond_6
    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    goto :goto_3
.end method
