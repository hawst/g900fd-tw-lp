.class public Lcom/google/android/apps/gmm/search/restriction/c/b/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/c;


# instance fields
.field final a:Lcom/google/maps/g/qp;

.field final b:Lcom/google/android/apps/gmm/z/a/b;

.field c:Ljava/lang/Runnable;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/qp;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/maps/g/qp;

.field private final f:Landroid/content/Context;

.field private g:Ljava/lang/Boolean;

.field private h:Landroid/widget/SpinnerAdapter;

.field private i:Landroid/widget/AdapterView$OnItemSelectedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/b/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/restriction/c/b/b;-><init>(Lcom/google/android/apps/gmm/search/restriction/c/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->c:Ljava/lang/Runnable;

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/c/b/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/restriction/c/b/c;-><init>(Lcom/google/android/apps/gmm/search/restriction/c/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->i:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 102
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->f:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->b:Lcom/google/android/apps/gmm/z/a/b;

    .line 106
    invoke-static {}, Lcom/google/maps/g/qp;->newBuilder()Lcom/google/maps/g/qr;

    move-result-object v0

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->lE:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v3, v0, Lcom/google/maps/g/qr;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/maps/g/qr;->a:I

    iput-object v2, v0, Lcom/google/maps/g/qr;->b:Ljava/lang/Object;

    .line 108
    invoke-virtual {v0}, Lcom/google/maps/g/qr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    .line 111
    invoke-virtual {p3}, Lcom/google/maps/g/qm;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    .line 114
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 113
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->g:Ljava/lang/Boolean;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 120
    invoke-virtual {p0, p4}, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a(Lcom/google/r/b/a/alh;)V

    .line 121
    return-void

    :cond_1
    move v0, v1

    .line 114
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 4

    .prologue
    .line 182
    invoke-static {}, Lcom/google/maps/g/qp;->h()Lcom/google/maps/g/qp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/maps/g/qo;->c()V

    iget-object v1, p1, Lcom/google/maps/g/qo;->h:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    invoke-direct {v2}, Lcom/google/n/ao;-><init>()V

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    if-eq v0, v1, :cond_2

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    invoke-virtual {v0}, Lcom/google/maps/g/qp;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :cond_1
    invoke-virtual {p1}, Lcom/google/r/b/a/alm;->d()V

    iget-object v1, p1, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    invoke-interface {v1, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_2
    return-object p1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->g:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    .line 155
    const-string v0, ""

    .line 156
    iget-object v1, p1, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 157
    iget-object v0, p1, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v0, v4}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 160
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qp;

    .line 162
    iget v2, v0, Lcom/google/maps/g/qp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/maps/g/qp;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 163
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    .line 169
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->h:Landroid/widget/SpinnerAdapter;

    .line 170
    return-void

    :cond_2
    move v2, v4

    .line 162
    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->c:Ljava/lang/Runnable;

    .line 191
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    invoke-virtual {v0}, Lcom/google/maps/g/qp;->g()Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_1
    return-object v0

    .line 205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->a:Lcom/google/maps/g/qp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/widget/SpinnerAdapter;
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->h:Landroid/widget/SpinnerAdapter;

    if-nez v0, :cond_3

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qp;

    .line 132
    invoke-virtual {v0}, Lcom/google/maps/g/qp;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 135
    :cond_2
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->f:Landroid/content/Context;

    const v3, 0x1090003

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->h:Landroid/widget/SpinnerAdapter;

    .line 141
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->h:Landroid/widget/SpinnerAdapter;

    return-object v0
.end method

.method public final c()Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->i:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b/a;->e:Lcom/google/maps/g/qp;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/google/b/f/t;->s:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method
