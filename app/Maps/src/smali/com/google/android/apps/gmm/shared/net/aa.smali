.class Lcom/google/android/apps/gmm/shared/net/aa;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field private final a:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/i;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/gmm/shared/net/s;

.field private final c:Lcom/google/android/apps/gmm/shared/net/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/net/s;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 877
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    .line 878
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget-object v1, p1, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 873
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    .line 879
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/shared/net/aa;->setDaemon(Z)V

    .line 880
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    iget-object v2, p1, Lcom/google/android/apps/gmm/shared/net/s;->e:Lcom/google/e/a/a/a/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/g;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/e/a/a/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->c:Lcom/google/android/apps/gmm/shared/net/g;

    .line 881
    const-string v0, "New task: %s"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 882
    return-void
.end method

.method private a(Lcom/google/b/e/p;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1225
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1226
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 1227
    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    .line 1228
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->b:Lcom/google/android/apps/gmm/shared/net/k;

    .line 1285
    :goto_0
    return-object v0

    :cond_0
    move-object v3, v2

    .line 1256
    :goto_1
    if-eqz v3, :cond_1

    .line 1257
    :try_start_0
    invoke-interface {v5, v3}, Lcom/google/android/apps/gmm/shared/net/q;->a(Ljava/lang/String;)V

    .line 1264
    :cond_1
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->m()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1265
    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 1266
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    .line 1232
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/i;

    if-eqz v0, :cond_6

    .line 1233
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v1

    .line 1234
    invoke-static {v1}, Lcom/google/r/b/a/el;->a(I)Lcom/google/r/b/a/el;

    move-result-object v3

    .line 1235
    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    if-eq v3, v1, :cond_3

    .line 1236
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    .line 1239
    :cond_3
    iget-wide v6, p1, Lcom/google/b/e/p;->a:J

    .line 1240
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    .line 1242
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/gmm/shared/net/s;->a(Lcom/google/android/apps/gmm/shared/net/i;Ljava/io/DataInputStream;)Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v1

    .line 1244
    iget-object v5, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v5, v5, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v10

    sub-long v8, v10, v8

    .line 1245
    iget-wide v10, p1, Lcom/google/b/e/p;->a:J

    sub-long v6, v10, v6

    .line 1246
    sget-object v5, Lcom/google/android/apps/gmm/shared/net/l;->e:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v11

    const/4 v6, 0x1

    const-string v7, "b;"

    aput-object v7, v10, v6

    const/4 v6, 0x2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v10, v6

    const/4 v6, 0x3

    const-string v7, "ms"

    aput-object v7, v10, v6

    invoke-virtual {v0, v5, v10}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 1248
    if-eqz v1, :cond_4

    .line 1249
    sget-object v3, Lcom/google/android/apps/gmm/shared/net/l;->h:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/k;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 1250
    sget-object v3, Lcom/google/android/apps/gmm/shared/net/x;->a:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/k;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    move-object v0, v1

    .line 1273
    goto/16 :goto_0

    .line 1252
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v5, v3, Lcom/google/android/apps/gmm/shared/net/s;->m:Lcom/google/android/apps/gmm/shared/net/q;

    .line 1253
    if-eqz v5, :cond_0

    .line 1254
    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/net/q;->b()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1275
    :cond_4
    sget-object v5, Lcom/google/r/b/a/el;->o:Lcom/google/r/b/a/el;

    if-ne v3, v5, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/shared/net/s;->n:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->c:Lcom/google/android/apps/gmm/shared/net/g;

    .line 1276
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/g;->g()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1277
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    const/16 v5, 0x8

    iput v5, v3, Lcom/google/android/apps/gmm/shared/net/s;->p:I

    .line 1280
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 1283
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 1285
    goto/16 :goto_0

    .line 1250
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/accounts/Account;ZZZZ)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 975
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v2, :cond_1

    move v1, v2

    .line 977
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 978
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/i;

    .line 979
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->l()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 980
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 981
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "Cancelled"

    aput-object v0, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_1

    :cond_1
    move v1, v3

    .line 975
    goto :goto_0

    .line 982
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->m()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 983
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 984
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    sget-object v6, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {v0, v4, v6}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 985
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "TimeOut"

    aput-object v0, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_1

    .line 986
    :cond_3
    if-eqz p2, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->al_()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 987
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 988
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 989
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    .line 993
    :goto_2
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    .line 994
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "InvalidGaiaAuthToken"

    aput-object v0, v6, v8

    .line 993
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_1

    .line 991
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    sget-object v6, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {v0, v4, v6}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_2

    .line 995
    :cond_5
    if-nez p5, :cond_6

    if-eqz p4, :cond_7

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->ak_()Z

    move-result v4

    if-nez v4, :cond_7

    .line 996
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 997
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    .line 998
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    .line 999
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "(Offline||NotImmediate)"

    aput-object v0, v6, v8

    .line 998
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_1

    .line 1000
    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->C()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1001
    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    sget-object v6, Lcom/google/r/b/a/el;->aw:Lcom/google/r/b/a/el;

    if-eq v4, v6, :cond_8

    .line 1004
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1005
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    .line 1006
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    .line 1007
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "ClientParametersNotReady"

    aput-object v0, v6, v8

    .line 1006
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_1

    .line 1008
    :cond_8
    if-nez v1, :cond_9

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/net/s;->a(Lcom/google/r/b/a/el;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1010
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1011
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    .line 1012
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "TermsNotAccepted"

    aput-object v0, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_1

    .line 1013
    :cond_9
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->al_()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->j()Landroid/accounts/Account;

    move-result-object v4

    if-eq v4, p1, :cond_a

    if-eqz v4, :cond_b

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    :cond_a
    move v4, v2

    :goto_3
    if-nez v4, :cond_c

    .line 1016
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1017
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    sget-object v6, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {v0, v4, v6}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 1018
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    .line 1019
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "InconsistentAccounts"

    aput-object v0, v6, v8

    .line 1018
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_1

    :cond_b
    move v4, v3

    .line 1013
    goto :goto_3

    .line 1020
    :cond_c
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    .line 1021
    if-eqz p3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1029
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1030
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/lang/Object;Z)Z

    .line 1031
    const-string v4, "%s filtered out requestType=%s, reason=%s"

    new-array v6, v9, [Ljava/lang/Object;

    .line 1032
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    aput-object v0, v6, v2

    const-string v0, "LocationUnavailable"

    aput-object v0, v6, v8

    .line 1031
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_1

    .line 1035
    :cond_d
    return-void
.end method

.method private a(Ljava/io/DataOutputStream;J)V
    .locals 10

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->c:Lcom/google/android/apps/gmm/shared/net/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/g;->i()V

    .line 1182
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->c:Lcom/google/android/apps/gmm/shared/net/g;

    invoke-interface {v0, v1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 1183
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->size()I

    .line 1188
    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1189
    invoke-virtual {p1, p2, p3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 1190
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1191
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1192
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1193
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1194
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->size()I

    .line 1195
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    .line 1200
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/i;

    .line 1201
    iget-object v3, v0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    iget v3, v3, Lcom/google/r/b/a/el;->cS:I

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1202
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->size()I

    move-result v3

    .line 1203
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    .line 1204
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Ljava/io/DataOutput;)V

    .line 1205
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 1206
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->size()I

    move-result v6

    sub-int v3, v6, v3

    .line 1207
    sget-object v6, Lcom/google/android/apps/gmm/shared/net/l;->d:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v3, 0x1

    const-string v8, "b;"

    aput-object v8, v7, v3

    const/4 v3, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x3

    const-string v4, "ms"

    aput-object v4, v7, v3

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1211
    :cond_0
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V

    .line 1212
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/shared/net/q;JZ)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 18

    .prologue
    .line 1050
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 1051
    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->A()Lcom/google/r/b/a/pe;

    move-result-object v5

    .line 1052
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->d:Ljava/net/URL;

    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    move-object/from16 v16, v4

    check-cast v16, Ljava/net/HttpURLConnection;

    .line 1054
    const/4 v4, 0x1

    :try_start_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 1055
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 1057
    iget v4, v5, Lcom/google/r/b/a/pe;->b:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 1058
    iget v4, v5, Lcom/google/r/b/a/pe;->c:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 1059
    const-string v4, "Content-Type"

    const-string v5, "application/binary"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    const/4 v4, 0x0

    .line 1062
    if-eqz p1, :cond_b

    .line 1063
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/shared/net/q;->c()Ljava/lang/String;

    move-result-object v6

    .line 1064
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_8

    :cond_0
    const/4 v5, 0x1

    :goto_0
    if-nez v5, :cond_a

    .line 1065
    const-string v5, "Authorization"

    const-string v7, "Bearer "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v7, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const/4 v4, 0x1

    move v6, v4

    .line 1072
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->i:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 1073
    const-string v4, "X-Google-Maps-Mobile-API"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v5, v5, Lcom/google/android/apps/gmm/shared/net/s;->i:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    :cond_1
    const/4 v4, 0x2

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1077
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v10, v4, Lcom/google/android/apps/gmm/shared/net/s;->k:Lcom/google/android/apps/gmm/p/d/f;

    .line 1078
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_c

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_3

    .line 1079
    if-eqz v10, :cond_2

    .line 1080
    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1083
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->l:Lcom/google/android/apps/gmm/p/d/f;

    .line 1084
    if-eqz v4, :cond_3

    .line 1085
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1088
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1089
    const-string v4, "X-Geo"

    invoke-static {v5}, Lcom/google/android/apps/gmm/shared/net/s;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    :cond_4
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 1092
    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    .line 1093
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v4

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/shared/net/a/h;->g:Z

    if-nez v4, :cond_5

    .line 1099
    const-string v4, "X-Android-Transports"

    const-string v5, "http/1.1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    :cond_5
    new-instance v17, Lcom/google/b/e/q;

    new-instance v4, Ljava/io/BufferedOutputStream;

    .line 1104
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lcom/google/b/e/q;-><init>(Ljava/io/OutputStream;)V

    .line 1106
    if-eqz p4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 1107
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/shared/net/f;->b(Ljava/util/Collection;)V

    .line 1110
    :cond_6
    if-nez p1, :cond_d

    const/4 v5, 0x0

    :goto_4
    if-nez v6, :cond_e

    const/4 v6, 0x1

    :goto_5
    if-nez v10, :cond_f

    const/4 v7, 0x1

    :goto_6
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p0

    .line 1109
    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/shared/net/aa;->a(Landroid/accounts/Account;ZZZZ)V

    .line 1118
    const-wide/16 v4, 0x0

    cmp-long v4, p2, v4

    if-nez v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 1119
    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    const/4 v4, 0x1

    :goto_7
    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 1120
    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->C()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1121
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    new-instance v5, Lcom/google/android/apps/gmm/shared/net/y;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    invoke-direct {v5, v6}, Lcom/google/android/apps/gmm/shared/net/y;-><init>(Lcom/google/android/apps/gmm/shared/net/s;)V

    invoke-interface {v4, v5}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 1124
    :cond_7
    new-instance v4, Ljava/io/DataOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/gmm/shared/net/aa;->a(Ljava/io/DataOutputStream;J)V

    .line 1126
    if-eqz v10, :cond_11

    .line 1129
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/shared/net/i;

    .line 1130
    iput-object v10, v4, Lcom/google/android/apps/gmm/shared/net/i;->s:Lcom/google/android/apps/gmm/p/d/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_8

    .line 1167
    :catchall_0
    move-exception v4

    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v4

    .line 1064
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 1065
    :cond_9
    :try_start_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1068
    :cond_a
    const-string v5, "%s cannot get auth token for %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 1069
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/shared/net/q;->a()Landroid/accounts/Account;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1068
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_b
    move v6, v4

    goto/16 :goto_2

    .line 1078
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 1110
    :cond_d
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/shared/net/q;->a()Landroid/accounts/Account;

    move-result-object v5

    goto/16 :goto_4

    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_f
    const/4 v7, 0x0

    goto/16 :goto_6

    .line 1119
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 1137
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->e()J

    move-result-wide v10

    .line 1139
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    .line 1140
    const/16 v5, 0xc8

    if-eq v4, v5, :cond_12

    .line 1141
    sparse-switch v4, :sswitch_data_0

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x24

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "HTTP_UNKNOWN_STATUS_CODE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->e:Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169
    :goto_9
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    :goto_a
    return-object v4

    .line 1141
    :sswitch_0
    :try_start_2
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->c:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_9

    :sswitch_1
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->h:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_9

    :sswitch_2
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->d:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_9

    :sswitch_3
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->b:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_9

    .line 1144
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->e()J

    move-result-wide v12

    .line 1146
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v4

    .line 1147
    const-string v5, "application/binary"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 1148
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->a:Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1169
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_a

    .line 1151
    :cond_13
    :try_start_3
    new-instance v4, Lcom/google/b/e/p;

    new-instance v5, Ljava/io/BufferedInputStream;

    .line 1152
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v5}, Lcom/google/b/e/p;-><init>(Ljava/io/InputStream;)V

    .line 1153
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/shared/net/aa;->a(Lcom/google/b/e/p;)Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v5

    .line 1155
    if-eqz v5, :cond_15

    .line 1156
    sget-object v4, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne v4, v5, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_14

    .line 1158
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/shared/net/i;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v6, v6, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-virtual {v4, v6, v5}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1169
    :cond_14
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v4, v5

    goto :goto_a

    .line 1163
    :cond_15
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->e()J

    move-result-wide v14

    .line 1164
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v5, v4, Lcom/google/android/apps/gmm/shared/net/s;->j:Lcom/google/android/apps/gmm/shared/net/ae;

    move-object/from16 v0, v17

    iget-wide v6, v0, Lcom/google/b/e/q;->a:J

    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v4

    int-to-long v8, v4

    iget-boolean v4, v5, Lcom/google/android/apps/gmm/shared/net/ae;->a:Z

    if-eqz v4, :cond_16

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/gmm/shared/net/ae;->a(JJJJJ)V

    :cond_16
    monitor-enter v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-wide v10, v5, Lcom/google/android/apps/gmm/shared/net/ae;->c:J

    add-long/2addr v6, v10

    iput-wide v6, v5, Lcom/google/android/apps/gmm/shared/net/ae;->c:J

    iget-wide v6, v5, Lcom/google/android/apps/gmm/shared/net/ae;->b:J

    add-long/2addr v6, v8

    iput-wide v6, v5, Lcom/google/android/apps/gmm/shared/net/ae;->b:J

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1167
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    const/4 v4, 0x0

    goto/16 :goto_a

    .line 1164
    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1141
    nop

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x193 -> :sswitch_1
        0x1f4 -> :sswitch_2
        0x1f5 -> :sswitch_3
        0x1f7 -> :sswitch_2
    .end sparse-switch
.end method

.method public run()V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 890
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 893
    const-string v0, "%s starts running"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 898
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    sget-wide v2, Lcom/google/android/apps/gmm/shared/net/s;->a:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/f;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/i;

    .line 899
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v6, v1, Lcom/google/android/apps/gmm/shared/net/s;->m:Lcom/google/android/apps/gmm/shared/net/q;

    .line 900
    if-eqz v0, :cond_4

    .line 901
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 912
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 913
    const-wide/16 v0, 0x0

    .line 917
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v2

    if-ne v2, v8, :cond_6

    move v2, v8

    :goto_1
    if-eqz v2, :cond_a

    .line 918
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/s;->e()J

    move-result-wide v2

    .line 919
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->c:Lcom/google/android/apps/gmm/shared/net/g;

    .line 920
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/g;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    const/16 v1, 0x8

    iput v1, v0, Lcom/google/android/apps/gmm/shared/net/s;->p:I

    .line 923
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->c:Lcom/google/android/apps/gmm/shared/net/g;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/4 v5, 0x7

    const/16 v10, 0x18

    invoke-virtual {v0, v5, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-wide v0, v2

    move-object v2, v6

    .line 931
    :goto_2
    :try_start_2
    const-string v3, "GmmServerImpl.processAllRequests"
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 933
    const/4 v3, 0x1

    :try_start_3
    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/apps/gmm/shared/net/aa;->a(Lcom/google/android/apps/gmm/shared/net/q;JZ)Lcom/google/android/apps/gmm/shared/net/k;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 939
    :goto_3
    if-eqz v1, :cond_10

    .line 942
    :try_start_4
    const-string v0, "%s got an error: %s %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/net/s;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->h:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->l()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    .line 953
    :catch_0
    move-exception v0

    .line 957
    const-string v1, "REQUEST"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 959
    :goto_5
    return-void

    .line 903
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/net/f;->b(Ljava/util/Collection;)V

    .line 905
    if-nez v6, :cond_5

    move-object v1, v7

    :goto_6
    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 908
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v5, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v5, v5, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 909
    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/shared/net/s;->a(Landroid/content/Context;)Z

    move-result v5

    move-object v0, p0

    .line 904
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/shared/net/aa;->a(Landroid/accounts/Account;ZZZZ)V

    goto/16 :goto_0

    .line 905
    :cond_5
    invoke-interface {v6}, Lcom/google/android/apps/gmm/shared/net/q;->a()Landroid/accounts/Account;
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v1

    goto :goto_6

    :cond_6
    move v2, v9

    .line 917
    goto/16 :goto_1

    .line 923
    :cond_7
    :try_start_6
    iget-object v5, v1, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/4 v10, 0x7

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v5, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v10, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->X:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v10, 0x0

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v10}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    if-eqz v0, :cond_8

    const-string v5, "*"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x8

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v5, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_8
    monitor-exit v4

    move-wide v0, v2

    move-object v2, v6

    goto/16 :goto_2

    :cond_9
    move-object v0, v7

    goto :goto_7

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0

    :cond_a
    move-object v2, v7

    .line 926
    goto/16 :goto_2

    .line 937
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    move-object v1, v0

    goto/16 :goto_3

    .line 942
    :cond_b
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->m()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto/16 :goto_4

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "%s will retry requests: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/aa;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/net/s;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/s;->b:Lcom/google/android/apps/gmm/shared/net/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/net/f;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 947
    :cond_e
    :goto_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 948
    const-string v0, "REQUEST"

    const-string v1, "requestsToSend must be empty but was %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/aa;->a:Ljava/util/Deque;

    .line 949
    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/net/s;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 948
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    :cond_f
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_5

    .line 944
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/aa;->b:Lcom/google/android/apps/gmm/shared/net/s;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/s;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/shared/net/s;->o:J
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_8
.end method
