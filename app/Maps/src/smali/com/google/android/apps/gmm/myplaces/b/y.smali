.class Lcom/google/android/apps/gmm/myplaces/b/y;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/e/a/a/a/b;

.field private b:Lcom/google/e/a/a/a/b;

.field private c:I

.field private final d:Lcom/google/android/apps/gmm/myplaces/b/x;


# direct methods
.method constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/b/x;)V
    .locals 2

    .prologue
    .line 183
    sget-object v0, Lcom/google/r/b/a/el;->bz:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ab;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 185
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->a:Lcom/google/e/a/a/a/b;

    .line 186
    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->d:Lcom/google/android/apps/gmm/myplaces/b/x;

    .line 187
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 2

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->b:Lcom/google/e/a/a/a/b;

    .line 207
    const/4 v0, 0x1

    const/16 v1, 0x15

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->c:I

    .line 208
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->c:I

    packed-switch v0, :pswitch_data_0

    .line 217
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_0
    return-object v0

    .line 210
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    .line 215
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method protected final al_()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 224
    if-eqz p1, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->d:Lcom/google/android/apps/gmm/myplaces/b/x;

    iget v1, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->c:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/x;->a(I)V

    .line 254
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->b:Lcom/google/e/a/a/a/b;

    iget-object v0, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v2

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {v3, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_2
    if-nez v0, :cond_2

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->d:Lcom/google/android/apps/gmm/myplaces/b/x;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/myplaces/b/x;->a(I)V

    .line 233
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->c:I

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->d:Lcom/google/android/apps/gmm/myplaces/b/x;

    iget v2, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->c:I

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/myplaces/b/x;->a(I)V

    .line 237
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->b:Lcom/google/e/a/a/a/b;

    .line 238
    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    .line 239
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    .line 240
    :goto_3
    if-ge v2, v3, :cond_6

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->b:Lcom/google/e/a/a/a/b;

    .line 242
    const/16 v5, 0x19

    invoke-virtual {v0, v6, v2, v5}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 241
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    move v0, v1

    .line 229
    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    .line 245
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->b:Lcom/google/e/a/a/a/b;

    .line 246
    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    .line 247
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 248
    :goto_4
    if-ge v1, v2, :cond_7

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->b:Lcom/google/e/a/a/a/b;

    .line 250
    const/16 v5, 0x1c

    invoke-virtual {v0, v7, v1, v5}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 249
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 253
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/y;->d:Lcom/google/android/apps/gmm/myplaces/b/x;

    invoke-interface {v0, v4, v3}, Lcom/google/android/apps/gmm/myplaces/b/x;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
