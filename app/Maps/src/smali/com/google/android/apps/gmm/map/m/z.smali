.class public Lcom/google/android/apps/gmm/map/m/z;
.super Lcom/google/android/apps/gmm/map/m/d;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/m/r;FF)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/m/d;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    .line 63
    return-void
.end method


# virtual methods
.method public final a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")",
            "Lcom/google/android/apps/gmm/map/m/e;"
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 82
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->b:Lcom/google/android/apps/gmm/map/m/e;

    .line 140
    :goto_0
    return-object v0

    .line 87
    :cond_0
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    .line 88
    iget v3, v0, Lcom/google/android/apps/gmm/map/m/k;->b:F

    .line 90
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    move-object v2, v0

    .line 91
    :goto_1
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/m/k;

    .line 94
    iget v5, v1, Lcom/google/android/apps/gmm/map/m/k;->e:I

    iget v6, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-ne v5, v6, :cond_2

    .line 95
    iget v2, v1, Lcom/google/android/apps/gmm/map/m/k;->b:F

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/map/m/z;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 105
    const v5, 0x3e32b8c2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1

    .line 106
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 110
    goto :goto_1

    .line 111
    :cond_2
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x3dcccccd    # 0.1f

    .line 124
    :goto_2
    iget v2, v2, Lcom/google/android/apps/gmm/map/m/k;->c:F

    .line 125
    iget v0, v0, Lcom/google/android/apps/gmm/map/m/k;->c:F

    .line 126
    iget v3, p0, Lcom/google/android/apps/gmm/map/m/z;->c:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/z;->d:F

    add-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    .line 127
    sub-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v3

    .line 128
    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 132
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->b:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    .line 111
    :cond_3
    const v1, 0x3e4ccccd    # 0.2f

    goto :goto_2

    .line 140
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->c:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/z;->a:Lcom/google/android/apps/gmm/map/m/r;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/m/r;->b(Lcom/google/android/apps/gmm/map/m/n;Z)Z

    move-result v0

    return v0
.end method

.method protected final d(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/z;->a:Lcom/google/android/apps/gmm/map/m/r;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/m/r;->c(Lcom/google/android/apps/gmm/map/m/n;Z)V

    .line 151
    return-void
.end method

.method protected final f(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/z;->a:Lcom/google/android/apps/gmm/map/m/r;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/m/r;->a(Lcom/google/android/apps/gmm/map/m/n;Z)Z

    move-result v0

    return v0
.end method
