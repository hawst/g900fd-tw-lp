.class public Lcom/google/android/apps/gmm/navigation/navui/b/b;
.super Lcom/google/android/apps/gmm/navigation/commonui/b/b;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/navigation/commonui/b/b",
        "<",
        "Lcom/google/android/apps/gmm/navigation/navui/b/b;",
        ">;"
    }
.end annotation


# instance fields
.field e:Lcom/google/android/apps/gmm/navigation/g/b/f;

.field public f:Z

.field public g:Lcom/google/android/apps/gmm/map/r/a/ag;

.field h:Z

.field public i:Z

.field public j:Lcom/google/android/apps/gmm/base/g/c;

.field public k:Lcom/google/android/apps/gmm/navigation/navui/b/c;

.field private l:Lcom/google/android/apps/gmm/navigation/navui/b/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;-><init>()V

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    .line 174
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/b/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->k:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    .line 177
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/b/d;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;-><init>()V

    .line 170
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    .line 174
    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/b/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->k:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    .line 183
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->l:Lcom/google/android/apps/gmm/navigation/navui/b/d;

    .line 190
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/navui/b/d;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v1, v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    :cond_0
    invoke-super {p0, v1}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 191
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/d;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    .line 192
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/d;->e:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    .line 193
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/d;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->c:Z

    .line 194
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/d;->g:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->k:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    .line 195
    return-void

    .line 190
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a()Lcom/google/android/apps/gmm/navigation/c/a/a;
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a()Lcom/google/android/apps/gmm/navigation/c/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Lcom/google/android/apps/gmm/navigation/navui/b/b;
    .locals 3

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->l:Lcom/google/android/apps/gmm/navigation/navui/b/d;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->e:Ljava/lang/String;

    const-string v1, "Restoring from serialized state"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->l:Lcom/google/android/apps/gmm/navigation/navui/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/d;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-ltz v0, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v1, v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->l:Lcom/google/android/apps/gmm/navigation/navui/b/d;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/d;->d:I

    if-ne v1, v2, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->l:Lcom/google/android/apps/gmm/navigation/navui/b/d;

    .line 200
    :cond_1
    return-object p0
.end method

.method public final b(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/navui/b/b;
    .locals 1

    .prologue
    .line 309
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq p1, v0, :cond_0

    .line 310
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    .line 312
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    return-object v0

    .line 310
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 317
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b()V

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 320
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 321
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-nez v0, :cond_0

    .line 322
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "headerStep can only return null when we aren\'t navigating, when we aren\'t showing a message, or when we have reached the destination."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 328
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 329
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "headerStep must return a step on the current route"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v0, :cond_3

    .line 333
    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a()Lcom/google/android/apps/gmm/navigation/c/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v1, :cond_3

    .line 334
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "headerStep must be non-null when inspecting a step"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->j:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_4

    .line 338
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "arrivedAtPlacemark is only valid when exiting navigation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v0, :cond_5

    .line 342
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "headerStep must be non-null when the step is locked"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    if-eqz v0, :cond_6

    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a()Lcom/google/android/apps/gmm/navigation/c/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v1, :cond_6

    .line 346
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "destination info is not compatible with following mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_6
    return-void
.end method

.method public final c()Lcom/google/android/apps/gmm/navigation/navui/b/a;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 352
    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->e:Ljava/lang/String;

    const-string v3, "state = %s"

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v2, :cond_4

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    iput-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a()Lcom/google/android/apps/gmm/navigation/c/a/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v2, v3, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_3

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    :cond_1
    invoke-super {p0, v2}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 354
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->b()V

    .line 355
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/b/a;-><init>(Lcom/google/android/apps/gmm/navigation/navui/b/b;)V

    return-object v0

    :cond_3
    move v0, v1

    .line 353
    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v4, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    if-eqz v4, :cond_6

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v1, v0

    if-lez v1, :cond_5

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_1

    :cond_5
    iput-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_1

    :cond_6
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    if-eqz v4, :cond_7

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v5, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v6, v6

    if-ge v5, v6, :cond_a

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v2, v2, v5

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    iget v4, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    if-ge v2, v4, :cond_2

    :cond_8
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-super {p0}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a()Lcom/google/android/apps/gmm/navigation/c/a/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v2, v3, :cond_c

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_9

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_b

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    :cond_9
    invoke-super {p0, v2}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    goto :goto_1

    :cond_a
    move v2, v1

    goto :goto_2

    :cond_b
    move v0, v1

    goto :goto_3

    :cond_c
    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_d

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_e

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->h:Z

    :cond_d
    invoke-super {p0, v2}, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    goto/16 :goto_1

    :cond_e
    move v0, v1

    goto :goto_4
.end method
