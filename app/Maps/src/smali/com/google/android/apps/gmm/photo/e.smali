.class public Lcom/google/android/apps/gmm/photo/e;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Lcom/google/android/apps/gmm/photo/f;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field private i:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/j;IIIILcom/google/android/apps/gmm/photo/f;)V
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/google/r/b/a/el;->bV:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/bc;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 53
    iput p2, p0, Lcom/google/android/apps/gmm/photo/e;->b:I

    .line 54
    iput p3, p0, Lcom/google/android/apps/gmm/photo/e;->c:I

    .line 55
    iput p4, p0, Lcom/google/android/apps/gmm/photo/e;->d:I

    .line 56
    iput p5, p0, Lcom/google/android/apps/gmm/photo/e;->e:I

    .line 57
    iput-object p6, p0, Lcom/google/android/apps/gmm/photo/e;->f:Lcom/google/android/apps/gmm/photo/f;

    .line 58
    return-void
.end method


# virtual methods
.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->S_()Lcom/google/b/a/ak;

    move-result-object v1

    const-string v0, "featureId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 127
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "firstIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->b:I

    .line 128
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maxFetchCount"

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->c:I

    .line 129
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "width"

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->d:I

    .line 130
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "height"

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->e:I

    .line 131
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v1
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 2

    .prologue
    .line 95
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->e(Lcom/google/e/a/a/a/b;I)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/photo/e;->i:I

    .line 97
    const/4 v0, 0x1

    .line 99
    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v1

    .line 97
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;ILcom/google/n/at;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/e;->h:Ljava/util/List;

    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 106
    instance-of v2, p1, Lcom/google/android/apps/gmm/photo/e;

    if-nez v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/photo/e;

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, p1, Lcom/google/android/apps/gmm/photo/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/photo/e;->b:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/photo/e;->c:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/photo/e;->d:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/photo/e;->e:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 74
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bc;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 76
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/maps/g/b/l;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 77
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 78
    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 80
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/bc;->e:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 81
    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->b:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 82
    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->c:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 83
    const/4 v2, 0x4

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 85
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/maps/a/a/a;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 86
    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->d:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 87
    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->e:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v5, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 88
    const/4 v2, 0x3

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 90
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 120
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/e;->f:Lcom/google/android/apps/gmm/photo/f;

    if-eqz v0, :cond_0

    .line 64
    if-nez p1, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/e;->f:Lcom/google/android/apps/gmm/photo/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/e;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/android/apps/gmm/photo/e;->i:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/f;->a(Ljava/util/List;I)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/e;->f:Lcom/google/android/apps/gmm/photo/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/photo/f;->a()V

    goto :goto_0
.end method
