.class Lcom/google/android/apps/gmm/shared/a/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/api/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/v",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/shared/a/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/a/c;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/a/d;->a:Lcom/google/android/apps/gmm/shared/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/u;)V
    .locals 5

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/a/d;->a:Lcom/google/android/apps/gmm/shared/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/a/c;->b:Lcom/google/android/apps/gmm/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/i/a;->b()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Failed to execute feedback request in Google Play Services."

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Failed to execute feedback request in Google Play Services: "

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/a/d;->a:Lcom/google/android/apps/gmm/shared/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/a/c;->c:Lcom/google/android/apps/gmm/shared/a/e;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/a/c;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/a/d;->a:Lcom/google/android/apps/gmm/shared/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/a/c;->c:Lcom/google/android/apps/gmm/shared/a/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/a/e;->a()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
