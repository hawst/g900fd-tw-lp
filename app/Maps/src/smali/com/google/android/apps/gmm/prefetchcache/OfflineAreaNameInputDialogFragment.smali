.class public Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final c:Ljava/lang/String;

.field private static final d:Lcom/google/android/libraries/curvular/au;


# instance fields
.field private e:Landroid/view/View;

.field private f:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide v8, 0x4072c00000000000L    # 300.0

    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    const v10, 0xffffff

    .line 30
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->c:Ljava/lang/String;

    .line 33
    const-wide/high16 v0, 0x4069000000000000L    # 200.0

    const-wide v2, 0x407c200000000000L    # 450.0

    .line 34
    new-instance v4, Lcom/google/android/libraries/curvular/as;

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v6

    if-eqz v6, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v10

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v1, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v6, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v10

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v7, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v7, v10

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sput-object v4, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->d:Lcom/google/android/libraries/curvular/au;

    .line 33
    return-void

    .line 34
    :cond_0
    mul-double/2addr v0, v12

    sget-object v6, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v6}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v10

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v6, v8, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v10

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double/2addr v8, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v7, v10

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double/2addr v2, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 39
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;-><init>()V

    .line 49
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 50
    const-string v2, "input_text"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 161
    instance-of v1, v0, Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 162
    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 165
    :cond_0
    return-void

    .line 163
    :cond_1
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->i()V

    .line 170
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/b/f/t;->cf:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 95
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    :goto_0
    return-void

    .line 122
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 134
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->c:Ljava/lang/String;

    goto :goto_0

    .line 124
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->ch:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/u;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, ""

    :goto_2
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/u;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    .line 126
    :cond_2
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 129
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_3

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->cg:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->dismiss()V

    goto :goto_0

    .line 129
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_3

    .line 122
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->e:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->d:Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v2, v0}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setMinimumWidth(I)V

    .line 87
    :cond_0
    return-void

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 62
    if-eqz p1, :cond_0

    .line 63
    :goto_0
    const-string v0, "input_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    move-object v1, v0

    .line 65
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v2

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/h;->ab:I

    .line 66
    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->e:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->e:Landroid/view/View;

    sget v3, Lcom/google/android/apps/gmm/g;->cF:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->e:Landroid/view/View;

    sget-object v3, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->d:Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v2

    :goto_3
    invoke-interface {v3, v0}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setMinimumWidth(I)V

    .line 72
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_4
    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/gmm/l;->jB:I

    .line 73
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->e:Landroid/view/View;

    .line 74
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->mC:I

    .line 75
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    .line 76
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto/16 :goto_0

    .line 63
    :cond_1
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 65
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto/16 :goto_2

    .line 70
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_3

    .line 72
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_4
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/u;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/u;->c()V

    .line 142
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onPause()V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->i()V

    .line 103
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 114
    const-string v1, "input_text"

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void

    .line 114
    :cond_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 180
    return-void
.end method
