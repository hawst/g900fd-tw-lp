.class public Lcom/google/android/apps/gmm/navigation/a/ax;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/google/android/apps/gmm/navigation/a/ae;

.field final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/maps/g/a/al;

.field private f:Lcom/google/android/apps/gmm/shared/c/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/ax;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->c:Ljava/util/HashMap;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->d:Ljava/util/Set;

    .line 306
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/navigation/a/ax;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/ax;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/a/ax;-><init>()V

    .line 77
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/ae;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/a/ae;-><init>(Landroid/content/Context;)V

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    iput-object p0, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 79
    return-object v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/am;Z)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 481
    .line 482
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    if-ne v1, v2, :cond_2

    .line 485
    if-eqz p2, :cond_1

    .line 486
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/ay;->b:[I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/n;->a(Ljava/util/List;)Lcom/google/maps/g/a/df;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/a/df;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 496
    sget v1, Lcom/google/android/apps/gmm/l;->eO:I

    .line 497
    const/4 v0, 0x6

    .line 516
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/b/f;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/a/b/f;-><init>(I)V

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 526
    :cond_0
    :goto_1
    return-object v0

    .line 488
    :pswitch_0
    sget v1, Lcom/google/android/apps/gmm/l;->cr:I

    .line 489
    const/16 v0, 0x9

    .line 490
    goto :goto_0

    .line 492
    :pswitch_1
    sget v1, Lcom/google/android/apps/gmm/l;->cs:I

    .line 493
    const/16 v0, 0xa

    .line 494
    goto :goto_0

    .line 501
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/ay;->b:[I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/n;->a(Ljava/util/List;)Lcom/google/maps/g/a/df;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/a/df;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 511
    sget v1, Lcom/google/android/apps/gmm/l;->cq:I

    .line 512
    const/4 v0, 0x5

    goto :goto_0

    .line 503
    :pswitch_2
    sget v1, Lcom/google/android/apps/gmm/l;->co:I

    .line 504
    const/4 v0, 0x7

    .line 505
    goto :goto_0

    .line 507
    :pswitch_3
    sget v1, Lcom/google/android/apps/gmm/l;->cp:I

    .line 508
    const/16 v0, 0x8

    .line 509
    goto :goto_0

    .line 519
    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    .line 520
    iget v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->j:I

    if-ltz v3, :cond_3

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/b/g;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/navigation/a/b/g;-><init>(I)V

    .line 521
    :goto_2
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 522
    invoke-static {p1, v2, v1}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v1, v0

    .line 520
    goto :goto_2

    .line 486
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 501
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 2

    .prologue
    .line 621
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 540
    .line 541
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->g:Ljava/lang/String;

    if-nez v3, :cond_0

    iget v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    if-ltz v3, :cond_2

    :cond_0
    move v3, v1

    :goto_0
    if-eqz v3, :cond_4

    .line 542
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/am;->a()Ljava/lang/CharSequence;

    move-result-object v2

    .line 543
    iget v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->j:I

    if-ltz v3, :cond_3

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/b/g;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/navigation/a/b/g;-><init>(I)V

    .line 544
    :goto_1
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 545
    invoke-static {p1, v2, v1}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 561
    :cond_1
    :goto_2
    return-object v0

    :cond_2
    move v3, v2

    .line 541
    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 543
    goto :goto_1

    .line 547
    :cond_4
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v4, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-ne v3, v4, :cond_6

    .line 550
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 551
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 552
    if-eqz v1, :cond_1

    .line 553
    const v0, 0x7fffffff

    if-eq p2, v0, :cond_5

    .line 555
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    .line 556
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    .line 555
    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/am;ILcom/google/maps/g/a/al;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    goto :goto_2

    .line 554
    :cond_5
    iget p2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    goto :goto_3

    .line 559
    :cond_6
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v3, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_4
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;Z)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 109
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    const v3, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v3

    .line 111
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    .line 112
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v4, p2, v4

    .line 113
    int-to-float v5, v4

    neg-float v1, v1

    cmpl-float v1, v5, v1

    if-ltz v1, :cond_0

    int-to-float v1, v4

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_0

    .line 114
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 117
    :cond_0
    const/4 v3, 0x0

    .line 118
    if-eqz p3, :cond_4

    .line 129
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/map/r/a/an;->a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/b/a/an;

    move-result-object v1

    .line 130
    iget-object v0, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 131
    iget-object v1, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 132
    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 133
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/am;

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/apps/gmm/navigation/a/ax;->b(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v1

    .line 137
    instance-of v3, v1, Lcom/google/android/apps/gmm/navigation/a/b/k;

    if-nez v3, :cond_3

    .line 138
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v0, v3, :cond_3

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    invoke-virtual {v0, v4, v3}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(ILcom/google/maps/g/a/al;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/k;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    invoke-direct {v0, v4, v3, v1}, Lcom/google/android/apps/gmm/navigation/a/b/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/b/h;)V

    .line 143
    :goto_0
    if-nez v0, :cond_1

    .line 144
    if-nez p3, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/navigation/a/ax;->b(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 146
    :cond_1
    return-object v0

    :cond_2
    move v0, v2

    .line 144
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_0
.end method

.method public a(I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    int-to-long v2, p1

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 167
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->dV:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 635
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    monitor-exit p0

    return-void

    .line 635
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 426
    .line 427
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v2, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    if-ne v0, v2, :cond_5

    .line 428
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v2

    .line 429
    if-eqz v2, :cond_2

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->e:Z

    if-eqz v0, :cond_2

    .line 430
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->f:Lcom/google/android/apps/gmm/map/r/a/am;

    if-eqz v0, :cond_2

    .line 432
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->f:Lcom/google/android/apps/gmm/map/r/a/am;

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;Z)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v6

    .line 433
    if-eqz v6, :cond_a

    .line 434
    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    .line 435
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->g:Ljava/lang/String;

    if-nez v3, :cond_0

    iget v3, p1, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    if-ltz v3, :cond_3

    :cond_0
    move v3, v5

    :goto_0
    if-nez v3, :cond_1

    .line 436
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->cj:I

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v4

    .line 437
    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    aput-object v0, v8, v5

    .line 436
    invoke-virtual {v3, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 440
    :cond_1
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/b/h;->b:Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 441
    new-instance v3, Lcom/google/android/apps/gmm/navigation/a/b/f;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/navigation/a/b/f;-><init>(I)V

    .line 443
    iget-object v4, v6, Lcom/google/android/apps/gmm/navigation/a/b/h;->b:Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 441
    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/navigation/a/b/a;->a(Lcom/google/android/apps/gmm/navigation/a/b/a;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/a;

    move-result-object v3

    .line 445
    if-nez v3, :cond_4

    :goto_1
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/navigation/a/b/a;->a(Lcom/google/android/apps/gmm/navigation/a/b/a;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/a;

    move-result-object v1

    .line 446
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    :goto_2
    move-object v2, v0

    .line 461
    :cond_2
    :goto_3
    return-object v2

    :cond_3
    move v3, v4

    .line 435
    goto :goto_0

    .line 445
    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/b/e;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/navigation/a/b/e;-><init>(Lcom/google/android/apps/gmm/navigation/a/b/a;)V

    goto :goto_1

    .line 449
    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v2, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v0, v2, :cond_7

    if-ltz p2, :cond_7

    .line 450
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_6

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    invoke-virtual {v1, p2, v2}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(ILcom/google/maps/g/a/al;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/b/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    invoke-direct {v1, v3, v2, v0}, Lcom/google/android/apps/gmm/navigation/a/b/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/b/h;)V

    :cond_6
    move-object v2, v1

    .line 454
    goto :goto_3

    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v2, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-ne v0, v2, :cond_9

    .line 457
    if-eqz p3, :cond_8

    const p2, 0x7fffffff

    .line 459
    :cond_8
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v2

    goto :goto_3

    :cond_9
    move-object v2, v1

    goto :goto_3

    :cond_a
    move-object v0, v2

    goto :goto_2
.end method
