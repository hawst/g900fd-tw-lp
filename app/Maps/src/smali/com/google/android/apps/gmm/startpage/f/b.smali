.class public Lcom/google/android/apps/gmm/startpage/f/b;
.super Lcom/google/android/apps/gmm/startpage/f/i;
.source "PG"


# instance fields
.field private k:Lcom/google/android/apps/gmm/startpage/e/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/startpage/f/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 23
    return-void
.end method

.method private k()V
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/b;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->w()Lcom/google/o/h/a/hb;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/f/b;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 42
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->x()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/b;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/startpage/d/d;->z()Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/f/b;->k:Lcom/google/android/apps/gmm/startpage/e/c;

    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/f/b;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 43
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/startpage/d/d;->A()Lcom/google/android/apps/gmm/map/s/a;

    move-result-object v5

    .line 41
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/f/a;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/startpage/e/c;Lcom/google/android/apps/gmm/map/s/a;)Lcom/google/android/apps/gmm/startpage/f/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/b;->d:Lcom/google/android/apps/gmm/startpage/e/h;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/startpage/e/c;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/b;->k:Lcom/google/android/apps/gmm/startpage/e/c;

    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/f/b;->k()V

    .line 29
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    iget v1, v0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hb;->g()Lcom/google/o/h/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hb;

    .line 34
    :goto_2
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 35
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/b;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/hb;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/f/b;->k()V

    .line 38
    :cond_0
    return-void

    .line 33
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
