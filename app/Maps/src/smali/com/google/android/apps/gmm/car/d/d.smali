.class Lcom/google/android/apps/gmm/car/d/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/d/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/d/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/d/a;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(F)V
    .locals 4

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 352
    :goto_0
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/car/d/z;->a(Lcom/google/android/apps/gmm/map/f/a/a;F)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v1

    .line 353
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    iget v3, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/car/d/i;->a(F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 354
    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/car/d/z;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/d/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/car/d/z;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 355
    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/car/d/z;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    .line 358
    :cond_1
    return-void

    .line 350
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/d/a;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->r:Lcom/google/android/apps/gmm/car/d/j;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/j;->b:Lcom/google/android/apps/gmm/car/d/j;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/d/a;->o:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/car/n/c;->g()Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/car/n/c;->f()Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->performClick()Z

    .line 301
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->b()V

    .line 307
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/car/d/d;->a(F)V

    .line 308
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->b()V

    .line 314
    const/high16 v0, -0x40800000    # -1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/car/d/d;->a(F)V

    .line 315
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->b:Lcom/google/android/apps/gmm/car/m/m;

    new-instance v1, Lcom/google/android/apps/gmm/car/d/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    .line 321
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/d/a;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/d/a;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/car/d/a;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/d/t;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/car/m/m;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/car/d/a;)V

    .line 320
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 322
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->b()V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/d/i;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/d/a;->e:Lcom/google/android/apps/gmm/map/util/b/a/a;

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/f/a/g;-><init>()V

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/f/a/g;->d:Lcom/google/android/apps/gmm/map/f/a/e;

    const/high16 v0, 0x41700000    # 15.0f

    iput v0, v2, Lcom/google/android/apps/gmm/map/f/a/g;->a:F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/a/g;->a()Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/mylocation/e/b;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/mylocation/e/b;-><init>(Lcom/google/android/apps/gmm/map/f/a/f;Z)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    .line 332
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 330
    :cond_2
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ax;->a:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ax;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget v1, v0, Lcom/google/android/apps/gmm/car/d/a;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/d/a;->q:I

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/gmm/car/d/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/d/c;-><init>(Lcom/google/android/apps/gmm/car/d/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 339
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget v1, v0, Lcom/google/android/apps/gmm/car/d/a;->q:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/d/a;->q:I

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/d;->a:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/gmm/car/d/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/d/c;-><init>(Lcom/google/android/apps/gmm/car/d/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 346
    const/4 v0, 0x0

    return-object v0
.end method
