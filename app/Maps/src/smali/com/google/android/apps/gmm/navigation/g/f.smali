.class public Lcom/google/android/apps/gmm/navigation/g/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/g/c;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/navigation/g/c;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 178
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/h;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 179
    if-eqz v0, :cond_0

    .line 181
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->e:Z

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->f:Z

    .line 187
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v3, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 188
    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->b:Z

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->d:Z

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->h:Lcom/google/android/apps/gmm/navigation/g/e;

    if-nez v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/g/e;-><init>(Lcom/google/android/apps/gmm/navigation/g/c;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->h:Lcom/google/android/apps/gmm/navigation/g/e;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/c;->h:Lcom/google/android/apps/gmm/navigation/g/e;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 205
    return-void

    :cond_3
    move v0, v2

    .line 188
    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/i;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 211
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->e:Z

    .line 213
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->f:Z

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 223
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 224
    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 225
    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v3, v3

    if-le v3, v1, :cond_4

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v1, v2, v1

    .line 226
    :goto_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    new-instance v2, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    .line 234
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_3

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->c:Lcom/google/android/apps/gmm/x/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/g/c;->a:Lcom/google/android/apps/gmm/x/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)V

    .line 239
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/f;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/c;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 240
    return-void

    .line 225
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method
