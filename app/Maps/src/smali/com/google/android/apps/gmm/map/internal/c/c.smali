.class public Lcom/google/android/apps/gmm/map/internal/c/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:I

.field public b:Lcom/google/android/apps/gmm/map/internal/c/y;

.field public c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->d:I

    .line 63
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    .line 64
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->e:I

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    .line 66
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    .line 67
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/y;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->d:I

    .line 71
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    .line 72
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->e:I

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    .line 74
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    .line 75
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/c;
    .locals 3

    .prologue
    const/16 v1, 0x1a

    const/4 v2, 0x1

    .line 82
    invoke-virtual {p0, v2, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 83
    if-eqz v0, :cond_0

    .line 84
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/c;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/c;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    .line 87
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 88
    if-eqz v0, :cond_1

    .line 89
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/y;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/y;

    move-result-object v0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/y;I)V

    move-object v0, v1

    goto :goto_0

    .line 91
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/b/a/c;)Lcom/google/android/apps/gmm/map/internal/c/c;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 106
    if-nez p0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/google/maps/b/a/c;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_2

    .line 110
    invoke-virtual {p0}, Lcom/google/maps/b/a/c;->a()Lcom/google/maps/b/a/d;

    move-result-object v1

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/c;

    iget-object v2, v1, Lcom/google/maps/b/a/d;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    iget-object v3, v1, Lcom/google/maps/b/a/d;->c:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    .line 112
    iget-object v4, v1, Lcom/google/maps/b/a/d;->d:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    iget-object v1, v1, Lcom/google/maps/b/a/d;->a:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/apps/gmm/map/internal/c/c;-><init>(IIII)V

    goto :goto_0

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/google/maps/b/a/c;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v1, :cond_0

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/c;

    invoke-virtual {p0}, Lcom/google/maps/b/a/c;->b()Lcom/google/maps/b/a/u;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/c/y;->a(Lcom/google/maps/b/a/u;)Lcom/google/android/apps/gmm/map/internal/c/y;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/y;I)V

    goto :goto_0
.end method

.method public static b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/c;
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 95
    const/4 v0, 0x1

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v1

    .line 97
    const/4 v0, 0x2

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v2

    .line 99
    const/4 v0, 0x3

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v3

    .line 101
    const/4 v0, 0x4

    const/16 v4, 0x15

    invoke-virtual {p0, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    .line 102
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/c;

    invoke-direct {v4, v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/c/c;-><init>(IIII)V

    return-object v4
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    if-ne p0, p1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 161
    goto :goto_0

    .line 163
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 164
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/c;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/c;->e:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 173
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/b/a/ak;

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "adsResponseId"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->d:I

    .line 174
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "textAdIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->a:I

    .line 175
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "textAdLocationIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->e:I

    .line 176
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "ad"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->b:Lcom/google/android/apps/gmm/map/internal/c/y;

    .line 177
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "adType"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    .line 178
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 179
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
