.class public final enum Lcom/google/android/apps/gmm/myplaces/c/j;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/myplaces/c/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/myplaces/c/j;

.field public static final enum b:Lcom/google/android/apps/gmm/myplaces/c/j;

.field public static final enum c:Lcom/google/android/apps/gmm/myplaces/c/j;

.field public static final enum d:Lcom/google/android/apps/gmm/myplaces/c/j;

.field public static final enum e:Lcom/google/android/apps/gmm/myplaces/c/j;

.field public static final enum f:Lcom/google/android/apps/gmm/myplaces/c/j;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/myplaces/c/j;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    const-string v1, "SYNCHRONIZE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/myplaces/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->a:Lcom/google/android/apps/gmm/myplaces/c/j;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    const-string v1, "FIRST_SYNC"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/myplaces/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->b:Lcom/google/android/apps/gmm/myplaces/c/j;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    const-string v1, "DELETE_DATA_SOURCES"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/myplaces/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->c:Lcom/google/android/apps/gmm/myplaces/c/j;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    const-string v1, "CLEAR_CACHE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/myplaces/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->d:Lcom/google/android/apps/gmm/myplaces/c/j;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    const-string v1, "STARRING_SUCCEEDED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/myplaces/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->e:Lcom/google/android/apps/gmm/myplaces/c/j;

    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    const-string v1, "STARRING_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/myplaces/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->f:Lcom/google/android/apps/gmm/myplaces/c/j;

    .line 17
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/myplaces/c/j;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->a:Lcom/google/android/apps/gmm/myplaces/c/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->b:Lcom/google/android/apps/gmm/myplaces/c/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->c:Lcom/google/android/apps/gmm/myplaces/c/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->d:Lcom/google/android/apps/gmm/myplaces/c/j;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->e:Lcom/google/android/apps/gmm/myplaces/c/j;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/j;->f:Lcom/google/android/apps/gmm/myplaces/c/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->g:[Lcom/google/android/apps/gmm/myplaces/c/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/myplaces/c/j;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/c/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/myplaces/c/j;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->g:[Lcom/google/android/apps/gmm/myplaces/c/j;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/myplaces/c/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/myplaces/c/j;

    return-object v0
.end method
