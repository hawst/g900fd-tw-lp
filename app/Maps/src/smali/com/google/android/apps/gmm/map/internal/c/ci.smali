.class public Lcom/google/android/apps/gmm/map/internal/c/ci;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:[Ljava/lang/Integer;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/b/a/j;

.field b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Integer;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ci;->c:[Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ci;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/ch;
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ci;->b:Ljava/util/Set;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ci;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 154
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ch;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ci;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ci;->b:Ljava/util/Set;

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/ci;->c:[Ljava/lang/Integer;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/ch;-><init>(Lcom/google/android/apps/gmm/map/b/a/j;[Ljava/lang/Integer;)V

    return-object v1
.end method
