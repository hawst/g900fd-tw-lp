.class public Lcom/google/android/apps/gmm/p/b/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/p/b/d;

.field public b:Lcom/google/android/apps/gmm/p/b/d;

.field public c:Lcom/google/android/apps/gmm/p/b/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    .line 47
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    .line 48
    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->a:Lcom/google/android/apps/gmm/p/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/p/b/b;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    .line 53
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    .line 54
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    .line 55
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;
    .locals 2

    .prologue
    .line 120
    sget-object v0, Lcom/google/android/apps/gmm/p/b/c;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/b/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 131
    sget-object v0, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    :goto_0
    return-object v0

    .line 122
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/jl;->e:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 124
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/jl;->d:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 126
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/jl;->c:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 128
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/jl;->b:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 160
    if-ne p0, p1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/google/android/apps/gmm/p/b/b;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 165
    goto :goto_0

    .line 168
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/p/b/b;

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    iget-object v3, p1, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    iget-object v3, p1, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    iget-object v3, p1, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    const-string v1, "GmmLocationControllerState[gps = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 178
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cell = "

    .line 179
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wifi = "

    .line 180
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    .line 181
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
