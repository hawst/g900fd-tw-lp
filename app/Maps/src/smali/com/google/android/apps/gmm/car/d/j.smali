.class final enum Lcom/google/android/apps/gmm/car/d/j;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/car/d/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/car/d/j;

.field public static final enum b:Lcom/google/android/apps/gmm/car/d/j;

.field public static final enum c:Lcom/google/android/apps/gmm/car/d/j;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/car/d/j;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/car/d/j;

    const-string v1, "ON"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/car/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/d/j;->a:Lcom/google/android/apps/gmm/car/d/j;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/car/d/j;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/car/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/d/j;->b:Lcom/google/android/apps/gmm/car/d/j;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/car/d/j;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/car/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/d/j;->c:Lcom/google/android/apps/gmm/car/d/j;

    .line 59
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/car/d/j;

    sget-object v1, Lcom/google/android/apps/gmm/car/d/j;->a:Lcom/google/android/apps/gmm/car/d/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/car/d/j;->b:Lcom/google/android/apps/gmm/car/d/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/car/d/j;->c:Lcom/google/android/apps/gmm/car/d/j;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/car/d/j;->d:[Lcom/google/android/apps/gmm/car/d/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/car/d/j;
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/google/android/apps/gmm/car/d/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/d/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/car/d/j;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/apps/gmm/car/d/j;->d:[Lcom/google/android/apps/gmm/car/d/j;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/car/d/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/car/d/j;

    return-object v0
.end method
