.class Lcom/google/android/apps/gmm/car/e/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/g;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/e/x;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/e/x;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    new-instance v0, Lcom/google/android/apps/gmm/car/e/v;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/e/x;->n:Lcom/google/android/apps/gmm/car/e/i;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/car/e/v;-><init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/e/i;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/m/i;->e()V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/h;->a()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/apps/gmm/car/m/o;->b:Lcom/google/android/apps/gmm/car/m/o;

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v0, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iput-object v2, v1, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    .line 420
    :cond_1
    :goto_1
    return-void

    .line 419
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/o;)V

    goto :goto_1
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->r:Lcom/google/android/apps/gmm/car/k/h;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/car/k/h;->a(I)V

    .line 483
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V
    .locals 0

    .prologue
    .line 425
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/j/b/a;)V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/j/b/b;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 429
    if-nez p1, :cond_0

    .line 452
    :goto_0
    return-void

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    .line 433
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    .line 432
    iget-wide v6, p1, Lcom/google/android/apps/gmm/navigation/j/b/b;->a:J

    cmp-long v0, v6, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    new-instance v3, Lcom/google/android/apps/gmm/z/b;

    sget-object v4, Lcom/google/b/f/b/a/ah;->b:Lcom/google/b/f/b/a/ah;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    .line 437
    iget-object v5, v5, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v5, v5, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/z/b;-><init>(Lcom/google/b/f/b/a/ah;Lcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v3, v1, v2

    .line 434
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 432
    goto :goto_1

    .line 442
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/gmm/car/e/aj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/e/aj;-><init>(Lcom/google/android/apps/gmm/car/e/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/j/a/d;->a:Lcom/google/android/apps/gmm/navigation/j/a/d;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 478
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    .line 470
    return-void
.end method

.method public isResumed()Z
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/ai;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/bi;->d:Z

    return v0
.end method
