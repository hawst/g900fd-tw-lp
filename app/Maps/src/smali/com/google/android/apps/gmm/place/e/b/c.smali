.class public Lcom/google/android/apps/gmm/place/e/b/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/e/b/a;


# instance fields
.field private a:Lcom/google/android/apps/gmm/z/b/l;

.field private b:Ljava/lang/Boolean;

.field private c:Lcom/google/maps/g/xo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/e/b/c;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/high16 v4, 0x4000000

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 42
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 43
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v1, v1, Lcom/google/r/b/a/ads;->a:I

    and-int/2addr v1, v4

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/e/b/c;->b:Ljava/lang/Boolean;

    .line 44
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/e/b/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->K:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/xo;->d()Lcom/google/maps/g/xo;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/xo;

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/e/b/c;->c:Lcom/google/maps/g/xo;

    .line 58
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 59
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    new-array v0, v2, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->dE:Lcom/google/b/f/t;

    aput-object v2, v0, v3

    .line 60
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 61
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/e/b/c;->a:Lcom/google/android/apps/gmm/z/b/l;

    .line 63
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 43
    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 74
    invoke-static {}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/a;

    move-result-object v2

    .line 75
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/e/b/c;->c:Lcom/google/maps/g/xo;

    .line 76
    iget-boolean v0, v0, Lcom/google/maps/g/xo;->f:Z

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/e/b/c;->c:Lcom/google/maps/g/xo;

    iget-object v0, v4, Lcom/google/maps/g/xo;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .line 75
    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " "

    .line 77
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/e/b/c;->c:Lcom/google/maps/g/xo;

    .line 78
    iget-object v0, v4, Lcom/google/maps/g/xo;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 76
    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v4, Lcom/google/maps/g/xo;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/e/b/c;->c:Lcom/google/maps/g/xo;

    iget-object v0, v4, Lcom/google/maps/g/xo;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_3
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v1, v4, Lcom/google/maps/g/xo;->e:Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 78
    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v1, v4, Lcom/google/maps/g/xo;->c:Ljava/lang/Object;

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/e/b/c;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4

    .prologue
    .line 104
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/e/b/c;->c:Lcom/google/maps/g/xo;

    .line 105
    iget-object v0, v3, Lcom/google/maps/g/xo;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    return-object v2

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v3, Lcom/google/maps/g/xo;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/e/b/c;->a:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
