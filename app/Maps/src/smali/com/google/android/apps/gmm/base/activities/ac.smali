.class public final enum Lcom/google/android/apps/gmm/base/activities/ac;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/activities/ac;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/activities/ac;

.field public static final enum b:Lcom/google/android/apps/gmm/base/activities/ac;

.field public static final enum c:Lcom/google/android/apps/gmm/base/activities/ac;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum d:Lcom/google/android/apps/gmm/base/activities/ac;

.field public static final enum e:Lcom/google/android/apps/gmm/base/activities/ac;

.field public static final enum f:Lcom/google/android/apps/gmm/base/activities/ac;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/base/activities/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ac;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ac;

    const-string v1, "COLLAPSED_OR_SIDE_SHEET"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/base/activities/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->b:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ac;

    const-string v1, "OMNIBOX"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/base/activities/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->c:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ac;

    const-string v1, "ONLY_WHEN_SLIDER_EXPANDED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/base/activities/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->d:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ac;

    const-string v1, "OVERLAPPING"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/base/activities/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->e:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ac;

    const-string v1, "OVERLAPPING_WITH_SPACE_FOR_SLIDER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->f:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 88
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->b:Lcom/google/android/apps/gmm/base/activities/ac;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->c:Lcom/google/android/apps/gmm/base/activities/ac;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->d:Lcom/google/android/apps/gmm/base/activities/ac;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->e:Lcom/google/android/apps/gmm/base/activities/ac;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->f:Lcom/google/android/apps/gmm/base/activities/ac;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->g:[Lcom/google/android/apps/gmm/base/activities/ac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/activities/ac;
    .locals 1

    .prologue
    .line 88
    const-class v0, Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/ac;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/activities/ac;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/ac;->g:[Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/activities/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/activities/ac;

    return-object v0
.end method
