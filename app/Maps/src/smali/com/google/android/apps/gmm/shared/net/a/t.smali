.class public Lcom/google/android/apps/gmm/shared/net/a/t;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/r/b/a/aqg;


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/aqg;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 117
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-object v0, v2, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v4, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "personalizedSmartMapsTileDurationMs"

    .line 22
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v5, v5, Lcom/google/r/b/a/aqg;->b:I

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "onlyRequestPsmWhenPoiInBaseTile"

    .line 23
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v1, v1, Lcom/google/r/b/a/aqg;->c:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "minPsmRequestZoom"

    .line 24
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v1, v1, Lcom/google/r/b/a/aqg;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "pertileDurationMs"

    .line 25
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v5, v5, Lcom/google/r/b/a/aqg;->e:I

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "psmPertileDurationMs"

    .line 26
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v5, v5, Lcom/google/r/b/a/aqg;->m:I

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "diskCacheServerSchemaVersion"

    .line 27
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v1, v1, Lcom/google/r/b/a/aqg;->h:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "offlineBorderTiles"

    .line 28
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v1, v1, Lcom/google/r/b/a/aqg;->i:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "spotlightTactileStylerEnabled"

    .line 29
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v1, v1, Lcom/google/r/b/a/aqg;->o:Z

    if-nez v1, :cond_7

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v1, v3

    goto :goto_0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "spotlightSecondaryLabelEnabled"

    .line 30
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v1, v1, Lcom/google/r/b/a/aqg;->n:I

    invoke-static {v1}, Lcom/google/r/b/a/aqj;->a(I)Lcom/google/r/b/a/aqj;

    move-result-object v1

    if-nez v1, :cond_9

    sget-object v1, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    :cond_9
    sget-object v5, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    invoke-virtual {v1, v5}, Lcom/google/r/b/a/aqj;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "secondaryLabelClickEnabled"

    .line 31
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v1, v1, Lcom/google/r/b/a/aqg;->p:Z

    if-nez v1, :cond_b

    :goto_1
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v2, v3

    goto :goto_1

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "is60FpsEnabled"

    .line 32
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v1, v1, Lcom/google/r/b/a/aqg;->s:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "iconBaseUrl"

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/a/t;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 34
    invoke-virtual {v4}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
