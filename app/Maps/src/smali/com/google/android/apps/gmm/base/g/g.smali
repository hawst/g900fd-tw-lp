.class public Lcom/google/android/apps/gmm/base/g/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/apps/gmm/base/g/i;

.field public c:Lcom/google/android/apps/gmm/base/g/h;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field h:Z

.field public i:Z

.field public j:Z

.field public k:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lcom/google/b/f/cq;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Lcom/google/android/apps/gmm/z/b/l;

.field public t:Lcom/google/android/apps/gmm/base/g/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/apps/gmm/base/g/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/g/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/base/g/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/base/g/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    .line 55
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 67
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/g;->g:Z

    .line 70
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/g;->h:Z

    .line 73
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/g;->i:Z

    .line 76
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/g;->j:Z

    .line 78
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->k:Lcom/google/b/c/cv;

    .line 437
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 122
    sget v0, Lcom/google/android/apps/gmm/l;->gV:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/g/g;->l:Ljava/lang/String;

    .line 123
    sget v0, Lcom/google/android/apps/gmm/l;->pc:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/g/g;->m:Ljava/lang/String;

    .line 124
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/g/c;
    .locals 18

    .prologue
    .line 373
    new-instance v1, Lcom/google/android/apps/gmm/base/g/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/i;->a()Lcom/google/r/b/a/ads;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    .line 374
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/g/h;->d:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v3, :cond_0

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/g/h;->d:Lcom/google/android/apps/gmm/base/g/a;

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/base/g/g;->g:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/base/g/g;->h:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/base/g/g;->k:Lcom/google/b/c/cv;

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/apps/gmm/base/g/g;->i:Z

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/apps/gmm/base/g/g;->j:Z

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/base/g/g;->p:Lcom/google/b/f/cq;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/base/g/g;->q:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/base/g/g;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/g;->s:Lcom/google/android/apps/gmm/z/b/l;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    move-object/from16 v17, v0

    invoke-direct/range {v1 .. v17}, Lcom/google/android/apps/gmm/base/g/c;-><init>(Lcom/google/r/b/a/ads;Lcom/google/android/apps/gmm/base/g/a;ZZZZLcom/google/b/c/cv;ZZLjava/lang/String;Ljava/lang/String;Lcom/google/b/f/cq;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/base/g/e;)V

    return-object v1

    :cond_0
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/g/h;->b:Lcom/google/android/apps/gmm/map/g/a;

    if-nez v3, :cond_1

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/g/h;->a:Lcom/google/e/a/a/a/b;

    if-nez v3, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/google/android/apps/gmm/base/g/a;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/g/h;->b:Lcom/google/android/apps/gmm/map/g/a;

    iget-object v6, v4, Lcom/google/android/apps/gmm/base/g/h;->a:Lcom/google/e/a/a/a/b;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/base/g/h;->c:Z

    invoke-direct {v3, v5, v6, v4}, Lcom/google/android/apps/gmm/base/g/a;-><init>(Lcom/google/android/apps/gmm/map/g/a;Lcom/google/e/a/a/a/b;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/g/a;)Lcom/google/android/apps/gmm/base/g/g;
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v1, :cond_1

    .line 165
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    if-eqz p1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-nez v1, :cond_2

    .line 166
    :goto_1
    if-eqz p1, :cond_3

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/g/a;->h:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/g;->h:Z

    .line 167
    return-object p0

    .line 164
    :cond_1
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/android/apps/gmm/map/g/a;)V

    goto :goto_0

    .line 165
    :cond_2
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/g/h;->b:Lcom/google/android/apps/gmm/map/g/a;

    goto :goto_1

    .line 166
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/base/g/g;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 223
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez p1, :cond_0

    .line 224
    :goto_0
    return-object p0

    .line 223
    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/base/g/i;

    :cond_2
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v2, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-nez v2, :cond_6

    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_4
    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    :goto_5
    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/g/i;->d:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 131
    .line 132
    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v1

    .line 131
    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/r/b/a/ads;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 133
    return-object p0

    :cond_0
    move-object v0, v1

    .line 131
    goto :goto_0
.end method

.method public final b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/e/a/a/a/b;Z)Lcom/google/android/apps/gmm/base/g/i;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/g/h;->a:Lcom/google/e/a/a/a/b;

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 160
    return-object p0
.end method
