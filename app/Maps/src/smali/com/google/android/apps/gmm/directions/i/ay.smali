.class Lcom/google/android/apps/gmm/directions/i/ay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/google/android/apps/gmm/directions/h/q;
.implements Ljava/io/Serializable;


# static fields
.field static final a:[Lcom/google/android/apps/gmm/directions/i/ax;


# instance fields
.field b:I

.field private final c:Lcom/google/android/apps/gmm/directions/i/au;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 142
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/i/ax;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/directions/i/ax;->a:Lcom/google/android/apps/gmm/directions/i/ax;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/directions/i/ax;->b:Lcom/google/android/apps/gmm/directions/i/ax;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/directions/i/ax;->c:Lcom/google/android/apps/gmm/directions/i/ax;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/agt;Lcom/google/android/apps/gmm/directions/i/au;)V
    .locals 5

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    sget-object v2, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x3

    if-ge v1, v0, :cond_1

    aget-object v3, v2, v1

    .line 155
    iget-object v4, v3, Lcom/google/android/apps/gmm/directions/i/ax;->f:Lcom/google/maps/g/a/gs;

    iget v0, p1, Lcom/google/r/b/a/agt;->f:I

    invoke-static {v0}, Lcom/google/maps/g/a/gs;->a(I)Lcom/google/maps/g/a/gs;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/gs;->a:Lcom/google/maps/g/a/gs;

    :cond_0
    if-ne v4, v0, :cond_2

    .line 156
    iget v0, v3, Lcom/google/android/apps/gmm/directions/i/ax;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/directions/i/ay;->b:I

    .line 160
    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/ay;->c:Lcom/google/android/apps/gmm/directions/i/au;

    .line 161
    return-void

    .line 154
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/widget/RadioGroup$OnCheckedChangeListener;
    .locals 0

    .prologue
    .line 208
    return-object p0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 170
    if-ltz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, p1, :cond_1

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/gmm/directions/i/ay;->b:I

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/directions/i/ax;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 183
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 188
    if-ltz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, p1, :cond_2

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/i/ay;->b:I

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ay;->c:Lcom/google/android/apps/gmm/directions/i/au;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/au;->b:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/au;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 190
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 188
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/directions/i/ax;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 200
    if-ltz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 201
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 203
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/directions/i/ax;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 175
    if-ltz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 176
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ay;->a:[Lcom/google/android/apps/gmm/directions/i/ax;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/apps/gmm/directions/i/ax;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 0

    .prologue
    .line 213
    iput p2, p0, Lcom/google/android/apps/gmm/directions/i/ay;->b:I

    .line 214
    return-void
.end method
