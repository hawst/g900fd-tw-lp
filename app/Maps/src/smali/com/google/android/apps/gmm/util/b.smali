.class public Lcom/google/android/apps/gmm/util/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public c:Landroid/app/Dialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/apps/gmm/util/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 196
    :cond_0
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 197
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    .line 198
    return-void
.end method

.method public final a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/util/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 152
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    .line 153
    invoke-virtual {v1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {v0, p4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v1, p5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/util/e;

    invoke-direct {v2, p0, p6}, Lcom/google/android/apps/gmm/util/e;-><init>(Lcom/google/android/apps/gmm/util/b;Landroid/content/Intent;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->hp:I

    .line 167
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/util/d;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/gmm/util/d;-><init>(Lcom/google/android/apps/gmm/util/b;Lcom/google/android/apps/gmm/util/g;Z)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/util/c;

    invoke-direct {v2, p0, p2, p1}, Lcom/google/android/apps/gmm/util/c;-><init>(Lcom/google/android/apps/gmm/util/b;Lcom/google/android/apps/gmm/util/g;Z)V

    .line 176
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 186
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/util/b;->a(Landroid/app/Dialog;)V

    .line 187
    return-void
.end method

.method public final a(ZLcom/google/android/apps/gmm/util/g;Lcom/google/android/apps/gmm/util/f;)Z
    .locals 7
    .param p2    # Lcom/google/android/apps/gmm/util/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v1

    .line 104
    if-nez v1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    invoke-static {v1}, Lcom/google/android/gms/common/g;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    const/4 v0, 0x3

    if-ne v1, v0, :cond_2

    .line 112
    sget v3, Lcom/google/android/apps/gmm/l;->gf:I

    .line 113
    invoke-interface {p3}, Lcom/google/android/apps/gmm/util/f;->a()I

    move-result v1

    .line 114
    sget v5, Lcom/google/android/apps/gmm/l;->cu:I

    .line 115
    const-string v0, "package"

    const-string v2, "com.google.android.gms"

    const/4 v4, 0x0

    .line 116
    invoke-static {v0, v2, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 117
    new-instance v6, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v6, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 136
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;ILjava/lang/CharSequence;ILandroid/content/Intent;)V

    .line 137
    const/4 v0, 0x1

    goto :goto_0

    .line 120
    :cond_2
    const/4 v0, 0x2

    if-ne v1, v0, :cond_3

    .line 121
    sget v2, Lcom/google/android/apps/gmm/l;->ok:I

    .line 122
    invoke-interface {p3}, Lcom/google/android/apps/gmm/util/f;->b()I

    move-result v1

    .line 123
    sget v0, Lcom/google/android/apps/gmm/l;->fa:I

    .line 129
    :goto_2
    const-string v3, "http://play.google.com/store/apps/details"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "id"

    const-string v5, "com.google.android.gms"

    .line 130
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 131
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 132
    new-instance v6, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v6, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v6, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 134
    const-string v3, "com.android.vending"

    invoke-virtual {v6, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move v5, v0

    move v3, v2

    goto :goto_1

    .line 125
    :cond_3
    sget v2, Lcom/google/android/apps/gmm/l;->hu:I

    .line 126
    invoke-interface {p3}, Lcom/google/android/apps/gmm/util/f;->c()I

    move-result v1

    .line 127
    sget v0, Lcom/google/android/apps/gmm/l;->eZ:I

    goto :goto_2
.end method
