.class public Lcom/google/android/apps/gmm/iamhere/c/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Ljava/lang/String;

.field public static final b:Lcom/google/android/apps/gmm/iamhere/c/o;

.field public static final c:Lcom/google/android/apps/gmm/iamhere/c/o;

.field public static final d:Lcom/google/android/apps/gmm/iamhere/c/o;

.field public static final e:Lcom/google/android/apps/gmm/iamhere/c/o;


# instance fields
.field public final f:Lcom/google/android/apps/gmm/iamhere/c/q;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;"
        }
    .end annotation
.end field

.field public final h:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/google/o/b/a/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/google/r/b/a/amd;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final l:Z

.field public transient m:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 33
    const-class v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->a:Ljava/lang/String;

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->a:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 116
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    .line 118
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->e:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 125
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    .line 127
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->c:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 135
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    .line 137
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->d:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->g:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 145
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    .line 147
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->e:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 144
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V
    .locals 5
    .param p3    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/r/b/a/amd;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/iamhere/c/q;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/o/b/a/v;",
            "Lcom/google/r/b/a/amd;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/n/f;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 182
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne p1, v0, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 183
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 184
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    .line 185
    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    .line 186
    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    .line 187
    iput-object p5, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->k:Lcom/google/r/b/a/amd;

    .line 188
    if-nez p6, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p6, Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->j:Ljava/util/List;

    .line 190
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/p;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unhandled state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->n:Ljava/util/List;

    .line 191
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->l:Z

    .line 192
    iput-object p8, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    .line 193
    return-void

    .line 190
    :pswitch_0
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/o/b/a/v;)Ljava/util/List;
    .locals 6
    .param p0    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/o/b/a/v;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    iget v1, p0, Lcom/google/o/b/a/v;->i:F

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    iget-wide v2, p0, Lcom/google/o/b/a/v;->d:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x27

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "m "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 337
    invoke-static {v0, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_1
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    .line 479
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 480
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 481
    if-lez v0, :cond_0

    .line 482
    new-array v1, v0, [B

    .line 483
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Ljava/io/ObjectInputStream;->read([BII)I

    .line 484
    invoke-static {v1}, Lcom/google/n/f;->a([B)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    .line 488
    :goto_0
    return-void

    .line 486
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    goto :goto_0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 471
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->c()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->write([B)V

    .line 476
    :cond_0
    return-void

    .line 472
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/iamhere/c/q;)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 9

    .prologue
    .line 223
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/iamhere/c/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    :goto_1
    return-object p0

    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->k:Lcom/google/r/b/a/amd;

    iget-object v6, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->j:Ljava/util/List;

    iget-boolean v7, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->l:Z

    iget-object v8, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    move-object p0, v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/iamhere/c/o;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/x/o;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->k:Lcom/google/r/b/a/amd;

    iget-object v6, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->j:Ljava/util/List;

    iget-boolean v7, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->l:Z

    iget-object v8, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 366
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    .line 367
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/p;->b:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 395
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x11

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unhandled state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v0, v1

    .line 397
    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, v1

    .line 372
    goto :goto_0

    :pswitch_1
    move-object v0, v1

    .line 375
    goto :goto_0

    .line 376
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 390
    if-eqz v0, :cond_0

    .line 391
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    :goto_2
    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Lcom/google/android/apps/gmm/x/o;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 428
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v1, v2, :cond_1

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    .line 434
    :cond_0
    :goto_0
    return-object v0

    .line 431
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v1, v2, :cond_0

    .line 432
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 402
    instance-of v2, p1, Lcom/google/android/apps/gmm/iamhere/c/o;

    if-nez v2, :cond_1

    .line 409
    :cond_0
    :goto_0
    return v0

    .line 405
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 407
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    .line 408
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    .line 409
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v0

    .line 407
    goto :goto_1

    :cond_6
    move v2, v0

    .line 408
    goto :goto_2

    :cond_7
    move v2, v0

    .line 409
    goto :goto_3
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 414
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 493
    new-instance v2, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v1, "stateType"

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 494
    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v1, Ljava/lang/String;

    iput-object v1, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v1, "currentPlace"

    if-eqz v0, :cond_1

    .line 495
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    :goto_0
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "places"

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    .line 496
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 497
    invoke-virtual {v2}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
