.class public Lcom/google/android/apps/gmm/mylocation/c/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/google/android/apps/gmm/mylocation/c/a;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/mylocation/g/a;

.field d:Z

.field e:I

.field final f:Landroid/content/res/Resources;

.field final g:Lcom/google/android/apps/gmm/mylocation/d/e;

.field final h:Lcom/google/android/apps/gmm/v/ad;

.field final i:Lcom/google/android/apps/gmm/mylocation/c/c;

.field final j:Lcom/google/android/apps/gmm/map/t/k;

.field final k:Lcom/google/android/apps/gmm/v/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/gmm/mylocation/c/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/mylocation/d/e;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/mylocation/c/c;Lcom/google/android/apps/gmm/map/t/k;)V
    .locals 4

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->a:Ljava/lang/Object;

    .line 39
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->b:Ljava/util/ArrayDeque;

    .line 45
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->e:I

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/c/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mylocation/c/k;-><init>(Lcom/google/android/apps/gmm/mylocation/c/j;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->k:Lcom/google/android/apps/gmm/v/f;

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->f:Landroid/content/res/Resources;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->g:Lcom/google/android/apps/gmm/mylocation/d/e;

    .line 71
    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->h:Lcom/google/android/apps/gmm/v/ad;

    .line 72
    iput-object p4, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->i:Lcom/google/android/apps/gmm/mylocation/c/c;

    .line 73
    iput-object p5, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->j:Lcom/google/android/apps/gmm/map/t/k;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->k:Lcom/google/android/apps/gmm/v/f;

    iget-object v1, p3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 76
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/c/a;

    .line 92
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mylocation/c/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 93
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v5, Lcom/google/android/apps/gmm/v/af;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 97
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->c:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/c/j;->d:Z

    .line 100
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
