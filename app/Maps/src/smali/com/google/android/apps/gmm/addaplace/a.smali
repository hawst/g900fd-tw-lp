.class public Lcom/google/android/apps/gmm/addaplace/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/addaplace/c;

.field private final b:Lcom/google/android/apps/gmm/addaplace/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/android/apps/gmm/addaplace/c;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/gmm/addaplace/a;->a:Lcom/google/android/apps/gmm/addaplace/c;

    .line 44
    return-void
.end method

.method private static a(Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/maps/g/bq;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 110
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    .line 111
    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    .line 112
    iget v2, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/bs;->a:I

    iput p1, v0, Lcom/google/maps/g/bs;->b:I

    .line 113
    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v2

    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    :cond_2
    iget v3, v2, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/maps/g/bw;->a:I

    iput-object p2, v2, Lcom/google/maps/g/bw;->b:Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/maps/g/bs;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/g/bs;->a:I

    .line 114
    invoke-virtual {v0}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    .line 111
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 93
    .line 94
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->a:Ljava/lang/String;

    invoke-static {v2, v7, v0}, Lcom/google/android/apps/gmm/addaplace/a;->a(Ljava/util/ArrayList;ILjava/lang/String;)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/a/a;->b:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/addaplace/a;->a(Ljava/util/ArrayList;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    const/4 v1, 0x5

    iget v3, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/maps/g/bs;->a:I

    iput v1, v0, Lcom/google/maps/g/bs;->b:I

    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v4, v1, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v4, Lcom/google/n/ao;->d:Z

    iget v3, v1, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v1, Lcom/google/maps/g/bw;->a:I

    iget-object v3, v0, Lcom/google/maps/g/bs;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v3, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/g/bs;->a:I

    invoke-virtual {v0}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    if-eqz v0, :cond_2

    const/16 v3, 0xe

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v4, v0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    iget-object v0, v4, Lcom/google/maps/g/cc;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/gmm/addaplace/a;->a(Ljava/util/ArrayList;ILjava/lang/String;)V

    :cond_2
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/a/a;->e:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/addaplace/a;->a(Ljava/util/ArrayList;ILjava/lang/String;)V

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/a/a;->f:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/addaplace/a;->a(Ljava/util/ArrayList;ILjava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v1

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/maps/g/hu;->c:Lcom/google/maps/g/hu;

    :goto_1
    invoke-static {}, Lcom/google/r/b/a/ajp;->newBuilder()Lcom/google/r/b/a/ajr;

    move-result-object v3

    invoke-static {}, Lcom/google/maps/g/og;->newBuilder()Lcom/google/maps/g/oi;

    move-result-object v4

    invoke-static {}, Lcom/google/maps/g/eq;->newBuilder()Lcom/google/maps/g/es;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/maps/g/es;->a(Ljava/lang/Iterable;)Lcom/google/maps/g/es;

    move-result-object v2

    iget-object v5, v4, Lcom/google/maps/g/oi;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/es;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v5, Lcom/google/n/ao;->d:Z

    iget v2, v4, Lcom/google/maps/g/oi;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v4, Lcom/google/maps/g/oi;->a:I

    iget-object v2, v3, Lcom/google/r/b/a/ajr;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/maps/g/oi;->g()Lcom/google/n/t;

    move-result-object v4

    iget-object v5, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v2, Lcom/google/n/ao;->d:Z

    iget v2, v3, Lcom/google/r/b/a/ajr;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v3, Lcom/google/r/b/a/ajr;->a:I

    invoke-static {}, Lcom/google/maps/g/hn;->newBuilder()Lcom/google/maps/g/hp;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/addaplace/a;->b:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/addaplace/a/a;->g:Lcom/google/maps/g/hs;

    if-nez v4, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v1, v4, Lcom/google/maps/g/cc;->b:Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/google/maps/g/hu;->b:Lcom/google/maps/g/hu;

    goto :goto_1

    :cond_6
    iget v5, v2, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/google/maps/g/hp;->a:I

    iget v4, v4, Lcom/google/maps/g/hs;->n:I

    iput v4, v2, Lcom/google/maps/g/hp;->b:I

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v4, v2, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v2, Lcom/google/maps/g/hp;->a:I

    iget v0, v0, Lcom/google/maps/g/hu;->d:I

    iput v0, v2, Lcom/google/maps/g/hp;->d:I

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iput-object v1, v2, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    iget v0, v2, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v2, Lcom/google/maps/g/hp;->a:I

    iget-object v0, v3, Lcom/google/r/b/a/ajr;->c:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/hp;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v0, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/r/b/a/ajr;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lcom/google/r/b/a/ajr;->a:I

    invoke-virtual {v3}, Lcom/google/r/b/a/ajr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ajp;

    .line 96
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/addaplace/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/addaplace/b;-><init>(Lcom/google/android/apps/gmm/addaplace/a;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 106
    return-void
.end method
