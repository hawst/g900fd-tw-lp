.class public Lcom/google/android/apps/gmm/search/SearchFragment;
.super Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/av;
.implements Lcom/google/android/apps/gmm/search/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment",
        "<",
        "Lcom/google/android/apps/gmm/search/al;",
        ">;",
        "Lcom/google/android/apps/gmm/l/av;",
        "Lcom/google/android/apps/gmm/search/ao;"
    }
.end annotation


# instance fields
.field private A:Lcom/google/android/apps/gmm/search/l;

.field private B:Z

.field private C:Z

.field private final D:Ljava/lang/Object;

.field private w:Ljava/lang/String;

.field private x:Lcom/google/android/apps/gmm/search/an;

.field private y:Lcom/google/android/apps/gmm/map/f/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;-><init>()V

    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/search/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/t;-><init>(Lcom/google/android/apps/gmm/search/SearchFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->D:Ljava/lang/Object;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)",
            "Lcom/google/android/apps/gmm/search/SearchFragment;"
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/search/SearchFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/map/b/a/j;Z)Lcom/google/android/apps/gmm/search/SearchFragment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/map/b/a/j;Z)Lcom/google/android/apps/gmm/search/SearchFragment;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Z)",
            "Lcom/google/android/apps/gmm/search/SearchFragment;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 107
    new-instance v1, Lcom/google/android/apps/gmm/search/SearchFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/search/SearchFragment;-><init>()V

    .line 108
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 109
    const-string v3, "placeItemListProviderRef"

    invoke-virtual {p0, v2, v3, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 110
    const-string v3, "fromSearchList"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 111
    const-string v3, "searchResultViewPortMoved"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 112
    if-eqz p2, :cond_0

    .line 113
    const-string v3, "PlacemarkFeatureId"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 116
    :cond_0
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v3, :cond_1

    .line 117
    const-string v3, "searchTriggeredViaVoice"

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->h:Z

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 119
    :cond_1
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/search/SearchFragment;->setArguments(Landroid/os/Bundle;)V

    .line 120
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/search/SearchFragment;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/search/SearchFragment;IZ)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setCurrentItem(I)V

    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/search/ap;Lcom/google/android/apps/gmm/search/al;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->f()Ljava/util/List;

    move-result-object v3

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->e()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v4

    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->l()V

    .line 277
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/search/ap;->a(Ljava/util/List;)V

    .line 278
    if-eqz v4, :cond_0

    .line 279
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/search/ap;->d(Lcom/google/android/apps/gmm/base/g/c;)Z

    .line 282
    :cond_0
    iget-object v5, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v6, 0x3

    const/16 v7, 0x15

    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    if-gtz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v6, 0x1f

    const/16 v7, 0x18

    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_1
    invoke-virtual {p0, v5, v2}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/android/apps/gmm/base/placelists/a/a;Z)V

    .line 283
    if-nez v4, :cond_2

    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->b()I

    move-result v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 286
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/ap;->e(I)V

    .line 290
    :cond_2
    iput-object p0, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 291
    return-void

    :cond_3
    move v0, v1

    .line 282
    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public static b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)",
            "Lcom/google/android/apps/gmm/search/SearchFragment;"
        }
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/search/SearchFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/map/b/a/j;Z)Lcom/google/android/apps/gmm/search/SearchFragment;

    move-result-object v0

    .line 98
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 99
    return-object v0
.end method

.method private y()Z
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final J_()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 549
    :cond_0
    const/4 v0, 0x0

    .line 551
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/gmm/base/l/aj;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 556
    invoke-direct {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->y()Z

    move-result v0

    if-nez v0, :cond_2

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/al;->g()Ljava/lang/String;

    move-result-object v3

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v4, v0, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    .line 559
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_4

    .line 560
    invoke-virtual {p1, v4}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    .line 564
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->o:Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p1, Lcom/google/android/apps/gmm/base/l/b;->e:Lcom/google/android/apps/gmm/x/o;

    .line 566
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 559
    goto :goto_0

    .line 561
    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    move v0, v2

    :goto_2
    if-nez v0, :cond_1

    .line 562
    invoke-virtual {p1, v3}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move v0, v1

    .line 561
    goto :goto_2
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->B:Z

    if-eqz v0, :cond_2

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/search/v;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/v;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v1, Lcom/google/android/apps/gmm/search/SearchFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0

    .line 155
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/map/j/z;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 3

    .prologue
    .line 518
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchFragment;->a(Z)V

    .line 519
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setCurrentItem(I)V

    .line 521
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/android/apps/gmm/map/MapFragment;)V

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->k()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->n:Lcom/google/android/apps/gmm/hotels/a/g;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/hotels/a/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/hotels/a/g;)V

    .line 527
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->t:Z

    .line 532
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 308
    sget-object v0, Lcom/google/b/f/t;->fI:Lcom/google/b/f/t;

    .line 310
    :goto_1
    return-object v0

    .line 307
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 310
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->eo:Lcom/google/b/f/t;

    goto :goto_1
.end method

.method protected final i()Landroid/view/View;
    .locals 2

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchFragment;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v1, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/al;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/aq;->b(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 422
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 449
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->p:Z

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v2, Lcom/google/android/apps/gmm/search/SearchFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    move v0, v1

    .line 459
    :goto_1
    return v0

    :cond_0
    move v0, v2

    .line 449
    goto :goto_0

    .line 454
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v0, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    :cond_2
    move v0, v2

    .line 459
    goto :goto_1

    :cond_3
    move v0, v2

    .line 454
    goto :goto_2
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 3

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 470
    if-nez v1, :cond_0

    .line 471
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->a:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 478
    :goto_0
    return-object v0

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v2, :cond_2

    .line 475
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->c:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->e:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0

    .line 478
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->b:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0
.end method

.method protected final o()V
    .locals 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 511
    iget-object v1, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1f

    const/16 v3, 0x18

    invoke-virtual {v1, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/r/b/a/tf;->e:Lcom/google/r/b/a/tf;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V

    .line 514
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->onCreate(Landroid/os/Bundle;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->w:Ljava/lang/String;

    .line 194
    new-instance v0, Lcom/google/android/apps/gmm/search/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/an;-><init>(Lcom/google/android/apps/gmm/search/ao;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->x:Lcom/google/android/apps/gmm/search/an;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 197
    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->v()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->C:Z

    .line 200
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 3

    .prologue
    .line 436
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->onDetach()V

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 438
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->p:Z

    if-nez v1, :cond_0

    .line 439
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    const-string v1, "clientMeasles"

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/map/o;->c(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 441
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->onPause()V

    .line 296
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->C:Z

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->A:Lcom/google/android/apps/gmm/search/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/l;->a()V

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/k/a/a;->c()V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->D:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 303
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 204
    new-instance v0, Lcom/google/android/apps/gmm/search/s;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v3, p0}, Lcom/google/android/apps/gmm/search/s;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    .line 205
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->onResume()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 210
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 211
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v3

    sget-object v4, Lcom/google/r/b/a/zt;->c:Lcom/google/r/b/a/zt;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/gmm/k/a/a;->a(Lcom/google/r/b/a/zt;Ljava/lang/String;)V

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->B:Z

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->m()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v3

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->m()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    const/4 v4, 0x0

    .line 221
    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/gmm/b/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/b/a/t;)V

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    sget-object v3, Lcom/google/android/apps/gmm/base/placelists/a/f;->a:Lcom/google/android/apps/gmm/base/placelists/a/f;

    if-eq v0, v3, :cond_5

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->w:Ljava/lang/String;

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    .line 228
    if-eq v3, v0, :cond_2

    if-eqz v3, :cond_6

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    :goto_1
    if-nez v0, :cond_3

    .line 230
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 233
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->C:Z

    if-eqz v0, :cond_4

    .line 234
    new-instance v0, Lcom/google/android/apps/gmm/search/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/search/u;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/search/u;-><init>(Lcom/google/android/apps/gmm/search/SearchFragment;)V

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/gmm/search/l;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/search/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->A:Lcom/google/android/apps/gmm/search/l;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 257
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    .line 258
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v2

    .line 259
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->A:Lcom/google/android/apps/gmm/search/l;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {v3, v0, v4, v1, v2}, Lcom/google/android/apps/gmm/search/l;->a(Lcom/google/android/apps/gmm/search/al;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 263
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->D:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 264
    return-void

    :cond_5
    move v0, v1

    .line 228
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method protected final p()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 161
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->p()Z

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "searchResultViewPortMoved"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 166
    if-nez v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->i()Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v1

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 174
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    .line 175
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v3, :cond_3

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-eqz v0, :cond_3

    move v0, v6

    .line 176
    :goto_0
    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    .line 177
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v2, :cond_4

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v2, :cond_4

    move v0, v6

    :goto_1
    if-nez v0, :cond_1

    .line 178
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 179
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 180
    iget v0, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 181
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "searchResultViewPortMoved"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_5

    move v0, v6

    :goto_2
    return v0

    :cond_3
    move v0, v7

    .line 175
    goto :goto_0

    :cond_4
    move v0, v7

    .line 177
    goto :goto_1

    :cond_5
    move v0, v7

    .line 186
    goto :goto_2
.end method

.method protected final r()Lcom/google/android/apps/gmm/map/b/a/t;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->p()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v0

    .line 362
    return-object v0
.end method

.method protected final s()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final t()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0x13

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 315
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->t:Z

    .line 317
    new-instance v4, Lcom/google/android/apps/gmm/search/aj;

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/search/aj;-><init>(Lcom/google/android/apps/gmm/search/ai;)V

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v0

    iput v0, v4, Lcom/google/android/apps/gmm/search/aj;->c:I

    sget-object v0, Lcom/google/b/f/t;->bh:Lcom/google/b/f/t;

    .line 320
    new-instance v5, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    iput-object v0, v4, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    .line 323
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->b()V

    .line 324
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 325
    iput-object v0, v4, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    .line 326
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v4

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    .line 328
    new-instance v5, Lcom/google/android/apps/gmm/search/al;

    invoke-direct {v5, v4, v0}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->x:Lcom/google/android/apps/gmm/search/an;

    iput-object v0, v5, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v6, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v6}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/aq;->f:Lcom/google/android/apps/gmm/search/ah;

    .line 331
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ah;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 332
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v0, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {v4, v7}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    move-object v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Sent TactileSearchRequest: LoggingParams="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    return-void

    :cond_1
    move v0, v1

    .line 332
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v1

    if-eqz v4, :cond_4

    const/16 v0, 0x19

    invoke-virtual {v4, v7, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_5

    :goto_4
    check-cast v0, Lcom/google/maps/g/hy;

    goto :goto_2

    :cond_4
    move-object v0, v2

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_4
.end method

.method protected final u()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0xa

    const/16 v4, 0x1c

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    return v0

    :cond_1
    move v0, v3

    .line 392
    goto :goto_0

    .line 393
    :cond_2
    new-instance v4, Lcom/google/android/apps/gmm/search/restriction/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x19

    const/16 v5, 0x1a

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_3
    check-cast v0, Lcom/google/r/b/a/alh;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/search/restriction/b/a;-><init>(Lcom/google/r/b/a/alh;)V

    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/b/a;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/b/a;-><init>(Lcom/google/r/b/a/alh;)V

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/search/restriction/b/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v1

    if-gt v1, v2, :cond_3

    if-nez v0, :cond_5

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method protected final v()V
    .locals 1

    .prologue
    .line 492
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v()V

    .line 493
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 494
    return-void
.end method

.method protected final w()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 498
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w()V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->B:Z

    if-nez v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 502
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 503
    iget v5, v5, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 500
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V

    .line 504
    iput-object v4, p0, Lcom/google/android/apps/gmm/search/SearchFragment;->y:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 506
    :cond_0
    return-void
.end method

.method public final x()Lcom/google/android/apps/gmm/search/al;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    return-object v0
.end method
