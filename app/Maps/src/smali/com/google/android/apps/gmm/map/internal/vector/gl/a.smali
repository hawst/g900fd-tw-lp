.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final g:Ljava/lang/Long;

.field private static final i:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/vector/gl/o;

.field public final b:Lcom/google/android/apps/gmm/v/ad;

.field public final c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

.field public final d:Lcom/google/android/apps/gmm/map/internal/vector/gl/g;

.field public final e:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

.field public final f:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

.field public h:Lcom/google/android/apps/gmm/map/internal/vector/gl/r;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 59
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->g:Ljava/lang/Long;

    .line 62
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 77
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    .line 70
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/o;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/o;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/o;

    .line 93
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/g;

    invoke-direct {v0, p1, p3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/g;-><init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/g;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    .line 99
    if-eqz p2, :cond_0

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    invoke-direct {v0, p2, p0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/vector/gl/r;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->h:Lcom/google/android/apps/gmm/map/internal/vector/gl/r;

    return-object v0
.end method
