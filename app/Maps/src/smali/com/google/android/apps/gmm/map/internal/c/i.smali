.class public Lcom/google/android/apps/gmm/map/internal/c/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/apps/gmm/map/b/a/ae;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/ae;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/i;->a:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/i;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/i;

    if-eqz v2, :cond_5

    .line 50
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/i;

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/i;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/i;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_2

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/i;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/i;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_1

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 53
    :goto_2
    return v0

    :cond_2
    move v2, v1

    .line 51
    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 53
    goto :goto_2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/i;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/i;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
