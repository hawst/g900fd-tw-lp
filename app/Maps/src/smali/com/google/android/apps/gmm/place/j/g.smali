.class public Lcom/google/android/apps/gmm/place/j/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;
.implements Lcom/google/android/apps/gmm/place/br;


# instance fields
.field public a:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/apps/gmm/z/b/l;

.field private f:Landroid/text/SpannableStringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->f:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_1

    .line 73
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 46
    goto :goto_0

    .line 50
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/g;->b:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/g;->a:Lcom/google/android/apps/gmm/x/o;

    .line 52
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 55
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v1

    .line 56
    iget-object v4, v1, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x9

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/place/j/g;->c:Ljava/lang/String;

    .line 57
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/hotels/a/c;->a()Ljava/lang/String;

    move-result-object v4

    .line 58
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    move v1, v3

    :goto_2
    if-nez v1, :cond_4

    .line 59
    sget v1, Lcom/google/android/apps/gmm/l;->gY:I

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-virtual {p1, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/j/g;->d:Ljava/lang/String;

    .line 64
    :goto_3
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    new-array v3, v3, [Lcom/google/b/f/cq;

    sget-object v4, Lcom/google/b/f/t;->db:Lcom/google/b/f/t;

    aput-object v4, v3, v2

    .line 65
    iput-object v3, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 67
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 69
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->f:Landroid/text/SpannableStringBuilder;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->f:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/util/a;->a(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;Z)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->f:Landroid/text/SpannableStringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->f:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    :cond_3
    move v1, v2

    .line 58
    goto :goto_2

    .line 61
    :cond_4
    sget v1, Lcom/google/android/apps/gmm/l;->gW:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/j/g;->d:Ljava/lang/String;

    goto :goto_3
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/g;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/hotels/a/b;->a(Lcom/google/android/apps/gmm/x/o;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 98
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/g;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 87
    sget v0, Lcom/google/android/apps/gmm/f;->dr:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method
