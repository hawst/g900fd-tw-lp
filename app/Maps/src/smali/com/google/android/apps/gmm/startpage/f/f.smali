.class public Lcom/google/android/apps/gmm/startpage/f/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/d;


# instance fields
.field a:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/x/o;Z)V
    .locals 0
    .param p2    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 32
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/startpage/f/f;->c:Z

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 53
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-object v5

    .line 56
    :cond_0
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 57
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/j/d/a/e;)V

    .line 59
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v3, Lcom/google/j/d/a/w;->b:Lcom/google/j/d/a/w;

    sget-object v4, Lcom/google/b/f/t;->cQ:Lcom/google/b/f/t;

    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 62
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/f;->a:Lcom/google/android/apps/gmm/x/o;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/j/d/a/e;)V

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 102
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/f/f;->c:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/f/t;->cM:Lcom/google/b/f/t;

    :goto_0
    aput-object v0, v2, v3

    .line 103
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 106
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 102
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->cQ:Lcom/google/b/f/t;

    goto :goto_0
.end method
