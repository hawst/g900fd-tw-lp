.class public Lcom/google/android/apps/gmm/map/t/n;
.super Lcom/google/android/apps/gmm/v/bh;
.source "PG"


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/v/bh;

.field final b:I

.field final c:Z

.field d:Lcom/google/android/apps/gmm/map/f/o;

.field e:Lcom/google/android/apps/gmm/map/t/a;

.field private final i:[Lcom/google/android/apps/gmm/map/t/k;

.field private volatile j:Ljava/lang/Long;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/t/n;-><init>(IZ)V

    .line 82
    return-void
.end method

.method constructor <init>(IZ)V
    .locals 4

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bh;-><init>(I)V

    .line 73
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->j:Ljava/lang/Long;

    .line 94
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/t/n;->c:Z

    .line 95
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/n;->g:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/t/i;->a(I)[Lcom/google/android/apps/gmm/map/t/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/n;->g:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x3f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GmmDrawOrderRenderBin cannot be used for pass index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    array-length v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/n;->b:I

    .line 102
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/n;->b:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/bh;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    .line 105
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    new-instance v2, Lcom/google/android/apps/gmm/map/t/au;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/map/t/au;-><init>(I)V

    aput-object v2, v1, v0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/k;ZLcom/google/android/apps/gmm/v/ad;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 222
    monitor-enter p0

    .line 223
    if-eqz p2, :cond_0

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v2

    shl-int v2, v4, v2

    xor-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->j:Ljava/lang/Long;

    .line 228
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    new-instance v0, Lcom/google/android/apps/gmm/map/t/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/t/o;-><init>(Lcom/google/android/apps/gmm/map/t/n;)V

    iget-object v1, p3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 230
    return-void

    .line 226
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v2

    shl-int v2, v4, v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->j:Ljava/lang/Long;

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 2

    .prologue
    .line 156
    check-cast p1, Lcom/google/android/apps/gmm/map/t/p;

    .line 157
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/t/k;->b()I

    move-result v0

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/aa;)V

    .line 159
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 171
    monitor-enter p0

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/n;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 173
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 174
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v6, v2, v0

    .line 176
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/v/bh;->a()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v2

    shl-int v2, v3, v2

    int-to-long v8, v2

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    if-eqz v2, :cond_1

    .line 177
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/t/k;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->e:Lcom/google/android/apps/gmm/map/t/a;

    .line 181
    :goto_2
    if-eqz v2, :cond_1

    .line 182
    iget-boolean v7, p0, Lcom/google/android/apps/gmm/map/t/n;->c:Z

    if-eqz v7, :cond_0

    .line 185
    iget-object v7, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v8, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/gmm/v/bi;->b(Lcom/google/android/apps/gmm/v/aq;)V

    .line 188
    :cond_0
    invoke-virtual {v6, p1}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/ad;)V

    .line 190
    iget-boolean v6, p0, Lcom/google/android/apps/gmm/map/t/n;->c:Z

    if-eqz v6, :cond_1

    .line 191
    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 174
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    .line 176
    goto :goto_1

    .line 177
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->d:Lcom/google/android/apps/gmm/map/f/o;

    goto :goto_2

    .line 194
    :cond_4
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/n;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 124
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/f/o;

    if-eqz v1, :cond_2

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->d:Lcom/google/android/apps/gmm/map/f/o;

    if-eqz v1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to set GmmCamera more than once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/f/o;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/n;->d:Lcom/google/android/apps/gmm/map/f/o;

    .line 129
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 130
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/t/k;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->d:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/n;)V

    .line 129
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_2
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/t/a;

    if-eqz v1, :cond_5

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->e:Lcom/google/android/apps/gmm/map/t/a;

    if-eqz v1, :cond_3

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to set Camera2D more than once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/t/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/n;->e:Lcom/google/android/apps/gmm/map/t/a;

    .line 139
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->i:[Lcom/google/android/apps/gmm/map/t/k;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/t/k;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->e:Lcom/google/android/apps/gmm/map/t/a;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/n;)V

    .line 139
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 145
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown camera type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_6
    return-void
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 248
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 249
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bh;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 253
    :goto_1
    return v0

    .line 248
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 253
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 208
    invoke-virtual {v4, p1}, Lcom/google/android/apps/gmm/v/bh;->b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_0
    return-object v1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 2

    .prologue
    .line 163
    check-cast p1, Lcom/google/android/apps/gmm/map/t/p;

    .line 164
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/p;->c:Lcom/google/android/apps/gmm/map/t/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/t/k;->b()I

    move-result v0

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/v/bh;->b(Lcom/google/android/apps/gmm/v/aa;)V

    .line 166
    return-void
.end method
