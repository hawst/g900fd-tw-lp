.class public Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;
.super Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;
.source "PG"


# instance fields
.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Lcom/google/android/apps/gmm/directions/views/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public k:Lcom/google/android/apps/gmm/directions/views/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:F

.field private final m:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/e;->ag:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->l:F

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/e;->af:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->m:F

    .line 44
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v6, v0, v3

    .line 124
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->f:I

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->j:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v0, :cond_3

    .line 126
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->l:F

    sub-float v0, v6, v0

    .line 128
    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->b:F

    mul-float/2addr v1, v3

    sub-float v3, v0, v1

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->j:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->j:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/directions/views/a;->c(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)F

    move-result v4

    .line 131
    iget v5, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->f:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->a(Landroid/graphics/Canvas;FFFI)V

    .line 136
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->g:I

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->k:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v0, :cond_4

    .line 138
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->l:F

    add-float v2, v6, v0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->getHeight()I

    move-result v0

    int-to-float v3, v0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->k:Lcom/google/android/apps/gmm/directions/views/a;

    sub-float v1, v3, v2

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->k:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/directions/views/a;->c(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)F

    move-result v4

    .line 142
    iget v5, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->g:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->a(Landroid/graphics/Canvas;FFFI)V

    .line 147
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->h:I

    if-eqz v0, :cond_2

    .line 148
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->l:F

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->h:I

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->b(Landroid/graphics/Canvas;FFI)V

    .line 149
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->m:F

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->i:I

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->b(Landroid/graphics/Canvas;FFI)V

    .line 151
    :cond_2
    return-void

    .line 133
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->f:I

    invoke-virtual {p0, p1, v2, v6, v0}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->a(Landroid/graphics/Canvas;FFI)V

    goto :goto_0

    .line 144
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->g:I

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->a(Landroid/graphics/Canvas;FFI)V

    goto :goto_1
.end method
