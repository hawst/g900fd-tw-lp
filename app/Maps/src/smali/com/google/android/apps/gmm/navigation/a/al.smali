.class public Lcom/google/android/apps/gmm/navigation/a/al;
.super Lcom/google/android/apps/gmm/navigation/a/a;
.source "PG"


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field c:Lcom/google/android/apps/gmm/navigation/a/ak;

.field final d:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private e:Z

.field private f:Lcom/google/android/apps/gmm/navigation/a/c;

.field private g:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/al;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/a;-><init>()V

    .line 121
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    .line 123
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/al;->f:Lcom/google/android/apps/gmm/navigation/a/c;

    .line 124
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/al;->d:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 126
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->n:Z

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/ak;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/a/ak;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->c:Lcom/google/android/apps/gmm/navigation/a/ak;

    .line 129
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->e:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/al;->f()V

    .line 130
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;ILcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 51
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v7

    .line 55
    if-nez v7, :cond_1

    move-object v0, v6

    .line 74
    :goto_1
    return-object v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_1
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    .line 60
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 62
    :try_start_0
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 63
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 64
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/al;

    invoke-direct {v1, p0, v0, p2, p3}, Lcom/google/android/apps/gmm/navigation/a/al;-><init>(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)V

    .line 65
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/a/a;->a()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-object v0, v1

    .line 66
    goto :goto_1

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const-string v1, "Error loading sound file from resource"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    move-object v0, v6

    .line 74
    goto :goto_1

    .line 69
    :catch_1
    move-exception v0

    .line 70
    const-string v1, "Error loading sound file from resource"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 71
    :catch_2
    move-exception v0

    .line 72
    const-string v1, "Error loading sound file from resource"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/io/File;Lcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 89
    :try_start_0
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 90
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    .line 91
    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 93
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/al;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "MediaAlert file doesn\'t exist"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    :goto_1
    return-object v0

    .line 89
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_2

    .line 98
    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/al;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "MediaAlert file doesn\'t have read permissions"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 112
    :catch_0
    move-exception v1

    .line 113
    const-string v2, "Exception creating MediaAlert from file"

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 105
    :cond_2
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 106
    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    .line 107
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 108
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 109
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/al;

    invoke-direct {v1, p0, v2, p2, p3}, Lcom/google/android/apps/gmm/navigation/a/al;-><init>(Landroid/content/Context;Landroid/media/MediaPlayer;Lcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)V

    .line 110
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/a/a;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    .line 111
    goto :goto_1
.end method

.method private f()V
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 238
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->c:Lcom/google/android/apps/gmm/navigation/a/ak;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->c:Lcom/google/android/apps/gmm/navigation/a/ak;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/al;->f:Lcom/google/android/apps/gmm/navigation/a/c;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/a/c;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/a/ak;->a(II)V

    .line 241
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->a:Z

    if-nez v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->a:Z

    .line 150
    :cond_1
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 141
    const-string v1, "Exception while preparing MediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 142
    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    const-string v1, "Exception while preparing MediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 145
    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/b;)V
    .locals 2

    .prologue
    .line 154
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 155
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 156
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/navigation/a/b;->a(Lcom/google/android/apps/gmm/navigation/a/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :goto_0
    monitor-exit p0

    return-void

    .line 159
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/am;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/navigation/a/am;-><init>(Lcom/google/android/apps/gmm/navigation/a/al;Lcom/google/android/apps/gmm/navigation/a/b;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/al;->f()V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 174
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/al;->b:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/c;)V
    .locals 3

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/al;->f:Lcom/google/android/apps/gmm/navigation/a/c;

    .line 201
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->e:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->c:Lcom/google/android/apps/gmm/navigation/a/ak;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->c:Lcom/google/android/apps/gmm/navigation/a/ak;

    iget v1, p1, Lcom/google/android/apps/gmm/navigation/a/c;->d:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Landroid/media/audiofx/LoudnessEnhancer;->setTargetGain(I)V

    .line 206
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->e:Z

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 183
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->e:Z

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/al;->f()V

    .line 191
    :cond_0
    return-void
.end method

.method declared-synchronized e()V
    .locals 1

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/al;->g:Landroid/media/MediaPlayer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    monitor-exit p0

    return-void

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
