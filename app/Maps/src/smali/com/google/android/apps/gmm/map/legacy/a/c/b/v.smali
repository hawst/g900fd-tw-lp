.class public final enum Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

.field public static final enum b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

.field public static final enum c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->d:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;
    .locals 2

    .prologue
    .line 46
    packed-switch p0, :pswitch_data_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown alignment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    .line 49
    :goto_0
    return-object v0

    .line 48
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    goto :goto_0

    .line 49
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->d:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    return-object v0
.end method
