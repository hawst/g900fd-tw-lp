.class public Lcom/google/android/apps/gmm/cardui/c/c;
.super Lcom/google/android/apps/gmm/cardui/c/a;
.source "PG"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/cardui/c/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/c/c;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/startpage/d/b;Lcom/google/o/h/a/br;)V
    .locals 6

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/cardui/c/a;-><init>(Lcom/google/android/apps/gmm/startpage/d/b;Lcom/google/o/h/a/br;)V

    .line 41
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/c;

    iget v2, v0, Lcom/google/o/h/a/lh;->c:I

    iget-wide v4, v0, Lcom/google/o/h/a/lh;->d:J

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/apps/gmm/startpage/d/c;-><init>(IJ)V

    iget v0, v1, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/cardui/c/c;->g:I

    .line 42
    iget v0, p0, Lcom/google/android/apps/gmm/cardui/c/c;->g:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/cardui/c/c;->g:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Lcom/google/android/apps/gmm/cardui/c/c;->g:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "moduleType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 121
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 122
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 124
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/m;

    .line 125
    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/m;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v3, :cond_1

    .line 127
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/m;->a:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/iv;

    .line 131
    iget-object v2, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/ir;

    iget v2, v2, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_1
    if-eqz v2, :cond_0

    .line 132
    iget-object v1, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ir;

    iget-object v1, v1, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/gp;

    .line 135
    iget-object v2, v1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    iget-object v1, v1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v1, v4}, Lcom/google/n/ay;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 139
    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 140
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v2, v4

    .line 131
    goto :goto_1

    .line 145
    :cond_3
    return-object v5
.end method

.method private static a(Ljava/util/List;Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)Ljava/util/List;
    .locals 5
    .param p3    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;",
            "Lcom/google/o/h/a/br;",
            "Ljava/lang/String;",
            "Lcom/google/r/b/a/tf;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/apps/gmm/cardui/c/c;->f:Ljava/lang/String;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cached historyItemList.size()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 103
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    .line 104
    new-instance v3, Lcom/google/android/apps/gmm/util/b/m;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4, p3}, Lcom/google/android/apps/gmm/util/b/m;-><init>(Lcom/google/o/h/a/iv;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {p1}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    .line 109
    new-instance v3, Lcom/google/android/apps/gmm/util/b/m;

    invoke-direct {v3, v0, p2, p3}, Lcom/google/android/apps/gmm/util/b/m;-><init>(Lcom/google/o/h/a/iv;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/c/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/startpage/a/c;)Ljava/util/List;
    .locals 12
    .param p1    # Lcom/google/android/apps/gmm/startpage/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/startpage/a/c;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    if-nez p1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 51
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 55
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/o/h/a/iv;

    .line 56
    iget v0, p0, Lcom/google/android/apps/gmm/cardui/c/c;->g:I

    if-ne v0, v4, :cond_1

    sget-object v0, Lcom/google/b/f/t;->fV:Lcom/google/b/f/t;

    .line 57
    :goto_2
    invoke-static {v3, v2, v0}, Lcom/google/android/apps/gmm/cardui/e/a;->a(IILcom/google/b/f/t;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v0, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_2

    move v0, v4

    :goto_3
    if-nez v0, :cond_3

    const-string v0, "CardUiUtil"

    const-string v7, "setClientGeneratedVed is called though the data is not GenericItemData"

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    :goto_4
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 63
    goto :goto_1

    .line 50
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/cardui/c/c;->g:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/a;->b:Lcom/google/o/h/a/lh;

    iget-wide v0, v0, Lcom/google/o/h/a/lh;->k:J

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/startpage/a/c;->a(J)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/a;->b:Lcom/google/o/h/a/lh;

    iget-wide v0, v0, Lcom/google/o/h/a/lh;->k:J

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/startpage/a/c;->b(J)Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->fS:Lcom/google/b/f/t;

    goto :goto_2

    :cond_2
    move v0, v3

    .line 57
    goto :goto_3

    :cond_3
    iget-object v0, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v8, 0x10

    if-ne v0, v8, :cond_5

    move v0, v4

    :goto_5
    if-eqz v0, :cond_4

    const-string v0, "CardUiUtil"

    const-string v8, "setClientGeneratedVed is called though the data already have GenericItemData"

    new-array v9, v3, [Ljava/lang/Object;

    invoke-static {v0, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v8

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v9

    iget-object v0, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/gr;

    move-result-object v0

    if-nez v7, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v3

    goto :goto_5

    :cond_6
    iget v10, v0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v10, v10, 0x200

    iput v10, v0, Lcom/google/o/h/a/gr;->a:I

    iput-object v7, v0, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-virtual {v9, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/it;

    move-result-object v0

    iget-object v7, v8, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v7, Lcom/google/n/ao;->d:Z

    iget v0, v8, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v8, Lcom/google/o/h/a/ix;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/iv;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jf;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    invoke-virtual {v8}, Lcom/google/o/h/a/ix;->c()V

    iget-object v7, v8, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    new-instance v9, Lcom/google/n/ao;

    invoke-direct {v9}, Lcom/google/n/ao;-><init>()V

    iget-object v10, v9, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v9, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v9, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v9, Lcom/google/n/ao;->d:Z

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_8
    invoke-virtual {v8}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    move-object v1, v0

    goto/16 :goto_4

    .line 65
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/a;->a:Lcom/google/o/h/a/br;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c/a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/c/a;->d:Lcom/google/r/b/a/tf;

    invoke-static {v5, v0, v1, v2}, Lcom/google/android/apps/gmm/cardui/c/c;->a(Ljava/util/List;Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
