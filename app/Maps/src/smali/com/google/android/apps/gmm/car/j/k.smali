.class public Lcom/google/android/apps/gmm/car/j/k;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/car/j/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/car/j/h;Lcom/google/android/apps/gmm/base/a;)V
    .locals 4

    .prologue
    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    invoke-static {p1}, Lcom/google/android/apps/gmm/car/j/k;->b(Lcom/google/android/apps/gmm/base/a;)Ljava/io/File;

    move-result-object v1

    .line 59
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 62
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    .line 70
    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/car/j/k;->a(Lcom/google/android/apps/gmm/car/j/h;Ljava/util/ArrayList;)V

    .line 71
    return-void

    .line 65
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/car/j/h;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 79
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 80
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    invoke-static {p0, v1}, Lcom/google/android/apps/gmm/car/j/k;->a(Lcom/google/android/apps/gmm/car/j/h;Ljava/util/ArrayList;)V

    .line 84
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/car/j/h;Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/car/j/h;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    const/16 v0, 0x3c

    .line 89
    const-string v1, "^(\\w*)$"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 90
    const-string v1, "^(\\w*)=(.*)$"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 91
    const-string v1, "^(\\w*),(.*)$"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 92
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    const-string v2, "#.*$"

    const-string v3, ""

    .line 94
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\s*$"

    const-string v4, ""

    .line 95
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "^\\s*"

    const-string v4, ""

    .line 96
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\s*=\\s*"

    const-string v4, "="

    .line 97
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 99
    const/4 v2, 0x0

    .line 100
    const/4 v3, 0x0

    .line 102
    invoke-virtual {v6, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 103
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 104
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    const/4 v3, 0x2

    invoke-virtual {v9, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 113
    :cond_1
    :goto_1
    if-eqz v2, :cond_0

    .line 114
    const/4 v4, 0x1

    .line 118
    if-eqz v3, :cond_1a

    .line 119
    const-string v9, "GMM_SHOWCASE_CATEGORY_SEARCH"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 121
    const/4 v2, 0x0

    .line 124
    const-string v9, "\\s*,\\s*"

    const-string v10, ","

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 125
    invoke-virtual {v7, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 126
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 127
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 128
    const/4 v2, 0x2

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 131
    :cond_2
    const-string v9, "GAS"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 132
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->a:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    .line 212
    :goto_2
    if-nez v2, :cond_0

    .line 214
    sget-object v2, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Note: no match for line \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 107
    :cond_3
    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 108
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 109
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 134
    :cond_4
    const-string v9, "PARKING"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 135
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->b:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto :goto_2

    .line 137
    :cond_5
    const-string v9, "RESTAURANT"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 138
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->c:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto :goto_2

    .line 140
    :cond_6
    const-string v9, "CAFE"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 141
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->d:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto :goto_2

    .line 143
    :cond_7
    const-string v9, "GROCERIES"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 144
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->f:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_2

    .line 146
    :cond_8
    const-string v9, "EMERGENCY"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 147
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->h:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_2

    .line 149
    :cond_9
    const-string v9, "FASTFOOD"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 150
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->e:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_2

    .line 152
    :cond_a
    const-string v9, "ATM"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 153
    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v9, Lcom/google/android/apps/gmm/car/drawer/g;->g:I

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_2

    .line 156
    :cond_b
    const/4 v2, 0x0

    .line 158
    goto/16 :goto_2

    :cond_c
    const-string v9, "GMM_SHOWCASE_LOCATION"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 160
    :try_start_0
    const-string v2, ",\\s*"

    invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 161
    array-length v9, v2

    const/4 v10, 0x2

    if-ne v9, v10, :cond_d

    .line 162
    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v10, 0x0

    aget-object v10, v2, v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    const/4 v12, 0x1

    aget-object v2, v2, v12

    .line 163
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 162
    iput-object v9, p0, Lcom/google/android/apps/gmm/car/j/h;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    move v2, v4

    goto/16 :goto_2

    .line 165
    :cond_d
    sget-object v0, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x34

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Malformed coordinates ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "): expected numeric lat, long"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 169
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    const-string v0, "Malformed coordinates: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_e
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 172
    :cond_f
    const-string v9, "GMM_SHOWCASE_SPEED_UP_FACTOR"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 174
    :try_start_1
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    iput-wide v10, p0, Lcom/google/android/apps/gmm/car/j/h;->f:D
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move v2, v4

    .line 178
    goto/16 :goto_2

    .line 176
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    const-string v0, "Malformed speedUpFactor: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_10
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 179
    :cond_11
    const-string v9, "GMM_SHOWCASE_DESTINATION"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 180
    const/4 v2, 0x0

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Ljava/lang/String;Z)V

    move v2, v4

    goto/16 :goto_2

    .line 181
    :cond_12
    const-string v9, "GMM_SHOWCASE_DESTINATION_ALTERNATE_ROUTE"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 182
    const/4 v2, 0x1

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/gmm/car/j/h;->a(Ljava/lang/String;Z)V

    move v2, v4

    goto/16 :goto_2

    .line 183
    :cond_13
    const-string v9, "GMM_SHOWCASE_ENABLE_DEBUG"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 184
    const-string v2, "true"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 185
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/j/h;->g:Z

    move v2, v4

    goto/16 :goto_2

    .line 186
    :cond_14
    const-string v2, "false"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 187
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/j/h;->g:Z

    move v2, v4

    goto/16 :goto_2

    .line 189
    :cond_15
    sget-object v0, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    const-string v0, "Malformed enableDebug: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_16

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_16
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 192
    :cond_17
    const-string v9, "GMM_SHOWCASE_PAUSE_AFTER_USER_INTERACTION"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 194
    :try_start_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v1

    move v2, v4

    .line 198
    goto/16 :goto_2

    .line 196
    :catch_2
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    const-string v0, "Malformed pauseAfterUserInteraction: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_18

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_18
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 203
    :cond_1a
    const-string v3, "GMM_SHOWCASE_TOGGLE_TRAFFIC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    new-instance v9, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v10, Lcom/google/android/apps/gmm/car/d/k;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v9, v10}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    iget-object v10, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v11, Lcom/google/android/apps/gmm/car/j/q;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v11, v9, v12, v13}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x2

    iget-object v10, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v11, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v11, v9}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v9, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v10, Lcom/google/android/apps/gmm/car/j/p;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/car/j/p;-><init>()V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x2

    iget-object v10, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v11, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v11, v9}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/google/android/apps/gmm/car/j/y;

    sget v10, Lcom/google/android/apps/gmm/car/drawer/t;->c:I

    invoke-direct {v9, v10}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    iget-object v10, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v11, Lcom/google/android/apps/gmm/car/j/q;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v11, v9, v12, v13}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x2

    iget-object v10, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v11, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v11, v9}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto/16 :goto_2

    .line 205
    :cond_1b
    const-string v3, "GMM_SHOWCASE_TOGGLE_DAY_NIGHT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 206
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    iget-object v9, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v10, Lcom/google/android/apps/gmm/car/j/v;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/car/j/v;-><init>()V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x2

    iget-object v10, v3, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v11, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v11, v9}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto/16 :goto_2

    .line 208
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 218
    :cond_1d
    new-instance v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    .line 220
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/t;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/car/j/t;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/o;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/car/j/o;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x2

    .line 223
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1e
    check-cast v0, Lcom/google/android/apps/gmm/car/j/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/h;->c:Lcom/google/android/apps/gmm/car/j/l;

    .line 225
    new-instance v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    const/4 v1, 0x1

    .line 227
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/t;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/car/j/t;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/o;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/car/j/o;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    .line 230
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1f
    check-cast v0, Lcom/google/android/apps/gmm/car/j/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/h;->d:Lcom/google/android/apps/gmm/car/j/l;

    .line 231
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/a;)Z
    .locals 1

    .prologue
    .line 234
    invoke-static {p0}, Lcom/google/android/apps/gmm/car/j/k;->b(Lcom/google/android/apps/gmm/base/a;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/google/android/apps/gmm/base/a;)Ljava/io/File;
    .locals 5

    .prologue
    .line 238
    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "showcase_config.txt"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 240
    sget-object v1, Lcom/google/android/apps/gmm/car/j/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x13

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "file="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", exists="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 241
    return-object v0
.end method
