.class public Lcom/google/android/apps/gmm/map/f/l;
.super Lcom/google/android/apps/gmm/map/f/t;
.source "PG"


# static fields
.field static final b:Landroid/view/animation/Interpolator;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/f/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/l;->b:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/map/f/l;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;Lcom/google/android/apps/gmm/map/f/m;)V

    .line 41
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;Lcom/google/android/apps/gmm/map/f/m;)V
    .locals 2
    .param p4    # Lcom/google/android/apps/gmm/map/f/m;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 47
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/f/l;->a:Lcom/google/android/apps/gmm/map/f/m;

    .line 50
    return-void

    .line 47
    :cond_0
    new-instance p4, Lcom/google/android/apps/gmm/map/f/m;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/y;-><init>()V

    sget-object v1, Lcom/google/android/apps/gmm/map/f/l;->b:Landroid/view/animation/Interpolator;

    invoke-direct {p4, v0, v1, p2, p3}, Lcom/google/android/apps/gmm/map/f/m;-><init>(Lcom/google/android/apps/gmm/map/f/y;Landroid/view/animation/Interpolator;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 87
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v1, v0, v1

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 18
    .param p1    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 60
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/l;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v5

    .line 61
    :try_start_0
    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    move-result v6

    .line 64
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/f/l;->a:Lcom/google/android/apps/gmm/map/f/m;

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->b:Lcom/google/android/apps/gmm/map/f/f;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->h:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->b:Lcom/google/android/apps/gmm/map/f/f;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->i:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget v2, v2, Lcom/google/android/apps/gmm/v/n;->K:F

    iput v2, v7, Lcom/google/android/apps/gmm/map/f/m;->f:F

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    iput v2, v7, Lcom/google/android/apps/gmm/map/f/m;->g:I

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p2

    iget v8, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v4, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-virtual {v7, v4}, Lcom/google/android/apps/gmm/map/f/m;->a(F)F

    move-result v4

    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-virtual {v7, v9}, Lcom/google/android/apps/gmm/map/f/m;->a(F)F

    move-result v9

    invoke-static {v4, v9}, Ljava/lang/Math;->min(FF)F

    move-result v10

    iput v10, v7, Lcom/google/android/apps/gmm/map/f/m;->l:F

    iget-object v10, v7, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget v11, v7, Lcom/google/android/apps/gmm/map/f/m;->g:I

    int-to-float v11, v11

    invoke-static {v11, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/4 v11, 0x0

    invoke-virtual {v10, v2, v11}, Lcom/google/android/apps/gmm/map/f/o;->a(FF)F

    move-result v2

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v2

    const/high16 v2, 0x4e800000

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    float-to-double v14, v3

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    double-to-float v11, v12

    div-float/2addr v2, v11

    iget-object v11, v7, Lcom/google/android/apps/gmm/map/f/m;->h:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v11, v11, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v12, v7, Lcom/google/android/apps/gmm/map/f/m;->i:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v12, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->j(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v11

    div-float/2addr v11, v2

    const/4 v2, 0x0

    cmpl-float v12, v11, v10

    if-lez v12, :cond_0

    invoke-static {v11}, Lcom/google/android/apps/gmm/shared/c/s;->d(F)F

    move-result v2

    invoke-static {v10}, Lcom/google/android/apps/gmm/shared/c/s;->d(F)F

    move-result v12

    sub-float/2addr v2, v12

    :cond_0
    iget-object v12, v7, Lcom/google/android/apps/gmm/map/f/m;->b:Lcom/google/android/apps/gmm/map/f/f;

    sub-float v2, v3, v2

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v12, v2, v13}, Lcom/google/android/apps/gmm/map/f/f;->a(FLcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-virtual {v7, v12}, Lcom/google/android/apps/gmm/map/f/m;->a(F)F

    move-result v2

    iget v3, v7, Lcom/google/android/apps/gmm/map/f/m;->l:F

    sub-float v3, v2, v3

    iput v3, v7, Lcom/google/android/apps/gmm/map/f/m;->k:F

    iget-object v13, v7, Lcom/google/android/apps/gmm/map/f/m;->d:Lcom/google/android/apps/gmm/map/f/y;

    sub-float v3, v2, v4

    const v4, 0x49742400    # 1000000.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v9

    const v4, 0x49742400    # 1000000.0f

    div-float/2addr v2, v4

    const/4 v4, 0x0

    cmpg-float v4, v4, v3

    if-gtz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    const-string v9, "startValue of %s less than 0"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v14, v15

    if-nez v4, :cond_2

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-static {v9, v14}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 83
    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 64
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    cmpg-float v4, v4, v2

    if-gtz v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    :try_start_1
    const-string v9, "endValue of %s less than 0"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v14, v15

    if-nez v4, :cond_4

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-static {v9, v14}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    :cond_4
    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    const/high16 v9, 0x40800000    # 4.0f

    cmpl-float v9, v4, v9

    if-lez v9, :cond_5

    const/high16 v9, 0x40800000    # 4.0f

    mul-float/2addr v3, v9

    div-float/2addr v3, v4

    const/high16 v9, 0x40800000    # 4.0f

    mul-float/2addr v2, v9

    div-float/2addr v2, v4

    :cond_5
    float-to-double v14, v3

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    neg-double v14, v14

    double-to-float v4, v14

    iput v4, v13, Lcom/google/android/apps/gmm/map/f/y;->a:F

    float-to-double v14, v2

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    double-to-float v4, v14

    iput v4, v13, Lcom/google/android/apps/gmm/map/f/y;->b:F

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v13, Lcom/google/android/apps/gmm/map/f/y;->c:F

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v13, v2}, Lcom/google/android/apps/gmm/map/f/y;->a(F)F

    move-result v2

    iput v2, v13, Lcom/google/android/apps/gmm/map/f/y;->d:F

    sub-float v2, v8, v12

    const-wide/16 v8, 0x320

    const-wide/16 v12, 0x7d0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v4, v11

    div-float/2addr v4, v10

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v2, v10

    const/high16 v10, 0x40800000    # 4.0f

    div-float/2addr v2, v10

    add-float/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v8, v9, v12, v13, v2}, Lcom/google/android/apps/gmm/map/f/m;->a(JJF)J

    move-result-wide v2

    iput-wide v2, v7, Lcom/google/android/apps/gmm/map/f/m;->j:J

    .line 65
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/l;->a:Lcom/google/android/apps/gmm/map/f/m;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/f/m;->j:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 66
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/l;->h:Lcom/google/android/apps/gmm/map/f/w;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/l;->a:Lcom/google/android/apps/gmm/map/f/m;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/w;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 67
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/l;->h:Lcom/google/android/apps/gmm/map/f/w;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/l;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/w;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/l;->g:Lcom/google/android/apps/gmm/map/f/w;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/l;->a:Lcom/google/android/apps/gmm/map/f/m;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/f/m;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/w;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/l;->j:Lcom/google/android/apps/gmm/map/f/w;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/l;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/w;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/l;->i:Lcom/google/android/apps/gmm/map/f/w;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/l;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/w;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 76
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 77
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 78
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 79
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 80
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/l;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 82
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v6
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method
