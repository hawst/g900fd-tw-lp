.class public Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;
.super Lcom/google/android/apps/gmm/streetview/StreetViewFragment;
.source "PG"


# instance fields
.field a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

.field b:Landroid/widget/ImageView;

.field c:Lcom/google/android/apps/gmm/util/h;

.field d:Lcom/google/android/apps/gmm/util/h;

.field e:Z

.field f:Lcom/google/android/apps/gmm/streetview/p;

.field private g:Landroid/view/ViewGroup;

.field private m:Landroid/widget/ImageView;

.field private n:Lcom/google/android/apps/gmm/base/views/HeaderView;

.field private final o:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;-><init>()V

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/streetview/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/i;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->o:Ljava/lang/Object;

    return-void
.end method

.method private c()Lcom/google/android/apps/gmm/p/b/g;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 225
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return-object v0
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->e:Z

    if-ne p1, v0, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    .line 195
    :goto_1
    if-eqz v0, :cond_0

    .line 199
    if-eqz p1, :cond_3

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->o:Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/p/b/h;)V

    .line 205
    :goto_2
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->e:Z

    goto :goto_0

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 203
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method b()V
    .locals 3

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->e:Z

    if-eqz v0, :cond_0

    .line 212
    sget v1, Lcom/google/android/apps/gmm/f;->du:I

    .line 213
    sget v0, Lcom/google/android/apps/gmm/l;->w:I

    .line 218
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 220
    return-void

    .line 215
    :cond_0
    sget v1, Lcom/google/android/apps/gmm/f;->dt:I

    .line 216
    sget v0, Lcom/google/android/apps/gmm/l;->y:I

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 110
    sget v0, Lcom/google/android/apps/gmm/l;->nk:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/a/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/a/a/a;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    invoke-static {p1}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 111
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v10

    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/base/views/aa;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/aa;-><init>(Landroid/content/Context;)V

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/views/aa;->d:Landroid/app/Fragment;

    const v1, 0x103006b

    .line 119
    iput v1, v0, Lcom/google/android/apps/gmm/base/views/aa;->c:I

    sget v1, Lcom/google/android/apps/gmm/h;->aj:I

    iput v1, v0, Lcom/google/android/apps/gmm/base/views/aa;->b:I

    new-instance v1, Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;-><init>(Lcom/google/android/apps/gmm/base/views/aa;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->n:Lcom/google/android/apps/gmm/base/views/HeaderView;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->n:Lcom/google/android/apps/gmm/base/views/HeaderView;

    const-string v1, "StreetView"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 122
    invoke-static {v10}, Lcom/google/geo/render/mirth/api/g;->a(Landroid/content/Context;)V

    .line 123
    new-instance v0, Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-direct {v0, v10}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->l:Z

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    const/4 v1, 0x3

    new-instance v2, Lcom/google/geo/render/mirth/api/ah;

    invoke-direct {v2, v0, v1}, Lcom/google/geo/render/mirth/api/ah;-><init>(Lcom/google/geo/render/mirth/api/MirthSurfaceView;I)V

    invoke-virtual {v0, v2}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->execute(Ljava/lang/Runnable;)V

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/streetview/p;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/p;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->f:Lcom/google/android/apps/gmm/streetview/p;

    .line 131
    if-eqz p3, :cond_3

    .line 132
    :goto_0
    if-eqz p3, :cond_4

    .line 133
    new-instance v3, Lcom/google/geo/render/mirth/api/aw;

    invoke-direct {v3}, Lcom/google/geo/render/mirth/api/aw;-><init>()V

    const-string v0, "panoId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v4, v3, Lcom/google/geo/render/mirth/api/aw;->a:J

    invoke-static {v4, v5, v3, v0}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->StreetViewParams_setId(JLcom/google/geo/render/mirth/api/aw;Ljava/lang/String;)V

    const-string v0, "userOrientation"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/b/a;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    float-to-double v4, v1

    iget v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    float-to-double v6, v1

    iget v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    float-to-double v8, v1

    iget-wide v1, v3, Lcom/google/geo/render/mirth/api/aw;->a:J

    invoke-static/range {v1 .. v9}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->StreetViewParams_setCameraParams(JLcom/google/geo/render/mirth/api/aw;DDD)V

    :cond_0
    const-string v1, "latLng"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/q;

    const-string v2, "placemarkLatLng"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/p;->b(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)F

    move-result v1

    float-to-double v4, v1

    const-wide/16 v6, 0x0

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    float-to-double v8, v0

    iget-wide v1, v3, Lcom/google/geo/render/mirth/api/aw;->a:J

    invoke-static/range {v1 .. v9}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->StreetViewParams_setCameraParams(JLcom/google/geo/render/mirth/api/aw;DDD)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->n:Lcom/google/android/apps/gmm/base/views/HeaderView;

    const-string v1, "address"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/j;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/gmm/streetview/j;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;Lcom/google/geo/render/mirth/api/aw;)V

    invoke-virtual {v0, v1}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->execute(Ljava/lang/Runnable;)V

    .line 154
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, v10}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->g:Landroid/view/ViewGroup;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 161
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, v10}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->b:Landroid/widget/ImageView;

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->b:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, -0xa00000

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 163
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->g:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/g;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment$3;

    invoke-direct {v0, p0, v10}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment$3;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bu:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/gmm/streetview/l;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/streetview/l;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41800000    # 16.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    sget v3, Lcom/google/android/apps/gmm/f;->dj:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->di:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v4, 0xb2

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    invoke-direct {v4, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setColorFilter(I)V

    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_6

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->b()V

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->g:Landroid/view/ViewGroup;

    return-object v0

    .line 131
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    goto/16 :goto_0

    .line 135
    :cond_4
    new-instance v3, Lcom/google/geo/render/mirth/api/aw;

    invoke-direct {v3}, Lcom/google/geo/render/mirth/api/aw;-><init>()V

    goto/16 :goto_1

    .line 166
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_3
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 413
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onDestroyView()V

    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    .line 415
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 387
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onPause()V

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->c()V

    .line 391
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->e:Z

    if-eqz v0, :cond_1

    .line 393
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->c()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    .line 394
    if-eqz v0, :cond_1

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;)V

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->d:Lcom/google/android/apps/gmm/util/h;

    if-eqz v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->d:Lcom/google/android/apps/gmm/util/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 401
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/o;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/streetview/o;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->execute(Ljava/lang/Runnable;)V

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 409
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 336
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onResume()V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->d()V

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    sget v1, Lcom/google/android/apps/gmm/l;->S:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 342
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 343
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 344
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->n:Lcom/google/android/apps/gmm/base/views/HeaderView;

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    const/4 v1, 0x0

    .line 346
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    .line 347
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->u:Lcom/google/android/apps/gmm/base/activities/z;

    .line 348
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 349
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/streetview/m;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V

    .line 350
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->n:Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/util/h;->a(Landroid/app/Fragment;Landroid/view/View;)Lcom/google/android/apps/gmm/util/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->c:Lcom/google/android/apps/gmm/util/h;

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/g;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->m:Landroid/widget/ImageView;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/util/h;->a(Landroid/app/Fragment;Landroid/view/View;)Lcom/google/android/apps/gmm/util/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->d:Lcom/google/android/apps/gmm/util/h;

    .line 367
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->e:Z

    if-eqz v0, :cond_2

    .line 369
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->c()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    .line 370
    if-eqz v0, :cond_2

    .line 371
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->o:Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/p/b/h;)V

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/streetview/n;-><init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->execute(Ljava/lang/Runnable;)V

    .line 383
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 323
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    .line 327
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/e;->b()Lcom/google/geo/render/mirth/api/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/av;->a()Lcom/google/geo/render/mirth/api/au;

    move-result-object v0

    .line 328
    const-string v1, "panoId"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/google/geo/render/mirth/api/au;

    if-ne v2, v3, :cond_1

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/au;->a:J

    invoke-static {v2, v3, v0}, Lcom/google/geo/render/mirth/api/StreetViewPanoInfoSwigJNI;->SmartPtrStreetViewPanoInfo_getId(JLcom/google/geo/render/mirth/api/au;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/google/geo/render/mirth/api/au;

    if-ne v2, v3, :cond_2

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/au;->a:J

    invoke-static {v2, v3, v0}, Lcom/google/geo/render/mirth/api/StreetViewPanoInfoSwigJNI;->SmartPtrStreetViewPanoInfo_getLatitude(JLcom/google/geo/render/mirth/api/au;)D

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/google/geo/render/mirth/api/au;

    if-ne v4, v5, :cond_3

    iget-wide v4, v0, Lcom/google/geo/render/mirth/api/au;->a:J

    invoke-static {v4, v5, v0}, Lcom/google/geo/render/mirth/api/StreetViewPanoInfoSwigJNI;->SmartPtrStreetViewPanoInfo_getLongitude(JLcom/google/geo/render/mirth/api/au;)D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 330
    const-string v0, "latLng"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 332
    :cond_0
    return-void

    .line 328
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method SmartPtrStreetViewPanoInfo::getId"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method SmartPtrStreetViewPanoInfo::getLatitude"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method SmartPtrStreetViewPanoInfo::getLongitude"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
