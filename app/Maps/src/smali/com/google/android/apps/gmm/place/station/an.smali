.class public Lcom/google/android/apps/gmm/place/station/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/e;


# instance fields
.field private final a:Lcom/google/maps/g/vs;

.field private final b:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/maps/g/vs;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/vx;)V
    .locals 19
    .param p5    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/libraries/curvular/cg;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Ljava/lang/String;",
            "Lcom/google/maps/g/vs;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;",
            "Lcom/google/maps/g/vx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v6, 0x0

    .line 51
    move-object/from16 v0, p7

    iget v2, v0, Lcom/google/maps/g/vx;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual/range {p7 .. p7}, Lcom/google/maps/g/vx;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v6

    .line 55
    :cond_0
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/station/an;->a:Lcom/google/maps/g/vs;

    .line 56
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/station/an;->b:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 58
    invoke-virtual/range {p7 .. p7}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->c:Ljava/util/List;

    .line 59
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/v;->a(Lcom/google/maps/g/vx;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->d:Ljava/lang/String;

    .line 60
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/v;->b(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->e:Lcom/google/android/libraries/curvular/a;

    .line 61
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/v;->c(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->f:Lcom/google/android/libraries/curvular/a;

    .line 62
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/v;->d(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->g:Lcom/google/android/libraries/curvular/a;

    .line 63
    move-object/from16 v0, p1

    move-object/from16 v1, p7

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Lcom/google/maps/g/vx;)Lcom/google/android/apps/gmm/base/views/c/b;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->h:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 65
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/an;->a(Lcom/google/maps/g/vx;)Lcom/google/b/c/cv;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->j:Lcom/google/b/c/cv;

    .line 67
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v17

    .line 68
    invoke-virtual/range {p7 .. p7}, Lcom/google/maps/g/vx;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/maps/g/vk;

    .line 69
    new-instance v2, Lcom/google/android/apps/gmm/place/station/ai;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/place/station/an;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/place/station/an;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/place/station/an;->e:Lcom/google/android/libraries/curvular/a;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/place/station/an;->f:Lcom/google/android/libraries/curvular/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/place/station/an;->g:Lcom/google/android/libraries/curvular/a;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/place/station/an;->h:Lcom/google/android/apps/gmm/base/views/c/b;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/an;->j:Lcom/google/b/c/cv;

    move-object/from16 v16, v0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    invoke-direct/range {v2 .. v16}, Lcom/google/android/apps/gmm/place/station/ai;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/vk;Ljava/util/List;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 51
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 86
    :cond_2
    invoke-virtual/range {v17 .. v17}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->i:Lcom/google/b/c/cv;

    .line 88
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->i:Lcom/google/b/c/cv;

    invoke-static {v2}, Lcom/google/android/apps/gmm/place/station/v;->b(Ljava/util/List;)Ljava/lang/String;

    .line 90
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/an;->i:Lcom/google/b/c/cv;

    invoke-static {v2}, Lcom/google/android/apps/gmm/place/station/v;->c(Ljava/util/List;)Ljava/lang/String;

    .line 91
    return-void
.end method

.method private static a(Lcom/google/maps/g/vx;)Lcom/google/b/c/cv;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/vx;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 95
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/maps/g/vx;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 96
    new-instance v3, Lcom/google/android/apps/gmm/directions/i/af;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/directions/i/af;-><init>(Lcom/google/maps/g/a/da;)V

    invoke-virtual {v1, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 98
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->i:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/a;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->e:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/a;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->f:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->g:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->h:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->j:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->b:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final i()Lcom/google/maps/g/vs;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/an;->a:Lcom/google/maps/g/vs;

    return-object v0
.end method
