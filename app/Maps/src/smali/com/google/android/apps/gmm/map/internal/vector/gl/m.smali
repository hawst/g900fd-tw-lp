.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/aw;


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:Ljava/nio/ShortBuffer;


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;Ljava/nio/ShortBuffer;)V
    .locals 0

    .prologue
    .line 1255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1256
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->a:Ljava/nio/ByteBuffer;

    .line 1257
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->b:Ljava/nio/ShortBuffer;

    .line 1258
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;[S)V
    .locals 2

    .prologue
    .line 1245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1246
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->a:Ljava/nio/ByteBuffer;

    .line 1247
    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 1249
    array-length v0, p2

    shl-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1250
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->b:Ljava/nio/ShortBuffer;

    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->b:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 1253
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->a:Ljava/nio/ByteBuffer;

    .line 1263
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->a:Ljava/nio/ByteBuffer;

    .line 1267
    return-object v0
.end method

.method public final b()Ljava/nio/ShortBuffer;
    .locals 2

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->b:Ljava/nio/ShortBuffer;

    .line 1273
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->b:Ljava/nio/ShortBuffer;

    .line 1274
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/m;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
