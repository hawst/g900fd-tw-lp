.class public Lcom/google/android/apps/gmm/directions/f/a/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/r/a/ae;

.field b:[Lcom/google/android/apps/gmm/map/r/a/s;

.field public c:Lcom/google/android/apps/gmm/directions/f/a/c;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Lcom/google/android/apps/gmm/map/r/a/ag;

.field public i:Lcom/google/android/apps/gmm/map/b/a/q;

.field public j:Lcom/google/android/apps/gmm/map/ag;

.field public k:Z

.field public l:Lcom/google/android/apps/gmm/map/r/a/v;

.field public m:Lcom/google/android/apps/gmm/map/o/b/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->e:Z

    .line 205
    sget-object v0, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->j:Lcom/google/android/apps/gmm/map/ag;

    .line 208
    sget-object v0, Lcom/google/android/apps/gmm/map/o/b/h;->a:Lcom/google/android/apps/gmm/map/o/b/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 210
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/directions/f/a/a;)V
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->e:Z

    .line 205
    sget-object v0, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->j:Lcom/google/android/apps/gmm/map/ag;

    .line 208
    sget-object v0, Lcom/google/android/apps/gmm/map/o/b/h;->a:Lcom/google/android/apps/gmm/map/o/b/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 213
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 214
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->b:[Lcom/google/android/apps/gmm/map/r/a/s;

    .line 215
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->c:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 216
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    .line 217
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->e:Z

    .line 218
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    .line 219
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->g:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    .line 220
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 221
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 222
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->j:Lcom/google/android/apps/gmm/map/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->j:Lcom/google/android/apps/gmm/map/ag;

    .line 223
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->k:Z

    .line 224
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 225
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 226
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/s;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->b:[Lcom/google/android/apps/gmm/map/r/a/s;

    move v1, v2

    .line 255
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 256
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->b:[Lcom/google/android/apps/gmm/map/r/a/s;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 257
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    .line 256
    :goto_1
    invoke-interface {p1, v4, v0}, Lcom/google/android/apps/gmm/map/i/s;->a(Lcom/google/maps/g/a/hm;Z)Lcom/google/android/apps/gmm/map/r/a/s;

    move-result-object v0

    aput-object v0, v3, v1

    .line 255
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 257
    goto :goto_1

    .line 259
    :cond_2
    return-object p0
.end method
