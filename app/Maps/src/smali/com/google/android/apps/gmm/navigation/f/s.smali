.class public Lcom/google/android/apps/gmm/navigation/f/s;
.super Lcom/google/android/apps/gmm/navigation/commonui/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/f/r;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/f/q;

.field final b:Lcom/google/android/apps/gmm/navigation/f/b/b;

.field c:Lcom/google/android/apps/gmm/navigation/f/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/map/util/b/g;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/navigation/f/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/f/s;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/a;-><init>()V

    .line 42
    const-string v0, "host"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/f/q;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/f/s;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    .line 43
    const-string v0, "eventBus"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/f/s;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/navigation/f/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/f/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    .line 45
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->f:Z

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->c:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->c:Z

    .line 70
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 76
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/d;

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/f/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    .line 82
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->f:Z

    if-eqz v0, :cond_2

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/s;->e()V

    .line 86
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-nez v0, :cond_3

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/f/q;->c()V

    .line 89
    :cond_3
    return-void

    .line 78
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/f/b/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    goto :goto_1

    :cond_5
    move v0, v2

    .line 86
    goto :goto_2
.end method

.method public final a(Ljava/lang/Float;)V
    .locals 2
    .param p1    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/f/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/s;->e()V

    .line 119
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->f:Z

    .line 57
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/f/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/s;->e()V

    .line 126
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/f/b/b;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/commonui/b/b;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/f/s;->e()V

    .line 133
    return-void
.end method

.method e()V
    .locals 5

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->f:Z

    const-string v1, "isResumed"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->b:Lcom/google/android/apps/gmm/navigation/f/b/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/f/b/b;->c()Lcom/google/android/apps/gmm/navigation/f/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->c:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 143
    sget-object v0, Lcom/google/android/apps/gmm/navigation/f/s;->d:Ljava/lang/String;

    const-string v1, "dispatchStateChange: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/f/s;->c:Lcom/google/android/apps/gmm/navigation/f/b/a;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/s;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/f/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/f/s;->c:Lcom/google/android/apps/gmm/navigation/f/b/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/f/t;-><init>(Lcom/google/android/apps/gmm/navigation/f/b/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 145
    return-void
.end method
