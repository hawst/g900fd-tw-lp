.class final Lcom/google/android/apps/gmm/startpage/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/util/SparseArray;

.field final synthetic c:Lcom/google/android/apps/gmm/base/l/a/ab;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/util/SparseArray;Lcom/google/android/apps/gmm/base/l/a/ab;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/d;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/d;->b:Landroid/util/SparseArray;

    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/d;->c:Lcom/google/android/apps/gmm/base/l/a/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 5

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/d;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/d;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/d;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/d;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/d;->c:Lcom/google/android/apps/gmm/base/l/a/ab;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/d;->a:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 206
    sget v2, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->c:I

    sget v3, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->d:I

    .line 207
    sget-boolean v4, Lcom/google/android/apps/gmm/startpage/GuidePageFragment;->e:Z

    .line 203
    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/support/v7/widget/RecyclerView;IIZ)V

    .line 211
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
