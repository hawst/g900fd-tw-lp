.class public abstract Lcom/google/android/apps/gmm/shared/net/af;
.super Lcom/google/android/apps/gmm/shared/net/i;
.source "PG"


# instance fields
.field public final g:Lcom/google/e/a/a/a/d;


# direct methods
.method public constructor <init>(ILcom/google/e/a/a/a/d;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(I)V

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/af;->g:Lcom/google/e/a/a/a/d;

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/af;->g:Lcom/google/e/a/a/a/d;

    .line 27
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
.end method

.method public final a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/af;->g:Lcom/google/e/a/a/a/d;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/d;Ljava/io/DataInput;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/shared/net/af;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/af;->g_()Lcom/google/e/a/a/a/b;

    move-result-object v0

    instance-of v1, p1, Ljava/io/OutputStream;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/io/OutputStream;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;Z)V

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataOutput;Lcom/google/e/a/a/a/b;)V

    goto :goto_0
.end method

.method public abstract g_()Lcom/google/e/a/a/a/b;
.end method
