.class public Lcom/google/android/apps/gmm/shared/net/n;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/apps/gmm/shared/net/i;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:I

.field c:Lcom/google/android/apps/gmm/shared/net/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field d:Z

.field e:J

.field private final g:Lcom/google/android/apps/gmm/shared/net/r;

.field private final h:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final i:Lcom/google/android/apps/gmm/shared/c/f;

.field private final j:Lcom/google/android/apps/gmm/shared/net/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/n",
            "<TT;>.com/google/android/apps/gmm/shared/net/o;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/shared/net/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/n;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/net/o;-><init>(Lcom/google/android/apps/gmm/shared/net/n;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->j:Lcom/google/android/apps/gmm/shared/net/o;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->a:Ljava/lang/Object;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->c:Lcom/google/android/apps/gmm/shared/net/i;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->d:Z

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->e:J

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/n;->g:Lcom/google/android/apps/gmm/shared/net/r;

    .line 55
    iput p1, p0, Lcom/google/android/apps/gmm/shared/net/n;->b:I

    .line 56
    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/net/n;->h:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 57
    iput-object p4, p0, Lcom/google/android/apps/gmm/shared/net/n;->i:Lcom/google/android/apps/gmm/shared/c/f;

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/n;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->d:Z

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "pushing / request throttled"

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/n;->c:Lcom/google/android/apps/gmm/shared/net/i;

    .line 76
    const/4 v0, 0x0

    monitor-exit v1

    .line 80
    :goto_0
    return v0

    .line 78
    :cond_0
    const-string v0, "pushing / request sent immediately"

    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/shared/net/n;->b(Lcom/google/android/apps/gmm/shared/net/i;)V

    .line 80
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->g:Lcom/google/android/apps/gmm/shared/net/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 88
    iget v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->b:I

    if-lez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->h:Lcom/google/android/apps/gmm/shared/c/a/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/n;->j:Lcom/google/android/apps/gmm/shared/net/o;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget v3, p0, Lcom/google/android/apps/gmm/shared/net/n;->b:I

    int-to-long v4, v3

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/n;->d:Z

    .line 93
    :cond_0
    return-void
.end method
