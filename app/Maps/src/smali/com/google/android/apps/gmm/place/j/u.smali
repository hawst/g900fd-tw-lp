.class public Lcom/google/android/apps/gmm/place/j/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/v;
.implements Lcom/google/android/apps/gmm/place/br;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/google/android/apps/gmm/base/j/b;

.field private d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/j/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/j/b;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/u;->a:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/u;->c:Lcom/google/android/apps/gmm/base/j/b;

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->b:Landroid/content/res/Resources;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 99
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 101
    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 102
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dr:Lcom/google/b/f/t;

    aput-object v3, v0, v2

    .line 103
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 104
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 100
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/u;->d:Lcom/google/android/apps/gmm/x/o;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v7, v6, Lcom/google/android/apps/gmm/base/g/c;->q:Lcom/google/r/b/a/ade;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v3, v6, Lcom/google/android/apps/gmm/base/g/c;->r:Lcom/google/r/b/a/ade;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->J()Ljava/util/List;

    move-result-object v8

    if-eqz v3, :cond_1

    iget-object v0, v3, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->hs:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v9, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->c:Lcom/google/android/apps/gmm/base/j/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/u;->d:Lcom/google/android/apps/gmm/x/o;

    iget-object v3, v3, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/aje;

    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->b:Lcom/google/android/apps/gmm/place/j/x;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/v;-><init>(Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/r/b/a/aje;Ljava/lang/String;Lcom/google/android/apps/gmm/place/j/x;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v7, :cond_2

    iget-object v0, v7, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->nk:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v9, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->c:Lcom/google/android/apps/gmm/base/j/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/u;->d:Lcom/google/android/apps/gmm/x/o;

    iget-object v3, v7, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/aje;

    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->a:Lcom/google/android/apps/gmm/place/j/x;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/v;-><init>(Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/r/b/a/aje;Ljava/lang/String;Lcom/google/android/apps/gmm/place/j/x;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v8, :cond_4

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_3
    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/aje;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/c;->I()V

    iget-object v0, v6, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :goto_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/apps/gmm/j;->D:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->c:Lcom/google/android/apps/gmm/base/j/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/u;->d:Lcom/google/android/apps/gmm/x/o;

    sget-object v5, Lcom/google/android/apps/gmm/place/j/x;->c:Lcom/google/android/apps/gmm/place/j/x;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/v;-><init>(Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/r/b/a/aje;Ljava/lang/String;Lcom/google/android/apps/gmm/place/j/x;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_4
    return-void

    .line 49
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    iget-object v0, v6, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    iget v0, v0, Lcom/google/r/b/a/ade;->f:I

    goto :goto_4
.end method

.method public final b()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    invoke-virtual {v2}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    move-object v0, v1

    .line 113
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->b:Ljava/lang/String;

    .line 122
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->b()Lcom/google/b/f/t;

    move-result-object v0

    aput-object v0, v2, v3

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    invoke-virtual {v2}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    move-object v0, v1

    .line 140
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->b:Ljava/lang/String;

    .line 149
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->b()Lcom/google/b/f/t;

    move-result-object v0

    aput-object v0, v2, v3

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    invoke-virtual {v2}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->a:Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    move-object v0, v1

    .line 167
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/v;->b:Ljava/lang/String;

    .line 176
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->b()Lcom/google/b/f/t;

    move-result-object v0

    aput-object v0, v2, v3

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_1

    .line 207
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v1, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 206
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 207
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 214
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v1, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 213
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final q()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->a()V

    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->a()V

    .line 236
    const/4 v0, 0x0

    return-object v0
.end method

.method public final t()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/u;->e:Ljava/util/List;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/j/v;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/v;->a()V

    .line 247
    const/4 v0, 0x0

    return-object v0
.end method
