.class public Lcom/google/android/apps/gmm/startpage/OdelayListFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# static fields
.field static final c:Ljava/lang/String;


# instance fields
.field public d:Lcom/google/android/apps/gmm/startpage/m;

.field public e:Lcom/google/android/apps/gmm/startpage/d/d;

.field private f:Lcom/google/android/apps/gmm/cardui/a/d;

.field private g:Lcom/google/android/apps/gmm/startpage/f/i;

.field private final m:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/startpage/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/startpage/s;-><init>(Lcom/google/android/apps/gmm/startpage/OdelayListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->m:Ljava/lang/Object;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/startpage/OdelayListFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    const/4 v1, 0x0

    const/16 v2, 0xfa

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;II)V

    :cond_0
    return-void
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 123
    if-nez p1, :cond_0

    move v0, v1

    .line 141
    :goto_0
    return v0

    .line 126
    :cond_0
    const-string v0, "odelay_list_fragment_odelay_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 127
    instance-of v2, v0, Lcom/google/android/apps/gmm/startpage/d/d;

    if-nez v2, :cond_1

    move v0, v1

    .line 128
    goto :goto_0

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "cardui_action_delegate"

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 134
    instance-of v1, v0, Lcom/google/android/apps/gmm/cardui/a/d;

    if-eqz v1, :cond_2

    .line 135
    check-cast v0, Lcom/google/android/apps/gmm/cardui/a/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->f:Lcom/google/android/apps/gmm/cardui/a/d;

    .line 140
    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->f:Lcom/google/android/apps/gmm/cardui/a/d;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "loaded actionDelegate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const/4 v0, 0x1

    goto :goto_0

    .line 137
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "loaded fragment = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->f:Lcom/google/android/apps/gmm/cardui/a/d;

    goto :goto_1
.end method

.method public static b(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)Lcom/google/android/apps/gmm/startpage/OdelayListFragment;
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 95
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/app/Fragment;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "actionDelegate should be null or instance of Fragment, but actionDelegate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;-><init>()V

    .line 99
    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)V

    .line 100
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 86
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 87
    const-string v0, "restoreCameraPositionOnResume"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 88
    const-string v0, "odelay_list_fragment_odelay_state"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 89
    if-eqz p2, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "cardui_action_delegate"

    check-cast p2, Landroid/app/Fragment;

    invoke-virtual {v0, v1, v2, p2}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 90
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 91
    return-void
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 241
    .line 242
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 241
    :cond_0
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/google/b/f/t;->am:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->m:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c:Ljava/lang/String;

    .line 115
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/startpage/f/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->g:Lcom/google/android/apps/gmm/startpage/f/i;

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->g:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->f:Lcom/google/android/apps/gmm/cardui/a/d;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/m;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    .line 119
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 153
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/startpage/c/d;

    invoke-virtual {v0, v1, p2, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 156
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/startpage/c/d;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 158
    new-instance v2, Lcom/google/android/apps/gmm/startpage/t;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/startpage/t;-><init>(Lcom/google/android/apps/gmm/startpage/OdelayListFragment;Landroid/view/View;)V

    .line 169
    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iput-object v2, v3, Lcom/google/android/apps/gmm/startpage/m;->e:Ljava/lang/Runnable;

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/libraries/curvular/ag;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    const/16 v3, 0xfa

    invoke-static {v2, v1, v4, v3}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;II)V

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->m:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 147
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroy()V

    .line 148
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 224
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 197
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 206
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 207
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v2, 0x0

    .line 208
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 209
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 210
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    .line 212
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 211
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->B()Lcom/google/android/apps/gmm/base/activities/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->B()Lcom/google/android/apps/gmm/base/activities/a/a;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 217
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 228
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 229
    const-string v0, "odelay_list_fragment_odelay_state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->f:Lcom/google/android/apps/gmm/cardui/a/d;

    if-eqz v1, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "cardui_action_delegate"

    check-cast v1, Landroid/app/Fragment;

    invoke-virtual {v0, p1, v2, v1}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 231
    :cond_0
    return-void
.end method
