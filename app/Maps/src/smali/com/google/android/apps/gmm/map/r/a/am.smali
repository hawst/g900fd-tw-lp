.class public Lcom/google/android/apps/gmm/map/r/a/am;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/maps/g/a/bx;

.field public final b:I

.field public final c:I

.field public final d:I

.field public e:Z

.field public f:Lcom/google/android/apps/gmm/map/r/a/am;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public i:Lcom/google/android/apps/gmm/map/r/a/ag;

.field public final j:I

.field public k:Z


# direct methods
.method public constructor <init>(Lcom/google/maps/g/a/bx;IIIZLjava/lang/String;II)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    .line 81
    iput p2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->b:I

    .line 82
    iput p3, p0, Lcom/google/android/apps/gmm/map/r/a/am;->c:I

    .line 83
    iput p4, p0, Lcom/google/android/apps/gmm/map/r/a/am;->d:I

    .line 84
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/r/a/am;->e:Z

    .line 85
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/r/a/am;->g:Ljava/lang/String;

    .line 86
    iput p7, p0, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    .line 87
    iput p8, p0, Lcom/google/android/apps/gmm/map/r/a/am;->j:I

    .line 88
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->g:Ljava/lang/String;

    .line 217
    :goto_0
    return-object v0

    .line 214
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 231
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "guidanceType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    .line 232
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "relevanceRangeEnd"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->b:I

    .line 233
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "minRelevanceDistance"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->d:I

    .line 234
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "minRelevanceSeconds"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->c:I

    .line 235
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "isNextStepRelevant"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->e:Z

    .line 236
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "cannedMessageId"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->j:I

    .line 237
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "text"

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/am;->a()Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "step#"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 239
    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "overrideText"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->g:Ljava/lang/String;

    .line 240
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "noticeIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->h:I

    .line 241
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 242
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
