.class public Lcom/google/android/apps/gmm/place/riddler/c/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/riddler/c/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/adv;)V
    .locals 17

    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 26
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    .line 27
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v11

    .line 29
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/google/maps/g/nc;

    .line 34
    iget v1, v5, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_9

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 35
    iget v1, v5, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_0

    .line 36
    iget v1, v5, Lcom/google/maps/g/nc;->g:I

    invoke-static {v1}, Lcom/google/maps/g/nf;->a(I)Lcom/google/maps/g/nf;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    :cond_1
    sget-object v2, Lcom/google/maps/g/nf;->b:Lcom/google/maps/g/nf;

    if-eq v1, v2, :cond_3

    .line 37
    iget v1, v5, Lcom/google/maps/g/nc;->g:I

    invoke-static {v1}, Lcom/google/maps/g/nf;->a(I)Lcom/google/maps/g/nf;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    :cond_2
    sget-object v2, Lcom/google/maps/g/nf;->c:Lcom/google/maps/g/nf;

    if-ne v1, v2, :cond_0

    .line 38
    :cond_3
    iget v1, v5, Lcom/google/maps/g/nc;->b:I

    invoke-static {v1}, Lcom/google/maps/g/ni;->a(I)Lcom/google/maps/g/ni;

    move-result-object v1

    sget-object v2, Lcom/google/maps/g/ni;->a:Lcom/google/maps/g/ni;

    if-ne v1, v2, :cond_0

    .line 40
    iget v1, v5, Lcom/google/maps/g/nc;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_b

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_0

    .line 43
    iget v1, v5, Lcom/google/maps/g/nc;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_c

    iget-object v1, v5, Lcom/google/maps/g/nc;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/iw;->g()Lcom/google/maps/g/iw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/iw;

    move-object v7, v1

    .line 45
    :goto_4
    invoke-virtual {v7}, Lcom/google/maps/g/iw;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    iget-object v2, v5, Lcom/google/maps/g/nc;->e:Lcom/google/n/f;

    .line 48
    iget-object v3, v5, Lcom/google/maps/g/nc;->f:Lcom/google/n/f;

    .line 49
    iget v1, v5, Lcom/google/maps/g/nc;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_d

    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_f

    iget-object v1, v5, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    .line 51
    :cond_4
    :goto_6
    iget v1, v5, Lcom/google/maps/g/nc;->g:I

    invoke-static {v1}, Lcom/google/maps/g/nf;->a(I)Lcom/google/maps/g/nf;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/maps/g/nf;->a:Lcom/google/maps/g/nf;

    :cond_5
    sget-object v5, Lcom/google/maps/g/nf;->c:Lcom/google/maps/g/nf;

    if-ne v1, v5, :cond_10

    sget-object v5, Lcom/google/android/apps/gmm/place/riddler/c/d;->b:Lcom/google/android/apps/gmm/place/riddler/c/d;

    .line 54
    :goto_7
    iget v1, v7, Lcom/google/maps/g/iw;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v6, 0x1

    if-ne v1, v6, :cond_11

    const/4 v1, 0x1

    :goto_8
    if-eqz v1, :cond_13

    .line 55
    iget-object v1, v7, Lcom/google/maps/g/iw;->b:Ljava/lang/Object;

    instance-of v6, v1, Ljava/lang/String;

    if-eqz v6, :cond_12

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    .line 57
    :cond_6
    :goto_9
    invoke-virtual {v7}, Lcom/google/maps/g/iw;->d()Ljava/util/List;

    move-result-object v1

    .line 58
    new-instance v7, Ljava/util/ArrayList;

    .line 59
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 61
    const/4 v9, 0x0

    .line 62
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_a
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/nj;

    .line 64
    iget v8, v1, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v8, v8, 0x2

    const/4 v10, 0x2

    if-ne v8, v10, :cond_14

    const/4 v8, 0x1

    :goto_b
    if-eqz v8, :cond_8

    .line 65
    iget-object v8, v1, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    invoke-virtual {v8}, Lcom/google/n/f;->b()I

    move-result v8

    if-lez v8, :cond_8

    .line 66
    iget v8, v1, Lcom/google/maps/g/nj;->a:I

    and-int/lit8 v8, v8, 0x4

    const/4 v10, 0x4

    if-ne v8, v10, :cond_15

    const/4 v8, 0x1

    :goto_c
    if-eqz v8, :cond_8

    .line 67
    invoke-virtual {v1}, Lcom/google/maps/g/nj;->d()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_16

    :cond_7
    const/4 v8, 0x1

    :goto_d
    if-nez v8, :cond_8

    .line 68
    new-instance v14, Lcom/google/android/apps/gmm/place/riddler/c/b;

    iget-object v15, v1, Lcom/google/maps/g/nj;->c:Lcom/google/n/f;

    .line 69
    invoke-virtual {v1}, Lcom/google/maps/g/nj;->d()Ljava/lang/String;

    move-result-object v16

    iget-object v8, v1, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    instance-of v10, v8, Ljava/lang/String;

    if-eqz v10, :cond_17

    move-object v1, v8

    check-cast v1, Ljava/lang/String;

    :goto_e
    move-object/from16 v0, v16

    invoke-direct {v14, v15, v0, v1}, Lcom/google/android/apps/gmm/place/riddler/c/b;-><init>(Lcom/google/n/f;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_8
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 73
    new-instance v1, Lcom/google/android/apps/gmm/place/riddler/c/a;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/place/riddler/c/a;-><init>(Lcom/google/n/f;Lcom/google/n/f;Ljava/lang/String;Lcom/google/android/apps/gmm/place/riddler/c/d;Ljava/lang/String;Ljava/util/List;)V

    :goto_f
    move-object v9, v1

    .line 76
    goto :goto_a

    .line 34
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 35
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 40
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 43
    :cond_c
    invoke-static {}, Lcom/google/maps/g/iw;->g()Lcom/google/maps/g/iw;

    move-result-object v1

    move-object v7, v1

    goto/16 :goto_4

    .line 49
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_e
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    iput-object v4, v5, Lcom/google/maps/g/nc;->d:Ljava/lang/Object;

    goto/16 :goto_6

    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 51
    :cond_10
    sget-object v5, Lcom/google/android/apps/gmm/place/riddler/c/d;->a:Lcom/google/android/apps/gmm/place/riddler/c/d;

    goto/16 :goto_7

    .line 54
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_8

    .line 55
    :cond_12
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    iput-object v6, v7, Lcom/google/maps/g/iw;->b:Ljava/lang/Object;

    goto/16 :goto_9

    :cond_13
    const-string v6, ""

    goto/16 :goto_9

    .line 64
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_b

    .line 66
    :cond_15
    const/4 v8, 0x0

    goto :goto_c

    .line 67
    :cond_16
    const/4 v8, 0x0

    goto :goto_d

    .line 69
    :cond_17
    check-cast v8, Lcom/google/n/f;

    invoke-virtual {v8}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8}, Lcom/google/n/f;->e()Z

    move-result v8

    if-eqz v8, :cond_18

    iput-object v10, v1, Lcom/google/maps/g/nj;->b:Ljava/lang/Object;

    :cond_18
    move-object v1, v10

    goto :goto_e

    .line 78
    :cond_19
    if-eqz v9, :cond_0

    .line 79
    invoke-virtual {v11, v9}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto/16 :goto_0

    .line 85
    :cond_1a
    invoke-virtual {v11}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/riddler/c/e;->a:Lcom/google/b/c/cv;

    .line 86
    return-void

    :cond_1b
    move-object v1, v9

    goto :goto_f
.end method
