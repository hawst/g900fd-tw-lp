.class public final Lcom/google/android/apps/gmm/map/b/a/s;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public d:D


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/s;->a:D

    .line 40
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/s;->b:D

    .line 44
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    .line 45
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 8

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "No points included"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/r;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->a:D

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/s;->b:D

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)V

    return-object v0
.end method

.method public final a(DD)Lcom/google/android/apps/gmm/map/b/a/s;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 66
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->a:D

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->a:D

    .line 67
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->b:D

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->b:D

    .line 69
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    .line 72
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    .line 81
    :cond_0
    :goto_0
    return-object p0

    .line 73
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    cmpg-double v2, v2, p3

    if-gtz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    cmpg-double v2, p3, v2

    if-gtz v2, :cond_3

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    .line 75
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    invoke-static {v0, v1, p3, p4}, Lcom/google/android/apps/gmm/map/b/a/p;->b(DD)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    invoke-static {v2, v3, p3, p4}, Lcom/google/android/apps/gmm/map/b/a/p;->a(DD)D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    .line 76
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    goto :goto_0

    :cond_3
    move v0, v1

    .line 73
    goto :goto_1

    :cond_4
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->c:D

    cmpg-double v2, v2, p3

    if-lez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    cmpg-double v2, p3, v2

    if-lez v2, :cond_2

    move v0, v1

    goto :goto_1

    .line 78
    :cond_5
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/s;->d:D

    goto :goto_0
.end method
