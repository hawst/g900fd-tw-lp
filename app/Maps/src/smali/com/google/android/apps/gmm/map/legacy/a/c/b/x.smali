.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

.field final d:[Ljava/lang/String;

.field private final e:Lcom/google/android/apps/gmm/v/ao;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/v/ao;Ljava/util/List;Ljava/util/List;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/v/ao;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a:Ljava/util/ArrayList;

    .line 459
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 460
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    .line 461
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->d:[Ljava/lang/String;

    .line 462
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->e:Lcom/google/android/apps/gmm/v/ao;

    .line 463
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->f:Ljava/util/List;

    .line 464
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->g:Ljava/util/List;

    .line 465
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->h:Ljava/util/Set;

    .line 466
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)Ljava/util/List;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/map/indoor/c/r;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 520
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 522
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .line 523
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 524
    new-instance v21, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;

    invoke-direct/range {v21 .. v21}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;-><init>()V

    .line 525
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 526
    new-instance v22, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;

    invoke-direct/range {v22 .. v22}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;-><init>()V

    .line 528
    const/4 v1, 0x0

    move-object v15, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    .line 545
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 546
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    .line 548
    iget-object v1, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v1, :cond_0

    .line 549
    new-instance v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;-><init>()V

    .line 550
    iget-object v1, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;)Z

    .line 551
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v14}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->e:Lcom/google/android/apps/gmm/v/ao;

    .line 552
    iget-object v9, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->g:Ljava/util/List;

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/t/as;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 551
    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->h:Ljava/util/Set;

    iget-object v2, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->d:Ljava/util/HashSet;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 562
    :cond_0
    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    .line 563
    new-instance v16, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;)V

    .line 566
    if-eqz v15, :cond_2

    .line 567
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 569
    :goto_1
    iget-object v2, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_9

    .line 573
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->e:Lcom/google/android/apps/gmm/v/ao;

    .line 574
    iget-object v9, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->g:Ljava/util/List;

    move-object/from16 v3, v22

    move-object/from16 v4, v17

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/t/as;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 573
    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 577
    const/4 v2, 0x0

    move-object/from16 v0, v22

    iput v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->b:I

    const/4 v2, 0x0

    move-object/from16 v0, v22

    iput v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    .line 578
    iget-object v2, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v22

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;)Z

    .line 580
    :goto_2
    invoke-interface {v1, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->h:Ljava/util/Set;

    iget-object v3, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->d:Ljava/util/HashSet;

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v15, v16

    move-object/from16 v17, v1

    .line 583
    goto/16 :goto_0

    .line 567
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 584
    :cond_3
    iget-object v1, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v21

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 585
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->e:Lcom/google/android/apps/gmm/v/ao;

    .line 587
    iget-object v9, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->g:Ljava/util/List;

    move-object/from16 v3, v21

    move-object/from16 v4, v18

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/t/as;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 585
    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 590
    const/4 v2, 0x0

    move-object/from16 v0, v21

    iput v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->b:I

    const/4 v2, 0x0

    move-object/from16 v0, v21

    iput v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    .line 591
    iget-object v2, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v21

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;)Z

    .line 593
    :goto_3
    invoke-interface {v1, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 594
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->h:Ljava/util/Set;

    iget-object v3, v14, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->d:Ljava/util/HashSet;

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v18, v1

    .line 596
    goto/16 :goto_0

    .line 597
    :cond_4
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 598
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->e:Lcom/google/android/apps/gmm/v/ao;

    const/4 v3, 0x0

    .line 600
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->g:Ljava/util/List;

    move-object/from16 v3, v21

    move-object/from16 v4, v18

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/t/as;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 598
    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    :cond_5
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 604
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->e:Lcom/google/android/apps/gmm/v/ao;

    const/4 v3, 0x0

    .line 606
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->g:Ljava/util/List;

    move-object/from16 v3, v22

    move-object/from16 v4, v17

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/t/as;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 604
    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    :cond_6
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 611
    const/4 v1, 0x0

    .line 613
    :goto_4
    return-object v1

    :cond_7
    move-object/from16 v1, v19

    goto :goto_4

    :cond_8
    move-object/from16 v1, v18

    goto/16 :goto_3

    :cond_9
    move-object/from16 v1, v17

    goto/16 :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/cp;)V
    .locals 6

    .prologue
    .line 473
    const/4 v0, 0x0

    .line 478
    :goto_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 479
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v1

    .line 480
    instance-of v2, v1, Lcom/google/android/apps/gmm/map/internal/c/ad;

    if-eqz v2, :cond_1

    .line 481
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ad;

    .line 484
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 486
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a(Lcom/google/android/apps/gmm/map/internal/c/ad;)V

    .line 496
    :goto_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto :goto_0

    .line 489
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->d:[Ljava/lang/String;

    new-instance v5, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;-><init>(Lcom/google/android/apps/gmm/map/internal/c/ad;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/t/as;)V

    .line 494
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 498
    :cond_1
    return-void
.end method
