.class Lcom/google/android/apps/gmm/p/e/q;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:D

.field b:Lcom/google/android/apps/gmm/p/e/a/g;

.field c:D

.field d:D

.field final e:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/q;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    return-void
.end method


# virtual methods
.method a()V
    .locals 10

    .prologue
    .line 578
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v4, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v8

    mul-double/2addr v2, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    div-double v2, v4, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 579
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/q;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 580
    return-void
.end method
