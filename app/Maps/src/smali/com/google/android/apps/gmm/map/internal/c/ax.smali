.class public Lcom/google/android/apps/gmm/map/internal/c/ax;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public final a:[B

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final c:[I

.field private final d:J

.field private final e:I

.field private f:I

.field private final g:I

.field private final h:I


# direct methods
.method private constructor <init>(JIII[BILcom/google/android/apps/gmm/map/internal/c/be;[I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->d:J

    .line 58
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->e:I

    .line 59
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->f:I

    .line 60
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->g:I

    .line 61
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->a:[B

    .line 62
    iput p7, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->h:I

    .line 63
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 64
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->c:[I

    .line 65
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/bu;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/ax;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 115
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v0

    .line 116
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v9

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v1, -0x80000000

    xor-int v5, v0, v1

    .line 128
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ax;

    const-wide/16 v2, 0x0

    .line 130
    iget-object v0, p0, Lcom/google/maps/b/a/bu;->b:Lcom/google/maps/b/a/cz;

    iget v4, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 133
    invoke-virtual {p0}, Lcom/google/maps/b/a/bu;->a()Lcom/google/maps/b/a/cu;

    move-result-object v0

    iget v7, v0, Lcom/google/maps/b/a/cu;->c:I

    new-array v7, v7, [B

    iget-object v8, v0, Lcom/google/maps/b/a/cu;->a:[B

    iget v10, v0, Lcom/google/maps/b/a/cu;->b:I

    iget v0, v0, Lcom/google/maps/b/a/cu;->c:I

    invoke-static {v8, v10, v7, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-array v10, v6, [I

    move v8, v6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/ax;-><init>(JIII[BILcom/google/android/apps/gmm/map/internal/c/be;[I)V

    return-object v1

    .line 116
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v9, v0, v6

    goto :goto_0
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/ax;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 79
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 80
    new-array v7, v0, [B

    .line 81
    invoke-interface {p0, v7}, Ljava/io/DataInput;->readFully([B)V

    .line 84
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v2, 0xc

    if-lt v0, v2, :cond_2

    .line 85
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v3, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 86
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v9, v0

    .line 90
    :goto_1
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v8

    .line 93
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 94
    new-array v10, v2, [I

    move v0, v1

    .line 95
    :goto_2
    if-ge v0, v2, :cond_3

    .line 96
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    aput v1, v10, v0

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 86
    :cond_0
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v0, v0, v1

    goto :goto_0

    .line 88
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v9

    goto :goto_1

    .line 99
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ax;

    .line 100
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v2, v0

    .line 101
    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 102
    iget v5, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/ax;-><init>(JIII[BILcom/google/android/apps/gmm/map/internal/c/be;[I)V

    return-object v1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->d:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->e:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->f:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x6

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->h:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->c:[I

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->a:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x24

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 208
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "zGrade"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->e:I

    .line 209
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "zWithinGrade"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->f:I

    .line 210
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "encoding"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->g:I

    .line 211
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "style"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ax;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 212
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 213
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
