.class public Lcom/google/android/apps/gmm/settings/SettingsFragment;
.super Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/apps/gmm/feedback/a/c;


# instance fields
.field b:Landroid/preference/PreferenceScreen;

.field c:Landroid/preference/Preference;

.field private d:Landroid/preference/Preference;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/ListPreference;

.field private g:Landroid/preference/Preference;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;

.field private l:Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->h:Z

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/settings/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/settings/d;-><init>(Lcom/google/android/apps/gmm/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->j:Ljava/lang/Object;

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/settings/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/settings/e;-><init>(Lcom/google/android/apps/gmm/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->k:Ljava/lang/Object;

    .line 161
    new-instance v0, Lcom/google/android/apps/gmm/settings/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/settings/f;-><init>(Lcom/google/android/apps/gmm/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->l:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->mW:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->h:Z

    .line 136
    if-eqz p1, :cond_2

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "edit_home_work"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->d:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    const-string v1, "maps_history"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->g:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/gmm/l;->nf:I

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 156
    :goto_0
    return-void

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->d:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->g:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/gmm/l;->ne:I

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    goto :goto_0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 411
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->i:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 174
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    .line 179
    if-nez p1, :cond_1

    .line 180
    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->i:Ljava/lang/String;

    .line 185
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v2, "settings_preference"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 186
    sget v0, Lcom/google/android/apps/gmm/o;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->addPreferencesFromResource(I)V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    .line 189
    const-string v0, "edit_home_work"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->d:Landroid/preference/Preference;

    .line 190
    const-string v0, "maps_history"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->e:Landroid/preference/Preference;

    .line 195
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->h:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->l:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 196
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 199
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->g:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 208
    :cond_0
    const-string v0, "sign_in_out"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->g:Landroid/preference/Preference;

    .line 213
    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->e()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->a(Z)V

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->b:Landroid/preference/PreferenceScreen;

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/h;->ak:I

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    :cond_1
    const-string v0, "accountNameAtCreation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->i:Ljava/lang/String;

    goto :goto_0

    .line 216
    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 256
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onDestroy()V

    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->i:Ljava/lang/String;

    .line 259
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v1

    .line 258
    if-eq v2, v1, :cond_0

    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    .line 265
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/settings/g;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/settings/g;-><init>(Lcom/google/android/apps/gmm/settings/SettingsFragment;Lcom/google/android/apps/gmm/base/activities/c;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 275
    :cond_1
    return-void

    .line 258
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 240
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onPause()V

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->j:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 246
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 359
    :goto_0
    return v0

    .line 283
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    .line 284
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 286
    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->f:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 287
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 290
    sget-object v0, Lcom/google/b/f/t;->go:Lcom/google/b/f/t;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 292
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;)V

    move v0, v2

    .line 293
    goto :goto_0

    .line 296
    :cond_2
    const-string v4, "edit_home_work"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 297
    sget-object v0, Lcom/google/b/f/t;->eD:Lcom/google/b/f/t;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 299
    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    sget-object v0, Lcom/google/o/h/a/dq;->m:Lcom/google/o/h/a/dq;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    sget-object v0, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->fZ:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->M:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->c:Lcom/google/android/apps/gmm/base/activities/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/base/activities/a/a;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v5}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->b(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 300
    goto/16 :goto_0

    .line 303
    :cond_3
    const-string v4, "location_reporting"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->H()Lcom/google/android/apps/gmm/settings/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/settings/a/a;->e()V

    move v0, v2

    .line 312
    goto/16 :goto_0

    .line 315
    :cond_4
    const-string v4, "improve_location"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/settings/h;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/settings/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(ZLcom/google/android/apps/gmm/mylocation/b/c;)V

    move v0, v2

    .line 317
    goto/16 :goto_0

    .line 320
    :cond_5
    const-string v4, "maps_history"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 321
    sget-object v0, Lcom/google/b/f/t;->eG:Lcom/google/b/f/t;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 323
    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    sget-object v0, Lcom/google/o/h/a/dq;->l:Lcom/google/o/h/a/dq;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    sget-object v0, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->iq:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->N:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->c:Lcom/google/android/apps/gmm/base/activities/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/base/activities/a/a;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v5}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->b(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 324
    goto/16 :goto_0

    .line 327
    :cond_6
    const-string v4, "navigation_settings"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 329
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/settings/b;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/settings/b;-><init>()V

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 330
    goto/16 :goto_0

    .line 333
    :cond_7
    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->h:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 334
    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    .line 335
    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    :goto_1
    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v0, Lcom/google/b/f/t;->eK:Lcom/google/b/f/t;

    .line 337
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 335
    invoke-interface {v3, v1, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move v0, v2

    .line 339
    goto/16 :goto_0

    .line 335
    :cond_8
    sget-object v0, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    goto :goto_1

    .line 342
    :cond_9
    const-string v4, "about"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 343
    sget-object v0, Lcom/google/b/f/t;->eC:Lcom/google/b/f/t;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;-><init>()V

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 346
    goto/16 :goto_0

    .line 349
    :cond_a
    const-string v3, "sign_in_out"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 350
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->h:Z

    if-eqz v0, :cond_b

    .line 351
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->j()V

    :goto_2
    move v0, v2

    .line 355
    goto/16 :goto_0

    .line 353
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;)V

    goto :goto_2

    :cond_c
    move v0, v1

    .line 359
    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 225
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onResume()V

    .line 227
    sget v0, Lcom/google/android/apps/gmm/l;->K:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->j:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 236
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 251
    const-string v0, "accountNameAtCreation"

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/SettingsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->g:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/SettingsFragment;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
