.class Lcom/google/android/apps/gmm/p/e/p;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:D

.field final b:Lcom/google/android/apps/gmm/p/e/a/g;

.field c:D

.field d:D


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/p/e/p;Lcom/google/android/apps/gmm/p/e/p;)V
    .locals 12

    .prologue
    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668
    iget-wide v0, p1, Lcom/google/android/apps/gmm/p/e/p;->a:D

    iget-wide v2, p2, Lcom/google/android/apps/gmm/p/e/p;->a:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    .line 670
    iget-wide v0, p1, Lcom/google/android/apps/gmm/p/e/p;->d:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/p;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Lcom/google/android/apps/gmm/p/e/p;->d:D

    iget-wide v4, p2, Lcom/google/android/apps/gmm/p/e/p;->a:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->d:D

    .line 673
    iget-wide v0, p1, Lcom/google/android/apps/gmm/p/e/p;->c:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/p;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Lcom/google/android/apps/gmm/p/e/p;->c:D

    iget-object v4, p1, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 675
    iget-object v5, v4, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, v4, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v5

    float-to-double v6, v5

    iget-object v4, v4, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v10

    mul-double/2addr v4, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v8

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v4

    div-double v4, v6, v4

    add-double/2addr v2, v4

    iget-wide v4, p2, Lcom/google/android/apps/gmm/p/e/p;->a:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    div-double/2addr v0, v2

    .line 678
    iget-object v2, p1, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v4, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v8

    mul-double/2addr v2, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    div-double v2, v4, v2

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 679
    iget-object v2, p1, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iput-object v2, p0, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 680
    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->c:D

    .line 687
    :goto_0
    return-void

    .line 684
    :cond_0
    iget-object v2, p2, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iput-object v2, p0, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 685
    iget-object v2, p1, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v4, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v8

    mul-double/2addr v2, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    div-double v2, v4, v2

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->c:D

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/p/e/q;)V
    .locals 2

    .prologue
    .line 654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 655
    iget-wide v0, p1, Lcom/google/android/apps/gmm/p/e/q;->a:D

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    .line 656
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/e/q;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 657
    iget-wide v0, p1, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->c:D

    .line 658
    iget-wide v0, p1, Lcom/google/android/apps/gmm/p/e/q;->d:D

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->d:D

    .line 659
    return-void
.end method


# virtual methods
.method a()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 10

    .prologue
    .line 707
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->c:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v4, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v8

    mul-double/2addr v2, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    div-double v2, v4, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 708
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 709
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/p;->b:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v3, v3, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 710
    return-object v1
.end method

.method public final a(Lcom/google/android/apps/gmm/p/e/q;)V
    .locals 6

    .prologue
    .line 695
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->d:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/q;->d:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/p/e/q;->a:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/p/e/q;->a:D

    add-double/2addr v2, v4

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->d:D

    .line 697
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->c:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/q;->c:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/p/e/q;->a:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/p/e/q;->a:D

    add-double/2addr v2, v4

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->c:D

    .line 700
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/p/e/q;->a:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/p;->a:D

    .line 701
    return-void
.end method
