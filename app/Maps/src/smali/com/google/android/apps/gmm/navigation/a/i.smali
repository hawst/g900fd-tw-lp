.class public Lcom/google/android/apps/gmm/navigation/a/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/a/j;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/media/AudioManager;

.field private final d:Lcom/google/android/apps/gmm/navigation/a/d;

.field private final e:Lcom/google/android/apps/gmm/navigation/a/h;

.field private final f:Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/a/h;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/base/a;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/i;->b:Landroid/content/Context;

    .line 69
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->c:Landroid/media/AudioManager;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/i;->e:Lcom/google/android/apps/gmm/navigation/a/h;

    .line 71
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/i;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 72
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/a/i;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/j;

    invoke-direct {v0, p4, p3}, Lcom/google/android/apps/gmm/navigation/a/j;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->a:Lcom/google/android/apps/gmm/navigation/a/j;

    .line 74
    return-void
.end method

.method private a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/i;->c:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/i;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    .line 139
    invoke-virtual {v1, v2, v3, p1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    .line 141
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    const-string v1, "TRANSIENT_EXCLUSIVE"

    .line 144
    :goto_0
    if-ne v2, v0, :cond_1

    .line 145
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " audio focus request granted."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    :goto_1
    return v0

    .line 141
    :cond_0
    const-string v1, "TRANSIENT_MAY_DUCK"

    goto :goto_0

    .line 148
    :cond_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " audio focus request denied."

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 91
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/i;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v3

    .line 92
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/i;->a:Lcom/google/android/apps/gmm/navigation/a/j;

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    sget-object v5, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    if-eq v4, v5, :cond_0

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/a/j;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v6, v2, Lcom/google/android/apps/gmm/navigation/a/j;->d:J

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/google/android/apps/gmm/navigation/a/j;->a:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    sget-object v4, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    iput-object v4, v2, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    const-string v2, "No state updates from GSA for a suspiciously long time."

    new-instance v4, Ljava/lang/Exception;

    const-string v5, "GSA audio state is stuck in a state other than IDLE for too long"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v4}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 93
    monitor-exit v3

    .line 104
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z

    if-eqz v0, :cond_3

    .line 96
    monitor-exit v3

    move v0, v1

    goto :goto_1

    .line 98
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->e:Lcom/google/android/apps/gmm/navigation/a/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/a/i;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z

    .line 104
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z

    monitor-exit v3

    goto :goto_1

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 101
    :cond_4
    const/4 v0, 0x3

    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/a/i;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 124
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/i;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v2

    .line 125
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/i;->c:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/i;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v3

    if-ne v3, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/i;->g:Z

    .line 128
    :cond_0
    monitor-exit v2

    return-void

    :cond_1
    move v0, v1

    .line 125
    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
