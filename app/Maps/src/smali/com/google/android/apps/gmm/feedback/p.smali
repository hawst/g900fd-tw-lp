.class Lcom/google/android/apps/gmm/feedback/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/a/g;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/gmm/feedback/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/l;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/p;->b:Lcom/google/android/apps/gmm/feedback/l;

    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/p;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 633
    new-instance v1, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 634
    const-string v0, "version"

    sget-object v2, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/p;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/p;->b:Lcom/google/android/apps/gmm/feedback/l;

    .line 637
    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/l;->f(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 636
    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 638
    invoke-static {}, Lcom/google/android/apps/gmm/util/q;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 639
    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 640
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 641
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/p;->b:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v1}, Lcom/google/android/apps/gmm/feedback/l;->g(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    .line 642
    new-instance v1, Lcom/google/android/gms/googlehelp/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/p;->b:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/feedback/l;->h(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/googlehelp/a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/a;->a(Landroid/content/Intent;)V

    .line 643
    return-void

    .line 637
    :cond_0
    new-instance v0, Landroid/accounts/Account;

    const-string v3, "anonymous"

    const-string v4, "com.example"

    invoke-direct {v0, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
