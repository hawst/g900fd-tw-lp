.class public Lcom/google/android/apps/gmm/l/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/ab;


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/String;


# instance fields
.field private e:Landroid/net/UrlQuerySanitizer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-string v0, "^(http|https)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/s;->a:Ljava/util/regex/Pattern;

    .line 47
    const-string v0, "^(maps|local|ditu)\\.google\\..+$"

    .line 48
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/s;->b:Ljava/util/regex/Pattern;

    .line 87
    const-string v0, "h"

    const-string v1, "k"

    invoke-static {v0, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/s;->c:Ljava/util/List;

    .line 90
    const-class v0, Lcom/google/android/apps/gmm/l/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/s;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 100
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 101
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 102
    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    .line 103
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    :cond_2
    move v0, v1

    .line 108
    :cond_3
    :goto_2
    return v0

    :cond_4
    move v2, v1

    .line 103
    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1

    .line 106
    :cond_6
    sget-object v2, Lcom/google/android/apps/gmm/l/s;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 107
    sget-object v3, Lcom/google/android/apps/gmm/l/s;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 108
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 27

    .prologue
    .line 121
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/l/s;->a(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    .line 122
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/l/s;->e:Landroid/net/UrlQuerySanitizer;

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    :goto_0
    return-object v2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/l/s;->e:Landroid/net/UrlQuerySanitizer;

    if-nez v2, :cond_8

    new-instance v2, Lcom/google/android/apps/gmm/l/t;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/l/t;-><init>()V

    move-object v3, v2

    :goto_1
    invoke-virtual {v3, v4}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    const-string v2, "q"

    invoke-virtual {v3, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    invoke-static {v4}, Lcom/google/android/apps/gmm/l/ac;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/af;

    move-result-object v5

    if-eqz v5, :cond_18

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/l/af;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v5, Lcom/google/android/apps/gmm/l/af;->a:Ljava/lang/String;

    move-object v5, v4

    move-object v4, v2

    :goto_2
    const-string v2, "hq"

    invoke-virtual {v3, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v2, "hnear"

    invoke-virtual {v3, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "sll"

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v11

    const-string v2, "ll"

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v12

    const-string v2, "spn"

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/l/ac;->f(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v13

    const-string v2, "sspn"

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/l/ac;->f(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v14

    const-string v2, "z"

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-static {v3, v2, v6, v7}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;FF)Ljava/lang/Float;

    move-result-object v15

    const-string v2, "geocode"

    const-string v6, ";"

    invoke-static {v3, v2, v6}, Lcom/google/android/apps/gmm/l/ac;->b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const-string v7, "saddr"

    if-eqz v6, :cond_9

    const/4 v2, 0x0

    aget-object v2, v6, v2

    :goto_3
    invoke-static {v3, v7, v2}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v16

    const-string v7, "daddr"

    if-eqz v6, :cond_a

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v6, v2

    :goto_4
    invoke-static {v3, v7, v2}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v17

    const-string v2, "myl"

    invoke-static {v3, v2}, Lcom/google/android/apps/gmm/l/ac;->c(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ag;

    move-result-object v18

    const-string v2, "layer"

    invoke-virtual {v3, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_b

    :cond_2
    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_c

    const/4 v2, 0x0

    move-object v6, v2

    :goto_6
    const-string v2, "t"

    invoke-virtual {v3, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x0

    if-eqz v8, :cond_4

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    sget-object v2, Lcom/google/android/apps/gmm/l/s;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :cond_4
    :goto_7
    const-string v7, "list"

    const-string v8, "gmmview"

    invoke-virtual {v3, v8}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    const-string v7, "nav"

    invoke-static {v3, v7}, Lcom/google/android/apps/gmm/l/ac;->b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    if-nez v7, :cond_e

    const/4 v7, 0x0

    :goto_8
    const-string v8, "dirflg"

    invoke-static {v3, v8}, Lcom/google/android/apps/gmm/l/ac;->d(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ad;

    move-result-object v20

    const-string v8, "ftid"

    invoke-static {v3, v8}, Lcom/google/android/apps/gmm/l/ac;->e(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    if-nez v8, :cond_5

    const-string v8, "cid"

    invoke-static {v3, v8}, Lcom/google/android/apps/gmm/l/ac;->e(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    :cond_5
    const-string v21, "cbll"

    move-object/from16 v0, v21

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v21

    const-string v22, "panoid"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-string v23, "cbp"

    move-object/from16 v0, v23

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/l/ac;->g(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v23

    const-string v24, "entry"

    move-object/from16 v0, v24

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/l/ac;->h(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;

    move-result-object v24

    const-string v25, "ptp"

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    invoke-static {}, Lcom/google/android/apps/gmm/l/u;->b()Lcom/google/android/apps/gmm/l/z;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v15, v0, Lcom/google/android/apps/gmm/l/z;->h:Ljava/lang/Float;

    move-object/from16 v0, v26

    iput-object v12, v0, Lcom/google/android/apps/gmm/l/z;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, v26

    iput-object v13, v0, Lcom/google/android/apps/gmm/l/z;->q:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, v26

    iput-object v6, v0, Lcom/google/android/apps/gmm/l/z;->m:Ljava/lang/Boolean;

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->n:Ljava/lang/Boolean;

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->v:Lcom/google/android/apps/gmm/l/ae;

    const-string v24, "gmm"

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/l/ac;->c(Ljava/lang/String;)Lcom/google/r/b/a/a/s;

    move-result-object v3

    move-object/from16 v0, v26

    iput-object v3, v0, Lcom/google/android/apps/gmm/l/z;->z:Lcom/google/r/b/a/a/s;

    if-nez v17, :cond_6

    if-eqz v16, :cond_f

    :cond_6
    if-eqz v7, :cond_7

    sget-object v2, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->o:Lcom/google/android/apps/gmm/l/aa;

    :cond_7
    sget-object v2, Lcom/google/android/apps/gmm/l/y;->a:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->i:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->j:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->l:Lcom/google/android/apps/gmm/l/ad;

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->k:Lcom/google/android/apps/gmm/l/ag;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->w:Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/l/s;->e:Landroid/net/UrlQuerySanitizer;

    move-object v3, v2

    goto/16 :goto_1

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_c
    const-string v2, "t"

    invoke-virtual {v6, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_d

    const/4 v2, 0x1

    :goto_9
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v6, v2

    goto/16 :goto_6

    :cond_d
    const/4 v2, 0x0

    goto :goto_9

    :cond_e
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    goto/16 :goto_8

    :cond_f
    if-nez v22, :cond_10

    if-eqz v21, :cond_11

    :cond_10
    sget-object v2, Lcom/google/android/apps/gmm/l/y;->g:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->s:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->t:Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    move-object/from16 v0, v26

    iput-object v5, v0, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    goto/16 :goto_0

    :cond_11
    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v3

    if-eqz v3, :cond_12

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->e:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v26

    iput-object v8, v0, Lcom/google/android/apps/gmm/l/z;->p:Lcom/google/android/apps/gmm/map/b/a/j;

    move-object/from16 v0, v26

    iput-object v5, v0, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    goto/16 :goto_0

    :cond_12
    if-eqz v5, :cond_14

    if-eqz v19, :cond_13

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->d:Lcom/google/android/apps/gmm/l/y;

    :goto_a
    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v26

    iput-object v5, v0, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v9, v0, Lcom/google/android/apps/gmm/l/z;->c:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v10, v0, Lcom/google/android/apps/gmm/l/z;->d:Ljava/lang/String;

    move-object/from16 v0, v26

    iput-object v11, v0, Lcom/google/android/apps/gmm/l/z;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, v26

    iput-object v14, v0, Lcom/google/android/apps/gmm/l/z;->r:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, v26

    iput-object v4, v0, Lcom/google/android/apps/gmm/l/z;->e:Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    goto/16 :goto_0

    :cond_13
    sget-object v2, Lcom/google/android/apps/gmm/l/y;->c:Lcom/google/android/apps/gmm/l/y;

    goto :goto_a

    :cond_14
    if-nez v12, :cond_15

    if-nez v13, :cond_15

    if-nez v15, :cond_15

    if-nez v6, :cond_15

    if-eqz v2, :cond_16

    :cond_15
    sget-object v2, Lcom/google/android/apps/gmm/l/y;->f:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    goto/16 :goto_0

    :cond_16
    sget-object v2, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto/16 :goto_0

    :cond_17
    move-object v2, v7

    goto/16 :goto_7

    :cond_18
    move-object v5, v4

    move-object v4, v2

    goto/16 :goto_2
.end method
