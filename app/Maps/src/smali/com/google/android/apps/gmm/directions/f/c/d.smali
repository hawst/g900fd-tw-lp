.class public final Lcom/google/android/apps/gmm/directions/f/c/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/f/c/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/android/apps/gmm/directions/f/c/b;",
        "Lcom/google/android/apps/gmm/directions/f/c/d;",
        ">;",
        "Lcom/google/android/apps/gmm/directions/f/c/g;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:I

.field public e:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 415
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/b;->f:Lcom/google/android/apps/gmm/directions/f/c/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 500
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->b:Lcom/google/n/ao;

    .line 559
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->c:Lcom/google/n/ao;

    .line 618
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->d:I

    .line 416
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/directions/f/c/b;)Lcom/google/android/apps/gmm/directions/f/c/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 463
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/c/b;->d()Lcom/google/android/apps/gmm/directions/f/c/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 479
    :goto_0
    return-object p0

    .line 464
    :cond_0
    iget v2, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 465
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 466
    iget v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    .line 468
    :cond_1
    iget v2, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 469
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 470
    iget v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    .line 472
    :cond_2
    iget v2, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 473
    iget v2, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/f/c/e;->a(I)Lcom/google/android/apps/gmm/directions/f/c/e;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/c/e;->a:Lcom/google/android/apps/gmm/directions/f/c/e;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 464
    goto :goto_1

    :cond_5
    move v2, v1

    .line 468
    goto :goto_2

    :cond_6
    move v2, v1

    .line 472
    goto :goto_3

    .line 473
    :cond_7
    iget v3, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    iget v2, v2, Lcom/google/android/apps/gmm/directions/f/c/e;->c:I

    iput v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->d:I

    .line 475
    :cond_8
    iget v2, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_4
    if-eqz v0, :cond_9

    .line 476
    iget-wide v0, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    iget v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    iput-wide v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->e:J

    .line 478
    :cond_9
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/c/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_a
    move v0, v1

    .line 475
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 407
    new-instance v2, Lcom/google/android/apps/gmm/directions/f/c/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/f/c/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->d:I

    iput v1, v2, Lcom/google/android/apps/gmm/directions/f/c/b;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->e:J

    iput-wide v4, v2, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    iput v0, v2, Lcom/google/android/apps/gmm/directions/f/c/b;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 407
    check-cast p1, Lcom/google/android/apps/gmm/directions/f/c/b;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/f/c/d;->a(Lcom/google/android/apps/gmm/directions/f/c/b;)Lcom/google/android/apps/gmm/directions/f/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 483
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agd;

    invoke-virtual {v0}, Lcom/google/r/b/a/agd;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 495
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 483
    goto :goto_0

    .line 489
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c/d;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    invoke-virtual {v0}, Lcom/google/r/b/a/agl;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 492
    goto :goto_1

    :cond_2
    move v0, v1

    .line 489
    goto :goto_2

    :cond_3
    move v0, v2

    .line 495
    goto :goto_1
.end method
