.class public Lcom/google/android/apps/gmm/directions/views/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:[Lcom/google/android/apps/gmm/directions/views/b;

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 4

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput p1, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    .line 125
    iput p2, p0, Lcom/google/android/apps/gmm/directions/views/a;->e:I

    .line 126
    iput p3, p0, Lcom/google/android/apps/gmm/directions/views/a;->c:I

    .line 128
    new-array v0, p2, [Lcom/google/android/apps/gmm/directions/views/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    .line 130
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    new-instance v2, Lcom/google/android/apps/gmm/directions/views/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/views/b;-><init>(Lcom/google/android/apps/gmm/directions/views/a;)V

    aput-object v2, v1, v0

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    iput v3, v1, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    iput v3, v1, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)I
    .locals 2

    .prologue
    .line 142
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->e:I

    if-ge v0, v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    if-ne v1, p1, :cond_0

    .line 147
    :goto_1
    return v0

    .line 142
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(ILcom/google/android/apps/gmm/directions/views/BaseSchematicView;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    .line 154
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->e:I

    add-int/2addr v0, v1

    if-lt p1, v0, :cond_1

    .line 155
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    const-string v1, "setSchematicView was called with a position %d outside the valid range [%d, %d)."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 157
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    iget v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->e:I

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 155
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    iput v2, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->d:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    iput-object p2, v0, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "height >= 0 failed: height was %d."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v1, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)I

    move-result v0

    .line 189
    if-ltz v0, :cond_2

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v1, v0

    iput p2, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    .line 194
    :goto_1
    return-void

    .line 192
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)V
    .locals 3

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    .line 169
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)I

    move-result v0

    .line 170
    if-ltz v0, :cond_0

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    iput v2, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)F
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 219
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)I

    move-result v6

    .line 220
    if-gez v6, :cond_1

    .line 221
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    move v0, v4

    .line 224
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "states[%d] was not layouted before drawing."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v2

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v5}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v6

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_5

    move v0, v1

    :goto_2
    const-string v3, "states[%d].isLayouted() was not true"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v2

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v5}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v5, v6

    :goto_3
    if-ltz v5, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v5

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v5

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_b

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    move-object v3, v0

    :goto_6
    iget-object v0, v3, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, v3, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v5

    iput v4, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v5

    iget v3, v0, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    if-lt v6, v5, :cond_18

    move v0, v3

    move v3, v5

    :goto_7
    if-ge v3, v6, :cond_1a

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v5, v5, v3

    iget v5, v5, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    add-float/2addr v0, v5

    iget v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->c:I

    int-to-float v7, v5

    rem-float/2addr v0, v7

    cmpg-float v7, v0, v4

    if-gez v7, :cond_8

    int-to-float v5, v5

    add-float/2addr v0, v5

    :cond_8
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    add-int/lit8 v7, v3, 0x1

    aget-object v5, v5, v7

    iput v0, v5, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_9
    move v0, v2

    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_5

    :cond_b
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_3

    :cond_c
    add-int/lit8 v0, v6, 0x1

    :goto_8
    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/a;->e:I

    if-ge v0, v3, :cond_10

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_d

    move v3, v1

    :goto_9
    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_e

    move v3, v1

    :goto_a
    if-eqz v3, :cond_f

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    move-object v3, v0

    goto :goto_6

    :cond_d
    move v3, v2

    goto :goto_9

    :cond_e
    move v3, v2

    goto :goto_a

    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_10
    if-ltz v5, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v5

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_12

    move v0, v1

    :goto_b
    if-nez v0, :cond_13

    :cond_11
    move v0, v1

    :goto_c
    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_12
    move v0, v2

    goto :goto_b

    :cond_13
    move v0, v2

    goto :goto_c

    :cond_14
    add-int/lit8 v3, v5, 0x1

    if-ltz v3, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v0, v0, v3

    iget v0, v0, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_15

    move v0, v1

    :goto_d
    if-eqz v0, :cond_16

    move v0, v1

    :goto_e
    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_15
    move v0, v2

    goto :goto_d

    :cond_16
    move v0, v2

    goto :goto_e

    :cond_17
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_6

    :cond_18
    add-int/lit8 v0, v5, -0x1

    move v8, v0

    move v0, v3

    move v3, v8

    :goto_f
    if-lt v3, v6, :cond_1a

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v5, v5, v3

    iget v5, v5, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    sub-float/2addr v0, v5

    iget v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->c:I

    int-to-float v7, v5

    rem-float/2addr v0, v7

    cmpg-float v7, v0, v4

    if-gez v7, :cond_19

    int-to-float v5, v5

    add-float/2addr v0, v5

    :cond_19
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v5, v5, v3

    iput v0, v5, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    add-int/lit8 v3, v3, -0x1

    goto :goto_f

    :cond_1a
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v3, v3, v6

    iget v3, v3, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1b

    move v3, v1

    :goto_10
    const-string v4, "state[index=%d] was not fixed."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    move v3, v2

    goto :goto_10
.end method
