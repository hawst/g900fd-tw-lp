.class Lcom/google/android/apps/gmm/map/legacy/a/b/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/at;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    monitor-enter v1

    .line 308
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->g:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 309
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)V
    .locals 3

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v1

    .line 325
    if-eqz v1, :cond_1

    instance-of v0, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 326
    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->i:I

    .line 327
    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    .line 328
    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->j:Z

    .line 329
    if-ne v0, p2, :cond_0

    if-eqz v1, :cond_1

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->c:Lcom/google/android/apps/gmm/map/internal/d/as;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->l:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 333
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/map/internal/c/bo;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 314
    if-eqz p2, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 320
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/e;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-static {v0, p1, p3}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;)V

    goto :goto_0
.end method
