.class public final Lcom/google/android/apps/gmm/map/b/a/ah;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;I)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 17
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    .line 18
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;II)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 23
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    .line 24
    iput p3, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 7

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    .line 109
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 111
    iget v1, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 114
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/lit8 v0, v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    iget v6, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    if-ge v0, v6, :cond_4

    .line 115
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v6, v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 116
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v6, v4, :cond_0

    .line 117
    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 119
    :cond_0
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v6, v3, :cond_1

    .line 120
    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 122
    :cond_1
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ge v6, v2, :cond_2

    .line 123
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 125
    :cond_2
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-le v6, v1, :cond_3

    .line 126
    iget v1, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 114
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_4
    iput v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 132
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v1, v5, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/ah;

    if-eqz v2, :cond_3

    .line 68
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ah;

    .line 69
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    if-ne v2, v3, :cond_2

    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    if-ne v2, v3, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 70
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 72
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/lit8 v0, v0, 0x1f

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    add-int/2addr v0, v1

    .line 81
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "[("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
