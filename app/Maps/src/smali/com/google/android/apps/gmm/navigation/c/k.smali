.class public Lcom/google/android/apps/gmm/navigation/c/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/c/a/b;


# static fields
.field private static final e:F


# instance fields
.field a:Lcom/google/android/apps/gmm/shared/net/a/b;

.field b:Lcom/google/r/b/a/op;

.field c:Z

.field d:Z

.field private f:F

.field private final g:Lcom/google/android/apps/gmm/map/f/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/navigation/c/k;->e:F

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/r/b/a/op;ZZLcom/google/android/apps/gmm/map/f/a/h;)V
    .locals 6

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/c/k;->a:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/c/k;->b:Lcom/google/r/b/a/op;

    .line 64
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/c/k;->c:Z

    .line 65
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/navigation/c/k;->d:Z

    .line 66
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/c/k;->g:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 67
    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/k;->a:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/k;->b:Lcom/google/r/b/a/op;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/navigation/c/k;->c:Z

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/c/k;->d:Z

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->c:Ljava/util/HashMap;

    new-instance v5, Lcom/google/android/apps/gmm/shared/net/a/m;

    invoke-direct {v5, v2, v3, v4, v0}, Lcom/google/android/apps/gmm/shared/net/a/m;-><init>(Lcom/google/r/b/a/op;ZZLcom/google/r/b/a/or;)V

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oe;

    iget-object v0, v0, Lcom/google/r/b/a/oe;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/oi;->d()Lcom/google/r/b/a/oi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oi;

    iget v0, v0, Lcom/google/r/b/a/oi;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/c/k;->f:F

    .line 68
    return-void
.end method

.method private a(Lcom/google/r/b/a/or;)F
    .locals 5

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/k;->a:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/k;->b:Lcom/google/r/b/a/op;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/c/k;->c:Z

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/navigation/c/k;->d:Z

    .line 72
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->c:Ljava/util/HashMap;

    new-instance v4, Lcom/google/android/apps/gmm/shared/net/a/m;

    invoke-direct {v4, v1, v2, v3, p1}, Lcom/google/android/apps/gmm/shared/net/a/m;-><init>(Lcom/google/r/b/a/op;ZZLcom/google/r/b/a/or;)V

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oe;

    iget-object v0, v0, Lcom/google/r/b/a/oe;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/oi;->d()Lcom/google/r/b/a/oi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oi;

    iget v0, v0, Lcom/google/r/b/a/oi;->c:F

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/ae;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 141
    const/high16 v0, 0x43800000    # 256.0f

    iget v1, p2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    .line 142
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 143
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 144
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const v1, 0x3f4ccccd    # 0.8f

    div-float/2addr v0, v1

    .line 145
    const/high16 v1, 0x41f00000    # 30.0f

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v0, v2

    sget v2, Lcom/google/android/apps/gmm/navigation/c/k;->e:F

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    const v1, 0x3e4ccccd    # 0.2f

    sub-float/2addr v0, v1

    .line 148
    sget-object v1, Lcom/google/r/b/a/or;->f:Lcom/google/r/b/a/or;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 152
    invoke-virtual {p3}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iget v3, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    .line 151
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v1

    .line 155
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 156
    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 157
    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 158
    iput-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 164
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 165
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 166
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/graphics/Rect;Landroid/util/DisplayMetrics;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 108
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 109
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/k;->g:Lcom/google/android/apps/gmm/map/f/a/h;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/a/h;->a:Lcom/google/android/apps/gmm/map/f/a/h;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 117
    :goto_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v4

    iget v5, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v5

    iget v6, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v6, v6

    .line 116
    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v3

    .line 119
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    sget-object v0, Lcom/google/r/b/a/or;->e:Lcom/google/r/b/a/or;

    .line 120
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v0

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iput v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iput-object v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 121
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0

    .line 114
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->l:F

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;[Lcom/google/android/apps/gmm/map/b/a/ah;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 128
    array-length v1, p2

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/b/a/y;

    .line 129
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    aput-object v2, v1, v0

    .line 130
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 131
    aget-object v2, p2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/ah;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    .line 132
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v4, v1, v3

    .line 133
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v2, v1, v3

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->b([Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 136
    invoke-virtual {p0, v0, p3, p4}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/android/apps/gmm/map/b/a/ae;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;Lcom/google/maps/g/a/hm;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/f/a/f;
    .locals 6
    .param p2    # Lcom/google/android/apps/gmm/map/r/a/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 85
    if-eqz p6, :cond_0

    .line 86
    invoke-virtual {p6}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 87
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/c/k;->f:F

    .line 99
    :goto_0
    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iget v3, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    .line 98
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v1

    .line 101
    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/f/a/g;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/k;->g:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 102
    iput-object v3, v2, Lcom/google/android/apps/gmm/map/f/a/g;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    iput v0, v2, Lcom/google/android/apps/gmm/map/f/a/g;->a:F

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/f/a/g;->d:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/a/g;->a()Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v0

    return-object v0

    .line 88
    :cond_0
    if-eqz p2, :cond_3

    .line 89
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    const/high16 v2, 0x43800000    # 256.0f

    mul-float/2addr v0, v2

    iget v2, p3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    const/high16 v1, 0x41f00000    # 30.0f

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    double-to-float v0, v2

    sget v2, Lcom/google/android/apps/gmm/navigation/c/k;->e:F

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    const v1, 0x3e4ccccd    # 0.2f

    sub-float/2addr v0, v1

    sget-object v1, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    sget-object v0, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v0

    .line 90
    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/c/k;->f:F

    goto :goto_0

    .line 89
    :cond_1
    sget-object v1, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v0

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/r/b/a/or;->b:Lcom/google/r/b/a/or;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/k;->a(Lcom/google/r/b/a/or;)F

    move-result v0

    goto :goto_1

    .line 94
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/c/k;->f:F

    goto/16 :goto_0
.end method
