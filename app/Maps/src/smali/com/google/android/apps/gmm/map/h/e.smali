.class public Lcom/google/android/apps/gmm/map/h/e;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/h/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/h/c;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/h/e;->a:Lcom/google/android/apps/gmm/map/h/c;

    .line 25
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 30
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/h/e;->a:Lcom/google/android/apps/gmm/map/h/c;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/h/c;->a(Z)V

    .line 39
    :goto_0
    return-void

    .line 32
    :cond_0
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/h/e;->a:Lcom/google/android/apps/gmm/map/h/c;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/h/c;->a(Z)V

    goto :goto_0

    .line 35
    :cond_1
    const-string v0, "PowerConnectionReceiver"

    const-string v1, "PowerConnectionReceiver should be only registered to ACTION_POWER_CONNECTED and ACTION_POWER_DISCONNECTED!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
