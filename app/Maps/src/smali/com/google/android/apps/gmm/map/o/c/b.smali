.class public Lcom/google/android/apps/gmm/map/o/c/b;
.super Lcom/google/android/apps/gmm/map/o/c/a;
.source "PG"


# instance fields
.field final c:Lcom/google/android/apps/gmm/map/internal/c/aa;

.field final d:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/c/aa;FLcom/google/android/apps/gmm/map/internal/d/c/b/f;)V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/gmm/map/o/c/a;-><init>(Lcom/google/android/apps/gmm/map/o/h;F)V

    .line 197
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/c/b;->c:Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 198
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/o/c/b;->d:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 199
    return-void
.end method


# virtual methods
.method protected final U_()Lcom/google/android/apps/gmm/v/by;
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/b;->a:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/c/b;->c:Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget v7, p0, Lcom/google/android/apps/gmm/map/o/c/b;->b:F

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/o/c/b;->d:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "#getTextureForLabelElement()"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v1, :cond_1

    move-object v1, v4

    :goto_0
    if-nez v1, :cond_3

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v1, :cond_2

    move-object v1, v4

    :goto_1
    if-nez v1, :cond_3

    :cond_0
    :goto_2
    return-object v4

    :cond_1
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    goto :goto_1

    :cond_3
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v1, :cond_4

    move-object v1, v4

    :goto_3
    if-eqz v1, :cond_a

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v1, :cond_5

    move-object v2, v4

    :goto_4
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v1, :cond_6

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    int-to-float v1, v1

    div-float v1, v5, v1

    :goto_5
    mul-float v6, v1, v7

    new-instance v1, Lcom/google/android/apps/gmm/map/o/m;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/map/o/m;-><init>(Ljava/lang/String;F)V

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v5

    if-eqz v5, :cond_7

    move-object v4, v5

    goto :goto_2

    :cond_4
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    goto :goto_3

    :cond_5
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    move-object v2, v1

    goto :goto_4

    :cond_6
    move v1, v5

    goto :goto_5

    :cond_7
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "#getTextureForIcon()"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/h;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    invoke-interface {v7, v2, v5, v9}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v5, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    if-eq v2, v11, :cond_9

    move-object v2, v4

    :cond_8
    :goto_6
    if-eqz v2, :cond_0

    iget v4, v0, Lcom/google/android/apps/gmm/map/o/h;->b:F

    mul-float v7, v6, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move v4, v3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/o/h;->a(Lcom/google/android/apps/gmm/map/o/k;Landroid/graphics/Bitmap;IIIIFZ)Lcom/google/android/apps/gmm/v/by;

    move-result-object v4

    goto :goto_2

    :cond_9
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-nez v2, :cond_8

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_6

    :cond_a
    new-instance v1, Lcom/google/android/apps/gmm/map/o/l;

    invoke-direct {v1, v6, v7}, Lcom/google/android/apps/gmm/map/o/l;-><init>(Lcom/google/android/apps/gmm/map/internal/c/aa;F)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v2

    if-eqz v2, :cond_b

    move-object v4, v2

    goto/16 :goto_2

    :cond_b
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/h;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    invoke-static {v6, v2, v10, v9}, Lcom/google/android/apps/gmm/map/o/h;->a(Lcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v9, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    if-eq v2, v11, :cond_e

    move-object v2, v4

    :cond_c
    :goto_7
    if-eqz v2, :cond_0

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v4, :cond_d

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    int-to-float v4, v4

    div-float/2addr v5, v4

    :cond_d
    iget v4, v0, Lcom/google/android/apps/gmm/map/o/h;->b:F

    mul-float/2addr v4, v5

    mul-float/2addr v7, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move v4, v3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/o/h;->a(Lcom/google/android/apps/gmm/map/o/k;Landroid/graphics/Bitmap;IIIIFZ)Lcom/google/android/apps/gmm/v/by;

    move-result-object v4

    goto/16 :goto_2

    :cond_e
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-nez v2, :cond_c

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_7
.end method
