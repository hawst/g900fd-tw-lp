.class public Lcom/google/android/apps/gmm/map/u/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/u/a/b;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/internal/d/as;

.field private final b:Lcom/google/android/apps/gmm/map/internal/d/b/ah;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/internal/d/as;Z)V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/u/a;->a:Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 72
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ah;

    if-nez p2, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 73
    :goto_0
    const/16 v2, 0x24

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/ah;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/u/a;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ah;

    .line 74
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/u/a;->c:Z

    .line 75
    return-void

    .line 73
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/d/as;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 188
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 189
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    move v1, v2

    move v3, v0

    .line 190
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 191
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 192
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/u/a;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ah;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v4

    .line 193
    if-nez v4, :cond_0

    .line 195
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/u/a;->a:Lcom/google/android/apps/gmm/map/internal/d/as;

    const/4 v6, 0x1

    invoke-interface {v4, v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v4

    .line 196
    if-eqz v4, :cond_0

    .line 197
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/u/a;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ah;

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v7

    :try_start_0
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v6, v0, v4}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_0
    if-eqz v4, :cond_1

    .line 201
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    const/4 v0, 0x0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 204
    add-int/lit8 v3, v3, -0x1

    .line 190
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 206
    :cond_1
    const-string v4, "RoadGraph"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1e

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "GetTiles: couldn\'t load tile: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v4, v0, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 209
    :cond_2
    if-nez p2, :cond_5

    .line 210
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/a/a;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/a/a;-><init>(I)V

    .line 212
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 213
    if-eqz v0, :cond_3

    .line 214
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/u/a;->a:Lcom/google/android/apps/gmm/map/internal/d/as;

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    goto :goto_2

    .line 218
    :cond_4
    const-wide/16 v6, 0x4e20

    :try_start_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/a/a;->b:Ljava/util/concurrent/CountDownLatch;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 222
    :goto_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/a;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ah;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v4

    :try_start_3
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 224
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 220
    :catch_0
    move-exception v0

    const-string v0, "RoadGraph"

    const-string v3, "Interupted while waiting for tiles"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 223
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 227
    :cond_5
    return-object v5
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/u/a/e;
    .locals 10
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/4 v5, -0x2

    const/16 v9, 0xe

    const/4 v2, 0x0

    .line 131
    .line 133
    if-eqz p2, :cond_6

    .line 135
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 134
    invoke-static {v9, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    .line 137
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/lit8 v3, v3, 0x1

    .line 136
    invoke-static {v9, v0, v3, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    move-object v3, v1

    move-object v1, v0

    .line 146
    :goto_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x2

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p1, v0, v6}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 147
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p1, v0, v7}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 151
    iget v0, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v8, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v0, v8, :cond_3

    move v0, v4

    .line 152
    :goto_1
    iget v8, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-le v8, v6, :cond_4

    .line 153
    :goto_2
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5, v0, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 154
    invoke-virtual {v7, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 153
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 156
    invoke-static {v4, v9}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;I)Ljava/util/List;

    move-result-object v0

    .line 157
    const/4 v4, 0x1

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/gmm/map/u/a;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v4

    .line 158
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 159
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v6

    .line 160
    if-eqz v1, :cond_5

    if-eqz v3, :cond_5

    .line 162
    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v8, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-gt v7, v8, :cond_1

    .line 163
    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v8, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-lt v7, v8, :cond_1

    .line 164
    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v8, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-gt v7, v8, :cond_1

    .line 165
    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v8, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-ge v7, v8, :cond_5

    .line 166
    :cond_1
    const-string v0, "RoadGraph"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Requested tile outside of marked available area: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    :cond_2
    :goto_3
    return-object v2

    :cond_3
    move v0, v5

    .line 151
    goto :goto_1

    :cond_4
    move v4, v5

    .line 152
    goto :goto_2

    .line 169
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 170
    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/u/a/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/map/u/a/e;

    move-result-object v2

    goto :goto_3

    :cond_6
    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;DLcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/Iterator;
    .locals 4
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "D",
            "Lcom/google/android/apps/gmm/map/b/a/ae;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 101
    double-to-int v0, p2

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 102
    if-eqz p4, :cond_0

    .line 103
    invoke-virtual {v0, p4}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 106
    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    const/16 v0, 0xe

    .line 105
    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;I)Ljava/util/List;

    move-result-object v0

    .line 107
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/u/a;->c:Z

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/map/u/a;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    .line 108
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 109
    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/c;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/u/b;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/u/b;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
