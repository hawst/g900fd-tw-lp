.class public Lcom/google/android/apps/gmm/search/ap;
.super Lcom/google/android/apps/gmm/base/placelists/a/a;
.source "PG"


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/search/i;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/search/i;",
            ">;"
        }
    .end annotation
.end field

.field private transient f:Lcom/google/android/apps/gmm/map/b/a/t;

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/search/f;",
            ">;"
        }
    .end annotation
.end field

.field private transient i:Lcom/google/e/a/a/a/b;

.field private transient j:Lcom/google/e/a/a/a/b;

.field private transient k:Lcom/google/e/a/a/a/b;

.field private transient l:Lcom/google/android/apps/gmm/map/r/a/e;

.field private m:Lcom/google/maps/g/qm;

.field private n:I

.field private o:Z

.field private p:Lcom/google/android/apps/gmm/base/g/c;

.field private q:Lcom/google/e/a/a/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/ap;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/t;

    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/maps/b/b/c;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/t;-><init>(Lcom/google/e/a/a/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->f:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ap;->g:Z

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->h:Ljava/util/List;

    .line 58
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bg;->g:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->i:Lcom/google/e/a/a/a/b;

    .line 60
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bd;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->j:Lcom/google/e/a/a/a/b;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->k:Lcom/google/e/a/a/a/b;

    .line 73
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->m:Lcom/google/maps/g/qm;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->c:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->d:Ljava/util/List;

    return-void
.end method

.method private declared-synchronized x()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 417
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v1

    const/16 v2, 0xd

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 420
    :cond_0
    monitor-exit p0

    return v0

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/base/placelists/a/a;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 351
    monitor-enter p0

    :try_start_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/search/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 354
    :cond_1
    :try_start_1
    check-cast p1, Lcom/google/android/apps/gmm/search/ap;

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ap;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    const/4 v2, 0x2

    .line 360
    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ap;->g:Z

    .line 362
    if-eqz p2, :cond_2

    .line 363
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/ap;->f:Lcom/google/android/apps/gmm/map/b/a/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->f:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 365
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/search/c/a;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ap;->i()Lcom/google/e/a/a/a/b;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/search/c/a;-><init>(Lcom/google/e/a/a/a/b;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/android/apps/gmm/search/c/a;)V

    .line 367
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 371
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    const/16 v3, 0xa

    const/16 v4, 0x1a

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 373
    if-eqz v0, :cond_3

    const/4 v3, 0x2

    .line 374
    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 375
    sget-object v0, Lcom/google/android/apps/gmm/search/ap;->e:Ljava/lang/String;

    const-string v1, "Secondary AdsResponses are not currently supported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 378
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/search/bc;->a(Lcom/google/e/a/a/a/b;Ljava/util/List;)V

    .line 381
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/search/ap;->a(Ljava/util/List;)V

    .line 383
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 384
    const/16 v1, 0xc

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v1

    .line 374
    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/MapFragment;)V
    .locals 3

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 394
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 395
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/search/e;

    .line 399
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/search/e;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;)V

    .line 400
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    const-string v2, "clientMeasles"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    monitor-exit p0

    return-void

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/e/a/a/a/b;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 126
    monitor-enter p0

    if-nez p1, :cond_1

    .line 237
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 129
    :cond_1
    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/ap;->j:Lcom/google/e/a/a/a/b;

    .line 130
    const/4 v0, 0x6

    .line 137
    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 138
    if-eqz v0, :cond_2

    .line 141
    const/4 v1, 0x2

    .line 142
    const/16 v5, 0x1c

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 143
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    .line 147
    :cond_2
    const/16 v0, 0xd

    .line 150
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v1

    .line 147
    if-eqz p1, :cond_3

    const/16 v4, 0x19

    invoke-virtual {p1, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    :goto_2
    check-cast v0, Lcom/google/maps/g/qm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->m:Lcom/google/maps/g/qm;

    .line 152
    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/search/ap;->n:I

    .line 154
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ap;->o:Z

    .line 158
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 161
    const/16 v0, 0xa

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->c:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/g/a;->a(Lcom/google/e/a/a/a/b;Z)[Lcom/google/e/a/a/a/b;

    move-result-object v6

    if-eqz v6, :cond_7

    array-length v7, v6

    move v1, v3

    :goto_3
    if-ge v1, v7, :cond_7

    aget-object v8, v6, v1

    const/4 v4, 0x7

    iget-object v9, v8, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_5

    new-instance v4, Lcom/google/android/apps/gmm/search/j;

    new-instance v9, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    iget-object v11, v9, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-boolean v10, v11, Lcom/google/android/apps/gmm/base/g/h;->c:Z

    invoke-virtual {v9, v8}, Lcom/google/android/apps/gmm/base/g/g;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/google/android/apps/gmm/search/j;-><init>(Lcom/google/android/apps/gmm/base/g/c;)V

    :goto_4
    invoke-interface {v4}, Lcom/google/android/apps/gmm/search/i;->c()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/gmm/search/ap;->c:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v4

    .line 147
    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2

    .line 164
    :cond_5
    new-instance v4, Lcom/google/android/apps/gmm/search/bd;

    new-instance v9, Lcom/google/android/apps/gmm/base/g/a;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v9, v10, v8, v11}, Lcom/google/android/apps/gmm/base/g/a;-><init>(Lcom/google/android/apps/gmm/map/g/a;Lcom/google/e/a/a/a/b;Z)V

    invoke-direct {v4, v9}, Lcom/google/android/apps/gmm/search/bd;-><init>(Lcom/google/android/apps/gmm/base/g/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164
    :cond_6
    :try_start_1
    iget-object v8, p0, Lcom/google/android/apps/gmm/search/ap;->d:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 167
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/i;

    .line 173
    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/i;->a()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 174
    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/i;->b()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 180
    :cond_9
    invoke-static {p1, v5}, Lcom/google/android/apps/gmm/search/bc;->a(Lcom/google/e/a/a/a/b;Ljava/util/List;)V

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/i;

    .line 185
    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/i;->a()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 186
    invoke-interface {v1}, Lcom/google/android/apps/gmm/search/i;->b()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 191
    :cond_b
    const/4 v1, 0x2

    const/4 v4, 0x0

    invoke-static {p1, v1, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/ap;->g:Z

    .line 195
    const/4 v1, 0x3

    const/16 v4, 0x1a

    invoke-virtual {p1, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    .line 196
    if-eqz v1, :cond_c

    .line 197
    new-instance v4, Lcom/google/android/apps/gmm/search/c/a;

    invoke-direct {v4, v1}, Lcom/google/android/apps/gmm/search/c/a;-><init>(Lcom/google/e/a/a/a/b;)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/android/apps/gmm/search/c/a;)V

    .line 200
    :cond_c
    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 210
    array-length v4, v1

    if-lez v4, :cond_d

    .line 211
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/t;

    const/4 v6, 0x0

    aget-object v1, v1, v6

    invoke-direct {v4, v1}, Lcom/google/android/apps/gmm/map/b/a/t;-><init>(Lcom/google/e/a/a/a/b;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/search/ap;->f:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 215
    :cond_d
    const/4 v1, 0x7

    .line 216
    const/16 v4, 0x1a

    invoke-virtual {p1, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    .line 217
    if-eqz v1, :cond_e

    .line 218
    invoke-static {v1}, Lcom/google/android/apps/gmm/search/bc;->a(Lcom/google/e/a/a/a/b;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->h:Ljava/util/List;

    .line 223
    :cond_e
    const/16 v1, 0x9

    const/16 v4, 0x1a

    invoke-virtual {p1, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->i:Lcom/google/e/a/a/a/b;

    .line 225
    const/16 v1, 0x11

    .line 226
    const/16 v4, 0x1a

    invoke-virtual {p1, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    .line 227
    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v4

    .line 225
    invoke-static {v1, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/agl;

    .line 228
    if-eqz v1, :cond_f

    iget v4, v1, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v2, :cond_10

    move v4, v2

    :goto_8
    if-eqz v4, :cond_f

    .line 229
    iget-object v4, v1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v4, :cond_11

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v4

    :goto_9
    iget v4, v4, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v2, :cond_12

    :goto_a
    if-eqz v2, :cond_f

    .line 230
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/search/ap;->l:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 234
    :cond_f
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/search/ap;->a(Ljava/util/List;)V

    .line 236
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/g/a;->b(Lcom/google/e/a/a/a/b;Z)Lcom/google/e/a/a/a/b;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ap;->p:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    const/4 v1, 0x7

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/g/g;->d:Z

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/base/g/h;->c:Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/g/g;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->p:Lcom/google/android/apps/gmm/base/g/c;

    goto/16 :goto_0

    :cond_10
    move v4, v3

    .line 228
    goto :goto_8

    .line 229
    :cond_11
    iget-object v4, v1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_9

    :cond_12
    move v2, v3

    goto :goto_a
.end method

.method public final declared-synchronized h()Z
    .locals 1

    .prologue
    .line 448
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/ap;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()Z
    .locals 1

    .prologue
    .line 430
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()V
    .locals 2

    .prologue
    .line 472
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->l()V

    .line 473
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ap;->g:Z

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 475
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bg;->g:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->i:Lcom/google/e/a/a/a/b;

    .line 476
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bd;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->j:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477
    monitor-exit p0

    return-void

    .line 472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized m()Lcom/google/android/apps/gmm/base/g/c;
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->p:Lcom/google/android/apps/gmm/base/g/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized n()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->j:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o()Ljava/lang/String;
    .locals 3

    .prologue
    .line 404
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 405
    if-eqz v0, :cond_0

    .line 406
    const/16 v1, 0xc

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized p()Lcom/google/android/apps/gmm/map/b/a/t;
    .locals 1

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->f:Lcom/google/android/apps/gmm/map/b/a/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized q()Lcom/google/maps/g/qm;
    .locals 1

    .prologue
    .line 425
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->m:Lcom/google/maps/g/qm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/search/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->h:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized s()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 480
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->i:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized t()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 484
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/apps/gmm/search/ap;->n:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/search/ap;->n:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 485
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v1

    if-gt v1, v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/gmm/search/ap;->x()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized u()I
    .locals 1

    .prologue
    .line 489
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/search/ap;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized v()Z
    .locals 1

    .prologue
    .line 493
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/ap;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized w()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ap;->q:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
