.class public Lcom/google/android/apps/gmm/navigation/a/bf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/a/c;


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/a/a/b;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/gmm/base/a;

.field private volatile d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->b:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->c:Lcom/google/android/apps/gmm/base/a;

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    .line 47
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/a/d;Lcom/google/android/apps/gmm/navigation/a/a/b;)V
    .locals 1

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bg;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/navigation/a/bg;-><init>(Lcom/google/android/apps/gmm/navigation/a/bf;Lcom/google/android/apps/gmm/navigation/a/a/d;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a(Landroid/speech/tts/TextToSpeech$OnInitListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/navigation/a/a/b;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    return-object v0
.end method

.method declared-synchronized a(ILcom/google/android/apps/gmm/navigation/a/a/d;)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v1, -0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    monitor-enter p0

    .line 95
    if-nez p1, :cond_a

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/aa/b/a;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v4

    .line 101
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xe

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "voiceLocale = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a()Ljava/util/Locale;

    move-result-object v5

    .line 105
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x18

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "userSelectedTtsLocale = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/navigation/a/a/b;->c(Ljava/util/Locale;)I

    move-result v0

    .line 107
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x23

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "isLanguageAvailable("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/navigation/a/bh;->a(Ljava/util/Locale;Ljava/util/Locale;)Z

    move-result v6

    .line 116
    if-nez v6, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a(Ljava/util/Locale;)I

    move-result v0

    .line 124
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x4e

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Overriding TTS language "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with preferred voice locale "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with result: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 127
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x22

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "textToSpeech.getLanguage() is now "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_0
    if-eq v0, v1, :cond_1

    if-ne v0, v9, :cond_3

    :cond_1
    move v1, v0

    move v2, v3

    .line 139
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->d:Z

    .line 141
    if-nez p1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    .line 142
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a()Ljava/util/Locale;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "TTS Initialized: status:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " supported:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " locale:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    if-eqz p2, :cond_2

    .line 145
    if-eqz v2, :cond_8

    .line 146
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/navigation/a/a/d;->a(I)V

    .line 155
    :cond_2
    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    monitor-exit p0

    return-void

    .line 133
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/navigation/a/a/b;->d()Ljava/lang/String;

    move-result-object v1

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->a:Lcom/google/android/apps/gmm/navigation/a/a/b;

    .line 135
    if-eqz v1, :cond_4

    const-string v4, "com.svox.pico"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/bc;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/navigation/a/bc;-><init>()V

    .line 134
    :goto_3
    invoke-interface {v3, v1}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a(Lcom/google/android/apps/gmm/navigation/a/a/a;)V

    move v1, v0

    goto :goto_0

    .line 135
    :cond_5
    const-string v4, "com.google.android.tts"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/ba;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/navigation/a/ba;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 135
    :cond_6
    :try_start_2
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/be;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/navigation/a/be;-><init>()V

    goto :goto_3

    .line 142
    :cond_7
    const-string v0, ""

    goto :goto_1

    .line 147
    :cond_8
    if-ne v1, v9, :cond_9

    .line 148
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/navigation/a/a/d;->a(I)V

    goto :goto_2

    .line 150
    :cond_9
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/navigation/a/a/d;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_a
    move v2, v3

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/a/d;Lcom/google/android/apps/gmm/navigation/logging/m;)V
    .locals 3

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bh;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bf;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/navigation/a/bh;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/logging/m;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/navigation/a/bf;->a(Lcom/google/android/apps/gmm/navigation/a/a/d;Lcom/google/android/apps/gmm/navigation/a/a/b;)V

    .line 60
    return-void
.end method
