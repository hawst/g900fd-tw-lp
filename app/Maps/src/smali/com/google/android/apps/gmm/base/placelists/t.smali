.class Lcom/google/android/apps/gmm/base/placelists/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:F

.field final synthetic d:Lcom/google/android/apps/gmm/base/placelists/s;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/placelists/s;IIF)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/t;->d:Lcom/google/android/apps/gmm/base/placelists/s;

    iput p2, p0, Lcom/google/android/apps/gmm/base/placelists/t;->a:I

    iput p3, p0, Lcom/google/android/apps/gmm/base/placelists/t;->b:I

    iput p4, p0, Lcom/google/android/apps/gmm/base/placelists/t;->c:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 146
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/t;->d:Lcom/google/android/apps/gmm/base/placelists/s;

    iget v4, p0, Lcom/google/android/apps/gmm/base/placelists/t;->a:I

    iget v5, p0, Lcom/google/android/apps/gmm/base/placelists/t;->b:I

    iget v0, p0, Lcom/google/android/apps/gmm/base/placelists/t;->c:F

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/placelists/s;->f:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/placelists/s;->e:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v2, :cond_1

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/placelists/s;->f:Lcom/google/android/apps/gmm/base/placelists/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v2, v10, v10, v1}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;IZLjava/lang/Float;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/placelists/s;->e:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/ah;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    :goto_1
    iget-object v2, v3, Lcom/google/android/apps/gmm/base/placelists/s;->a:Landroid/graphics/Rect;

    iget v7, v3, Lcom/google/android/apps/gmm/base/placelists/s;->b:I

    sub-int v7, v4, v7

    div-int/lit8 v7, v7, 0x2

    iget v8, v3, Lcom/google/android/apps/gmm/base/placelists/s;->c:I

    sub-int v8, v5, v8

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v2, v7, v8}, Landroid/graphics/Rect;->offset(II)V

    new-instance v7, Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    sget-object v8, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v7, v2, v8}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v9, :cond_4

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v2, v8}, Lcom/google/android/apps/gmm/map/ah;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    :goto_2
    iget-object v8, v3, Lcom/google/android/apps/gmm/base/placelists/s;->a:Landroid/graphics/Rect;

    invoke-static {v7, v0, v2, v8}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v3, Lcom/google/android/apps/gmm/base/placelists/s;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v6, v0, v2, v10, v1}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;)V

    :cond_2
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/placelists/s;->e:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/placelists/s;->a:Landroid/graphics/Rect;

    iput v4, v3, Lcom/google/android/apps/gmm/base/placelists/s;->b:I

    iput v5, v3, Lcom/google/android/apps/gmm/base/placelists/s;->c:I

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_2
.end method
