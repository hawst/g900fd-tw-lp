.class public final Lcom/google/android/apps/gmm/mylocation/c/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/mylocation/c/f;",
            "Lcom/google/android/apps/gmm/mylocation/c/j;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field public c:Z

.field public final d:Ljava/lang/Object;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/google/android/apps/gmm/map/t;

.field private final g:Lcom/google/android/apps/gmm/mylocation/d/e;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->a:Ljava/util/HashMap;

    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/c/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mylocation/c/i;-><init>(Lcom/google/android/apps/gmm/mylocation/c/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->d:Ljava/lang/Object;

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->e:Landroid/content/res/Resources;

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->f:Lcom/google/android/apps/gmm/map/t;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    .line 60
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/t;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/mylocation/d/e;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->g:Lcom/google/android/apps/gmm/mylocation/d/e;

    .line 61
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/c/f;

    .line 125
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mylocation/c/f;->b()V

    goto :goto_0

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->b:Z

    .line 128
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/util/b/a/a;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->d:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 101
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->c:Z

    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->b:Z

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/c/g;->a()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/c/j;

    .line 106
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mylocation/c/j;->a()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/c/j;->k:Lcom/google/android/apps/gmm/v/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 109
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/c/f;)V
    .locals 6

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->c:Z

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "destroy(EventBus) has already been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    :goto_0
    return-void

    .line 75
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/c/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->e:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->g:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->f:Lcom/google/android/apps/gmm/map/t;

    .line 76
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/apps/gmm/mylocation/c/f;->b:Lcom/google/android/apps/gmm/mylocation/c/c;

    .line 77
    iget-object v5, p1, Lcom/google/android/apps/gmm/mylocation/c/f;->c:Lcom/google/android/apps/gmm/map/t/k;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/mylocation/c/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/mylocation/d/e;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/mylocation/c/c;Lcom/google/android/apps/gmm/map/t/k;)V

    .line 78
    new-instance v1, Lcom/google/android/apps/gmm/mylocation/c/h;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/mylocation/c/h;-><init>(Lcom/google/android/apps/gmm/mylocation/c/g;Lcom/google/android/apps/gmm/mylocation/c/j;)V

    iget-object v2, p1, Lcom/google/android/apps/gmm/mylocation/c/f;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/c/g;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
