.class public Lcom/google/android/apps/gmm/search/aq;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"


# static fields
.field static final a:Ljava/util/regex/Pattern;

.field public static final b:I

.field public static final c:I

.field private static final h:Ljava/lang/String;


# instance fields
.field public f:Lcom/google/android/apps/gmm/search/ah;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field volatile g:Lcom/google/maps/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private i:Lcom/google/maps/g/qm;

.field private j:Lcom/google/android/apps/gmm/place/aa;

.field private final k:Lcom/google/android/apps/gmm/map/r;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 97
    const-string v0, "^\\s*ok(?:ay)?\\s*maps?\\s*$"

    const/4 v1, 0x2

    .line 98
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/aq;->a:Ljava/util/regex/Pattern;

    .line 100
    const/16 v0, 0xa

    sput v0, Lcom/google/android/apps/gmm/search/aq;->b:I

    .line 102
    const/16 v0, 0x14

    sput v0, Lcom/google/android/apps/gmm/search/aq;->c:I

    .line 117
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/aq;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/search/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/ar;-><init>(Lcom/google/android/apps/gmm/search/aq;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->k:Lcom/google/android/apps/gmm/map/r;

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 793
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 794
    sget v0, Lcom/google/android/apps/gmm/search/aq;->c:I

    .line 796
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/search/aq;->b:I

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 753
    if-nez p1, :cond_1

    .line 769
    :cond_0
    :goto_0
    return-void

    .line 756
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    .line 757
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ai;->e()Lcom/google/maps/g/hy;

    move-result-object v3

    .line 758
    if-eqz v3, :cond_0

    .line 762
    iget v0, v3, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 763
    iget v0, v3, Lcom/google/maps/g/hy;->f:I

    invoke-static {v0}, Lcom/google/b/f/cj;->a(I)Lcom/google/b/f/cj;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/b/f/cj;->a:Lcom/google/b/f/cj;

    :cond_2
    sget-object v4, Lcom/google/b/f/cj;->f:Lcom/google/b/f/cj;

    if-eq v0, v4, :cond_4

    .line 764
    :cond_3
    iget-object v0, v3, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b;->d()Lcom/google/b/f/b;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    iget v0, v0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 765
    iget-object v0, v3, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b;->d()Lcom/google/b/f/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    iget v0, v0, Lcom/google/b/f/b;->d:I

    sget-object v3, Lcom/google/b/f/t;->gr:Lcom/google/b/f/t;

    .line 766
    iget v3, v3, Lcom/google/b/f/t;->gy:I

    if-ne v0, v3, :cond_0

    .line 767
    :cond_4
    iget-object v7, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v0

    if-ne v0, v1, :cond_5

    invoke-virtual {v7, v6}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/startpage/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/b/b;->d:Lcom/google/android/apps/gmm/startpage/b/b;

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/b/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/apps/gmm/startpage/b/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-interface {v8, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v8

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/search/ap;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v6

    new-instance v1, Lcom/google/android/apps/gmm/startpage/b/a;

    sget-object v5, Lcom/google/android/apps/gmm/startpage/b/b;->a:Lcom/google/android/apps/gmm/startpage/b/b;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/startpage/b/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/apps/gmm/startpage/b/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-interface {v8, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_6
    move v0, v6

    .line 762
    goto/16 :goto_1

    :cond_7
    move v0, v6

    .line 764
    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/search/aq;)Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/search/aq;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/search/aq;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/search/aq;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/gmm/search/aq;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 4

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 150
    new-instance v1, Lcom/google/android/apps/gmm/search/ah;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/search/ah;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/aq;->f:Lcom/google/android/apps/gmm/search/ah;

    .line 152
    new-instance v1, Lcom/google/android/apps/gmm/search/restriction/c/k;

    .line 156
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 157
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v2

    .line 158
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v3

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/google/android/apps/gmm/search/restriction/c/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    invoke-static {}, Lcom/google/maps/g/qm;->newBuilder()Lcom/google/maps/g/qo;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/restriction/c/k;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/restriction/d/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/d/a;->a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/maps/g/qo;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->i:Lcom/google/maps/g/qm;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/aq;->k:Lcom/google/android/apps/gmm/map/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/r;)V

    .line 162
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/base/placelists/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 541
    if-nez p2, :cond_0

    .line 542
    new-instance p2, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {p2}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    .line 544
    :cond_0
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/base/placelists/a/e;->j:Z

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 548
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/search/aq;->a:Ljava/util/regex/Pattern;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1c

    invoke-virtual {v0, v3, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->c()V

    .line 560
    :goto_0
    return-void

    .line 553
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 554
    new-instance v1, Lcom/google/android/apps/gmm/search/al;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 555
    invoke-static {v1}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 558
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchLoadingFragment;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/aj;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 429
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 430
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 431
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/a;->a()Lcom/google/maps/g/a/hm;

    .line 434
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 435
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 436
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/b/a/a;->d()Ljava/lang/String;

    move-result-object v6

    .line 438
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    .line 432
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/apps/gmm/search/aj;->f:Lcom/google/android/apps/gmm/map/internal/d/bd;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v1

    sget-object v2, Lcom/google/o/b/a/a/a;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/apps/gmm/search/aj;->g:Lcom/google/e/a/a/a/b;

    :cond_0
    sget-object v7, Lcom/google/android/apps/gmm/search/au;->a:[I

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v8

    const/4 v2, 0x0

    iget-object v1, v8, Lcom/google/android/apps/gmm/shared/c/c/c;->b:Lcom/google/android/apps/gmm/shared/b/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;

    move-result-object v1

    if-eqz v1, :cond_4

    :goto_0
    invoke-virtual {v1}, Lcom/google/maps/g/a/al;->ordinal()I

    move-result v1

    aget v1, v7, v1

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lcom/google/r/b/a/acc;->a:Lcom/google/r/b/a/acc;

    :goto_1
    iput-object v1, p1, Lcom/google/android/apps/gmm/search/aj;->C:Lcom/google/r/b/a/acc;

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    :cond_1
    move v1, v3

    :goto_2
    if-nez v1, :cond_2

    iput-object v6, p1, Lcom/google/android/apps/gmm/search/aj;->z:Ljava/lang/String;

    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/aj;->k:Lcom/google/e/a/a/a/b;

    if-nez v1, :cond_3

    invoke-static {v5, v2}, Lcom/google/android/apps/gmm/search/bc;->a(Landroid/graphics/Point;Landroid/content/res/Resources;)Lcom/google/e/a/a/a/b;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/apps/gmm/search/aj;->k:Lcom/google/e/a/a/a/b;

    :cond_3
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget v1, Lcom/google/android/apps/gmm/search/aq;->c:I

    :goto_3
    iput v1, p1, Lcom/google/android/apps/gmm/search/aj;->b:I

    sget v1, Lcom/google/android/apps/gmm/e;->bb:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p1, Lcom/google/android/apps/gmm/search/aj;->i:I

    sget v1, Lcom/google/android/apps/gmm/e;->ba:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p1, Lcom/google/android/apps/gmm/search/aj;->j:I

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/i/f;->a(Landroid/content/res/Resources;)Lcom/google/r/b/a/ado;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/apps/gmm/search/aj;->A:Lcom/google/r/b/a/ado;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->l()Lcom/google/r/b/a/wn;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/r/b/a/wn;->i:Z

    if-eqz v1, :cond_8

    move v1, v3

    :goto_4
    iput-boolean v1, p1, Lcom/google/android/apps/gmm/search/aj;->x:Z

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->a()V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/aky;->newBuilder()Lcom/google/r/b/a/ala;

    move-result-object v0

    const/4 v1, 0x2

    iget v2, v0, Lcom/google/r/b/a/ala;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/r/b/a/ala;->a:I

    iput v1, v0, Lcom/google/r/b/a/ala;->b:I

    invoke-virtual {v0}, Lcom/google/r/b/a/ala;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aky;

    iput-object v0, p1, Lcom/google/android/apps/gmm/search/aj;->B:Lcom/google/r/b/a/aky;

    .line 439
    return-void

    .line 432
    :cond_4
    if-eqz v2, :cond_5

    move-object v1, v2

    goto/16 :goto_0

    :cond_5
    iget-object v1, v8, Lcom/google/android/apps/gmm/shared/c/c/c;->c:Lcom/google/maps/g/a/al;

    goto/16 :goto_0

    :pswitch_0
    sget-object v1, Lcom/google/r/b/a/acc;->b:Lcom/google/r/b/a/acc;

    goto/16 :goto_1

    :pswitch_1
    sget-object v1, Lcom/google/r/b/a/acc;->c:Lcom/google/r/b/a/acc;

    goto/16 :goto_1

    :pswitch_2
    sget-object v1, Lcom/google/r/b/a/acc;->c:Lcom/google/r/b/a/acc;

    goto/16 :goto_1

    :cond_6
    move v1, v4

    goto/16 :goto_2

    :cond_7
    sget v1, Lcom/google/android/apps/gmm/search/aq;->b:I

    goto :goto_3

    :cond_8
    move v1, v4

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/base/placelists/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->g:Lcom/google/maps/a/a;

    if-eqz v0, :cond_0

    .line 406
    :goto_0
    if-nez v0, :cond_1

    .line 416
    :goto_1
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    goto :goto_0

    .line 409
    :cond_1
    iput-object v0, p1, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->i:Lcom/google/maps/g/qm;

    iput-object v0, p1, Lcom/google/android/apps/gmm/search/aj;->p:Lcom/google/maps/g/qm;

    .line 414
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;)V

    .line 415
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;Lcom/google/android/apps/gmm/base/placelists/a/e;)V
    .locals 7
    .param p4    # Lcom/google/android/apps/gmm/base/placelists/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 269
    .line 270
    new-instance v4, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v4, p2}, Lcom/google/android/apps/gmm/z/b/f;-><init>(Lcom/google/maps/g/hy;)V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/suggest/d/e;->b()Lcom/google/b/f/bf;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/bf;)Lcom/google/android/apps/gmm/z/b/f;

    :cond_0
    iget-object v5, p1, Lcom/google/android/apps/gmm/suggest/e/d;->n:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_0
    if-eq v5, v0, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->n:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, v4, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v3, p2, Lcom/google/maps/g/hy;->b:Ljava/lang/Object;

    :cond_2
    move-object v0, v3

    goto :goto_0

    :cond_3
    iget v5, v3, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/maps/g/ia;->a:I

    iput-object v0, v3, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    :cond_4
    new-instance v3, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->m:[B

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->e:[B

    iget-object v0, v4, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->k:Lcom/google/e/a/a/a/b;

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->o:Lcom/google/e/a/a/a/b;

    .line 271
    if-nez p4, :cond_6

    new-instance p4, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {p4}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    :cond_6
    iget v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->b:I

    const/16 v4, 0x9

    if-ne v0, v4, :cond_8

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->e:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/apps/gmm/base/placelists/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v1, p4, Lcom/google/android/apps/gmm/base/placelists/a/e;->b:Ljava/lang/String;

    .line 272
    :cond_7
    :goto_1
    invoke-virtual {p0, v3, p4}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 273
    return-void

    .line 271
    :cond_8
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_a

    :cond_9
    move v0, v2

    :goto_2
    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_3
    if-nez v0, :cond_c

    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/f;->b:Lcom/google/android/apps/gmm/base/placelists/a/f;

    iput-object v0, p4, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    goto :goto_1

    :cond_a
    move v0, v1

    goto :goto_2

    :cond_b
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/d;->a:Ljava/lang/String;

    const-string v4, "AliasType was set to an unknown value"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_3

    :cond_c
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_4
    if-ne v0, v2, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/f;->c:Lcom/google/android/apps/gmm/base/placelists/a/f;

    iput-object v0, p4, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    goto :goto_1

    :cond_d
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/d;->a:Ljava/lang/String;

    const-string v4, "AliasType was set to an unknown value"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    sget-object v2, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    .line 241
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/o/h/a/dq;)Z

    move-result v2

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 243
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 244
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/search/SearchStartPageFragment;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 245
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 663
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 664
    iget-object v1, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->b()I

    move-result v1

    if-lez v1, :cond_0

    .line 665
    iget-object v1, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/search/ap;->e(I)V

    .line 668
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 670
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 671
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/search/SearchFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchFragment;

    move-result-object v0

    .line 673
    if-eqz p2, :cond_1

    .line 674
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 678
    :goto_0
    return-void

    .line 676
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    sget-object v2, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    .line 225
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/o/h/a/dq;)Z

    move-result v2

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 227
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 228
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->a(Lcom/google/android/apps/gmm/x/a;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/search/SearchStartPageFragment;

    move-result-object v0

    .line 227
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 229
    return-void
.end method

.method final b(Ljava/lang/String;)Landroid/view/View;
    .locals 4

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    .line 340
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->r:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    const/4 v1, 0x2

    .line 341
    iput v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    sget v1, Lcom/google/android/apps/gmm/f;->cy:I

    .line 342
    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    new-instance v1, Lcom/google/android/apps/gmm/search/as;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/search/as;-><init>(Lcom/google/android/apps/gmm/search/aq;)V

    .line 343
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 353
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 355
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    .line 356
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    .line 357
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->pq:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/i;->b:Ljava/lang/CharSequence;

    .line 358
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/search/at;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/search/at;-><init>(Lcom/google/android/apps/gmm/search/aq;)V

    .line 359
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 367
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    .line 368
    new-instance v0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 369
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->setProperties(Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 370
    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/x/o;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 589
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v3, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 607
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->b()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    .line 608
    :goto_0
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->j()Z

    move-result v4

    if-eqz v4, :cond_5

    if-nez v0, :cond_5

    .line 609
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v3, 0x19

    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3

    :goto_1
    if-eqz v1, :cond_0

    .line 610
    sget-object v0, Lcom/google/android/apps/gmm/search/aq;->h:Ljava/lang/String;

    .line 613
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v3, Lcom/google/android/apps/gmm/search/q;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    invoke-direct {v3, v2, p1}, Lcom/google/android/apps/gmm/search/q;-><init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/x/o;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->r()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/f;

    new-instance v3, Lcom/google/android/apps/gmm/search/r;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/search/r;-><init>(Lcom/google/android/apps/gmm/search/f;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    move v0, v2

    .line 607
    goto :goto_0

    :cond_3
    move v1, v2

    .line 609
    goto :goto_1

    .line 613
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    new-instance v2, Lcom/google/android/apps/gmm/didyoumean/g;

    invoke-direct {v2, v1, p1}, Lcom/google/android/apps/gmm/didyoumean/g;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/x/o;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/didyoumean/a;)Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 636
    :goto_3
    return-void

    .line 614
    :cond_5
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v0

    if-nez v0, :cond_6

    .line 620
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    const/16 v3, 0xc

    .line 619
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    .line 622
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    .line 623
    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    new-array v0, v1, [Lcom/google/b/f/cq;

    sget-object v4, Lcom/google/b/f/t;->bR:Lcom/google/b/f/t;

    aput-object v4, v0, v2

    .line 624
    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 625
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 627
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 628
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/l;->mM:I

    new-array v6, v1, [Ljava/lang/Object;

    .line 629
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v7, 0x1c

    invoke-virtual {v0, v1, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v6, v2

    .line 628
    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 627
    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 630
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 634
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v3, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->t()Z

    move-result v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v4, :cond_7

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->d:Lcom/google/android/apps/gmm/base/placelists/a/h;

    sget-object v4, Lcom/google/android/apps/gmm/base/placelists/a/h;->a:Lcom/google/android/apps/gmm/base/placelists/a/h;

    if-ne v0, v4, :cond_7

    move v2, v1

    :cond_7
    if-nez v3, :cond_8

    if-eqz v2, :cond_9

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/al;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchListFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_3

    :cond_9
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/x/o;Z)V

    goto/16 :goto_3
.end method

.method public final c()Lcom/google/android/apps/gmm/place/aa;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->j:Lcom/google/android/apps/gmm/place/aa;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Lcom/google/android/apps/gmm/place/aa;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/aa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->j:Lcom/google/android/apps/gmm/place/aa;

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/aq;->j:Lcom/google/android/apps/gmm/place/aa;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/aq;->k:Lcom/google/android/apps/gmm/map/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o;->b(Lcom/google/android/apps/gmm/map/r;)V

    .line 168
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 169
    return-void
.end method
