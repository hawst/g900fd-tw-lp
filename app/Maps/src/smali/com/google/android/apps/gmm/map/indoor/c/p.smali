.class public Lcom/google/android/apps/gmm/map/indoor/c/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/map/indoor/c/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/map/indoor/c/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/indoor/c/p;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/indoor/c/o;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    packed-switch p2, :pswitch_data_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    invoke-interface {v0, p1, v4, v3}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    .line 70
    :goto_0
    return-void

    .line 43
    :pswitch_0
    if-eqz p3, :cond_4

    .line 44
    check-cast p3, Lcom/google/android/apps/gmm/map/internal/c/cm;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/internal/c/cm;->h()Lcom/google/android/apps/gmm/map/internal/c/cp;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v2

    const/4 v7, 0x3

    if-ne v2, v7, :cond_0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/h;

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->l:I

    const/16 v7, 0x8

    and-int/2addr v2, v7

    if-eqz v2, :cond_1

    move v2, v4

    :goto_2
    if-nez v2, :cond_2

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_4
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_1

    :cond_1
    move v2, v5

    goto :goto_2

    :cond_2
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->o:Lcom/google/android/apps/gmm/map/indoor/d/a;

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/e;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/e;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Lcom/google/android/apps/gmm/map/b/a/e;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/c;

    invoke-direct {v0, v2, v7}, Lcom/google/android/apps/gmm/map/indoor/d/c;-><init>(Lcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/b/a/d;)V

    move-object v2, v0

    goto :goto_3

    .line 46
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/c/p;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Tile "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " has "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " indoor metadata"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    invoke-interface {v0, p1, v5, v1}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    goto/16 :goto_0

    .line 50
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    invoke-interface {v0, p1, v4, v3}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    goto/16 :goto_0

    .line 54
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    const/4 v1, 0x2

    invoke-interface {v0, p1, v1, v3}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    goto/16 :goto_0

    .line 58
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/c/p;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Tile not found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    invoke-interface {v0, p1, v4, v3}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    goto/16 :goto_0

    .line 63
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/c/p;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "IO error for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/p;->b:Lcom/google/android/apps/gmm/map/indoor/c/o;

    invoke-interface {v0, p1, v4, v3}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto/16 :goto_4

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
