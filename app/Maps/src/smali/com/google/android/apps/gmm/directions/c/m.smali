.class public Lcom/google/android/apps/gmm/directions/c/m;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/directions/h/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 4

    .prologue
    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/c/m;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    sget-object v3, Lcom/google/android/libraries/curvular/g;->C:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    .line 27
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.ListView"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 4

    .prologue
    .line 23
    check-cast p2, Lcom/google/android/apps/gmm/directions/h/f;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/directions/h/f;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/h/g;

    instance-of v1, v0, Lcom/google/android/apps/gmm/directions/h/w;

    if-eqz v1, :cond_2

    iget-object v1, p4, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const-class v1, Lcom/google/android/apps/gmm/base/f/u;

    new-instance v3, Lcom/google/android/apps/gmm/directions/c/n;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/c/n;-><init>()V

    invoke-virtual {p4, v1, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    const-class v3, Lcom/google/android/apps/gmm/directions/c/ap;

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/directions/h/w;

    invoke-virtual {p4, v3, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_2
    instance-of v1, v0, Lcom/google/android/apps/gmm/directions/h/b;

    if-eqz v1, :cond_0

    iget-object v1, p4, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    const-class v1, Lcom/google/android/apps/gmm/base/f/u;

    new-instance v3, Lcom/google/android/apps/gmm/directions/c/n;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/c/n;-><init>()V

    invoke-virtual {p4, v1, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_3
    const-class v1, Lcom/google/android/apps/gmm/directions/c/a;

    check-cast v0, Lcom/google/android/apps/gmm/directions/h/b;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    :cond_4
    return-void
.end method
