.class public Lcom/google/android/apps/gmm/map/internal/c/e;
.super Lcom/google/android/apps/gmm/map/internal/c/d;
.source "PG"


# static fields
.field public static final c:Lcom/google/android/apps/gmm/map/internal/c/e;

.field private static final d:[Lcom/google/android/apps/gmm/map/internal/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x100

    .line 158
    new-array v0, v3, [Lcom/google/android/apps/gmm/map/internal/c/e;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 160
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    if-ge v0, v3, :cond_0

    .line 161
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/e;-><init>(I)V

    aput-object v2, v1, v0

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    const/16 v1, 0x50

    aget-object v0, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/e;->c:Lcom/google/android/apps/gmm/map/internal/c/e;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/d;-><init>(I)V

    .line 170
    return-void
.end method

.method public static b(I)Lcom/google/android/apps/gmm/map/internal/c/e;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/16 v0, 0x9

    const/16 v3, 0xd

    const/4 v2, 0x7

    const/4 v1, 0x6

    .line 184
    .line 186
    packed-switch p0, :pswitch_data_0

    move v0, v3

    .line 248
    :goto_0
    :pswitch_0
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    shl-int/lit8 v0, v0, 0x4

    or-int/2addr v0, v1

    aget-object v0, v2, v0

    return-object v0

    .line 189
    :pswitch_1
    const/16 v0, 0xa

    move v1, v2

    .line 190
    goto :goto_0

    :pswitch_2
    move v1, v2

    .line 195
    goto :goto_0

    :pswitch_3
    move v1, v4

    .line 199
    goto :goto_0

    .line 207
    :pswitch_4
    const/16 v0, 0xb

    .line 208
    goto :goto_0

    :pswitch_5
    move v0, v1

    move v1, v2

    .line 216
    goto :goto_0

    :pswitch_6
    move v0, v2

    .line 224
    goto :goto_0

    .line 227
    :pswitch_7
    const/16 v0, 0xe

    move v1, v2

    .line 228
    goto :goto_0

    :pswitch_8
    move v0, v3

    move v1, v2

    .line 233
    goto :goto_0

    :pswitch_9
    move v0, v3

    move v1, v4

    .line 237
    goto :goto_0

    :pswitch_a
    move v0, v3

    .line 242
    goto :goto_0

    .line 245
    :pswitch_b
    const/16 v0, 0xf

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static b(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/e;
    .locals 2

    .prologue
    .line 177
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 178
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public static c(I)Lcom/google/android/apps/gmm/map/internal/c/e;
    .locals 2

    .prologue
    const/4 v0, 0x5

    .line 252
    packed-switch p0, :pswitch_data_0

    .line 285
    :goto_0
    :pswitch_0
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/e;->d:[Lcom/google/android/apps/gmm/map/internal/c/e;

    shl-int/lit8 v0, v0, 0x4

    aget-object v0, v1, v0

    return-object v0

    .line 258
    :pswitch_1
    const/4 v0, 0x6

    .line 259
    goto :goto_0

    .line 261
    :pswitch_2
    const/4 v0, 0x7

    .line 262
    goto :goto_0

    .line 264
    :pswitch_3
    const/16 v0, 0x9

    .line 265
    goto :goto_0

    .line 267
    :pswitch_4
    const/16 v0, 0xa

    .line 268
    goto :goto_0

    .line 270
    :pswitch_5
    const/16 v0, 0xb

    .line 271
    goto :goto_0

    .line 273
    :pswitch_6
    const/16 v0, 0xd

    .line 274
    goto :goto_0

    .line 276
    :pswitch_7
    const/16 v0, 0xe

    .line 277
    goto :goto_0

    .line 279
    :pswitch_8
    const/16 v0, 0xf

    .line 280
    goto :goto_0

    .line 252
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
