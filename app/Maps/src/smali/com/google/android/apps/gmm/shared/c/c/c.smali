.class public Lcom/google/android/apps/gmm/shared/c/c/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I


# instance fields
.field public final b:Lcom/google/android/apps/gmm/shared/b/a;

.field public final c:Lcom/google/maps/g/a/al;

.field private final d:Lcom/google/android/apps/gmm/shared/c/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const v0, 0x47b29800    # 91440.0f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/shared/c/c/c;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 1

    .prologue
    .line 83
    sget v0, Lcom/google/android/apps/gmm/l;->cL:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/shared/c/c/c;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->b:Lcom/google/android/apps/gmm/shared/b/a;

    .line 90
    const-string v0, "imperial"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->c:Lcom/google/maps/g/a/al;

    .line 97
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->d:Lcom/google/android/apps/gmm/shared/c/c/e;

    .line 98
    return-void

    .line 92
    :cond_0
    const-string v0, "imperial_yards"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    sget-object v0, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->c:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 95
    :cond_1
    sget-object v0, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->c:Lcom/google/maps/g/a/al;

    goto :goto_0
.end method

.method private a(ILcom/google/maps/g/a/al;ZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;
    .locals 12
    .param p6    # Lcom/google/android/apps/gmm/shared/c/c/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/shared/c/c/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 337
    int-to-long v2, p1

    const-wide/32 v4, 0x8028

    mul-long v6, v2, v4

    .line 338
    sget-object v2, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    if-ne p2, v2, :cond_3

    const-wide/32 v2, 0x4c9960

    cmp-long v2, v6, v2

    if-gez v2, :cond_3

    .line 340
    const-wide/16 v2, 0x3

    div-long v4, v6, v2

    .line 341
    long-to-int v2, v4

    div-int/lit16 v2, v2, 0x2710

    .line 342
    mul-int/lit16 v3, v2, 0x2710

    int-to-long v6, v3

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1388

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    .line 343
    add-int/lit8 v2, v2, 0x1

    .line 345
    :cond_0
    if-eqz p3, :cond_1

    .line 346
    const/16 v3, 0xa

    add-int/lit8 v2, v2, 0x5

    div-int/2addr v2, v3

    mul-int/2addr v2, v3

    .line 348
    :cond_1
    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move/from16 p5, p4

    .line 396
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->d:Lcom/google/android/apps/gmm/shared/c/c/e;

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v4, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    .line 397
    if-eqz p7, :cond_2

    .line 398
    iget-object v4, v3, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v4, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    move-object/from16 v0, p7

    iget-object v6, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v4, v3, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 400
    :cond_2
    if-eqz p6, :cond_c

    .line 401
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->d:Lcom/google/android/apps/gmm/shared/c/c/e;

    new-instance v7, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v7, v6, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v2, v7, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v6, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    move-object/from16 v0, p6

    iget-object v8, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v2, v7, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    aput-object v7, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 405
    :goto_1
    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    return-object v2

    .line 350
    :cond_3
    sget-object v2, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    if-ne p2, v2, :cond_6

    const-wide/32 v2, 0x4c9960

    cmp-long v2, v6, v2

    if-gez v2, :cond_6

    .line 352
    long-to-int v2, v6

    div-int/lit16 v2, v2, 0x2710

    .line 353
    mul-int/lit16 v3, v2, 0x2710

    int-to-long v4, v3

    sub-long v4, v6, v4

    const-wide/16 v6, 0x1388

    cmp-long v3, v4, v6

    if-ltz v3, :cond_4

    .line 354
    add-int/lit8 v2, v2, 0x1

    .line 356
    :cond_4
    if-eqz p3, :cond_5

    .line 357
    const/16 v3, 0x32

    add-int/lit8 v2, v2, 0x19

    div-int/2addr v2, v3

    mul-int/2addr v2, v3

    .line 359
    :cond_5
    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move/from16 p5, p4

    .line 361
    goto :goto_0

    :cond_6
    const-wide/32 v2, 0x2fd6180

    cmp-long v2, v6, v2

    if-gez v2, :cond_7

    .line 363
    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide v8, 0x3fb999999999999aL    # 0.1

    const v5, 0x509100

    .line 364
    const/16 v10, 0x8

    shl-long/2addr v6, v10

    const v10, 0x28488000

    int-to-long v10, v10

    add-long/2addr v6, v10

    int-to-long v10, v5

    div-long/2addr v6, v10

    const/16 v5, 0x8

    shr-long/2addr v6, v5

    long-to-int v5, v6

    int-to-double v6, v5

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    .line 363
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 366
    :cond_7
    const-wide/32 v2, 0x325aa00

    cmp-long v2, v6, v2

    if-gtz v2, :cond_8

    .line 368
    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 372
    :cond_8
    const-wide/32 v2, 0x325aa00

    div-long v4, v6, v2

    .line 373
    const-wide/32 v2, 0x325aa00

    rem-long v2, v6, v2

    .line 374
    const-wide/16 v6, 0xa

    cmp-long v6, v4, v6

    if-ltz v6, :cond_a

    const-wide/32 v6, 0x192d500

    cmp-long v6, v2, v6

    if-ltz v6, :cond_a

    .line 376
    const-wide/16 v2, 0x1

    add-long/2addr v4, v2

    .line 377
    const-wide/16 v2, 0x0

    .line 387
    :cond_9
    :goto_2
    const-wide/16 v6, 0xa

    cmp-long v6, v4, v6

    if-ltz v6, :cond_b

    .line 389
    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 379
    :cond_a
    const v6, 0x509100

    const/16 v7, 0x8

    shl-long/2addr v2, v7

    const v7, 0x28488000

    int-to-long v8, v7

    add-long/2addr v2, v8

    int-to-long v6, v6

    div-long/2addr v2, v6

    const/16 v6, 0x8

    shr-long/2addr v2, v6

    long-to-int v2, v2

    int-to-long v2, v2

    .line 380
    const-wide/16 v6, 0xa

    cmp-long v6, v2, v6

    if-nez v6, :cond_9

    .line 382
    const-wide/16 v2, 0x1

    add-long/2addr v4, v2

    .line 383
    const-wide/16 v2, 0x0

    goto :goto_2

    .line 392
    :cond_b
    const-string v6, "%.1f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    long-to-double v4, v4

    const-wide v10, 0x3fb999999999999aL    # 0.1

    long-to-double v2, v2

    mul-double/2addr v2, v10

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 403
    :cond_c
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private a(IZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;
    .locals 12
    .param p5    # Lcom/google/android/apps/gmm/shared/c/c/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/shared/c/c/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 260
    const/16 v2, 0x3cf

    if-ge p1, v2, :cond_3

    .line 262
    if-eqz p2, :cond_0

    .line 267
    const/16 v2, 0x12c

    if-ge p1, v2, :cond_2

    const/16 v2, 0xa

    .line 269
    :goto_0
    shr-int/lit8 v3, v2, 0x1

    add-int/2addr v3, p1

    div-int/2addr v3, v2

    mul-int p1, v3, v2

    .line 271
    :cond_0
    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move/from16 p4, p3

    .line 306
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->d:Lcom/google/android/apps/gmm/shared/c/c/e;

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v4, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    .line 307
    if-eqz p6, :cond_1

    .line 308
    iget-object v4, v3, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v4, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v4, v3, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 310
    :cond_1
    if-eqz p5, :cond_8

    .line 311
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->d:Lcom/google/android/apps/gmm/shared/c/c/e;

    new-instance v7, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v7, v6, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v2, v7, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v6, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    move-object/from16 v0, p5

    iget-object v8, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v2, v7, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    aput-object v7, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 315
    :goto_2
    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    return-object v2

    .line 267
    :cond_2
    const/16 v2, 0x32

    goto :goto_0

    .line 273
    :cond_3
    const/16 v2, 0x3e8

    if-gt p1, v2, :cond_4

    .line 275
    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 279
    :cond_4
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v4, v3, -0x3

    .line 281
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 282
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 283
    const/16 v4, 0xa

    if-lt v3, v4, :cond_6

    const/16 v4, 0x1f4

    if-lt v2, v4, :cond_6

    .line 285
    add-int/lit8 v3, v3, 0x1

    .line 286
    const/4 v2, 0x0

    .line 296
    :cond_5
    :goto_3
    const/16 v4, 0xa

    if-lt v3, v4, :cond_7

    .line 298
    const-string v2, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 288
    :cond_6
    int-to-long v4, v2

    const/16 v2, 0x64

    const/16 v6, 0x8

    shl-long/2addr v4, v6

    const/16 v6, 0x3200

    int-to-long v6, v6

    add-long/2addr v4, v6

    int-to-long v6, v2

    div-long/2addr v4, v6

    const/16 v2, 0x8

    shr-long/2addr v4, v2

    long-to-int v2, v4

    .line 289
    const/16 v4, 0xa

    if-ne v2, v4, :cond_5

    .line 291
    add-int/lit8 v3, v3, 0x1

    .line 292
    const/4 v2, 0x0

    goto :goto_3

    .line 301
    :cond_7
    const-string v4, "%.1f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    int-to-double v8, v3

    const-wide v10, 0x3fb999999999999aL    # 0.1

    int-to-double v2, v2

    mul-double/2addr v2, v10

    add-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 313
    :cond_8
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    goto/16 :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->g:Lcom/google/android/apps/gmm/shared/b/c;

    const-class v1, Lcom/google/maps/g/a/al;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/al;

    return-object v0
.end method


# virtual methods
.method public final a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;
    .locals 8
    .param p2    # Lcom/google/maps/g/a/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/shared/c/c/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/shared/c/c/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 201
    if-gez p1, :cond_0

    .line 202
    new-instance v0, Landroid/text/SpannedString;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    .line 253
    :goto_0
    return-object v0

    .line 204
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/maps/g/a/al;)Lcom/google/maps/g/a/al;

    move-result-object v0

    .line 210
    sget-object v1, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    if-ne v0, v1, :cond_2

    .line 211
    if-eqz p4, :cond_1

    .line 212
    sget v3, Lcom/google/android/apps/gmm/l;->cH:I

    sget v4, Lcom/google/android/apps/gmm/l;->cF:I

    move-object v0, p0

    move v1, p1

    move v2, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(IZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_1
    sget v3, Lcom/google/android/apps/gmm/l;->cI:I

    sget v4, Lcom/google/android/apps/gmm/l;->cG:I

    move-object v0, p0

    move v1, p1

    move v2, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(IZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 224
    :cond_2
    sget-object v1, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    if-ne v0, v1, :cond_4

    .line 225
    if-eqz p4, :cond_3

    .line 226
    sget v4, Lcom/google/android/apps/gmm/l;->cD:I

    sget v5, Lcom/google/android/apps/gmm/l;->cJ:I

    sget-object v2, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_3
    sget v4, Lcom/google/android/apps/gmm/l;->cE:I

    sget v5, Lcom/google/android/apps/gmm/l;->cK:I

    sget-object v2, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 238
    :cond_4
    sget-object v1, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    if-ne v0, v1, :cond_6

    .line 239
    if-eqz p4, :cond_5

    .line 240
    sget v4, Lcom/google/android/apps/gmm/l;->cM:I

    sget v5, Lcom/google/android/apps/gmm/l;->cJ:I

    sget-object v2, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 246
    :cond_5
    sget v4, Lcom/google/android/apps/gmm/l;->cN:I

    sget v5, Lcom/google/android/apps/gmm/l;->cK:I

    sget-object v2, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZIILcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 253
    :cond_6
    new-instance v0, Landroid/text/SpannedString;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/g/a/al;)Lcom/google/maps/g/a/al;
    .locals 4
    .param p1    # Lcom/google/maps/g/a/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->b:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->g:Lcom/google/android/apps/gmm/shared/b/c;

    const-class v2, Lcom/google/maps/g/a/al;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/al;

    .line 116
    if-eqz v0, :cond_0

    .line 122
    :goto_0
    return-object v0

    .line 119
    :cond_0
    if-eqz p1, :cond_1

    move-object v0, p1

    .line 120
    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/c;->c:Lcom/google/maps/g/a/al;

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/g/a/ai;ZZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 138
    iget v2, p1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    iget v2, p1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    :goto_1
    if-eqz v0, :cond_3

    iget v0, p1, Lcom/google/maps/g/a/ai;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_0
    :goto_2
    iget v1, p1, Lcom/google/maps/g/a/ai;->b:I

    move-object v0, p0

    move v3, p2

    move v4, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    .line 139
    :goto_3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    .line 138
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move-object v2, v5

    goto :goto_2

    :cond_4
    new-instance v0, Landroid/text/SpannedString;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
