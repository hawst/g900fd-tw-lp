.class public Lcom/google/android/apps/gmm/search/d/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/search/aq;

.field private final b:Lcom/google/android/apps/gmm/place/b/b;

.field private final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/android/libraries/curvular/aw;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/search/aq;Lcom/google/android/apps/gmm/place/b/b;Lcom/google/android/apps/gmm/search/f;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/d/b;->a:Lcom/google/android/apps/gmm/search/aq;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/d/b;->b:Lcom/google/android/apps/gmm/place/b/b;

    .line 46
    iget-object v0, p3, Lcom/google/android/apps/gmm/search/f;->c:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    .line 47
    iget-object v0, p3, Lcom/google/android/apps/gmm/search/f;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 48
    iget-object v0, p3, Lcom/google/android/apps/gmm/search/f;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->e:Ljava/lang/String;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_0

    .line 51
    const/16 v0, 0xc

    .line 55
    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/d;->b(I)Ljava/lang/Integer;

    move-result-object v0

    .line 56
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->f:Lcom/google/android/libraries/curvular/aw;

    .line 57
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/d;->ao:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    .line 62
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_1

    .line 94
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->e:Ljava/lang/String;

    .line 95
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 96
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    const/4 v0, 0x0

    .line 97
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 98
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 99
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->b:Lcom/google/android/apps/gmm/place/b/b;

    sget-object v3, Lcom/google/b/f/t;->gp:Lcom/google/b/f/t;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/d/b;->e:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;)V

    .line 113
    :goto_1
    return-object v4

    .line 96
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->gp:Lcom/google/b/f/t;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/b;->a:Lcom/google/android/apps/gmm/search/aq;

    new-instance v2, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/d/b;->e:Ljava/lang/String;

    .line 108
    if-eqz v3, :cond_2

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    .line 109
    :cond_2
    iput-object v0, v2, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 106
    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_1
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    .line 78
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/google/b/f/t;->gp:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/b;->f:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method
