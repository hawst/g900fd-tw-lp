.class public Lcom/google/android/apps/gmm/ulr/GcoreUlrService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field final a:J

.field private final b:Lcom/google/android/gms/location/reporting/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->a:J

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/ulr/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/ulr/a;-><init>(Lcom/google/android/apps/gmm/ulr/GcoreUlrService;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->b:Lcom/google/android/gms/location/reporting/a;

    return-void
.end method

.method static a(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 50
    const-string v0, "LOCATION_REPORTING_DEVICE_ENABLED_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static a([B)Z
    .locals 2

    .prologue
    .line 58
    array-length v0, p0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/b/h/a;->a([B)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->b:Lcom/google/android/gms/location/reporting/a;

    return-object v0
.end method
