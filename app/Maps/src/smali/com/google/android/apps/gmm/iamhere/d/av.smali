.class public Lcom/google/android/apps/gmm/iamhere/d/av;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final c:Lcom/google/android/apps/gmm/iamhere/d/an;

.field final d:Lcom/google/android/apps/gmm/iamhere/d/au;

.field e:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/d;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/av;->a:Ljava/lang/String;

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/av;->b:Ljava/util/List;

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/d/an;Lcom/google/android/apps/gmm/iamhere/d/au;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/av;->d:Lcom/google/android/apps/gmm/iamhere/d/au;

    .line 45
    return-void
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 109
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    const v1, 0x50808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 111
    const-string v1, "notification_activity_intent_type"

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/at;->c:Lcom/google/android/apps/gmm/iamhere/d/at;

    .line 112
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/iamhere/d/at;->name()Ljava/lang/String;

    move-result-object v2

    .line 111
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v1, "disambiguation_debug_mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 114
    return-object v0
.end method


# virtual methods
.method a(ZLjava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/l;

    .line 102
    new-instance v3, Lcom/google/android/apps/gmm/iamhere/d/j;

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v5, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-direct {v3, v4, v5, v0, p1}, Lcom/google/android/apps/gmm/iamhere/d/j;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/iamhere/c/a;Lcom/google/android/apps/gmm/iamhere/c/h;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 105
    :cond_2
    return-object v2
.end method
