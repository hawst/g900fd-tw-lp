.class public Lcom/google/android/apps/gmm/map/internal/c/bl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/be;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    .line 19
    return-void
.end method

.method public static a(Ljava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/bl;
    .locals 5

    .prologue
    .line 28
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bl;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/bl;-><init>()V

    .line 29
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 30
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 31
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(ILjava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_0
    return-object v1
.end method
