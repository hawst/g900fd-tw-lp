.class public Lcom/google/android/apps/gmm/directions/d/a;
.super Lcom/google/android/apps/gmm/shared/net/e;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/shared/net/e",
        "<",
        "Lcom/google/r/b/a/agd;",
        "Lcom/google/r/b/a/agl;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/r/b/a/agd;

.field public final b:Lcom/google/android/apps/gmm/directions/d/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public c:Lcom/google/android/apps/gmm/map/r/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/google/android/apps/gmm/directions/d/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/d/a;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/directions/d/b;)V
    .locals 1
    .param p2    # Lcom/google/r/b/a/aha;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/directions/d/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/directions/f/a;",
            "Lcom/google/r/b/a/aha;",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/acy;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/gmm/directions/d/b;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 101
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;)Lcom/google/r/b/a/agd;

    move-result-object v0

    invoke-direct {p0, v0, p5}, Lcom/google/android/apps/gmm/directions/d/a;-><init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/directions/d/b;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/directions/d/b;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/directions/d/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 113
    sget-object v0, Lcom/google/r/b/a/el;->bF:Lcom/google/r/b/a/el;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/e;-><init>(Lcom/google/r/b/a/el;)V

    .line 114
    const-string v0, "tactileRequest"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/r/b/a/agd;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/d/a;->a:Lcom/google/r/b/a/agd;

    .line 115
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/d/a;->b:Lcom/google/android/apps/gmm/directions/d/b;

    .line 116
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;)Lcom/google/r/b/a/agd;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/directions/f/a;",
            "Lcom/google/r/b/a/aha;",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/acy;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/r/b/a/agd;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    invoke-static {v0}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    .line 155
    if-nez p3, :cond_0

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object p3

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    iget v1, v0, Lcom/google/r/b/a/agb;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, v0, Lcom/google/r/b/a/agb;->a:I

    invoke-virtual {v0, p3}, Lcom/google/r/b/a/agb;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/agb;

    .line 156
    invoke-static {}, Lcom/google/r/b/a/afj;->newBuilder()Lcom/google/r/b/a/afl;

    move-result-object v1

    sget-object v2, Lcom/google/maps/g/a/dx;->d:Lcom/google/maps/g/a/dx;

    .line 166
    invoke-virtual {v1, v2}, Lcom/google/r/b/a/afl;->a(Lcom/google/maps/g/a/dx;)Lcom/google/r/b/a/afl;

    move-result-object v1

    .line 167
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v3, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Lcom/google/r/b/a/afl;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hm;->h:I

    iput v2, v1, Lcom/google/r/b/a/afl;->c:I

    .line 168
    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/afl;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afl;

    move-result-object v1

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 170
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->c()Lcom/google/maps/g/a/je;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v1}, Lcom/google/r/b/a/afl;->c()V

    iget-object v3, v1, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->f:Lcom/google/maps/a/a;

    if-eqz v0, :cond_4

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->f:Lcom/google/maps/a/a;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/afl;->a(Lcom/google/maps/a/a;)Lcom/google/r/b/a/afl;

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->c:Lcom/google/maps/g/a/al;

    if-eqz v0, :cond_5

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->c:Lcom/google/maps/g/a/al;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/afl;->a(Lcom/google/maps/g/a/al;)Lcom/google/r/b/a/afl;

    .line 178
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    .line 179
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v2, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v1, Lcom/google/r/b/a/afl;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/afl;->d:Ljava/lang/Object;

    .line 182
    :cond_7
    iget v0, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, v1, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v6, v1, Lcom/google/r/b/a/afl;->h:Z

    .line 184
    iget v0, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, v1, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v6, v1, Lcom/google/r/b/a/afl;->i:Z

    .line 186
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->i:Z

    .line 185
    iget v2, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, v1, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v0, v1, Lcom/google/r/b/a/afl;->k:Z

    .line 187
    iget v0, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, v1, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v6, v1, Lcom/google/r/b/a/afl;->g:Z

    .line 188
    iget v0, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, v1, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v6, v1, Lcom/google/r/b/a/afl;->e:Z

    .line 189
    if-eqz p1, :cond_9

    .line 190
    if-nez p1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v0, v1, Lcom/google/r/b/a/afl;->j:Lcom/google/n/ao;

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, v1, Lcom/google/r/b/a/afl;->a:I

    .line 192
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    if-eqz v0, :cond_b

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget-object v2, v1, Lcom/google/r/b/a/afl;->f:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, v1, Lcom/google/r/b/a/afl;->a:I

    .line 196
    :cond_b
    invoke-static {}, Lcom/google/r/b/a/agd;->newBuilder()Lcom/google/r/b/a/agh;

    move-result-object v0

    .line 197
    iget-object v2, v0, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/r/b/a/afl;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/r/b/a/agh;->a:I

    .line 198
    iget v1, v0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, v0, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v6, v0, Lcom/google/r/b/a/agh;->e:Z

    .line 201
    if-eqz p2, :cond_c

    .line 202
    invoke-virtual {v0, p2}, Lcom/google/r/b/a/agh;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/agh;

    .line 204
    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_e

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    if-nez v1, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget-object v2, v0, Lcom/google/r/b/a/agh;->c:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/r/b/a/agh;->a:I

    .line 207
    :cond_e
    iget v1, v0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v6, v0, Lcom/google/r/b/a/agh;->d:Z

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    .line 211
    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-eq v1, v2, :cond_f

    sget-object v2, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_11

    .line 212
    :cond_f
    sget-object v1, Lcom/google/r/b/a/agi;->a:Lcom/google/r/b/a/agi;

    invoke-virtual {v0, v1}, Lcom/google/r/b/a/agh;->a(Lcom/google/r/b/a/agi;)Lcom/google/r/b/a/agh;

    .line 217
    :cond_10
    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/agh;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agd;

    return-object v0

    .line 213
    :cond_11
    sget-object v2, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_10

    .line 214
    sget-object v1, Lcom/google/r/b/a/agi;->c:Lcom/google/r/b/a/agi;

    invoke-virtual {v0, v1}, Lcom/google/r/b/a/agh;->a(Lcom/google/r/b/a/agi;)Lcom/google/r/b/a/agh;

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/c/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->q()Lcom/google/android/apps/gmm/shared/net/a/o;

    move-result-object v0

    .line 259
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/shared/net/a/o;->a(Lcom/google/android/apps/gmm/shared/net/ad;)Ljava/util/List;

    move-result-object v0

    .line 261
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final L_()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    sget-object v0, Lcom/google/r/b/a/agl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method protected final synthetic a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 46
    check-cast p1, Lcom/google/r/b/a/agl;

    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_0
    iget v0, v0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/directions/d/a;->d:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_2
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/d/a;->c:Lcom/google/android/apps/gmm/map/r/a/e;

    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected final bridge synthetic b()Lcom/google/n/at;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/d/a;->a:Lcom/google/r/b/a/agd;

    return-object v0
.end method

.method public onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/d/a;->b:Lcom/google/android/apps/gmm/directions/d/b;

    if-eqz v0, :cond_0

    .line 127
    if-nez p1, :cond_1

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/d/a;->b:Lcom/google/android/apps/gmm/directions/d/b;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/directions/d/b;->a(Lcom/google/android/apps/gmm/directions/d/a;)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/d/a;->b:Lcom/google/android/apps/gmm/directions/d/b;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/directions/d/b;->b(Lcom/google/android/apps/gmm/directions/d/a;)V

    goto :goto_0
.end method
