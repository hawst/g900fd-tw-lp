.class public Lcom/google/android/apps/gmm/directions/i/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/y;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/directions/i/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/r;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/r;->b:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/r;->c:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/directions/i/r;
    .locals 14
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    .line 48
    if-nez v0, :cond_1

    .line 49
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/r;->a:Ljava/lang/String;

    const-string v2, "Should not have resume from my location without location fix."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    :cond_0
    :goto_0
    return-object v1

    .line 52
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 53
    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/shared/net/ad;)I

    move-result v0

    int-to-double v6, v0

    .line 54
    iget v0, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v0

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    .line 55
    invoke-virtual {p1, v4, v6, v7, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 56
    :goto_1
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/r/a/ad;)D

    move-result-wide v6

    .line 61
    invoke-virtual {p1, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 62
    sget v4, Lcom/google/android/apps/gmm/l;->ny:I

    new-array v5, v2, [Ljava/lang/Object;

    sget-object v8, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    new-instance v9, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    .line 63
    invoke-static {p0, v0, v8, v9}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    aput-object v0, v5, v3

    .line 62
    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 65
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v4, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    .line 67
    :goto_3
    iget v4, v0, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v2, :cond_6

    move v4, v2

    :goto_4
    if-eqz v4, :cond_a

    .line 68
    iget v1, v0, Lcom/google/maps/g/a/ai;->b:I

    double-to-int v4, v6

    sub-int/2addr v1, v4

    .line 69
    invoke-static {}, Lcom/google/maps/g/a/ai;->newBuilder()Lcom/google/maps/g/a/ak;

    move-result-object v4

    iget v6, v4, Lcom/google/maps/g/a/ak;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v4, Lcom/google/maps/g/a/ak;->a:I

    iput v1, v4, Lcom/google/maps/g/a/ak;->b:I

    .line 70
    iget v1, v0, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v6, 0x4

    if-ne v1, v6, :cond_7

    move v1, v2

    :goto_5
    if-eqz v1, :cond_9

    .line 71
    iget v0, v0, Lcom/google/maps/g/a/ai;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_2
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55
    :cond_3
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_1

    .line 65
    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_3

    :cond_6
    move v4, v3

    .line 67
    goto :goto_4

    :cond_7
    move v1, v3

    .line 70
    goto :goto_5

    .line 71
    :cond_8
    iget v1, v4, Lcom/google/maps/g/a/ak;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v4, Lcom/google/maps/g/a/ak;->a:I

    iget v0, v0, Lcom/google/maps/g/a/al;->e:I

    iput v0, v4, Lcom/google/maps/g/a/ak;->c:I

    .line 73
    :cond_9
    invoke-virtual {v4}, Lcom/google/maps/g/a/ak;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/c;

    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/gmm/shared/c/c/c;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;)V

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/maps/g/a/ai;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_6
    new-instance v1, Lcom/google/android/apps/gmm/directions/i/r;

    invoke-direct {v1, v5, v0}, Lcom/google/android/apps/gmm/directions/i/r;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    goto :goto_6
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/r;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/r;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method
