.class Lcom/google/android/apps/gmm/map/internal/c/cq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/cp;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/c/cm;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/cm;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 922
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->a:Lcom/google/android/apps/gmm/map/internal/c/cm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 924
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    .line 927
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->c:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/m;
    .locals 2

    .prologue
    .line 947
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->a:Lcom/google/android/apps/gmm/map/internal/c/cm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 952
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->c:I

    .line 953
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 957
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    .line 958
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 931
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->a:Lcom/google/android/apps/gmm/map/internal/c/cm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->a:Lcom/google/android/apps/gmm/map/internal/c/cm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cq;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 942
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
