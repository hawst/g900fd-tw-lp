.class public Lcom/google/android/apps/gmm/shared/c/a/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/c/a/j;


# static fields
.field static final a:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/shared/c/a/p;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/google/android/apps/gmm/shared/c/a/n;

.field private final d:Lcom/google/android/apps/gmm/shared/c/a/n;

.field private final e:Lcom/google/android/apps/gmm/shared/c/f;

.field private final f:Lcom/google/android/apps/gmm/shared/c/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/shared/c/a/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/k;->b:Ljava/lang/String;

    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/apps/gmm/shared/c/a/p;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/ct;->a(Ljava/util/EnumSet;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/a/k;->a:Lcom/google/b/c/dn;

    return-void
.end method

.method public constructor <init>(IILcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p4, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->e:Lcom/google/android/apps/gmm/shared/c/f;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/n;

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/a/f;

    const-string v2, "default"

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v1, v2, v3, p3, v4}, Lcom/google/android/apps/gmm/shared/c/a/f;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/gmm/shared/c/a/n;-><init>(Lcom/google/android/apps/gmm/shared/c/a/k;ILjava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/n;

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/a/f;

    const-string v2, "network"

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v1, v2, v3, p3, v4}, Lcom/google/android/apps/gmm/shared/c/a/f;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/gmm/shared/c/a/n;-><init>(Lcom/google/android/apps/gmm/shared/c/a/k;ILjava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->f:Lcom/google/android/apps/gmm/shared/c/a/o;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->f:Lcom/google/android/apps/gmm/shared/c/a/o;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/a/c;

    .line 139
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/c;-><init>(Landroid/os/Looper;)V

    .line 138
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/o;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/shared/c/a/i;)V

    .line 140
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/shared/c/a/o;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->f:Lcom/google/android/apps/gmm/shared/c/a/o;

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 2

    .prologue
    .line 212
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/k;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 217
    return-void
.end method

.method public final a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 161
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/l;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->f:Lcom/google/android/apps/gmm/shared/c/a/o;

    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/c/a/o;->a:[Lcom/google/android/apps/gmm/shared/c/a/i;

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/o;->a()[Lcom/google/android/apps/gmm/shared/c/a/i;

    move-result-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No executor registered for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :pswitch_0
    cmp-long v0, p3, v2

    if-eqz v0, :cond_1

    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t schedule a delayed task on Threads.CURRENT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_1
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 189
    :cond_2
    :goto_0
    return-void

    .line 169
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    .line 183
    :goto_1
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/a/g;->a(Ljava/lang/Runnable;)Lcom/google/android/apps/gmm/shared/c/a/g;

    move-result-object v1

    .line 184
    cmp-long v2, p3, v2

    if-eqz v2, :cond_3

    .line 185
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->e:Lcom/google/android/apps/gmm/shared/c/f;

    iput-object v2, v1, Lcom/google/android/apps/gmm/shared/c/a/g;->b:Lcom/google/android/apps/gmm/shared/c/f;

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->e:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    add-long/2addr v2, p3

    iput-wide v2, v1, Lcom/google/android/apps/gmm/shared/c/a/g;->a:J

    .line 188
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->isShutdown()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->prestartCoreThread()Z

    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    goto :goto_1

    .line 175
    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/i;

    invoke-interface {v0, p1, p3, p4}, Lcom/google/android/apps/gmm/shared/c/a/i;->a(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x58

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Tried to schedule "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", but its executor is not accepting work (probably already shut down)."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/c/a/p;)Z
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_THREADS:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-ne p1, v0, :cond_1

    .line 247
    :cond_0
    const/4 v0, 0x0

    .line 249
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 265
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/k;->a:Lcom/google/b/c/dn;

    invoke-virtual {v0, p1}, Lcom/google/b/c/dn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x1

    .line 268
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->f:Lcom/google/android/apps/gmm/shared/c/a/o;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/shared/c/a/o;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 300
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdown()V

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdown()V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/n;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/n;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdownNow()Ljava/util/List;

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdownNow()Ljava/util/List;

    .line 318
    :goto_0
    return-void

    .line 311
    :catch_0
    move-exception v0

    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/k;->b:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdownNow()Ljava/util/List;

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdownNow()Ljava/util/List;

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->c:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdownNow()Ljava/util/List;

    .line 317
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->d:Lcom/google/android/apps/gmm/shared/c/a/n;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/n;->shutdownNow()Ljava/util/List;

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/k;->a:Lcom/google/b/c/dn;

    invoke-virtual {v0, p1}, Lcom/google/b/c/dn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/k;->f:Lcom/google/android/apps/gmm/shared/c/a/o;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/shared/c/a/o;->b(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 4

    .prologue
    .line 227
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 228
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/a/m;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/gmm/shared/c/a/m;-><init>(Ljava/lang/Runnable;Ljava/util/concurrent/Semaphore;)V

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/k;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 230
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
