.class public Lcom/google/android/apps/gmm/map/b/a/u;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static c:[I

.field static d:[I

.field private static final g:Lcom/google/android/apps/gmm/map/b/a/be;


# instance fields
.field public final a:I

.field public final b:I

.field private final e:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->c:[I

    .line 64
    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->d:[I

    .line 73
    const/16 v0, 0x16

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/be;->a(I)Lcom/google/android/apps/gmm/map/b/a/be;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->g:Lcom/google/android/apps/gmm/map/b/a/be;

    .line 74
    return-void
.end method

.method public constructor <init>(II)V
    .locals 12

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    invoke-static {p2}, Lcom/google/android/apps/gmm/map/b/a/u;->a(I)I

    move-result v1

    .line 149
    const v0, 0x4c4b400

    if-le p1, v0, :cond_2

    .line 150
    const v0, 0x4c4b400

    .line 153
    :goto_0
    const v2, -0x4c4b400

    if-ge v0, v2, :cond_0

    .line 154
    const v0, -0x4c4b400

    .line 157
    :cond_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    .line 158
    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    .line 159
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/u;->g:Lcom/google/android/apps/gmm/map/b/a/be;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    int-to-long v4, v3

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    int-to-long v6, v1

    iget v1, v2, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    int-to-long v2, v1

    mul-long/2addr v2, v6

    const-wide/32 v6, 0x15752a00

    div-long/2addr v2, v6

    add-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/u;->e:I

    .line 160
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/u;->g:Lcom/google/android/apps/gmm/map/b/a/be;

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v3, 0xf4240

    div-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v4, 0xf4240

    rem-int/2addr v3, v4

    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/u;->b()[I

    move-result-object v4

    add-int/lit8 v5, v1, -0x1

    aget v5, v4, v5

    mul-int/lit8 v5, v5, -0x1

    aget v6, v4, v1

    mul-int/lit8 v6, v6, 0x3

    add-int/2addr v5, v6

    add-int/lit8 v6, v1, 0x1

    aget v6, v4, v6

    mul-int/lit8 v6, v6, 0x3

    sub-int/2addr v5, v6

    add-int/lit8 v6, v1, 0x2

    aget v6, v4, v6

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x6

    add-int/lit8 v6, v1, -0x1

    aget v6, v4, v6

    mul-int/lit8 v6, v6, 0x3

    aget v7, v4, v1

    mul-int/lit8 v7, v7, 0x6

    sub-int/2addr v6, v7

    add-int/lit8 v7, v1, 0x1

    aget v7, v4, v7

    mul-int/lit8 v7, v7, 0x3

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x6

    add-int/lit8 v7, v1, -0x1

    aget v7, v4, v7

    mul-int/lit8 v7, v7, -0x2

    aget v8, v4, v1

    mul-int/lit8 v8, v8, 0x3

    sub-int/2addr v7, v8

    add-int/lit8 v8, v1, 0x1

    aget v8, v4, v8

    mul-int/lit8 v8, v8, 0x6

    add-int/2addr v7, v8

    add-int/lit8 v8, v1, 0x2

    aget v8, v4, v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x6

    aget v1, v4, v1

    int-to-long v4, v5

    int-to-long v8, v3

    mul-long/2addr v4, v8

    int-to-long v8, v3

    mul-long/2addr v4, v8

    const-wide/32 v8, 0xf4240

    div-long/2addr v4, v8

    int-to-long v8, v3

    mul-long/2addr v4, v8

    const-wide/32 v8, 0xf4240

    div-long/2addr v4, v8

    const-wide/32 v8, 0xf4240

    div-long/2addr v4, v8

    int-to-long v8, v6

    int-to-long v10, v3

    mul-long/2addr v8, v10

    int-to-long v10, v3

    mul-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    add-long/2addr v4, v8

    int-to-long v6, v7

    int-to-long v8, v3

    mul-long/2addr v6, v8

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    add-long/2addr v4, v6

    int-to-long v6, v1

    add-long/2addr v4, v6

    long-to-int v1, v4

    if-gez v0, :cond_1

    neg-int v0, v1

    :goto_1
    iget v1, v2, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    int-to-long v4, v1

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    int-to-long v0, v0

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xa

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x15752a00

    div-long/2addr v0, v2

    long-to-int v0, v0

    div-int/lit8 v0, v0, 0xa

    int-to-long v0, v0

    sub-long v0, v4, v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->f:I

    .line 161
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, p1

    goto/16 :goto_0
.end method

.method private constructor <init>(III)V
    .locals 12

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    invoke-static {p3}, Lcom/google/android/apps/gmm/map/b/a/be;->a(I)Lcom/google/android/apps/gmm/map/b/a/be;

    move-result-object v1

    .line 172
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/u;->b(II)I

    move-result v0

    .line 173
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    if-gez p2, :cond_2

    const/4 p2, 0x0

    .line 174
    :cond_0
    :goto_0
    const/16 v2, 0x16

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    if-ge v3, v2, :cond_3

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    sub-int/2addr v2, v3

    shl-int/2addr v0, v2

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->e:I

    .line 176
    const/16 v0, 0x16

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    if-ge v2, v0, :cond_4

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    sub-int/2addr v0, v1

    shl-int v0, p2, v0

    :goto_2
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->f:I

    .line 178
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->f:I

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/u;->g:Lcom/google/android/apps/gmm/map/b/a/be;

    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/u;->c()[I

    move-result-object v2

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    div-int/lit8 v3, v3, 0x2

    sub-int v0, v3, v0

    int-to-long v4, v0

    const-wide/32 v6, 0x3b9aca00

    mul-long/2addr v4, v6

    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    int-to-long v0, v0

    div-long v0, v4, v0

    long-to-int v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const v3, 0x4c4b40

    div-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    array-length v3, v2

    add-int/lit8 v3, v3, -0x2

    if-lt v0, v3, :cond_6

    if-lez v1, :cond_5

    const v0, 0x4c4b400

    :cond_1
    :goto_3
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    .line 179
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->e:I

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/u;->g:Lcom/google/android/apps/gmm/map/b/a/be;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    int-to-long v2, v0

    const-wide/32 v4, 0x15752a00

    mul-long/2addr v2, v4

    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    int-to-long v0, v0

    div-long v0, v2, v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    .line 180
    return-void

    .line 173
    :cond_2
    if-lt p2, v2, :cond_0

    add-int/lit8 p2, v2, -0x1

    goto :goto_0

    .line 174
    :cond_3
    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    sub-int v2, v3, v2

    shr-int/2addr v0, v2

    goto :goto_1

    .line 176
    :cond_4
    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    sub-int v0, v1, v0

    shr-int v0, p2, v0

    goto :goto_2

    .line 178
    :cond_5
    const v0, -0x4c4b400

    goto :goto_3

    :cond_6
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const v4, 0x4c4b40

    rem-int/2addr v3, v4

    add-int/lit8 v4, v0, -0x1

    aget v4, v2, v4

    mul-int/lit8 v4, v4, -0x1

    aget v5, v2, v0

    mul-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    add-int/lit8 v5, v0, 0x1

    aget v5, v2, v5

    mul-int/lit8 v5, v5, 0x3

    sub-int/2addr v4, v5

    add-int/lit8 v5, v0, 0x2

    aget v5, v2, v5

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x6

    add-int/lit8 v5, v0, -0x1

    aget v5, v2, v5

    mul-int/lit8 v5, v5, 0x3

    aget v6, v2, v0

    mul-int/lit8 v6, v6, 0x6

    sub-int/2addr v5, v6

    add-int/lit8 v6, v0, 0x1

    aget v6, v2, v6

    mul-int/lit8 v6, v6, 0x3

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x6

    add-int/lit8 v6, v0, -0x1

    aget v6, v2, v6

    mul-int/lit8 v6, v6, -0x2

    aget v7, v2, v0

    mul-int/lit8 v7, v7, 0x3

    sub-int/2addr v6, v7

    add-int/lit8 v7, v0, 0x1

    aget v7, v2, v7

    mul-int/lit8 v7, v7, 0x6

    add-int/2addr v6, v7

    add-int/lit8 v7, v0, 0x2

    aget v7, v2, v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x6

    aget v0, v2, v0

    int-to-long v8, v4

    int-to-long v10, v3

    mul-long/2addr v8, v10

    int-to-long v10, v3

    mul-long/2addr v8, v10

    const-wide/32 v10, 0x4c4b40

    div-long/2addr v8, v10

    int-to-long v10, v3

    mul-long/2addr v8, v10

    const-wide/32 v10, 0x4c4b40

    div-long/2addr v8, v10

    const-wide/32 v10, 0x4c4b40

    div-long/2addr v8, v10

    int-to-long v4, v5

    int-to-long v10, v3

    mul-long/2addr v4, v10

    int-to-long v10, v3

    mul-long/2addr v4, v10

    const-wide/32 v10, 0x4c4b40

    div-long/2addr v4, v10

    const-wide/32 v10, 0x4c4b40

    div-long/2addr v4, v10

    add-long/2addr v4, v8

    int-to-long v6, v6

    int-to-long v2, v3

    mul-long/2addr v2, v6

    const-wide/32 v6, 0x4c4b40

    div-long/2addr v2, v6

    add-long/2addr v2, v4

    int-to-long v4, v0

    add-long/2addr v2, v4

    long-to-int v0, v2

    const v2, 0x4c4b400

    if-le v0, v2, :cond_7

    const v0, 0x4c4b400

    :cond_7
    if-gez v1, :cond_1

    neg-int v0, v0

    goto/16 :goto_3
.end method

.method private static a(I)I
    .locals 3

    .prologue
    const v2, 0x15752a00

    .line 101
    move v0, p0

    :goto_0
    const v1, -0xaba9500

    if-ge v0, v1, :cond_0

    .line 102
    add-int/2addr v0, v2

    goto :goto_0

    .line 104
    :cond_0
    :goto_1
    const v1, 0xaba9500

    if-le v0, v1, :cond_1

    .line 105
    sub-int/2addr v0, v2

    goto :goto_1

    .line 107
    :cond_1
    return v0
.end method

.method public static a(II)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 3

    .prologue
    .line 550
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    div-int/lit8 v1, p0, 0xa

    div-int/lit8 v2, p1, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    return-object v0
.end method

.method public static a(III)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 1

    .prologue
    .line 822
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(III)V

    return-object v0
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 4

    .prologue
    const/16 v2, 0x15

    .line 543
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v1, v0

    .line 544
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 545
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    return-object v2
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 526
    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 527
    if-eq v0, v2, :cond_0

    .line 529
    :try_start_0
    const-string v0, ","

    .line 530
    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, -0x1

    .line 529
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 531
    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/u;->b(Ljava/lang/String;)I

    move-result v2

    .line 532
    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/u;->b(Ljava/lang/String;)I

    move-result v3

    .line 533
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 538
    :goto_0
    return-object v0

    .line 535
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 538
    goto :goto_0
.end method

.method private static a(Ljava/io/DataInput;[I)V
    .locals 4

    .prologue
    .line 479
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    aput v1, p1, v0

    .line 483
    const/4 v0, 0x1

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 484
    add-int/lit8 v1, v0, -0x1

    aget v1, p1, v1

    .line 485
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    shl-int/lit8 v2, v2, 0x10

    .line 486
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    .line 487
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    or-int/2addr v2, v3

    add-int/2addr v1, v2

    aput v1, p1, v0

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 489
    :cond_0
    return-void
.end method

.method public static a(D)Z
    .locals 2

    .prologue
    .line 83
    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpg-double v0, v0, p0

    if-gtz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(II)I
    .locals 1

    .prologue
    .line 116
    move v0, p0

    :goto_0
    if-lt v0, p1, :cond_0

    .line 117
    sub-int/2addr v0, p1

    goto :goto_0

    .line 119
    :cond_0
    :goto_1
    if-gez v0, :cond_1

    .line 120
    add-int/2addr v0, p1

    goto :goto_1

    .line 122
    :cond_1
    return v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 10

    .prologue
    const/16 v9, 0x30

    const/4 v8, 0x6

    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 556
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 557
    const/16 v0, 0x2e

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 558
    if-eq v0, v3, :cond_6

    .line 559
    const-string v0, "."

    .line 560
    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 559
    invoke-virtual {v4, v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 561
    array-length v0, v5

    const/4 v6, 0x2

    if-le v0, v6, :cond_1

    .line 562
    new-instance v1, Ljava/lang/NumberFormatException;

    const-string v2, "Coordinate has more than one decimal point: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 565
    :cond_1
    aget-object v0, v5, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 566
    aget-object v0, v5, v2

    const-string v7, "-"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_3

    move v0, v1

    .line 568
    :goto_1
    aget-object v7, v5, v1

    aget-object v5, v5, v1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v7, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 571
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    .line 572
    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-lt v7, v9, :cond_2

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v7, 0x39

    if-le v2, v7, :cond_4

    .line 573
    :cond_2
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid fractional part in \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v2

    .line 566
    goto :goto_1

    .line 576
    :cond_4
    invoke-static {v5, v8, v9}, Lcom/google/b/a/bv;->a(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v2

    .line 577
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 578
    const v4, 0xf4240

    mul-int/2addr v4, v6

    if-eqz v0, :cond_5

    move v1, v3

    :cond_5
    mul-int v0, v2, v1

    add-int/2addr v0, v4

    .line 580
    :goto_2
    return v0

    :cond_6
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const v1, 0xf4240

    mul-int/2addr v0, v1

    goto :goto_2
.end method

.method public static b(D)Z
    .locals 2

    .prologue
    .line 93
    const-wide/high16 v0, -0x3fac000000000000L    # -80.0

    cmpg-double v0, v0, p0

    if-gtz v0, :cond_0

    const-wide/high16 v0, 0x4054000000000000L    # 80.0

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized b()[I
    .locals 4

    .prologue
    .line 244
    const-class v1, Lcom/google/android/apps/gmm/map/b/a/u;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->c:[I

    if-nez v0, :cond_0

    .line 245
    const/16 v0, 0x54

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->c:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    const/16 v0, 0xfd

    :try_start_1
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 267
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->c:[I

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/u;->a(Ljava/io/DataInput;[I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    :cond_0
    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->c:[I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    .line 270
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Can\'t read mercator.dat"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 248
    nop

    :array_0
    .array-data 1
        -0x1t
        -0x10t
        -0x43t
        -0x73t
        0xft
        0x42t
        0x73t
        0xft
        0x42t
        0x73t
        0xft
        0x43t
        -0x5dt
        0xft
        0x46t
        0x6t
        0xft
        0x49t
        -0x67t
        0xft
        0x4et
        0x61t
        0xft
        0x54t
        0x5et
        0xft
        0x5bt
        -0x6dt
        0xft
        0x64t
        0x2t
        0xft
        0x6dt
        -0x50t
        0xft
        0x78t
        -0x61t
        0xft
        -0x7ct
        -0x2ct
        0xft
        -0x6et
        0x54t
        0xft
        -0x5ft
        0x26t
        0xft
        -0x4ft
        0x4et
        0xft
        -0x3et
        -0x2dt
        0xft
        -0x2bt
        -0x43t
        0xft
        -0x16t
        0x15t
        0xft
        -0x1t
        -0x1ft
        0x10t
        0x17t
        0x2dt
        0x10t
        0x30t
        0x1t
        0x10t
        0x4at
        0x6bt
        0x10t
        0x66t
        0x74t
        0x10t
        -0x7ct
        0x2bt
        0x10t
        -0x5dt
        -0x64t
        0x10t
        -0x3ct
        -0x29t
        0x10t
        -0x19t
        -0x13t
        0x11t
        0xct
        -0x12t
        0x11t
        0x33t
        -0x14t
        0x11t
        0x5ct
        -0x4t
        0x11t
        -0x78t
        0x34t
        0x11t
        -0x4bt
        -0x57t
        0x11t
        -0x1bt
        0x76t
        0x12t
        0x17t
        -0x4ct
        0x12t
        0x4ct
        -0x7ft
        0x12t
        -0x7dt
        -0x3t
        0x12t
        -0x42t
        0x46t
        0x12t
        -0x5t
        -0x7ct
        0x13t
        0x3bt
        -0x25t
        0x13t
        0x7ft
        0x77t
        0x13t
        -0x3at
        -0x7at
        0x14t
        0x11t
        0x38t
        0x14t
        0x5ft
        -0x3ct
        0x14t
        -0x4et
        0x64t
        0x15t
        0x9t
        0x57t
        0x15t
        0x64t
        -0x1bt
        0x15t
        -0x3bt
        0x56t
        0x16t
        0x2at
        -0x1t
        0x16t
        -0x6at
        0x3at
        0x17t
        0x7t
        0x6dt
        0x17t
        0x7ft
        0x2t
        0x17t
        -0x3t
        0x75t
        0x18t
        -0x7dt
        0x48t
        0x19t
        0x11t
        0x14t
        0x19t
        -0x59t
        0x78t
        0x1at
        0x47t
        0x2et
        0x1at
        -0xft
        0x3t
        0x1bt
        -0x5bt
        -0x27t
        0x1ct
        0x66t
        -0x4dt
        0x1dt
        0x34t
        -0x4dt
        0x1et
        0x11t
        0x1ft
        0x1et
        -0x3t
        0x6ft
        0x1ft
        -0x5t
        0x4at
        0x21t
        0xct
        -0x69t
        0x22t
        0x33t
        -0x78t
        0x23t
        0x72t
        -0x5bt
        0x24t
        -0x34t
        -0x1et
        0x26t
        0x45t
        -0x4ct
        0x27t
        -0x1ft
        0x2at
        0x29t
        -0x5ct
        0x19t
        0x2bt
        -0x6ct
        0x46t
        0x2dt
        -0x48t
        -0x5bt
        0x30t
        0x19t
        -0x54t
        0x32t
        -0x3ft
        -0x3ft
        0x35t
        -0x43t
        -0x2ft
        0x39t
        0x1et
        0x1ct
        0x3ct
        -0x9t
        0x69t
        0x41t
        0x64t
        -0x60t
        0x46t
        -0x77t
        0x52t
        0x4ct
        -0x6bt
        0x73t
        0x53t
        -0x35t
        0x4ft
        0x5ct
        -0x77t
        0x34t
        0x67t
        0x5at
        0xct
    .end array-data
.end method

.method private static declared-synchronized c()[I
    .locals 4

    .prologue
    .line 373
    const-class v1, Lcom/google/android/apps/gmm/map/b/a/u;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->d:[I

    if-nez v0, :cond_0

    .line 374
    const/16 v0, 0x8d

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->d:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    const/16 v0, 0x1a8

    :try_start_1
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 414
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->d:[I

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/u;->a(Ljava/io/DataInput;[I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    :cond_0
    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/u;->d:[I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    .line 417
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "rmercator.dat is incorrect"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 377
    nop

    :array_0
    .array-data 1
        -0x1t
        -0x1ct
        -0x77t
        -0x18t
        0x1bt
        0x76t
        0x18t
        0x1bt
        0x76t
        0x18t
        0x1bt
        0x6ft
        0x2at
        0x1bt
        0x61t
        0x55t
        0x1bt
        0x4ct
        -0x52t
        0x1bt
        0x31t
        0x4bt
        0x1bt
        0xft
        0x4ft
        0x1at
        -0x1at
        -0x1at
        0x1at
        -0x48t
        0x3dt
        0x1at
        -0x7dt
        -0x72t
        0x1at
        0x49t
        0x16t
        0x1at
        0x9t
        0x19t
        0x19t
        -0x3dt
        -0x23t
        0x19t
        0x79t
        -0x51t
        0x19t
        0x2at
        -0x22t
        0x18t
        -0x29t
        -0x46t
        0x18t
        -0x80t
        -0x67t
        0x18t
        0x25t
        -0x33t
        0x17t
        -0x39t
        -0x52t
        0x17t
        0x66t
        -0x73t
        0x17t
        0x2t
        -0x40t
        0x16t
        -0x64t
        -0x65t
        0x16t
        0x34t
        0x6dt
        0x15t
        -0x36t
        -0x7at
        0x15t
        0x5ft
        0x30t
        0x14t
        -0xet
        -0x48t
        0x14t
        -0x7bt
        0x61t
        0x14t
        0x17t
        0x6ft
        0x13t
        -0x57t
        0x21t
        0x13t
        0x3at
        -0x4ct
        0x12t
        -0x34t
        0x5et
        0x12t
        0x5et
        0x55t
        0x11t
        -0x10t
        -0x38t
        0x11t
        -0x7dt
        -0x1at
        0x11t
        0x17t
        -0x2at
        0x10t
        -0x54t
        -0x40t
        0x10t
        0x42t
        -0x3dt
        0xft
        -0x26t
        0x2t
        0xft
        0x72t
        -0x6at
        0xft
        0xct
        -0x67t
        0xet
        -0x58t
        0x20t
        0xet
        0x45t
        0x40t
        0xdt
        -0x1ct
        0x7t
        0xdt
        -0x7ct
        -0x7at
        0xdt
        0x26t
        -0x3at
        0xct
        -0x36t
        -0x2dt
        0xct
        0x70t
        -0x4bt
        0xct
        0x18t
        0x71t
        0xbt
        -0x3et
        0xat
        0xbt
        0x6dt
        -0x79t
        0xbt
        0x1at
        -0x1bt
        0xat
        -0x36t
        0x28t
        0xat
        0x7bt
        0x4dt
        0xat
        0x2et
        0x52t
        0x9t
        -0x1dt
        0x34t
        0x9t
        -0x67t
        -0x10t
        0x9t
        0x52t
        -0x7et
        0x9t
        0xct
        -0x1dt
        0x8t
        -0x37t
        0xet
        0x8t
        -0x7at
        -0x2t
        0x8t
        0x46t
        -0x56t
        0x8t
        0x8t
        0xdt
        0x7t
        -0x35t
        0x1et
        0x7t
        -0x71t
        -0x29t
        0x7t
        0x56t
        0x2dt
        0x7t
        0x1et
        0x1bt
        0x6t
        -0x19t
        -0x68t
        0x6t
        -0x4et
        -0x66t
        0x6t
        0x7ft
        0x1at
        0x6t
        0x4dt
        0xft
        0x6t
        0x1ct
        0x72t
        0x5t
        -0x13t
        0x38t
        0x5t
        -0x41t
        0x59t
        0x5t
        -0x6et
        -0x32t
        0x5t
        0x67t
        -0x72t
        0x5t
        0x3dt
        -0x6ft
        0x5t
        0x14t
        -0x33t
        0x4t
        -0x13t
        0x3bt
        0x4t
        -0x3at
        -0x2bt
        0x4t
        -0x5ft
        -0x71t
        0x4t
        0x7dt
        0x66t
        0x4t
        0x5at
        0x4et
        0x4t
        0x38t
        0x41t
        0x4t
        0x17t
        0x3at
        0x3t
        -0x9t
        0x2ft
        0x3t
        -0x28t
        0x1at
        0x3t
        -0x47t
        -0xdt
        0x3t
        -0x64t
        -0x4bt
        0x3t
        -0x80t
        0x58t
        0x3t
        0x64t
        -0x29t
        0x3t
        0x4at
        0x2bt
        0x3t
        0x30t
        0x4ct
        0x3t
        0x17t
        0x37t
        0x2t
        -0x2t
        -0x1ct
        0x2t
        -0x19t
        0x4ft
        0x2t
        -0x30t
        0x72t
        0x2t
        -0x46t
        0x46t
        0x2t
        -0x5ct
        -0x38t
        0x2t
        -0x71t
        -0xft
        0x2t
        0x7bt
        -0x43t
        0x2t
        0x68t
        0x28t
        0x2t
        0x55t
        0x2bt
        0x2t
        0x42t
        -0x3dt
        0x2t
        0x30t
        -0x14t
        0x2t
        0x1ft
        -0x60t
        0x2t
        0xet
        -0x23t
        0x1t
        -0x2t
        -0x64t
        0x1t
        -0x12t
        -0x24t
        0x1t
        -0x21t
        -0x6at
        0x1t
        -0x30t
        -0x35t
        0x1t
        -0x3et
        0x72t
        0x1t
        -0x4ct
        -0x76t
        0x1t
        -0x59t
        0x11t
        0x1t
        -0x66t
        0x1t
        0x1t
        -0x73t
        0x58t
        0x1t
        -0x7ft
        0x12t
        0x1t
        0x75t
        0x2ft
        0x1t
        0x69t
        -0x59t
        0x1t
        0x5et
        0x7ct
        0x1t
        0x53t
        -0x58t
        0x1t
        0x49t
        0x2at
        0x1t
        0x3et
        -0x1t
        0x1t
        0x35t
        0x23t
        0x1t
        0x2bt
        -0x69t
        0x1t
        0x22t
        0x54t
        0x1t
        0x19t
        0x5bt
        0x1t
        0x10t
        -0x56t
        0x1t
        0x8t
        0x3ct
        0x1t
        0x0t
        0x11t
        0x0t
        -0x8t
        0x28t
        0x0t
        -0x10t
        0x7ct
        0x0t
        -0x17t
        0xdt
        0x0t
        -0x1ft
        -0x28t
        0x0t
        -0x26t
        -0x22t
        0x0t
        -0x2ct
        0x19t
        0x0t
        -0x33t
        -0x75t
        0x0t
        -0x39t
        0x30t
        0x0t
        -0x3ft
        0x8t
        0x0t
        -0x45t
        0x10t
        0x0t
        -0x4bt
        0x47t
    .end array-data
.end method


# virtual methods
.method public final a()Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 512
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/q;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 514
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 515
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 516
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 833
    if-ne p0, p1, :cond_1

    .line 843
    :cond_0
    :goto_0
    return v0

    .line 836
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 837
    goto :goto_0

    .line 840
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/u;

    .line 843
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/u;->e:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/u;->f:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/u;->f:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 851
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    .line 852
    mul-int/lit8 v0, v0, 0x1d

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    add-int/2addr v0, v1

    .line 853
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 507
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    const-string v2, ","

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/x;->a(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
