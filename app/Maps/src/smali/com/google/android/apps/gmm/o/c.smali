.class Lcom/google/android/apps/gmm/o/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/o/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/o/a;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/c;->a:Lcom/google/android/apps/gmm/o/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/c;->a:Lcom/google/android/apps/gmm/o/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/c;->a:Lcom/google/android/apps/gmm/o/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/o/a;->d:Ljava/util/List;

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->k:I

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/google/android/gms/people/accountswitcherview/p;-><init>(Landroid/content/Context;ILcom/google/android/gms/people/accountswitcherview/t;Lcom/google/android/gms/people/accountswitcherview/r;)V

    iput-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iput-boolean v6, v2, Lcom/google/android/gms/people/accountswitcherview/p;->d:Z

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v3, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->j:Lcom/google/android/gms/people/accountswitcherview/g;

    iput-object v3, v2, Lcom/google/android/gms/people/accountswitcherview/p;->a:Lcom/google/android/gms/people/accountswitcherview/g;

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->f:Landroid/widget/ListView;

    iget-object v3, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-boolean v3, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->m:Z

    iget-boolean v4, v2, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-eq v4, v3, :cond_0

    iput-boolean v3, v2, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    invoke-virtual {v2}, Lcom/google/android/gms/people/accountswitcherview/p;->notifyDataSetChanged()V

    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-boolean v3, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->n:Z

    iget-boolean v4, v2, Lcom/google/android/gms/people/accountswitcherview/p;->c:Z

    if-eq v4, v3, :cond_1

    iput-boolean v3, v2, Lcom/google/android/gms/people/accountswitcherview/p;->c:Z

    invoke-virtual {v2}, Lcom/google/android/gms/people/accountswitcherview/p;->notifyDataSetChanged()V

    :cond_1
    iput-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    if-nez v1, :cond_2

    iput-object v5, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    :cond_2
    invoke-virtual {v0, v5, v6}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setSelectedAccount(Lcom/google/android/gms/people/model/a;Z)V

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/p;->a(Ljava/util/List;)V

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->f:Ljava/util/ArrayList;

    :goto_0
    if-eqz v5, :cond_3

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    if-eqz v5, :cond_4

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()V

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/c;->a:Lcom/google/android/apps/gmm/o/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/c;->a:Lcom/google/android/apps/gmm/o/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/o/a;->a(Lcom/google/android/apps/gmm/o/a;Ljava/lang/String;)V

    .line 161
    return-void

    .line 159
    :cond_5
    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method
