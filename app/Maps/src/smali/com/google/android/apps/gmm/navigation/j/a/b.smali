.class public Lcom/google/android/apps/gmm/navigation/j/a/b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/g/b/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/navigation/g/b/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/f;Lcom/google/android/apps/gmm/navigation/g/b/d;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/navigation/g/b/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/navigation/g/b/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 37
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 70
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method
