.class public Lcom/google/android/apps/gmm/iamhere/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final f:I


# instance fields
.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;>;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/base/activities/c;

.field final d:Lcom/google/android/apps/gmm/v/ar;

.field final e:Lcom/google/android/apps/gmm/v/ar;

.field private final g:Lcom/google/android/apps/gmm/iamhere/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/iamhere/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/a;->a:Ljava/lang/String;

    .line 37
    sget v0, Lcom/google/android/apps/gmm/f;->gc:I

    sput v0, Lcom/google/android/apps/gmm/iamhere/a;->f:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/a;->b:Ljava/util/List;

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/a;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/e/b;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/iamhere/e/b;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/a;->g:Lcom/google/android/apps/gmm/iamhere/e/b;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    sget v1, Lcom/google/android/apps/gmm/iamhere/a;->f:I

    .line 53
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/a;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/content/Context;ILcom/google/android/apps/gmm/v/ao;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/a;->d:Lcom/google/android/apps/gmm/v/ar;

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/a;->g:Lcom/google/android/apps/gmm/iamhere/e/b;

    const-string v2, ""

    .line 55
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/e/b;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/a;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/a;->e:Lcom/google/android/apps/gmm/v/ar;

    .line 56
    return-void
.end method


# virtual methods
.method final declared-synchronized a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/a;->b:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 63
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/b;->a:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 100
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/a;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 76
    :pswitch_1
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 93
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_6

    :cond_2
    move v3, v2

    :goto_1
    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    :goto_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_8

    :cond_3
    move v3, v2

    :goto_3
    if-nez v3, :cond_0

    .line 94
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    if-nez v1, :cond_9

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    move-object v1, v0

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/a;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    const-string v2, "BlueDotAssetManager#maybeFetchIconResources"

    invoke-interface {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    move v3, v1

    .line 93
    goto :goto_1

    :cond_7
    :try_start_2
    const-string v3, ""

    goto :goto_2

    :cond_8
    move v3, v1

    goto :goto_3

    .line 94
    :cond_9
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    goto :goto_4

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
