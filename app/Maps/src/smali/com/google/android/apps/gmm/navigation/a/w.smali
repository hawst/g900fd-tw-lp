.class public Lcom/google/android/apps/gmm/navigation/a/w;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/maps/g/a/al;

.field final c:J

.field d:J

.field e:J

.field private final f:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/maps/g/a/al;JJJLjava/lang/String;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    .line 81
    iput-wide p3, p0, Lcom/google/android/apps/gmm/navigation/a/w;->c:J

    .line 82
    iput-wide p5, p0, Lcom/google/android/apps/gmm/navigation/a/w;->d:J

    .line 83
    iput-wide p7, p0, Lcom/google/android/apps/gmm/navigation/a/w;->e:J

    .line 84
    iput-object p9, p0, Lcom/google/android/apps/gmm/navigation/a/w;->f:Ljava/lang/String;

    .line 85
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/File;
    .locals 8

    .prologue
    .line 150
    const-string v0, "."

    .line 151
    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/v;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-virtual {v2}, Lcom/google/maps/g/a/al;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 157
    :goto_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/w;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/gmm/navigation/a/w;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 153
    :pswitch_0
    const-string v0, ".i."

    goto :goto_0

    .line 156
    :pswitch_1
    const-string v0, ".y."

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/d/b;->a(Lcom/google/maps/g/a/al;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 124
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/a/w;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/a/w;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/a/w;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
