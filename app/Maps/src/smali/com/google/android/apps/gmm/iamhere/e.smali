.class Lcom/google/android/apps/gmm/iamhere/e;
.super Lcom/google/android/apps/gmm/map/t/al;
.source "PG"


# static fields
.field private static final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lcom/google/android/apps/gmm/iamhere/i;

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;>;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/iamhere/c/o;

.field f:Lcom/google/android/apps/gmm/mylocation/d/a;

.field g:Z

.field h:Z

.field private l:Lcom/google/android/apps/gmm/mylocation/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/e;->k:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/iamhere/i;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/t/al;-><init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;)V

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/e;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/e;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->d:Ljava/util/List;

    .line 80
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->e:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->g:Z

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->h:Z

    .line 99
    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/e;->a:Lcom/google/android/apps/gmm/iamhere/i;

    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/f;-><init>(Lcom/google/android/apps/gmm/iamhere/e;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/v/c;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 146
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/mylocation/d/a;)V
    .locals 1

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->f:Lcom/google/android/apps/gmm/mylocation/d/a;

    if-nez v0, :cond_0

    .line 182
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/e;->f:Lcom/google/android/apps/gmm/mylocation/d/a;

    .line 183
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/iamhere/e;->a(Lcom/google/android/apps/gmm/map/b/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :cond_0
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/mylocation/d/a;)V
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->l:Lcom/google/android/apps/gmm/mylocation/d/a;

    if-nez v0, :cond_0

    .line 189
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/e;->l:Lcom/google/android/apps/gmm/mylocation/d/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :cond_0
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 4

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->reset()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->j:Lcom/google/android/apps/gmm/v/c;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/c;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->j:Lcom/google/android/apps/gmm/v/c;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/c;->setRepeatCount(I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->j:Lcom/google/android/apps/gmm/v/c;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/v/c;->setDuration(J)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->j:Lcom/google/android/apps/gmm/v/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/c;->setRepeatMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    monitor-exit p0

    return-void

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()V
    .locals 2

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->f:Lcom/google/android/apps/gmm/mylocation/d/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->i:Lcom/google/android/apps/gmm/v/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 249
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 242
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/e;->e()V

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->start()V

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/e;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method declared-synchronized N_()V
    .locals 1

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->c:Ljava/util/List;

    .line 224
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/e;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->d:Ljava/util/List;

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    monitor-exit p0

    return-void

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/mylocation/d/c;)V
    .locals 5

    .prologue
    .line 257
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/mylocation/d/c;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 259
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/b;

    .line 261
    instance-of v2, v1, Lcom/google/android/apps/gmm/mylocation/d/a;

    if-eqz v2, :cond_0

    .line 262
    sget-object v4, Lcom/google/android/apps/gmm/iamhere/g;->b:[I

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/d/a;

    move-object v2, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/d/a;->i:Lcom/google/android/apps/gmm/mylocation/d/b;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/mylocation/d/b;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 264
    :pswitch_0
    check-cast v1, Lcom/google/android/apps/gmm/mylocation/d/a;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/iamhere/e;->a(Lcom/google/android/apps/gmm/mylocation/d/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 257
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 267
    :pswitch_1
    :try_start_1
    check-cast v1, Lcom/google/android/apps/gmm/mylocation/d/a;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/iamhere/e;->b(Lcom/google/android/apps/gmm/mylocation/d/a;)V

    goto :goto_0

    .line 274
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/e;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    monitor-exit p0

    return-void

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/e;->i:Lcom/google/android/apps/gmm/v/g;

    .line 199
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/e;->f()V

    .line 200
    return-void
.end method

.method final declared-synchronized a(Ljava/util/List;Lcom/google/android/apps/gmm/iamhere/c/o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/v/ci;",
            "Lcom/google/android/apps/gmm/v/ci;",
            ">;>;",
            "Lcom/google/android/apps/gmm/iamhere/c/o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/e;->d:Ljava/util/List;

    .line 206
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/e;->e:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->h:Z

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/e;->N_()V

    .line 210
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/e;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :cond_1
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()V
    .locals 1

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->cancel()V

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/e;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    monitor-exit p0

    return-void

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
