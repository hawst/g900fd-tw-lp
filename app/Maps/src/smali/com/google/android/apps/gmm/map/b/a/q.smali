.class public final Lcom/google/android/apps/gmm/map/b/a/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x1c212a5af57cd033L


# instance fields
.field public a:D

.field public b:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 7

    .prologue
    const-wide v4, 0x4076800000000000L    # 360.0

    const-wide v2, 0x4066800000000000L    # 180.0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpg-double v0, v0, p3

    if-gtz v0, :cond_0

    cmpg-double v0, p3, v2

    if-gez v0, :cond_0

    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    :goto_0
    const-wide v0, -0x3fa9800000000000L    # -90.0

    const-wide v2, 0x4056800000000000L    # 90.0

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 47
    return-void

    .line 46
    :cond_0
    sub-double v0, p3, v2

    rem-double/2addr v0, v4

    add-double/2addr v0, v4

    rem-double/2addr v0, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    goto :goto_0
.end method

.method public static a(II)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8

    .prologue
    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 102
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    int-to-double v2, p0

    mul-double/2addr v2, v6

    int-to-double v4, p1

    mul-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    return-object v0
.end method

.method public static a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8
    .param p0    # Lcom/google/d/a/a/hp;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    .line 270
    if-nez p0, :cond_0

    .line 271
    const/4 v0, 0x0

    .line 275
    :goto_0
    return-object v0

    .line 273
    :cond_0
    iget v1, p0, Lcom/google/d/a/a/hp;->b:I

    .line 274
    iget v2, p0, Lcom/google/d/a/a/hp;->c:I

    .line 275
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    int-to-double v4, v1

    mul-double/2addr v4, v6

    int-to-double v2, v2

    mul-double/2addr v2, v6

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8
    .param p0    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 234
    if-nez p0, :cond_0

    .line 235
    const/4 v0, 0x0

    .line 239
    :goto_0
    return-object v0

    .line 237
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v1

    .line 238
    const/4 v0, 0x2

    invoke-static {p0, v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v2

    .line 239
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    int-to-double v4, v1

    mul-double/2addr v4, v6

    int-to-double v2, v2

    mul-double/2addr v2, v6

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/g/gy;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6
    .param p0    # Lcom/google/maps/g/gy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 161
    if-nez p0, :cond_0

    .line 162
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, p0, Lcom/google/maps/g/gy;->b:D

    iget-wide v4, p0, Lcom/google/maps/g/gy;->c:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_0
.end method

.method public static a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8
    .param p0    # Lcom/google/o/b/a/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    const/4 v1, 0x1

    .line 245
    if-nez p0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-object v0

    .line 248
    :cond_1
    iget v3, p0, Lcom/google/o/b/a/l;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_2

    move v3, v1

    :goto_1
    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/o/b/a/l;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    :goto_2
    if-eqz v1, :cond_0

    .line 251
    iget v1, p0, Lcom/google/o/b/a/l;->b:I

    iget v2, p0, Lcom/google/o/b/a/l;->c:I

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    int-to-double v4, v1

    mul-double/2addr v4, v6

    int-to-double v2, v2

    mul-double/2addr v2, v6

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_0

    :cond_2
    move v3, v2

    .line 248
    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public static a(Lcom/google/r/b/a/hw;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8
    .param p0    # Lcom/google/r/b/a/hw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 288
    if-nez p0, :cond_0

    .line 289
    const/4 v0, 0x0

    .line 293
    :goto_0
    return-object v0

    .line 291
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/hw;->b:I

    .line 292
    iget v2, p0, Lcom/google/r/b/a/hw;->c:I

    .line 293
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    int-to-double v4, v1

    mul-double/2addr v4, v6

    int-to-double v2, v2

    mul-double/2addr v2, v6

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_0
.end method

.method public static b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8
    .param p0    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 260
    if-nez p0, :cond_0

    move-object v0, v1

    .line 264
    :goto_0
    return-object v0

    .line 263
    :cond_0
    const/4 v0, 0x1

    const/16 v2, 0x1a

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 264
    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v2, 0x3

    invoke-static {v0, v2, v6, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;ID)D

    move-result-wide v2

    const/4 v4, 0x2

    invoke-static {v0, v4, v6, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;ID)D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 153
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/q;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 155
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 156
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 157
    return-object v0
.end method

.method public final b()Lcom/google/maps/g/gy;
    .locals 4

    .prologue
    .line 171
    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 172
    iget v1, v0, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v0, Lcom/google/maps/g/ha;->b:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 173
    iget v1, v0, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v0, Lcom/google/maps/g/ha;->c:D

    .line 174
    invoke-virtual {v0}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    return-object v0
.end method

.method public final c()Lcom/google/d/a/a/hp;
    .locals 6

    .prologue
    const-wide v4, 0x416312d000000000L    # 1.0E7

    .line 279
    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 280
    iget v2, v0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/d/a/a/hr;->a:I

    iput v1, v0, Lcom/google/d/a/a/hr;->b:I

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 281
    iget v2, v0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/d/a/a/hr;->a:I

    iput v1, v0, Lcom/google/d/a/a/hr;->c:I

    .line 282
    invoke-virtual {v0}, Lcom/google/d/a/a/hr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    return-object v0
.end method

.method public final d()Lcom/google/r/b/a/hw;
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 297
    invoke-static {}, Lcom/google/r/b/a/hw;->newBuilder()Lcom/google/r/b/a/hy;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 298
    iget v2, v0, Lcom/google/r/b/a/hy;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/r/b/a/hy;->a:I

    iput v1, v0, Lcom/google/r/b/a/hy;->b:I

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iget v2, v0, Lcom/google/r/b/a/hy;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/r/b/a/hy;->a:I

    iput v1, v0, Lcom/google/r/b/a/hy;->c:I

    invoke-virtual {v0}, Lcom/google/r/b/a/hy;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hw;

    return-object v0
.end method

.method public final e()Lcom/google/r/b/a/ia;
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 302
    invoke-static {}, Lcom/google/r/b/a/ia;->newBuilder()Lcom/google/r/b/a/ic;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 303
    iget v2, v0, Lcom/google/r/b/a/ic;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/r/b/a/ic;->a:I

    iput v1, v0, Lcom/google/r/b/a/ic;->b:I

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iget v2, v0, Lcom/google/r/b/a/ic;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/r/b/a/ic;->a:I

    iput v1, v0, Lcom/google/r/b/a/ic;->c:I

    invoke-virtual {v0}, Lcom/google/r/b/a/ic;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-ne p0, p1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 87
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 88
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x3c

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "lat/lng: ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
