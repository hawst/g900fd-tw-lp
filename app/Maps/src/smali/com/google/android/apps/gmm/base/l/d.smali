.class public abstract Lcom/google/android/apps/gmm/base/l/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/aa;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/d;->a:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/d;->b:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    sget v1, Lcom/google/android/apps/gmm/f;->eV:I

    .line 37
    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    new-instance v1, Lcom/google/android/apps/gmm/base/l/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/l/e;-><init>(Lcom/google/android/apps/gmm/base/l/d;)V

    .line 38
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    const/4 v1, 0x2

    .line 44
    iput v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/d;->b:Ljava/lang/String;

    .line 45
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/d;->b:Ljava/lang/String;

    .line 46
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    .line 47
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/d;->a:Ljava/lang/String;

    .line 49
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/base/l/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/l/f;-><init>(Lcom/google/android/apps/gmm/base/l/d;)V

    .line 50
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 56
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    return-object v1
.end method
