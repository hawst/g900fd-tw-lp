.class public Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation

.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "satellite-status"
    b = .enum Lcom/google/android/apps/gmm/util/replay/d;->HIGH:Lcom/google/android/apps/gmm/util/replay/d;
.end annotation

.annotation runtime Lcom/google/android/apps/gmm/util/replay/j;
.end annotation


# instance fields
.field private numUsedInFix:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "numUsedInFix"
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->numUsedInFix:I

    .line 26
    return-void
.end method

.method public static fromGpsStatus(Landroid/location/GpsStatus;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-virtual {p0}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    .line 32
    invoke-virtual {v0}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;-><init>(I)V

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;

    if-nez v1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;

    .line 51
    iget v1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->numUsedInFix:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->numUsedInFix:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getNumUsedInFix()I
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "numUsedInFix"
    .end annotation

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->numUsedInFix:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->numUsedInFix:I

    return v0
.end method
