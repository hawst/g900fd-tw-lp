.class public Lcom/google/android/apps/gmm/base/k/af;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ai;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/google/android/apps/gmm/base/k/ag;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ak;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    .line 332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/k/ad;
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    if-nez v0, :cond_0

    .line 420
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 421
    new-instance v0, Lcom/google/android/apps/gmm/base/k/ae;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/af;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/k/ae;-><init>(Ljava/util/List;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    .line 426
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/base/k/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/k/ad;-><init>(Lcom/google/android/apps/gmm/base/k/ag;)V

    return-object v0

    .line 423
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/base/k/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/k/aj;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    goto :goto_0
.end method

.method public final a(IIZLcom/google/android/apps/gmm/base/k/a;)Lcom/google/android/apps/gmm/base/k/af;
    .locals 7

    .prologue
    .line 396
    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add further states after build() has been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/k/af;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/base/k/ak;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/k/ak;-><init>(IIZLcom/google/android/apps/gmm/base/k/a;Z)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
