.class public Lcom/google/android/apps/gmm/search/restriction/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/h;
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;


# static fields
.field private static final f:Lcom/google/android/libraries/curvular/bi;

.field private static final g:Lcom/google/android/libraries/curvular/bi;

.field private static final j:Ljava/text/SimpleDateFormat;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/l/h;

.field final b:Lcom/google/android/apps/gmm/base/l/h;

.field c:Ljava/lang/Boolean;

.field d:Lcom/google/android/apps/gmm/hotels/a/e;

.field private final e:Ljava/util/Calendar;

.field private final h:Landroid/content/Context;

.field private i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    sget v0, Lcom/google/android/apps/gmm/l;->gZ:I

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/b;->f:Lcom/google/android/libraries/curvular/bi;

    .line 44
    sget v0, Lcom/google/android/apps/gmm/l;->ha:I

    .line 45
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/b;->g:Lcom/google/android/libraries/curvular/bi;

    .line 54
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/b;->j:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Calendar;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->c:Ljava/lang/Boolean;

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->h:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    .line 63
    iget v1, p3, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->i:Ljava/lang/Boolean;

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/base/l/h;

    .line 70
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/search/restriction/c/b;->f:Lcom/google/android/libraries/curvular/bi;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->i:Ljava/lang/Boolean;

    sget-object v6, Lcom/google/b/f/t;->t:Lcom/google/b/f/t;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/l/h;-><init>(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Date;Lcom/google/android/libraries/curvular/bi;Ljava/lang/Boolean;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/base/l/h;

    .line 77
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/search/restriction/c/b;->g:Lcom/google/android/libraries/curvular/bi;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->i:Ljava/lang/Boolean;

    sget-object v6, Lcom/google/b/f/t;->u:Lcom/google/b/f/t;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/l/h;-><init>(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Date;Lcom/google/android/libraries/curvular/bi;Ljava/lang/Boolean;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    .line 83
    invoke-virtual {p0, p4}, Lcom/google/android/apps/gmm/search/restriction/c/b;->a(Lcom/google/r/b/a/alh;)V

    .line 84
    return-void
.end method

.method private static a(Ljava/util/Calendar;Ljava/lang/String;)Ljava/util/Date;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/search/restriction/c/b;->j:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 285
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 286
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 287
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 289
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0

    .line 279
    :catch_0
    move-exception v0

    .line 280
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 3

    .prologue
    .line 132
    invoke-static {}, Lcom/google/maps/g/qx;->d()Lcom/google/maps/g/qx;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/qo;->f:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p1, Lcom/google/maps/g/qo;->a:I

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 8

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/gn;)Lcom/google/r/b/a/alm;

    move-result-object p1

    .line 127
    :cond_0
    return-object p1

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Date;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {}, Lcom/google/maps/g/gn;->newBuilder()Lcom/google/maps/g/gp;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/search/restriction/c/b;->j:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v2, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/maps/g/gp;->a:I

    iput-object v0, v2, Lcom/google/maps/g/gp;->b:Ljava/lang/Object;

    iget v0, v2, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Lcom/google/maps/g/gp;->a:I

    iput v1, v2, Lcom/google/maps/g/gp;->c:I

    invoke-virtual {v2}, Lcom/google/maps/g/gp;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    goto :goto_0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/16 v7, 0xb

    const/4 v6, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    iget-object v0, p1, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    .line 89
    iget v1, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_3

    move v1, v3

    :goto_0
    if-eqz v1, :cond_0

    .line 90
    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    move v1, v3

    .line 89
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->c:Ljava/lang/Boolean;

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 94
    invoke-virtual {v0}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    :cond_1
    move v1, v3

    :goto_2
    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    .line 95
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 98
    :goto_3
    iget v4, v0, Lcom/google/maps/g/gn;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    move v4, v3

    :goto_4
    if-eqz v4, :cond_2

    .line 99
    iget v3, v0, Lcom/google/maps/g/gn;->c:I

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    invoke-virtual {v0, v6, v3}, Ljava/util/Calendar;->add(II)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    .line 107
    new-instance v4, Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    .line 108
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-static {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Ljava/util/Date;)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    .line 109
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-static {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Ljava/util/Date;)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/google/android/apps/gmm/hotels/a/e;-><init>(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/i;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->d:Lcom/google/android/apps/gmm/hotels/a/e;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    iput-object v3, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v3, -0x1

    invoke-virtual {v1, v6, v3}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1, v7, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1, v8, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xd

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xe

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    .line 116
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v4, 0xb4

    invoke-virtual {v3, v6, v4}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v3, v7, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v3, v8, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xd

    invoke-virtual {v3, v4, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xe

    invoke-virtual {v3, v4, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    .line 114
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/h;->c:Ljava/util/Date;

    iput-object v3, v0, Lcom/google/android/apps/gmm/base/l/h;->d:Ljava/util/Date;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    .line 118
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1, v7, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1, v8, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xd

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xe

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v4, 0xb4

    invoke-virtual {v3, v6, v4}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v3, v7, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v3, v8, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xd

    invoke-virtual {v3, v4, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xe

    invoke-virtual {v3, v4, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 117
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/h;->c:Ljava/util/Date;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/l/h;->d:Ljava/util/Date;

    .line 120
    return-void

    :cond_3
    move v1, v2

    .line 89
    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 90
    goto/16 :goto_1

    :cond_5
    move v1, v2

    .line 94
    goto/16 :goto_2

    .line 95
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->e:Ljava/util/Calendar;

    invoke-virtual {v0}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/apps/gmm/search/restriction/c/b;->a(Ljava/util/Calendar;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    goto/16 :goto_3

    :cond_7
    move v4, v2

    .line 98
    goto/16 :goto_4
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    new-instance v1, Lcom/google/android/apps/gmm/search/restriction/c/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/search/restriction/c/c;-><init>(Lcom/google/android/apps/gmm/search/restriction/c/b;Ljava/lang/Runnable;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/h;->a:Ljava/lang/Runnable;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    new-instance v1, Lcom/google/android/apps/gmm/search/restriction/c/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/search/restriction/c/d;-><init>(Lcom/google/android/apps/gmm/search/restriction/c/b;Ljava/lang/Runnable;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/h;->a:Ljava/lang/Runnable;

    .line 182
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 255
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->h:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    .line 257
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    add-long/2addr v2, v6

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    .line 258
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/h;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    add-long/2addr v4, v6

    const v6, 0x10010

    .line 255
    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/base/l/a/i;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->a:Lcom/google/android/apps/gmm/base/l/h;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/l/a/i;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/b;->b:Lcom/google/android/apps/gmm/base/l/h;

    return-object v0
.end method
