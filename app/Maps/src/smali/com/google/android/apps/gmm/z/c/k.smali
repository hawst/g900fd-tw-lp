.class public Lcom/google/android/apps/gmm/z/c/k;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Lcom/google/android/apps/gmm/z/c/d;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field final e:J

.field f:Z

.field g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field h:Z

.field private final i:J

.field private j:Lcom/google/android/apps/gmm/z/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/z/c/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/c/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/z/c/d;)V
    .locals 4

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/k;->h:Z

    .line 78
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/c/k;->c:Lcom/google/android/apps/gmm/z/c/d;

    .line 80
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 82
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v0

    .line 83
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget v2, v2, Lcom/google/r/b/a/jn;->e:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/z/c/k;->e:J

    .line 84
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget v0, v0, Lcom/google/r/b/a/jn;->f:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/z/c/k;->i:J

    .line 85
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/login/a/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    invoke-interface {p0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    .line 107
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method


# virtual methods
.method declared-synchronized a()Lcom/google/android/apps/gmm/z/c/a;
    .locals 2

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->j:Lcom/google/android/apps/gmm/z/c/a;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/z/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/c/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->j:Lcom/google/android/apps/gmm/z/c/a;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->j:Lcom/google/android/apps/gmm/z/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->j:Lcom/google/android/apps/gmm/z/c/a;

    if-eqz v0, :cond_0

    .line 96
    sget-object v0, Lcom/google/android/apps/gmm/z/c/k;->a:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->j:Lcom/google/android/apps/gmm/z/c/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/a;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_0
    monitor-exit p0

    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Ljava/util/List;
    .locals 14
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 128
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/z/c/k;->d()Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/z/c/k;->a()Lcom/google/android/apps/gmm/z/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 136
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/k;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v10

    .line 137
    iget-wide v2, p0, Lcom/google/android/apps/gmm/z/c/k;->e:J

    sub-long v2, v10, v2

    .line 141
    iget-wide v4, p0, Lcom/google/android/apps/gmm/z/c/k;->i:J

    add-long/2addr v4, v12

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/c/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;JJ)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 143
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    int-to-long v4, v3

    .line 144
    iget-wide v8, p0, Lcom/google/android/apps/gmm/z/c/k;->i:J

    cmp-long v3, v4, v8

    if-lez v3, :cond_2

    .line 145
    iget-wide v4, p0, Lcom/google/android/apps/gmm/z/c/k;->i:J

    move-wide v8, v4

    .line 152
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_4

    .line 153
    if-eqz v2, :cond_1

    .line 172
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 174
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v0, v6

    .line 177
    :goto_1
    return-object v0

    .line 147
    :cond_2
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 148
    const/4 v3, 0x0

    :try_start_3
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/z/c/k;->f:Z

    .line 149
    iput-object v1, p0, Lcom/google/android/apps/gmm/z/c/k;->g:Ljava/lang/String;

    .line 150
    monitor-exit p0

    move-wide v8, v4

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 171
    :catchall_1
    move-exception v1

    move-object v6, v2

    :goto_2
    if-eqz v6, :cond_3

    .line 172
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 174
    :cond_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 156
    :cond_4
    :try_start_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 157
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 158
    const-wide/16 v4, 0x0

    :goto_3
    cmp-long v6, v4, v8

    if-gez v6, :cond_5

    .line 159
    iget-object v6, p0, Lcom/google/android/apps/gmm/z/c/k;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-static {v2, v10, v11, v6, v1}, Lcom/google/android/apps/gmm/z/c/a;->a(Landroid/database/Cursor;JLcom/google/android/apps/gmm/shared/c/f;Ljava/util/List;)I

    move-result v6

    .line 160
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 158
    add-long/2addr v4, v12

    goto :goto_3

    .line 164
    :cond_5
    sget-object v4, Lcom/google/android/apps/gmm/z/c/k;->a:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x1b

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Retrying "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " events"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/z/c/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 169
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 171
    if-eqz v2, :cond_6

    .line 172
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 174
    :cond_6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v0, v1

    .line 177
    goto :goto_1

    .line 171
    :catchall_2
    move-exception v1

    goto :goto_2
.end method

.method declared-synchronized d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/k;->h:Z

    if-nez v0, :cond_1

    .line 307
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 310
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    .line 311
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/k;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/z/c/k;->h:Z

    if-eqz v1, :cond_0

    .line 312
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/z/c/k;->f:Z

    .line 313
    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/k;->g:Ljava/lang/String;

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/k;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/c/d;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 315
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/z/c/k;->h:Z

    .line 316
    new-instance v1, Lcom/google/android/apps/gmm/z/c/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/z/c/m;-><init>(Lcom/google/android/apps/gmm/z/c/k;Ljava/lang/String;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 318
    :cond_4
    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/z/c/k;->h:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 329
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/k;->h:Z

    .line 330
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/z/c/k;->d()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    monitor-exit p0

    return-void

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
