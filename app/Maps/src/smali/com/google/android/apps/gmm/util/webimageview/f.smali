.class public Lcom/google/android/apps/gmm/util/webimageview/f;
.super Lcom/google/android/apps/gmm/util/webimageview/k;
.source "PG"


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

.field private final c:Lcom/google/android/apps/gmm/util/webimageview/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;Lcom/google/android/apps/gmm/util/webimageview/g;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/util/webimageview/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->b:Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/util/webimageview/k;-><init>()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->a:Z

    .line 85
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->c:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 86
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->c:Lcom/google/android/apps/gmm/util/webimageview/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/webimageview/g;->b()V

    .line 106
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->b:Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    iget v0, v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->d:I

    if-gtz v0, :cond_2

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->b:Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a(Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;Landroid/graphics/Bitmap;)V

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->c:Lcom/google/android/apps/gmm/util/webimageview/g;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->c:Lcom/google/android/apps/gmm/util/webimageview/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/webimageview/g;->a()V

    .line 101
    :cond_1
    return-void

    .line 95
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/webimageview/f;->b:Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->setFadingImage(Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
