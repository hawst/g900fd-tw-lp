.class public Lcom/google/android/apps/gmm/tutorial/BlueDotTutorialView;
.super Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;
.source "PG"


# instance fields
.field private e:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;-><init>(Landroid/content/Context;)V

    .line 22
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/tutorial/BlueDotTutorialView;->e:Lcom/google/android/apps/gmm/base/activities/c;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/tutorial/BlueDotTutorialView;->e:Lcom/google/android/apps/gmm/base/activities/c;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/tutorial/BlueDotTutorialView;->e:Lcom/google/android/apps/gmm/base/activities/c;

    .line 33
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 37
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/tutorial/RedrawBaseTutorialView;->onLayout(ZIIII)V

    .line 39
    sget v0, Lcom/google/android/apps/gmm/g;->en:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/gmm/tutorial/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/tutorial/a;-><init>(Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/gmm/tutorial/BlueDotTutorialView;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/c;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v5, v4, Landroid/graphics/Point;->x:I

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, -0x14

    iget v6, v4, Landroid/graphics/Point;->x:I

    div-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x14

    iget v7, v4, Landroid/graphics/Point;->y:I

    div-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x14

    iget v4, v4, Landroid/graphics/Point;->y:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x14

    int-to-float v5, v5

    cmpl-float v5, v2, v5

    if-ltz v5, :cond_0

    int-to-float v5, v6

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_0

    int-to-float v2, v7

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_0

    int-to-float v2, v4

    cmpg-float v2, v3, v2

    if-gtz v2, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 49
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 46
    goto :goto_0

    :cond_1
    move v0, v1

    .line 49
    goto :goto_1
.end method
