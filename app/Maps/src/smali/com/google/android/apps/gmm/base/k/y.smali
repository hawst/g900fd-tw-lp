.class Lcom/google/android/apps/gmm/base/k/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/google/android/apps/gmm/base/k/o;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/k/p;Landroid/view/View;Lcom/google/android/apps/gmm/base/k/o;)V
    .locals 0

    .prologue
    .line 1038
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    sget v1, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    sget-object v1, Lcom/google/android/apps/gmm/base/k/p;->b:Lcom/google/android/libraries/curvular/bk;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/k/o;->a:Lcom/google/android/libraries/curvular/au;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 1045
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/k/o;->c:Lcom/google/android/libraries/curvular/au;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1046
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/k/o;->b:Lcom/google/android/libraries/curvular/au;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    .line 1047
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/k/o;->d:Lcom/google/android/libraries/curvular/au;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/y;->a:Landroid/view/View;

    .line 1048
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget v1, v1, Lcom/google/android/apps/gmm/base/k/o;->f:I

    int-to-long v2, v1

    .line 1049
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/k/o;->e:Landroid/animation/TimeInterpolator;

    .line 1050
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/y;->b:Lcom/google/android/apps/gmm/base/k/o;

    iget v1, v1, Lcom/google/android/apps/gmm/base/k/o;->g:I

    int-to-long v2, v1

    .line 1051
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 1052
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1053
    const/4 v0, 0x1

    return v0
.end method
