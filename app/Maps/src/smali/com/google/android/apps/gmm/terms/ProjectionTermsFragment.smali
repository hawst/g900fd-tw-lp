.class public abstract Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;
.super Lcom/google/android/apps/gmm/base/fragments/OobFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/terms/b/b;


# instance fields
.field a:Z

.field private b:Lcom/google/android/apps/gmm/terms/b/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->a:Z

    return-void
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/base/f/bl;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 50
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 51
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 58
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->o()Lcom/google/android/apps/gmm/terms/b/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->b:Lcom/google/android/apps/gmm/terms/b/a;

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->b:Lcom/google/android/apps/gmm/terms/b/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 60
    return-object v0

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/base/f/bk;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 53
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 54
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    goto :goto_0
.end method

.method protected abstract o()Lcom/google/android/apps/gmm/terms/b/a;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    if-eqz p1, :cond_0

    .line 35
    const-string v0, "isChecked"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->a:Z

    .line 37
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 42
    const-string v0, "isChecked"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 43
    return-void
.end method

.method protected abstract p()V
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->b:Lcom/google/android/apps/gmm/terms/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/terms/b/a;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->a:Z

    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->a:Z

    if-eqz v0, :cond_1

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->b()V

    .line 72
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->p()V

    .line 73
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final r()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    :goto_0
    return v0

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    .line 82
    const/4 v0, 0x1

    goto :goto_0
.end method
