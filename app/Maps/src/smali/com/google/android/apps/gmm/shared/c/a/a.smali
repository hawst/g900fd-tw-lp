.class public Lcom/google/android/apps/gmm/shared/c/a/a;
.super Landroid/os/HandlerThread;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/c/a/r;


# instance fields
.field private a:Ljava/lang/Runnable;

.field private final b:Lcom/google/android/apps/gmm/shared/c/a/p;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/shared/c/a/p;ILcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->a:Ljava/lang/Runnable;

    .line 141
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->b:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 142
    invoke-static {p0, p2, p3}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 147
    invoke-direct {p0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 28
    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->a:Ljava/lang/Runnable;

    .line 148
    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->b:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 149
    const/4 v0, 0x4

    invoke-static {p0, v0, p2}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 150
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/shared/c/a/a;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x4

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;I)Lcom/google/android/apps/gmm/shared/c/a/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;I)Lcom/google/android/apps/gmm/shared/c/a/a;
    .locals 4

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-direct {v0, p0, p3, p1}, Lcom/google/android/apps/gmm/shared/c/a/a;-><init>(Lcom/google/android/apps/gmm/shared/c/a/p;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 83
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->start()V

    .line 84
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/c;-><init>(Landroid/os/Looper;)V

    .line 85
    if-eqz p2, :cond_0

    .line 86
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a()Lcom/google/android/apps/gmm/shared/c/a/o;

    move-result-object v2

    .line 87
    invoke-virtual {v2, p0, v1}, Lcom/google/android/apps/gmm/shared/c/a/o;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/shared/c/a/i;)V

    .line 88
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/a/b;

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/gmm/shared/c/a/b;-><init>(Lcom/google/android/apps/gmm/shared/c/a/o;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/shared/c/a/a;->a:Ljava/lang/Runnable;

    .line 97
    :goto_0
    return-object v0

    .line 95
    :cond_0
    const-string v1, "GmmHandlerThread"

    const-string v2, "Cannot register a ThreadExecutor without a ThreadPoolService."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final P_()Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->b:Lcom/google/android/apps/gmm/shared/c/a/p;

    return-object v0
.end method

.method public quit()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 118
    :cond_0
    invoke-super {p0}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    return v0
.end method
