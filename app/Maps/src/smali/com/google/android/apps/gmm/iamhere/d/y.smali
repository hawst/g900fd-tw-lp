.class public Lcom/google/android/apps/gmm/iamhere/d/y;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field static final c:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final f:Lcom/google/android/apps/gmm/shared/b/a;

.field private final g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final h:Ljava/lang/Object;

.field private final i:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/y;->a:Ljava/lang/String;

    .line 39
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aX:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-static {v0, v1}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/y;->c:Lcom/google/b/c/dn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 82
    .line 83
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    .line 84
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    .line 85
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    .line 82
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/google/android/apps/gmm/iamhere/d/y;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V

    .line 86
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/z;-><init>(Lcom/google/android/apps/gmm/iamhere/d/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/aa;-><init>(Lcom/google/android/apps/gmm/iamhere/d/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->h:Ljava/lang/Object;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/ab;-><init>(Lcom/google/android/apps/gmm/iamhere/d/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->i:Landroid/content/BroadcastReceiver;

    .line 94
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->d:Landroid/content/Context;

    .line 95
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 96
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/shared/net/a/b;

    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->e:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 97
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/shared/b/a;

    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->f:Lcom/google/android/apps/gmm/shared/b/a;

    .line 98
    return-void
.end method

.method private d()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 136
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "location_mode"

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 145
    :goto_1
    return v0

    :cond_0
    move v0, v2

    .line 134
    goto :goto_0

    :cond_1
    move v0, v2

    .line 136
    goto :goto_1

    .line 138
    :catch_0
    move-exception v0

    .line 139
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/y;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move v0, v2

    .line 140
    goto :goto_1

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->d:Landroid/content/Context;

    const-string v3, "location"

    .line 144
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 145
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->f:Lcom/google/android/apps/gmm/shared/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->h:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 103
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 104
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "android.location.MODE_CHANGED"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "android.location.PROVIDERS_CHANGED"

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->h:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->f:Lcom/google/android/apps/gmm/shared/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->g:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 113
    return-void
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->f:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->aX:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v3

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->f:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 118
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/d/y;->d()Z

    move-result v4

    .line 119
    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/d/y;->e:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 120
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/net/a/b;->z()Lcom/google/r/b/a/ie;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/r/b/a/ie;->b:Z

    .line 122
    sget-object v6, Lcom/google/android/apps/gmm/iamhere/d/y;->a:Ljava/lang/String;

    const-string v6, "User Enabled: %b, Server Enabled: %b Tos Accepted: %b Location Services:%b"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    .line 128
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v1

    const/4 v1, 0x2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v7, v1

    const/4 v0, 0x3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v7, v0

    .line 126
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 130
    return v2

    :cond_0
    move v0, v2

    .line 117
    goto :goto_0
.end method
