.class public Lcom/google/android/apps/gmm/map/internal/d/bd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final e:[Lcom/google/android/apps/gmm/map/b/a/ai;

.field private static final f:I


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/d/as;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/map/internal/d/r;

.field public final c:Lcom/google/android/apps/gmm/map/internal/d/ag;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field private final h:Landroid/content/res/Resources;

.field private final i:Ljava/io/File;

.field private final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/bd;->e:[Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 82
    sget v0, Lcom/google/android/apps/gmm/k;->O:I

    sput v0, Lcom/google/android/apps/gmm/map/internal/d/bd;->f:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;)V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 29
    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    .line 32
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 31
    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->d:Ljava/util/Map;

    .line 95
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->a()Landroid/content/Context;

    move-result-object v1

    .line 96
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->h:Landroid/content/res/Resources;

    .line 97
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->g:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 99
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 101
    sget v0, Lcom/google/android/apps/gmm/map/internal/d/bd;->f:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->h:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/apps/gmm/map/internal/d/bd;->f:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/br;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->i:Ljava/io/File;

    .line 110
    iput-boolean v7, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->j:Z

    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 112
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/aw;

    move-result-object v4

    .line 114
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    const-string v2, "PSM Difftile"

    .line 117
    invoke-virtual {v4, p1}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;)Lcom/google/android/apps/gmm/map/internal/d/b/as;

    move-result-object v3

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 119
    invoke-static {v6, p1}, Lcom/google/android/apps/gmm/map/internal/d/be;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/ac;)Ljava/lang/String;

    move-result-object v6

    .line 118
    invoke-virtual {v4, p1, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v4

    .line 122
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->i:Ljava/io/File;

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/d/au;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/as;Lcom/google/android/apps/gmm/map/internal/d/b/s;ZLjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/ac;)V

    .line 125
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 127
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/t;

    const/16 v3, 0x2a8

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/gmm/map/internal/d/t;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;I)V

    invoke-direct {v0, p1, v1, v2, v9}, Lcom/google/android/apps/gmm/map/internal/d/r;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/internal/d/t;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->b:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->b:Lcom/google/android/apps/gmm/map/internal/d/r;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/r;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ag;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->b:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-direct {v0, p1, p0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/ag;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/bd;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/r;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->c:Lcom/google/android/apps/gmm/map/internal/d/ag;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->c:Lcom/google/android/apps/gmm/map/internal/d/ag;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->B_()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Landroid/accounts/Account;)V

    .line 138
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 139
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->d:Ljava/util/Map;

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 104
    :catch_0
    move-exception v0

    .line 105
    const-string v2, "Could not load encryption key"

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 143
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->e:[Lcom/google/android/apps/gmm/map/b/a/ai;

    array-length v2, v1

    :goto_2
    if-ge v5, v2, :cond_3

    aget-object v3, v1, v5

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 144
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 152
    :cond_3
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 180
    if-nez v0, :cond_1

    .line 181
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/aw;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->g:Lcom/google/android/apps/gmm/map/internal/d/ac;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->h:Landroid/content/res/Resources;

    .line 185
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->i:Ljava/io/File;

    const/4 v5, 0x1

    .line 189
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/ai;->b()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->b:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 190
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/ai;->b()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->c:Lcom/google/android/apps/gmm/map/internal/d/ag;

    .line 182
    :cond_0
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZLcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_3

    .line 192
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/as;->i()V

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_1
    monitor-exit v8

    return-object v0

    :cond_2
    move-object v6, v7

    .line 189
    goto :goto_0

    .line 195
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to create TileStore for type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 236
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/as;->j()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 237
    :catch_0
    move-exception v2

    .line 238
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/as;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not stop "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " tile store"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 242
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 221
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Z)V

    goto :goto_0

    .line 223
    :cond_0
    return-void
.end method
