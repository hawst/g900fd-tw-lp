.class public Lcom/google/android/apps/gmm/directions/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/a/a;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field final b:Lcom/google/android/apps/gmm/map/t;

.field private c:Lcom/google/android/apps/gmm/directions/a/d;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/e;Lcom/google/android/apps/gmm/map/x;)V
    .locals 6

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/e;->d:Landroid/content/res/Resources;

    .line 71
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/e;->b:Lcom/google/android/apps/gmm/map/t;

    .line 72
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/directions/e/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/e/a;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/e;Lcom/google/android/apps/gmm/map/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    .line 75
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/map/r/a/e;Z)Lcom/google/android/apps/gmm/directions/a/c;
    .locals 3

    .prologue
    .line 250
    new-instance v0, Lcom/google/android/apps/gmm/directions/aa;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/e;->d:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/aa;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;)V

    .line 251
    invoke-virtual {v0, p1, p3, p2}, Lcom/google/android/apps/gmm/directions/aa;->a(Lcom/google/android/apps/gmm/directions/f/a;ZLcom/google/android/apps/gmm/map/r/a/e;)V

    .line 252
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/map/r/a/f;Z)Lcom/google/android/apps/gmm/directions/a/c;
    .locals 3

    .prologue
    .line 261
    new-instance v0, Lcom/google/android/apps/gmm/directions/aa;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/e;->d:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/aa;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;)V

    .line 262
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/directions/aa;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/map/r/a/f;Z)V

    .line 263
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/hy;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/directions/a/c;
    .locals 9
    .param p2    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 191
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-object v6

    .line 195
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/e;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    .line 196
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_6

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    if-eqz v0, :cond_0

    .line 201
    new-instance v6, Lcom/google/android/apps/gmm/directions/aa;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/e;->d:Landroid/content/res/Resources;

    invoke-direct {v6, v0, v2}, Lcom/google/android/apps/gmm/directions/aa;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;)V

    .line 202
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/z/b/f;-><init>(Lcom/google/maps/g/hy;)V

    .line 203
    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v4, v2, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, v2, Lcom/google/maps/g/ia;->a:I

    iput-boolean v1, v2, Lcom/google/maps/g/ia;->f:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v5

    check-cast v5, Lcom/google/maps/g/hy;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/gmm/directions/f;

    move-object v1, p0

    move v2, p4

    move-object v4, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/directions/f;-><init>(Lcom/google/android/apps/gmm/directions/e;ZLcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/directions/aa;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v8, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0

    :cond_5
    move v2, v0

    .line 196
    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 14
    .param p1    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const-wide v12, 0x412e848000000000L    # 1000000.0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 141
    if-eqz p1, :cond_1

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v3, v4, :cond_0

    move v3, v2

    :goto_0
    if-eqz v3, :cond_1

    .line 142
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 166
    :goto_1
    return-object v0

    :cond_0
    move v3, v1

    .line 141
    goto :goto_0

    .line 147
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v3

    if-nez v3, :cond_4

    move-object v3, v0

    :goto_2
    if-eqz v3, :cond_5

    move v3, v2

    :goto_3
    if-nez v3, :cond_7

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v4, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v3, v4, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v3, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    if-eqz v1, :cond_6

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_1

    .line 147
    :cond_4
    invoke-interface {v3}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v3

    goto :goto_2

    :cond_5
    move v3, v1

    goto :goto_3

    .line 152
    :cond_6
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_1

    .line 157
    :cond_7
    if-eqz p1, :cond_d

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v3, :cond_9

    move v3, v2

    :goto_4
    if-eqz v3, :cond_d

    .line 158
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v4, :cond_a

    const-string v2, "DirectionsControllerImpl"

    const-string v3, "Caller should handle null latLng"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    :goto_5
    if-eqz v1, :cond_d

    .line 159
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "End point is too far from My Location: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    move v3, v1

    .line 157
    goto :goto_4

    .line 158
    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v3

    if-nez v3, :cond_b

    move-object v3, v0

    :goto_6
    if-nez v3, :cond_c

    const-string v2, "DirectionsControllerImpl"

    const-string v3, "Caller should handle unavailable location"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_b
    invoke-interface {v3}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v3

    goto :goto_6

    :cond_c
    iget-wide v6, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v6, v7, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/n;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v6

    mul-double/2addr v6, v12

    add-double/2addr v6, v10

    double-to-int v6, v6

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v8

    mul-double/2addr v8, v12

    add-double/2addr v8, v10

    double-to-int v3, v8

    invoke-direct {v5, v6, v3}, Lcom/google/android/apps/gmm/map/b/a/n;-><init>(II)V

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/n;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v6, v3

    iget v3, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v10

    mul-double/2addr v4, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v8

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v4

    div-double v4, v6, v4

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x46

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Destination distance from My Location: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " meters"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide v6, 0x4122ebc000000000L    # 620000.0

    cmpl-double v3, v4, v6

    if-lez v3, :cond_8

    move v1, v2

    goto/16 :goto_5

    .line 164
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    if-nez v1, :cond_e

    .line 166
    :goto_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto/16 :goto_1

    .line 164
    :cond_e
    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    goto :goto_7
.end method

.method public final a()Lcom/google/maps/g/a/hm;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/f;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/hm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/f/a/a;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    .line 277
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/q;Z)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/q;Z)V

    .line 285
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/f/a/c;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/directions/f/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/f/a/c;)V

    .line 310
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/indoor/b/c;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/d;->b()V

    .line 269
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V

    .line 324
    return-void
.end method

.method public final a(Lcom/google/maps/g/a/hm;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/directions/f/d/f;->a(Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/maps/g/a/hm;)V

    .line 90
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/d;->b()V

    .line 293
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e;->c:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/d;->c()V

    .line 318
    return-void
.end method
