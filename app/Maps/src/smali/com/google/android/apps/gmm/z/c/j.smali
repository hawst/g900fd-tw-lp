.class Lcom/google/android/apps/gmm/z/c/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/z/c/f;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/z/c/f;)V
    .locals 0

    .prologue
    .line 673
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/j;->a:Lcom/google/android/apps/gmm/z/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 687
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/j;->b:Z

    if-nez v0, :cond_0

    .line 688
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/j;->b:Z

    .line 689
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/j;->a:Lcom/google/android/apps/gmm/z/c/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p0, v1, p1, p2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 692
    :cond_0
    monitor-exit p0

    return-void

    .line 687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 1

    .prologue
    .line 679
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/j;->a:Lcom/google/android/apps/gmm/z/c/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/z/c/f;->g:Z

    if-nez v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/j;->a:Lcom/google/android/apps/gmm/z/c/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/f;->a()V

    .line 681
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/j;->a:Lcom/google/android/apps/gmm/z/c/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/f;->f:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/b;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/d;->a()V

    .line 683
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/j;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 684
    monitor-exit p0

    return-void

    .line 679
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
