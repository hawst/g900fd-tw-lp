.class public Lcom/google/android/apps/gmm/navigation/d/b;
.super Lcom/google/android/apps/gmm/navigation/commonui/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/d/a;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field public b:Z

.field public c:Z

.field private final d:Lcom/google/android/apps/gmm/navigation/commonui/d;

.field private final e:Lcom/google/android/apps/gmm/shared/b/a;

.field private f:Z

.field private final g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/a;-><init>()V

    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/navigation/d/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/d/c;-><init>(Lcom/google/android/apps/gmm/navigation/d/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->g:Ljava/lang/Object;

    .line 48
    const-string v0, "host"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/commonui/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/d/b;->d:Lcom/google/android/apps/gmm/navigation/commonui/d;

    .line 49
    const-string v0, "eventBus"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/d/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 50
    const-string v0, "gmmSettings"

    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/gmm/shared/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->e:Lcom/google/android/apps/gmm/shared/b/a;

    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->ai:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {p3, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    .line 52
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/d/b;->c:Z

    .line 53
    return-void

    :cond_3
    move v0, v2

    .line 51
    goto :goto_0
.end method


# virtual methods
.method public G_()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/d/b;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 3

    .prologue
    .line 89
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->b:Z

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/map/j/c;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/d/b;->d()Lcom/google/android/apps/gmm/map/b/a/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/c;-><init>(Lcom/google/android/apps/gmm/map/b/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/j/b;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->d:Lcom/google/android/apps/gmm/navigation/commonui/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/d;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 79
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/b;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/f;->i:Z

    if-nez v0, :cond_1

    .line 86
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->e:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->ai:Lcom/google/android/apps/gmm/shared/b/c;

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    if-nez v4, :cond_4

    :goto_2
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/map/j/c;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/d/b;->d()Lcom/google/android/apps/gmm/map/b/a/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/c;-><init>(Lcom/google/android/apps/gmm/map/b/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/d/a/a;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/d/a/a;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 82
    goto :goto_1

    :cond_4
    move v1, v2

    .line 83
    goto :goto_2
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/d/b;->e:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ai:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/d/b;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 64
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    return v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/b/a/f;
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->c:Z

    if-eqz v0, :cond_1

    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->g:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 109
    :goto_0
    return-object v0

    .line 103
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->f:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0

    .line 105
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->f:Z

    if-eqz v0, :cond_3

    .line 106
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->b:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->d:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0

    .line 109
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/d/b;->b:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->e:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->c:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0
.end method
