.class public Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/internal/c/cf;

.field b:Lcom/google/android/apps/gmm/map/b/a/q;

.field c:Z

.field d:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/traffic/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/navigation/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/cf;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;-><init>()V

    .line 72
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 73
    const-string v2, "trafficIncidentMetadata"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 74
    const-string v2, "trafficIncidentLocation"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 75
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->setArguments(Landroid/os/Bundle;)V

    .line 76
    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->M()Lcom/google/android/apps/gmm/traffic/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/traffic/a/a;->c()V

    .line 94
    return-void
.end method

.method a(Z)V
    .locals 6

    .prologue
    .line 253
    if-eqz p1, :cond_1

    .line 255
    new-instance v0, Lcom/google/android/apps/gmm/map/g/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 256
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/g/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/cf;)V

    .line 257
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/g/e;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    new-instance v3, Lcom/google/android/apps/gmm/map/o/aj;

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Lcom/google/android/apps/gmm/map/internal/c/p;)V

    invoke-direct {v3, v1, v4, v2, v0}, Lcom/google/android/apps/gmm/map/o/aj;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/c/cf;Lcom/google/android/apps/gmm/map/g/e;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ag;)V

    .line 263
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ag;)V

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->M()Lcom/google/android/apps/gmm/traffic/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/traffic/a/a;->c()V

    .line 207
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    if-eqz p1, :cond_0

    .line 108
    :goto_0
    const-string v0, "trafficIncidentMetadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 109
    :cond_1
    const-string v0, "trafficIncidentMetadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/cf;

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-nez v0, :cond_2

    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->e:Ljava/lang/String;

    const-string v1, "onCreate incidentMetadata should not be null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :cond_2
    const-string v0, "trafficIncidentLocation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_3

    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->e:Ljava/lang/String;

    const-string v1, "onCreate incidentLocation should not be null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/traffic/b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->d:Lcom/google/android/libraries/curvular/ae;

    .line 119
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 193
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 196
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a(Z)V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->f:Lcom/google/android/apps/gmm/navigation/b/g;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->f:Lcom/google/android/apps/gmm/navigation/b/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/g;->c()V

    .line 201
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 123
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/f;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->c:Z

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/traffic/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->c:Z

    iget-object v6, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->d:Lcom/google/android/libraries/curvular/ae;

    .line 127
    iget-object v6, v6, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-direct {v0, p0, v1, v4, v6}, Lcom/google/android/apps/gmm/traffic/d;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/map/internal/c/cf;ZLcom/google/android/libraries/curvular/ag;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 131
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a(Z)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    move-object v6, v0

    .line 139
    :goto_0
    if-nez v6, :cond_5

    .line 140
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    .line 141
    :goto_1
    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    .line 142
    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/ag;->l:Z

    .line 144
    if-eqz v6, :cond_0

    .line 145
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/activities/p;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_0
    move v4, v2

    .line 146
    :goto_2
    if-eqz v6, :cond_8

    .line 147
    iget-boolean v1, v6, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 149
    :goto_3
    iget-object v7, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v7, v7, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 153
    new-instance v8, Lcom/google/android/apps/gmm/traffic/c;

    invoke-direct {v8, p0}, Lcom/google/android/apps/gmm/traffic/c;-><init>(Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 166
    new-instance v8, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 167
    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v10, v9, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v9, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 168
    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v2, v9, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 169
    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v10, v9, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v9, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 170
    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v9, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v7, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v9, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v9, v7, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v7, Lcom/google/android/apps/gmm/map/b/a/f;->f:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 171
    iget-object v9, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v9, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 172
    iget-object v7, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v4, :cond_1

    move v2, v3

    :cond_1
    iput v2, v7, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 173
    iget-object v2, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 174
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 175
    iget-object v1, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 176
    iget-object v0, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 177
    iget-object v0, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v0

    .line 180
    if-eqz v6, :cond_2

    if-eqz v0, :cond_2

    if-nez v4, :cond_2

    .line 181
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->c:Z

    if-eqz v1, :cond_2

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/b/f;->a(Landroid/app/Activity;)Lcom/google/android/apps/gmm/navigation/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->f:Lcom/google/android/apps/gmm/navigation/b/g;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->f:Lcom/google/android/apps/gmm/navigation/b/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/g;->b()V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->f:Lcom/google/android/apps/gmm/navigation/b/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/g;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, v8, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 189
    return-void

    .line 138
    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    move-object v6, v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    move-object v6, v0

    goto/16 :goto_0

    .line 140
    :cond_5
    iget-object v0, v6, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    if-eqz v0, :cond_6

    iget-object v0, v6, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    goto/16 :goto_1

    :cond_6
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move v4, v5

    .line 145
    goto/16 :goto_2

    :cond_8
    move v1, v2

    .line 147
    goto/16 :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 99
    const-string v0, "trafficIncidentMetadata"

    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 100
    const-string v0, "trafficIncidentLocation"

    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 101
    return-void
.end method
