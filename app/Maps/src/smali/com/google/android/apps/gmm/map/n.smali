.class final Lcom/google/android/apps/gmm/map/n;
.super Lcom/google/android/apps/gmm/map/a;
.source "PG"


# instance fields
.field final synthetic b:Lcom/google/android/apps/gmm/map/b/a/q;

.field final synthetic c:F

.field final synthetic d:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iput p2, p0, Lcom/google/android/apps/gmm/map/n;->c:F

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/n;->d:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b;)V
    .locals 6

    .prologue
    .line 255
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, p0, Lcom/google/android/apps/gmm/map/n;->c:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n;->d:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n;->d:Landroid/graphics/Rect;

    .line 258
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n;->d:Landroid/graphics/Rect;

    .line 259
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v1

    .line 260
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->a()I

    move-result v2

    int-to-float v2, v2

    .line 261
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->b()I

    move-result v3

    int-to-float v3, v3

    .line 257
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 265
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iget v1, p0, Lcom/google/android/apps/gmm/map/n;->a:I

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/map/b;->a(Lcom/google/android/apps/gmm/map/f/a/a;I)V

    .line 266
    return-void

    .line 263
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    goto :goto_0
.end method
