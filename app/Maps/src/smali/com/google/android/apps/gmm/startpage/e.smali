.class public Lcom/google/android/apps/gmm/startpage/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/a/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/startpage/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/startpage/f;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/startpage/f;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/google/android/apps/gmm/startpage/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->b:Ljava/util/LinkedList;

    .line 90
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->c:Ljava/util/LinkedList;

    .line 93
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->d:Ljava/util/LinkedList;

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/e;->e:Z

    .line 358
    return-void
.end method

.method private static a(Ljava/util/List;J)Lcom/google/b/c/cv;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/startpage/f;",
            ">;J)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/high16 v10, 0x100000

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 287
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "getCacheItems() itemList.size()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 288
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v6

    .line 289
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 290
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/f;

    .line 291
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/f;->a:Lcom/google/o/h/a/iv;

    iget-object v1, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ir;

    iget v1, v1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_0

    move v1, v4

    :goto_1
    if-nez v1, :cond_1

    .line 292
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move v1, v5

    .line 291
    goto :goto_1

    .line 295
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/f;->a:Lcom/google/o/h/a/iv;

    iget-object v1, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ir;

    iget-object v1, v1, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/gp;

    .line 296
    iget v2, v1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v11, :cond_3

    move v2, v4

    :goto_2
    if-eqz v2, :cond_2

    .line 297
    iget-object v2, v1, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/a;

    iget v3, v2, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_4

    move v3, v4

    :goto_3
    if-eqz v3, :cond_8

    iget-object v3, v2, Lcom/google/o/h/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ph;->g()Lcom/google/o/h/a/ph;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/o/h/a/ph;

    iget v3, v3, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v9, 0x200

    if-ne v3, v9, :cond_5

    move v3, v4

    :goto_4
    if-eqz v3, :cond_7

    iget-object v2, v2, Lcom/google/o/h/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ph;->g()Lcom/google/o/h/a/ph;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/ph;

    iget v2, v2, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_6

    move v2, v4

    :goto_5
    if-eqz v2, :cond_7

    move v2, v4

    :goto_6
    if-nez v2, :cond_11

    .line 298
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    move v2, v5

    .line 296
    goto :goto_2

    :cond_4
    move v3, v5

    .line 297
    goto :goto_3

    :cond_5
    move v3, v5

    goto :goto_4

    :cond_6
    move v2, v5

    goto :goto_5

    :cond_7
    move v2, v5

    goto :goto_6

    :cond_8
    iget v3, v2, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v3, v10

    if-ne v3, v10, :cond_a

    move v3, v4

    :goto_7
    if-eqz v3, :cond_10

    iget-object v3, v2, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/o/h/a/se;

    iget-object v3, v3, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/je;

    iget v3, v3, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v9, 0x10

    if-ne v3, v9, :cond_b

    move v3, v4

    :goto_8
    if-eqz v3, :cond_f

    iget-object v3, v2, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/o/h/a/se;

    iget-object v3, v3, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/je;

    iget v3, v3, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_c

    move v3, v4

    :goto_9
    if-nez v3, :cond_9

    iget-object v3, v2, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/o/h/a/se;

    iget-object v3, v3, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/je;

    iget v3, v3, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v11, :cond_d

    move v3, v4

    :goto_a
    if-nez v3, :cond_9

    iget-object v2, v2, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/se;

    iget-object v2, v2, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/je;

    iget v2, v2, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v4

    :goto_b
    if-eqz v2, :cond_f

    :cond_9
    move v2, v4

    goto/16 :goto_6

    :cond_a
    move v3, v5

    goto/16 :goto_7

    :cond_b
    move v3, v5

    goto :goto_8

    :cond_c
    move v3, v5

    goto :goto_9

    :cond_d
    move v3, v5

    goto :goto_a

    :cond_e
    move v2, v5

    goto :goto_b

    :cond_f
    move v2, v5

    goto/16 :goto_6

    :cond_10
    move v2, v5

    goto/16 :goto_6

    .line 301
    :cond_11
    iget-wide v2, v0, Lcom/google/android/apps/gmm/startpage/f;->b:J

    cmp-long v2, v2, p1

    if-gtz v2, :cond_12

    .line 302
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 305
    :cond_12
    iget-object v1, v1, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/a;

    iget v2, v1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_13

    move v2, v4

    :goto_c
    if-eqz v2, :cond_14

    iget-object v1, v1, Lcom/google/o/h/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ph;->g()Lcom/google/o/h/a/ph;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ph;

    invoke-virtual {v1}, Lcom/google/o/h/a/ph;->d()Ljava/lang/String;

    move-result-object v1

    .line 306
    :goto_d
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 307
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_13
    move v2, v5

    .line 305
    goto :goto_c

    :cond_14
    iget v2, v1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v2, v10

    if-ne v2, v10, :cond_15

    move v2, v4

    :goto_e
    if-eqz v2, :cond_16

    iget-object v1, v1, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/se;

    iget-object v1, v1, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    invoke-virtual {v1}, Lcom/google/maps/g/a/je;->h()Ljava/lang/String;

    move-result-object v1

    goto :goto_d

    :cond_15
    move v2, v5

    goto :goto_e

    :cond_16
    const/4 v1, 0x0

    goto :goto_d

    .line 310
    :cond_17
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f;->a:Lcom/google/o/h/a/iv;

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 311
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 313
    :cond_18
    invoke-virtual {v6}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    .line 314
    sget-object v1, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "getCacheItems() cachedItems.size()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 315
    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/base/g/c;Z)Lcom/google/o/h/a/iv;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 179
    invoke-static {}, Lcom/google/o/h/a/ph;->newBuilder()Lcom/google/o/h/a/pj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/o/h/a/pj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/o/h/a/pj;->a:I

    iput-object v1, v0, Lcom/google/o/h/a/pj;->b:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v0, Lcom/google/o/h/a/pj;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v0, Lcom/google/o/h/a/pj;->a:I

    iput-object v1, v0, Lcom/google/o/h/a/pj;->f:Ljava/lang/Object;

    iget v1, v0, Lcom/google/o/h/a/pj;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Lcom/google/o/h/a/pj;->a:I

    iput-boolean v4, v0, Lcom/google/o/h/a/pj;->d:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/q;->c()Lcom/google/d/a/a/hp;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v2, v0, Lcom/google/o/h/a/pj;->e:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/pj;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, v0, Lcom/google/o/h/a/pj;->a:I

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v1, v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a()Lcom/google/d/a/a/ds;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget-object v2, v0, Lcom/google/o/h/a/pj;->c:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/pj;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/o/h/a/pj;->a:I

    :cond_5
    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v1

    iget-object v2, v1, Lcom/google/o/h/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/pj;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/o/h/a/c;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    .line 180
    if-eqz p1, :cond_7

    sget-object v1, Lcom/google/android/apps/gmm/cardui/e/d;->e:Lcom/google/o/h/a/hv;

    .line 182
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v3}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/apps/gmm/cardui/f/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/o/h/a/hv;Lcom/google/o/h/a/a;)Lcom/google/o/h/a/iv;

    move-result-object v0

    return-object v0

    .line 180
    :cond_7
    sget-object v1, Lcom/google/android/apps/gmm/cardui/e/d;->d:Lcom/google/o/h/a/hv;

    goto :goto_0
.end method

.method private a(Lcom/google/o/h/a/iv;J)V
    .locals 4

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "item added to search item:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/e;->e:Z

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->b:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/f;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/gmm/startpage/f;-><init>(Lcom/google/o/h/a/iv;J)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 112
    :cond_0
    return-void
.end method

.method private static b(Lcom/google/android/apps/gmm/base/g/c;Z)Lcom/google/o/h/a/iv;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 189
    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/a/jg;->a:I

    iput-object v1, v0, Lcom/google/maps/g/a/jg;->b:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/maps/g/a/jg;->a:I

    iput-object v1, v0, Lcom/google/maps/g/a/jg;->d:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v2, v0, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/a/jg;->a:I

    iput-object v1, v0, Lcom/google/maps/g/a/jg;->c:Ljava/lang/Object;

    :cond_4
    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v1

    invoke-static {}, Lcom/google/o/h/a/se;->newBuilder()Lcom/google/o/h/a/sg;

    move-result-object v2

    iget-object v3, v2, Lcom/google/o/h/a/sg;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/a/jg;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/o/h/a/sg;->a:I

    iget-object v0, v1, Lcom/google/o/h/a/c;->e:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/o/h/a/sg;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/c;->a:I

    const/high16 v2, 0x100000

    or-int/2addr v0, v2

    iput v0, v1, Lcom/google/o/h/a/c;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    .line 190
    if-eqz p1, :cond_6

    sget-object v1, Lcom/google/android/apps/gmm/cardui/e/d;->e:Lcom/google/o/h/a/hv;

    .line 192
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v3}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/apps/gmm/cardui/f/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/o/h/a/hv;Lcom/google/o/h/a/a;)Lcom/google/o/h/a/iv;

    move-result-object v0

    return-object v0

    .line 190
    :cond_6
    sget-object v1, Lcom/google/android/apps/gmm/cardui/e/d;->d:Lcom/google/o/h/a/hv;

    goto :goto_0
.end method

.method private b(Lcom/google/o/h/a/iv;J)V
    .locals 4

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "item added to directions item:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/e;->e:Z

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->c:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/f;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/gmm/startpage/f;-><init>(Lcom/google/o/h/a/iv;J)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->b:Ljava/util/LinkedList;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/gmm/startpage/e;->a(Ljava/util/List;J)Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/e;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/base/g/c;J)V
    .locals 2

    .prologue
    .line 214
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/startpage/e;->a(Lcom/google/android/apps/gmm/base/g/c;Z)Lcom/google/o/h/a/iv;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/startpage/e;->a(Lcom/google/o/h/a/iv;J)V

    .line 218
    :cond_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/startpage/e;->b(Lcom/google/android/apps/gmm/base/g/c;Z)Lcom/google/o/h/a/iv;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_1

    .line 220
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/startpage/e;->b(Lcom/google/o/h/a/iv;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :cond_1
    monitor-exit p0

    return-void

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/Integer;J)V
    .locals 5
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v0

    invoke-static {}, Lcom/google/o/h/a/ph;->newBuilder()Lcom/google/o/h/a/pj;

    move-result-object v1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v2, v1, Lcom/google/o/h/a/pj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/o/h/a/pj;->a:I

    iput-object p1, v1, Lcom/google/o/h/a/pj;->b:Ljava/lang/Object;

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/o/h/a/pj;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v1, Lcom/google/o/h/a/pj;->a:I

    iput-object p1, v1, Lcom/google/o/h/a/pj;->f:Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/o/h/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/o/h/a/pj;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/c;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/o/h/a/c;->a:I

    invoke-virtual {v0}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/cardui/e/d;->d:Lcom/google/o/h/a/hv;

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/apps/gmm/cardui/f/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/o/h/a/hv;Lcom/google/o/h/a/a;)Lcom/google/o/h/a/iv;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_2

    .line 235
    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/apps/gmm/startpage/e;->a(Lcom/google/o/h/a/iv;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(J)Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->c:Ljava/util/LinkedList;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/gmm/startpage/e;->a(Ljava/util/List;J)Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 103
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/e;->e:Z

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/base/g/c;J)V
    .locals 2

    .prologue
    .line 246
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/startpage/e;->b(Lcom/google/android/apps/gmm/base/g/c;Z)Lcom/google/o/h/a/iv;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_0

    .line 248
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/startpage/e;->b(Lcom/google/o/h/a/iv;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :cond_0
    monitor-exit p0

    return-void

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(J)Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->d:Ljava/util/LinkedList;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/gmm/startpage/e;->a(Ljava/util/List;J)Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()V
    .locals 1

    .prologue
    .line 350
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/e;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    monitor-exit p0

    return-void

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/gmm/base/g/c;J)V
    .locals 4

    .prologue
    .line 261
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/startpage/e;->a(Lcom/google/android/apps/gmm/base/g/c;Z)Lcom/google/o/h/a/iv;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_0

    .line 263
    sget-object v1, Lcom/google/android/apps/gmm/startpage/e;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "item added to search recent navigation item:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/startpage/e;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/e;->d:Ljava/util/LinkedList;

    new-instance v2, Lcom/google/android/apps/gmm/startpage/f;

    invoke-direct {v2, v0, p2, p3}, Lcom/google/android/apps/gmm/startpage/f;-><init>(Lcom/google/o/h/a/iv;J)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    :cond_0
    monitor-exit p0

    return-void

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
