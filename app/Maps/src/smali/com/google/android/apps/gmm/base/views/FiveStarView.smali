.class public Lcom/google/android/apps/gmm/base/views/FiveStarView;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:Landroid/graphics/drawable/Drawable;

.field d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

.field public e:Lcom/google/android/apps/gmm/base/views/s;

.field private final f:I

.field private g:F

.field private h:F

.field private i:Z

.field private j:Z

.field private k:F

.field private l:F

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 96
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 51
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    .line 57
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    .line 63
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    .line 78
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->f:I

    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a(Landroid/content/Context;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    .line 57
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    .line 63
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    .line 78
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/n;->q:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 105
    sget v1, Lcom/google/android/apps/gmm/n;->v:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->c:Landroid/graphics/drawable/Drawable;

    .line 106
    sget v1, Lcom/google/android/apps/gmm/n;->s:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->b:Landroid/graphics/drawable/Drawable;

    .line 107
    sget v1, Lcom/google/android/apps/gmm/n;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a:Landroid/graphics/drawable/Drawable;

    .line 108
    sget v1, Lcom/google/android/apps/gmm/n;->u:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    .line 109
    sget v1, Lcom/google/android/apps/gmm/n;->t:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->f:I

    .line 111
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 113
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a(Landroid/content/Context;)V

    .line 114
    return-void
.end method

.method private a(F)V
    .locals 0

    .prologue
    .line 271
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a()V

    .line 273
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v1, 0x0

    const/4 v3, -0x2

    .line 117
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->m:I

    .line 120
    new-array v0, v5, [Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    .line 121
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move v0, v1

    .line 123
    :goto_0
    if-ge v0, v5, :cond_0

    .line 124
    new-instance v3, Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    invoke-direct {v3, p1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;-><init>(Landroid/content/Context;)V

    .line 126
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aput-object v3, v4, v0

    .line 127
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    add-int/lit8 v4, v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setTag(Ljava/lang/Object;)V

    .line 130
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->addView(Landroid/view/View;)V

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v0, v0, v1

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setIsInteractive(Z)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a()V

    .line 137
    return-void
.end method

.method private a(Z)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v12, 0x1

    const/4 v3, 0x0

    .line 255
    move v2, v3

    :goto_0
    const/4 v0, 0x5

    if-ge v2, v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v0, v2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/l;->p:I

    new-array v6, v12, [Ljava/lang/Object;

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    add-int/lit8 v8, v2, 0x1

    sget v9, Lcom/google/android/apps/gmm/j;->b:I

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v3

    invoke-virtual {v7, v9, v8, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 256
    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 255
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 256
    goto :goto_1

    .line 262
    :cond_1
    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    return-void

    .line 263
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v1, ""

    goto :goto_2

    :cond_3
    const/high16 v0, 0x41200000    # 10.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_4

    float-to-int v0, v2

    :goto_3
    sget v4, Lcom/google/android/apps/gmm/j;->a:I

    new-array v5, v12, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v3

    invoke-virtual {v1, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    const/4 v0, 0x3

    goto :goto_3
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 191
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v2, v1, v0

    int-to-float v1, v0

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    const/high16 v4, 0x3f400000    # 0.75f

    sub-float/2addr v3, v4

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_0
    int-to-float v1, v0

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    const/high16 v4, 0x3e800000    # 0.25f

    sub-float/2addr v3, v4

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 195
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->invalidate()V

    .line 196
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 326
    int-to-float v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setValue(F)V

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->e:Lcom/google/android/apps/gmm/base/views/s;

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->e:Lcom/google/android/apps/gmm/base/views/s;

    int-to-float v0, v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/s;->a(F)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 378
    if-nez p1, :cond_1

    .line 400
    :cond_0
    return-void

    .line 382
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v3

    .line 386
    if-eqz v3, :cond_2

    sub-int v0, p4, p2

    :goto_0
    move v2, v1

    .line 387
    :goto_1
    const/4 v1, 0x5

    if-ge v2, v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->getMeasuredWidth()I

    move-result v1

    .line 389
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->getMeasuredHeight()I

    move-result v4

    .line 390
    sub-int v5, p5, p3

    sub-int/2addr v5, v4

    div-int/lit8 v5, v5, 0x2

    .line 391
    if-eqz v3, :cond_3

    .line 392
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v6, v6, v2

    sub-int v7, v0, v1

    add-int/2addr v4, v5

    invoke-virtual {v6, v7, v5, v0, v4}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->layout(IIII)V

    .line 393
    sub-int/2addr v0, v1

    .line 398
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v1, v2

    if-eqz v3, :cond_4

    const/high16 v1, -0x40800000    # -1.0f

    :goto_3
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setScaleX(F)V

    .line 387
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 386
    goto :goto_0

    .line 395
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v6, v6, v2

    add-int v7, v0, v1

    add-int/2addr v4, v5

    invoke-virtual {v6, v0, v5, v7, v4}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->layout(IIII)V

    .line 396
    add-int/2addr v0, v1

    goto :goto_2

    .line 398
    :cond_4
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x5

    const/4 v2, 0x0

    .line 338
    move v0, v2

    move v3, v2

    move v1, v2

    .line 340
    :goto_0
    if-ge v0, v5, :cond_0

    .line 342
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4, v2, v2}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->measure(II)V

    .line 343
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 340
    add-int/lit8 v0, v0, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    .line 349
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->f:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_4

    .line 350
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->f:I

    shl-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 354
    :goto_1
    invoke-static {v0, p1, v2}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->resolveSizeAndState(III)I

    move-result v0

    .line 355
    invoke-static {v3, p2, v2}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->resolveSizeAndState(III)I

    move-result v3

    .line 356
    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setMeasuredDimension(II)V

    .line 360
    const v3, 0xffffff

    and-int/2addr v0, v3

    .line 361
    sub-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x4

    .line 364
    :goto_2
    if-ge v2, v5, :cond_2

    .line 366
    if-eqz v2, :cond_1

    const/4 v0, 0x4

    if-ne v2, v0, :cond_3

    .line 367
    :cond_1
    div-int/lit8 v0, v1, 0x2

    .line 369
    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v0, v3

    .line 370
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->getMeasuredHeight()I

    move-result v3

    .line 371
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v4, v2

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 372
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 371
    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->measure(II)V

    .line 364
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 374
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 277
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    if-eqz v2, :cond_1

    .line 278
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->k:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->l:F

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a()V

    :cond_0
    :goto_0
    move v0, v1

    .line 280
    :cond_1
    return v0

    .line 278
    :cond_2
    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a()V

    goto :goto_0

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    if-eqz v3, :cond_4

    if-ne v2, v1, :cond_4

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->performClick()Z

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v4, v2, v5

    if-ltz v4, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-gtz v4, :cond_5

    cmpg-float v4, v3, v5

    if-ltz v4, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-gtz v4, :cond_5

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->k:F

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->m:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_5

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->l:F

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->m:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    :cond_5
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->h:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a()V

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    goto :goto_0
.end method

.method public final setIsInteractive(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 169
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    .line 170
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setFocusable(Z)V

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a(Z)V

    move v3, v1

    .line 174
    :goto_0
    const/4 v0, 0x5

    if-ge v3, v0, :cond_3

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v0, v3

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v4, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->a:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v0, v3

    if-eqz p1, :cond_1

    move-object v0, p0

    :goto_2
    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v4, v0, v3

    if-eqz p1, :cond_2

    move-object v0, p0

    :goto_3
    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setClickable(Z)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->d:[Lcom/google/android/apps/gmm/base/views/PresentableImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/views/PresentableImageView;->setFocusable(Z)V

    .line 174
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 175
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 178
    goto :goto_2

    :cond_2
    move-object v0, v2

    .line 179
    goto :goto_3

    .line 185
    :cond_3
    return-void
.end method

.method public final setValue(F)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 231
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_2

    const/high16 v0, 0x40a00000    # 5.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    .line 237
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of stars given out of range of widget."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_3
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->g:F

    .line 241
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->i:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a(Z)V

    .line 244
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FiveStarView;->j:Z

    if-nez v0, :cond_0

    .line 245
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a(F)V

    goto :goto_0
.end method
