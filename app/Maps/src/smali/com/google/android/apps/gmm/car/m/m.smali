.class public Lcom/google/android/apps/gmm/car/m/m;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/car/m/l;

.field private final b:Lcom/google/android/apps/gmm/car/m/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/m/l;Lcom/google/android/apps/gmm/car/m/f;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/m/l;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    .line 17
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    .line 18
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/car/m/e;->a:Lcom/google/android/apps/gmm/car/m/e;

    if-eq v0, v1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 47
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 65
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/l;->d()Lcom/google/android/apps/gmm/car/m/h;

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    if-eqz v0, :cond_2

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/m;->b:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 71
    :cond_2
    return-void
.end method
