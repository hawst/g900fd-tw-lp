.class public Lcom/google/android/apps/gmm/place/h/b/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/h/b/d;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/h/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/lm;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 35
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->L:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/lq;->d()Lcom/google/maps/g/lq;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/lq;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/google/maps/g/lq;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, v1, Lcom/google/maps/g/lq;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/lm;->d()Lcom/google/maps/g/lm;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/lm;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/gmm/place/h/b/a;->c:Ljava/util/List;

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/h/b/a;->b:Landroid/content/Context;

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 41
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    sget-object v3, Lcom/google/b/f/t;->dB:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 42
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 43
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->d:Lcom/google/android/apps/gmm/z/b/l;

    move v1, v2

    .line 45
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const/4 v0, 0x3

    if-ge v1, v0, :cond_1

    .line 46
    new-instance v2, Lcom/google/android/apps/gmm/place/h/b/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->c:Ljava/util/List;

    .line 47
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/lm;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/h/b/a;->d:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct {v2, p1, v0, v3}, Lcom/google/android/apps/gmm/place/h/b/b;-><init>(Landroid/content/Context;Lcom/google/maps/g/lm;Lcom/google/android/apps/gmm/z/b/l;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 50
    :cond_1
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/h/b/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 69
    const/4 v0, 0x3

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 70
    new-instance v2, Lcom/google/android/apps/gmm/place/h/b/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/h/b/a;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->c:Ljava/util/List;

    .line 71
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/lm;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/h/b/a;->d:Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/apps/gmm/place/h/b/b;-><init>(Landroid/content/Context;Lcom/google/maps/g/lm;Lcom/google/android/apps/gmm/z/b/l;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 75
    :cond_0
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/a;->d:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
