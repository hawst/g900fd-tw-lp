.class public Lcom/google/android/apps/gmm/prefetchcache/b/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetchcache/b/d;


# instance fields
.field public a:I

.field public final b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field private final c:Lcom/google/android/apps/gmm/prefetchcache/b/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/prefetchcache/b/g;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->a:I

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 35
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->c:Lcom/google/android/apps/gmm/prefetchcache/b/g;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->a:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->jg:I

    .line 61
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 60
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->mD:I

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->a:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    sget v0, Lcom/google/android/apps/gmm/f;->ci:I

    .line 76
    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0

    .line 74
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->cw:I

    goto :goto_0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->a:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/e;->c:Lcom/google/android/apps/gmm/prefetchcache/b/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/b/g;->c()V

    .line 69
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
