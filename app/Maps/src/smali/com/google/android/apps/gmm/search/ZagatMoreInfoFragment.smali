.class public Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;
.super Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;
.source "PG"


# instance fields
.field private c:Landroid/view/View;

.field private d:Lcom/google/android/apps/gmm/place/i/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;-><init>()V

    .line 34
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "placemark"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->setArguments(Landroid/os/Bundle;)V

    .line 35
    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/base/g/c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 78
    sget v0, Lcom/google/android/apps/gmm/l;->gD:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->pt:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/b/f/t;->fJ:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    new-instance v1, Lcom/google/android/apps/gmm/place/cp;

    .line 42
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->e:Lcom/google/android/apps/gmm/x/o;

    .line 43
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/place/cp;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->d:Lcom/google/android/apps/gmm/place/i/m;

    .line 44
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 51
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/cg;

    .line 52
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->c:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->d:Lcom/google/android/apps/gmm/place/i/m;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->c:Landroid/view/View;

    return-object v0
.end method
