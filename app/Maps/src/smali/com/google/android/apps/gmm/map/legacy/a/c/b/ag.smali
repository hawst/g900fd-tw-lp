.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;
.super Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field private C:Z

.field private volatile D:Z

.field private volatile E:Lcom/google/android/apps/gmm/v/by;

.field public volatile b:Z

.field private final c:Lcom/google/android/apps/gmm/map/o/b/i;

.field private final d:Lcom/google/android/apps/gmm/map/o/b/i;

.field private e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

.field private f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

.field private g:Z

.field private h:Lcom/google/android/apps/gmm/map/internal/c/a;

.field private z:Lcom/google/android/apps/gmm/map/internal/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->d:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;-><init>()V

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->A:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 173
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/f/o;FFLcom/google/android/apps/gmm/map/b/a/y;[FLcom/google/android/apps/gmm/map/b/a/ay;)D
    .locals 8

    .prologue
    .line 454
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    .line 455
    const v1, -0x472e48e9    # -1.0E-4f

    cmpg-float v1, v1, v0

    if-gez v1, :cond_1

    const v1, 0x38d1b717    # 1.0E-4f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    .line 458
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 463
    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    const-wide v2, 0x4056800000000000L    # 90.0

    add-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    rem-double/2addr v0, v2

    const-wide v2, 0x4056800000000000L    # 90.0

    sub-double/2addr v0, v2

    .line 465
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 466
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 467
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 526
    :goto_0
    return-wide v0

    .line 483
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 487
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    neg-float v1, v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 488
    const/high16 v1, 0x42c80000    # 100.0f

    .line 489
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v4

    mul-float/2addr v1, v4

    .line 492
    float-to-double v4, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 493
    float-to-double v6, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v2, v6

    double-to-int v1, v2

    .line 494
    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v2, v4

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v0, v1

    iput v2, p4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 495
    invoke-virtual {p1, p4, p5}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v0

    if-nez v0, :cond_2

    .line 497
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 498
    const/4 v0, 0x0

    iput v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 499
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 502
    :cond_2
    const/4 v0, 0x0

    aget v0, p5, v0

    sub-float/2addr v0, p2

    iput v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 503
    const/4 v0, 0x1

    aget v0, p5, v0

    sub-float/2addr v0, p3

    iput v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 521
    iget v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 522
    iget v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    neg-float v0, v0

    iput v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 523
    iget v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v0, v0

    iput v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 525
    :cond_3
    invoke-virtual {p6}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 526
    iget v0, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v0, v0

    iget v2, p6, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    goto :goto_0
.end method

.method private a(F)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    mul-float/2addr v1, p1

    float-to-int v9, v1

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    mul-float/2addr v1, p1

    float-to-int v10, v1

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    div-int/lit8 v3, v9, 0x2

    int-to-float v6, v3

    div-int/lit8 v3, v10, 0x2

    int-to-float v7, v3

    move v3, v2

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    .line 241
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    mul-float/2addr v1, p1

    float-to-int v6, v1

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    mul-float/2addr v1, p1

    float-to-int v7, v1

    .line 246
    add-int v1, v9, v6

    div-int/lit8 v2, v1, 0x2

    .line 247
    add-int v1, v10, v7

    div-int/lit8 v1, v1, 0x2

    .line 248
    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ah;->a:[I

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->ordinal()I

    move-result v10

    aget v3, v3, v10

    packed-switch v3, :pswitch_data_0

    move v1, v0

    .line 285
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->d:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    if-ne v2, v3, :cond_1

    .line 289
    :cond_0
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ah;->b:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :cond_1
    move v9, v1

    .line 297
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    int-to-float v2, v9

    int-to-float v3, v0

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    .line 304
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->A:Lcom/google/android/apps/gmm/map/b/a/ay;

    int-to-float v2, v9

    div-float/2addr v2, p1

    int-to-float v0, v0

    div-float/2addr v0, p1

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 306
    :cond_2
    return-void

    :pswitch_0
    move v1, v0

    .line 250
    goto :goto_0

    :pswitch_1
    move v1, v2

    .line 254
    goto :goto_0

    .line 257
    :pswitch_2
    neg-int v1, v2

    .line 258
    goto :goto_0

    :pswitch_3
    move v11, v1

    move v1, v0

    move v0, v11

    .line 263
    goto :goto_0

    .line 265
    :pswitch_4
    neg-int v1, v1

    move v11, v1

    move v1, v0

    move v0, v11

    .line 267
    goto :goto_0

    :pswitch_5
    move v0, v1

    move v1, v2

    .line 271
    goto :goto_0

    .line 273
    :pswitch_6
    neg-int v0, v2

    move v11, v1

    move v1, v0

    move v0, v11

    .line 275
    goto :goto_0

    .line 278
    :pswitch_7
    neg-int v0, v1

    move v1, v2

    .line 279
    goto :goto_0

    .line 281
    :pswitch_8
    neg-int v2, v2

    .line 282
    neg-int v0, v1

    move v1, v2

    goto :goto_0

    .line 292
    :pswitch_9
    sub-int v1, v6, v9

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0xa

    move v9, v1

    .line 293
    goto :goto_1

    .line 296
    :pswitch_a
    sub-int v1, v9, v6

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0xa

    move v9, v1

    goto :goto_1

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 289
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private a(FLcom/google/android/apps/gmm/map/b/a/ay;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->z:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 313
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/e;->a:I

    shr-int/lit8 v0, v0, 0x4

    and-int/lit8 v0, v0, 0xf

    and-int/lit8 v0, v0, 0x3

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    mul-float/2addr v0, p1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 314
    iget v4, v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    mul-float/2addr v0, v4

    div-float/2addr v0, v5

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->z:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 315
    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/e;->a:I

    shr-int/lit8 v4, v4, 0x4

    and-int/lit8 v4, v4, 0xf

    shr-int/lit8 v4, v4, 0x2

    and-int/lit8 v4, v4, 0x3

    packed-switch v4, :pswitch_data_1

    :goto_1
    :pswitch_0
    mul-float/2addr v1, p1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 316
    iget v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    mul-float/2addr v1, v2

    div-float/2addr v1, v5

    .line 312
    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 317
    return-void

    :pswitch_1
    move v0, v1

    .line 313
    goto :goto_0

    :pswitch_2
    move v0, v2

    goto :goto_0

    :pswitch_3
    move v0, v3

    goto :goto_0

    :pswitch_4
    move v1, v2

    .line 315
    goto :goto_1

    :pswitch_5
    move v1, v3

    goto :goto_1

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 315
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;ILcom/google/android/apps/gmm/map/internal/c/a;FLcom/google/android/apps/gmm/map/legacy/a/c/b/s;Lcom/google/android/apps/gmm/map/internal/c/e;Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;ZLcom/google/android/apps/gmm/map/t/l;)V
    .locals 10

    .prologue
    .line 45
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->g()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/m;->i()I

    move-result v7

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/o/af;->a(Lcom/google/android/apps/gmm/map/internal/c/m;)Lcom/google/android/apps/gmm/map/o/af;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move v6, p5

    move-object/from16 v9, p11

    invoke-super/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(Lcom/google/android/apps/gmm/map/internal/c/m;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/be;FILcom/google/android/apps/gmm/map/o/af;Lcom/google/android/apps/gmm/map/t/l;)V

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->z:Lcom/google/android/apps/gmm/map/internal/c/e;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->C:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->D:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->E:Lcom/google/android/apps/gmm/v/by;

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 410
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-ne v2, p1, :cond_0

    .line 430
    :goto_0
    return v0

    .line 414
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 416
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    move v0, v1

    goto :goto_0

    .line 420
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    move v0, v1

    goto :goto_0

    .line 424
    :cond_2
    const/high16 v1, 0x3f800000    # 1.0f

    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(F)V

    .line 425
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->g:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 427
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 429
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->u:Lcom/google/android/apps/gmm/map/o/ap;

    goto :goto_0

    .line 427
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 19

    .prologue
    .line 531
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 533
    const/high16 v2, 0x3f800000    # 1.0f

    .line 534
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v4

    if-nez v4, :cond_0

    .line 535
    const/4 v2, 0x0

    .line 573
    :goto_0
    return v2

    .line 537
    :cond_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    .line 538
    const/4 v4, 0x0

    aget v4, v7, v4

    const/4 v5, 0x1

    aget v5, v7, v5

    move-object/from16 v0, v18

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v18

    iput v5, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 539
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 541
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v5}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 543
    :try_start_0
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    iget-boolean v5, v5, Lcom/google/android/apps/gmm/map/f/j;->g:Z

    if-nez v5, :cond_1

    .line 544
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    .line 545
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(F)V

    :cond_1
    move/from16 v17, v2

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_4

    .line 548
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)V

    .line 549
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/i;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 550
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/map/o/b/i;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Z)V

    .line 552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_2

    .line 553
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/map/o/b/i;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 573
    const/4 v2, 0x1

    goto :goto_0

    .line 547
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 556
    :cond_4
    :try_start_1
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 557
    move-object/from16 v0, v18

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/au;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(Lcom/google/android/apps/gmm/map/f/o;FFLcom/google/android/apps/gmm/map/b/a/y;[FLcom/google/android/apps/gmm/map/b/a/ay;)D

    move-result-wide v12

    .line 559
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    move-object/from16 v0, v18

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v18

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/i;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v3, v4

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v14, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 560
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/i;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v3, v4

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v15, v2

    .line 559
    const/16 v16, 0x1

    invoke-virtual/range {v9 .. v16}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    .line 561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_2

    .line 562
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/au;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 563
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->A:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v3, v8, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->c(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    .line 564
    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float v3, v3, v17

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float v3, v3, v17

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 565
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 566
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    iget v10, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v11, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 567
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/i;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v3, v4

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v14, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/i;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v3, v4

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v15, v2

    .line 566
    const/16 v16, 0x1

    invoke-virtual/range {v9 .. v16}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 571
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    throw v2
.end method

.method private l()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 438
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 439
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 440
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 442
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 443
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    if-ne v3, v4, :cond_2

    :cond_0
    move v3, v1

    .line 444
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 439
    goto :goto_0

    :cond_2
    move v3, v2

    .line 443
    goto :goto_1

    :cond_3
    move v1, v2

    .line 444
    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 13

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 389
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/t/ac;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 396
    const-string v2, ""

    .line 397
    const-string v1, ""

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 399
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->p:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 400
    if-eqz v0, :cond_6

    .line 401
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/cu;->a:Ljava/lang/String;

    .line 402
    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cu;->b:Ljava/lang/String;

    :goto_0
    move-object v11, v0

    move-object v12, v1

    .line 406
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    move-object v10, v0

    check-cast v10, Lcom/google/android/apps/gmm/map/internal/c/an;

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-eqz v0, :cond_1

    iget-object v1, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    new-instance v0, Lcom/google/android/apps/gmm/map/g/e;

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/g/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/cf;)V

    :goto_2
    new-instance v1, Lcom/google/android/apps/gmm/map/j/t;

    invoke-direct {v1, v0, v12, v11}, Lcom/google/android/apps/gmm/map/j/t;-><init>(Lcom/google/android/apps/gmm/map/g/b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 407
    return-void

    .line 393
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0

    .line 403
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cu;->c:Ljava/lang/String;

    goto :goto_0

    .line 406
    :cond_1
    iget-object v0, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_4

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/g/c;->a(Lcom/google/android/apps/gmm/map/internal/c/an;)Lcom/google/android/apps/gmm/map/g/c;

    move-result-object v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/map/g/a;

    iget-object v1, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->v:Ljava/lang/Integer;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    iget-object v7, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/internal/c/an;->f()Z

    move-result v8

    iget-boolean v9, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->w:Z

    iget v10, v10, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v10, v10, 0x100

    if-eqz v10, :cond_5

    const/4 v10, 0x1

    :goto_4
    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/g/a;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/c;Lcom/google/android/apps/gmm/map/internal/c/bb;Ljava/lang/Integer;Lcom/google/android/apps/gmm/map/indoor/d/g;Lcom/google/android/apps/gmm/map/b/a/j;ZZZ)V

    goto :goto_2

    :cond_5
    const/4 v10, 0x0

    goto :goto_4

    :cond_6
    move-object v11, v1

    move-object v12, v2

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 586
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->t:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 603
    :goto_0
    return-void

    .line 589
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 593
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/t/b;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 596
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/t/b;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 600
    :cond_2
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->g:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 321
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    .line 322
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/bc;Z)Z
    .locals 22

    .prologue
    .line 609
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->n:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 610
    const/4 v2, 0x1

    .line 694
    :goto_0
    return v2

    .line 613
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->v:F

    move/from16 v19, v0

    .line 614
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 616
    const/4 v2, 0x1

    goto :goto_0

    .line 618
    :cond_1
    const/4 v2, 0x0

    aget v4, v7, v2

    .line 619
    const/4 v2, 0x1

    aget v5, v7, v2

    .line 621
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v2

    if-nez v2, :cond_2

    .line 628
    const/4 v2, 0x0

    goto :goto_0

    .line 632
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_3

    .line 633
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    const/4 v2, 0x0

    goto :goto_0

    .line 636
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v16

    .line 637
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->D:Z

    if-eqz v2, :cond_4

    .line 638
    const v2, 0x3f99999a    # 1.2f

    mul-float v16, v16, v2

    .line 641
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->h:Lcom/google/android/apps/gmm/map/internal/c/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_9

    .line 642
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 643
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)V

    .line 644
    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v4, v3

    .line 645
    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v5, v2

    .line 646
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->D:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->E:Lcom/google/android/apps/gmm/v/by;

    if-eqz v2, :cond_5

    .line 647
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->E:Lcom/google/android/apps/gmm/v/by;

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f800000    # 1.0f

    sget-object v8, Lcom/google/android/apps/gmm/map/t/l;->x:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v2, p3

    move-object/from16 v9, p2

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/by;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 650
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->q:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v3, p3

    move/from16 v6, v16

    move/from16 v7, v19

    move-object/from16 v9, p2

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/bc;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 651
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    if-eqz v2, :cond_6

    .line 654
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->A:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 655
    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float v3, v3, v16

    add-float/2addr v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->A:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 656
    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float v3, v3, v16

    add-float/2addr v5, v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->q:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v3, p3

    move/from16 v6, v16

    move/from16 v7, v19

    move-object/from16 v9, p2

    .line 654
    invoke-virtual/range {v2 .. v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/bc;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 685
    :cond_6
    :goto_2
    if-eqz p4, :cond_7

    .line 686
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/map/o/b/b;Lcom/google/android/apps/gmm/map/t/l;)V

    .line 687
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_7

    .line 688
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/map/o/b/b;Lcom/google/android/apps/gmm/map/t/l;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 692
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 694
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 641
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 663
    :cond_9
    :try_start_2
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/o/au;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 664
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/au;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v18, v0

    .line 665
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/au;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    .line 666
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(Lcom/google/android/apps/gmm/map/f/o;FFLcom/google/android/apps/gmm/map/b/a/y;[FLcom/google/android/apps/gmm/map/b/a/ay;)D

    move-result-wide v14

    .line 667
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->q:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v20, v0

    move-object/from16 v11, p3

    move v12, v4

    move v13, v5

    move-object/from16 v17, v8

    move-object/from16 v21, p2

    invoke-virtual/range {v10 .. v21}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/bc;FFDFLcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    if-eqz v2, :cond_6

    .line 670
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/au;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 671
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->A:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v3, v8, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->c(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    .line 672
    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float v3, v3, v16

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float v3, v3, v16

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 673
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float v12, v4, v3

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float v13, v5, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->q:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v20, v0

    move-object/from16 v11, p3

    move-object/from16 v17, v8

    move-object/from16 v21, p2

    invoke-virtual/range {v10 .. v21}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/o/bc;FFDFLcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 692
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    throw v2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;Z)Z
    .locals 1

    .prologue
    .line 579
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z
    .locals 13

    .prologue
    .line 360
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->C:Z

    if-nez v0, :cond_0

    .line 361
    const/4 v0, 0x0

    .line 383
    :goto_0
    return v0

    .line 363
    :cond_0
    const/4 v1, 0x0

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 369
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/t/ac;)Z

    move-result v10

    .line 371
    if-nez v10, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    if-eqz v2, :cond_9

    .line 373
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v2, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/c;->c:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 375
    :goto_2
    if-nez p2, :cond_1

    if-eqz v0, :cond_9

    .line 376
    :cond_1
    iget-object v11, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 380
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 383
    if-nez v10, :cond_2

    if-eqz v0, :cond_8

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 373
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 376
    :cond_5
    const/4 v0, 0x0

    :try_start_1
    iget-object v1, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v9, v0

    :goto_4
    if-ge v9, v12, :cond_7

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/by;

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v2, v3

    iget-object v3, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v3, v0

    iget-wide v4, v11, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->g:D

    iget v0, v1, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v6, v1, Lcom/google/android/apps/gmm/v/by;->a:I

    sub-int/2addr v0, v6

    int-to-float v0, v0

    iget v6, v1, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v1, v1, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int v1, v6, v1

    int-to-float v7, v1

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t/ac;->g:Lcom/google/android/apps/gmm/map/o/b/i;

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v0, v6

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v7, v0

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/ac;->g:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t/ac;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/o/b/i;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_3

    :cond_6
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 380
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0

    .line 383
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_3
.end method

.method public final g()Lcom/google/android/apps/gmm/map/o/b/b;
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->c:Lcom/google/android/apps/gmm/map/o/b/i;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/map/o/b/b;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->d:Lcom/google/android/apps/gmm/map/o/b/i;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->C:Z

    return v0
.end method

.method protected final k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 227
    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->B:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 228
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->C:Z

    .line 229
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    .line 230
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->D:Z

    .line 231
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->E:Lcom/google/android/apps/gmm/v/by;

    .line 232
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k()V

    .line 233
    return-void
.end method
