.class Lcom/google/android/apps/gmm/aa/d/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/aa/d/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/aa/d/d;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/f;->a:Lcom/google/android/apps/gmm/aa/d/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 364
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/aa/d/f;->a:Lcom/google/android/apps/gmm/aa/d/d;

    iget-object v5, v4, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    monitor-enter v5

    :try_start_0
    iget-wide v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    iget-wide v6, v4, Lcom/google/android/apps/gmm/aa/d/d;->e:J

    cmp-long v2, v2, v6

    if-gtz v2, :cond_0

    iget v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    iget v3, v4, Lcom/google/android/apps/gmm/aa/d/d;->g:I

    if-le v2, v3, :cond_7

    :cond_0
    iget-wide v6, v4, Lcom/google/android/apps/gmm/aa/d/d;->f:J

    iget v8, v4, Lcom/google/android/apps/gmm/aa/d/d;->h:I

    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v2, v3

    new-array v9, v2, [Lcom/google/android/apps/gmm/aa/d/g;

    const/4 v2, 0x0

    :goto_0
    array-length v10, v3

    if-ge v2, v10, :cond_2

    new-instance v10, Lcom/google/android/apps/gmm/aa/d/g;

    aget-object v11, v3, v2

    invoke-direct {v10, v11}, Lcom/google/android/apps/gmm/aa/d/g;-><init>(Ljava/io/File;)V

    aput-object v10, v9, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v9}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v10

    array-length v12, v9

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v12, :cond_7

    aget-object v2, v9, v3

    iget-object v13, v2, Lcom/google/android/apps/gmm/aa/d/g;->a:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v2, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v2, "Can\'t delete file which is still in use [path=%s, usageCount=%s]."

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget-object v0, v4, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    move-object/from16 v16, v0

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v14, v15

    invoke-static {v2, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_4
    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_5
    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v14

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_b

    sget-object v2, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v2, "Deleted file [filename=%s, sizeInBytes=%s, numOfFiles=%s, targetSize=%s, targetCount=%s]."

    const/4 v14, 0x5

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v14, v15

    const/4 v13, 0x1

    iget-wide v0, v4, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v14, v13

    const/4 v13, 0x2

    iget v15, v4, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v14, v13

    const/4 v13, 0x3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v14, v13

    const/4 v13, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v14, v13

    invoke-static {v2, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :goto_3
    iget-wide v14, v4, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    cmp-long v2, v14, v6

    if-gtz v2, :cond_6

    iget v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    if-le v2, v8, :cond_7

    :cond_6
    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v14

    sub-long/2addr v14, v10

    iget-wide v0, v4, Lcom/google/android/apps/gmm/aa/d/d;->b:J

    move-wide/from16 v16, v0

    cmp-long v2, v14, v16

    if-lez v2, :cond_3

    iget-wide v0, v4, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    move-wide/from16 v16, v0

    iget-wide v0, v4, Lcom/google/android/apps/gmm/aa/d/d;->e:J

    move-wide/from16 v18, v0

    cmp-long v2, v16, v18

    if-gtz v2, :cond_3

    iget v2, v4, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    iget v13, v4, Lcom/google/android/apps/gmm/aa/d/d;->g:I

    if-gt v2, v13, :cond_3

    sget-object v2, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v2, "Timeout during trimming -- exiting [elapsedTime=%sms, size=%s, count=%s]."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x1

    iget-wide v8, v4, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x2

    iget v4, v4, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_7
    monitor-exit v5

    return-void

    :cond_8
    const/4 v2, -0x1

    neg-long v14, v14

    invoke-virtual {v4, v2, v14, v15}, Lcom/google/android/apps/gmm/aa/d/d;->a(IJ)V

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/aa/d/d;->b(Ljava/lang/String;)Ljava/util/Map$Entry;

    move-result-object v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    :goto_4
    if-eqz v2, :cond_9

    iget-object v14, v4, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    invoke-interface {v14, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    const/4 v2, 0x1

    goto/16 :goto_2

    :cond_a
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_4

    :cond_b
    sget-object v2, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v2, "Couldn\'t delete file [filename=%s]."

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v14, v15

    invoke-static {v2, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3
.end method
