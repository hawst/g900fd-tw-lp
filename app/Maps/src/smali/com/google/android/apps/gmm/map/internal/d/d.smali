.class public abstract Lcom/google/android/apps/gmm/map/internal/d/d;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/ai;
.implements Lcom/google/android/apps/gmm/map/util/a/m;


# static fields
.field private static final A:[J

.field private static final B:I

.field static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/os/Looper;

.field private c:Z

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/i;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/d/p;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/p;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/google/android/apps/gmm/map/internal/d/au;

.field public final i:Lcom/google/android/apps/gmm/map/internal/d/r;

.field public final j:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field public k:Landroid/os/Handler;

.field public l:Lcom/google/android/apps/gmm/map/internal/d/i;

.field public final m:Lcom/google/android/apps/gmm/shared/c/f;

.field final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/at;",
            ">;>;"
        }
    .end annotation
.end field

.field public final o:Lcom/google/android/apps/gmm/map/b/a/ai;

.field final p:Lcom/google/android/apps/gmm/map/internal/d/ag;

.field private final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/m;",
            ">;"
        }
    .end annotation
.end field

.field private volatile r:I

.field private s:Z

.field private u:Lcom/google/android/apps/gmm/map/internal/d/a/c;

.field private final v:Lcom/google/android/apps/gmm/map/e/a;

.field private final w:Lcom/google/android/apps/gmm/v/b/a;

.field private final x:Ljava/lang/StringBuilder;

.field private final y:Lcom/google/android/apps/gmm/map/internal/d/l;

.field private final z:Lcom/google/android/apps/gmm/map/internal/d/l;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 100
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    .line 251
    const/16 v0, 0x16

    new-array v6, v0, [J

    move v0, v1

    move-wide v2, v4

    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_0

    mul-int/lit8 v7, v0, 0x2

    shl-long v8, v10, v7

    add-long/2addr v2, v8

    sub-long v8, v2, v10

    aput-wide v8, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_0
    sput-object v6, Lcom/google/android/apps/gmm/map/internal/d/d;->A:[J

    const/16 v0, 0x15

    aget-wide v2, v6, v0

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    const/16 v0, 0x40

    :cond_1
    sput v0, Lcom/google/android/apps/gmm/map/internal/d/d;->B:I

    return-void

    :goto_1
    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    shr-long/2addr v2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/as;Lcom/google/android/apps/gmm/map/internal/d/b/s;ZILjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V
    .locals 10

    .prologue
    .line 291
    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v2

    invoke-direct {p0, p3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->a:Ljava/util/List;

    .line 146
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->d:Ljava/util/List;

    .line 166
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    .line 175
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    .line 188
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->s:Z

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    .line 200
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/internal/d/e;-><init>(Lcom/google/android/apps/gmm/map/internal/d/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->u:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->x:Ljava/lang/StringBuilder;

    .line 229
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/l;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/d/l;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    .line 236
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/l;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/d/l;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    .line 292
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 293
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/d/au;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/as;Lcom/google/android/apps/gmm/map/internal/d/b/s;ZLjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/ac;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 302
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 304
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    .line 305
    if-eqz p10, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->b(Z)Lcom/google/android/apps/gmm/map/internal/d/i;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    iput-object p0, v1, Lcom/google/android/apps/gmm/map/internal/d/i;->k:Lcom/google/android/apps/gmm/map/internal/d/d;

    .line 307
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/f;

    move/from16 v0, p7

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/f;-><init>(Lcom/google/android/apps/gmm/map/internal/d/d;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 316
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 317
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->q()Lcom/google/android/apps/gmm/map/e/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->v:Lcom/google/android/apps/gmm/map/e/a;

    .line 321
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->w:Lcom/google/android/apps/gmm/v/b/a;

    .line 329
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v1, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    .line 333
    return-void

    .line 305
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/k;Lcom/google/android/apps/gmm/shared/c/b;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 15
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1763
    const/4 v9, 0x0

    .line 1764
    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 1765
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->i:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1766
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->d:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->d:I

    .line 1768
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v3, v4}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1769
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    .line 1771
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->h:Lcom/google/b/a/an;

    .line 1770
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/b/a/an;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 1773
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/map/internal/d/i;->c(I)[B

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v2, 0x1

    move-object/from16 v0, p2

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->s:Z

    :cond_1
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v5, :cond_5

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_6

    move-object v10, v2

    :goto_1
    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v4, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/map/internal/d/i;->d(I)[B

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/map/internal/d/i;->b(I)I

    move-result v7

    move-object/from16 v6, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[B[BLcom/google/android/apps/gmm/shared/c/f;I)V

    :cond_2
    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-nez v4, :cond_3

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v12, v1}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v8

    :cond_3
    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v12, v1}, Lcom/google/android/apps/gmm/map/internal/d/i;->b(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    if-eqz v5, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/internal/c/bt;->a(Lcom/google/android/apps/gmm/shared/net/ad;)Z

    move-result v6

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v4, v7, :cond_7

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Z)V

    :goto_2
    move-object/from16 v4, p2

    :goto_3
    if-eqz v4, :cond_9

    if-eqz v2, :cond_4

    if-eqz v6, :cond_4

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v12, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->c:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;Z)I

    move-result v13

    invoke-interface {v2, v7, v12, v13}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;I)V

    :cond_4
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    move-object v10, v2

    goto :goto_1

    :cond_7
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/p;->b(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1808
    :catch_0
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 1809
    :goto_4
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v4, v5, :cond_8

    .line 1810
    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 1812
    :cond_8
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->d(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 1813
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1815
    :goto_5
    return-object v2

    .line 1773
    :cond_9
    if-nez v5, :cond_a

    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p2

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->s:Z

    :cond_a
    if-eqz v8, :cond_c

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v2, :cond_b

    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-nez v4, :cond_b

    invoke-interface {v2, v3, v8}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v2, :cond_c

    invoke-interface {v8}, Lcom/google/android/apps/gmm/map/internal/c/bo;->b()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    invoke-virtual {v2, v8}, Lcom/google/android/apps/gmm/map/internal/d/t;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/map/internal/c/bo;->b()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_e

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    :goto_6
    if-eqz v2, :cond_c

    invoke-interface {v8}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1774
    :cond_c
    if-nez v8, :cond_d

    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/d/p;->a()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1775
    :cond_d
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-eqz v2, :cond_f

    .line 1776
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->b:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->b:I

    .line 1780
    :goto_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v3, v4}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v8

    goto :goto_5

    .line 1773
    :cond_e
    const/4 v2, 0x0

    goto :goto_6

    .line 1778
    :cond_f
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->a:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->a:I

    goto :goto_7

    .line 1808
    :catch_1
    move-exception v2

    move-object v3, v2

    move-object v2, v8

    goto/16 :goto_4

    .line 1785
    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v2, :cond_1b

    .line 1786
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/i;->j:Z

    if-nez v2, :cond_1a

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v3, :cond_14

    const/4 v2, 0x0

    :goto_8
    if-eqz v2, :cond_15

    move-object v4, v2

    :goto_9
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v5, :cond_16

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v5, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v5

    if-eqz v5, :cond_16

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v3, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v3

    move-object v6, v3

    :goto_a
    if-eqz v6, :cond_19

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_11

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/bo;->b()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    :cond_11
    const/4 v3, 0x1

    :goto_b
    if-eqz v3, :cond_19

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v9, v5, v7}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v12

    iput-wide v12, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v9, v5, v7}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v12

    iput-wide v12, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v3, :cond_12

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-nez v3, :cond_12

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v3, v11, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    :cond_12
    if-eqz v2, :cond_13

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v3

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    move-object v3, v11

    invoke-interface/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;JJ)V

    :cond_13
    const/4 v2, 0x1

    :goto_c
    if-eqz v2, :cond_1a

    .line 1787
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->c:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->c:I

    .line 1788
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v3, v4}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v8

    goto/16 :goto_5

    .line 1786
    :cond_14
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto/16 :goto_8

    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    move-object v4, v2

    goto/16 :goto_9

    :cond_16
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v5

    if-eqz v5, :cond_17

    invoke-interface {v5, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v6

    if-eqz v6, :cond_17

    invoke-interface {v5, v11}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v3

    :cond_17
    move-object v6, v3

    goto/16 :goto_a

    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_b

    :cond_19
    const/4 v2, 0x0

    goto :goto_c

    .line 1800
    :cond_1a
    move-object/from16 v0, p2

    invoke-direct {p0, v0, v11}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 1801
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->e:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->e:I

    move-object v2, v8

    goto/16 :goto_5

    .line 1804
    :cond_1b
    move-object/from16 v0, p2

    invoke-direct {p0, v0, v11}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 1805
    move-object/from16 v0, p3

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->e:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/k;->e:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v2, v8

    .line 1814
    goto/16 :goto_5
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/au;
    .locals 1

    .prologue
    .line 2070
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2073
    :goto_0
    if-eqz v0, :cond_1

    .line 2076
    :goto_1
    return-object v0

    .line 2070
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 2072
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_0

    .line 2076
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bo;Lcom/google/android/apps/gmm/map/internal/d/p;)Lcom/google/android/apps/gmm/map/internal/d/p;
    .locals 12

    .prologue
    .line 2319
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->e()I

    move-result v0

    .line 2320
    const/4 v6, -0x1

    .line 2321
    const/4 v2, 0x0

    .line 2322
    const/4 v1, 0x0

    .line 2324
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/n;->a:Lcom/google/android/apps/gmm/map/internal/d/n;

    iget-object v4, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/n;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2326
    const/4 v0, 0x0

    .line 2353
    :goto_0
    if-eqz v0, :cond_8

    .line 2357
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 2362
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->u:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/c;->b:Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/n;->c:Lcom/google/android/apps/gmm/map/internal/d/n;

    iget v7, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->j:I

    .line 2368
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v8

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bt;->h:I

    const/4 v9, 0x1

    .line 2370
    iget-object v10, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->o:Lcom/google/android/apps/gmm/map/internal/d/p;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/n;IIIZLcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 2372
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v1, :cond_0

    .line 2373
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->m()Z

    .line 2376
    :cond_0
    :goto_1
    return-object v0

    .line 2327
    :cond_1
    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 2328
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bo;->c()I

    move-result v3

    if-eq v0, v3, :cond_2

    .line 2329
    const/4 v0, 0x1

    goto :goto_0

    .line 2331
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2332
    const/4 v0, 0x1

    goto :goto_0

    .line 2333
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-nez v0, :cond_9

    .line 2337
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    if-eqz v0, :cond_4

    .line 2338
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 2339
    :goto_2
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v4

    .line 2340
    if-nez v0, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v8, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-ltz v5, :cond_5

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    iget-wide v10, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    cmp-long v3, v8, v10

    if-lez v3, :cond_5

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_6

    .line 2343
    iget v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    .line 2344
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2338
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 2340
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 2345
    :cond_6
    if-eqz v0, :cond_9

    .line 2346
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    if-nez v0, :cond_7

    .line 2347
    iget v0, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    iget v3, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    if-eq v0, v3, :cond_9

    .line 2348
    :cond_7
    iget v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    .line 2349
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    goto :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ")",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const v6, 0x1fffffff

    .line 3194
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v0

    .line 3195
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 3196
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    and-int/lit8 v0, v0, 0x1f

    int-to-long v0, v0

    const/16 v3, 0x3a

    shl-long/2addr v0, v3

    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    and-int/2addr v3, v6

    int-to-long v4, v3

    const/16 v3, 0x1d

    shl-long/2addr v4, v3

    or-long/2addr v0, v4

    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    and-int/2addr v3, v6

    int-to-long v4, v3

    or-long/2addr v0, v4

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 3197
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    .line 3198
    :goto_1
    new-instance v2, Lcom/google/b/a/an;

    invoke-direct {v2, v1, v0}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 3196
    :cond_0
    iget v4, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-nez v4, :cond_1

    const-wide/16 v0, 0x0

    :goto_2
    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    int-to-long v6, v5

    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    int-to-long v8, v5

    shl-long v4, v8, v4

    add-long/2addr v4, v6

    add-long/2addr v0, v4

    const-wide/high16 v4, -0x8000000000000000L

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/cd;->hashCode()I

    move-result v3

    int-to-long v6, v3

    sget v3, Lcom/google/android/apps/gmm/map/internal/d/d;->B:I

    shl-long/2addr v6, v3

    or-long/2addr v4, v6

    or-long/2addr v0, v4

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/d;->A:[J

    add-int/lit8 v1, v4, -0x1

    aget-wide v0, v0, v1

    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_2

    .line 3197
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 581
    .line 582
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 585
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v3, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    move v3, v0

    move v4, v0

    :goto_1
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v4, v2

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    if-eqz v4, :cond_2

    move v1, v2

    .line 588
    :cond_2
    if-eqz v1, :cond_3

    .line 589
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->h()V

    .line 591
    :cond_3
    return-void

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 2

    .prologue
    .line 1318
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v0, v1, :cond_0

    .line 1327
    :goto_0
    return-void

    .line 1321
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1322
    if-eqz v0, :cond_1

    .line 1323
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    goto :goto_0

    .line 1325
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/d;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->c:Z

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->l()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/i;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 79
    new-instance v5, Lcom/google/android/apps/gmm/shared/c/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/shared/c/b;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->s:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->s:Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/p;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->b(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/i;->a()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->e()I

    move-result v1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->e()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/internal/d/au;->f:Z

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v8, 0x46

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Received version: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " Cached version: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Clear: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(I)V

    :cond_1
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->i:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Request not found in list of outstanding requests"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/d/k;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/internal/d/k;-><init>()V

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/d/k;Lcom/google/android/apps/gmm/shared/c/b;)V

    goto :goto_2

    :cond_4
    move v0, v6

    :goto_3
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    if-ge v0, v1, :cond_7

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v1, v1, v0

    invoke-direct {p0, p1, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/k;Lcom/google/android/apps/gmm/shared/c/b;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v2

    if-nez v2, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/p;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    invoke-direct {p0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Response received. Total tiles: prefetch: %d normal: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, v4, Lcom/google/android/apps/gmm/map/internal/d/k;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget v3, v4, Lcom/google/android/apps/gmm/map/internal/d/k;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/au;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/r;->a(Z)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v6, v7

    :cond_a
    if-eqz v6, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->m()Z

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 5

    .prologue
    .line 79
    const/4 v0, 0x0

    :goto_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    if-ge v0, v1, :cond_1

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v3, v4}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x5

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    :goto_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->d(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/p;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p2, v0, :cond_2

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/i;->n:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->s:Z

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->b(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/d;Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/d;Z)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/r;->b(Z)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/ag;->b()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->h()V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/d/k;Lcom/google/android/apps/gmm/shared/c/b;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/i;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/p;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/k;",
            "Lcom/google/android/apps/gmm/shared/c/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1636
    const/4 v9, 0x0

    .line 1637
    const/4 v8, 0x0

    .line 1638
    sget-object v7, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    .line 1639
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    .line 1640
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/d/r;->b()Lcom/google/android/apps/gmm/map/internal/d/au;

    move-result-object v12

    .line 1641
    const/4 v4, 0x0

    move v11, v4

    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v11, v4, :cond_d

    .line 1642
    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1643
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/i;Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/k;Lcom/google/android/apps/gmm/shared/c/b;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v10

    .line 1644
    if-nez v10, :cond_0

    iget-boolean v5, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-eqz v5, :cond_c

    .line 1645
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 1646
    iget-object v13, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v5, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 1647
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v14, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v13, v14, :cond_1

    move-object v5, v7

    move-object v7, v10

    move-object/from16 v18, v4

    move-object v4, v6

    move-object/from16 v6, v18

    .line 1641
    :goto_1
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    move-object v9, v7

    move-object v7, v5

    move-object v8, v6

    move-object v6, v4

    goto :goto_0

    .line 1650
    :cond_1
    if-eqz v5, :cond_b

    .line 1652
    if-ne v12, v5, :cond_c

    .line 1653
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    if-ne v7, v5, :cond_1c

    .line 1654
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v6, v5, -0x1

    new-instance v7, Ljava/util/ArrayList;

    if-ltz v6, :cond_2

    const/4 v5, 0x1

    :goto_2
    if-nez v5, :cond_3

    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    const-wide/16 v14, 0x5

    int-to-long v0, v6

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    div-int/lit8 v5, v6, 0xa

    int-to-long v0, v5

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    const-wide/32 v16, 0x7fffffff

    cmp-long v5, v14, v16

    if-lez v5, :cond_4

    const v5, 0x7fffffff

    :goto_3
    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1655
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v13, v5, -0x1

    new-instance v6, Ljava/util/ArrayList;

    if-ltz v13, :cond_6

    const/4 v5, 0x1

    :goto_4
    if-nez v5, :cond_7

    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 1654
    :cond_4
    const-wide/32 v16, -0x80000000

    cmp-long v5, v14, v16

    if-gez v5, :cond_5

    const/high16 v5, -0x80000000

    goto :goto_3

    :cond_5
    long-to-int v5, v14

    goto :goto_3

    .line 1655
    :cond_6
    const/4 v5, 0x0

    goto :goto_4

    :cond_7
    const-wide/16 v14, 0x5

    int-to-long v0, v13

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    div-int/lit8 v5, v13, 0xa

    int-to-long v0, v5

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    const-wide/32 v16, 0x7fffffff

    cmp-long v5, v14, v16

    if-lez v5, :cond_9

    const v5, 0x7fffffff

    :goto_5
    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object v5, v6

    move-object v6, v7

    .line 1657
    :goto_6
    if-eqz v10, :cond_8

    .line 1658
    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1660
    :cond_8
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v4, v5

    move-object v7, v9

    move-object v5, v6

    move-object v6, v8

    goto/16 :goto_1

    .line 1655
    :cond_9
    const-wide/32 v16, -0x80000000

    cmp-long v5, v14, v16

    if-gez v5, :cond_a

    const/high16 v5, -0x80000000

    goto :goto_5

    :cond_a
    long-to-int v5, v14

    goto :goto_5

    .line 1663
    :cond_b
    const-string v4, "DashServerTileStore"

    const-string v5, "Unexpected tile type received"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v4, v5, v10}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_c
    move-object v4, v6

    move-object v5, v7

    move-object v6, v8

    move-object v7, v9

    goto/16 :goto_1

    .line 1669
    :cond_d
    const/4 v5, 0x0

    .line 1670
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    .line 1671
    const/4 v4, 0x0

    move v10, v5

    move v5, v4

    :goto_7
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_f

    .line 1673
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    or-int/2addr v10, v4

    .line 1671
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    :cond_e
    move v10, v5

    .line 1678
    :cond_f
    if-eqz v8, :cond_1a

    if-nez v9, :cond_10

    .line 1679
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/d/p;->a()Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1680
    :cond_10
    invoke-virtual {v8, v10}, Lcom/google/android/apps/gmm/map/internal/d/p;->b(Z)V

    .line 1682
    if-eqz v9, :cond_12

    .line 1685
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1686
    invoke-interface {v9}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v7

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/d/p;->p:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v5, :cond_14

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    :cond_11
    :goto_8
    move-object v7, v4

    .line 1690
    :cond_12
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v4, v9, v7}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 1702
    :cond_13
    :goto_9
    return-void

    .line 1686
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/d/r;->b()Lcom/google/android/apps/gmm/map/internal/d/au;

    move-result-object v10

    if-nez v10, :cond_15

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    goto :goto_8

    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v11, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v6, v11, :cond_16

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    move-object v6, v5

    :goto_a
    if-eqz v6, :cond_11

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v5, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v11

    if-eqz v11, :cond_18

    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v14, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-ltz v13, :cond_17

    invoke-interface {v12}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v12

    iget-wide v14, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v5, v12, v14

    if-lez v5, :cond_17

    const/4 v5, 0x1

    :goto_b
    if-nez v5, :cond_18

    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bt;)Z

    move-result v5

    if-nez v5, :cond_18

    invoke-static {v11}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    goto :goto_8

    :cond_16
    const/4 v5, 0x0

    move-object v6, v5

    goto :goto_a

    :cond_17
    const/4 v5, 0x0

    goto :goto_b

    :cond_18
    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v5

    if-eqz v5, :cond_11

    invoke-interface {v5, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v7

    if-eqz v7, :cond_11

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v12, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-ltz v11, :cond_19

    invoke-interface {v10}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v10

    iget-wide v12, v5, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v5, v10, v12

    if-lez v5, :cond_19

    const/4 v5, 0x1

    :goto_c
    if-nez v5, :cond_11

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bt;)Z

    move-result v5

    if-nez v5, :cond_11

    invoke-static {v7}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    goto/16 :goto_8

    :cond_19
    const/4 v5, 0x0

    goto :goto_c

    .line 1695
    :cond_1a
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1b

    .line 1696
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v4, v7}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 1698
    :cond_1b
    if-eqz v10, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v4, :cond_13

    .line 1699
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/r;->a(Z)V

    goto/16 :goto_9

    :cond_1c
    move-object v5, v6

    move-object v6, v7

    goto/16 :goto_6
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->h:Lcom/google/android/apps/gmm/map/internal/d/av;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->h:Lcom/google/android/apps/gmm/map/internal/d/av;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/av;->a:Z

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 485
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 486
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 3
    .param p3    # Lcom/google/android/apps/gmm/map/internal/c/bo;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/p;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2428
    const/4 v0, 0x0

    .line 2429
    :goto_0
    if-eqz p1, :cond_3

    .line 2432
    if-nez p2, :cond_2

    .line 2434
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2436
    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    if-eqz v2, :cond_0

    move v0, v1

    .line 2440
    :cond_0
    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->q:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 2429
    :goto_1
    iget-object p1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_0

    .line 2444
    :cond_1
    const/4 v2, 0x4

    invoke-static {p1, v2, p3, p4}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    goto :goto_1

    .line 2447
    :cond_2
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    goto :goto_1

    .line 2450
    :cond_3
    if-eqz v0, :cond_4

    .line 2452
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/au;->b(Z)V

    .line 2453
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_4

    .line 2454
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/r;->a(Z)V

    .line 2457
    :cond_4
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1936
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 1937
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    if-eq v0, v2, :cond_2

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->i:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 1938
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)V

    .line 1942
    :goto_2
    return-void

    .line 1936
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_1

    .line 1940
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v2, :cond_5

    move-object v0, v1

    :goto_3
    if-eqz v0, :cond_6

    :goto_4
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v2, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a_(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->d(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_4
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1467
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 1468
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->a:Lcom/google/android/apps/gmm/map/internal/d/n;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->b:Lcom/google/android/apps/gmm/map/internal/d/n;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 1472
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1473
    :cond_2
    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ZLcom/google/android/apps/gmm/map/internal/d/l;)V

    .line 1489
    :cond_3
    :goto_1
    iget v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    if-gez v0, :cond_4

    .line 1490
    const/4 v0, 0x3

    iput v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 1492
    :cond_4
    return-void

    .line 1475
    :cond_5
    if-eqz p2, :cond_8

    .line 1476
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    if-nez v0, :cond_6

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/internal/d/p;->f:Z

    if-nez v0, :cond_8

    .line 1479
    :cond_6
    iput-boolean v2, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    .line 1480
    iput-object p1, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_1

    :cond_7
    move v0, v2

    .line 1476
    goto :goto_2

    .line 1482
    :cond_8
    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ZLcom/google/android/apps/gmm/map/internal/d/l;)V

    .line 1483
    iget-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    if-nez v0, :cond_3

    .line 1485
    iput-object p1, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/p;ZLcom/google/android/apps/gmm/map/internal/d/l;)V
    .locals 10

    .prologue
    .line 2152
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 2154
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/au;

    move-result-object v1

    .line 2156
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_5

    .line 2157
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v2

    .line 2158
    if-eqz v2, :cond_5

    .line 2159
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v4

    .line 2160
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v6, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    iget-wide v8, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 2161
    :goto_0
    if-nez v0, :cond_5

    .line 2162
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2163
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    .line 2164
    const/4 v0, 0x2

    iput v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 2179
    :cond_0
    :goto_1
    iget-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    if-eqz v0, :cond_5

    .line 2276
    :goto_2
    return-void

    .line 2160
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2165
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-nez v0, :cond_3

    .line 2166
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;Lcom/google/android/apps/gmm/map/internal/d/p;)Lcom/google/android/apps/gmm/map/internal/d/p;

    move-result-object v0

    iput-object v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 2167
    const/4 v0, 0x0

    iput v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 2168
    iput-object v2, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 2169
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    goto :goto_1

    .line 2170
    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-eqz v0, :cond_0

    .line 2171
    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bt;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2173
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    if-eqz v0, :cond_4

    .line 2174
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bt;->a(Lcom/google/android/apps/gmm/shared/net/ad;)Z

    move-result v0

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->e:Z

    .line 2176
    :cond_4
    const/4 v0, 0x0

    iput v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 2177
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    goto :goto_1

    .line 2182
    :cond_5
    if-eqz p2, :cond_10

    .line 2195
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v2

    .line 2196
    if-eqz v2, :cond_10

    .line 2198
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-eqz v0, :cond_9

    .line 2199
    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v2

    .line 2200
    if-eqz v2, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_7

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_10

    .line 2201
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bt;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2202
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    if-ne v0, v1, :cond_8

    iget-boolean v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->g:Z

    if-eqz v0, :cond_8

    .line 2203
    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 2210
    :cond_6
    :goto_4
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    goto :goto_2

    .line 2200
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 2205
    :cond_8
    const/4 v0, 0x0

    iput v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 2206
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bt;->a(Lcom/google/android/apps/gmm/shared/net/ad;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2207
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->e:Z

    goto :goto_4

    .line 2214
    :cond_9
    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v4

    .line 2219
    if-eqz v4, :cond_10

    .line 2225
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-ltz v6, :cond_a

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_a

    const/4 v0, 0x1

    .line 2226
    :goto_5
    if-nez v0, :cond_10

    .line 2230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    if-ne v0, v1, :cond_b

    invoke-interface {v2, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2233
    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 2234
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    goto/16 :goto_2

    .line 2225
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 2236
    :cond_b
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_c

    .line 2237
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    .line 2239
    :cond_c
    invoke-direct {p0, v4, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;Lcom/google/android/apps/gmm/map/internal/d/p;)Lcom/google/android/apps/gmm/map/internal/d/p;

    move-result-object v0

    iput-object v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 2243
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-nez v0, :cond_d

    .line 2244
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v0, v1, :cond_d

    .line 2245
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v1, v2, :cond_e

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    move-object v2, v0

    :goto_6
    if-eqz v2, :cond_d

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_f

    const-wide/16 v0, -0x1

    :goto_7
    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/t;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    .line 2247
    :cond_d
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    .line 2248
    const/4 v0, 0x0

    iput v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 2249
    iput-object v4, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    goto/16 :goto_2

    .line 2245
    :cond_e
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_6

    :cond_f
    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v0

    sub-long v0, v6, v0

    goto :goto_7

    .line 2252
    :cond_10
    const/4 v0, -0x1

    iput v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->i:I

    .line 2274
    const/4 v0, 0x0

    iput-boolean v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    .line 2275
    const/4 v0, 0x0

    iput-object v0, p3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto/16 :goto_2
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/c/bt;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2283
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    .line 2284
    iget-wide v4, p2, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_1

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-wide v4, p2, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    .line 2285
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v2, v3, :cond_2

    .line 2286
    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    iget v3, p2, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    if-eq v2, v3, :cond_2

    .line 2289
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 2284
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2289
    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/d/d;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 79
    new-instance v4, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->d:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/i;

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v7, v7, v3

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v9, v7}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v7, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v7, v7, v3

    invoke-direct {p0, v7}, Lcom/google/android/apps/gmm/map/internal/d/d;->b(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 20

    .prologue
    .line 1037
    .line 1039
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/d;->t:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1042
    const/4 v2, 0x3

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 1150
    :cond_0
    :goto_0
    return-void

    .line 1047
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v2, v3}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v2

    .line 1048
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1049
    const/4 v9, 0x0

    .line 1050
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->e:Z

    .line 1053
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1054
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v3, 0x3

    iput v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 1055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    .line 1056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1063
    :goto_1
    const/4 v4, 0x0

    .line 1064
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    .line 1065
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v2, :cond_16

    const/4 v2, 0x0

    move-object v14, v2

    .line 1067
    :goto_2
    if-eqz v14, :cond_5

    .line 1068
    iget-object v2, v14, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_5

    .line 1069
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->e:Z

    .line 1070
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-object v15, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 1072
    iget-object v3, v14, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eqz v15, :cond_1a

    const/4 v2, 0x1

    :goto_4
    if-eqz v15, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-nez v4, :cond_1b

    .line 1075
    invoke-interface {v15}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v4

    iget v9, v4, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    .line 1071
    :goto_5
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/n;->b:Lcom/google/android/apps/gmm/map/internal/d/n;

    if-ne v4, v5, :cond_1c

    if-eqz v2, :cond_1c

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/d/n;->c:Lcom/google/android/apps/gmm/map/internal/d/n;

    :goto_6
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/p;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v12, p1

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/n;IIIZLcom/google/android/apps/gmm/map/internal/d/p;)V

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    .line 1077
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v4, v5}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1079
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/n;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1080
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    .line 1081
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    iput-object v2, v3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1086
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v5, v14, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v5, v6, :cond_1e

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    :goto_8
    if-eqz v8, :cond_30

    iget-boolean v4, v3, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    if-eqz v4, :cond_24

    iget-object v10, v3, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    const/4 v3, 0x0

    if-eqz v10, :cond_31

    invoke-interface {v10}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v6

    iget v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    iget v5, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    if-eq v4, v5, :cond_1f

    const/4 v4, 0x1

    :goto_9
    if-eqz v4, :cond_31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    iget-wide v0, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    move-wide/from16 v16, v0

    const-wide/16 v18, -0x1

    cmp-long v4, v16, v18

    if-nez v4, :cond_21

    const-wide/16 v6, -0x1

    :goto_a
    move-object v4, v14

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/apps/gmm/map/internal/d/t;->a(Lcom/google/android/apps/gmm/map/internal/d/au;Lcom/google/android/apps/gmm/map/internal/c/bp;JLcom/google/android/apps/gmm/map/internal/d/s;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v2

    :goto_b
    const/4 v3, 0x2

    new-instance v4, Ljava/util/ArrayList;

    const-wide/16 v6, 0x5

    int-to-long v0, v3

    move-wide/from16 v16, v0

    add-long v6, v6, v16

    const/4 v3, 0x0

    int-to-long v0, v3

    move-wide/from16 v16, v0

    add-long v6, v6, v16

    const-wide/32 v16, 0x7fffffff

    cmp-long v3, v6, v16

    if-lez v3, :cond_22

    const v3, 0x7fffffff

    :goto_c
    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz v2, :cond_3

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    if-eqz v10, :cond_4

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v2, v4

    .line 1088
    :goto_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1089
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/internal/d/l;->e:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/p;->b(Z)V

    move-object v4, v3

    move-object v3, v2

    .line 1095
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    const/4 v5, 0x3

    if-ne v2, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-eqz v2, :cond_6

    .line 1098
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v2, v5}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v2

    .line 1099
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/m;

    .line 1100
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/m;->a()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v5, 0x2

    iput v5, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    .line 1102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    const/4 v5, 0x0

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1106
    :cond_6
    if-eqz v4, :cond_7

    .line 1107
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v2, v5}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v2

    .line 1108
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/m;

    .line 1109
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/m;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1110
    const/4 v4, 0x0

    .line 1115
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->a:Z

    if-eqz v2, :cond_a

    .line 1116
    :cond_8
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->q:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v5, :cond_25

    const/4 v2, 0x0

    :goto_e
    if-eqz v2, :cond_26

    :goto_f
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->c:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;Z)I

    move-result v7

    invoke-interface {v2, v5, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;I)V

    :cond_9
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    if-eqz v2, :cond_a

    if-eqz v14, :cond_a

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v2

    iget-object v5, v14, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eqz v2, :cond_a

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v6, v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;Z)I

    move-result v7

    invoke-interface {v2, v5, v6, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;I)V

    .line 1128
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    if-nez v2, :cond_27

    move-object v2, v3

    :goto_10
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 1136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 1137
    if-nez v2, :cond_b

    if-eqz v4, :cond_0

    .line 1141
    :cond_b
    if-eqz v2, :cond_c

    .line 1142
    iput-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->p:Ljava/util/List;

    .line 1145
    :cond_c
    if-eqz v2, :cond_d

    if-nez v13, :cond_28

    :cond_d
    move-object v3, v2

    .line 1146
    :goto_11
    if-eqz v4, :cond_e

    if-nez v9, :cond_2a

    .line 1147
    :cond_e
    :goto_12
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-nez v3, :cond_2c

    if-nez v4, :cond_2c

    const/4 v2, 0x0

    :goto_13
    if-eqz v2, :cond_0

    .line 1148
    if-nez v3, :cond_f

    if-eqz v4, :cond_0

    :cond_f
    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_10
    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->l()V

    :cond_12
    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->c(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    :cond_13
    if-eqz v4, :cond_14

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/d;->c(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/i;->Q_()Z

    move-result v2

    if-eqz v2, :cond_2f

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->l()V

    goto/16 :goto_0

    .line 1058
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/l;)V

    .line 1059
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->y:Lcom/google/android/apps/gmm/map/internal/d/l;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->e:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Z)V

    goto/16 :goto_1

    .line 1065
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 1066
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/r;->b()Lcom/google/android/apps/gmm/map/internal/d/au;

    move-result-object v2

    move-object v14, v2

    goto/16 :goto_2

    .line 1068
    :cond_17
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/bv;->e:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v2

    aget-object v2, v5, v2

    if-eqz v2, :cond_19

    instance-of v5, v2, Lcom/google/android/apps/gmm/map/internal/c/b;

    if-eqz v5, :cond_19

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/b;->a:Landroid/accounts/Account;

    if-eqz v2, :cond_18

    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1072
    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1075
    :cond_1b
    const/4 v9, -0x1

    goto/16 :goto_5

    .line 1071
    :cond_1c
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    goto/16 :goto_6

    .line 1083
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->z:Lcom/google/android/apps/gmm/map/internal/d/l;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9, v3}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/android/apps/gmm/map/internal/d/l;)V

    goto/16 :goto_7

    .line 1086
    :cond_1e
    const/4 v8, 0x0

    goto/16 :goto_8

    :cond_1f
    iget-object v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-wide v0, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    move-wide/from16 v16, v0

    const-wide/16 v18, -0x1

    cmp-long v5, v16, v18

    if-nez v5, :cond_20

    const-wide/16 v4, -0x1

    :goto_14
    invoke-virtual {v8, v7, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Z

    move-result v4

    goto/16 :goto_9

    :cond_20
    iget-wide v0, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    move-wide/from16 v16, v0

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v4

    sub-long v4, v16, v4

    goto :goto_14

    :cond_21
    iget-wide v0, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    move-wide/from16 v16, v0

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v6

    sub-long v6, v16, v6

    goto/16 :goto_a

    :cond_22
    const-wide/32 v16, -0x80000000

    cmp-long v3, v6, v16

    if-gez v3, :cond_23

    const/high16 v3, -0x80000000

    goto/16 :goto_c

    :cond_23
    long-to-int v3, v6

    goto/16 :goto_c

    :cond_24
    if-eqz v15, :cond_30

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/r;->c:Lcom/google/android/apps/gmm/map/internal/d/t;

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    const-wide/16 v6, 0x0

    move-object v4, v14

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/apps/gmm/map/internal/d/t;->a(Lcom/google/android/apps/gmm/map/internal/d/au;Lcom/google/android/apps/gmm/map/internal/c/bp;JLcom/google/android/apps/gmm/map/internal/d/s;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v2

    if-eqz v2, :cond_30

    invoke-static {v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v2

    goto/16 :goto_d

    .line 1116
    :cond_25
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto/16 :goto_e

    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    goto/16 :goto_f

    .line 1128
    :cond_27
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    goto/16 :goto_10

    .line 1145
    :cond_28
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->f:Z

    if-eqz v3, :cond_29

    const/4 v3, 0x0

    goto/16 :goto_11

    :cond_29
    invoke-virtual {v13, v2}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    const/4 v3, 0x0

    goto/16 :goto_11

    .line 1146
    :cond_2a
    iget-boolean v2, v4, Lcom/google/android/apps/gmm/map/internal/d/p;->f:Z

    if-eqz v2, :cond_2b

    const/4 v4, 0x0

    goto/16 :goto_12

    :cond_2b
    invoke-virtual {v9, v4}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    const/4 v4, 0x0

    goto/16 :goto_12

    .line 1147
    :cond_2c
    if-eqz v3, :cond_2d

    move-object v2, v3

    :goto_15
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/d/ac;->u()Lcom/google/android/apps/gmm/map/h/d;

    iget-boolean v6, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->s:Z

    if-eqz v6, :cond_2e

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2e

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-nez v2, :cond_2e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/p;)V

    const/4 v2, 0x0

    goto/16 :goto_13

    :cond_2d
    move-object v2, v4

    goto :goto_15

    :cond_2e
    const/4 v2, 0x1

    goto/16 :goto_13

    .line 1148
    :cond_2f
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->c:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const-wide/16 v4, 0x32

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->c:Z

    goto/16 :goto_0

    :cond_30
    move-object v2, v10

    goto/16 :goto_d

    :cond_31
    move-object v2, v3

    goto/16 :goto_b
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 869
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 870
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 869
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 872
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->a:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 873
    monitor-exit p0

    return-void
.end method

.method private c(Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 3

    .prologue
    .line 1369
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Z)V

    .line 1370
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->f:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v1, v2}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1371
    return-void

    .line 1369
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 3

    .prologue
    .line 2001
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v0, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    .line 2002
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2003
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->q:Ljava/util/Map;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/m;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/map/internal/d/m;-><init>(Lcom/google/android/apps/gmm/map/internal/d/d;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2007
    :cond_0
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1515
    .line 1516
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    if-lez v0, :cond_0

    .line 1517
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 1518
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1524
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->b(Z)Lcom/google/android/apps/gmm/map/internal/d/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    .line 1525
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->l:Lcom/google/android/apps/gmm/map/internal/d/i;

    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/d/i;->k:Lcom/google/android/apps/gmm/map/internal/d/d;

    .line 1527
    :cond_0
    return-void

    .line 1524
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Z
    .locals 4

    .prologue
    .line 3179
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->W:Lcom/google/android/apps/gmm/shared/b/c;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;J)J

    move-result-wide v0

    .line 3185
    const-wide/16 v2, 0x64

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 752
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a()Z

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_2

    .line 754
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    move v1, v2

    :goto_0
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a()Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 756
    :cond_2
    return v2
.end method

.method public final a(J)J
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 788
    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 790
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->r:I

    if-lez v0, :cond_2

    .line 798
    :goto_1
    return-wide p1

    .line 793
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/r;->a()J

    move-result-wide v0

    .line 794
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v4, v0, v4

    if-nez v4, :cond_3

    move-wide v0, v2

    .line 798
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sub-long v0, v4, v0

    sub-long v0, p1, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 6

    .prologue
    .line 435
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/o;-><init>()V

    .line 441
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v1, v2, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 442
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/l;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/internal/d/l;-><init>()V

    .line 444
    invoke-direct {p0, v1, p2, v2}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ZLcom/google/android/apps/gmm/map/internal/d/l;)V

    .line 446
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    if-ltz v3, :cond_0

    .line 449
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->b:I

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->c:Lcom/google/android/apps/gmm/map/internal/c/bo;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 452
    :cond_0
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/d/l;->d:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 453
    if-eqz v1, :cond_1

    .line 455
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 457
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/o;->a:Lcom/google/android/apps/gmm/map/internal/c/bo;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Z)Lcom/google/android/apps/gmm/map/internal/d/ak;
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v6, -0x1

    .line 766
    if-eqz p4, :cond_0

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 769
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v10, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v7, v6

    move v9, v8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/n;IIIZLcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 781
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 782
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 783
    return-object v0

    .line 766
    :cond_0
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/n;->c:Lcom/google/android/apps/gmm/map/internal/d/n;

    goto :goto_0
.end method

.method a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1952
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 1953
    :goto_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v2, :cond_0

    .line 1954
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 1955
    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v4

    .line 1954
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/co;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/internal/c/co;-><init>()V

    iput-object p1, v3, Lcom/google/android/apps/gmm/map/internal/c/co;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object p2, v3, Lcom/google/android/apps/gmm/map/internal/c/co;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->e()I

    move-result v6

    iput v6, v3, Lcom/google/android/apps/gmm/map/internal/c/co;->b:I

    iput-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/c/co;->k:J

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/co;->a()Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v3

    invoke-interface {v2, p2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    .line 1957
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    .line 1958
    if-eqz v0, :cond_1

    .line 1959
    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a_(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 1961
    :cond_1
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq p1, v0, :cond_2

    .line 1962
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v2, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    .line 1963
    :goto_2
    if-eqz v0, :cond_2

    .line 1964
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 1967
    :cond_2
    return-void

    .line 1952
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 1962
    goto :goto_2
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2466
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/au;

    move-result-object v8

    .line 2467
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 2468
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    move-object v7, v0

    .line 2470
    :goto_0
    if-eqz v8, :cond_5

    .line 2471
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_5

    .line 2472
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 2473
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_0

    .line 2474
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v0

    .line 2475
    if-eqz v0, :cond_0

    .line 2476
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    .line 2479
    :cond_0
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2483
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 2484
    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    .line 2483
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;JJ)V

    .line 2487
    :cond_1
    if-eqz v7, :cond_2

    .line 2488
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/d/s;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/d/s;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v4

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    add-long/2addr v2, v4

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2490
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 2471
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 2468
    :cond_3
    const/4 v0, 0x0

    move-object v7, v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    move-object v7, v0

    goto :goto_0

    .line 2493
    :cond_5
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/bc;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/bc;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->p:Lcom/google/android/apps/gmm/map/internal/d/ag;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 492
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v0, v2, v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v2

    if-gtz v0, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 496
    new-instance v3, Ljava/util/ArrayList;

    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0x5

    int-to-long v6, v2

    add-long/2addr v4, v6

    div-int/lit8 v0, v2, 0xa

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    const v0, 0x7fffffff

    :goto_2
    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 498
    :goto_3
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 499
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 496
    :cond_4
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    const/high16 v0, -0x80000000

    goto :goto_2

    :cond_5
    long-to-int v0, v4

    goto :goto_2

    .line 502
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/16 v1, 0x9

    new-instance v2, Lcom/google/b/a/an;

    invoke-direct {v2, p1, v3}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;JLcom/google/android/apps/gmm/map/internal/d/a/c;)V
    .locals 4

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-nez v0, :cond_1

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/r;->b()Lcom/google/android/apps/gmm/map/internal/d/au;

    move-result-object v0

    .line 467
    if-eqz v0, :cond_0

    .line 470
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v2, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/r;->d:Lcom/google/android/apps/gmm/map/internal/d/s;

    .line 471
    :goto_1
    if-eqz v0, :cond_0

    .line 474
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    invoke-virtual {p0, p1, p4}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    goto :goto_0

    .line 470
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 680
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    monitor-enter v2

    .line 681
    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/at;

    .line 683
    if-eqz v0, :cond_0

    .line 684
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/d/at;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    move v0, v1

    .line 681
    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 686
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 689
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V
    .locals 2

    .prologue
    .line 395
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 401
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 402
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Z)V
    .locals 3

    .prologue
    .line 415
    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->b:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 423
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v1, v2, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/n;)V

    .line 425
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 426
    return-void

    .line 415
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->a:Lcom/google/android/apps/gmm/map/internal/d/n;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/at;)V
    .locals 3

    .prologue
    .line 649
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    monitor-enter v1

    .line 650
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Queue;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/a/c;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/c;",
            "Lcom/google/android/apps/gmm/map/internal/d/a/c;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/q;

    invoke-direct {v2, p1, p3, p2, p4}, Lcom/google/android/apps/gmm/map/internal/d/q;-><init>(Ljava/util/Queue;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 941
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 942
    return-void
.end method

.method public declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 532
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    :goto_0
    monitor-exit p0

    return-void

    .line 535
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/g;-><init>(Lcom/google/android/apps/gmm/map/internal/d/d;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 532
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 2802
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z
.end method

.method protected abstract b(Z)Lcom/google/android/apps/gmm/map/internal/d/i;
.end method

.method public final b(Lcom/google/android/apps/gmm/map/internal/d/at;)V
    .locals 3

    .prologue
    .line 656
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    monitor-enter v1

    .line 657
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 658
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 659
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 660
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 663
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public abstract b()Z
.end method

.method public final d()Lcom/google/android/apps/gmm/v/b/a;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->w:Lcom/google/android/apps/gmm/v/b/a;

    return-object v0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->e:I

    goto :goto_0
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    move v1, v2

    .line 576
    :goto_1
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/h;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_2
    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_1

    :cond_0
    move v2, v3

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 575
    goto :goto_0

    :cond_3
    move v0, v2

    .line 576
    goto :goto_2

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_3
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->d()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected final h()V
    .locals 4

    .prologue
    .line 667
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    monitor-enter v2

    .line 668
    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/at;

    .line 670
    if-eqz v0, :cond_0

    .line 671
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/at;->a()V

    move v0, v1

    .line 668
    :goto_1
    add-int/lit8 v1, v0, 0x1

    goto :goto_0

    .line 673
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->n:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 676
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->start()V

    .line 710
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 712
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 714
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 716
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 718
    :goto_1
    return-void

    .line 714
    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public j()V
    .locals 3

    .prologue
    .line 735
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->b:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->b:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 737
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->b:Landroid/os/Looper;

    .line 740
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 744
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->c()V

    .line 745
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_1

    .line 746
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/d/r;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->c()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 742
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 748
    :cond_1
    return-void
.end method

.method public k()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 3190
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 842
    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 848
    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 849
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->b:Landroid/os/Looper;

    .line 851
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->b:Landroid/os/Looper;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/h;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/h;-><init>(Lcom/google/android/apps/gmm/map/internal/d/d;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->k:Landroid/os/Handler;

    .line 854
    monitor-enter p0

    .line 855
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 856
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 858
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->a()V

    .line 859
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/d;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/r;->c()V

    .line 863
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->c()V

    .line 865
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 866
    return-void

    .line 843
    :catch_0
    move-exception v0

    .line 844
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/d;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 856
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
