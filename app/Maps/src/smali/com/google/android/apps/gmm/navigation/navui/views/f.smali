.class public Lcom/google/android/apps/gmm/navigation/navui/views/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/graphics/Paint;

.field final b:Landroid/graphics/Paint;

.field final c:Landroid/graphics/Paint;

.field final d:F

.field final e:F


# direct methods
.method public constructor <init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;FF)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/views/f;->a:Landroid/graphics/Paint;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/f;->b:Landroid/graphics/Paint;

    .line 71
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/f;->c:Landroid/graphics/Paint;

    .line 72
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/navui/views/f;->d:F

    .line 73
    iput p5, p0, Lcom/google/android/apps/gmm/navigation/navui/views/f;->e:F

    .line 74
    return-void
.end method

.method public static a(Landroid/content/res/Resources;IIIFF)Lcom/google/android/apps/gmm/navigation/navui/views/f;
    .locals 6

    .prologue
    .line 54
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 55
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 58
    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 61
    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/navui/views/f;-><init>(Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;FF)V

    return-object v0
.end method
