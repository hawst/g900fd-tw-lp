.class public Lcom/google/android/apps/gmm/map/internal/d/au;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/ai;

.field b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

.field public c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

.field volatile d:Z

.field e:I

.field final f:Z

.field final g:Ljava/lang/String;

.field volatile h:Lcom/google/android/apps/gmm/map/internal/d/av;

.field final i:I

.field volatile j:Z

.field final k:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field private l:Ljava/util/Locale;

.field private m:Ljava/io/File;

.field private final n:Ljava/util/concurrent/locks/ReentrantLock;

.field private o:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/as;Lcom/google/android/apps/gmm/map/internal/d/b/s;ZLjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/ac;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->n:Ljava/util/concurrent/locks/ReentrantLock;

    .line 77
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    .line 81
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->o:Z

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 92
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->g:Ljava/lang/String;

    .line 93
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    .line 94
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->e:I

    .line 96
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->f:Z

    .line 97
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->l:Ljava/util/Locale;

    .line 98
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->m:Ljava/io/File;

    .line 99
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v0, :cond_0

    const/16 v0, 0x3e8

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->i:I

    .line 100
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 101
    return-void

    .line 99
    :cond_0
    const/16 v0, 0xbb8

    goto :goto_0
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 3

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 136
    :goto_0
    monitor-exit p0

    return-void

    .line 117
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->m:Ljava/io/File;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->g:Ljava/lang/String;

    const-string v1, "Unable to init disk cache"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    if-eqz v0, :cond_3

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->l:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->e()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->l:Ljava/util/Locale;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(Ljava/util/Locale;)Z

    .line 127
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->d:Z

    .line 130
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->o:Z

    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Z)V
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a()Z

    .line 166
    :cond_0
    if-eqz p1, :cond_1

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 169
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->f()V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->g:Ljava/lang/String;

    const-string v1, "Unable to clear disk cache"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    .line 174
    :cond_1
    return-void
.end method

.method final a(I)Z
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 195
    :cond_0
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->e:I

    .line 197
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->f:Z

    if-eqz v1, :cond_3

    .line 198
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->a()Z

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a()Z

    .line 204
    :cond_2
    const/4 v0, 0x1

    .line 206
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/internal/d/b/s;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->d:Z

    if-nez v0, :cond_1

    .line 148
    monitor-enter p0

    .line 150
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->d:Z

    if-nez v0, :cond_0

    .line 151
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 155
    const/4 v0, 0x0

    monitor-exit p0

    .line 159
    :goto_1
    return-object v0

    .line 157
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    goto :goto_1

    .line 157
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method final b(Z)V
    .locals 2

    .prologue
    .line 300
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->n:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->h:Lcom/google/android/apps/gmm/map/internal/d/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->h:Lcom/google/android/apps/gmm/map/internal/d/av;

    .line 306
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/d/av;->b:Z

    if-eqz v0, :cond_1

    .line 307
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/av;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/d/av;-><init>(Lcom/google/android/apps/gmm/map/internal/d/au;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->h:Lcom/google/android/apps/gmm/map/internal/d/av;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->n:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 312
    if-eqz p1, :cond_2

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    .line 315
    :cond_2
    return-void

    .line 310
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->n:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 270
    :goto_0
    monitor-exit p0

    return-void

    .line 262
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a()Z

    .line 265
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_2

    .line 267
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->f()V

    .line 269
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/au;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
