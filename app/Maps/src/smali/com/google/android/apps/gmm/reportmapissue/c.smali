.class public Lcom/google/android/apps/gmm/reportmapissue/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/b/a/u;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/android/apps/gmm/map/b/a/w;

.field public e:I

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:[B

.field public l:I

.field public m:I

.field public n:I

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 91
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/b/a/ak;->b:Z

    const-string v0, "location"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 93
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "levelIdString"

    .line 94
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "mapsLink"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->c:Ljava/lang/String;

    .line 95
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "mapState"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->d:Lcom/google/android/apps/gmm/map/b/a/w;

    .line 96
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "latSpan"

    iget v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->e:I

    .line 97
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "lngSpan"

    iget v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->f:I

    .line 98
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "type"

    iget v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    .line 99
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "description"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->h:Ljava/lang/String;

    .line 100
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "sendEmail"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->j:Z

    .line 101
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "audioSnippet"

    .line 102
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "sourceApplication"

    iget v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->l:I

    .line 103
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "routeIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->m:I

    .line 104
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->n:I

    .line 105
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepText"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    .line 106
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "entryPoint"

    const/4 v2, 0x0

    .line 107
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 108
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
