.class public Lcom/google/android/apps/gmm/map/r/a/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/maps/g/a/cj;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/maps/g/a/cj;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/cj;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/k;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/j;->a:Lcom/google/maps/g/a/cj;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/r/a/j;->b:Ljava/util/List;

    .line 32
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/cg;)Lcom/google/android/apps/gmm/map/r/a/j;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 48
    iget v0, p0, Lcom/google/maps/g/a/cg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_0

    move v0, v6

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v2

    .line 67
    :goto_1
    return-object v0

    :cond_0
    move v0, v4

    .line 48
    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/google/maps/g/a/cg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 54
    if-nez v7, :cond_2

    move-object v0, v2

    .line 55
    goto :goto_1

    .line 58
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v4

    .line 59
    :goto_2
    if-ge v3, v7, :cond_7

    .line 60
    iget-object v0, p0, Lcom/google/maps/g/a/cg;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/cm;->d()Lcom/google/maps/g/a/cm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/cm;

    iget v1, v0, Lcom/google/maps/g/a/cm;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v6, :cond_3

    move v1, v6

    :goto_3
    if-nez v1, :cond_4

    move-object v0, v2

    .line 61
    :goto_4
    if-nez v0, :cond_6

    move-object v0, v2

    .line 62
    goto :goto_1

    :cond_3
    move v1, v4

    .line 60
    goto :goto_3

    :cond_4
    new-instance v5, Lcom/google/android/apps/gmm/map/r/a/k;

    iget v1, v0, Lcom/google/maps/g/a/cm;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/cp;->a(I)Lcom/google/maps/g/a/cp;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/maps/g/a/cp;->a:Lcom/google/maps/g/a/cp;

    :cond_5
    iget-boolean v0, v0, Lcom/google/maps/g/a/cm;->c:Z

    invoke-direct {v5, v1, v0}, Lcom/google/android/apps/gmm/map/r/a/k;-><init>(Lcom/google/maps/g/a/cp;Z)V

    move-object v0, v5

    goto :goto_4

    .line 64
    :cond_6
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 67
    :cond_7
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/j;

    iget v0, p0, Lcom/google/maps/g/a/cg;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/cj;->a(I)Lcom/google/maps/g/a/cj;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/a/cj;->a:Lcom/google/maps/g/a/cj;

    :cond_8
    invoke-direct {v1, v0, v8}, Lcom/google/android/apps/gmm/map/r/a/j;-><init>(Lcom/google/maps/g/a/cj;Ljava/util/List;)V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/j;->a:Lcom/google/maps/g/a/cj;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "[guidance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " laneTurns("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/j;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/k;

    .line 88
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/k;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 89
    goto :goto_0

    .line 90
    :cond_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " )]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    return-object v0
.end method
