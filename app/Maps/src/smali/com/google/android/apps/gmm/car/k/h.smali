.class public Lcom/google/android/apps/gmm/car/k/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/widget/FrameLayout;

.field private final b:Lcom/google/android/libraries/curvular/bd;

.field private c:Lcom/google/android/apps/gmm/car/k/a;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Lcom/google/android/libraries/curvular/bd;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/k/h;->a:Landroid/widget/FrameLayout;

    .line 18
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/k/h;->b:Lcom/google/android/libraries/curvular/bd;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 26
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->b:Lcom/google/android/libraries/curvular/bd;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 27
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/k/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    iget-object v2, v4, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    sget-object v5, Lcom/google/android/apps/gmm/car/k/e;->a:Lcom/google/android/apps/gmm/car/k/e;

    if-eq v2, v5, :cond_5

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, v4, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    sget-object v5, Lcom/google/android/apps/gmm/car/k/e;->d:Lcom/google/android/apps/gmm/car/k/e;

    if-eq v2, v5, :cond_1

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/car/k/a;->a(Z)V

    :cond_1
    new-instance v2, Lcom/google/android/apps/gmm/car/k/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/k/h;->b:Lcom/google/android/libraries/curvular/bd;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/k/h;->a:Landroid/widget/FrameLayout;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/k/a;-><init>(Ljava/lang/String;Lcom/google/android/libraries/curvular/bd;Landroid/view/ViewGroup;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    sget-object v4, Lcom/google/android/apps/gmm/car/k/e;->a:Lcom/google/android/apps/gmm/car/k/e;

    if-eq v3, v4, :cond_3

    move v0, v1

    :cond_3
    if-eqz v0, :cond_9

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    sget-object v3, Lcom/google/android/apps/gmm/car/k/e;->c:Lcom/google/android/apps/gmm/car/k/e;

    if-ne v0, v3, :cond_8

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/car/k/a;->a(Z)V

    .line 28
    :cond_4
    :goto_2
    return-void

    :cond_5
    move v2, v0

    .line 27
    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    sget-object v4, Lcom/google/android/apps/gmm/car/k/e;->a:Lcom/google/android/apps/gmm/car/k/e;

    if-eq v2, v4, :cond_7

    move v2, v1

    :goto_3
    if-nez v2, :cond_2

    new-instance v2, Lcom/google/android/apps/gmm/car/k/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/k/h;->b:Lcom/google/android/libraries/curvular/bd;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/k/h;->a:Landroid/widget/FrameLayout;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/k/a;-><init>(Ljava/lang/String;Lcom/google/android/libraries/curvular/bd;Landroid/view/ViewGroup;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/car/k/h;->c:Lcom/google/android/apps/gmm/car/k/a;

    goto :goto_1

    :cond_7
    move v2, v0

    goto :goto_3

    :cond_8
    iget-object v0, v2, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    sget-object v1, Lcom/google/android/apps/gmm/car/k/e;->d:Lcom/google/android/apps/gmm/car/k/e;

    if-ne v0, v1, :cond_4

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/k/a;->a()V

    goto :goto_2

    :cond_9
    iget-object v0, v2, Lcom/google/android/apps/gmm/car/k/a;->c:Landroid/view/View;

    iget-object v1, v2, Lcom/google/android/apps/gmm/car/k/a;->d:Lcom/google/android/libraries/curvular/ce;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/k/a;->b:Landroid/view/ViewGroup;

    iget-object v1, v2, Lcom/google/android/apps/gmm/car/k/a;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/k/a;->a()V

    goto :goto_2
.end method
