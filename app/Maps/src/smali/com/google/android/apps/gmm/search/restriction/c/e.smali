.class public Lcom/google/android/apps/gmm/search/restriction/c/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;
.implements Lcom/google/android/apps/gmm/search/restriction/d/d;


# instance fields
.field private a:Lcom/google/android/apps/gmm/search/restriction/c/f;

.field private final b:Landroid/content/res/Resources;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget-object v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->b:Landroid/content/res/Resources;

    .line 99
    iget v0, p2, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->c:Ljava/lang/Boolean;

    .line 100
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/search/restriction/c/e;->a(Lcom/google/r/b/a/alh;)V

    .line 101
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 3

    .prologue
    .line 126
    invoke-static {}, Lcom/google/maps/g/rb;->d()Lcom/google/maps/g/rb;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/qo;->c:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p1, Lcom/google/maps/g/qo;->a:I

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->e:Lcom/google/b/c/dn;

    invoke-virtual {p1, v0}, Lcom/google/r/b/a/alm;->b(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;

    move-result-object p1

    .line 121
    :cond_0
    return-object p1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 194
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 195
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 198
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 113
    new-instance v0, Lcom/google/n/ai;

    iget-object v1, p1, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->g:Lcom/google/n/aj;

    invoke-direct {v0, v1, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-static {v0}, Lcom/google/b/c/jp;->a(Ljava/lang/Iterable;)Lcom/google/b/c/dn;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/search/restriction/c/f;->a(Ljava/util/Set;)Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 114
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->d:Ljava/lang/Runnable;

    .line 135
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->h:I

    .line 159
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/restriction/c/f;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/f;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 203
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 204
    const-string v0, ""

    .line 207
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 221
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->a:Lcom/google/android/apps/gmm/search/restriction/c/f;

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 227
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->lK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 167
    :goto_0
    return-object v0

    .line 168
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/restriction/c/f;->f:Lcom/google/b/f/t;

    .line 167
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->lF:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 212
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/f;->values()[Lcom/google/android/apps/gmm/search/restriction/c/f;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 213
    const-string v0, ""

    .line 216
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/e;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final g(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
