.class public Lcom/google/android/apps/gmm/map/m/af;
.super Lcom/google/android/apps/gmm/map/m/d;
.source "PG"


# instance fields
.field private final e:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/m/r;FFF)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/m/d;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    .line 29
    mul-float v0, p4, p4

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/af;->e:F

    .line 30
    return-void
.end method


# virtual methods
.method public final a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")",
            "Lcom/google/android/apps/gmm/map/m/e;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 52
    .line 53
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    .line 54
    iget v3, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-ne v3, v5, :cond_0

    move-object v1, v0

    .line 60
    :goto_0
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 61
    :cond_1
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 62
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    .line 63
    iget v4, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-ne v4, v5, :cond_1

    .line 68
    :goto_1
    if-eqz v1, :cond_2

    if-nez v0, :cond_3

    .line 72
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    .line 105
    :goto_2
    return-object v0

    .line 76
    :cond_3
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/m/k;->d:J

    sub-long/2addr v2, p1

    .line 77
    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 81
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_2

    .line 87
    :cond_4
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v2, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v3, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float/2addr v2, v3

    .line 88
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v3, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v4, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    sub-float/2addr v3, v4

    .line 89
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v4, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v5, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    sub-float/2addr v4, v5

    .line 90
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 91
    mul-float v1, v2, v2

    mul-float v2, v3, v3

    add-float/2addr v1, v2

    mul-float v2, v4, v4

    mul-float/2addr v0, v0

    add-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 92
    iget v1, p0, Lcom/google/android/apps/gmm/map/m/af;->e:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 97
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_2

    .line 105
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->c:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_2

    :cond_6
    move-object v0, v2

    goto :goto_1

    :cond_7
    move-object v1, v2

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/af;->a:Lcom/google/android/apps/gmm/map/m/r;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/m/r;->b(Lcom/google/android/apps/gmm/map/m/n;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method protected final d(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/af;->a:Lcom/google/android/apps/gmm/map/m/r;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/m/r;->c(Lcom/google/android/apps/gmm/map/m/n;Z)V

    .line 116
    return-void
.end method

.method protected final f(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/af;->a:Lcom/google/android/apps/gmm/map/m/r;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/map/m/r;->a(Lcom/google/android/apps/gmm/map/m/n;Z)Z

    move-result v0

    return v0
.end method
