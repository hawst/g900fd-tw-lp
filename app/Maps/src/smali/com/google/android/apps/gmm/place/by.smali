.class Lcom/google/android/apps/gmm/place/by;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/l;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/base/activities/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/by;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 57
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/by;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/by;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/by;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/ZagatMoreInfoFragment;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 57
    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/by;->b:Lcom/google/android/apps/gmm/x/o;

    .line 41
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->w()Ljava/util/List;

    move-result-object v4

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/by;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 43
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/place/by;->c:Lcom/google/android/apps/gmm/base/activities/c;

    move v2, v3

    .line 46
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    const/4 v0, 0x3

    if-ge v2, v0, :cond_0

    .line 48
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 49
    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 50
    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 51
    iget-object v5, p0, Lcom/google/android/apps/gmm/place/by;->a:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/gmm/place/co;

    iget-object v7, p0, Lcom/google/android/apps/gmm/place/by;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v6, v1, v0, v7, v3}, Lcom/google/android/apps/gmm/place/co;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;Z)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/by;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/by;->a:Ljava/util/List;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/by;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 77
    if-eqz v0, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-eqz v2, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 82
    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 83
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dH:Lcom/google/b/f/t;

    aput-object v3, v0, v2

    .line 84
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 85
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 87
    :cond_0
    return-object v1

    .line 79
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    goto :goto_0
.end method
