.class public Lcom/google/android/apps/gmm/streetview/internal/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static e:Lorg/apache/http/client/HttpClient;


# instance fields
.field a:Ljava/io/File;

.field private final b:Lcom/google/android/apps/gmm/streetview/internal/l;

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;ILcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 1
    .param p4    # Lcom/google/android/apps/gmm/map/util/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->c:Ljava/util/HashSet;

    .line 318
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/l;

    invoke-direct {v0, p3, p4}, Lcom/google/android/apps/gmm/streetview/internal/l;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    .line 319
    iput p1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->d:I

    .line 320
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->f:Ljava/lang/String;

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/g;->a()V

    .line 322
    return-void
.end method

.method private static declared-synchronized a(I)Lorg/apache/http/client/HttpClient;
    .locals 7

    .prologue
    .line 345
    const-class v1, Lcom/google/android/apps/gmm/streetview/internal/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/g;->e:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 346
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 351
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 354
    const/16 v2, 0x4e20

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 355
    const/16 v2, 0x4e20

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 356
    const/16 v2, 0x2000

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 359
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 362
    const-string v2, "Android StreetView"

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 363
    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 364
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "http"

    .line 365
    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v5

    const/16 v6, 0x50

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    .line 364
    invoke-virtual {v2, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 366
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "https"

    .line 367
    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v5

    const/16 v6, 0x1bb

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    .line 366
    invoke-virtual {v2, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 368
    new-instance v3, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v3, v0, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 372
    new-instance v2, Lorg/apache/http/conn/params/ConnPerRouteBean;

    invoke-direct {v2, p0}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v0, v2}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 375
    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/h;

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/streetview/internal/h;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    sput-object v2, Lcom/google/android/apps/gmm/streetview/internal/g;->e:Lorg/apache/http/client/HttpClient;

    .line 394
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/g;->e:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized a()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 798
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 799
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_2

    .line 800
    const-string v0, "Could not open cache directory "

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->f:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 841
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 800
    :cond_1
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 798
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 803
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 804
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 805
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 809
    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->a:Ljava/io/File;

    .line 821
    array-length v1, v2

    new-array v3, v1, [Lcom/google/android/apps/gmm/streetview/internal/k;

    move v1, v0

    .line 822
    :goto_1
    array-length v4, v2

    if-ge v1, v4, :cond_3

    .line 823
    new-instance v4, Lcom/google/android/apps/gmm/streetview/internal/k;

    aget-object v5, v2, v1

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->a:Ljava/io/File;

    invoke-direct {v6, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v6}, Lcom/google/android/apps/gmm/streetview/internal/k;-><init>(Ljava/io/File;)V

    aput-object v4, v3, v1

    .line 822
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 825
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 826
    invoke-static {v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 827
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 829
    array-length v2, v3

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_0

    aget-object v4, v3, v1

    .line 830
    iget-object v0, v4, Lcom/google/android/apps/gmm/streetview/internal/k;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 831
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    iget-object v0, v4, Lcom/google/android/apps/gmm/streetview/internal/k;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/streetview/internal/l;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/k;

    if-eqz v0, :cond_4

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/streetview/internal/k;->d:Z

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/streetview/internal/k;->b:Ljava/lang/String;

    invoke-virtual {v5, v0, v4}, Lcom/google/android/apps/gmm/streetview/internal/l;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 829
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/streetview/internal/k;)V
    .locals 2

    .prologue
    .line 500
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 502
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/l;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    :cond_0
    monitor-exit p0

    return-void

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/streetview/internal/k;J)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 646
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 647
    iget-wide v4, p1, Lcom/google/android/apps/gmm/streetview/internal/k;->c:J

    sub-long/2addr v2, v4

    cmp-long v1, v2, p2

    if-lez v1, :cond_0

    .line 648
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Expired cache file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Lcom/google/android/apps/gmm/streetview/internal/k;)V

    .line 671
    :goto_0
    return-object v0

    .line 656
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p1, Lcom/google/android/apps/gmm/streetview/internal/k;->a:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 657
    :try_start_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/k;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v1, v4

    new-array v1, v1, [B

    .line 658
    invoke-virtual {v2, v1}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 666
    if-eqz v2, :cond_1

    :try_start_2
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_1
    move-object v0, v1

    .line 667
    goto :goto_0

    .line 661
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 662
    :goto_2
    :try_start_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not find cache file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 666
    if-eqz v2, :cond_2

    :try_start_4
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 668
    :cond_2
    :goto_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Lcom/google/android/apps/gmm/streetview/internal/k;)V

    goto :goto_0

    .line 663
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 664
    :goto_4
    :try_start_5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not read cache file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 666
    if-eqz v2, :cond_3

    :try_start_6
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 668
    :cond_3
    :goto_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Lcom/google/android/apps/gmm/streetview/internal/k;)V

    goto :goto_0

    .line 666
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_6
    if-eqz v2, :cond_4

    :try_start_7
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 668
    :cond_4
    :goto_7
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Lcom/google/android/apps/gmm/streetview/internal/k;)V

    throw v0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_7

    .line 666
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 663
    :catch_6
    move-exception v1

    goto :goto_4

    .line 661
    :catch_7
    move-exception v1

    goto :goto_2
.end method

.method private b(Ljava/lang/String;Lcom/google/android/apps/gmm/streetview/internal/j;)Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 601
    const/4 v1, 0x0

    .line 603
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 604
    if-eqz p2, :cond_0

    .line 605
    :try_start_1
    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/streetview/internal/j;->a(Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    .line 607
    :cond_0
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->d:I

    invoke-static {v1}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(I)Lorg/apache/http/client/HttpClient;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 608
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_2

    .line 609
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Bad status code"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621
    :catch_0
    move-exception v1

    :goto_0
    :try_start_2
    new-instance v1, Ljava/io/IOException;

    const-string v2, "IllegalStateException"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 623
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    .line 624
    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/streetview/internal/j;->b(Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    :cond_1
    throw v0

    .line 611
    :cond_2
    :try_start_3
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 613
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 612
    invoke-static {v1, p2, v0}, Lcom/google/android/apps/gmm/streetview/internal/i;->a(Ljava/io/InputStream;Lcom/google/android/apps/gmm/streetview/internal/j;Lorg/apache/http/client/methods/AbortableHttpRequest;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 623
    return-object v0

    :catchall_1
    move-exception v0

    goto :goto_1

    .line 621
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private b([BLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 770
    .line 774
    :try_start_0
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->a:Ljava/io/File;

    invoke-direct {v3, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 775
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 776
    :try_start_2
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 777
    :try_start_3
    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 778
    if-eqz v1, :cond_0

    :try_start_4
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 780
    :cond_0
    :goto_0
    :try_start_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    new-instance v4, Lcom/google/android/apps/gmm/streetview/internal/k;

    invoke-direct {v4, v3}, Lcom/google/android/apps/gmm/streetview/internal/k;-><init>(Ljava/io/File;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/streetview/internal/k;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/l;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/k;

    if-eqz v0, :cond_1

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/streetview/internal/k;->d:Z

    :cond_1
    iget-object v0, v4, Lcom/google/android/apps/gmm/streetview/internal/k;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/gmm/streetview/internal/l;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 781
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 786
    const/4 v0, 0x0

    if-eqz v2, :cond_2

    :try_start_6
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 791
    :cond_2
    :goto_1
    return-void

    .line 781
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 784
    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v0, v3

    :goto_3
    :try_start_9
    new-instance v1, Ljava/io/IOException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Couldn\'t create cache file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 786
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_3

    :try_start_a
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 787
    :cond_3
    :goto_5
    if-eqz v3, :cond_4

    .line 788
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_4
    throw v0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_5

    .line 786
    :catchall_2
    move-exception v0

    move-object v3, v2

    goto :goto_4

    :catchall_3
    move-exception v0

    goto :goto_4

    :catchall_4
    move-exception v0

    move-object v2, v1

    goto :goto_4

    .line 784
    :catch_4
    move-exception v0

    move-object v0, v2

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v0, v3

    goto :goto_3

    .line 781
    :catchall_5
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public final a([BLjava/lang/String;)V
    .locals 2

    .prologue
    .line 751
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 752
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "persistentKey"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 755
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->a:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 756
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/streetview/internal/g;->b([BLjava/lang/String;)V

    .line 761
    :cond_2
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;J)Z
    .locals 4

    .prologue
    .line 522
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 523
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "persistentKey"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 525
    :cond_1
    const-wide/16 v0, 0x1

    cmp-long v0, p2, v0

    if-gez v0, :cond_2

    .line 526
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "shelfLife"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/streetview/internal/l;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/k;

    .line 532
    if-eqz v0, :cond_3

    iget-wide v0, v0, Lcom/google/android/apps/gmm/streetview/internal/k;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v0, v2, v0

    cmp-long v0, v0, p2

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Ljava/lang/String;Lcom/google/android/apps/gmm/streetview/internal/j;)[B
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 567
    .line 571
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/streetview/internal/g;->b(Ljava/lang/String;Lcom/google/android/apps/gmm/streetview/internal/j;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 572
    :try_start_1
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x2000

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 573
    const/16 v3, 0x2000

    new-array v3, v3, [B

    .line 575
    :goto_0
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 576
    if-lez v4, :cond_1

    .line 577
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 586
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    :try_start_2
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_2
    throw v0

    .line 581
    :cond_1
    if-eqz p2, :cond_3

    :try_start_3
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/streetview/internal/j;->a()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    .line 582
    if-eqz v1, :cond_2

    :try_start_4
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 586
    :cond_2
    :goto_3
    return-object v0

    .line 584
    :cond_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 586
    if-eqz v1, :cond_2

    :try_start_6
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_1
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/streetview/internal/j;)[B
    .locals 4

    .prologue
    .line 696
    const/4 v1, 0x0

    .line 699
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 703
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 704
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 715
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 725
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_0

    .line 726
    monitor-enter p0

    .line 727
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->c:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 729
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 730
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_0
    throw v0

    .line 705
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/streetview/internal/l;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 706
    const-wide/32 v2, 0x36ee80

    invoke-virtual {p0, p2, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/g;->b(Ljava/lang/String;J)[B

    move-result-object v0

    monitor-exit p0

    .line 730
    :goto_1
    return-object v0

    .line 713
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 714
    const/4 v1, 0x1

    .line 715
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 717
    :try_start_5
    const-string v0, "fetchToCache"

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2, v0}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 719
    :cond_3
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/streetview/internal/j;)[B

    move-result-object v0

    .line 720
    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    .line 721
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/streetview/internal/g;->b([BLjava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 725
    :cond_4
    monitor-enter p0

    .line 727
    :try_start_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->c:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 729
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 730
    monitor-exit p0

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method

.method declared-synchronized b(Ljava/lang/String;J)[B
    .locals 2

    .prologue
    .line 549
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/g;->b:Lcom/google/android/apps/gmm/streetview/internal/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/streetview/internal/l;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551
    if-nez v0, :cond_0

    .line 552
    const/4 v0, 0x0

    .line 554
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Lcom/google/android/apps/gmm/streetview/internal/k;J)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 549
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
