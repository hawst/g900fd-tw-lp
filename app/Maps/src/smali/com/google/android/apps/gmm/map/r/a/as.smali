.class public Lcom/google/android/apps/gmm/map/r/a/as;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(I)I
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x2

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x33

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Need at least 2 waypoints, but actually "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_1
    return p0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/jm;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 109
    iget v0, p2, Lcom/google/maps/g/a/jm;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/jp;->a(I)Lcom/google/maps/g/a/jp;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    :cond_0
    sget-object v3, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    if-eq v0, v3, :cond_3

    .line 110
    iget v0, p2, Lcom/google/maps/g/a/jm;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/jp;->a(I)Lcom/google/maps/g/a/jp;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    :cond_1
    sget-object v3, Lcom/google/maps/g/a/jp;->b:Lcom/google/maps/g/a/jp;

    if-eq v0, v3, :cond_3

    .line 142
    :cond_2
    :goto_0
    return-object p1

    .line 113
    :cond_3
    iget v0, p2, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_9

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p2, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    iget v0, v0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_a

    move v0, v2

    :goto_2
    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v3, :cond_b

    move v0, v2

    :goto_3
    if-nez v0, :cond_2

    .line 122
    iget-object v0, p2, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    iget-object v0, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    .line 123
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v4, p1}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 124
    iget v3, v0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v5, 0x2

    if-ne v3, v5, :cond_c

    move v3, v2

    :goto_4
    if-eqz v3, :cond_4

    .line 125
    invoke-virtual {v0}, Lcom/google/maps/g/a/je;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v3

    .line 126
    if-eqz v3, :cond_4

    .line 127
    iput-object v3, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 130
    :cond_4
    iget v3, v0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v5, 0x4

    if-ne v3, v5, :cond_d

    move v3, v2

    :goto_5
    if-eqz v3, :cond_5

    .line 131
    iget-object v0, v0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/maps/g/gy;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 134
    :cond_5
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    if-nez v0, :cond_7

    .line 135
    invoke-static {p0, p2}, Lcom/google/android/apps/gmm/map/r/a/as;->a(Landroid/content/res/Resources;Lcom/google/maps/g/a/jm;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_e

    :cond_6
    move v0, v2

    :goto_6
    if-nez v0, :cond_7

    iput-object v3, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 138
    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 139
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 140
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x16

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Waypoint refined: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " -> "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    move-object p1, v0

    .line 142
    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 113
    goto/16 :goto_1

    :cond_a
    move v0, v1

    .line 116
    goto/16 :goto_2

    :cond_b
    move v0, v1

    .line 119
    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 124
    goto/16 :goto_4

    :cond_d
    move v3, v1

    .line 130
    goto :goto_5

    :cond_e
    move v0, v1

    .line 135
    goto :goto_6
.end method

.method public static a(Landroid/content/res/Resources;Lcom/google/maps/g/a/jm;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 50
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 51
    :cond_0
    iget-object v0, p1, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    iget v0, v0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    iget-object v0, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    iget v0, v0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    iget-object v0, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    iget v0, v0, Lcom/google/maps/g/a/je;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/jh;->a(I)Lcom/google/maps/g/a/jh;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    :cond_1
    :goto_2
    sget-object v1, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    if-ne v0, v1, :cond_5

    .line 52
    sget v0, Lcom/google/android/apps/gmm/l;->fM:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    :goto_3
    return-object v0

    :cond_2
    move v0, v3

    .line 51
    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    goto :goto_2

    .line 55
    :cond_5
    iget-object v0, p1, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    .line 56
    iget v1, v0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_6

    move v1, v2

    :goto_4
    if-eqz v1, :cond_b

    .line 57
    iget-object v0, v0, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/a;->d()Lcom/google/maps/g/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/a;

    .line 58
    iget v1, v0, Lcom/google/maps/g/a/a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_7

    :goto_5
    if-eqz v2, :cond_a

    .line 59
    iget-object v1, v0, Lcom/google/maps/g/a/a;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_8

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    :cond_6
    move v1, v3

    .line 56
    goto :goto_4

    :cond_7
    move v2, v3

    .line 58
    goto :goto_5

    .line 59
    :cond_8
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    iput-object v2, v0, Lcom/google/maps/g/a/a;->c:Ljava/lang/Object;

    :cond_9
    move-object v0, v2

    goto :goto_3

    .line 60
    :cond_a
    iget-object v1, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-lez v1, :cond_10

    .line 61
    iget-object v0, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    .line 63
    :cond_b
    iget v1, v0, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_c

    move v1, v2

    :goto_6
    if-eqz v1, :cond_e

    .line 64
    iget-object v1, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    iget v1, v1, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_d

    move v1, v2

    :goto_7
    if-eqz v1, :cond_e

    .line 65
    iget-object v1, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    invoke-virtual {v1}, Lcom/google/maps/g/a/je;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    .line 66
    iget-object v0, v0, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    invoke-virtual {v0}, Lcom/google/maps/g/a/je;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_c
    move v1, v3

    .line 63
    goto :goto_6

    :cond_d
    move v1, v3

    .line 64
    goto :goto_7

    .line 67
    :cond_e
    iget v0, p1, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_f

    move v0, v2

    :goto_8
    if-eqz v0, :cond_10

    .line 68
    invoke-virtual {p1}, Lcom/google/maps/g/a/jm;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 69
    invoke-virtual {p1}, Lcom/google/maps/g/a/jm;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_f
    move v0, v3

    .line 67
    goto :goto_8

    .line 72
    :cond_10
    sget v0, Lcom/google/android/apps/gmm/l;->cV:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3
.end method
