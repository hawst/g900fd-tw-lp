.class public abstract Lcom/google/android/apps/gmm/terms/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/terms/b/a;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/terms/b/b;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/terms/b/b;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/gmm/terms/a/c;->a:Lcom/google/android/apps/gmm/terms/b/b;

    .line 18
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/terms/a/c;->b:Z

    .line 19
    return-void
.end method


# virtual methods
.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/terms/a/c;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/apps/gmm/l;->fS:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/apps/gmm/l;->ct:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/google/android/apps/gmm/l;->bP:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/c;->a:Lcom/google/android/apps/gmm/terms/b/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/terms/b/b;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 46
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/a/c;->a:Lcom/google/android/apps/gmm/terms/b/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/terms/b/b;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 54
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/terms/a/c;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/terms/a/c;->b:Z

    .line 60
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 61
    const/4 v0, 0x0

    return-object v0

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
