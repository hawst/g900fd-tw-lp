.class public Lcom/google/android/apps/gmm/cardui/h/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/d;


# instance fields
.field private a:Landroid/app/DialogFragment;

.field private b:Landroid/app/Activity;

.field private c:Lcom/google/o/h/a/mv;


# direct methods
.method public constructor <init>(Landroid/app/DialogFragment;Landroid/app/Activity;Lcom/google/o/h/a/mv;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/h;->a:Landroid/app/DialogFragment;

    .line 26
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/h;->b:Landroid/app/Activity;

    .line 27
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/h/h;->c:Lcom/google/o/h/a/mv;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/h;->a:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 33
    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/h;->b:Landroid/app/Activity;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    iget-object v5, p0, Lcom/google/android/apps/gmm/cardui/h/h;->c:Lcom/google/o/h/a/mv;

    .line 34
    iget-object v0, v5, Lcom/google/o/h/a/mv;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 33
    invoke-virtual {v2, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 35
    const/4 v0, 0x0

    return-object v0

    .line 34
    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v5, Lcom/google/o/h/a/mv;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/h;->a:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 47
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->bj:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 48
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->bk:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 55
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->bl:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 62
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method
