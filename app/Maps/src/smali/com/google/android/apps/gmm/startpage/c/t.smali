.class public Lcom/google/android/apps/gmm/startpage/c/t;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/startpage/e/g;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 22

    .prologue
    .line 38
    const/4 v2, 0x4

    new-array v3, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 39
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 40
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x2

    .line 42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->a()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-static {v2, v5, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v4, 0x3

    const/4 v2, 0x5

    new-array v5, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 45
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x1

    const/4 v6, -0x1

    .line 46
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x2

    const/4 v6, 0x1

    .line 47
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->R:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x3

    .line 48
    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x4

    const-wide/high16 v8, 0x403a000000000000L    # 26.0

    .line 51
    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_1

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v8, 0x403a000000000000L    # 26.0

    .line 52
    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_2

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v10, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const/4 v2, 0x6

    new-array v8, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v9, 0x0

    .line 54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->d()Lcom/google/android/libraries/curvular/cf;

    move-result-object v11

    const/4 v2, 0x0

    if-eqz v11, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v11

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v11, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x1

    .line 55
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->c()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v11, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v11, 0x0

    .line 58
    sget v12, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bU:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v9, v11

    const/4 v11, 0x1

    sget v12, Lcom/google/android/apps/gmm/f;->fy:I

    .line 59
    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v9, v11

    .line 57
    new-instance v11, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v11, v9}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v9, "android.widget.ImageView"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    invoke-virtual {v11, v9}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v9, 0x3

    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->b()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v11, 0x0

    new-array v11, v11, [Lcom/google/android/libraries/curvular/cu;

    .line 61
    invoke-static {v2, v11}, Lcom/google/android/apps/gmm/base/f/w;->a(Ljava/lang/CharSequence;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cu;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x4

    const/4 v2, 0x1

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    .line 65
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->c()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/16 v14, 0x8

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    .line 64
    invoke-static {v11}, Lcom/google/android/apps/gmm/base/f/w;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cu;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x5

    .line 68
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->e()Ljava/lang/Boolean;

    move-result-object v11

    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->f()Lcom/google/android/libraries/curvular/cf;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v2, 0x3

    new-array v14, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v15, 0x0

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/startpage/e/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/startpage/e/g;->e()Ljava/lang/Boolean;

    move-result-object v2

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    const/16 v17, 0x8

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    sget-object v18, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v2, v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v14, v15

    const/4 v15, 0x1

    const-wide/high16 v16, 0x403a000000000000L    # 26.0

    .line 72
    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v14, v15

    const/4 v15, 0x2

    const-wide/high16 v16, 0x403a000000000000L    # 26.0

    .line 73
    new-instance v18, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v14, v15

    .line 67
    invoke-static {v11, v12, v13, v14}, Lcom/google/android/apps/gmm/base/f/bx;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v8, v9

    .line 50
    invoke-static {v7, v10, v8}, Lcom/google/android/apps/gmm/base/f/w;->a(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v5, v6

    .line 44
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.ScrollView"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v3, v4

    .line 38
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.FrameLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    .line 51
    :cond_1
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 52
    :cond_2
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 72
    :cond_3
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    .line 73
    :cond_4
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v20

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3
.end method
