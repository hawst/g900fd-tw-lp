.class public Lcom/google/android/apps/gmm/navigation/base/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/b/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/gmm/x/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/x/a;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/base/k;->a:Landroid/content/Context;

    .line 20
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/x/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/base/k;->b:Lcom/google/android/apps/gmm/x/a;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/k;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/b/d;)V
    .locals 7

    .prologue
    .line 25
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/k;->b:Lcom/google/android/apps/gmm/x/a;

    .line 26
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    const-string v4, "mode"

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v3, "nav://params"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "m"

    iget-object v5, p1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/b/e;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v4, p1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    sget-object v5, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v4, v5, :cond_2

    const-string v4, "d"

    iget-object v5, p1, Lcom/google/android/apps/gmm/navigation/b/d;->b:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/gmm/x/a;->a(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v2, "idx"

    iget v4, p1, Lcom/google/android/apps/gmm/navigation/b/d;->c:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    :goto_0
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/base/k;->a:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/k;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 28
    return-void

    .line 26
    :cond_2
    iget-object v4, p1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    sget-object v5, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v4, v5, :cond_1

    const-string v4, "fn"

    iget-object v5, p1, Lcom/google/android/apps/gmm/navigation/b/d;->d:Lcom/google/android/apps/gmm/navigation/b/a;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/gmm/x/a;->a(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method
