.class public Lcom/google/android/apps/gmm/navigation/a/az;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/android/apps/gmm/map/r/a/am;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/apps/gmm/map/r/a/ag;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/am;)V
    .locals 1

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    .line 313
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/a/am;
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/am;

    .line 323
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    goto :goto_0

    .line 324
    :cond_0
    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/a/az;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/az;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/az;->a()Lcom/google/android/apps/gmm/map/r/a/am;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 337
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
