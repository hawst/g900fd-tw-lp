.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field public static final h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field public static final i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;


# instance fields
.field public final a:Landroid/graphics/Paint;

.field final b:Landroid/graphics/Paint;

.field final c:Landroid/graphics/Path;

.field public final d:Lcom/google/android/apps/gmm/v/bz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/bz",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;",
            ">;"
        }
    .end annotation
.end field

.field public final e:F

.field public final f:F

.field private final j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 150
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;F)V
    .locals 2

    .prologue
    .line 163
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;-><init>(Lcom/google/android/apps/gmm/v/ad;FLandroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 164
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/v/ad;FLandroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/v/bz;

    const/16 v1, 0x800

    const/16 v2, 0x80

    const/4 v3, 0x7

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/gmm/v/bz;-><init>(Lcom/google/android/apps/gmm/v/ad;III)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->d:Lcom/google/android/apps/gmm/v/bz;

    .line 170
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 173
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->b:Landroid/graphics/Paint;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 176
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->c:Landroid/graphics/Path;

    .line 179
    const v0, 0x3fb33333    # 1.4f

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->e:F

    .line 182
    const v0, 0x3f8020c5    # 1.001f

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const v0, 0x3f83d70a    # 1.03f

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->f:F

    .line 183
    return-void

    .line 182
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FIII)Lcom/google/android/apps/gmm/v/by;
    .locals 8

    .prologue
    .line 323
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FIII)V

    .line 325
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->d:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v1

    .line 326
    if-nez v1, :cond_5

    .line 330
    if-nez p6, :cond_0

    if-eqz p7, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 332
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput-object p1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput-object p2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput p4, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->d:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput-object p3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput p5, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput p6, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->f:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    iput p7, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->g:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    if-eqz v6, :cond_3

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->e:F

    :goto_1
    iput v1, v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->h:F

    const/high16 v7, 0x3f800000    # 1.0f

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FZF)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v1, v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    const/4 v3, 0x1

    aget v1, v1, v3

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    const/4 v1, 0x0

    if-lez v2, :cond_4

    if-lez v3, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->d:Lcom/google/android/apps/gmm/v/bz;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;IILcom/google/android/apps/gmm/v/cc;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_1

    const-string v0, "TextGenerator"

    const-string v1, "Failed to generate texture for: text: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x26

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " width: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 336
    :cond_1
    :goto_3
    return-object v0

    .line 330
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 332
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 443
    .line 444
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-ne p1, v0, :cond_f

    move v0, v1

    .line 447
    :goto_0
    if-eqz p2, :cond_2

    .line 448
    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_6

    move v2, v1

    :goto_1
    if-nez v2, :cond_0

    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_7

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 452
    :cond_1
    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_8

    move v2, v1

    :goto_3
    if-eqz v2, :cond_2

    .line 453
    or-int/lit8 v0, v0, 0x2

    .line 456
    :cond_2
    const/4 v2, 0x0

    .line 457
    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-ne p1, v4, :cond_a

    .line 460
    if-eqz p2, :cond_3

    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_9

    :goto_4
    if-nez v1, :cond_4

    .line 461
    :cond_3
    or-int/lit8 v0, v0, 0x1

    .line 463
    :cond_4
    const-string v1, "sans-serif-condensed"

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 472
    :goto_5
    if-nez v0, :cond_5

    .line 473
    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 475
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 478
    return-void

    :cond_6
    move v2, v3

    .line 448
    goto :goto_1

    :cond_7
    move v2, v3

    goto :goto_2

    :cond_8
    move v2, v3

    .line 452
    goto :goto_3

    :cond_9
    move v1, v3

    .line 460
    goto :goto_4

    .line 464
    :cond_a
    if-eqz p2, :cond_c

    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_b

    move v4, v1

    :goto_6
    if-eqz v4, :cond_c

    .line 465
    const-string v1, "sans-serif-condensed"

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_5

    :cond_b
    move v4, v3

    .line 464
    goto :goto_6

    .line 466
    :cond_c
    if-eqz p2, :cond_e

    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_d

    :goto_7
    if-eqz v1, :cond_e

    .line 467
    const-string v1, "sans-serif-light"

    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_5

    :cond_d
    move v1, v3

    .line 466
    goto :goto_7

    :cond_e
    move v1, v0

    move-object v0, v2

    goto :goto_5

    :cond_f
    move v0, v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FZF)[F
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 229
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 235
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v1, v3

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-float v3, v4

    .line 236
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v1, v4

    .line 237
    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float v0, v4, v0

    .line 240
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v4, p6, v4

    mul-float/2addr v4, v3

    .line 243
    if-eqz p5, :cond_0

    const/4 v5, 0x0

    cmpl-float v5, v2, v5

    if-lez v5, :cond_0

    .line 244
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->e:F

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 245
    mul-int/lit8 v6, v5, 0x2

    int-to-float v6, v6

    add-float/2addr v2, v6

    .line 246
    int-to-float v6, v5

    add-float/2addr v1, v6

    .line 247
    int-to-float v5, v5

    add-float/2addr v0, v5

    .line 249
    :cond_0
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->f:F

    mul-float/2addr v2, v5

    .line 252
    add-float v5, v1, v0

    add-float/2addr v3, v5

    .line 260
    div-float v5, v4, v8

    sub-float/2addr v1, v5

    .line 261
    div-float/2addr v4, v8

    sub-float/2addr v0, v4

    .line 263
    const/4 v4, 0x4

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v2, 0x1

    aput v3, v4, v2

    const/4 v2, 0x2

    aput v1, v4, v2

    const/4 v1, 0x3

    aput v0, v4, v1

    return-object v4
.end method
