.class public Lcom/google/android/apps/gmm/map/t/al;
.super Lcom/google/android/apps/gmm/v/d;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/t/a/a;

.field public i:Lcom/google/android/apps/gmm/v/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/d;-><init>(Lcom/google/android/apps/gmm/v/c;)V

    .line 34
    check-cast p1, Lcom/google/android/apps/gmm/map/t/a/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/al;->a:Lcom/google/android/apps/gmm/map/t/a/a;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->a:Lcom/google/android/apps/gmm/map/t/a/a;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GmmScaleAnimationBehavior expects a GmmScaleAnimation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    const-wide/16 v0, 0x12c

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/v/c;->setDuration(J)V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->a:Lcom/google/android/apps/gmm/map/t/a/a;

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/a/a;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/i;)V
    .locals 1

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->a:Lcom/google/android/apps/gmm/map/t/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/a/a;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/al;->i:Lcom/google/android/apps/gmm/v/g;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->start()V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/al;->d()V

    .line 59
    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->a:Lcom/google/android/apps/gmm/map/t/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/a/a;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->a()V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/al;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->a:Lcom/google/android/apps/gmm/map/t/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/a/a;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->i:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/al;->i:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :cond_0
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
