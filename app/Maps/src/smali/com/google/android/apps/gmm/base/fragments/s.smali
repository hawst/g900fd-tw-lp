.class Lcom/google/android/apps/gmm/base/fragments/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/accounts/Account;

.field final synthetic c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/fragments/s;->c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/fragments/s;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/gmm/base/fragments/s;->b:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 245
    const/4 v7, 0x0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/s;->c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 248
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/s;->c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/s;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "weblogin:service="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&continue="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 249
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/s;->b:Landroid/accounts/Account;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/s;->c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    .line 250
    iget-object v4, v4, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 251
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/s;->c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 253
    const/4 v0, 0x1

    .line 261
    :goto_0
    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/s;->c:Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/s;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Ljava/lang/String;)V

    .line 265
    :cond_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 255
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Exception during authentication flow: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v7

    .line 260
    goto :goto_0

    .line 256
    :catch_1
    move-exception v0

    .line 257
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Authentication cancelled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v7

    .line 260
    goto :goto_0

    .line 258
    :catch_2
    move-exception v0

    .line 259
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Exception during authentication flow: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v7

    goto :goto_0
.end method
