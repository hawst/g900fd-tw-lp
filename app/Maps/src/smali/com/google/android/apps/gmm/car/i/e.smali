.class Lcom/google/android/apps/gmm/car/i/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/car/support/q;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/i/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/i/a;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/car/i/a;->a(Lcom/google/android/apps/gmm/car/i/a;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 174
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/a;->e()V

    .line 180
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    iput-object p1, v0, Lcom/google/android/apps/gmm/car/i/a;->e:Ljava/lang/String;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/a;->c:Lcom/google/android/apps/gmm/car/i/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/i/a;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/e;->a:Lcom/google/android/apps/gmm/car/i/a;

    iget-object v5, v2, Lcom/google/android/apps/gmm/car/i/a;->g:Lcom/google/android/apps/gmm/car/i/k;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    if-nez v5, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/z;->a()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/z;->c:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v2

    if-nez v2, :cond_3

    move-object v3, v4

    :goto_1
    if-nez v3, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/car/i/z;->a:Ljava/lang/String;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/car/i/k;->a()V

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v3

    goto :goto_1

    :cond_4
    new-instance v6, Lcom/google/android/apps/gmm/suggest/d/g;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/suggest/d/g;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1, v2, v7}, Ljava/lang/String;->codePointCount(II)I

    move-result v7

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/z;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    new-instance v2, Lcom/google/android/apps/gmm/suggest/e/a;

    invoke-direct {v2, v1, v7, v8, v9}, Lcom/google/android/apps/gmm/suggest/e/a;-><init>(Ljava/lang/String;IJ)V

    invoke-virtual {v6, v2}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/e/a;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/i/z;->f:Ljava/lang/String;

    iput-object v5, v0, Lcom/google/android/apps/gmm/car/i/z;->g:Lcom/google/android/apps/gmm/car/i/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/z;->e:Lcom/google/android/apps/gmm/suggest/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->b:Lcom/google/android/apps/gmm/suggest/e/c;

    move-object v5, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/gmm/suggest/a/b;->a(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/suggest/e/b;Lcom/google/android/apps/gmm/suggest/d/g;)Lcom/google/android/apps/gmm/shared/net/af;

    goto :goto_0
.end method
