.class public Lcom/google/android/apps/gmm/v/ci;
.super Lcom/google/android/apps/gmm/v/ai;
.source "PG"


# instance fields
.field private a:Z

.field f:Lcom/google/android/apps/gmm/v/ar;

.field g:[I

.field public volatile h:I

.field public volatile i:I

.field public volatile j:Z

.field public volatile k:I

.field public volatile l:I

.field public volatile m:Z

.field public final t:I

.field public u:Lcom/google/android/apps/gmm/v/ax;

.field public v:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ar;I)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;IZ)V

    .line 118
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ar;IZ)V
    .locals 7

    .prologue
    const v6, 0x812f

    const/16 v3, 0x2901

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported texture unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    .line 26
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    .line 64
    const/16 v0, 0x2600

    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    .line 69
    const/16 v0, 0x2601

    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    .line 74
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 79
    iput v3, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    .line 84
    iput v3, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    .line 89
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/v/ci;->m:Z

    .line 99
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/v/ci;->a:Z

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    .line 106
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v4, p0, Lcom/google/android/apps/gmm/v/ci;->v:J

    .line 131
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    .line 132
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/v/ci;->a:Z

    .line 134
    if-ltz p2, :cond_0

    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    move v0, v1

    :goto_1
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 130
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->c:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->d:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->e:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->f:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->g:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->h:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->i:Lcom/google/android/apps/gmm/v/aj;

    goto :goto_0

    :cond_0
    move v0, v2

    .line 134
    goto :goto_1

    .line 136
    :cond_1
    const v0, 0x84c0

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->t:I

    .line 139
    if-eqz p1, :cond_3

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/v/ar;->g:Z

    if-nez v0, :cond_3

    .line 140
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    iput v6, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    iput v6, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/ci;->m:Z

    .line 142
    :cond_3
    return-void

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a(FFFF)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 426
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_0

    .line 427
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x5

    aput v2, v0, v1

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/16 v2, 0x8

    aput v1, v0, v2

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x0

    aput p3, v0, v1

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x4

    aput p4, v0, v1

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x6

    aput p1, v0, v1

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v1, 0x7

    aput p2, v0, v1

    .line 439
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_0

    .line 287
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 289
    :cond_0
    iput p1, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    .line 305
    iput p2, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->m:Z

    .line 307
    return-void
.end method

.method a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xde1

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    if-nez v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    aget v1, v1, v4

    iget v2, p0, Lcom/google/android/apps/gmm/v/ci;->t:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/v/aq;->a(II)V

    .line 258
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->m:Z

    if-eqz v0, :cond_2

    .line 260
    const/16 v0, 0x2802

    iget v1, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 261
    const/16 v0, 0x2803

    iget v1, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 262
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/v/ci;->m:Z

    .line 265
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    if-eqz v0, :cond_0

    .line 267
    const/16 v0, 0x2801

    iget v1, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 268
    const/16 v0, 0x2800

    iget v1, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 269
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ar;ZII)V
    .locals 2

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called BEFORE set live"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/v/ci;->a:Z

    .line 160
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    iput p3, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    iput p4, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->m:Z

    .line 162
    return-void
.end method

.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/16 v4, 0xde1

    const/4 v3, 0x0

    .line 212
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    move-result v0

    .line 214
    if-eqz v0, :cond_0

    .line 215
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-eqz v1, :cond_2

    .line 216
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    invoke-static {v2, v1, v3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 217
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    aget v2, v2, v3

    iget v3, p0, Lcom/google/android/apps/gmm/v/ci;->t:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/v/aq;->a(II)V

    .line 218
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    if-nez v1, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v0

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/gmm/v/ar;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 223
    const/16 v1, 0x2801

    iget v2, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 224
    const/16 v1, 0x2800

    iget v2, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 226
    const/16 v1, 0x2802

    iget v2, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 227
    const/16 v1, 0x2803

    iget v2, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    invoke-static {v4, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 228
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/ci;->a:Z

    if-eqz v1, :cond_0

    .line 229
    invoke-static {v4}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    goto :goto_0

    .line 232
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    if-eqz v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/gmm/v/ar;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 236
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    aget v4, v4, v3

    iget v5, p0, Lcom/google/android/apps/gmm/v/ci;->t:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/aq;->g:[I

    const v6, 0x84c0

    sub-int/2addr v5, v6

    aget v1, v1, v5

    if-ne v1, v4, :cond_4

    move v1, v2

    :goto_1
    if-eqz v1, :cond_3

    .line 237
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v4, p0, Lcom/google/android/apps/gmm/v/ci;->t:I

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/gmm/v/aq;->a(II)V

    .line 240
    :cond_3
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    invoke-static {v2, v1, v3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ci;->g:[I

    aput v3, v1, v3

    goto :goto_0

    :cond_4
    move v1, v3

    .line 236
    goto :goto_1
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_0

    .line 318
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 320
    :cond_0
    iput p1, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    .line 334
    iput p2, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 336
    return-void
.end method

.method final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    if-nez v0, :cond_0

    .line 278
    :cond_0
    return-void
.end method

.method final b()Z
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ci;->f:Lcom/google/android/apps/gmm/v/ar;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ar;->a:Lcom/google/android/apps/gmm/v/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/as;->c()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ar;->b:Lcom/google/android/apps/gmm/v/at;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ar;->b:Lcom/google/android/apps/gmm/v/at;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/at;->c()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
