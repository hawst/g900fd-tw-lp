.class Lcom/google/android/apps/gmm/map/internal/c/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/cp;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/c/aj;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/aj;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 224
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->a:Lcom/google/android/apps/gmm/map/internal/c/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    .line 229
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->c:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/m;
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->a:Lcom/google/android/apps/gmm/map/internal/c/aj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->c:I

    .line 255
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    .line 260
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 233
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->a:Lcom/google/android/apps/gmm/map/internal/c/aj;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->a:Lcom/google/android/apps/gmm/map/internal/c/aj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ak;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 244
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
