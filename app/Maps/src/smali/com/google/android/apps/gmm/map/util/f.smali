.class public Lcom/google/android/apps/gmm/map/util/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[B

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 30
    new-array v0, p1, [B

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/util/f;-><init>([B)V

    .line 31
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/f;->c:Z

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B

    .line 38
    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    .line 39
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/f;->c:Z

    const-string v1, "GrowableByteArray has not been released"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/f;->c:Z

    .line 67
    iput p1, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I

    .line 68
    iget v0, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 69
    iget v0, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a()[B
    .locals 2

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/f;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "GrowableByteArray has been released"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 5

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B

    array-length v0, v0

    if-lt v0, p1, :cond_0

    .line 85
    iput p1, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :goto_0
    monitor-exit p0

    return-void

    .line 88
    :cond_0
    :try_start_1
    new-array v1, p1, [B

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget v0, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I

    if-ge v0, p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I

    :goto_1
    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    iput p1, p0, Lcom/google/android/apps/gmm/map/util/f;->b:I

    .line 91
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/util/f;->a:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, p1

    .line 89
    goto :goto_1
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 76
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/util/f;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
