.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;
.super Lcom/google/android/apps/gmm/map/legacy/a/c/b/q;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/q",
        "<",
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 1

    .prologue
    .line 662
    const-string v0, "LineLabels"

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/q;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    .line 663
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/internal/c/m;Ljava/lang/Integer;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;
    .locals 19

    .prologue
    .line 699
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/m;->g()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v6

    .line 700
    const/4 v8, 0x0

    .line 702
    const/4 v9, 0x0

    .line 703
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ae;

    if-eqz v1, :cond_0

    move-object/from16 v1, p2

    .line 704
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ae;

    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/c/ae;->m:Z

    if-eqz v2, :cond_5

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/ae;->a:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_5

    const/4 v1, 0x1

    move v2, v1

    :goto_1
    move-object/from16 v1, p2

    .line 705
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ae;

    iget v9, v1, Lcom/google/android/apps/gmm/map/internal/c/ad;->h:F

    move v8, v2

    .line 707
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    move-object/from16 v0, p5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 708
    move-object/from16 v0, p5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 709
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_7

    .line 710
    iget-object v6, v1, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 711
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/aa;->e:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 717
    :cond_1
    move-object/from16 v0, p5

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    .line 718
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 719
    const/4 v5, 0x0

    .line 722
    :cond_2
    const/4 v7, 0x0

    .line 723
    const/4 v1, 0x0

    move v2, v1

    :goto_4
    move-object/from16 v0, p5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 724
    move-object/from16 v0, p5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 725
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_5
    if-eqz v3, :cond_9

    move-object v7, v1

    .line 730
    :cond_3
    if-nez v5, :cond_a

    if-nez v7, :cond_a

    .line 731
    const/4 v1, 0x0

    .line 761
    :goto_6
    return-object v1

    .line 704
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    .line 709
    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    .line 707
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 725
    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    .line 723
    :cond_9
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 734
    :cond_a
    const/4 v1, 0x0

    .line 735
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ae;

    if-eqz v2, :cond_b

    move-object/from16 v1, p2

    .line 736
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/ae;->l:F

    .line 738
    :cond_b
    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(Lcom/google/android/apps/gmm/map/o/ak;F)F

    move-result v11

    .line 740
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/o/au;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 741
    const/high16 v1, 0x3f000000    # 0.5f

    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FLcom/google/android/apps/gmm/map/b/a/y;)I

    move-result v14

    .line 742
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;->b()Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;

    .line 752
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/m;->i()I

    move-result v10

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v12, p6

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    move-object/from16 v18, p10

    .line 743
    invoke-virtual/range {v1 .. v18}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;->a(Lcom/google/android/apps/gmm/map/internal/c/m;Ljava/lang/Integer;Lcom/google/android/apps/gmm/map/o/ak;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/aa;ZFIFLcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;ILcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    goto :goto_6
.end method

.method protected final synthetic a()Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
    .locals 1

    .prologue
    .line 660
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;-><init>()V

    return-object v0
.end method
