.class public Lcom/google/android/apps/gmm/car/bi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Ljava/lang/Runnable;

.field private f:Lcom/google/android/apps/gmm/car/bj;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/bj;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/bi;->f:Lcom/google/android/apps/gmm/car/bj;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/car/bi;->a:Z

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 51
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 60
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/car/bi;->a:Z

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bi;->e:Ljava/lang/Runnable;

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 63
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 70
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 71
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 73
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 150
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/car/bi;->d:Z

    .line 151
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->d:Z

    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->d:Z

    if-ne v1, v0, :cond_2

    .line 167
    :cond_0
    :goto_1
    return-void

    .line 151
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 157
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bi;->d:Z

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bi;->f:Lcom/google/android/apps/gmm/car/bj;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/bj;->a()V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bi;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bi;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bi;->e:Ljava/lang/Runnable;

    goto :goto_1

    .line 165
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bi;->f:Lcom/google/android/apps/gmm/car/bj;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/bj;->b()V

    goto :goto_1
.end method
