.class public Lcom/google/android/apps/gmm/navigation/commonui/c/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/c/l;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/commonui/d;

.field public b:Z

.field c:Lcom/google/android/d/h;

.field private e:Lcom/google/android/apps/gmm/base/a;

.field private f:Lcom/google/android/apps/gmm/base/l/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/base/a;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c:Lcom/google/android/d/h;

    .line 40
    const-string v0, "host"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/commonui/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->a:Lcom/google/android/apps/gmm/navigation/commonui/d;

    .line 41
    const-string v0, "gmmEnvironment"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/base/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->e:Lcom/google/android/apps/gmm/base/a;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/l/j;
    .locals 10

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->f:Lcom/google/android/apps/gmm/base/l/j;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/n;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/base/l/n;->a:Lcom/google/android/apps/gmm/base/l/n;

    sget-object v4, Lcom/google/android/apps/gmm/base/l/k;->b:Lcom/google/android/apps/gmm/base/l/k;

    sget v5, Lcom/google/android/apps/gmm/f;->en:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/apps/gmm/l;->ix:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v1, Lcom/google/b/f/t;->bP:Lcom/google/b/f/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    sget v9, Lcom/google/android/apps/gmm/g;->bP:I

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/navigation/commonui/c/n;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/c/m;Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/n;Lcom/google/android/apps/gmm/base/l/k;ILjava/lang/String;Lcom/google/android/apps/gmm/z/b/l;ZI)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->f:Lcom/google/android/apps/gmm/base/l/j;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->f:Lcom/google/android/apps/gmm/base/l/j;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/search/a/a;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/a/a;->a:Lcom/google/android/d/f;

    iget v0, v0, Lcom/google/android/d/f;->f:I

    invoke-static {v0}, Lcom/google/android/d/h;->a(I)Lcom/google/android/d/h;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c:Lcom/google/android/d/h;

    .line 92
    sget-object v0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c:Lcom/google/android/d/h;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "new audio state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c()V

    .line 94
    return-void
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->e:Lcom/google/android/apps/gmm/base/a;

    .line 47
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->e:Lcom/google/android/apps/gmm/base/a;

    .line 50
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/aa/a/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 48
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/apps/gmm/navigation/commonui/c/o;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c:Lcom/google/android/d/h;

    invoke-virtual {v1}, Lcom/google/android/d/h;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->e:Lcom/google/android/apps/gmm/base/l/k;

    .line 112
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->a()Lcom/google/android/apps/gmm/base/l/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/l/j;->a(Lcom/google/android/apps/gmm/base/l/k;)V

    .line 113
    return-void

    .line 102
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->f:Lcom/google/android/apps/gmm/base/l/k;

    goto :goto_0

    .line 105
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->c:Lcom/google/android/apps/gmm/base/l/k;

    goto :goto_0

    .line 109
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->b:Lcom/google/android/apps/gmm/base/l/k;

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
