.class public Lcom/google/android/apps/gmm/map/o/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;
.implements Lcom/google/android/apps/gmm/map/o/p;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/o/at;

.field b:Lcom/google/android/apps/gmm/map/o/y;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/v;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/map/o/x;

.field private final e:Lcom/google/android/apps/gmm/map/o/x;

.field private final f:Lcom/google/android/apps/gmm/map/o/am;

.field private final g:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/map/o/v;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/ak;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/android/apps/gmm/map/util/a/m;

.field private final j:Lcom/google/android/apps/gmm/map/c/a;

.field private final k:Lcom/google/android/apps/gmm/map/q/a;

.field private final l:Landroid/content/res/Resources;

.field private final m:Lcom/google/android/apps/gmm/v/b/a;

.field private volatile n:Z

.field private o:Z

.field private p:Lcom/google/android/apps/gmm/v/ad;

.field private q:Lcom/google/android/apps/gmm/map/f/o;

.field private r:Lcom/google/android/apps/gmm/map/o/u;

.field private s:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/q/a;Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/map/o/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/o/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    .line 98
    new-instance v0, Lcom/google/android/apps/gmm/map/o/am;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->f:Lcom/google/android/apps/gmm/map/o/am;

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/map/o/r;

    const/16 v1, 0xa

    const-string v2, "LabelSourceOp"

    invoke-direct {v0, p0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/o/r;-><init>(Lcom/google/android/apps/gmm/map/o/q;ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/map/o/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/o/s;-><init>(Lcom/google/android/apps/gmm/map/o/q;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->i:Lcom/google/android/apps/gmm/map/util/a/m;

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/q;->n:Z

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/q;->s:Z

    .line 181
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/q;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 182
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/q;->k:Lcom/google/android/apps/gmm/map/q/a;

    .line 183
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/q;->l:Landroid/content/res/Resources;

    .line 185
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/o/q;->m:Lcom/google/android/apps/gmm/v/b/a;

    .line 194
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 304
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 345
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 308
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 309
    goto :goto_0

    .line 314
    :cond_2
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 315
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 316
    float-to-int v4, v2

    float-to-int v5, v3

    if-ne v4, v5, :cond_0

    .line 320
    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 321
    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    .line 326
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    sub-float/2addr v2, v3

    .line 325
    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/s;->c(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 327
    const/high16 v3, 0x40400000    # 3.0f

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    .line 331
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 332
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    .line 336
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/x;->d:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/x;->e:F

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(FIF)F

    move-result v2

    .line 339
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    mul-float/2addr v2, v2

    div-float v2, v3, v2

    .line 341
    const v3, 0x3c23d70b    # 0.010000001f

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    move v0, v1

    .line 345
    goto :goto_0

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 232
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/y;->b()V

    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/y;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 233
    :cond_0
    monitor-enter p0

    .line 234
    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/q;->s:Z

    if-nez v1, :cond_1

    .line 235
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 255
    :goto_0
    return-void

    .line 232
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 238
    :cond_1
    :try_start_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/ak;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o/ak;->b(Lcom/google/android/apps/gmm/map/o/p;)V

    .line 238
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->i:Lcom/google/android/apps/gmm/map/util/a/m;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 244
    :try_start_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->p:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->r:Lcom/google/android/apps/gmm/map/o/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->r:Lcom/google/android/apps/gmm/map/o/u;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/o/o;->a(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/at;->n:Lcom/google/android/apps/gmm/map/o/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/ab;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/av;->c()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/at;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bz;->b()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->d:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bz;->b()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/z;->d:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bz;->b()V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->p:Lcom/google/android/apps/gmm/v/ad;

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->q:Lcom/google/android/apps/gmm/map/f/o;

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/q;->s:Z

    .line 255
    monitor-exit p0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 243
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 2

    .prologue
    .line 354
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/x;->c:I

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/x;->d:I

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/x;->e:F

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/o;->e:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/x;->b:I

    .line 359
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 360
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/q;->n:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/o/q;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 362
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :cond_0
    monitor-exit p0

    return-void

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 2

    .prologue
    .line 518
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 519
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V

    .line 521
    :cond_1
    return-void

    .line 518
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/o/ag;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/o/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/x;->h:Lcom/google/android/apps/gmm/map/o/ag;

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    monitor-exit p0

    return-void

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/o/ak;)V
    .locals 3

    .prologue
    .line 422
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/w;->a:Lcom/google/android/apps/gmm/map/o/w;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/v;->b:Lcom/google/android/apps/gmm/map/o/w;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    monitor-exit p0

    return-void

    .line 422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/ak;)V
    .locals 3

    .prologue
    .line 441
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 442
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 443
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/w;->b:Lcom/google/android/apps/gmm/map/o/w;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/v;->b:Lcom/google/android/apps/gmm/map/o/w;

    .line 442
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    :cond_0
    if-eqz p2, :cond_1

    .line 447
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/w;->a:Lcom/google/android/apps/gmm/map/o/w;

    iput-object p2, v0, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/v;->b:Lcom/google/android/apps/gmm/map/o/w;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    monitor-exit p0

    return-void

    .line 441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/o/ap;)V
    .locals 1

    .prologue
    .line 376
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/x;->f:Lcom/google/android/apps/gmm/map/o/ap;

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    monitor-exit p0

    return-void

    .line 376
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 1

    .prologue
    .line 387
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/x;->g:Lcom/google/android/apps/gmm/map/t/b;

    .line 388
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    monitor-exit p0

    return-void

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 8

    .prologue
    .line 198
    new-instance v7, Lcom/google/android/apps/gmm/map/f/o;

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->LABELING_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v7, p2, v0}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/o/at;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/q;->j:Lcom/google/android/apps/gmm/map/c/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/q;->k:Lcom/google/android/apps/gmm/map/q/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/q;->l:Landroid/content/res/Resources;

    move-object v1, p2

    move-object v2, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/o/at;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/q/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    new-instance v1, Lcom/google/android/apps/gmm/map/o/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/map/o/y;-><init>(Lcom/google/android/apps/gmm/map/o/q;Lcom/google/android/apps/gmm/map/c/a;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->a()V

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/q;->p:Lcom/google/android/apps/gmm/v/ad;

    iput-object v7, p0, Lcom/google/android/apps/gmm/map/o/q;->q:Lcom/google/android/apps/gmm/map/f/o;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v2, p2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/gmm/map/o/x;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v2, p2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/gmm/map/o/x;->d:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget v2, p2, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iput v2, v0, Lcom/google/android/apps/gmm/map/o/x;->e:F

    new-instance v0, Lcom/google/android/apps/gmm/map/o/u;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/map/o/u;-><init>(Lcom/google/android/apps/gmm/map/o/q;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->r:Lcom/google/android/apps/gmm/map/o/u;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->r:Lcom/google/android/apps/gmm/map/o/u;

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->i:Lcom/google/android/apps/gmm/map/util/a/m;

    const-string v3, "LabelController"

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/y;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/q;->s:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 416
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/o/q;->n:Z

    .line 417
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V

    .line 418
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/y;->c:Lcom/google/android/apps/gmm/map/o/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/y;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/y;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :cond_0
    monitor-exit p0

    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/o/ak;)V
    .locals 3

    .prologue
    .line 428
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/w;->b:Lcom/google/android/apps/gmm/map/o/w;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/v;->b:Lcom/google/android/apps/gmm/map/o/w;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    monitor-exit p0

    return-void

    .line 428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/gmm/map/o/ak;)V
    .locals 3

    .prologue
    .line 434
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/o/w;->c:Lcom/google/android/apps/gmm/map/o/w;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/v;->b:Lcom/google/android/apps/gmm/map/o/w;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/q;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    monitor-exit p0

    return-void

    .line 434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 293
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/o/q;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 297
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/o/q;->s:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/y;->c:Lcom/google/android/apps/gmm/map/o/q;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/q;->b:Lcom/google/android/apps/gmm/map/o/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/y;->getState()Ljava/lang/Thread$State;

    move-result-object v2

    sget-object v3, Ljava/lang/Thread$State;->WAITING:Ljava/lang/Thread$State;

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/av;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/o/z;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/v/cp;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/at;->m:Lcom/google/android/apps/gmm/v/cp;

    return-object v0
.end method

.method public final declared-synchronized f()Lcom/google/android/apps/gmm/map/o/ap;
    .locals 1

    .prologue
    .line 382
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/x;->f:Lcom/google/android/apps/gmm/map/o/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 2

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bz;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->d:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bz;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/z;->d:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bz;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    :cond_0
    monitor-exit p0

    return-void

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final h()Z
    .locals 28

    .prologue
    .line 457
    monitor-enter p0

    .line 458
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 459
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_2

    .line 460
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/q;->o:Z

    .line 461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/v;

    .line 462
    sget-object v7, Lcom/google/android/apps/gmm/map/o/t;->a:[I

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/o/v;->b:Lcom/google/android/apps/gmm/map/o/w;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/o/w;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 477
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/q;->g:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v7, v4}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 459
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 464
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 465
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    move-object/from16 v0, p0

    invoke-interface {v7, v0}, Lcom/google/android/apps/gmm/map/o/ak;->a(Lcom/google/android/apps/gmm/map/o/p;)V

    goto :goto_1

    .line 487
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 468
    :cond_0
    :try_start_1
    const-string v7, "LabelControllerImpl"

    const-string v8, "LabelSource is already added: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 472
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 473
    iget-object v7, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    move-object/from16 v0, p0

    invoke-interface {v7, v0}, Lcom/google/android/apps/gmm/map/o/ak;->b(Lcom/google/android/apps/gmm/map/o/p;)V

    goto :goto_1

    .line 475
    :cond_1
    const-string v7, "LabelControllerImpl"

    const-string v8, "Failed to remove LabelSource: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v4, Lcom/google/android/apps/gmm/map/o/v;->a:Lcom/google/android/apps/gmm/map/o/ak;

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 484
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 486
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->d:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v6, v4, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v6, v5, Lcom/google/android/apps/gmm/map/o/x;->b:I

    iput v6, v4, Lcom/google/android/apps/gmm/map/o/x;->b:I

    iget v6, v5, Lcom/google/android/apps/gmm/map/o/x;->c:I

    iput v6, v4, Lcom/google/android/apps/gmm/map/o/x;->c:I

    iget v6, v5, Lcom/google/android/apps/gmm/map/o/x;->d:I

    iput v6, v4, Lcom/google/android/apps/gmm/map/o/x;->d:I

    iget v6, v5, Lcom/google/android/apps/gmm/map/o/x;->e:F

    iput v6, v4, Lcom/google/android/apps/gmm/map/o/x;->e:F

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/x;->f:Lcom/google/android/apps/gmm/map/o/ap;

    iput-object v6, v4, Lcom/google/android/apps/gmm/map/o/x;->f:Lcom/google/android/apps/gmm/map/o/ap;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/x;->g:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v6, v4, Lcom/google/android/apps/gmm/map/o/x;->g:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/o/x;->h:Lcom/google/android/apps/gmm/map/o/ag;

    iput-object v6, v4, Lcom/google/android/apps/gmm/map/o/x;->h:Lcom/google/android/apps/gmm/map/o/ag;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/o/x;->i:Ljava/util/List;

    iput-object v5, v4, Lcom/google/android/apps/gmm/map/o/x;->i:Ljava/util/List;

    .line 487
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/q;->n:Z

    if-nez v4, :cond_6

    .line 490
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v4}, La/a/a/a/b/s;->size()I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v4}, La/a/a/a/b/s;->b()La/a/a/a/d/be;

    move-result-object v4

    invoke-interface {v4}, La/a/a/a/d/be;->a()La/a/a/a/d/bg;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/d;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto :goto_2

    :cond_3
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v4}, La/a/a/a/b/s;->clear()V

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/o/av;->d()V

    .line 491
    :cond_4
    const/4 v5, 0x0

    .line 508
    :cond_5
    :goto_3
    return v5

    .line 494
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/q;->o:Z

    if-eqz v4, :cond_8

    .line 495
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->f:Lcom/google/android/apps/gmm/map/o/am;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/am;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/am;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/am;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/am;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/am;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/am;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 496
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    :goto_4
    if-ge v5, v6, :cond_7

    .line 497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/ak;

    .line 498
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/q;->f:Lcom/google/android/apps/gmm/map/o/am;

    invoke-interface {v4, v7}, Lcom/google/android/apps/gmm/map/o/ak;->a(Lcom/google/android/apps/gmm/map/o/am;)V

    .line 496
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_4

    .line 500
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->f:Lcom/google/android/apps/gmm/map/o/am;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/am;->a:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v5

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 501
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/q;->o:Z

    .line 504
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->q:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/o/x;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->q:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/x;->c:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget v6, v6, Lcom/google/android/apps/gmm/map/o/x;->d:I

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/map/f/o;->a(II)V

    .line 506
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->q:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/x;->b:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/f/o;->a(I)V

    .line 508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/q;->q:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/q;->f:Lcom/google/android/apps/gmm/map/o/am;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/x;->f:Lcom/google/android/apps/gmm/map/o/ap;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/o/x;->g:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/q;->e:Lcom/google/android/apps/gmm/map/o/x;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/o/x;->h:Lcom/google/android/apps/gmm/map/o/ag;

    move-object/from16 v24, v0

    if-nez v22, :cond_9

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_9
    if-nez v23, :cond_a

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_a
    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_b
    if-nez v5, :cond_c

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_c
    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->i:Lcom/google/android/apps/gmm/map/o/ap;

    move-object/from16 v0, v21

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/o/at;->j:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->p:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v4, v4, Lcom/google/r/b/a/aqg;->n:I

    invoke-static {v4}, Lcom/google/r/b/a/aqj;->a(I)Lcom/google/r/b/a/aqj;

    move-result-object v4

    if-nez v4, :cond_d

    sget-object v4, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    :cond_d
    sget-object v5, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    invoke-virtual {v4, v5}, Lcom/google/r/b/a/aqj;->equals(Ljava/lang/Object;)Z

    move-result v4

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/at;->z:Z

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    iget-object v5, v4, La/a/a/a/b/s;->i:La/a/a/a/b/ag;

    if-nez v5, :cond_e

    new-instance v5, La/a/a/a/b/z;

    invoke-direct {v5, v4}, La/a/a/a/b/z;-><init>(La/a/a/a/b/s;)V

    iput-object v5, v4, La/a/a/a/b/s;->i:La/a/a/a/b/ag;

    :cond_e
    iget-object v4, v4, La/a/a/a/b/s;->i:La/a/a/a/b/ag;

    invoke-interface {v4}, La/a/a/a/b/ag;->c()La/a/a/a/d/bg;

    move-result-object v5

    :cond_f
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/a/a/a/b/ad;

    invoke-interface {v4}, La/a/a/a/b/ad;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/d;

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v6, v6, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o/d;->c()F

    move-result v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_f

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    :cond_10
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v26

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x24

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "start labeling @"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v26

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v5

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v6

    add-int/lit16 v7, v5, 0xc8

    add-int/lit8 v8, v6, 0x64

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    if-eqz v4, :cond_13

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/a;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    :goto_6
    if-eqz v4, :cond_11

    const/16 v9, -0xc8

    iget v10, v4, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    float-to-int v10, v10

    if-ne v9, v10, :cond_11

    const/16 v9, -0x64

    iget v10, v4, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    float-to-int v10, v10

    if-ne v9, v10, :cond_11

    iget v9, v4, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    float-to-int v9, v9

    if-ne v7, v9, :cond_11

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    float-to-int v4, v4

    if-eq v8, v4, :cond_14

    :cond_11
    new-instance v4, Lcom/google/android/apps/gmm/map/o/a;

    new-instance v9, Lcom/google/android/apps/gmm/map/o/b/a;

    const/high16 v10, -0x3cb80000    # -200.0f

    const/high16 v11, -0x3d380000    # -100.0f

    int-to-float v7, v7

    int-to-float v8, v8

    invoke-direct {v9, v10, v11, v7, v8}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    invoke-direct {v4, v9}, Lcom/google/android/apps/gmm/map/o/a;-><init>(Lcom/google/android/apps/gmm/map/o/b/a;)V

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    :goto_7
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/a;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v5, v4, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v6, v4, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v7, v4, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6, v7, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(FFFF)Lcom/google/android/apps/gmm/map/b/a/m;

    move-result-object v4

    if-eqz v4, :cond_15

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/m;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v4

    move-object/from16 v19, v4

    :goto_8
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/at;->w:Z

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->d:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    const/4 v8, 0x0

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/am;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    move-object/from16 v20, v8

    :cond_12
    :goto_9
    move-object/from16 v0, v21

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/o/at;->w:Z

    if-nez v4, :cond_38

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_38

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lcom/google/android/apps/gmm/map/o/as;

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    if-eqz v24, :cond_16

    if-nez v20, :cond_16

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/o/ag;->a(Lcom/google/android/apps/gmm/map/internal/c/m;)Z

    move-result v5

    if-eqz v5, :cond_16

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/ag;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/f/o;)V

    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v5, v5, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/internal/c/m;F)V

    move-object/from16 v20, v8

    goto :goto_9

    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_14
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->h:Lcom/google/android/apps/gmm/map/o/a;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/o/c;->a:I

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/o/c;->b:I

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/o/c;->c:I

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/o/c;->d:I

    const/4 v6, 0x0

    iput v6, v5, Lcom/google/android/apps/gmm/map/o/c;->e:I

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/o/a;->a:Lcom/google/android/apps/gmm/map/o/b;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;)V

    goto/16 :goto_7

    :cond_15
    const/4 v4, 0x0

    move-object/from16 v19, v4

    goto :goto_8

    :cond_16
    iget-object v4, v8, Lcom/google/android/apps/gmm/map/o/as;->a:Lcom/google/android/apps/gmm/map/o/d;

    if-eqz v4, :cond_17

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    goto :goto_9

    :cond_17
    if-eqz v19, :cond_18

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v5, v4, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v5, :cond_1f

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/an;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    :goto_a
    if-eqz v4, :cond_12

    :cond_18
    const/4 v6, 0x1

    const/4 v4, 0x0

    move v5, v4

    :goto_b
    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/am;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_43

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/am;->b:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/ay;

    invoke-interface {v4, v8}, Lcom/google/android/apps/gmm/map/o/ay;->a(Lcom/google/android/apps/gmm/map/o/as;)Z

    move-result v4

    if-nez v4, :cond_21

    const/4 v4, 0x0

    :goto_c
    if-eqz v4, :cond_12

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    iget-object v7, v8, Lcom/google/android/apps/gmm/map/o/as;->c:Lcom/google/android/apps/gmm/map/o/ak;

    instance-of v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v4, :cond_32

    check-cast v5, Lcom/google/android/apps/gmm/map/internal/c/an;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v6, 0x41a00000    # 20.0f

    cmpl-float v4, v4, v6

    if-lez v4, :cond_22

    const/4 v4, 0x1

    :goto_d
    if-eqz v4, :cond_19

    const/high16 v4, 0x20000

    iget v6, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/2addr v4, v6

    if-eqz v4, :cond_23

    const/4 v4, 0x1

    :goto_e
    if-eqz v4, :cond_24

    const/4 v4, 0x1

    :goto_f
    if-nez v4, :cond_12

    :cond_19
    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    const/4 v9, 0x0

    iget-boolean v4, v8, Lcom/google/android/apps/gmm/map/o/as;->e:Z

    if-eqz v4, :cond_27

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v4

    if-nez v4, :cond_26

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v4, :cond_26

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    if-eqz v4, :cond_1a

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_25

    :cond_1a
    const/4 v4, 0x1

    :goto_10
    if-nez v4, :cond_26

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    move-object v13, v4

    :goto_11
    if-eqz v13, :cond_1b

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->g:Ljava/util/Map;

    invoke-interface {v4, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/an;

    const/4 v6, 0x0

    if-eqz v4, :cond_44

    if-ne v4, v5, :cond_2f

    const/4 v4, 0x1

    :goto_12
    if-nez v4, :cond_12

    :cond_1b
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    const/4 v6, 0x0

    aget-object v4, v4, v6

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v5, v4, v7}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/internal/c/an;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/ak;)I

    move-result v6

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    invoke-virtual {v4, v6}, La/a/a/a/b/s;->b(I)Z

    move-result v4

    if-nez v4, :cond_12

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/o/at;->a(I)Lcom/google/android/apps/gmm/map/o/d;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;

    if-nez v4, :cond_1c

    move-object/from16 v0, v21

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    iget-boolean v12, v8, Lcom/google/android/apps/gmm/map/o/as;->e:Z

    const/4 v11, 0x0

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/o/z;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;

    iget-object v8, v10, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v9, v10, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v10, v10, Lcom/google/android/apps/gmm/map/o/z;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    invoke-virtual/range {v4 .. v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;->a(Lcom/google/android/apps/gmm/map/internal/c/an;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/t/l;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;

    move-result-object v4

    if-eqz v4, :cond_1c

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(I)V

    :cond_1c
    move-object v6, v4

    if-eqz v6, :cond_12

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/internal/c/m;F)V

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    move-result v4

    if-eqz v4, :cond_30

    const/4 v4, 0x1

    :goto_13
    if-eqz v4, :cond_1e

    if-eqz v13, :cond_1d

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->g:Ljava/util/Map;

    invoke-interface {v4, v13, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1d
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->p:Lcom/google/android/apps/gmm/map/internal/c/cu;

    if-eqz v5, :cond_1e

    move-object/from16 v0, v21

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/o/at;->r:Ljava/util/Set;

    new-instance v9, Lcom/google/android/apps/gmm/map/j/g;

    iget-object v10, v5, Lcom/google/android/apps/gmm/map/internal/c/cu;->a:Ljava/lang/String;

    iget-object v11, v5, Lcom/google/android/apps/gmm/map/internal/c/cu;->b:Ljava/lang/String;

    if-eqz v7, :cond_31

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/o/ak;->c()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v4

    if-eqz v4, :cond_31

    const/4 v4, 0x1

    :goto_14
    invoke-direct {v9, v10, v11, v4}, Lcom/google/android/apps/gmm/map/j/g;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-boolean v4, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b:Z

    if-eqz v4, :cond_1e

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->r:Ljava/util/Set;

    new-instance v7, Lcom/google/android/apps/gmm/map/j/g;

    iget-object v8, v5, Lcom/google/android/apps/gmm/map/internal/c/cu;->a:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/cu;->c:Ljava/lang/String;

    invoke-direct {v7, v8, v5}, Lcom/google/android/apps/gmm/map/j/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1e
    const/4 v4, 0x1

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->b(I)V

    goto/16 :goto_9

    :cond_1f
    instance-of v5, v4, Lcom/google/android/apps/gmm/map/internal/c/ad;

    if-eqz v5, :cond_20

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v4

    goto/16 :goto_a

    :cond_20
    const/4 v4, 0x0

    goto/16 :goto_a

    :cond_21
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_b

    :cond_22
    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_23
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_24
    const/4 v4, 0x0

    goto/16 :goto_f

    :cond_25
    const/4 v4, 0x0

    goto/16 :goto_10

    :cond_26
    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v4

    if-eqz v4, :cond_42

    move-object v13, v6

    goto/16 :goto_11

    :cond_27
    iget v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_29

    const/4 v4, 0x1

    :goto_15
    if-nez v4, :cond_28

    iget v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_2a

    const/4 v4, 0x1

    :goto_16
    if-eqz v4, :cond_2b

    :cond_28
    const/4 v4, 0x1

    :goto_17
    if-nez v4, :cond_2e

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v4

    if-eqz v4, :cond_2e

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/c/an;->f()Z

    move-result v4

    if-nez v4, :cond_2d

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-eqz v4, :cond_2c

    const/4 v4, 0x1

    :goto_18
    if-nez v4, :cond_2d

    const/4 v4, 0x1

    :goto_19
    if-eqz v4, :cond_2e

    const/4 v4, 0x1

    :goto_1a
    if-eqz v4, :cond_42

    move-object v13, v6

    goto/16 :goto_11

    :cond_29
    const/4 v4, 0x0

    goto :goto_15

    :cond_2a
    const/4 v4, 0x0

    goto :goto_16

    :cond_2b
    const/4 v4, 0x0

    goto :goto_17

    :cond_2c
    const/4 v4, 0x0

    goto :goto_18

    :cond_2d
    const/4 v4, 0x0

    goto :goto_19

    :cond_2e
    const/4 v4, 0x0

    goto :goto_1a

    :cond_2f
    iget-object v9, v5, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eqz v9, :cond_44

    if-eqz v4, :cond_44

    iget v9, v9, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v9, v4, :cond_44

    const/4 v4, 0x1

    goto/16 :goto_12

    :cond_30
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/o/o;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    const/4 v4, 0x0

    goto/16 :goto_13

    :cond_31
    const/4 v4, 0x0

    goto/16 :goto_14

    :cond_32
    instance-of v4, v5, Lcom/google/android/apps/gmm/map/internal/c/ad;

    if-eqz v4, :cond_12

    move-object v10, v5

    check-cast v10, Lcom/google/android/apps/gmm/map/internal/c/ad;

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v4, v4

    if-eqz v4, :cond_12

    sget-object v15, Lcom/google/android/apps/gmm/map/t/l;->n:Lcom/google/android/apps/gmm/map/t/l;

    if-eqz v7, :cond_33

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/o/ak;->c()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v4, v5, :cond_33

    sget-object v15, Lcom/google/android/apps/gmm/map/t/l;->p:Lcom/google/android/apps/gmm/map/t/l;

    :cond_33
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x41a00000    # 20.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_36

    const/4 v4, 0x1

    :goto_1b
    if-eqz v4, :cond_34

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/internal/c/ad;->f()Z

    move-result v4

    if-nez v4, :cond_12

    :cond_34
    iget-object v14, v10, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    const/4 v4, 0x0

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    aget-object v4, v5, v4

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/b/a/ab;->hashCode()I

    move-result v6

    xor-int/2addr v4, v6

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/c/be;->hashCode()I

    move-result v5

    xor-int/2addr v5, v4

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    invoke-virtual {v4, v5}, La/a/a/a/b/s;->b(I)Z

    move-result v4

    if-nez v4, :cond_12

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/o/at;->a(I)Lcom/google/android/apps/gmm/map/o/d;

    move-result-object v4

    if-nez v4, :cond_35

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->b:Lcom/google/android/apps/gmm/map/o/z;

    const/4 v6, 0x0

    iget-object v8, v10, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    aget-object v13, v8, v6

    move-object/from16 v0, v21

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/o/at;->y:Lcom/google/android/apps/gmm/map/o/au;

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/o/z;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    move-object/from16 v16, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    move-object/from16 v17, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/o/z;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    move-object/from16 v18, v0

    move-object v12, v7

    invoke-virtual/range {v8 .. v18}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/internal/c/m;Ljava/lang/Integer;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ac;

    move-result-object v4

    if-eqz v4, :cond_35

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    :cond_35
    if-eqz v4, :cond_12

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    move-result v5

    if-eqz v5, :cond_37

    :goto_1c
    const/4 v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    goto/16 :goto_9

    :cond_36
    const/4 v4, 0x0

    goto :goto_1b

    :cond_37
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/at;->a:Lcom/google/android/apps/gmm/map/o/o;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/o/o;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto :goto_1c

    :cond_38
    if-eqz v24, :cond_39

    if-nez v20, :cond_39

    const/4 v4, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/ag;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/f/o;)V

    :cond_39
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->s:Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/at;->d:Ljava/util/Set;

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Ljava/util/Set;)V

    const/4 v5, 0x0

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3b

    const/4 v4, 0x1

    move v7, v4

    :goto_1d
    if-eqz v7, :cond_3d

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->t:Lcom/google/android/apps/gmm/map/o/a/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/o/a/c;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/am;->e:Ljava/util/List;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/am;->f:Ljava/util/List;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/gmm/map/o/at;->a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v4

    if-nez v4, :cond_3a

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->u:Lcom/google/android/apps/gmm/map/o/a/i;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/am;->d:Ljava/util/List;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/a/a;Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v4

    if-nez v4, :cond_3a

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->v:Lcom/google/android/apps/gmm/map/o/a/e;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/o/am;->c:Ljava/util/List;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/a/a;Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v4

    if-eqz v4, :cond_3c

    :cond_3a
    const/4 v4, 0x1

    move v5, v4

    :goto_1e
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v4}, La/a/a/a/b/s;->b()La/a/a/a/d/be;

    move-result-object v4

    invoke-interface {v4}, La/a/a/a/d/be;->a()La/a/a/a/d/bg;

    move-result-object v6

    :goto_1f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_40

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/d;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    goto :goto_1f

    :cond_3b
    const/4 v4, 0x0

    move v7, v4

    goto :goto_1d

    :cond_3c
    const/4 v4, 0x0

    move v5, v4

    goto :goto_1e

    :cond_3d
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->c:Ljava/util/List;

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v6}, La/a/a/a/b/s;->b()La/a/a/a/d/be;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/at;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    move v6, v4

    :goto_20
    if-ge v6, v8, :cond_3f

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->c:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/o/d;

    const/4 v9, 0x1

    invoke-interface {v4, v9}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/d;)Z

    move-result v9

    if-nez v9, :cond_3e

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/o/at;->a(Lcom/google/android/apps/gmm/map/o/d;)V

    :cond_3e
    const/4 v9, 0x1

    invoke-interface {v4, v9}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_20

    :cond_3f
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    :cond_40
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/gmm/map/o/at;->a()V

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    move-object/from16 v0, v21

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->f:La/a/a/a/b/s;

    invoke-virtual {v4}, La/a/a/a/b/s;->clear()V

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->g:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->k:Lcom/google/android/apps/gmm/map/o/az;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/o/az;->a()V

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->e:La/a/a/a/b/s;

    invoke-virtual {v4}, La/a/a/a/b/s;->size()I

    move-result v4

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v8, 0x17

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Num labels: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v26

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v6, 0x32

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Label placement is done in "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "ms,"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->l:Lcom/google/android/apps/gmm/map/o/av;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/o/av;->d()V

    if-eqz v7, :cond_5

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_5

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->o:Lcom/google/android/apps/gmm/map/q/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/q/a;->a()Z

    move-result v4

    if-eqz v4, :cond_41

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->q:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v6, Lcom/google/android/apps/gmm/map/j/f;

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/at;->r:Ljava/util/Set;

    invoke-static {v7}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/apps/gmm/map/j/f;-><init>(Ljava/util/List;)V

    invoke-interface {v4, v6}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_41
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/o/at;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    goto/16 :goto_3

    :cond_42
    move-object v13, v9

    goto/16 :goto_11

    :cond_43
    move v4, v6

    goto/16 :goto_c

    :cond_44
    move v4, v6

    goto/16 :goto_12

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
