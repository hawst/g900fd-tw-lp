.class public Lcom/google/android/apps/gmm/prefetchcache/b/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;
.implements Lcom/google/android/apps/gmm/prefetchcache/b/h;


# instance fields
.field public a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

.field private b:Landroid/content/Context;

.field private c:Lcom/google/android/apps/gmm/prefetchcache/b/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/prefetchcache/b/k;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/b/l;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->b:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->c:Lcom/google/android/apps/gmm/prefetchcache/b/k;

    .line 41
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->ji:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/b/l;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/b/j;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/prefetchcache/b/l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    sget v0, Lcom/google/android/apps/gmm/l;->jh:I

    .line 84
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 74
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->iW:I

    goto :goto_0

    .line 77
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->je:I

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->c:Lcom/google/android/apps/gmm/prefetchcache/b/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/b/k;->o()V

    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->c:Lcom/google/android/apps/gmm/prefetchcache/b/k;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/b/k;->i()V

    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/b/i;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/b/l;->a:Lcom/google/android/apps/gmm/prefetchcache/b/l;

    if-ne v0, v1, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->ez:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0
.end method
