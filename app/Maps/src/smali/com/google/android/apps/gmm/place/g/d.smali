.class public Lcom/google/android/apps/gmm/place/g/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/g/a/f;


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/base/e/c;

.field private d:Lcom/google/android/apps/gmm/place/bw;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/place/bw;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bw;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->d:Lcom/google/android/apps/gmm/place/bw;

    .line 39
    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->a:Landroid/content/Context;

    .line 40
    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->b:Lcom/google/android/apps/gmm/x/o;

    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/base/e/c;->d:Lcom/google/android/apps/gmm/base/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->c:Lcom/google/android/apps/gmm/base/e/c;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/place/g/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/g/e;-><init>(Lcom/google/android/apps/gmm/place/g/d;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->g:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .param p3    # Lcom/google/android/apps/gmm/place/an;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 59
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 60
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->f:Ljava/lang/Boolean;

    .line 61
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v4

    .line 63
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->O()Lcom/google/maps/g/pc;

    move-result-object v2

    .line 68
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->N()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 69
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->P()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 70
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    iget-object v1, v1, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/aec;

    iget v1, v1, Lcom/google/r/b/a/aec;->h:I

    if-lez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->c:Lcom/google/android/apps/gmm/base/e/c;

    sget-object v5, Lcom/google/android/apps/gmm/base/e/c;->d:Lcom/google/android/apps/gmm/base/e/c;

    if-eq v1, v5, :cond_8

    .line 72
    :cond_0
    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->B()Lcom/google/android/apps/gmm/u/a/a;

    move-result-object v5

    .line 73
    if-eqz v5, :cond_8

    .line 74
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-interface {v5, v1}, Lcom/google/android/apps/gmm/u/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 76
    invoke-interface {v5, v1}, Lcom/google/android/apps/gmm/u/a/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    if-nez v6, :cond_1

    if-eqz v1, :cond_8

    .line 78
    :cond_1
    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v2

    .line 79
    if-eqz v6, :cond_3

    .line 80
    if-nez v6, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v5, v2, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, v2, Lcom/google/maps/g/hi;->a:I

    iput-object v6, v2, Lcom/google/maps/g/hi;->c:Ljava/lang/Object;

    .line 82
    :cond_3
    if-eqz v1, :cond_5

    .line 83
    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v5, v2, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v2, Lcom/google/maps/g/hi;->a:I

    iput-object v1, v2, Lcom/google/maps/g/hi;->d:Ljava/lang/Object;

    .line 86
    :cond_5
    invoke-static {}, Lcom/google/maps/g/pc;->newBuilder()Lcom/google/maps/g/pe;

    move-result-object v5

    .line 87
    iget-object v1, v5, Lcom/google/maps/g/pe;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/hi;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v6, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v1, Lcom/google/n/ao;->d:Z

    iget v1, v5, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v5, Lcom/google/maps/g/pe;->a:I

    .line 88
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->P()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->gw:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    :goto_0
    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 89
    :cond_6
    const-string v1, ""

    goto :goto_0

    .line 88
    :cond_7
    iget v2, v5, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v5, Lcom/google/maps/g/pe;->a:I

    iput-object v1, v5, Lcom/google/maps/g/pe;->e:Ljava/lang/Object;

    .line 90
    invoke-virtual {v5}, Lcom/google/maps/g/pe;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/pc;

    .line 91
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/g/d;->f:Ljava/lang/Boolean;

    move-object v2, v1

    .line 100
    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->N()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_9
    move v1, v3

    :goto_1
    if-nez v1, :cond_e

    .line 101
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->c:Z

    if-eqz v1, :cond_e

    .line 102
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v1, v1, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_d

    move v1, v3

    :goto_2
    if-eqz v1, :cond_e

    move v1, v3

    .line 100
    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->g:Ljava/lang/Boolean;

    .line 104
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v0

    if-nez v0, :cond_f

    move v4, v3

    .line 105
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->d:Lcom/google/android/apps/gmm/place/bw;

    move-object v1, p1

    move-object v5, p2

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/bw;->a(Landroid/content/Context;Lcom/google/maps/g/pc;ZZLcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/g/a/f;)V

    .line 108
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/g/d;->a:Landroid/content/Context;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->d:Lcom/google/android/apps/gmm/place/bw;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/bw;->a:Ljava/lang/String;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    move v7, v3

    :cond_b
    if-eqz v7, :cond_10

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->pm:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->e:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/g/d;->b:Lcom/google/android/apps/gmm/x/o;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->d:Lcom/google/android/apps/gmm/place/bw;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 113
    return-void

    :cond_c
    move v1, v7

    .line 100
    goto :goto_1

    :cond_d
    move v1, v7

    .line 102
    goto :goto_2

    :cond_e
    move v1, v7

    goto :goto_3

    :cond_f
    move v4, v7

    .line 104
    goto :goto_4

    .line 109
    :cond_10
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->gc:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/place/g/a/d;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->d:Lcom/google/android/apps/gmm/place/bw;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->b(Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/place/review/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->b:Lcom/google/android/apps/gmm/x/o;

    .line 146
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/review/j;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/place/review/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g/d;->d:Lcom/google/android/apps/gmm/place/bw;

    .line 147
    iget-object v1, v1, Lcom/google/android/apps/gmm/place/bw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v3, "fivestarrating"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 148
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/review/j;->a()V

    .line 149
    const/4 v0, 0x0

    return-object v0
.end method
