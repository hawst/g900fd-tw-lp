.class public Lcom/google/android/apps/gmm/base/views/c/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/apps/gmm/util/webimageview/b;

.field public final c:Lcom/google/android/libraries/curvular/aw;

.field public final d:I

.field public final e:Lcom/google/android/apps/gmm/util/webimageview/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/c/k;->a:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/c/k;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 31
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/k;->d:I

    .line 33
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/c/k;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V
    .locals 1

    .prologue
    .line 43
    invoke-static {p3}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Lcom/google/android/libraries/curvular/aw;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;IILcom/google/android/apps/gmm/util/webimageview/g;)V
    .locals 6

    .prologue
    .line 54
    invoke-static {p3}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Lcom/google/android/libraries/curvular/aw;ILcom/google/android/apps/gmm/util/webimageview/g;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Lcom/google/android/libraries/curvular/aw;I)V
    .locals 6

    .prologue
    .line 48
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Lcom/google/android/libraries/curvular/aw;ILcom/google/android/apps/gmm/util/webimageview/g;)V

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Lcom/google/android/libraries/curvular/aw;ILcom/google/android/apps/gmm/util/webimageview/g;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/c/k;->a:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 63
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    .line 64
    iput p4, p0, Lcom/google/android/apps/gmm/base/views/c/k;->d:I

    .line 65
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/views/c/k;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 66
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 80
    if-ne p1, p0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/views/c/k;

    if-eqz v2, :cond_9

    .line 84
    check-cast p1, Lcom/google/android/apps/gmm/base/views/c/k;

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/k;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    move v2, v0

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/k;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 86
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_3
    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    .line 87
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_4
    move v2, v0

    :goto_3
    if-eqz v2, :cond_5

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/views/c/k;->d:I

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    .line 85
    goto :goto_1

    :cond_7
    move v2, v1

    .line 86
    goto :goto_2

    :cond_8
    move v2, v1

    .line 87
    goto :goto_3

    :cond_9
    move v0, v1

    .line 90
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/k;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
