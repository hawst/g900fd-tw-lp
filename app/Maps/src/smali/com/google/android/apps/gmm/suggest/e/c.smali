.class public final enum Lcom/google/android/apps/gmm/suggest/e/c;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/suggest/e/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum b:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum c:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum d:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum e:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum f:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum g:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum h:Lcom/google/android/apps/gmm/suggest/e/c;

.field public static final enum i:Lcom/google/android/apps/gmm/suggest/e/c;

.field private static final synthetic k:[Lcom/google/android/apps/gmm/suggest/e/c;


# instance fields
.field public j:Lcom/google/b/f/t;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 10
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "SEARCH"

    sget-object v2, Lcom/google/b/f/t;->cE:Lcom/google/b/f/t;

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->b:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 11
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "START_LOCATION"

    sget-object v2, Lcom/google/b/f/t;->aJ:Lcom/google/b/f/t;

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 12
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "END_LOCATION"

    sget-object v2, Lcom/google/b/f/t;->aH:Lcom/google/b/f/t;

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 13
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "HOME"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "WORK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->f:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "PLACE_PICKER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->g:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "ADD_A_PLACE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->h:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/c;

    const-string v1, "CATEGORY_SELECTOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/suggest/e/c;-><init>(Ljava/lang/String;ILcom/google/b/f/t;)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->i:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 8
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/suggest/e/c;

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->b:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->f:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->g:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->h:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->i:Lcom/google/android/apps/gmm/suggest/e/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->k:[Lcom/google/android/apps/gmm/suggest/e/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/b/f/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/f/t;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/e/c;->j:Lcom/google/b/f/t;

    .line 23
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->k:[Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/suggest/e/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/suggest/e/c;

    return-object v0
.end method
