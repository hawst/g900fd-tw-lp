.class Lcom/google/android/apps/gmm/photo/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic c:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;Landroid/net/Uri;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/p;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    iput-object p2, p0, Lcom/google/android/apps/gmm/photo/p;->a:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/gmm/photo/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/p;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->dismiss()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/p;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    .line 217
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    new-array v3, v1, [Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/p;->a:Landroid/net/Uri;

    aput-object v2, v3, v4

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v2, v3

    if-ltz v2, :cond_1

    :goto_0
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v1, v4

    goto :goto_0

    :cond_2
    const-wide/16 v6, 0x5

    int-to-long v8, v2

    add-long/2addr v6, v8

    div-int/lit8 v1, v2, 0xa

    int-to-long v8, v1

    add-long/2addr v6, v8

    const-wide/32 v8, 0x7fffffff

    cmp-long v1, v6, v8

    if-lez v1, :cond_3

    const v1, 0x7fffffff

    move v2, v1

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/p;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/p;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    .line 218
    iget-boolean v3, v3, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->d:Z

    .line 216
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->a(Lcom/google/android/apps/gmm/x/a;Ljava/util/ArrayList;Lcom/google/android/apps/gmm/base/g/c;ZZLcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 220
    return-void

    .line 217
    :cond_3
    const-wide/32 v8, -0x80000000

    cmp-long v1, v6, v8

    if-gez v1, :cond_4

    const/high16 v1, -0x80000000

    move v2, v1

    goto :goto_1

    :cond_4
    long-to-int v1, v6

    move v2, v1

    goto :goto_1
.end method
