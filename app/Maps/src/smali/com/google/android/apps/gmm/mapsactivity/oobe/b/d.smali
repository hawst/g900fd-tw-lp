.class public Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;


# instance fields
.field a:I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/support/v4/view/bz;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a(I)Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 19
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a(I)Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;

    move-result-object v1

    const/4 v2, 0x2

    .line 20
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a(I)Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;

    move-result-object v2

    const/4 v3, 0x3

    .line 21
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a(I)Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;

    move-result-object v3

    .line 17
    invoke-static {v0, v1, v2, v3}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->b:Ljava/util/List;

    .line 23
    iput v4, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a:I

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/e;-><init>(Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->c:Landroid/support/v4/view/bz;

    return-void
.end method

.method private a(I)Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 34
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;

    .line 35
    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;->b()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v3

    .line 36
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;

    .line 37
    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;->c()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v4

    .line 38
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;

    .line 39
    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/c;->d()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v5

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;

    move v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/b;-><init>(ILcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/libraries/curvular/b/i;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/mapsactivity/oobe/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a:I

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a:I

    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a:I

    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Landroid/support/v4/view/bz;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->c:Landroid/support/v4/view/bz;

    return-object v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/apps/gmm/mapsactivity/oobe/b/d;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
