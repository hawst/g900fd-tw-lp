.class Lcom/google/android/apps/gmm/cardui/a/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 3

    .prologue
    .line 26
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 27
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->w:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/in;->d()Lcom/google/o/h/a/in;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/in;

    iget-object v0, v0, Lcom/google/o/h/a/in;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/aw;->d()Lcom/google/maps/g/aw;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/aw;

    .line 26
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/a/s;->a(Lcom/google/maps/g/aw;)Landroid/content/Intent;

    move-result-object v0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :goto_0
    return-void

    .line 26
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    sget v2, Lcom/google/android/apps/gmm/l;->gx:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/google/o/h/a/g;->B:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const/high16 v1, 0x200000

    .line 16
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
