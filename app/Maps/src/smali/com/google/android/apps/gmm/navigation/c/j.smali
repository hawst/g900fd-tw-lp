.class public Lcom/google/android/apps/gmm/navigation/c/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/c/a/b;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:F

.field private static final c:F

.field private static final d:F


# instance fields
.field private e:Lcom/google/r/b/a/or;

.field private f:F

.field private g:Lcom/google/android/apps/gmm/map/r/b/a;

.field private final h:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final i:Lcom/google/r/b/a/op;

.field private final j:Z

.field private final k:Z

.field private final l:Lcom/google/android/apps/gmm/map/f/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 41
    const-class v0, Lcom/google/android/apps/gmm/navigation/c/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/c/j;->a:Ljava/lang/String;

    .line 56
    const-wide v0, 0x408f400000000000L    # 1000.0

    .line 57
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/navigation/c/j;->b:F

    .line 58
    const-wide v0, 0x40a7700000000000L    # 3000.0

    .line 59
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/navigation/c/j;->c:F

    .line 75
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/navigation/c/j;->d:F

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/r/b/a/op;ZZLcom/google/android/apps/gmm/map/f/a/h;)V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    .line 91
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->f:F

    .line 116
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->h:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 117
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/c/j;->i:Lcom/google/r/b/a/op;

    .line 118
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/c/j;->j:Z

    .line 119
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->k:Z

    .line 120
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/c/j;->l:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 121
    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->f:F

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 122
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/m;IIIILcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 5

    .prologue
    .line 430
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/b/a/y;)[I

    move-result-object v1

    .line 431
    const/high16 v0, 0x3f800000    # 1.0f

    .line 432
    if-nez v1, :cond_1

    .line 435
    const/high16 v0, 0x3f800000    # 1.0f

    .line 436
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 435
    invoke-static {v1, v2, p7, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    .line 456
    :cond_0
    :goto_0
    return v0

    .line 438
    :cond_1
    const/4 v2, 0x0

    aget v2, v1, v2

    if-gez v2, :cond_3

    .line 440
    const/high16 v0, 0x3f800000    # 1.0f

    .line 441
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    .line 440
    invoke-static {v2, v3, p7, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v0, v2

    .line 447
    :cond_2
    :goto_1
    const/4 v2, 0x1

    aget v2, v1, v2

    if-ge v2, p5, :cond_4

    .line 449
    const/high16 v1, 0x3f800000    # 1.0f

    .line 450
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    .line 449
    invoke-static {v2, v3, p7, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0

    .line 442
    :cond_3
    const/4 v2, 0x0

    aget v2, v1, v2

    if-lt v2, p3, :cond_2

    .line 444
    const/high16 v0, 0x3f800000    # 1.0f

    .line 445
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    .line 444
    invoke-static {v2, v3, p7, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_1

    .line 451
    :cond_4
    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int v2, p4, p6

    if-lt v1, v2, :cond_0

    .line 453
    const/high16 v1, 0x3f800000    # 1.0f

    .line 454
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    .line 453
    invoke-static {v2, v3, p7, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;
    .locals 5

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->h:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->i:Lcom/google/r/b/a/op;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/c/j;->j:Z

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/navigation/c/j;->k:Z

    .line 126
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->c:Ljava/util/HashMap;

    new-instance v4, Lcom/google/android/apps/gmm/shared/net/a/m;

    invoke-direct {v4, v1, v2, v3, p1}, Lcom/google/android/apps/gmm/shared/net/a/m;-><init>(Lcom/google/r/b/a/op;ZZLcom/google/r/b/a/or;)V

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oe;

    iget-object v0, v0, Lcom/google/r/b/a/oe;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/oi;->d()Lcom/google/r/b/a/oi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oi;

    return-object v0
.end method

.method private a(Lcom/google/r/b/a/or;Lcom/google/android/apps/gmm/map/b/a/y;Landroid/util/DisplayMetrics;Lcom/google/android/apps/gmm/map/f/a/e;)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 389
    const v0, 0x3e4ccccc    # 0.19999999f

    .line 390
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    if-ne v1, p1, :cond_1

    .line 391
    const/4 v0, 0x0

    move v7, v0

    .line 395
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 396
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v0

    iget v2, v0, Lcom/google/r/b/a/oi;->c:F

    .line 397
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v0

    iget v3, v0, Lcom/google/r/b/a/oi;->b:F

    .line 400
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v4

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 402
    new-instance v1, Lcom/google/android/apps/gmm/map/f/o;

    iget v3, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, p3, Landroid/util/DisplayMetrics;->density:F

    sget-object v6, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;IIFLcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 406
    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/b/a/y;)[I

    move-result-object v0

    .line 407
    iget-object v2, v1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    .line 408
    iget-object v1, v1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    .line 409
    if-eqz v0, :cond_0

    aget v3, v0, v9

    int-to-float v3, v3

    int-to-float v4, v2

    mul-float/2addr v4, v7

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    aget v3, v0, v9

    int-to-float v3, v3

    int-to-float v2, v2

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v7

    mul-float/2addr v2, v4

    cmpg-float v2, v3, v2

    if-gez v2, :cond_0

    aget v2, v0, v8

    if-ge v2, v1, :cond_0

    aget v0, v0, v8

    int-to-float v0, v0

    int-to-float v1, v1

    mul-float/2addr v1, v7

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    move v0, v8

    :goto_1
    return v0

    :cond_0
    move v0, v9

    goto :goto_1

    :cond_1
    move v7, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/ae;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 3

    .prologue
    .line 274
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/j;->a:Ljava/lang/String;

    const-string v1, "positionForBounds should not be called for ObliqueCameraPositioner"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 281
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 282
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    .line 283
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/oi;->b:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    .line 284
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 285
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    .line 286
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/graphics/Rect;Landroid/util/DisplayMetrics;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 12

    .prologue
    .line 192
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 194
    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iget v3, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    .line 193
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v1

    .line 196
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 197
    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v3

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v6, v10

    mul-double/2addr v6, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v6, v8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v8

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v6, v7, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 198
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->l:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    sget-object v0, Lcom/google/r/b/a/or;->e:Lcom/google/r/b/a/or;

    .line 199
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/oi;->c:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    sget-object v0, Lcom/google/r/b/a/or;->e:Lcom/google/r/b/a/or;

    .line 200
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/oi;->b:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    .line 201
    iput-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;[Lcom/google/android/apps/gmm/map/b/a/ah;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 20

    .prologue
    .line 208
    move-object/from16 v0, p3

    iget v15, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 209
    move-object/from16 v0, p3

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v16, v0

    .line 210
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 211
    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v9, v16, v3

    .line 214
    const/high16 v3, 0x42820000    # 65.0f

    move-object/from16 v0, p3

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    add-int v17, v2, v3

    .line 215
    const/high16 v2, 0x41c80000    # 25.0f

    move-object/from16 v0, p3

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 218
    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    mul-float/2addr v3, v4

    int-to-float v4, v15

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    .line 219
    const/high16 v4, 0x3f800000    # 1.0f

    add-int/2addr v2, v9

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move/from16 v0, v16

    int-to-float v5, v0

    div-float/2addr v2, v5

    sub-float v2, v4, v2

    .line 220
    new-instance v4, Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct {v4, v3, v2}, Lcom/google/android/apps/gmm/map/f/a/e;-><init>(FF)V

    .line 222
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v2, v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v10

    .line 224
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const/4 v3, 0x0

    aget-object v3, p2, v3

    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    sub-int v3, v5, v3

    add-int/lit8 v3, v3, -0x1

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v2, v3

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    .line 226
    sget-object v2, Lcom/google/r/b/a/or;->f:Lcom/google/r/b/a/or;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v2

    iget v2, v2, Lcom/google/r/b/a/oi;->c:F

    .line 228
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v7

    .line 229
    iput-object v10, v7, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    iput-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 230
    iput v2, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    sget-object v2, Lcom/google/r/b/a/or;->f:Lcom/google/r/b/a/or;

    .line 231
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v2

    iget v2, v2, Lcom/google/r/b/a/oi;->b:F

    iput v2, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    .line 232
    iget v2, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    iget v3, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v3, v5

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v2

    iput v2, v7, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    .line 233
    iput-object v4, v7, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 234
    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v4, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v5, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v6, v7, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 238
    new-instance v3, Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p3

    iget v7, v0, Landroid/util/DisplayMetrics;->density:F

    sget-object v8, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    move-object v4, v2

    move v5, v15

    move/from16 v6, v16

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;IIFLcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 245
    const/4 v4, 0x0

    add-int/lit8 v5, v15, -0x1

    int-to-float v5, v5

    move/from16 v0, v17

    int-to-float v6, v0

    sub-int v7, v16, v9

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(FFFF)Lcom/google/android/apps/gmm/map/b/a/m;

    move-result-object v5

    move-object v4, v11

    move v6, v15

    move/from16 v7, v16

    move/from16 v8, v17

    .line 248
    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/m;IIIILcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    .line 252
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 253
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v18, v0

    const/4 v6, 0x0

    move v14, v6

    :goto_0
    move/from16 v0, v18

    if-ge v14, v0, :cond_1

    aget-object v19, p2, v14

    .line 254
    move-object/from16 v0, v19

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    move-object/from16 v0, v19

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    sub-int/2addr v6, v8

    div-int/lit8 v6, v6, 0xa

    const/4 v8, 0x1

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 255
    if-nez v6, :cond_2

    .line 256
    const/4 v6, 0x1

    move v13, v6

    .line 258
    :goto_1
    const/4 v6, 0x0

    move v11, v6

    move v12, v7

    :goto_2
    move-object/from16 v0, v19

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    move-object/from16 v0, v19

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    sub-int/2addr v6, v7

    if-ge v11, v6, :cond_0

    .line 259
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v19

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v7, v11

    invoke-virtual {v6, v7, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move v6, v15

    move/from16 v7, v16

    move/from16 v8, v17

    .line 261
    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/m;IIIILcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v6

    .line 260
    invoke-static {v12, v6}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 258
    add-int v6, v11, v13

    move v11, v6

    move v12, v7

    goto :goto_2

    .line 253
    :cond_0
    add-int/lit8 v6, v14, 0x1

    move v14, v6

    move v7, v12

    goto :goto_0

    .line 265
    :cond_1
    iget v3, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    float-to-double v4, v7

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v4, v4

    sget v5, Lcom/google/android/apps/gmm/navigation/c/j;->d:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 267
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v7

    .line 268
    iput v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 269
    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v4, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v5, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v6, v7, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v2

    :cond_2
    move v13, v6

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;Lcom/google/maps/g/a/hm;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/f/a/f;
    .locals 7
    .param p2    # Lcom/google/android/apps/gmm/map/r/a/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/high16 v1, 0x41a00000    # 20.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 148
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 149
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->f:F

    .line 154
    :cond_0
    iget v0, p3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v4, v0

    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne p5, v0, :cond_1

    const/16 v0, 0x1d

    :goto_0
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x64

    iget v5, p4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v4

    sub-int v0, v5, v0

    invoke-virtual {p4}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    int-to-float v0, v0

    iget v5, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v5

    iget v6, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v6, v6

    invoke-static {v4, v0, v5, v6}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v5

    .line 157
    if-nez p2, :cond_2

    const/4 v0, 0x0

    .line 158
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v4, :cond_3

    move v4, v2

    :goto_2
    if-eqz v4, :cond_6

    sget-object v0, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    .line 159
    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    .line 161
    if-eqz p6, :cond_11

    invoke-virtual {p6}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 162
    :goto_4
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/oi;->b:F

    .line 163
    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/f/a/g;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/j;->l:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 164
    iput-object v3, v2, Lcom/google/android/apps/gmm/map/f/a/g;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 165
    iput-object v5, v2, Lcom/google/android/apps/gmm/map/f/a/g;->d:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 166
    iput v1, v2, Lcom/google/android/apps/gmm/map/f/a/g;->a:F

    .line 167
    iput v0, v2, Lcom/google/android/apps/gmm/map/f/a/g;->b:F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/a/g;->a()Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v0

    return-object v0

    .line 154
    :cond_1
    const/16 v0, 0x9

    goto :goto_0

    .line 157
    :cond_2
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_1

    .line 158
    :cond_3
    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    sget v6, Lcom/google/android/apps/gmm/navigation/c/j;->b:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_5

    :cond_4
    move v4, v2

    goto :goto_2

    :cond_5
    sget-object v4, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    invoke-direct {p0, v4, v0, p3, v5}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;Lcom/google/android/apps/gmm/map/b/a/y;Landroid/util/DisplayMetrics;Lcom/google/android/apps/gmm/map/f/a/e;)Z

    move-result v4

    goto :goto_2

    :cond_6
    iget v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->f:F

    const/4 v6, 0x0

    cmpg-float v4, v4, v6

    if-ltz v4, :cond_7

    iget v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->f:F

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v4, v4, v6

    if-lez v4, :cond_9

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    sget-object v1, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    if-ne v0, v1, :cond_8

    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    goto :goto_3

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->e:Lcom/google/r/b/a/or;

    sget-object v6, Lcom/google/r/b/a/or;->b:Lcom/google/r/b/a/or;

    if-ne v4, v6, :cond_a

    const/high16 v1, 0x41700000    # 15.0f

    :cond_a
    iget v4, p0, Lcom/google/android/apps/gmm/navigation/c/j;->f:F

    cmpg-float v1, v4, v1

    if-gez v1, :cond_b

    move v0, v2

    :goto_5
    if-eqz v0, :cond_10

    sget-object v0, Lcom/google/r/b/a/or;->b:Lcom/google/r/b/a/or;

    goto :goto_3

    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v1, :cond_c

    move v0, v3

    goto :goto_5

    :cond_c
    if-eqz v0, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/j;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    sget v4, Lcom/google/android/apps/gmm/navigation/c/j;->c:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_e

    :cond_d
    move v0, v3

    goto :goto_5

    :cond_e
    sget-object v1, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    invoke-direct {p0, v1, v0, p3, v5}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;Lcom/google/android/apps/gmm/map/b/a/y;Landroid/util/DisplayMetrics;Lcom/google/android/apps/gmm/map/f/a/e;)Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v3

    goto :goto_5

    :cond_f
    move v0, v2

    goto :goto_5

    :cond_10
    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    goto/16 :goto_3

    .line 161
    :cond_11
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/j;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oi;

    move-result-object v1

    iget v1, v1, Lcom/google/r/b/a/oi;->c:F

    goto/16 :goto_4
.end method
