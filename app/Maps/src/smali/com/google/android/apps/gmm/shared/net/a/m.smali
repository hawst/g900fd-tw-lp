.class public Lcom/google/android/apps/gmm/shared/net/a/m;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/r/b/a/op;

.field b:Z

.field c:Z

.field d:Lcom/google/r/b/a/or;


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/op;ZZLcom/google/r/b/a/or;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->a:Lcom/google/r/b/a/op;

    .line 38
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->b:Z

    .line 39
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->c:Z

    .line 40
    iput-object p4, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->d:Lcom/google/r/b/a/or;

    .line 41
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    if-ne p0, p1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/shared/net/a/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 54
    goto :goto_0

    .line 56
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/shared/net/a/m;

    .line 57
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->a:Lcom/google/r/b/a/op;

    iget-object v3, p1, Lcom/google/android/apps/gmm/shared/net/a/m;->a:Lcom/google/r/b/a/op;

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->b:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/shared/net/a/m;->b:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->c:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/shared/net/a/m;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->d:Lcom/google/r/b/a/or;

    iget-object v3, p1, Lcom/google/android/apps/gmm/shared/net/a/m;->d:Lcom/google/r/b/a/or;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->a:Lcom/google/r/b/a/op;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/a/m;->d:Lcom/google/r/b/a/or;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
