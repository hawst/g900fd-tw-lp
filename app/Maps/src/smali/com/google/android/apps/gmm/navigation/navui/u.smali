.class public Lcom/google/android/apps/gmm/navigation/navui/u;
.super Lcom/google/android/apps/gmm/navigation/navui/f;
.source "PG"


# instance fields
.field private final d:Lcom/google/android/apps/gmm/base/activities/c;

.field private e:Lcom/google/android/apps/gmm/navigation/navui/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/navui/f;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/v;-><init>(Lcom/google/android/apps/gmm/navigation/navui/u;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->f:Ljava/lang/Object;

    .line 36
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 37
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 53
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 42
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 56
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :cond_0
    :goto_1
    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Z)V

    .line 57
    return-void

    :cond_1
    move v2, v1

    .line 56
    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/u;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v4, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_1
.end method
