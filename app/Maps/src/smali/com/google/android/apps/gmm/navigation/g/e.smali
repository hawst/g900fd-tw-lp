.class public Lcom/google/android/apps/gmm/navigation/g/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/g/c;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/navigation/g/c;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/d;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/d;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 295
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/e;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->e:Z

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 257
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/f;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 336
    new-instance v0, Lcom/google/android/apps/gmm/navigation/j/b/a;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/f;->b:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 338
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/c;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/j/b/a;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/k;J)V

    .line 339
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/j/a/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/j/a/c;-><init>(Lcom/google/android/apps/gmm/navigation/j/b/a;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 340
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/g;)V
    .locals 8
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 307
    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 308
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 309
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v4, v4, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 310
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    if-gez v5, :cond_1

    move-object v0, v1

    :goto_0
    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/navigation/g/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 316
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/a;->d:Z

    if-eqz v0, :cond_0

    .line 317
    new-instance v0, Lcom/google/android/apps/gmm/navigation/j/b/b;

    .line 318
    iget v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/a;->c:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 320
    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/c;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    const-wide/16 v6, 0x7530

    add-long/2addr v4, v6

    invoke-direct {v0, v2, v1, v4, v5}, Lcom/google/android/apps/gmm/navigation/j/b/b;-><init>(ILjava/lang/String;J)V

    .line 321
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/j/a/d;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/j/a/d;-><init>(Lcom/google/android/apps/gmm/navigation/j/b/b;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 323
    :cond_0
    return-void

    .line 310
    :cond_1
    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->c:I

    aget-object v0, v5, v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/j;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 265
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/j;->b:Lcom/google/android/apps/gmm/navigation/g/a/k;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/g/a/k;->a:Lcom/google/android/apps/gmm/navigation/g/a/k;

    if-ne v0, v1, :cond_1

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->b:Z

    .line 271
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 272
    return-void

    .line 267
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/j;->b:Lcom/google/android/apps/gmm/navigation/g/a/k;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/g/a/k;->b:Lcom/google/android/apps/gmm/navigation/g/a/k;

    if-ne v0, v1, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->d:Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/l;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 284
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/a;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/j/a/d;->a:Lcom/google/android/apps/gmm/navigation/j/a/d;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/j/a/c;->a:Lcom/google/android/apps/gmm/navigation/j/a/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 349
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/e;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/j/a/c;->a:Lcom/google/android/apps/gmm/navigation/j/a/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 357
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/p/b/e;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/p/b/e;->a:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->c:Z

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/e;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/navigation/g/c;)V

    .line 364
    return-void
.end method
