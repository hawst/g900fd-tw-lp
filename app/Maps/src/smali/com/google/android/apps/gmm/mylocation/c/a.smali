.class public Lcom/google/android/apps/gmm/mylocation/c/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/android/apps/gmm/v/aa;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/apps/gmm/mylocation/d/d;

.field final b:Lcom/google/android/apps/gmm/mylocation/d/d;

.field final c:Lcom/google/android/apps/gmm/map/t/q;

.field final d:Lcom/google/android/apps/gmm/map/t/q;

.field final e:F

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/mylocation/d/e;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/mylocation/c/c;Lcom/google/android/apps/gmm/map/t/k;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const v3, -0x21524111

    const/4 v2, 0x4

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/c/b;->a:[I

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/mylocation/c/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 97
    new-instance v0, Ljava/lang/AssertionError;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected breadcrumb type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 79
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/f;->j:I

    const-string v1, "breadcrumb_red_dot"

    invoke-virtual {p1, v0, v1, v2, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->a:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 80
    sget v0, Lcom/google/android/apps/gmm/f;->k:I

    const-string v1, "breadcrumb_red_pointer"

    invoke-virtual {p1, v0, v1, v2, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->b:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 81
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v1, "red_accuracy_fill"

    invoke-virtual {p1, v0, v2, v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->c:Lcom/google/android/apps/gmm/map/t/q;

    .line 82
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v1, "red_accuracy_outline"

    invoke-virtual {p1, v0, v4, v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->d:Lcom/google/android/apps/gmm/map/t/q;

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->a:Lcom/google/android/apps/gmm/mylocation/d/d;

    sget v1, Lcom/google/android/apps/gmm/e;->az:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->e:F

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->c:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->d:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->a:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->b:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 104
    invoke-static {v0, v1, v2, v3}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->f:Ljava/util/List;

    .line 105
    return-void

    .line 85
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/f;->h:I

    const-string v1, "breadcrumb_orange_dot"

    invoke-virtual {p1, v0, v1, v2, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->a:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 86
    sget v0, Lcom/google/android/apps/gmm/f;->i:I

    const-string v1, "breadcrumb_orange_pointer"

    invoke-virtual {p1, v0, v1, v2, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->b:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 87
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v1, "orange_accuracy_fill"

    invoke-virtual {p1, v0, v2, v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->c:Lcom/google/android/apps/gmm/map/t/q;

    .line 88
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v1, "orange_accuracy_outline"

    invoke-virtual {p1, v0, v4, v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->d:Lcom/google/android/apps/gmm/map/t/q;

    goto :goto_0

    .line 91
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/f;->f:I

    const-string v1, "breadcrumb_green_dot"

    invoke-virtual {p1, v0, v1, v2, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->a:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 92
    sget v0, Lcom/google/android/apps/gmm/f;->g:I

    const-string v1, "breadcrumb_green_pointer"

    invoke-virtual {p1, v0, v1, v2, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->b:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 93
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v1, "green_accuracy_fill"

    invoke-virtual {p1, v0, v2, v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->c:Lcom/google/android/apps/gmm/map/t/q;

    .line 94
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v1, "green_accuracy_outline"

    invoke-virtual {p1, v0, v4, v1, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->d:Lcom/google/android/apps/gmm/map/t/q;

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
