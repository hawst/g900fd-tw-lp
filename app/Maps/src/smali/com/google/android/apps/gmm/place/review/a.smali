.class public Lcom/google/android/apps/gmm/place/review/a;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/b/h/c;

.field private b:Lcom/google/android/apps/gmm/place/review/b;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/b/h/c;Ljava/lang/String;Lcom/google/android/apps/gmm/place/review/b;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 35
    sget-object v0, Lcom/google/r/b/a/el;->aS:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ai;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/review/a;->c:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/a;->a:Lcom/google/b/h/c;

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/review/a;->b:Lcom/google/android/apps/gmm/place/review/b;

    .line 39
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 56
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v0

    .line 57
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/ai;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 44
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/a;->a:Lcom/google/b/h/c;

    invoke-virtual {v2}, Lcom/google/b/h/c;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 46
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/a;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/a;->c:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 50
    :cond_0
    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/a;->b:Lcom/google/android/apps/gmm/place/review/b;

    if-eqz v0, :cond_0

    .line 70
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 71
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/a;->b:Lcom/google/android/apps/gmm/place/review/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/place/review/b;->a(Z)V

    .line 73
    :cond_0
    return-void

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
