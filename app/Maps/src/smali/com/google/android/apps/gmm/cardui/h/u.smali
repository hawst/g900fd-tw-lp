.class Lcom/google/android/apps/gmm/cardui/h/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/o;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/cardui/h/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/cardui/h/t;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/u;->a:Lcom/google/android/apps/gmm/cardui/h/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/u;->a:Lcom/google/android/apps/gmm/cardui/h/t;

    .line 50
    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    iget-object v0, v0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    sget v1, Lcom/google/android/apps/gmm/d;->Y:I

    .line 49
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Lcom/google/o/h/a/hv;I)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/u;->a:Lcom/google/android/apps/gmm/cardui/h/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/h/t;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/Float;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/u;->a:Lcom/google/android/apps/gmm/cardui/h/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/h/t;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    const/high16 v0, 0x40a00000    # 5.0f

    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method public final h()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 76
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
