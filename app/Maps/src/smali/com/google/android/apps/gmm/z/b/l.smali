.class public Lcom/google/android/apps/gmm/z/b/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:[Lcom/google/b/f/cq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/b/f/b/a/dj;

.field public final e:Lcom/google/r/b/a/tf;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 66
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/b/l;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/google/b/f/cq;Lcom/google/b/f/b/a/dj;Lcom/google/r/b/a/tf;)V

    .line 67
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;[Lcom/google/b/f/cq;Lcom/google/b/f/b/a/dj;Lcom/google/r/b/a/tf;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    .line 75
    iput-object p4, p0, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    .line 76
    iput-object p5, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    .line 77
    return-void
.end method

.method public static a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/google/android/apps/gmm/z/b/m;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;
    .locals 3
    .param p0    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 90
    if-nez p0, :cond_0

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    .line 99
    :goto_0
    return-object v0

    .line 94
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    invoke-static {}, Lcom/google/b/f/b/a/dj;->newBuilder()Lcom/google/b/f/b/a/dl;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/b/f/b/a/dl;->a(Lcom/google/b/f/b/a/dj;)Lcom/google/b/f/b/a/dl;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->d:Lcom/google/b/f/b/a/dl;

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    goto :goto_0
.end method


# virtual methods
.method public final b(Lcom/google/android/apps/gmm/z/b/l;)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182
    if-nez p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 186
    :cond_1
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    if-eq v3, v4, :cond_3

    .line 187
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 188
    :cond_3
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    if-eq v3, v4, :cond_5

    .line 189
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    .line 190
    :cond_5
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    aput-object v5, v4, v2

    .line 191
    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    if-eq v3, v4, :cond_7

    .line 192
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    aput-object v5, v4, v2

    .line 193
    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    .line 192
    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_6

    move v0, v1

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto/16 :goto_0

    .line 195
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    iget-object v4, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v4

    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_8

    move v0, v1

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    .line 213
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_1
    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    .line 214
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0

    :cond_4
    move v2, v0

    .line 212
    goto :goto_0

    :cond_5
    move v2, v0

    .line 213
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 164
    instance-of v2, p1, Lcom/google/android/apps/gmm/z/b/l;

    if-eqz v2, :cond_7

    .line 165
    check-cast p1, Lcom/google/android/apps/gmm/z/b/l;

    .line 166
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    .line 167
    if-eq v2, v3, :cond_1

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    .line 168
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    .line 169
    invoke-virtual {v2}, Lcom/google/b/f/b/a/dj;->k()Lcom/google/n/f;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    .line 170
    invoke-virtual {v3}, Lcom/google/b/f/b/a/dj;->k()Lcom/google/n/f;

    move-result-object v3

    .line 169
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    iget-object v3, p1, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    if-ne v2, v3, :cond_6

    .line 173
    :goto_3
    return v0

    :cond_3
    move v2, v1

    .line 166
    goto :goto_0

    :cond_4
    move v2, v1

    .line 167
    goto :goto_1

    :cond_5
    move v2, v1

    .line 169
    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move v0, v1

    .line 173
    goto :goto_3
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 158
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    .line 159
    invoke-virtual {v2}, Lcom/google/b/f/b/a/dj;->k()Lcom/google/n/f;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    aput-object v2, v0, v1

    .line 158
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 144
    const-string v0, "UE3_LOGGING_COMMON_PARAMS"

    new-instance v4, Lcom/google/b/a/ak;

    invoke-direct {v4, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/b/a/ak;->b:Z

    const-string v0, "SERVER_EI"

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    .line 146
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "SERVER_VED"

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    .line 147
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "CLIENT_VE_TREE"

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    .line 148
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v1, "AD_REDIRECT_URL"

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 150
    :goto_0
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    .line 151
    iget-object v2, v0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/b/f/b/a/a;->d()Lcom/google/b/f/b/a/a;

    move-result-object v0

    move-object v2, v0

    :goto_1
    iget-object v0, v2, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v0, v0, Lcom/google/b/f/b/a/dj;->c:Lcom/google/b/f/b/a/a;

    move-object v2, v0

    goto :goto_1

    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v3, v2, Lcom/google/b/f/b/a/a;->b:Ljava/lang/Object;

    :cond_6
    move-object v0, v3

    goto :goto_0

    :cond_7
    move-object v0, v1

    .line 150
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "PREFETCH_UPGRADE_TYPE"

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    .line 152
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 153
    invoke-virtual {v4}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
