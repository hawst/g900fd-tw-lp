.class public Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;
.super Landroid/opengl/GLSurfaceView;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/streetview/internal/an;
.implements Lcom/google/android/apps/gmm/streetview/internal/ap;
.implements Lcom/google/android/apps/gmm/v/bf;


# static fields
.field private static final o:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/streetview/internal/v;

.field public final b:Lcom/google/android/apps/gmm/map/c/a;

.field public final c:Lcom/google/android/apps/gmm/streetview/internal/ac;

.field public d:Lcom/google/android/apps/gmm/streetview/internal/aq;

.field public volatile e:Lcom/google/android/apps/gmm/map/b/a/q;

.field public f:Lcom/google/android/apps/gmm/streetview/internal/b;

.field public g:Lcom/google/android/apps/gmm/streetview/internal/ak;

.field public h:Lcom/google/android/apps/gmm/streetview/internal/ar;

.field i:Lcom/google/android/apps/gmm/streetview/internal/aa;

.field private j:Lcom/google/android/apps/gmm/v/ad;

.field private k:Lcom/google/android/apps/gmm/streetview/b/a;

.field private l:Lcom/google/android/apps/gmm/streetview/internal/q;

.field private m:Lcom/google/android/apps/gmm/map/m/n;

.field private final n:Lcom/google/android/apps/gmm/streetview/internal/o;

.field private p:Lcom/google/android/apps/gmm/streetview/internal/t;

.field private q:I

.field private final r:Lcom/google/android/apps/gmm/streetview/internal/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->o:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;ZLjava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 9
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/u;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/streetview/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 160
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->q:I

    .line 123
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/av;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/internal/av;-><init>(Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->r:Lcom/google/android/apps/gmm/streetview/internal/ae;

    .line 161
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 163
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    const/high16 v1, 0x20000

    if-lt v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_6

    .line 166
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->setEGLContextClientVersion(I)V

    .line 171
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    .line 173
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 174
    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/g;

    const/4 v3, 0x3

    const/16 v4, 0x64

    invoke-direct {v2, v3, v1, v4, v0}, Lcom/google/android/apps/gmm/streetview/internal/g;-><init>(ILjava/lang/String;ILcom/google/android/apps/gmm/map/util/a/b;)V

    .line 176
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ac;

    invoke-direct {v1, p2, v2}, Lcom/google/android/apps/gmm/streetview/internal/ac;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/streetview/internal/g;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->c:Lcom/google/android/apps/gmm/streetview/internal/ac;

    .line 178
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/v;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/streetview/internal/v;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a:Lcom/google/android/apps/gmm/streetview/internal/v;

    .line 180
    if-eqz p3, :cond_7

    new-instance v0, Lcom/google/android/apps/gmm/v/ad;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/apps/gmm/v/bh;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/4 v4, 0x4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/16 v4, 0x8

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/16 v4, 0x10

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/ad;-><init>([Lcom/google/android/apps/gmm/v/bh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    :goto_1
    new-instance v0, Lcom/google/android/apps/gmm/v/bu;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    invoke-direct {v0, v1, p3}, Lcom/google/android/apps/gmm/v/bu;-><init>(Lcom/google/android/apps/gmm/v/ad;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/aq;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/streetview/internal/aq;-><init>(Lcom/google/android/apps/gmm/v/bu;Landroid/util/DisplayMetrics;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    if-eqz p3, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/n;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/n;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    if-eqz p6, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    invoke-virtual {v0, p6}, Lcom/google/android/apps/gmm/streetview/internal/aq;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/gmm/streetview/internal/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/streetview/internal/aq;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    if-eqz p3, :cond_9

    const/4 v2, 0x3

    :goto_3
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->c:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/streetview/internal/ak;-><init>(Lcom/google/android/apps/gmm/v/ad;ILcom/google/android/apps/gmm/streetview/internal/b;Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    const/16 v1, 0xa

    iput v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->b:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    if-eqz p3, :cond_a

    const/16 v0, 0xc

    :goto_4
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v2, v0, p0, v3}, Lcom/google/android/apps/gmm/streetview/internal/q;-><init>(Lcom/google/android/apps/gmm/v/ad;ILcom/google/android/apps/gmm/streetview/internal/ap;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->l:Lcom/google/android/apps/gmm/streetview/internal/q;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->l:Lcom/google/android/apps/gmm/streetview/internal/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/ar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/apps/gmm/streetview/internal/ar;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aq;Lcom/google/android/apps/gmm/streetview/internal/b;Lcom/google/android/apps/gmm/streetview/internal/ak;Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->h:Lcom/google/android/apps/gmm/streetview/internal/ar;

    new-instance v0, Lcom/google/android/apps/gmm/map/m/n;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->h:Lcom/google/android/apps/gmm/streetview/internal/ar;

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/gmm/map/m/n;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/r;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->m:Lcom/google/android/apps/gmm/map/m/n;

    if-nez p3, :cond_2

    new-instance v0, Lcom/google/android/apps/gmm/v/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->c:Lcom/google/android/apps/gmm/v/n;

    const v2, 0x3d3851eb    # 0.044999998f

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/v/ac;-><init>(Lcom/google/android/apps/gmm/v/n;F)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iput-object v0, v1, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->h:Lcom/google/android/apps/gmm/streetview/internal/ar;

    iput-object v0, v1, Lcom/google/android/apps/gmm/streetview/internal/ar;->a:Lcom/google/android/apps/gmm/v/bc;

    .line 186
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->n:Lcom/google/android/apps/gmm/streetview/internal/o;

    .line 193
    if-eqz p3, :cond_4

    .line 194
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->setKeepScreenOn(Z)V

    .line 195
    const/4 v0, 0x6

    .line 196
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_3

    .line 197
    const/16 v0, 0x1806

    .line 199
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->setSystemUiVisibility(I)V

    .line 202
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, p4, p5, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 203
    return-void

    .line 163
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 168
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to created gles20 context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/v/ad;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/apps/gmm/v/bh;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/4 v4, 0x4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/google/android/apps/gmm/v/ba;

    const/16 v4, 0x10

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/ad;-><init>([Lcom/google/android/apps/gmm/v/bh;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/n;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->j:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->d:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/aq;->c:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/n;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 184
    :cond_9
    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_a
    const/4 v0, 0x4

    goto/16 :goto_4
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/p;->b(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)F

    move-result v0

    .line 437
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 438
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v1

    .line 439
    iput v0, v1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 442
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/aa;Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/streetview/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 299
    :goto_1
    return-void

    .line 294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 297
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->i:Lcom/google/android/apps/gmm/streetview/internal/aa;

    .line 298
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/streetview/internal/t;)V
    .locals 13

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 320
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->p:Lcom/google/android/apps/gmm/streetview/internal/t;

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a:Lcom/google/android/apps/gmm/streetview/internal/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/v;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 322
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a(Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    .line 324
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->a:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->i:Lcom/google/android/apps/gmm/streetview/internal/aa;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    .line 325
    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->i:Lcom/google/android/apps/gmm/streetview/internal/aa;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    const/high16 v4, 0x43b40000    # 360.0f

    div-float/2addr v0, v4

    invoke-static {v0}, Lcom/google/android/apps/gmm/streetview/internal/ao;->b(F)F

    move-result v4

    invoke-static {v0}, Lcom/google/android/apps/gmm/streetview/internal/ao;->c(F)F

    move-result v5

    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    neg-float v5, v5

    invoke-direct {v0, v4, v3, v5}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-object v4, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)D

    move-result-wide v4

    double-to-float v1, v4

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v4, v8

    mul-float/2addr v5, v1

    aput v5, v4, v8

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v4, v12

    mul-float/2addr v5, v1

    aput v5, v4, v12

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x2

    aget v6, v4, v5

    mul-float/2addr v1, v6

    aput v1, v4, v5

    .line 328
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    invoke-virtual {v1, p1, v0, p0}, Lcom/google/android/apps/gmm/streetview/internal/ak;->a(Lcom/google/android/apps/gmm/streetview/internal/t;Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/streetview/internal/an;)V

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget v4, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->b:I

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->a:Z

    if-eqz v1, :cond_3

    .line 332
    iget v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    iget v5, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    invoke-static {v1, v5}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF)Lcom/google/android/apps/gmm/v/cj;

    move-result-object v1

    .line 335
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v5, v0, v1, v4}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 338
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->l:Lcom/google/android/apps/gmm/streetview/internal/q;

    if-eqz v0, :cond_9

    .line 339
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->l:Lcom/google/android/apps/gmm/streetview/internal/q;

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-eqz v0, :cond_6

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-object v6, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v6, v6, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v7, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v7, v0, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_1

    .line 325
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->o:Lcom/google/android/apps/gmm/v/cn;

    goto :goto_0

    .line 339
    :cond_5
    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v6, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v6, v1, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :cond_6
    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-eqz v0, :cond_8

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-object v6, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v6, v6, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v7, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v7, v0, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_2

    :cond_7
    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v6, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v6, v1, v8}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/w;-><init>(Ljava/util/Collection;FFILcom/google/android/apps/gmm/streetview/internal/x;)V

    iput-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v6, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v6, v1, v12}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v7, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    move v8, v3

    move v9, v2

    move v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/gmm/streetview/internal/w;-><init>(Ljava/util/Collection;FFILcom/google/android/apps/gmm/streetview/internal/x;)V

    iput-object v6, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v1, v12}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    iget-object v1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    iput-object p1, v5, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, v5, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 342
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/au;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/streetview/internal/au;-><init>(Lcom/google/android/apps/gmm/streetview/internal/t;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 343
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/n;)V
    .locals 5

    .prologue
    .line 446
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 447
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    .line 448
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v1

    .line 449
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->q:I

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 450
    iget v2, v1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    int-to-float v3, v0

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->q:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    const/high16 v4, 0x42b40000    # 90.0f

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/b/a;->a(FFF)F

    move-result v2

    iput v2, v1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 452
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 453
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->q:I

    .line 454
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 10
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/u;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/streetview/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 305
    iput-object p3, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->k:Lcom/google/android/apps/gmm/streetview/b/a;

    .line 307
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a:Lcom/google/android/apps/gmm/streetview/internal/v;

    if-nez p1, :cond_2

    move-object v0, v3

    .line 308
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 309
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    if-nez v0, :cond_5

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->c:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->r:Lcom/google/android/apps/gmm/streetview/internal/ae;

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Lcom/google/android/apps/gmm/streetview/internal/ae;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Z)Z

    .line 317
    :goto_2
    return-void

    .line 307
    :cond_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/v;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/streetview/internal/t;->a()J

    move-result-wide v6

    iget-wide v8, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->A:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x6ddd00

    cmp-long v4, v6, v8

    if-lez v4, :cond_3

    move v4, v2

    :goto_3
    if-eqz v4, :cond_0

    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/v;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    goto :goto_0

    :cond_3
    move v4, v1

    goto :goto_3

    :cond_4
    move v0, v1

    .line 309
    goto :goto_1

    .line 312
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->c:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->r:Lcom/google/android/apps/gmm/streetview/internal/ae;

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/apps/gmm/streetview/internal/ac;->a(Lcom/google/android/apps/gmm/streetview/internal/ae;Lcom/google/android/apps/gmm/map/b/a/u;Z)Z

    goto :goto_2

    .line 315
    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a(Lcom/google/android/apps/gmm/streetview/internal/t;)V

    goto :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->k:Lcom/google/android/apps/gmm/streetview/b/a;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->k:Lcom/google/android/apps/gmm/streetview/b/a;

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/streetview/b/a;I)V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->k:Lcom/google/android/apps/gmm/streetview/b/a;

    .line 464
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 390
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->n:Lcom/google/android/apps/gmm/streetview/internal/o;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->n:Lcom/google/android/apps/gmm/streetview/internal/o;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/o;->b:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/o;->b:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/o;->a:Lcom/google/android/apps/gmm/streetview/internal/p;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/p;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 395
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 378
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->n:Lcom/google/android/apps/gmm/streetview/internal/o;

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->n:Lcom/google/android/apps/gmm/streetview/internal/o;

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/o;->a:Lcom/google/android/apps/gmm/streetview/internal/p;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/o;->b:Ljava/lang/Thread;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/o;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 386
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 347
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->m:Lcom/google/android/apps/gmm/map/m/n;

    .line 348
    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/m/n;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method
