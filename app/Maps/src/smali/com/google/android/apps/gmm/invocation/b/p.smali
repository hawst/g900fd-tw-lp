.class public Lcom/google/android/apps/gmm/invocation/b/p;
.super Lcom/google/android/apps/gmm/invocation/b/e;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/r/b/a/gb;->d:Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method public final b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x1a

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    const/4 v4, 0x2

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p1, v4, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 31
    :goto_2
    if-nez v0, :cond_4

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v1, "No search request in response."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 29
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 34
    :cond_4
    new-instance v4, Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/search/ai;-><init>(Lcom/google/e/a/a/a/b;)V

    .line 35
    const/4 v5, 0x6

    if-eqz p1, :cond_8

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_6

    move v0, v3

    :goto_3
    if-nez v0, :cond_5

    invoke-virtual {p1, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    :cond_5
    move v0, v3

    :goto_4
    if-eqz v0, :cond_8

    invoke-virtual {p1, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 37
    :goto_5
    if-nez v0, :cond_9

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/q;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/gmm/invocation/b/q;-><init>(Lcom/google/android/apps/gmm/invocation/b/p;Lcom/google/android/apps/gmm/search/ai;)V

    .line 50
    :goto_6
    return-object v0

    :cond_6
    move v0, v2

    .line 35
    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move-object v0, v1

    goto :goto_5

    .line 48
    :cond_9
    new-instance v1, Lcom/google/android/apps/gmm/search/al;

    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/e/a/a/a/b;)V

    .line 49
    invoke-static {v1}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/r;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/r;-><init>(Lcom/google/android/apps/gmm/invocation/b/p;Lcom/google/android/apps/gmm/x/o;)V

    goto :goto_6
.end method
