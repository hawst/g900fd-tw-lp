.class public Lcom/google/android/apps/gmm/car/f/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/libraries/curvular/aq;

.field final c:Lcom/google/android/apps/gmm/directions/a/a;

.field final d:Lcom/google/android/apps/gmm/car/e/m;

.field final e:Lcom/google/android/apps/gmm/car/w;

.field final f:Lcom/google/android/apps/gmm/car/h/p;

.field final g:Lcom/google/android/apps/gmm/car/e/t;

.field final h:Lcom/google/android/apps/gmm/car/ac;

.field final i:Ljava/lang/Runnable;

.field final j:Lcom/google/android/apps/gmm/car/f/r;

.field k:Lcom/google/android/apps/gmm/car/bm;

.field l:Lcom/google/android/apps/gmm/car/f/y;

.field private final m:Lcom/google/android/apps/gmm/car/f/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/car/f/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/f/p;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/directions/a/a;Lcom/google/android/apps/gmm/car/e/m;Lcom/google/android/apps/gmm/car/w;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/h/p;Lcom/google/android/apps/gmm/car/e/t;Lcom/google/android/apps/gmm/car/ac;Ljava/lang/Runnable;Lcom/google/android/apps/gmm/car/f/r;Lcom/google/android/apps/gmm/car/f/o;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/google/android/apps/gmm/car/n/a;->r:Lcom/google/android/libraries/curvular/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->b:Lcom/google/android/libraries/curvular/aq;

    .line 59
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/directions/a/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/p;->c:Lcom/google/android/apps/gmm/directions/a/a;

    .line 60
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/e/m;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/f/p;->d:Lcom/google/android/apps/gmm/car/e/m;

    .line 61
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/w;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/f/p;->e:Lcom/google/android/apps/gmm/car/w;

    .line 62
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move-object v0, p4

    check-cast v0, Lcom/google/android/apps/gmm/car/bm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->k:Lcom/google/android/apps/gmm/car/bm;

    .line 63
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p5, Lcom/google/android/apps/gmm/car/h/p;

    iput-object p5, p0, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    .line 65
    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast p6, Lcom/google/android/apps/gmm/car/e/t;

    iput-object p6, p0, Lcom/google/android/apps/gmm/car/f/p;->g:Lcom/google/android/apps/gmm/car/e/t;

    .line 66
    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast p7, Lcom/google/android/apps/gmm/car/ac;

    iput-object p7, p0, Lcom/google/android/apps/gmm/car/f/p;->h:Lcom/google/android/apps/gmm/car/ac;

    .line 67
    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast p8, Ljava/lang/Runnable;

    iput-object p8, p0, Lcom/google/android/apps/gmm/car/f/p;->i:Ljava/lang/Runnable;

    .line 68
    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast p9, Lcom/google/android/apps/gmm/car/f/r;

    iput-object p9, p0, Lcom/google/android/apps/gmm/car/f/p;->j:Lcom/google/android/apps/gmm/car/f/r;

    .line 69
    if-nez p10, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast p10, Lcom/google/android/apps/gmm/car/f/o;

    iput-object p10, p0, Lcom/google/android/apps/gmm/car/f/p;->m:Lcom/google/android/apps/gmm/car/f/o;

    .line 71
    invoke-virtual {p0, p4}, Lcom/google/android/apps/gmm/car/f/p;->a(Lcom/google/android/apps/gmm/car/bm;)V

    .line 72
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 145
    sget-object v0, Lcom/google/android/apps/gmm/car/f/p;->a:Ljava/lang/String;

    const-string v0, "onDirectionsFetched(): state %s"

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/p;->k:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/car/f/q;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/p;->k:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/bn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 160
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unrecognized directions state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->d()Lcom/google/android/apps/gmm/car/f/y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/f/p;->a(Lcom/google/android/apps/gmm/car/f/y;)V

    .line 155
    :goto_0
    return-void

    .line 151
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/car/f/y;->a(Z)Lcom/google/android/apps/gmm/car/f/y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/f/p;->a(Lcom/google/android/apps/gmm/car/f/y;)V

    goto :goto_0

    .line 154
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/car/f/y;->a(Z)Lcom/google/android/apps/gmm/car/f/y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/f/p;->a(Lcom/google/android/apps/gmm/car/f/y;)V

    goto :goto_0

    .line 157
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call onDirectionsFetched while directions pending."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/car/bm;)V
    .locals 2

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/p;->k:Lcom/google/android/apps/gmm/car/bm;

    .line 76
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/car/bn;->b:Lcom/google/android/apps/gmm/car/bn;

    if-ne v0, v1, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/car/f/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/f/u;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    .line 85
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->m:Lcom/google/android/apps/gmm/car/f/o;

    sget-object v1, Lcom/google/android/apps/gmm/car/f/o;->c:Lcom/google/android/apps/gmm/car/f/o;

    if-ne v0, v1, :cond_1

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/car/f/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/f/aa;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->m:Lcom/google/android/apps/gmm/car/f/o;

    sget-object v1, Lcom/google/android/apps/gmm/car/f/o;->b:Lcom/google/android/apps/gmm/car/f/o;

    if-ne v0, v1, :cond_2

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/car/f/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/f/ab;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    goto :goto_0

    .line 83
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/car/f/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/f/s;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    goto :goto_0
.end method

.method a(Lcom/google/android/apps/gmm/car/f/y;)V
    .locals 4

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/apps/gmm/car/f/p;->a:Ljava/lang/String;

    const-string v0, "enterState(): state %s -> %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    if-eq v0, p1, :cond_0

    .line 186
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->h()V

    .line 189
    :cond_0
    return-void
.end method
