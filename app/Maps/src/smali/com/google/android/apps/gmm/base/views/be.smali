.class public Lcom/google/android/apps/gmm/base/views/be;
.super Landroid/widget/PopupMenu;
.source "PG"


# static fields
.field static volatile a:Z

.field static b:Landroid/widget/PopupMenu$OnDismissListener;

.field private static final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/base/views/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/views/be;->c:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/base/views/bf;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/bf;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/be;->b:Landroid/widget/PopupMenu$OnDismissListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 38
    return-void
.end method


# virtual methods
.method public show()V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 43
    sget-boolean v0, Lcom/google/android/apps/gmm/base/views/be;->a:Z

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lcom/google/android/apps/gmm/base/views/be;->c:Ljava/lang/String;

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/be;->b:Landroid/widget/PopupMenu$OnDismissListener;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/be;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/gmm/base/views/be;->a:Z

    invoke-super {p0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method
