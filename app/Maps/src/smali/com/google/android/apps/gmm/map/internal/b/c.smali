.class public Lcom/google/android/apps/gmm/map/internal/b/c;
.super Lcom/google/android/apps/gmm/map/internal/b/f;
.source "PG"


# instance fields
.field private b:Lcom/google/android/apps/gmm/map/internal/b/e;

.field private c:Lcom/google/android/apps/gmm/map/internal/b/d;

.field private d:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/cw;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/b/f;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cw;)V

    .line 26
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/ai;ZLcom/google/android/apps/gmm/map/internal/c/ce;)Lcom/google/android/apps/gmm/map/internal/b/d;
    .locals 1

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->c:Lcom/google/android/apps/gmm/map/internal/b/d;

    if-nez v0, :cond_0

    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/b/f;->a(Lcom/google/android/apps/gmm/map/b/a/ai;ZLcom/google/android/apps/gmm/map/internal/c/ce;)Lcom/google/android/apps/gmm/map/internal/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->c:Lcom/google/android/apps/gmm/map/internal/b/d;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->c:Lcom/google/android/apps/gmm/map/internal/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/ai;IZLcom/google/android/apps/gmm/map/internal/c/ce;)Lcom/google/android/apps/gmm/map/internal/b/e;
    .locals 1

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->b:Lcom/google/android/apps/gmm/map/internal/b/e;

    if-nez v0, :cond_0

    .line 33
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/internal/b/f;->a(Lcom/google/android/apps/gmm/map/b/a/ai;IZLcom/google/android/apps/gmm/map/internal/c/ce;)Lcom/google/android/apps/gmm/map/internal/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->b:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 35
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->e:I

    .line 36
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->f:Z

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/c;->b:Lcom/google/android/apps/gmm/map/internal/b/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
