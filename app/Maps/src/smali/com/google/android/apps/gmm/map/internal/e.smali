.class final enum Lcom/google/android/apps/gmm/map/internal/e;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/e;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/e;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/e;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/map/internal/e;


# instance fields
.field d:I

.field e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 135
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/e;

    const-string v1, "LOW"

    const v2, -0xff0100

    const/4 v3, 0x4

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/gmm/map/internal/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/e;->a:Lcom/google/android/apps/gmm/map/internal/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/e;

    const-string v1, "MEDIUM"

    const/16 v2, -0x100

    const/16 v3, 0xc

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/gmm/map/internal/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/e;->b:Lcom/google/android/apps/gmm/map/internal/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/e;

    const-string v1, "HIGH"

    const/high16 v2, -0x10000

    const/16 v3, 0x14

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/map/internal/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/e;->c:Lcom/google/android/apps/gmm/map/internal/e;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/e;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->a:Lcom/google/android/apps/gmm/map/internal/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->b:Lcom/google/android/apps/gmm/map/internal/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/e;->c:Lcom/google/android/apps/gmm/map/internal/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/e;->f:[Lcom/google/android/apps/gmm/map/internal/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 145
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/e;->d:I

    .line 146
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/e;->e:I

    .line 147
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/e;
    .locals 1

    .prologue
    .line 135
    const-class v0, Lcom/google/android/apps/gmm/map/internal/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/e;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/e;->f:[Lcom/google/android/apps/gmm/map/internal/e;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/e;

    return-object v0
.end method
