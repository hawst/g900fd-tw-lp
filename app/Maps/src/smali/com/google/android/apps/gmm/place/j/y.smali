.class public Lcom/google/android/apps/gmm/place/j/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/j;


# static fields
.field private static final f:[I


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/android/apps/gmm/map/r/b/a;

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private final g:Lcom/google/android/apps/gmm/directions/a/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/o/h/a/od;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/i/j;",
            ">;"
        }
    .end annotation
.end field

.field private final k:I

.field private l:Ljava/lang/CharSequence;

.field private final m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Float;

.field private final o:Lcom/google/android/apps/gmm/z/b/l;

.field private final p:Lcom/google/android/apps/gmm/place/j/z;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/gmm/f;->bs:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/gmm/f;->bt:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/gmm/f;->bu:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/place/j/y;->f:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/libraries/curvular/cg;ILcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/a/f;Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 12
    .param p5    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/directions/a/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/base/g/c;",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/i/j;",
            ">;I",
            "Lcom/google/android/apps/gmm/map/r/b/a;",
            "Z",
            "Lcom/google/android/apps/gmm/directions/a/f;",
            "Lcom/google/android/apps/gmm/z/b/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    .line 116
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->h:Lcom/google/o/h/a/od;

    .line 117
    invoke-static {p2}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    .line 118
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v2, v2, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/acq;

    iget-object v2, v2, Lcom/google/r/b/a/acq;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ee;->d()Lcom/google/maps/g/ee;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/ee;

    iget-object v2, v2, Lcom/google/maps/g/ee;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/uf;->h()Lcom/google/maps/g/uf;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/uf;

    invoke-virtual {v2}, Lcom/google/maps/g/uf;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->i:Ljava/lang/String;

    .line 119
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/j/y;->j:Lcom/google/android/libraries/curvular/cg;

    .line 120
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/apps/gmm/place/j/y;->k:I

    .line 121
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 122
    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->m:Ljava/lang/Boolean;

    .line 123
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->g:Lcom/google/android/apps/gmm/directions/a/f;

    .line 124
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->o:Lcom/google/android/apps/gmm/z/b/l;

    .line 125
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_1

    const-string v2, " "

    const-string v5, "\u00a0"

    invoke-virtual {v3, v2, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " \u00b7 "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->d:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.telephony"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    .line 131
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->n:Ljava/lang/Float;

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/j/y;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v5

    if-nez v5, :cond_7

    :cond_2
    const/4 v2, 0x0

    :goto_2
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->n:Ljava/lang/Float;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->n:Ljava/lang/Float;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->n()Ljava/lang/String;

    move-result-object v4

    .line 130
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const v6, 0x481d299a

    cmpg-float v5, v5, v6

    if-gez v5, :cond_8

    :cond_4
    sget-object v2, Lcom/google/android/apps/gmm/place/j/z;->c:Lcom/google/android/apps/gmm/place/j/z;

    :goto_3
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    .line 132
    return-void

    .line 125
    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    .line 131
    :cond_7
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/n;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide v8, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const-wide v10, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v8, v10

    double-to-int v2, v8

    invoke-direct {v5, v6, v2}, Lcom/google/android/apps/gmm/map/b/a/n;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/n;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_2

    .line 130
    :cond_8
    if-eqz v3, :cond_b

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const v3, 0x481d299a

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_b

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_a

    :cond_9
    const/4 v2, 0x1

    :goto_4
    if-nez v2, :cond_b

    sget-object v2, Lcom/google/android/apps/gmm/place/j/z;->b:Lcom/google/android/apps/gmm/place/j/z;

    goto :goto_3

    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    sget-object v2, Lcom/google/android/apps/gmm/place/j/z;->a:Lcom/google/android/apps/gmm/place/j/z;

    goto :goto_3
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 152
    new-instance v4, Lcom/google/android/apps/gmm/y/a;

    .line 153
    sget v1, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/gmm/y/a;-><init>(Landroid/content/Context;I)V

    .line 156
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->B()Lcom/google/android/apps/gmm/y/f;

    move-result-object v5

    .line 157
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    .line 158
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v6

    .line 155
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz v1, :cond_1

    iget-object v0, v4, Lcom/google/android/apps/gmm/y/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->ku:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v4, Lcom/google/android/apps/gmm/y/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    :goto_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v0

    :cond_1
    if-eqz v6, :cond_2

    iget-object v0, v4, Lcom/google/android/apps/gmm/y/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->kw:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v4, Lcom/google/android/apps/gmm/y/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    iget-object v1, v5, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v1, :cond_6

    iget-object v1, v5, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v1, v1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_5

    move v1, v2

    :goto_1
    if-eqz v1, :cond_6

    iget-object v1, v5, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v1, v1, Lcom/google/maps/g/jl;->d:I

    invoke-static {v1}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_3
    :goto_2
    if-eqz v1, :cond_0

    sget-object v6, Lcom/google/android/apps/gmm/y/b;->a:[I

    iget-object v1, v5, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v1, :cond_4

    iget-object v1, v5, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v1, v1, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_7

    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    iget-object v0, v5, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_4
    invoke-virtual {v0}, Lcom/google/maps/g/jq;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, v4, Lcom/google/android/apps/gmm/y/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->kr:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00a0"

    new-instance v6, Lcom/google/b/a/ab;

    invoke-direct {v6, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/y/a;->a:Landroid/content/Context;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/y/f;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4, v1}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v7, v4, v1, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    :cond_5
    move v1, v3

    goto/16 :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_2

    :cond_7
    move v1, v3

    goto/16 :goto_3

    :pswitch_1
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/y/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, v4, Lcom/google/android/apps/gmm/y/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->km:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v4, Lcom/google/android/apps/gmm/y/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/y/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->kl:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v4, Lcom/google/android/apps/gmm/y/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/x/o;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/r/b/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 308
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    if-eqz p1, :cond_0

    .line 312
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {p0, p2, v0}, Lcom/google/android/apps/gmm/base/views/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/String;

    move-result-object v2

    .line 317
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 318
    const-string v0, " \u00b7 "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 317
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->b:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_0

    .line 369
    const-string v0, ""

    .line 374
    :goto_0
    return-object v0

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->c:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_1

    .line 371
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 374
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final B()Lcom/google/android/apps/gmm/z/b/l;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->b:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_0

    .line 410
    sget-object v0, Lcom/google/b/f/t;->eh:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 417
    :goto_0
    return-object v0

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->c:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_1

    .line 413
    sget-object v0, Lcom/google/b/f/t;->ei:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 417
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Lcom/google/android/libraries/curvular/cf;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v2, Lcom/google/android/apps/gmm/place/j/z;->b:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v2, :cond_2

    .line 381
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.DIAL"

    const-string v4, "tel: "

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    .line 382
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 403
    :cond_0
    :goto_1
    return-object v1

    .line 382
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v2, Lcom/google/android/apps/gmm/place/j/z;->c:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v2, :cond_0

    .line 389
    invoke-static {}, Lcom/google/r/b/a/afz;->newBuilder()Lcom/google/r/b/a/agb;

    move-result-object v0

    .line 390
    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v2

    sget-object v3, Lcom/google/maps/g/a/hr;->b:Lcom/google/maps/g/a/hr;

    if-nez v3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v4, v2, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Lcom/google/maps/g/a/hq;->a:I

    iget v3, v3, Lcom/google/maps/g/a/hr;->d:I

    iput v3, v2, Lcom/google/maps/g/a/hq;->c:I

    .line 389
    iget-object v3, v0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/a/hq;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/n/ao;->d:Z

    iget v2, v0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v0, Lcom/google/r/b/a/agb;->a:I

    .line 391
    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v4

    check-cast v4, Lcom/google/r/b/a/afz;

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->g:Lcom/google/android/apps/gmm/directions/a/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    .line 395
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->e()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v2, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 392
    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final D()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/j/y;->h()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->o:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->W()Lcom/google/r/b/a/aje;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    .line 188
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->W()Lcom/google/r/b/a/aje;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    .line 187
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 188
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 6

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/j/y;->d()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_0

    .line 205
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/k;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget-object v3, Lcom/google/android/apps/gmm/place/j/y;->f:[I

    iget v4, p0, Lcom/google/android/apps/gmm/place/j/y;->k:I

    sget-object v5, Lcom/google/android/apps/gmm/place/j/y;->f:[I

    rem-int/lit8 v4, v4, 0x3

    aget v3, v3, v4

    const/16 v4, 0xfa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 5

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/j/y;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    const/4 v0, 0x0

    .line 196
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->W()Lcom/google/r/b/a/aje;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    invoke-virtual {v0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v2

    iget v0, v0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    const/4 v3, 0x0

    const/16 v4, 0xfa

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final i()Ljava/lang/Float;
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    .line 239
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 245
    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 244
    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 245
    goto :goto_1
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v0

    .line 256
    if-lez v0, :cond_0

    const-string v1, "(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 267
    :goto_1
    return-object v0

    .line 266
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v0, " \u00b7 "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    .line 267
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->j:Lcom/google/android/libraries/curvular/cg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/i/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->j:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->G()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 4

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/y;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/y;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/j/y;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/x/o;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->e:Ljava/lang/String;

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 441
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 438
    goto :goto_0

    .line 441
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public final u()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->l:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/place/j/y;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->l:Ljava/lang/CharSequence;

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->l:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final v()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->l:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 466
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/place/j/y;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->l:Ljava/lang/CharSequence;

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->l:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 447
    sget v0, Lcom/google/android/apps/gmm/f;->fG:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v1}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 5

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->b:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->bf:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 351
    :goto_0
    return-object v0

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->c:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_1

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/y;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->u:I

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->b:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final z()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->b:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_0

    .line 357
    sget v0, Lcom/google/android/apps/gmm/f;->cI:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    .line 362
    :goto_0
    return-object v0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/y;->p:Lcom/google/android/apps/gmm/place/j/z;

    sget-object v1, Lcom/google/android/apps/gmm/place/j/z;->c:Lcom/google/android/apps/gmm/place/j/z;

    if-ne v0, v1, :cond_1

    .line 359
    sget v0, Lcom/google/android/apps/gmm/f;->cU:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0

    .line 362
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
