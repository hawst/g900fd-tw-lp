.class public Lcom/google/android/apps/gmm/iamhere/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/iamhere/f/f;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/iamhere/f/g;

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/iamhere/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/u;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;Lcom/google/android/apps/gmm/iamhere/c/o;Z)V
    .locals 4

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    .line 54
    iget-object v0, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->a(Lcom/google/o/b/a/v;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->e:Ljava/util/List;

    .line 55
    iget-object v0, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->d:Ljava/util/List;

    .line 56
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/iamhere/u;->g:Z

    .line 57
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 58
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/v;->a:[I

    iget-object v1, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/c/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->a:Lcom/google/android/apps/gmm/iamhere/f/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/u;->b:Ljava/lang/String;

    iget-object v1, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    :goto_1
    return-void

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 63
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->c:Lcom/google/android/apps/gmm/iamhere/f/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    goto :goto_1

    .line 66
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->a:Lcom/google/android/apps/gmm/iamhere/f/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    goto :goto_1

    .line 69
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->d:Lcom/google/android/apps/gmm/iamhere/f/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    goto :goto_1

    .line 72
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->e:Lcom/google/android/apps/gmm/iamhere/f/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    goto :goto_1

    .line 75
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->b:Lcom/google/android/apps/gmm/iamhere/f/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    goto :goto_1

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 4

    .prologue
    .line 86
    new-instance v1, Lcom/google/android/apps/gmm/base/l/ab;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->d:Ljava/util/List;

    .line 87
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/base/l/ab;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)V

    return-object v1
.end method

.method public final a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/suggest/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/suggest/k;-><init>()V

    .line 108
    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->g:Lcom/google/android/apps/gmm/suggest/e/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    sget v2, Lcom/google/android/apps/gmm/l;->ho:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/suggest/l;->b(Ljava/lang/String;)V

    .line 110
    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/suggest/l;->a(Z)V

    .line 112
    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/suggest/l;->b(Z)V

    .line 113
    const v1, 0x12000001

    iget-object v2, v0, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/suggest/l;->b(I)V

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->dismiss()V

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 118
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/suggest/k;)Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 121
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->dismiss()V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/e;->c()V

    .line 130
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->i()V

    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 175
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    .line 174
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/av;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 177
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/f/g;->a:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 211
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/f/g;->b:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/f/g;->d:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/f/g;->e:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->dismiss()V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/startpage/a/e;->b(Z)V

    .line 218
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->c:Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->dismiss()V

    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    sget v0, Lcom/google/android/apps/gmm/l;->oi:I

    .line 235
    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/f/g;->e:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v3, v4, :cond_1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_2

    .line 236
    sget v0, Lcom/google/android/apps/gmm/l;->ch:I

    .line 242
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v3, v2

    .line 235
    goto :goto_0

    .line 237
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/f/g;->d:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v3, v4, :cond_3

    move v3, v1

    :goto_2
    if-eqz v3, :cond_4

    .line 238
    sget v0, Lcom/google/android/apps/gmm/l;->ci:I

    goto :goto_1

    :cond_3
    move v3, v2

    .line 237
    goto :goto_2

    .line 239
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/u;->f:Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/f/g;->b:Lcom/google/android/apps/gmm/iamhere/f/g;

    if-ne v3, v4, :cond_5

    :goto_3
    if-eqz v1, :cond_0

    .line 240
    sget v0, Lcom/google/android/apps/gmm/l;->cg:I

    goto :goto_1

    :cond_5
    move v1, v2

    .line 239
    goto :goto_3
.end method

.method public final m()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/u;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->hf:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
