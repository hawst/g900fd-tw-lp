.class Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/cc;


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field c:Lcom/google/android/apps/gmm/map/internal/c/bn;

.field d:F

.field e:I

.field f:I

.field g:I

.field h:F

.field final synthetic i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;)V
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/by;Landroid/graphics/Canvas;)Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->d:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->g:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 497
    iget v0, p1, Lcom/google/android/apps/gmm/v/by;->a:I

    int-to-float v1, v0

    iget v0, p1, Lcom/google/android/apps/gmm/v/by;->b:I

    int-to-float v2, v0

    iget v0, p1, Lcom/google/android/apps/gmm/v/by;->c:I

    int-to-float v3, v0

    iget v0, p1, Lcom/google/android/apps/gmm/v/by;->d:I

    int-to-float v4, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->b:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->f:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 504
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->b:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->h:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 505
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->e:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 506
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->f:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->h:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    move v7, v8

    .line 507
    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->e:I

    if-eqz v1, :cond_3

    move v10, v8

    .line 509
    :goto_1
    iget v1, p1, Lcom/google/android/apps/gmm/v/by;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->h:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/2addr v2, v1

    .line 510
    iget v1, p1, Lcom/google/android/apps/gmm/v/by;->b:I

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v0, v0

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->h:F

    add-float/2addr v0, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    add-int/2addr v0, v1

    .line 511
    iget-object v11, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/d;->a:Ljava/lang/String;

    int-to-float v4, v2

    int-to-float v5, v0

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, v11, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->c:Landroid/graphics/Path;

    move v2, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    if-eqz v7, :cond_0

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->c:Landroid/graphics/Path;

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->b:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    if-eqz v10, :cond_1

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->c:Landroid/graphics/Path;

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 512
    :cond_1
    return v8

    :cond_2
    move v7, v9

    .line 506
    goto :goto_0

    :cond_3
    move v10, v9

    .line 507
    goto :goto_1
.end method
