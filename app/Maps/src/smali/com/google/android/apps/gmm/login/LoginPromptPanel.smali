.class public final Lcom/google/android/apps/gmm/login/LoginPromptPanel;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/login/c/e;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 71
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v1, Lcom/google/android/apps/gmm/n;->aa:[I

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v0, Lcom/google/android/apps/gmm/n;->ab:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    move v1, v0

    :goto_0
    if-ne v1, v6, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/login/b/c;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/gmm/login/c/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/login/c/d;-><init>(Lcom/google/android/apps/gmm/login/c/e;)V

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 72
    :goto_1
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->a:Ljava/lang/String;

    const-string v4, "default login panel is obsolete"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget v0, Lcom/google/android/apps/gmm/h;->V:I

    :goto_2
    invoke-virtual {v3, v0, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/apps/gmm/g;->bA:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-ne v1, v6, :cond_2

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->ij:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "http://support.google.com/gmm/bin/answer.py?hl=%s&answer=2803351"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_2
    sget v0, Lcom/google/android/apps/gmm/g;->bB:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/gmm/login/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/login/n;-><init>(Lcom/google/android/apps/gmm/login/LoginPromptPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/h;->W:I

    goto :goto_2

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/h;->X:I

    goto :goto_2

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->a:Ljava/lang/String;

    const-string v4, "transit login panel is obsolete"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget v0, Lcom/google/android/apps/gmm/h;->Y:I

    goto :goto_2

    :cond_3
    move v1, v2

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final o()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->go:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginPromptPanel;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/login/o;->a(Landroid/app/Activity;)V

    .line 154
    return-void
.end method
