.class Lcom/google/android/apps/gmm/map/internal/d/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/b/a/ai;

.field b:Lcom/google/b/a/an;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/b/a/an;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3109
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/j;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/j;->b:Lcom/google/b/a/an;

    .line 3110
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3128
    if-ne p1, p0, :cond_1

    .line 3135
    :cond_0
    :goto_0
    return v0

    .line 3130
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/d/j;

    if-eqz v2, :cond_3

    .line 3131
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/j;

    .line 3132
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/j;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/j;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/j;->b:Lcom/google/b/a/an;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/j;->b:Lcom/google/b/a/an;

    .line 3133
    invoke-virtual {v2, v3}, Lcom/google/b/a/an;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 3135
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 3119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/j;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 3122
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/j;->b:Lcom/google/b/a/an;

    invoke-virtual {v1}, Lcom/google/b/a/an;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3123
    return v0
.end method
