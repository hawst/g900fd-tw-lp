.class public Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field private a:Landroid/view/View;

.field private b:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/l;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/l;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/base/l/a/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Fragment;Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/directions/i/ad;",
            ">(T",
            "L;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;-><init>()V

    .line 46
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    invoke-virtual {p0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "listener_fragment"

    invoke-virtual {v2, v1, v3, p0}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 49
    const-string v2, "title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 51
    return-object v0
.end method


# virtual methods
.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/b/f/t;->aC:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/x;

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->b:Lcom/google/android/libraries/curvular/ae;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/y;

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->c:Lcom/google/android/libraries/curvular/ae;

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "listener_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/i/ad;

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ac;

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->fp:I

    .line 69
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->jE:I

    .line 70
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/gmm/l;->aZ:I

    .line 71
    invoke-virtual {p0, v6}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/i/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/directions/i/ad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->d:Lcom/google/android/apps/gmm/base/l/a/l;

    .line 72
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;I)Z

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->c:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;)V

    .line 106
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 76
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->aC:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->a:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->a:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;I)Z

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->d:Lcom/google/android/apps/gmm/base/l/a/l;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->c:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->d:Lcom/google/android/apps/gmm/base/l/a/l;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 83
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    .line 84
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    .line 86
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 87
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->b:Lcom/google/android/libraries/curvular/ae;

    .line 88
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->c:Lcom/google/android/libraries/curvular/ae;

    .line 89
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 90
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 91
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 92
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v6, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 93
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v6, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 94
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 95
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 96
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 98
    return-void
.end method
