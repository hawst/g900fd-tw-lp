.class Lcom/google/android/apps/gmm/place/aj;
.super Landroid/support/v4/view/cd;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-direct {p0}, Landroid/support/v4/view/cd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Lcom/google/android/apps/gmm/place/PlacePageViewPager;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->k:Landroid/support/v4/widget/ContentLoadingProgressBar;

    invoke-virtual {v0}, Landroid/support/v4/widget/ContentLoadingProgressBar;->a()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->c()V

    .line 214
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->K_()V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/ao;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/place/ao;-><init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 223
    :cond_1
    return-void

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->o:Z

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/place/am;->a(IZ)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 194
    if-nez p1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->m:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/e;->a()V

    .line 199
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->m:Lcom/google/android/apps/gmm/util/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/aj;->a:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->m:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/e;)V

    .line 202
    :cond_1
    return-void

    .line 199
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
