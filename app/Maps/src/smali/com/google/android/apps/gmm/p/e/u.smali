.class public Lcom/google/android/apps/gmm/p/e/u;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/shared/c/f;

.field b:Lcom/google/android/apps/gmm/map/r/a/w;

.field c:Lcom/google/android/apps/gmm/map/r/b/a;

.field d:J

.field final e:[F

.field f:I

.field final g:Lcom/google/android/apps/gmm/shared/net/a/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/p/e/u;->f:I

    .line 62
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/u;->a:Lcom/google/android/apps/gmm/shared/c/f;

    .line 63
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/u;->g:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 64
    const/16 v0, 0xa

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/u;->e:[F

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/u;->e:[F

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 66
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 183
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/u;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 184
    return-void
.end method
