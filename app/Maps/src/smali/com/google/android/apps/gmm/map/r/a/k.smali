.class public Lcom/google/android/apps/gmm/map/r/a/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/maps/g/a/cp;

.field public final b:Z


# direct methods
.method constructor <init>(Lcom/google/maps/g/a/cp;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/k;->a:Lcom/google/maps/g/a/cp;

    .line 24
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/r/a/k;->b:Z

    .line 25
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/l;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/k;->a:Lcom/google/maps/g/a/cp;

    invoke-virtual {v1}, Lcom/google/maps/g/a/cp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 88
    const-string v0, " "

    :goto_0
    return-object v0

    .line 61
    :pswitch_0
    const-string v0, "\u2191"

    goto :goto_0

    .line 63
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/k;->b:Z

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "\u2197"

    goto :goto_0

    .line 66
    :cond_0
    const-string v0, "\u2196"

    goto :goto_0

    .line 68
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/k;->b:Z

    if-eqz v0, :cond_1

    .line 69
    const-string v0, "\u21b1"

    goto :goto_0

    .line 71
    :cond_1
    const-string v0, "\u21b0"

    goto :goto_0

    .line 73
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/k;->b:Z

    if-eqz v0, :cond_2

    .line 74
    const-string v0, "\u2198"

    goto :goto_0

    .line 76
    :cond_2
    const-string v0, "\u2199"

    goto :goto_0

    .line 78
    :pswitch_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/k;->b:Z

    if-eqz v0, :cond_3

    .line 79
    const-string v0, "\u21b7"

    goto :goto_0

    .line 81
    :cond_3
    const-string v0, "\u21b6"

    goto :goto_0

    .line 83
    :pswitch_5
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/k;->b:Z

    if-eqz v0, :cond_4

    .line 84
    const-string v0, "\u21bf"

    goto :goto_0

    .line 86
    :cond_4
    const-string v0, "\u21be"

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
