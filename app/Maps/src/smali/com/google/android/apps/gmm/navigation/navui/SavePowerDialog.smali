.class public Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/navui/av;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Z

.field private d:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/ag;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/BroadcastReceiver;

.field private final f:Lcom/google/android/apps/gmm/base/l/ag;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/navui/av;Z)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/at;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/at;-><init>(Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->e:Landroid/content/BroadcastReceiver;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/au;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/au;-><init>(Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->f:Lcom/google/android/apps/gmm/base/l/ag;

    .line 100
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 101
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->a:Lcom/google/android/apps/gmm/navigation/navui/av;

    .line 102
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->c:Z

    .line 103
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->a:Lcom/google/android/apps/gmm/navigation/navui/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/av;->a()V

    .line 138
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/bg;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->d:Lcom/google/android/libraries/curvular/ae;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->f:Lcom/google/android/apps/gmm/base/l/ag;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 110
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->c:Z

    if-nez v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->e:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 115
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 117
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->c:Z

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 125
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 126
    return-void
.end method
