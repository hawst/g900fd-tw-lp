.class final Lcom/google/android/apps/gmm/map/e;
.super Lcom/google/android/apps/gmm/map/a;
.source "PG"


# instance fields
.field final synthetic b:Landroid/graphics/Rect;

.field final synthetic c:Lcom/google/android/apps/gmm/map/b/a/q;

.field final synthetic d:F


# direct methods
.method constructor <init>(Landroid/graphics/Rect;Lcom/google/android/apps/gmm/map/b/a/q;F)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/e;->b:Landroid/graphics/Rect;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/e;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iput p3, p0, Lcom/google/android/apps/gmm/map/e;->d:F

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b;)V
    .locals 8

    .prologue
    .line 288
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->c()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/e;->b:Landroid/graphics/Rect;

    .line 290
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/e;->b:Landroid/graphics/Rect;

    .line 291
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    .line 292
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->a()I

    move-result v3

    int-to-float v3, v3

    .line 293
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->b()I

    move-result v4

    int-to-float v4, v4

    .line 289
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v1

    .line 295
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/e;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, p0, Lcom/google/android/apps/gmm/map/e;->d:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iput-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 296
    iget v1, p0, Lcom/google/android/apps/gmm/map/e;->a:I

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/map/b;->a(Lcom/google/android/apps/gmm/map/f/a/a;I)V

    .line 297
    return-void
.end method
