.class public Lcom/google/android/apps/gmm/car/j/h;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static h:Lcom/google/android/apps/gmm/car/j/s;


# instance fields
.field final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/car/j/l;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/car/j/l;

.field d:Lcom/google/android/apps/gmm/car/j/l;

.field e:Lcom/google/android/apps/gmm/map/b/a/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:D

.field g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/car/j/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/j/h;->a:Ljava/lang/String;

    .line 236
    new-instance v0, Lcom/google/android/apps/gmm/car/j/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/j;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/j/h;->h:Lcom/google/android/apps/gmm/car/j/s;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/h;->c:Lcom/google/android/apps/gmm/car/j/l;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/h;->d:Lcom/google/android/apps/gmm/car/j/l;

    .line 57
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/car/j/h;->f:D

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/j/y;Ljava/lang/String;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 164
    new-instance v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/k;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 165
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v6, v7}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/p;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/car/j/p;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget v2, Lcom/google/android/apps/gmm/car/drawer/t;->b:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    .line 169
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v6, v7}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    if-nez p2, :cond_0

    .line 173
    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget v2, Lcom/google/android/apps/gmm/car/h/a;->a:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    .line 174
    invoke-static {v1}, Lcom/google/android/apps/gmm/car/j/q;->a(Lcom/google/android/apps/gmm/car/j/y;)Lcom/google/android/apps/gmm/car/j/s;

    move-result-object v1

    .line 173
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, p1, v8, v1}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    :goto_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/i/a/e;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    sget-object v2, Lcom/google/android/apps/gmm/car/j/h;->h:Lcom/google/android/apps/gmm/car/j/s;

    .line 182
    iget-object v3, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v4, v1, v8, v2}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/f/c;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 185
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v6, v7}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x14

    .line 186
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/f/c;->i:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 187
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v6, v7}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    return-void

    .line 177
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/u;

    invoke-direct {v2, p2}, Lcom/google/android/apps/gmm/car/j/u;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/l;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/d/k;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 111
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v7, v8}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/u;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/car/j/u;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/i/a/e;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/gmm/car/j/i;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/car/j/i;-><init>(Lcom/google/android/apps/gmm/car/j/h;)V

    .line 115
    iget-object v4, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v5, v1, v2, v3}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/f/c;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 124
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v7, v8}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    if-eqz p2, :cond_0

    .line 131
    const/16 v1, 0x3c

    .line 132
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/e/at;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 133
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v7, v8}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/e/u;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 136
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v7, v8}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget v2, Lcom/google/android/apps/gmm/car/h/a;->b:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    const/16 v2, 0xa

    new-instance v3, Lcom/google/android/apps/gmm/car/j/y;

    sget v4, Lcom/google/android/apps/gmm/car/h/a;->a:I

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/car/j/y;-><init>(I)V

    .line 141
    invoke-static {v3}, Lcom/google/android/apps/gmm/car/j/q;->a(Lcom/google/android/apps/gmm/car/j/y;)Lcom/google/android/apps/gmm/car/j/s;

    move-result-object v3

    .line 139
    iget-object v4, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v5, v1, v2, v3}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_0
    const/16 v1, 0x14

    .line 148
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/gmm/car/j/y;

    sget-object v2, Lcom/google/android/apps/gmm/car/f/c;->i:Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/j/y;-><init>(Lcom/google/android/libraries/curvular/bk;)V

    .line 149
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/car/j/q;

    invoke-direct {v3, v1, v7, v8}, Lcom/google/android/apps/gmm/car/j/q;-><init>(Lcom/google/android/apps/gmm/car/j/y;ILcom/google/android/apps/gmm/car/j/s;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/car/j/w;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/car/j/w;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    return-void
.end method
