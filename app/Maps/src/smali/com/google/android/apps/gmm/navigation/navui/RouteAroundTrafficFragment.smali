.class public Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field final a:J

.field b:Landroid/view/View;

.field c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

.field d:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/shared/c/f;

.field g:J

.field private final m:Lcom/google/android/apps/gmm/navigation/navui/d/o;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 300
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/as;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->m:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    .line 70
    mul-int/lit16 v0, p1, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a:J

    .line 71
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/j/b/b;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/gmm/navigation/j/b/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    if-nez p1, :cond_0

    .line 105
    :goto_0
    return-void

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/google/android/apps/gmm/navigation/j/b/b;->a:J

    cmp-long v0, v6, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    new-instance v4, Lcom/google/android/apps/gmm/z/b;

    sget-object v5, Lcom/google/b/f/b/a/ah;->b:Lcom/google/b/f/b/a/ah;

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/google/android/apps/gmm/z/b;-><init>(Lcom/google/b/f/b/a/ah;Lcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v4, v1, v2

    .line 85
    invoke-interface {v3, v1}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 84
    goto :goto_1

    .line 95
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v0, Lcom/google/r/b/a/ou;->V:I

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/ao;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/navigation/navui/ao;-><init>(Lcom/google/android/apps/gmm/base/activities/c;I)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/j/a/d;->a:Lcom/google/android/apps/gmm/navigation/j/a/d;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/s;Lcom/google/android/apps/gmm/navigation/g/b/k;)V
    .locals 14

    .prologue
    const-wide v12, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    .line 263
    const v0, 0x47435000    # 50000.0f

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/navigation/g/b/k;->a(F)Lcom/google/android/apps/gmm/map/b/a/ah;

    move-result-object v0

    .line 264
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ah;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 265
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v1

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    sub-double/2addr v2, v8

    mul-double/2addr v2, v10

    mul-double/2addr v2, v12

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    .line 266
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v1

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    sub-double/2addr v2, v8

    mul-double/2addr v2, v10

    mul-double/2addr v2, v12

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    .line 267
    return-void
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->aa:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 245
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/s;

    move-result-object v2

    .line 246
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/navui/d/m;->h:Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a(Lcom/google/android/apps/gmm/map/b/a/s;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 247
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a(Lcom/google/android/apps/gmm/map/b/a/s;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 248
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/s;->a()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v2

    .line 251
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v4, v0, v0, v0, v1}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    .line 252
    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/f/a/c;)V

    .line 255
    :cond_0
    return-void
.end method

.method public final a(IIF)V
    .locals 1

    .prologue
    .line 289
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a(IIF)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a()V

    .line 293
    :cond_0
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/ar;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/navui/ar;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 280
    return-void
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/b/f/b/a/ah;->d:Lcom/google/b/f/b/a/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    .line 185
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k()Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 117
    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->m:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/d/m;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/navui/d/o;Z)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/navigation/navui/a/u;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->d:Lcom/google/android/libraries/curvular/ae;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b:Landroid/view/View;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->dO:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/ap;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/navui/ap;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->b:Lcom/google/android/apps/gmm/base/views/ba;

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->g:J

    .line 137
    if-eqz p1, :cond_1

    const-string v0, "expirationRelativeTimeMs"

    .line 138
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    const-string v0, "expirationRelativeTimeMs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->g:J

    .line 141
    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 173
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    .line 174
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b:Landroid/view/View;

    .line 175
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->d:Lcom/google/android/libraries/curvular/ae;

    .line 176
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    .line 178
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroy()V

    .line 179
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->p:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 169
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->m:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->p:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 151
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 110
    const-string v0, "expirationRelativeTimeMs"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->g:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 111
    return-void
.end method
