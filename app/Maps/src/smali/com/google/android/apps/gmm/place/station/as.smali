.class Lcom/google/android/apps/gmm/place/station/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/station/au;

.field final synthetic b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;Lcom/google/android/apps/gmm/place/station/au;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/station/as;->a:Lcom/google/android/apps/gmm/place/station/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 174
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/station/as;->a:Lcom/google/android/apps/gmm/place/station/au;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    iget-object v5, v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->e:Lcom/google/b/a/aa;

    iget-object v0, v4, Lcom/google/android/apps/gmm/place/station/au;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v6

    move v1, v2

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/ao;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/station/ao;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/ao;->b:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_1

    invoke-interface {v5, v0}, Lcom/google/b/a/aa;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v1, v0, :cond_1

    move v1, v0

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/place/station/au;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/ao;

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iput v1, v0, Lcom/google/android/apps/gmm/place/station/ao;->c:I

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/apps/gmm/place/station/ao;->d:Lcom/google/android/libraries/curvular/au;

    goto :goto_2

    .line 175
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/as;->a:Lcom/google/android/apps/gmm/place/station/au;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    .line 180
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/as;->b:Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->d:Landroid/view/View;

    .line 179
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/station/a/m;->a(Lcom/google/android/apps/gmm/place/station/b/g;Landroid/view/View;)V

    .line 182
    :cond_6
    return-void
.end method
