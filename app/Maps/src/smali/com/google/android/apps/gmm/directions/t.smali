.class public final enum Lcom/google/android/apps/gmm/directions/t;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/t;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/t;

.field private static final synthetic c:[Lcom/google/android/apps/gmm/directions/t;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/directions/t;

    const-string v1, "NAVIGATION_LAUNCHED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/t;->a:Lcom/google/android/apps/gmm/directions/t;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/directions/t;

    const-string v1, "ROUTE_PREVIEW_LAUNCHED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/directions/t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/t;->b:Lcom/google/android/apps/gmm/directions/t;

    .line 115
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/t;

    sget-object v1, Lcom/google/android/apps/gmm/directions/t;->a:Lcom/google/android/apps/gmm/directions/t;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/t;->b:Lcom/google/android/apps/gmm/directions/t;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/directions/t;->c:[Lcom/google/android/apps/gmm/directions/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/t;
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/google/android/apps/gmm/directions/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/t;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/t;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/directions/t;->c:[Lcom/google/android/apps/gmm/directions/t;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/t;

    return-object v0
.end method
