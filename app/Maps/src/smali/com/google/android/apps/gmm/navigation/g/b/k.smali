.class public Lcom/google/android/apps/gmm/navigation/g/b/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/r/a/w;

.field public final b:Lcom/google/android/apps/gmm/map/r/a/ag;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field final h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/l;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 65
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 66
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 67
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    .line 68
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 69
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->e:I

    .line 70
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    .line 71
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    .line 72
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/l;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->h:Z

    .line 73
    return-void
.end method


# virtual methods
.method public final a(F)Lcom/google/android/apps/gmm/map/b/a/ah;
    .locals 12

    .prologue
    .line 140
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 144
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    add-int/lit8 v0, v0, 0x1

    .line 146
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v4, v1, v0

    .line 147
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_3

    .line 148
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v1

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v6, v10

    mul-double/2addr v6, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v6

    .line 149
    float-to-double v8, p1

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    invoke-static {v1, v4, v5}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v1

    if-gez v1, :cond_1

    add-int/lit8 v1, v1, 0x2

    neg-int v1, v1

    :cond_1
    add-int/lit8 v3, v1, 0x1

    .line 151
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ah;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;II)V

    move-object v0, v1

    goto :goto_0

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 145
    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 153
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ah;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;I)V

    move-object v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204
    if-ne p0, p1, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v0

    .line 207
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 208
    goto :goto_0

    .line 210
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 211
    goto :goto_0

    .line 214
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_d

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_4
    move v2, v0

    :goto_1
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 216
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_e

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_5
    move v2, v0

    :goto_2
    if-eqz v2, :cond_c

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    .line 217
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_f

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_6
    move v2, v0

    :goto_3
    if-eqz v2, :cond_c

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 218
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_10

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_7
    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->e:I

    .line 219
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eq v2, v3, :cond_8

    if-eqz v2, :cond_11

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_8
    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    .line 220
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eq v2, v3, :cond_9

    if-eqz v2, :cond_12

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_9
    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    .line 221
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eq v2, v3, :cond_a

    if-eqz v2, :cond_13

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    :cond_a
    move v2, v0

    :goto_7
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->h:Z

    .line 222
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    if-eq v2, v3, :cond_b

    if-eqz v2, :cond_14

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_b
    move v2, v0

    :goto_8
    if-nez v2, :cond_0

    :cond_c
    move v0, v1

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 215
    goto/16 :goto_1

    :cond_e
    move v2, v1

    .line 216
    goto/16 :goto_2

    :cond_f
    move v2, v1

    .line 217
    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 218
    goto :goto_4

    :cond_11
    move v2, v1

    .line 219
    goto :goto_5

    :cond_12
    move v2, v1

    .line 220
    goto :goto_6

    :cond_13
    move v2, v1

    .line 221
    goto :goto_7

    :cond_14
    move v2, v1

    .line 222
    goto :goto_8
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 191
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    .line 194
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 195
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->e:I

    .line 196
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    .line 197
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    .line 198
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->h:Z

    .line 199
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 191
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 177
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/b/a/ak;

    invoke-direct {v2, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "route"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 178
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "curStep"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v1, :cond_1

    const/4 v1, -0x1

    .line 179
    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 179
    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    goto :goto_0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "curSegment"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    .line 180
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "metersToNextStep"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 181
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "secondsToNextStep"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->e:I

    .line 182
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "metersRemaining"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    .line 183
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "secondsRemaining"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    .line 184
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "isOnRoute"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/k;->h:Z

    .line 185
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 186
    invoke-virtual {v2}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
