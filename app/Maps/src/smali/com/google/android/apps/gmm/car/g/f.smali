.class public Lcom/google/android/apps/gmm/car/g/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/g/e;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/car/g/g;

.field private final b:Lcom/google/android/apps/gmm/directions/f/c;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/g/g;Lcom/google/android/apps/gmm/directions/f/c;Z)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/g/g;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/g/f;->a:Lcom/google/android/apps/gmm/car/g/g;

    .line 27
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/directions/f/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/g/f;->b:Lcom/google/android/apps/gmm/directions/f/c;

    .line 28
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/car/g/f;->c:Z

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/g/f;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/f/b/m;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/f;->b:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/f;->a:Lcom/google/android/apps/gmm/car/g/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/g/g;->a()V

    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/directions/f/b/m;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g/f;->b:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/f;->b:Lcom/google/android/apps/gmm/directions/f/c;

    .line 44
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 43
    :goto_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/f;->a:Lcom/google/android/apps/gmm/car/g/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/g/g;->b()V

    .line 46
    const/4 v0, 0x0

    return-object v0

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
