.class public abstract Lcom/google/android/apps/gmm/map/internal/b/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/b/e;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/ce;

.field c:J

.field final d:Lcom/google/android/apps/gmm/map/internal/b/i;

.field public e:J

.field public f:Lcom/google/android/apps/gmm/map/internal/c/cw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/ce;Lcom/google/android/apps/gmm/map/internal/c/cw;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->c:J

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/b/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/b/i;-><init>(Lcom/google/android/apps/gmm/map/internal/b/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->d:Lcom/google/android/apps/gmm/map/internal/b/i;

    .line 49
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->e:J

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    .line 63
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->f:Lcom/google/android/apps/gmm/map/internal/c/cw;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->f:Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    .line 155
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->b:I

    int-to-float v0, v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 5

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->f:Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    .line 78
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-ltz v1, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    array-length v2, v2

    if-lt v1, v2, :cond_2

    :cond_0
    const/4 v0, -0x1

    move v1, v0

    .line 79
    :goto_0
    if-gez v1, :cond_3

    .line 80
    const/4 p1, 0x0

    .line 82
    :cond_1
    :goto_1
    return-object p1

    .line 78
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    aget v0, v0, v1

    move v1, v0

    goto :goto_0

    .line 82
    :cond_3
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_1

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    shr-int/2addr v2, v0

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    shr-int/2addr v3, v0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    move-object p1, v0

    goto :goto_1
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 114
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->f:Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, p2, v2}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v5

    move v0, v1

    .line 116
    :goto_0
    if-gt v0, p1, :cond_2

    .line 117
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/internal/c/cv;->e:Ljava/util/TreeSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    const/4 v2, 0x1

    shl-int v6, v2, v0

    move v3, v1

    .line 121
    :goto_1
    if-ge v3, v6, :cond_1

    move v2, v1

    .line 122
    :goto_2
    if-ge v2, v6, :cond_0

    .line 123
    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v8

    invoke-direct {v7, v0, v3, v2, v8}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 121
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 116
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_2
    return-object v4
.end method

.method protected final a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->d:Lcom/google/android/apps/gmm/map/internal/b/i;

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/b/i;->a:I

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/b/i;->b:I

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->d:Lcom/google/android/apps/gmm/map/internal/b/i;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 147
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->f:Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/b/h;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, p2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v1

    .line 95
    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-ltz v3, :cond_0

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    array-length v4, v4

    if-lt v3, v4, :cond_2

    :cond_0
    const/4 v1, -0x1

    move v4, v1

    .line 96
    :goto_0
    if-gez v4, :cond_3

    .line 109
    :cond_1
    return-object v0

    .line 95
    :cond_2
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    aget v1, v1, v3

    move v4, v1

    goto :goto_0

    .line 99
    :cond_3
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int v5, v4, v1

    .line 100
    const/4 v1, 0x1

    shl-int v6, v1, v5

    move v3, v2

    .line 101
    :goto_1
    if-ge v3, v6, :cond_1

    move v1, v2

    .line 102
    :goto_2
    if-ge v1, v6, :cond_4

    .line 104
    iget v7, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    shl-int/2addr v7, v5

    add-int/2addr v7, v1

    .line 105
    iget v8, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    shl-int/2addr v8, v5

    add-int/2addr v8, v3

    .line 103
    new-instance v9, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v10, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v9, v4, v7, v8, v10}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 106
    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 101
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1
.end method
