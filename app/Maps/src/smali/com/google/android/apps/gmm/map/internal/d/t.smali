.class Lcom/google/android/apps/gmm/map/internal/d/t;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/v;",
            "Lcom/google/android/apps/gmm/map/internal/d/u;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/v;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/map/internal/d/ac;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;I)V
    .locals 6

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v1, Ljava/util/ArrayList;

    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x5

    int-to-long v4, p2

    add-long/2addr v2, v4

    div-int/lit8 v0, p2, 0xa

    int-to-long v4, v0

    add-long/2addr v2, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    .line 200
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v1, "ImportantLabelCache"

    .line 202
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 203
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 204
    return-void

    .line 199
    :cond_2
    const-wide/32 v4, -0x80000000

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_3
    long-to-int v0, v2

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 12
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 359
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v5

    .line 360
    const/4 v3, 0x0

    .line 361
    const/4 v2, 0x0

    .line 362
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    monitor-enter v6

    .line 363
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/v;

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/d/u;

    .line 366
    if-eqz v1, :cond_3

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->k:J

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    iget-wide v10, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->k:J

    cmp-long v0, v8, v10

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_a

    .line 368
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, p1, :cond_a

    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->h:I

    .line 369
    iget v7, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-gt v0, v7, :cond_a

    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->j:I

    .line 370
    iget v7, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-lt v0, v7, :cond_a

    .line 371
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->c:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v7, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    if-eq v0, v7, :cond_0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_a

    .line 372
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 373
    iget-object v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->f:Lcom/google/android/apps/gmm/map/internal/c/ck;

    .line 374
    if-eqz v7, :cond_9

    .line 375
    if-nez v3, :cond_8

    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 378
    :goto_3
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 380
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->a:Lcom/google/android/apps/gmm/map/internal/d/v;

    invoke-virtual {v3, v7, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_4
    move-object v1, v0

    move-object v0, v2

    .line 363
    :goto_5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move-object v2, v0

    goto :goto_0

    .line 367
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 371
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 384
    :cond_3
    if-nez v2, :cond_7

    .line 385
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 387
    :goto_6
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v3

    goto :goto_5

    .line 390
    :cond_4
    if-eqz v2, :cond_5

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 393
    :cond_5
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    if-eqz v3, :cond_6

    .line 395
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/co;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/co;-><init>()V

    .line 396
    iput-object p2, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 397
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/cj;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/c/cj;

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->j:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    const-wide/16 v2, -0x1

    .line 398
    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->g:J

    const-wide/16 v2, -0x1

    .line 399
    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/internal/c/co;->k:J

    .line 400
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/co;->a()Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v0

    .line 403
    :goto_7
    return-object v0

    .line 393
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 403
    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    :cond_7
    move-object v1, v2

    goto :goto_6

    :cond_8
    move-object v0, v3

    goto :goto_3

    :cond_9
    move-object v0, v3

    goto :goto_4

    :cond_a
    move-object v0, v2

    move-object v1, v3

    goto :goto_5
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/v;)Lcom/google/android/apps/gmm/map/internal/d/u;
    .locals 3

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/u;

    .line 408
    if-nez v0, :cond_1

    .line 409
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/u;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/u;-><init>(Lcom/google/android/apps/gmm/map/internal/d/v;)V

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 411
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    monitor-enter v1

    .line 412
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 413
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_0
    monitor-exit v1

    .line 417
    :cond_1
    return-object v0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/internal/d/au;Lcom/google/android/apps/gmm/map/internal/c/bp;JLcom/google/android/apps/gmm/map/internal/d/s;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 15
    .param p5    # Lcom/google/android/apps/gmm/map/internal/d/s;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 225
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p2

    invoke-direct {p0, v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/t;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v5

    .line 226
    if-nez v5, :cond_5

    .line 228
    if-nez p5, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-object v4

    .line 228
    :cond_1
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    move-wide/from16 v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/s;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v8, :cond_3

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v8, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v8

    if-eqz v8, :cond_3

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v8, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v9

    iget-wide v10, v8, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v10, v10, v12

    if-ltz v10, :cond_2

    invoke-interface {v9}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v10

    iget-wide v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v8, v10, v8

    if-lez v8, :cond_2

    :goto_1
    if-nez v6, :cond_0

    move-object v4, v5

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v8, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/internal/c/bo;->d()Lcom/google/android/apps/gmm/map/internal/c/bt;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v9

    iget-wide v10, v8, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v10, v10, v12

    if-ltz v10, :cond_4

    invoke-interface {v9}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v10

    iget-wide v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bt;->b:J

    cmp-long v8, v10, v8

    if-lez v8, :cond_4

    :goto_2
    if-nez v6, :cond_0

    move-object v4, v5

    goto :goto_0

    :cond_4
    move v6, v7

    goto :goto_2

    :cond_5
    move-object v4, v5

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 237
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/c/bo;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 291
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-nez v0, :cond_0

    .line 342
    :goto_0
    return-void

    .line 294
    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/cm;

    .line 295
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    array-length v0, v0

    move v2, v0

    :goto_1
    move v6, v3

    move-object v4, v5

    .line 297
    :goto_2
    if-ge v6, v2, :cond_8

    .line 298
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    aget-object v0, v0, v6

    .line 299
    :goto_3
    instance-of v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ck;

    if-eqz v1, :cond_4

    .line 300
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ck;

    .line 301
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ck;->a:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 304
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_f

    .line 305
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 306
    new-instance v7, Lcom/google/android/apps/gmm/map/internal/d/v;

    invoke-direct {v7, v1}, Lcom/google/android/apps/gmm/map/internal/d/v;-><init>(Lcom/google/android/apps/gmm/map/internal/c/an;)V

    invoke-direct {p0, v7}, Lcom/google/android/apps/gmm/map/internal/d/t;->a(Lcom/google/android/apps/gmm/map/internal/d/v;)Lcom/google/android/apps/gmm/map/internal/d/u;

    move-result-object v7

    .line 307
    iget-object v8, v1, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 308
    if-nez v4, :cond_e

    .line 309
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 311
    :goto_4
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/d/u;->a:Lcom/google/android/apps/gmm/map/internal/d/v;

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v4, v1

    move-object v1, v7

    move-object v7, v8

    .line 313
    :goto_5
    if-eqz v1, :cond_4

    .line 314
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    iget-object v9, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v10, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iput-object v9, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v11, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iput-object v11, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->c:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v11, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v11, :cond_7

    iput-object v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iput v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->e:I

    :cond_1
    :goto_6
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->f:Lcom/google/android/apps/gmm/map/internal/c/ck;

    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->g:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->g:I

    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->i:I

    iget v7, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->i:I

    iget v0, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->h:I

    if-ge v0, v7, :cond_2

    const/4 v0, 0x1

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->h:I

    :cond_2
    iget v0, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->j:I

    if-le v0, v7, :cond_3

    const/16 v0, 0x16

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->j:I

    :cond_3
    invoke-interface {v8}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-virtual {v9, v0, v8}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v8

    iput-wide v8, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->k:J

    .line 297
    :cond_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_2

    :cond_5
    move v2, v3

    .line 295
    goto/16 :goto_1

    :cond_6
    move-object v0, v5

    .line 298
    goto :goto_3

    .line 314
    :cond_7
    iget v11, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v12, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->e:I

    if-le v11, v12, :cond_1

    iput-object v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v10, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iput v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->e:I

    goto :goto_6

    .line 322
    :cond_8
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    .line 323
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 324
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    monitor-enter v6

    .line 325
    :goto_7
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_d

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/v;

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/d/u;

    .line 328
    if-eqz v1, :cond_a

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/u;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 329
    if-eqz v4, :cond_9

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->a:Lcom/google/android/apps/gmm/map/internal/d/v;

    invoke-interface {v4, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 330
    :cond_9
    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->g:I

    if-gt v7, v5, :cond_b

    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->i:I

    if-lt v7, v5, :cond_b

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/t;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 325
    :cond_a
    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 337
    :cond_b
    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->i:I

    if-lt v5, v0, :cond_c

    add-int/lit8 v0, v5, -0x1

    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->j:I

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->j:I

    :cond_c
    iget v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->g:I

    if-gt v5, v0, :cond_a

    add-int/lit8 v0, v5, 0x1

    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->h:I

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/d/u;->h:I

    goto :goto_8

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_d
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_e
    move-object v1, v4

    goto/16 :goto_4

    :cond_f
    move-object v1, v5

    move-object v7, v5

    goto/16 :goto_5
.end method
