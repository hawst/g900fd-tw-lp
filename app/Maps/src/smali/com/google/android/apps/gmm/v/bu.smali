.class public Lcom/google/android/apps/gmm/v/bu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/v/bv;

.field public final b:Lcom/google/android/apps/gmm/v/bv;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/v/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Z)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/bu;->c:Lcom/google/android/apps/gmm/v/ad;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/v/bv;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/v/bv;-><init>(Lcom/google/android/apps/gmm/v/bu;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->a:Lcom/google/android/apps/gmm/v/bv;

    .line 41
    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/v/bv;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/v/bv;-><init>(Lcom/google/android/apps/gmm/v/bu;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->b:Lcom/google/android/apps/gmm/v/bv;

    .line 42
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->c:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ad;->b()V

    .line 67
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->b:Lcom/google/android/apps/gmm/v/bv;

    if-eqz v0, :cond_0

    .line 52
    div-int/lit8 v0, p2, 0x2

    .line 54
    add-int/lit8 v1, v0, -0x2

    .line 55
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bu;->a:Lcom/google/android/apps/gmm/v/bv;

    invoke-virtual {v2, v1, p3}, Lcom/google/android/apps/gmm/v/bv;->a(II)V

    .line 56
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bu;->b:Lcom/google/android/apps/gmm/v/bv;

    invoke-virtual {v2, v1, p3}, Lcom/google/android/apps/gmm/v/bv;->a(II)V

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bu;->b:Lcom/google/android/apps/gmm/v/bv;

    add-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/android/apps/gmm/v/bv;->a:I

    .line 61
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->c:Lcom/google/android/apps/gmm/v/ad;

    .line 62
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->a:Lcom/google/android/apps/gmm/v/bv;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/gmm/v/bv;->a(II)V

    goto :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bu;->c:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ad;->c()V

    .line 47
    return-void
.end method
