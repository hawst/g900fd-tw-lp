.class Lcom/google/android/apps/gmm/navigation/navui/ak;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->q:Z

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->g:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->f:Lcom/google/android/apps/gmm/navigation/navui/an;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 198
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/a;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b()V

    .line 179
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 8
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 137
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-nez v0, :cond_3

    .line 138
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    if-eqz v0, :cond_5

    .line 139
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b()V

    .line 170
    :cond_4
    :goto_1
    return-void

    .line 144
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-nez v0, :cond_7

    .line 145
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v4, v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 150
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 152
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    new-array v6, v6, [Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v0, v6, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    .line 153
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v0, v6, v1

    invoke-static {v2, v6}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    sget-object v0, Lcom/google/android/apps/gmm/map/i/r;->a:Lcom/google/android/apps/gmm/map/i/r;

    .line 154
    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v0

    .line 155
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    .line 156
    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 157
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->k:Z

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/v;->e:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 158
    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 159
    new-instance v3, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    .line 152
    iput-object v3, v4, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->d:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 162
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->p:Z

    if-eqz v0, :cond_4

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->p:Z

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    .line 166
    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->d:Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-nez v3, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->g:Lcom/google/android/libraries/curvular/ae;

    iget-object v3, v3, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->f:Lcom/google/android/apps/gmm/navigation/navui/an;

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/activities/ag;->a(Lcom/google/maps/g/a/hm;)Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v3

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    new-instance v4, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v5, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v5, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v5, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v5, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v5, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v5, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/f;->f:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->e:Landroid/view/View;

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/al;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/al;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;)V

    iget-object v2, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    iget-wide v2, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->o:J

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->n:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_9

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->m:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->a:J

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->setDuration(J)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->m:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->a:J

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a(J)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b()V

    goto/16 :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/e;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ak;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b()V

    .line 189
    :cond_0
    return-void
.end method
