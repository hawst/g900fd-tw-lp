.class Lcom/google/android/apps/gmm/prefetch/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/prefetch/b;

.field private final b:Lcom/google/android/apps/gmm/prefetch/a/c;

.field private c:I

.field private d:Z

.field private e:Z

.field private final f:Lcom/google/android/apps/gmm/prefetch/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/prefetch/b;ILcom/google/android/apps/gmm/prefetch/h;)V
    .locals 1

    .prologue
    .line 928
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/i;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 929
    iput p2, p0, Lcom/google/android/apps/gmm/prefetch/i;->c:I

    .line 930
    iput-object p3, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    .line 931
    iget-object v0, p3, Lcom/google/android/apps/gmm/prefetch/h;->e:Lcom/google/android/apps/gmm/prefetch/a/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    .line 932
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x1

    .line 937
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/prefetch/m;->e:Z

    if-eqz v0, :cond_2

    .line 938
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->e:Z

    if-nez v0, :cond_1

    .line 939
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->e:Z

    .line 940
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->a:Lcom/google/android/apps/gmm/prefetch/b;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/k;->c:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    .line 941
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->a:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 945
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 950
    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 952
    :try_start_1
    const-string v0, "PrefetcherService"

    const-string v1, "status should not be STATUS_INVALID in handleTile."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 937
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 955
    :cond_3
    if-eq p2, v1, :cond_1

    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->d:Z

    if-nez v0, :cond_1

    .line 966
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->c:I

    .line 970
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    iget v1, v0, Lcom/google/android/apps/gmm/prefetch/h;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/android/apps/gmm/prefetch/h;->b:I

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    if-eqz v0, :cond_8

    .line 973
    const/4 v0, 0x0

    .line 974
    if-ne p2, v2, :cond_5

    .line 975
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->f:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 981
    :cond_4
    :goto_1
    if-eqz v0, :cond_7

    .line 982
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/prefetch/i;->d:Z

    .line 983
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/i;->a:Lcom/google/android/apps/gmm/prefetch/b;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/k;->c:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v2, v1, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    .line 984
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 986
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->a:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    goto :goto_0

    .line 976
    :cond_5
    const/4 v1, 0x4

    if-ne p2, v1, :cond_6

    .line 977
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->g:Lcom/google/android/apps/gmm/prefetch/a/e;

    goto :goto_1

    .line 978
    :cond_6
    const/4 v1, 0x5

    if-ne p2, v1, :cond_4

    .line 979
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->h:Lcom/google/android/apps/gmm/prefetch/a/e;

    goto :goto_1

    .line 990
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    .line 991
    iget v2, v2, Lcom/google/android/apps/gmm/prefetch/h;->c:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    .line 992
    iget-object v3, v3, Lcom/google/android/apps/gmm/prefetch/h;->a:Lcom/google/android/apps/gmm/prefetch/q;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/prefetch/q;->b()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/prefetch/i;->c:I

    add-int/2addr v3, v4

    .line 990
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;II)V

    .line 996
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->c:I

    if-nez v0, :cond_1

    .line 997
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/h;->a:Lcom/google/android/apps/gmm/prefetch/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetch/q;->b()I

    move-result v0

    if-nez v0, :cond_9

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->b:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/e;->b:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 1002
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    .line 1003
    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/m;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-wide/16 v0, 0x32

    .line 1006
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/i;->a:Lcom/google/android/apps/gmm/prefetch/b;

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetch/i;->f:Lcom/google/android/apps/gmm/prefetch/h;

    iget-object v5, v2, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/apps/gmm/prefetch/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1003
    :cond_a
    const-wide/16 v0, 0x3e8

    goto :goto_2
.end method
