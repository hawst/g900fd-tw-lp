.class public Lcom/google/android/apps/gmm/place/review/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public final b:Landroid/os/Bundle;

.field public c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 122
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    .line 123
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/place/review/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/place/review/j;"
        }
    .end annotation

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v2, "placemarkref"

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 131
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/j;->c:Lcom/google/android/apps/gmm/x/o;

    .line 132
    return-object p0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->p()Lcom/google/android/apps/gmm/j/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/j/a/a;->e(Landroid/os/Bundle;)V

    .line 191
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/place/review/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/review/k;-><init>(Lcom/google/android/apps/gmm/place/review/j;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V

    goto :goto_0
.end method
