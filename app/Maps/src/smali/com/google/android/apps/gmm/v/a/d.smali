.class public abstract Lcom/google/android/apps/gmm/v/a/d;
.super Lcom/google/android/apps/gmm/v/c;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/cl;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/v/cj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/c;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/a/d;->b:Lcom/google/android/apps/gmm/v/cj;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/v/c;->getTransformation(JLandroid/view/animation/Transformation;)Z

    iget v0, p0, Lcom/google/android/apps/gmm/v/c;->e:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/v/a/d;->a(F)V

    .line 67
    return-void
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a/d;->b:Lcom/google/android/apps/gmm/v/cj;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/gmm/v/a/d;->a(Lcom/google/android/apps/gmm/v/cj;F)V

    .line 75
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/cl;

    .line 77
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a/d;->b:Lcom/google/android/apps/gmm/v/cj;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/v/cl;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 79
    :cond_0
    return-void
.end method

.method protected abstract a(Lcom/google/android/apps/gmm/v/cj;F)V
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/v/a/d;->a(F)V

    .line 50
    return-void
.end method
