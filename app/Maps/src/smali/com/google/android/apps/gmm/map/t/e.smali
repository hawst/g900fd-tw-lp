.class public Lcom/google/android/apps/gmm/map/t/e;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/cl;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public b:F

.field public c:F

.field public d:F

.field private final e:Lcom/google/android/apps/gmm/v/cj;

.field private final f:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/map/t/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/t/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(F)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 53
    const-class v0, Lcom/google/android/apps/gmm/map/t/f;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 27
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/e;->b:F

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/e;->e:Lcom/google/android/apps/gmm/v/cj;

    .line 37
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/e;->c:F

    .line 42
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/e;->d:F

    .line 54
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/e;->b:F

    .line 55
    iput p1, p0, Lcom/google/android/apps/gmm/map/t/e;->f:F

    .line 56
    return-void
.end method

.method public static a(F)F
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const v0, 0x3dcccccd    # 0.1f

    .line 91
    const/high16 v2, 0x41980000    # 19.0f

    sub-float/2addr v2, p0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 92
    cmpg-float v3, v2, v0

    if-gez v3, :cond_0

    .line 97
    :goto_0
    return v0

    .line 94
    :cond_0
    cmpl-float v0, v2, v1

    if-lez v0, :cond_1

    move v0, v1

    .line 95
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 3

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/e;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/f;

    .line 118
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/e;->b:F

    iget v2, v0, Lcom/google/android/apps/gmm/map/t/f;->c:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 119
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/e;->b:F

    iput v1, v0, Lcom/google/android/apps/gmm/map/t/f;->c:F

    .line 121
    :cond_0
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/f;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/e;->b:F

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 122
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/f;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/e;->d:F

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 127
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/f;->f:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/e;->f:F

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 132
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/f;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/e;->c:F

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 138
    iget v0, v0, Lcom/google/android/apps/gmm/map/t/f;->e:I

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/e;->e:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v2, 0xa

    aget v1, v1, v2

    .line 142
    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 144
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/cj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/e;->e:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v2, 0x10

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    return-void
.end method
