.class public Lcom/google/android/apps/gmm/streetview/internal/q;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/streetview/internal/x;


# static fields
.field private static final i:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field final a:Lcom/google/android/apps/gmm/v/ad;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/streetview/internal/t;

.field d:Lcom/google/android/apps/gmm/v/g;

.field e:Lcom/google/android/apps/gmm/streetview/internal/w;

.field f:Lcom/google/android/apps/gmm/streetview/internal/w;

.field g:Lcom/google/android/apps/gmm/streetview/internal/a;

.field private final h:I

.field private final j:I

.field private final k:Lcom/google/android/apps/gmm/streetview/internal/s;

.field private final l:Z

.field private final m:[F

.field private final n:[Ljava/lang/String;

.field private o:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v1, v2}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/q;->i:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;ILcom/google/android/apps/gmm/streetview/internal/ap;Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->g:Lcom/google/android/apps/gmm/streetview/internal/a;

    .line 85
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->m:[F

    .line 93
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "N"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "E"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "S"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SW"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "W"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NW"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->n:[Ljava/lang/String;

    .line 121
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    .line 122
    iput p2, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->j:I

    .line 123
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/s;

    invoke-direct {v0, p3, p0}, Lcom/google/android/apps/gmm/streetview/internal/s;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ap;Lcom/google/android/apps/gmm/streetview/internal/q;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->k:Lcom/google/android/apps/gmm/streetview/internal/s;

    .line 126
    sget v0, Lcom/google/android/apps/gmm/d;->aN:I

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->h:I

    .line 127
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/ap;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->l:Z

    .line 128
    return-void

    .line 85
    nop

    :array_0
    .array-data 4
        -0x425c28f6    # -0.08f
        -0x41b33333    # -0.2f
        0x0
        0x0
        0x0
        0x3da3d70a    # 0.08f
        -0x41b33333    # -0.2f
        0x0
        0x0
        0x3f800000    # 1.0f
        -0x425c28f6    # -0.08f
        -0x41b33333    # -0.2f
        -0x42333333    # -0.1f
        0x3f800000    # 1.0f
        0x0
        0x3da3d70a    # 0.08f
        -0x41b33333    # -0.2f
        -0x42333333    # -0.1f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/streetview/internal/w;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-ne p1, v0, :cond_2

    .line 182
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 183
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0

    .line 185
    :cond_0
    iput-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->e:Lcom/google/android/apps/gmm/streetview/internal/w;

    .line 189
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v1, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v1, p1, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 190
    return-void

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    if-ne p1, v0, :cond_1

    .line 187
    iput-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->f:Lcom/google/android/apps/gmm/streetview/internal/w;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/q;->d:Lcom/google/android/apps/gmm/v/g;

    .line 141
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 25

    .prologue
    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->g:Lcom/google/android/apps/gmm/streetview/internal/a;

    if-eqz v2, :cond_0

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->g:Lcom/google/android/apps/gmm/streetview/internal/a;

    sget-object v3, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    const/16 v4, 0x10

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/a;->a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/v/r;

    .line 196
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->h:I

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/v/r;->a(I)V

    .line 197
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->g:Lcom/google/android/apps/gmm/streetview/internal/a;

    .line 199
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v2, :cond_7

    .line 202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v3, v3, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF)Lcom/google/android/apps/gmm/v/cj;

    move-result-object v12

    .line 205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/high16 v3, 0x42be0000    # 95.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 211
    new-instance v13, Lcom/google/android/apps/gmm/v/cd;

    const-class v2, Lcom/google/android/apps/gmm/streetview/internal/r;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-direct {v13, v2, v3, v4}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;II)V

    .line 213
    new-instance v14, Lcom/google/android/apps/gmm/v/m;

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-direct {v14, v2, v3}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    .line 216
    new-instance v15, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v15}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    .line 218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v0, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v2, 0x0

    move v11, v2

    :goto_0
    move/from16 v0, v17

    if-ge v11, v0, :cond_4

    aget-object v18, v16, v11

    .line 219
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 220
    new-instance v19, Lcom/google/android/apps/gmm/v/aa;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/apps/gmm/v/aa;-><init>()V

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->n:[Ljava/lang/String;

    move-object/from16 v0, v18

    iget v4, v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->b:I

    aget-object v3, v3, v4

    .line 226
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    .line 227
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 229
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0x30

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v2

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    .line 231
    rem-int/lit8 v4, v2, 0x4

    if-eqz v4, :cond_1

    .line 233
    div-int/lit8 v2, v2, 0x4

    add-int/lit8 v2, v2, 0x1

    shl-int/lit8 v2, v2, 0x2

    .line 235
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v20

    .line 236
    move-object/from16 v0, v20

    iget v4, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move-object/from16 v0, v20

    iget v6, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v7, v4, v6

    .line 239
    const/high16 v6, 0x3f800000    # 1.0f

    .line 240
    const/high16 v4, 0x3f800000    # 1.0f

    .line 242
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->l:Z

    if-nez v8, :cond_a

    .line 244
    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/google/android/apps/gmm/v/ar;->a(II)I

    move-result v8

    .line 245
    const/4 v4, 0x1

    invoke-static {v7, v4}, Lcom/google/android/apps/gmm/v/ar;->a(II)I

    move-result v6

    .line 246
    int-to-float v2, v2

    int-to-float v4, v8

    div-float v4, v2, v4

    .line 247
    int-to-float v2, v7

    int-to-float v7, v6

    div-float/2addr v2, v7

    move v7, v8

    .line 252
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v8, v8, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget v8, v8, Lcom/google/android/apps/gmm/v/ao;->b:I

    .line 253
    if-le v7, v8, :cond_9

    move v10, v8

    .line 256
    :goto_2
    if-le v6, v8, :cond_8

    move v9, v8

    .line 260
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->m:[F

    const/16 v7, 0x9

    aput v2, v6, v7

    .line 261
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->m:[F

    const/16 v7, 0xd

    aput v4, v6, v7

    .line 262
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->m:[F

    const/16 v7, 0x12

    aput v4, v6, v7

    .line 263
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->m:[F

    const/16 v6, 0x13

    aput v2, v4, v6

    .line 264
    new-instance v2, Lcom/google/android/apps/gmm/v/av;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->m:[F

    const/16 v6, 0x11

    const/4 v7, 0x5

    invoke-direct {v2, v4, v6, v7}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->j:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/co;I)V

    .line 272
    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v9, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 273
    new-instance v22, Landroid/graphics/Canvas;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 275
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 277
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v20

    iget v7, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const v4, -0xcfcfd0

    const/16 v6, 0xff

    const/16 v7, 0xff

    const/16 v23, 0xff

    move/from16 v0, v23

    invoke-virtual {v2, v4, v6, v7, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 287
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 288
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    neg-int v2, v2

    int-to-float v7, v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->o:Landroid/graphics/Paint;

    move-object/from16 v2, v22

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 290
    int-to-float v2, v10

    int-to-float v3, v9

    div-float v8, v2, v3

    .line 291
    iget-object v2, v12, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v4, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    const/16 v6, 0x10

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v7, Lcom/google/android/apps/gmm/streetview/internal/q;->i:Lcom/google/android/apps/gmm/v/cn;

    move-object/from16 v0, v18

    iget v2, v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    neg-float v4, v2

    .line 292
    iget-object v2, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v5, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v9, 0x1

    aget v6, v6, v9

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v9, 0x2

    aget v7, v7, v9

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    const v2, 0x3ccccccd    # 0.025f

    const/4 v3, 0x0

    const v4, -0x41b33333    # -0.2f

    .line 293
    iget-object v5, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v6, 0x0

    invoke-static {v5, v6, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 294
    iget-object v4, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    invoke-static {v4, v5, v2, v3, v8}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 295
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 298
    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    .line 299
    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    .line 300
    new-instance v3, Lcom/google/android/apps/gmm/v/ci;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    .line 301
    const/16 v2, 0x2601

    const/16 v4, 0x2601

    iget-boolean v5, v3, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    iput v2, v3, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, v3, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 302
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->j:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 303
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->j:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v13, v2}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 304
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->j:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v2}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    move-object/from16 v0, v19

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_3
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto/16 :goto_0

    .line 310
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v9, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    array-length v10, v9

    const/4 v2, 0x0

    move v8, v2

    :goto_4
    if-ge v8, v10, :cond_5

    aget-object v2, v9, v8

    .line 311
    new-instance v11, Lcom/google/android/apps/gmm/streetview/internal/a;

    const v3, -0xcfcfd0

    const/16 v4, 0x10

    invoke-direct {v11, v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/a;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aa;II)V

    .line 312
    iget-object v3, v12, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v6, 0x0

    const/16 v7, 0x10

    invoke-static {v3, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v7, Lcom/google/android/apps/gmm/streetview/internal/q;->i:Lcom/google/android/apps/gmm/v/cn;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    neg-float v4, v2

    iget-object v2, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v5, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x1

    aget v6, v6, v13

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x2

    aget v7, v7, v13

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    const/4 v2, 0x0

    const v3, -0x445c28f6    # -0.005f

    const v4, -0x42333333    # -0.1f

    .line 313
    iget-object v5, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v6, 0x0

    invoke-static {v5, v6, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    sget-object v7, Lcom/google/android/apps/gmm/streetview/internal/q;->i:Lcom/google/android/apps/gmm/v/cn;

    const/high16 v4, -0x3cf90000    # -135.0f

    iget-object v2, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v5, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x1

    aget v6, v6, v13

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x2

    aget v7, v7, v13

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 314
    invoke-virtual {v11, v15}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v11, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 310
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_4

    .line 318
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v9, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    array-length v10, v9

    const/4 v2, 0x0

    move v8, v2

    :goto_5
    if-ge v8, v10, :cond_6

    aget-object v2, v9, v8

    .line 319
    new-instance v11, Lcom/google/android/apps/gmm/streetview/internal/a;

    const/4 v3, -0x1

    const/16 v4, 0x10

    invoke-direct {v11, v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/a;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aa;II)V

    .line 321
    iget-object v3, v12, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    iget-object v5, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v6, 0x0

    const/16 v7, 0x10

    invoke-static {v3, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v7, Lcom/google/android/apps/gmm/streetview/internal/q;->i:Lcom/google/android/apps/gmm/v/cn;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    neg-float v4, v2

    iget-object v2, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v5, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x1

    aget v6, v6, v13

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x2

    aget v7, v7, v13

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, -0x42333333    # -0.1f

    .line 322
    iget-object v5, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v6, 0x0

    invoke-static {v5, v6, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    sget-object v7, Lcom/google/android/apps/gmm/streetview/internal/q;->i:Lcom/google/android/apps/gmm/v/cn;

    const/high16 v4, -0x3cf90000    # -135.0f

    .line 323
    iget-object v2, v15, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v5, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x1

    aget v6, v6, v13

    iget-object v7, v7, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v13, 0x2

    aget v7, v7, v13

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 324
    invoke-virtual {v11, v15}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->k:Lcom/google/android/apps/gmm/streetview/internal/s;

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->b:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v11, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 318
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_5

    .line 330
    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/q;->c:Lcom/google/android/apps/gmm/streetview/internal/t;

    .line 332
    :cond_7
    return-void

    :cond_8
    move v9, v6

    goto/16 :goto_3

    :cond_9
    move v10, v7

    goto/16 :goto_2

    :cond_a
    move/from16 v24, v4

    move v4, v6

    move v6, v7

    move v7, v2

    move/from16 v2, v24

    goto/16 :goto_1
.end method
