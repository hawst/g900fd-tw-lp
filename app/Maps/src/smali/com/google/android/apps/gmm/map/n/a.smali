.class public Lcom/google/android/apps/gmm/map/n/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/f/i;
.implements Lcom/google/android/apps/gmm/map/f/z;


# static fields
.field public static final synthetic g:Z


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/a/a;

.field final b:Lcom/google/android/apps/gmm/map/a/b;

.field public final c:Lcom/google/android/apps/gmm/map/b;

.field public d:Lcom/google/android/apps/gmm/map/p;

.field public e:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/r;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private final h:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/map/n/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/map/n/a;->g:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/a/b;Lcom/google/android/apps/gmm/map/a/a;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/map/n/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/b;-><init>(Lcom/google/android/apps/gmm/map/n/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->c:Lcom/google/android/apps/gmm/map/b;

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/a;->b:Lcom/google/android/apps/gmm/map/a/b;

    .line 91
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/n/a;->a:Lcom/google/android/apps/gmm/map/a/a;

    .line 92
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/n/a;->h:Ljava/util/concurrent/Executor;

    .line 93
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/a/b;Lcom/google/android/apps/gmm/map/a/a;Ljava/util/concurrent/Executor;)Lcom/google/android/apps/gmm/map/n/a;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/map/n/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/map/n/a;-><init>(Lcom/google/android/apps/gmm/map/a/b;Lcom/google/android/apps/gmm/map/a/a;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/n/a;->c()Lcom/google/android/apps/gmm/map/p;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_0

    .line 231
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/p;->c()V

    .line 233
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/gmm/map/n/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/map/n/d;-><init>(Lcom/google/android/apps/gmm/map/n/a;Lcom/google/android/apps/gmm/map/f/a/a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 244
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->d:Lcom/google/android/apps/gmm/map/p;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 110
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/a;->a:Lcom/google/android/apps/gmm/map/a/a;

    if-eqz v0, :cond_2

    move-object v0, p0

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/map/a/a;->a(Lcom/google/android/apps/gmm/map/f/z;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->a:Lcom/google/android/apps/gmm/map/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/a/a;->a(Lcom/google/android/apps/gmm/map/f/i;)V

    .line 112
    return-void

    .line 109
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 110
    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/gmm/map/n/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/map/n/c;-><init>(Lcom/google/android/apps/gmm/map/n/a;Lcom/google/android/apps/gmm/map/f/a/a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 224
    return-void
.end method

.method declared-synchronized c()Lcom/google/android/apps/gmm/map/p;
    .locals 2

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->d:Lcom/google/android/apps/gmm/map/p;

    .line 117
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/n/a;->d:Lcom/google/android/apps/gmm/map/p;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/n/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/a;->d:Lcom/google/android/apps/gmm/map/p;

    if-eqz v0, :cond_1

    .line 138
    iget v0, p0, Lcom/google/android/apps/gmm/map/n/a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/n/a;->f:I

    .line 140
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/n/a;->c()Lcom/google/android/apps/gmm/map/p;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/p;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/n/a;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/n/a;->f:I

    .line 148
    :cond_1
    return-void

    .line 145
    :catchall_0
    move-exception v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/n/a;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/map/n/a;->f:I

    throw v0
.end method
