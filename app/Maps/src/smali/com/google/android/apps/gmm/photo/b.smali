.class public Lcom/google/android/apps/gmm/photo/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/photo/a;
.implements Lcom/google/android/apps/gmm/photo/g;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/photo/h;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/photo/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Ljava/lang/Runnable;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/apps/gmm/base/views/c/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/photo/h;ZLjava/util/List;Ljava/lang/Runnable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/photo/h;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/photo/d;",
            ">;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/photo/b;->a:Lcom/google/android/apps/gmm/photo/h;

    .line 42
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/photo/b;->c:Z

    .line 43
    iput-object p5, p0, Lcom/google/android/apps/gmm/photo/b;->d:Ljava/lang/Runnable;

    .line 44
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/photo/d;

    .line 45
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/photo/c;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/photo/c;-><init>(Lcom/google/android/apps/gmm/photo/d;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/aw;

    goto :goto_0
.end method

.method public final a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/photo/d;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/photo/c;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/photo/c;-><init>(Lcom/google/android/apps/gmm/photo/d;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 126
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/b;->e:Ljava/lang/String;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/k;

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v2, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->f:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 137
    return-void
.end method

.method public final b(I)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/photo/b;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->f:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->a:Lcom/google/android/apps/gmm/photo/h;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/photo/b;->c:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/photo/h;->a(Z)V

    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/photo/c;

    .line 98
    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/c;->a:Lcom/google/android/apps/gmm/photo/d;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->a:Lcom/google/android/apps/gmm/photo/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/b;->e:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/photo/b;->c:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/photo/h;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->a:Lcom/google/android/apps/gmm/photo/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/photo/h;->b()V

    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->a:Lcom/google/android/apps/gmm/photo/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/photo/h;->a()V

    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->a:Lcom/google/android/apps/gmm/photo/h;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/photo/b;->c:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/photo/h;->b(Z)V

    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/photo/c;

    .line 73
    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/c;->a:Lcom/google/android/apps/gmm/photo/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/d;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_0
    return-object v1
.end method
