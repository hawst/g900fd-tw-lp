.class Lcom/google/android/apps/gmm/k/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/a/a;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/k/c/a;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field d:Lcom/google/android/apps/gmm/k/f;

.field e:Z

.field f:Lcom/google/android/libraries/a/c;

.field private final h:Lcom/google/android/apps/gmm/base/activities/c;

.field private final i:Lcom/google/r/b/a/zq;

.field private final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/k/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/zq;Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/k/a;->e:Z

    .line 71
    iput-object p1, p0, Lcom/google/android/apps/gmm/k/a;->i:Lcom/google/r/b/a/zq;

    .line 72
    iput-object p2, p0, Lcom/google/android/apps/gmm/k/a;->j:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/k/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/k/b;-><init>(Lcom/google/android/apps/gmm/k/a;)V

    new-instance v1, Lcom/google/android/apps/gmm/k/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/k/c;-><init>(Lcom/google/android/apps/gmm/k/a;)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/k/a;->i:Lcom/google/r/b/a/zq;

    new-instance v5, Lcom/google/android/apps/gmm/k/c/b;

    invoke-direct {v5, v4, v0, v1}, Lcom/google/android/apps/gmm/k/c/b;-><init>(Lcom/google/r/b/a/zq;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/k/a;->a:Lcom/google/android/apps/gmm/k/c/a;

    const-class v0, Lcom/google/android/apps/gmm/k/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->a:Lcom/google/android/apps/gmm/k/c/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/k/a;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/k/c/a;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/a;->b:Landroid/view/View;

    const-class v0, Lcom/google/android/apps/gmm/k/b/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->a:Lcom/google/android/apps/gmm/k/c/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/k/a;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/k/c/a;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/a;->c:Landroid/view/View;

    .line 76
    new-instance v4, Lcom/google/android/libraries/a/n;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/k/a;->i:Lcom/google/r/b/a/zq;

    iget-object v0, v6, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-direct {v4, v5, v0}, Lcom/google/android/libraries/a/n;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->l:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{0}"

    sget-object v5, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{1}"

    sget-object v5, Lcom/google/android/apps/gmm/d/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "app_version"

    iget-object v5, v4, Lcom/google/android/libraries/a/n;->b:Ljava/util/Map;

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    const-string v0, "parent_ei"

    iget-object v1, v4, Lcom/google/android/libraries/a/n;->b:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->v()Lcom/google/r/b/a/zn;

    move-result-object v5

    iget v0, v5, Lcom/google/r/b/a/zn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    const-string v2, "survey_url"

    iget-object v0, v5, Lcom/google/r/b/a/zn;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    :goto_2
    iget-object v1, v4, Lcom/google/android/libraries/a/n;->a:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v0, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/util/q;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v4, Lcom/google/android/libraries/a/n;->a:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v0, Lcom/google/android/libraries/a/c;

    invoke-direct {v0, p3, p0, v4}, Lcom/google/android/libraries/a/c;-><init>(Landroid/content/Context;Lcom/google/android/libraries/a/a;Lcom/google/android/libraries/a/n;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/a;->f:Lcom/google/android/libraries/a/c;

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->a:Lcom/google/android/apps/gmm/k/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    .line 80
    return-void

    .line 76
    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, v6, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v1, v5, Lcom/google/r/b/a/zn;->c:Ljava/lang/Object;

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private a(Ljava/lang/Class;Lcom/google/android/apps/gmm/k/c/a;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/apps/gmm/k/c/a;",
            ">;>;",
            "Lcom/google/android/apps/gmm/k/c/a;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->k:Lcom/google/android/apps/gmm/base/views/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const/4 v2, 0x0

    .line 106
    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 107
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v1, p2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 108
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/k/f;)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/k/f;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/k/f;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 137
    sget-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    const-string v0, "transitioning state backwards: %s to %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 165
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/k/a;->e:Z

    if-nez v0, :cond_2

    .line 143
    sget-object v0, Lcom/google/android/apps/gmm/k/e;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/k/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 164
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    goto :goto_0

    .line 145
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/view/View;)V

    goto :goto_1

    .line 148
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/view/View;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->f:Lcom/google/android/libraries/a/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/a/c;->a()Landroid/app/DialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "hats-survey"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 152
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/view/View;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->f:Lcom/google/android/libraries/a/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/a/c;->a()Landroid/app/DialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    goto :goto_1

    .line 156
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->j()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->f:Lcom/google/android/libraries/a/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/a/c;->a()Landroid/app/DialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    goto :goto_1

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->j()V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->f:Lcom/google/android/libraries/a/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/a/c;->a()Landroid/app/DialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    goto :goto_1

    .line 143
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onSurveyCanceled()V
    .locals 2

    .prologue
    .line 237
    sget-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    if-eq v0, v1, :cond_0

    .line 239
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    .line 241
    :cond_0
    return-void
.end method

.method public onSurveyComplete(ZZ)V
    .locals 4

    .prologue
    .line 209
    if-eqz p1, :cond_0

    .line 210
    sget-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->c:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/k/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/k/d;-><init>(Lcom/google/android/apps/gmm/k/a;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 217
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->e:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    .line 228
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->d:Lcom/google/android/apps/gmm/k/f;

    if-ne v0, v1, :cond_1

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/k/a;->onSurveyCanceled()V

    goto :goto_0

    .line 224
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    .line 225
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    goto :goto_0
.end method

.method public onSurveyReady()V
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    .line 204
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->c:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    .line 205
    return-void
.end method

.method public onSurveyResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/a;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/k/a;->j:Ljava/lang/String;

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    return-void
.end method

.method public onWindowError()V
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lcom/google/android/apps/gmm/k/a;->g:Ljava/lang/String;

    .line 198
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    .line 199
    return-void
.end method
