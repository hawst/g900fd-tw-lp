.class public abstract Lcom/google/android/apps/gmm/myplaces/c/b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/apps/gmm/myplaces/c/f;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/apps/gmm/myplaces/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/k;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/apps/gmm/myplaces/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final d:Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dc",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/myplaces/c/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/myplaces/c/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->b:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 38
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->b:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 39
    invoke-static {v0, v1}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->c:Lcom/google/b/c/dn;

    .line 45
    invoke-static {}, Lcom/google/b/c/dc;->h()Lcom/google/b/c/dd;

    move-result-object v1

    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->c:Lcom/google/b/c/dn;

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 47
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {v1}, Lcom/google/b/c/dd;->a()Lcom/google/b/c/dc;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->d:Lcom/google/b/c/dc;

    .line 50
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/myplaces/c/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->d:Lcom/google/b/c/dc;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/b/c/dc;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/c/b;

    return-object v0
.end method

.method static b(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    .line 151
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/ar;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 152
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->e:Lcom/google/android/apps/gmm/myplaces/c/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/myplaces/c/h;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 153
    iget-wide v2, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 154
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 156
    :cond_0
    return-object v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/google/e/a/a/a/b;"
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "updateItem() is not supported for this corpus."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract a(Lcom/google/e/a/a/a/b;)Z
.end method

.method public abstract b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/myplaces/c/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/e/a/a/a/b;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract c(Lcom/google/e/a/a/a/b;)J
.end method

.method public d(Lcom/google/e/a/a/a/b;)Lcom/google/b/h/c;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract e(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;
.end method
