.class public final Lcom/google/android/apps/gmm/map/b/a/l;
.super Lcom/google/android/apps/gmm/map/b/a/j;
.source "PG"


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    .line 373
    return-void
.end method

.method public static c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 381
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 382
    if-eqz v1, :cond_0

    .line 383
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/l;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/l;-><init>(JJ)V

    .line 385
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 367
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Lcom/google/android/apps/gmm/map/b/a/j;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 394
    if-ne p0, p1, :cond_1

    .line 402
    :cond_0
    :goto_0
    return v0

    .line 398
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/l;

    if-eqz v2, :cond_2

    .line 399
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/l;->c:J

    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/l;

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/l;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 402
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 407
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/l;->c:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/l;->c:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
