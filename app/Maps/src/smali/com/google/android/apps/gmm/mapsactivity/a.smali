.class public Lcom/google/android/apps/gmm/mapsactivity/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mapsactivity/a/f;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field private a:Lcom/google/android/apps/gmm/mapsactivity/a/g;

.field private b:Lcom/google/android/apps/gmm/mapsactivity/a/b;

.field private c:Lcom/google/android/apps/gmm/mapsactivity/a/j;

.field private f:Lcom/google/android/apps/gmm/mapsactivity/a/h;

.field private g:Lcom/google/android/apps/gmm/mapsactivity/a/e;

.field private h:Lcom/google/android/apps/gmm/mapsactivity/a/i;

.field private i:Lcom/google/android/apps/gmm/mapsactivity/c/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mapsactivity/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->a:Lcom/google/android/apps/gmm/mapsactivity/a/g;

    .line 55
    new-instance v1, Lcom/google/android/apps/gmm/mapsactivity/b/a;

    .line 56
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->a:Lcom/google/android/apps/gmm/mapsactivity/a/g;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/mapsactivity/b/a;-><init>(Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/mapsactivity/a/g;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->b:Lcom/google/android/apps/gmm/mapsactivity/a/b;

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/mapsactivity/b/d;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->a:Lcom/google/android/apps/gmm/mapsactivity/a/g;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/mapsactivity/b/d;-><init>(Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/mapsactivity/a/g;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->c:Lcom/google/android/apps/gmm/mapsactivity/a/j;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/b/c;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/mapsactivity/b/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->f:Lcom/google/android/apps/gmm/mapsactivity/a/h;

    .line 60
    new-instance v1, Lcom/google/android/apps/gmm/mapsactivity/b/b;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/mapsactivity/b/b;-><init>(Lcom/google/android/apps/gmm/shared/net/r;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->g:Lcom/google/android/apps/gmm/mapsactivity/a/e;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->c:Lcom/google/android/apps/gmm/mapsactivity/a/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->b:Lcom/google/android/apps/gmm/mapsactivity/a/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/mapsactivity/d/a/d;-><init>(Lcom/google/android/apps/gmm/mapsactivity/a/j;Lcom/google/android/apps/gmm/mapsactivity/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->h:Lcom/google/android/apps/gmm/mapsactivity/a/i;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/c/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/mapsactivity/c/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->i:Lcom/google/android/apps/gmm/mapsactivity/c/a;

    .line 64
    return-void
.end method

.method public final a(Lcom/google/r/b/a/ks;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->f:Lcom/google/android/apps/gmm/mapsactivity/c;

    iput-object p1, v0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mapsactivity/c;->a()V

    .line 117
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/r/b/a/ks;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 73
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->a(Lcom/google/r/b/a/ks;Ljava/lang/String;)Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 75
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->q()Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 82
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/mapsactivity/a/h;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/a;->f:Lcom/google/android/apps/gmm/mapsactivity/a/h;

    return-object v0
.end method
