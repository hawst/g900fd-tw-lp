.class public Lcom/google/android/apps/gmm/map/r/a/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/r/b/a/agl;

.field public final b:Lcom/google/r/b/a/afb;

.field public c:[Lcom/google/android/apps/gmm/map/r/a/ao;

.field d:[Lcom/google/android/apps/gmm/map/r/a/a;

.field private e:Lcom/google/android/apps/gmm/map/r/a/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/agl;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/r/b/a/agl;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    .line 35
    iget v0, p1, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 36
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_1
    iget v0, v0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    .line 37
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_3
    iget-object v1, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->d:[Lcom/google/android/apps/gmm/map/r/a/a;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ao;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 40
    return-void

    .line 37
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_3

    :cond_7
    iget-object v0, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_4
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/a/h;
    .locals 13
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->e:Lcom/google/android/apps/gmm/map/r/a/h;

    if-nez v0, :cond_1

    move v0, v1

    .line 252
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/e;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v8

    if-nez v0, :cond_2

    move-object v0, v5

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->e:Lcom/google/android/apps/gmm/map/r/a/h;

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->e:Lcom/google/android/apps/gmm/map/r/a/h;

    return-object v0

    .line 252
    :cond_2
    array-length v9, v8

    move v7, v1

    move v3, v1

    move-object v4, v5

    move-object v2, v5

    :goto_2
    if-ge v7, v9, :cond_d

    aget-object v10, v8, v7

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v6, 0x1000

    if-ne v0, v6, :cond_6

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_10

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v6, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v6, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v0

    :goto_4
    iget-boolean v0, v0, Lcom/google/maps/g/a/bi;->j:Z

    if-nez v0, :cond_10

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v6, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v6, :cond_8

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v0

    move-object v6, v0

    :goto_5
    if-eqz v4, :cond_3

    iget-object v0, v6, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-le v0, v11, :cond_4

    :cond_3
    iget-object v0, v6, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :cond_4
    if-eqz v2, :cond_5

    iget-object v0, v6, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ge v0, v11, :cond_f

    :cond_5
    iget-object v0, v6, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_6
    iget-object v2, v10, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v6, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v6, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_7
    iget-object v6, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v6, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_8
    iget v2, v2, Lcom/google/maps/g/a/ai;->b:I

    if-le v2, v3, :cond_11

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_9
    iget-object v3, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v3, :cond_c

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_a
    iget v2, v2, Lcom/google/maps/g/a/ai;->b:I

    move-object v3, v0

    move v0, v2

    move-object v2, v4

    :goto_b
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move-object v4, v2

    move-object v2, v3

    move v3, v0

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_3

    :cond_7
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    goto/16 :goto_4

    :cond_8
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    move-object v6, v0

    goto/16 :goto_5

    :cond_9
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_7

    :cond_a
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_8

    :cond_b
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_9

    :cond_c
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_a

    :cond_d
    if-eqz v2, :cond_e

    if-eqz v4, :cond_e

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/h;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/r/a/h;-><init>(III)V

    goto/16 :goto_1

    :cond_e
    move-object v0, v5

    goto/16 :goto_1

    :cond_f
    move-object v0, v2

    goto :goto_6

    :cond_10
    move v0, v3

    move-object v3, v2

    move-object v2, v4

    goto :goto_b

    :cond_11
    move-object v2, v4

    move v12, v3

    move-object v3, v0

    move v0, v12

    goto :goto_b
.end method

.method a(I)V
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v2, v0, p0}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v2, v1, p1

    .line 109
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 227
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v0

    .line 231
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/r/a/e;

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    if-eq v1, v2, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 241
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
