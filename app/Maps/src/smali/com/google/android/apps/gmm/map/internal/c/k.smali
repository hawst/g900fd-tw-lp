.class public Lcom/google/android/apps/gmm/map/internal/c/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field private a:J

.field private final b:I

.field private final c:I

.field private d:Lcom/google/android/apps/gmm/map/internal/c/be;


# direct methods
.method private constructor <init>(JIILcom/google/android/apps/gmm/map/internal/c/be;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->a:J

    .line 26
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->b:I

    .line 27
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->c:I

    .line 28
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 29
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/k;
    .locals 7

    .prologue
    .line 79
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/k;

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v2, v0

    .line 80
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 81
    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    .line 82
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/k;-><init>(JIILcom/google/android/apps/gmm/map/internal/c/be;)V

    return-object v1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->a:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->b:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->c:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/k;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method
