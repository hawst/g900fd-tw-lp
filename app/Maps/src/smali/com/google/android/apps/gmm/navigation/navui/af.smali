.class public Lcom/google/android/apps/gmm/navigation/navui/af;
.super Lcom/google/android/apps/gmm/navigation/navui/f;
.source "PG"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field d:Z

.field e:Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;

.field private g:Z

.field private h:Z

.field private i:I

.field private j:Lcom/google/android/apps/gmm/navigation/navui/ai;

.field private final k:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/af;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/af;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/navui/f;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->d:Z

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ai;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/navui/ai;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->j:Lcom/google/android/apps/gmm/navigation/navui/ai;

    .line 74
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->k:Lcom/google/android/apps/gmm/base/activities/c;

    .line 75
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/base/a;Z)Z
    .locals 10

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    const/4 v2, 0x0

    .line 166
    new-instance v3, Lcom/google/android/apps/gmm/navigation/util/a;

    invoke-direct {v3, p1, p2}, Lcom/google/android/apps/gmm/navigation/util/a;-><init>(Lcom/google/android/apps/gmm/base/a;Z)V

    .line 167
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v4

    .line 168
    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v4, v4, Lcom/google/r/b/a/ou;->Y:I

    .line 170
    if-gez v4, :cond_0

    move v0, v2

    .line 179
    :goto_0
    return v0

    .line 174
    :cond_0
    iget-object v5, v3, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    iget-wide v6, v5, Lcom/google/r/b/a/a/a/b;->b:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gtz v5, :cond_2

    :cond_1
    :goto_1
    long-to-float v0, v0

    int-to-float v1, v4

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    .line 179
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->i:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    .line 174
    :cond_2
    iget-object v5, v3, Lcom/google/android/apps/gmm/navigation/util/a;->b:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/h/a;->b(Landroid/content/Context;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    iget-wide v0, v0, Lcom/google/r/b/a/a/a/b;->c:J

    int-to-long v6, v5

    mul-long/2addr v0, v6

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    iget-wide v6, v3, Lcom/google/r/b/a/a/a/b;->b:J

    div-long/2addr v0, v6

    goto :goto_1

    :cond_3
    move v0, v2

    .line 179
    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/navui/af;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->h:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->k:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/gmm/navigation/navui/af;->a(Lcom/google/android/apps/gmm/base/a;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/af;->a(Lcom/google/android/apps/gmm/base/a;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ag;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/gmm/navigation/navui/ag;-><init>(Lcom/google/android/apps/gmm/navigation/navui/af;Z)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->j:Lcom/google/android/apps/gmm/navigation/navui/ai;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->k:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;

    invoke-direct {v4, v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/navui/av;Z)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->e:Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->k:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->e:Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/navui/af;->f:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->d:Z

    goto :goto_0
.end method


# virtual methods
.method public final G_()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->e:Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->e:Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/SavePowerDialog;->dismiss()V

    .line 109
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 97
    if-eqz p1, :cond_0

    .line 98
    const-string v0, "RNDC_clp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->d:Z

    .line 100
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/navigation/navui/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 80
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 81
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->g:Z

    if-nez v2, :cond_0

    .line 82
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->g:Z

    .line 84
    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v2, v3, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->h:Z

    .line 85
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->i:I

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->k:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/ah;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/navui/ah;-><init>(Lcom/google/android/apps/gmm/navigation/navui/af;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(Lcom/google/android/apps/gmm/mylocation/b/c;)V

    .line 88
    :cond_0
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->g:Z

    .line 93
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    const-string v0, "RNDC_clp"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/af;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    return-void
.end method
