.class public final Lcom/google/android/apps/gmm/car/b/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field final b:F

.field public final c:Landroid/widget/FrameLayout;

.field public final d:Landroid/os/Handler;

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/Runnable;

.field private final g:Landroid/view/LayoutInflater;

.field private final h:Lcom/google/android/apps/gmm/car/b/f;

.field private final i:Lcom/google/android/apps/gmm/car/b/f;

.field private final j:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/car/b/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/google/android/gms/car/support/CarAppLayout;F)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->d:Landroid/os/Handler;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->e:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/car/b/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/b/c;-><init>(Lcom/google/android/apps/gmm/car/b/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->f:Ljava/lang/Runnable;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/car/b/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/b/d;-><init>(Lcom/google/android/apps/gmm/car/b/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->h:Lcom/google/android/apps/gmm/car/b/f;

    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/car/b/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/b/e;-><init>(Lcom/google/android/apps/gmm/car/b/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->i:Lcom/google/android/apps/gmm/car/b/f;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->h:Lcom/google/android/apps/gmm/car/b/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/b/b;->i:Lcom/google/android/apps/gmm/car/b/f;

    invoke-static {v0, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->j:Lcom/google/b/c/cv;

    .line 52
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->g:Landroid/view/LayoutInflater;

    .line 53
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/b/b;->a:Landroid/view/ViewGroup;

    .line 54
    iput p3, p0, Lcom/google/android/apps/gmm/car/b/b;->b:F

    .line 55
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/b/b;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 20
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/b/b;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/b/b;->g:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/view/View;II)V
    .locals 5

    .prologue
    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    :cond_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->j:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/b/f;

    .line 103
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/car/b/f;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    .line 104
    if-eqz v2, :cond_2

    .line 105
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput p3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 109
    :cond_3
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 110
    check-cast p1, Landroid/view/ViewGroup;

    .line 111
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 112
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, p3

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/b/b;->a(Landroid/view/View;II)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
