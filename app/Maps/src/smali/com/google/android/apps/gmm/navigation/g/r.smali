.class public Lcom/google/android/apps/gmm/navigation/g/r;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    .line 45
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v4

    mul-double v6, v8, v4

    .line 47
    invoke-virtual {p0, p2, v6, v7, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v4, v2

    .line 50
    :goto_0
    if-eqz v4, :cond_0

    .line 51
    invoke-virtual {p1, p2, v6, v7, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v2

    :goto_1
    if-nez v0, :cond_3

    :cond_0
    move v0, v1

    .line 78
    :goto_2
    return v0

    .line 47
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    move-object v4, v0

    goto :goto_0

    .line 51
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_1

    .line 55
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 56
    array-length v0, v5

    add-int/lit8 v0, v0, -0x2

    move v3, v0

    :goto_3
    if-ltz v3, :cond_6

    .line 57
    aget-object v0, v5, v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 58
    add-int/lit8 v6, v3, 0x1

    aget-object v6, v5, v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 63
    add-int/2addr v0, v6

    div-int/lit8 v0, v0, 0x2

    .line 64
    iget v6, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    if-le v0, v6, :cond_6

    .line 66
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 72
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    mul-double/2addr v6, v8

    .line 73
    invoke-virtual {p1, v0, v6, v7, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v2

    :goto_4
    if-nez v0, :cond_5

    move v0, v1

    .line 74
    goto :goto_2

    .line 73
    :cond_4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_4

    .line 56
    :cond_5
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_3

    .line 78
    :cond_6
    const/4 v0, 0x1

    goto :goto_2
.end method
