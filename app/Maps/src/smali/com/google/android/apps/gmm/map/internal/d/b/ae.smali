.class Lcom/google/android/apps/gmm/map/internal/d/b/ae;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[B

.field final b:I

.field c:I

.field d:I

.field e:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 3335
    const/16 v0, 0x2000

    new-array v0, v0, [B

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;-><init>(I[B)V

    .line 3336
    return-void
.end method

.method private constructor <init>(I[B)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3331
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d:I

    .line 3332
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    .line 3339
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    .line 3340
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    .line 3341
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    .line 3342
    return-void
.end method

.method constructor <init>([B)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 3344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3331
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d:I

    .line 3332
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    .line 3345
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    .line 3346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    .line 3347
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    .line 3348
    return-void
.end method


# virtual methods
.method final a()I
    .locals 3

    .prologue
    .line 3417
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-nez v0, :cond_0

    .line 3418
    const/4 v0, 0x0

    .line 3424
    :goto_0
    return v0

    .line 3420
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d:I

    if-gez v0, :cond_1

    .line 3421
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move-result-object v0

    .line 3422
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v1, v2

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d:I

    .line 3424
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d:I

    goto :goto_0
.end method

.method final a(I)J
    .locals 6

    .prologue
    .line 3386
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v2

    int-to-long v2, v2

    add-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    int-to-long v0, v0

    const-wide v4, 0xffffffffL

    and-long/2addr v0, v4

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;)V
    .locals 2

    .prologue
    .line 3406
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)V

    .line 3407
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    .line 3408
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    add-int/2addr v0, v1

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->d:I

    .line 3409
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    .line 3410
    return-void
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/b/ad;I)V
    .locals 6

    .prologue
    .line 3413
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v1, p2, 0x14

    add-int/lit8 v1, v1, 0x8

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    const/16 v4, 0x20

    shr-long v4, v2, v4

    long-to-int v4, v4

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v4, v1, 0x4

    long-to-int v2, v2

    invoke-static {v0, v4, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v1, v1, 0x8

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    shl-int/lit8 v2, v2, 0x5

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    or-int/2addr v2, v3

    ushr-int/lit8 v3, v2, 0x5

    iget v4, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    if-eq v3, v4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not pack data offset of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v3, v2, 0x1f

    iget v4, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    if-eq v3, v4, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not pack refCount of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v1, v1, 0x4

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    shl-int/lit8 v2, v2, 0x18

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    or-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v1, v1, 0x4

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 3414
    return-void
.end method

.method final b()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3428
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 3429
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    .line 3430
    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge v0, v1, :cond_1

    .line 3431
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c(I)I

    move-result v1

    if-lez v1, :cond_0

    .line 3432
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v4, v0, 0x14

    add-int/lit8 v4, v4, 0x8

    add-int/lit8 v4, v4, 0x8

    add-int/lit8 v4, v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v3

    const v4, 0xffffff

    and-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    .line 3430
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3436
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->e:I

    return v0
.end method

.method final b(I)I
    .locals 2

    .prologue
    .line 3390
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    add-int/lit8 v1, v1, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    ushr-int/lit8 v0, v0, 0x18

    return v0
.end method

.method final c(I)I
    .locals 2

    .prologue
    .line 3398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method final d(I)Lcom/google/android/apps/gmm/map/internal/d/b/ad;
    .locals 11

    .prologue
    .line 3402
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v1, p1, 0x14

    add-int/lit8 v1, v1, 0x8

    iget v9, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v2

    int-to-long v2, v2

    add-int/lit8 v4, v1, 0x4

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v4

    int-to-long v4, v4

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    const/16 v6, 0x20

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    add-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v5

    add-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v6

    add-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v8

    ushr-int/lit8 v4, v5, 0x5

    and-int/lit8 v7, v5, 0x1f

    ushr-int/lit8 v5, v6, 0x18

    const v0, 0xffffff

    and-int/2addr v6, v0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    move v10, p1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/b/ad;-><init>(JIIIIIII)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3441
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ID:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Size:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
