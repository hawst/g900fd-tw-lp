.class Lcom/google/android/apps/gmm/reportmapissue/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/t;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/a;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/a;

    .line 88
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/g/a;->g:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    new-instance v2, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 95
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/g/a;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    .line 96
    iput-boolean v6, v0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 97
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v5, v2, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 98
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 94
    iput-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    .line 102
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/shared/net/i;

    move-result-object v1

    .line 101
    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    .line 107
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/g/b;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->j()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    .line 106
    sget-object v3, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v1, v2, v3, v5}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->c(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 9
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/z;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->j()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 57
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v3, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    sget-object v4, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v3, v0, v4, v6}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/reportmapissue/n;->l()Z

    move-result v2

    if-nez v2, :cond_1

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/reportmapissue/n;->f()V

    .line 65
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    .line 67
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    .line 68
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    move-object v2, v1

    .line 66
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/reportmapissue/n;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Landroid/content/res/Resources;ZLcom/google/android/apps/gmm/reportmapissue/a/h;)Lcom/google/android/apps/gmm/reportmapissue/n;

    move-result-object v0

    .line 65
    iput-object v0, v8, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/k;->a:Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V

    goto/16 :goto_0
.end method
