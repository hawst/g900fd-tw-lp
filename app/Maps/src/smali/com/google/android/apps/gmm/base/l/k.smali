.class public final enum Lcom/google/android/apps/gmm/base/l/k;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/l/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/l/k;

.field public static final enum b:Lcom/google/android/apps/gmm/base/l/k;

.field public static final enum c:Lcom/google/android/apps/gmm/base/l/k;

.field public static final enum d:Lcom/google/android/apps/gmm/base/l/k;

.field public static final enum e:Lcom/google/android/apps/gmm/base/l/k;

.field public static final enum f:Lcom/google/android/apps/gmm/base/l/k;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/base/l/k;


# instance fields
.field final g:Lcom/google/android/libraries/curvular/aw;

.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/base/l/k;

    const-string v1, "NO_TINT_ON_BLUE"

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->l:Lcom/google/android/apps/gmm/base/h/c;

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/google/android/apps/gmm/base/l/k;-><init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->a:Lcom/google/android/apps/gmm/base/l/k;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/base/l/k;

    const-string v1, "NO_TINT_ON_WHITE"

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->k:Lcom/google/android/apps/gmm/base/h/c;

    invoke-direct {v0, v1, v6, v2, v5}, Lcom/google/android/apps/gmm/base/l/k;-><init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->b:Lcom/google/android/apps/gmm/base/l/k;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/base/l/k;

    const-string v1, "WHITE_ON_BLUE"

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->l:Lcom/google/android/apps/gmm/base/h/c;

    sget v3, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/base/l/k;-><init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->c:Lcom/google/android/apps/gmm/base/l/k;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/base/l/k;

    const-string v1, "BLUE_ON_WHITE"

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->k:Lcom/google/android/apps/gmm/base/h/c;

    sget v3, Lcom/google/android/apps/gmm/d;->X:I

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/gmm/base/l/k;-><init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->d:Lcom/google/android/apps/gmm/base/l/k;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/base/l/k;

    const-string v1, "WHITE_ON_NIGHTBLACK"

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->m:Lcom/google/android/apps/gmm/base/h/c;

    sget v3, Lcom/google/android/apps/gmm/d;->aR:I

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/gmm/base/l/k;-><init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->e:Lcom/google/android/apps/gmm/base/l/k;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/base/l/k;

    const-string v1, "WHITE_ON_RED"

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/apps/gmm/base/h/c;->n:Lcom/google/android/apps/gmm/base/h/c;

    sget v4, Lcom/google/android/apps/gmm/d;->aR:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/l/k;-><init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->f:Lcom/google/android/apps/gmm/base/l/k;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/l/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/k;->a:Lcom/google/android/apps/gmm/base/l/k;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/base/l/k;->b:Lcom/google/android/apps/gmm/base/l/k;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/base/l/k;->c:Lcom/google/android/apps/gmm/base/l/k;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/base/l/k;->d:Lcom/google/android/apps/gmm/base/l/k;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/gmm/base/l/k;->e:Lcom/google/android/apps/gmm/base/l/k;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/base/l/k;->f:Lcom/google/android/apps/gmm/base/l/k;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/base/l/k;->i:[Lcom/google/android/apps/gmm/base/l/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/libraries/curvular/aw;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/aw;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/k;->g:Lcom/google/android/libraries/curvular/aw;

    .line 33
    iput p4, p0, Lcom/google/android/apps/gmm/base/l/k;->h:I

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/l/k;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/base/l/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/l/k;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/l/k;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->i:[Lcom/google/android/apps/gmm/base/l/k;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/l/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/l/k;

    return-object v0
.end method
