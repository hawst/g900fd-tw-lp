.class Lcom/google/android/apps/gmm/map/legacy/internal/b/e;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/c;
.source "PG"


# instance fields
.field final synthetic d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/b/d;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;)V
    .locals 1

    .prologue
    .line 397
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    .line 398
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/c;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    .line 399
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    .line 404
    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->a(Lcom/google/android/apps/gmm/map/f/o;)V

    .line 407
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 12

    .prologue
    const v8, 0x71c71c7

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 414
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    if-nez v0, :cond_3

    move v0, v4

    :goto_0
    if-eqz v0, :cond_13

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iput-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_7

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/bb;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, v5

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v7, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v5, v7

    if-gt v3, v8, :cond_1

    if-le v5, v8, :cond_a

    :cond_1
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/bb;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-virtual {v1, v5, v3, v7, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(IIII)V

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x2

    rsub-int/lit8 v0, v0, 0x1e

    shl-int v0, v1, v0

    div-int/lit16 v3, v0, 0x100

    move v1, v2

    :goto_2
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    int-to-float v7, v3

    invoke-static {v0, v7, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;FI)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    move-result-object v0

    invoke-interface {v5, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->g:F

    iget v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->f:F

    mul-float/2addr v1, v3

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_4

    iget v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->f:F

    div-float/2addr v1, v3

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    :cond_4
    move v0, v4

    goto/16 :goto_0

    :cond_5
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v4

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :cond_7
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    if-nez v1, :cond_8

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    const/16 v3, 0x2000

    int-to-float v3, v3

    const/16 v5, 0x8

    invoke-static {v1, v3, v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;FI)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    const/high16 v3, 0x20000

    int-to-float v3, v3

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;FI)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->c:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    :cond_8
    const/high16 v1, 0x40c00000    # 6.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    goto/16 :goto_1

    :cond_9
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->c:Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    goto/16 :goto_1

    :cond_a
    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    mul-int/lit8 v3, v3, 0x4

    mul-int/lit8 v5, v5, 0x4

    invoke-direct {v7, v3, v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {v3, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->k(Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v1, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->k(Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget-object v5, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget v7, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v8, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-virtual {v5, v7, v3, v8, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(IIII)V

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/b;

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/map/b/a/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/af;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    if-nez v1, :cond_b

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v3, v0, v5}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Ljava/util/List;)V

    move v1, v2

    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v7, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_b
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    array-length v1, v1

    new-array v9, v1, [I

    move v1, v2

    :goto_4
    array-length v10, v9

    if-ge v1, v10, :cond_c

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v10, v10, v1

    iget v11, v10, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    and-int/lit16 v11, v11, -0x100

    iget-byte v10, v10, Lcom/google/android/apps/gmm/map/internal/c/aq;->b:B

    and-int/lit16 v10, v10, 0xff

    or-int/2addr v10, v11

    aput v10, v9, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_c
    invoke-virtual {v3, v8, v9, v5, v7}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ab;[ILjava/util/List;Ljava/util/List;)V

    move v3, v2

    :goto_5
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    iget-object v8, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    invoke-direct {v9, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;[I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_d
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->b()V

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->b()V

    move v5, v2

    :goto_6
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_12

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v7, v1, 0x3

    if-eqz v7, :cond_10

    move v3, v2

    move v4, v2

    :goto_7
    add-int/lit8 v1, v7, -0x1

    if-ge v3, v1, :cond_f

    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a(I)Z

    move-result v1

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a(I)Z

    move-result v8

    if-eq v1, v8, :cond_14

    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    :goto_8
    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v0, v4, v8}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a(II)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;)V

    move v1, v3

    :goto_9
    add-int/lit8 v3, v3, 0x1

    move v4, v1

    goto :goto_7

    :cond_e
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    goto :goto_8

    :cond_f
    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    :goto_a
    invoke-virtual {v0, v4, v7}, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a(II)Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;)V

    :cond_10
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_6

    :cond_11
    iget-object v1, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    goto :goto_a

    :cond_12
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->g:F

    iput v0, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->f:F

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d()V

    .line 415
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/e;->d:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 417
    return-void

    :cond_14
    move v1, v4

    goto :goto_9
.end method
