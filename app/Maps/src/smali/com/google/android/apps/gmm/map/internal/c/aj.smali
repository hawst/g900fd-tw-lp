.class public Lcom/google/android/apps/gmm/map/internal/c/aj;
.super Lcom/google/android/apps/gmm/map/internal/c/cm;
.source "PG"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/cm;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/aj;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cm;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 43
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/cm;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cd;)V
    .locals 21

    .prologue
    .line 55
    .line 56
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->e:Lcom/google/android/apps/gmm/map/internal/c/bs;

    if-nez p3, :cond_0

    .line 58
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 59
    :goto_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/c/ct;->i:I

    .line 60
    move-object/from16 v0, p1

    iget-byte v6, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->c:B

    .line 61
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget v7, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 64
    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->g:I

    const/4 v11, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, -0x1

    .line 69
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget-wide v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    move-wide/from16 v16, v0

    .line 70
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->j:Lcom/google/android/apps/gmm/map/internal/c/ct;

    iget v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bt;->h:I

    move/from16 v18, v0

    const/4 v2, 0x0

    new-array v0, v2, [Lcom/google/android/apps/gmm/map/internal/c/i;

    move-object/from16 v19, v0

    .line 72
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->k:Z

    move/from16 v20, v0

    move-object/from16 v2, p0

    move-object/from16 v12, p2

    .line 55
    invoke-direct/range {v2 .. v20}, Lcom/google/android/apps/gmm/map/internal/c/cm;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bp;IBI[Ljava/lang/String;[Ljava/lang/String;ILjava/util/List;Lcom/google/android/apps/gmm/map/b/a/ai;[Lcom/google/android/apps/gmm/map/internal/c/cj;JJI[Lcom/google/android/apps/gmm/map/internal/c/i;Z)V

    .line 74
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    .line 75
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->h()Lcom/google/android/apps/gmm/map/internal/c/cp;

    move-result-object v2

    .line 76
    :goto_1
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 58
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cm;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v5, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v6, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    move-object/from16 v0, p3

    invoke-direct {v4, v5, v6, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    goto :goto_0

    .line 79
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->l:Ljava/util/List;

    .line 80
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->f()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 81
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->l:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->f()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 83
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->m:Ljava/util/List;

    .line 84
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->e()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/aj;->m:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/cm;->e()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 87
    :cond_3
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/cm;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 96
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/aj;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/map/internal/c/aj;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cm;)V

    move v1, v2

    .line 97
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    array-length v0, v0

    :goto_1
    if-ge v1, v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->i:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    aget-object v0, v0, v1

    .line 99
    :goto_2
    instance-of v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ck;

    if-eqz v4, :cond_0

    .line 100
    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ck;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ck;->a:Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 98
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 103
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->k:Z

    if-eqz v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 108
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 111
    :cond_4
    return-object v3
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/cm;Ljava/util/Map;)Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/cm;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/l;",
            "Lcom/google/android/apps/gmm/map/internal/c/cm;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/internal/c/cm;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 132
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v5

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->h()Lcom/google/android/apps/gmm/map/internal/c/cp;

    move-result-object v3

    .line 134
    :cond_0
    :goto_0
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 137
    const/4 v1, 0x0

    .line 138
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v2

    const/4 v6, 0x3

    if-ne v2, v6, :cond_3

    move-object v1, v0

    .line 142
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/h;->p:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 150
    :cond_1
    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 151
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 152
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 153
    if-nez v2, :cond_2

    .line 154
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 155
    invoke-interface {v5, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_3
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v2

    const/4 v6, 0x7

    if-ne v2, v6, :cond_4

    move-object v1, v0

    .line 144
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/an;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    goto :goto_1

    .line 145
    :cond_4
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v2

    const/16 v6, 0x8

    if-eq v2, v6, :cond_5

    .line 146
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->h()I

    move-result v2

    const/16 v6, 0xb

    if-ne v2, v6, :cond_1

    :cond_5
    move-object v1, v0

    .line 147
    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/ad;->k:Lcom/google/android/apps/gmm/map/indoor/d/g;

    goto :goto_1

    .line 162
    :cond_6
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 195
    :goto_2
    return-object p0

    .line 168
    :cond_7
    instance-of v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;

    if-eqz v0, :cond_9

    move-object v0, p0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aj;

    move-object v2, v0

    .line 171
    :goto_3
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 172
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 173
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 174
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v1, v4

    :goto_4
    if-ge v1, v6, :cond_8

    .line 175
    iget-object v7, v2, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 168
    :cond_9
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/aj;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cm;)V

    move-object v2, v0

    goto :goto_3

    .line 179
    :cond_a
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 180
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 181
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 182
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    .line 183
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/w;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Lcom/google/android/apps/gmm/map/internal/c/w;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    .line 185
    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/aj;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v7, p0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/c/aj;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cm;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 187
    iget-object v1, v7, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 188
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 189
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    move v3, v4

    :goto_6
    if-ge v3, v8, :cond_b

    .line 190
    iget-object v9, v7, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 193
    :cond_b
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-interface {p1, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_c
    move-object p0, v2

    .line 195
    goto/16 :goto_2
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/map/internal/c/m;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public final e()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;->m:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final f()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final h()Lcom/google/android/apps/gmm/map/internal/c/cp;
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/ak;-><init>(Lcom/google/android/apps/gmm/map/internal/c/aj;)V

    return-object v0
.end method
