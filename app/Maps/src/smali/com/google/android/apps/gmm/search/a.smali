.class public Lcom/google/android/apps/gmm/search/a;
.super Lcom/google/android/apps/gmm/base/placelists/a/d;
.source "PG"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/google/android/apps/gmm/search/b;

.field private final d:I

.field private final e:Landroid/view/View$OnClickListener;

.field private f:Lcom/google/android/apps/gmm/search/d/a;

.field private g:Lcom/google/android/apps/gmm/map/r/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;ILandroid/view/View$OnClickListener;Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 0
    .param p5    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/base/placelists/a/d;-><init>(Lcom/google/android/apps/gmm/base/g/c;)V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/a;->b:Landroid/content/Context;

    .line 40
    iput p3, p0, Lcom/google/android/apps/gmm/search/a;->d:I

    .line 41
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/a;->e:Landroid/view/View$OnClickListener;

    .line 42
    iput-object p5, p0, Lcom/google/android/apps/gmm/search/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 43
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 69
    sget v0, Lcom/google/android/apps/gmm/h;->an:I

    return v0
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/views/a/f;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/search/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/b;-><init>()V

    .line 58
    iput-object p1, v0, Lcom/google/android/apps/gmm/search/b;->a:Landroid/view/View;

    .line 59
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/a/f;)V
    .locals 6

    .prologue
    .line 47
    check-cast p1, Lcom/google/android/apps/gmm/search/b;

    iput-object p1, p0, Lcom/google/android/apps/gmm/search/a;->c:Lcom/google/android/apps/gmm/search/b;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/search/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/a;->a:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/a;->c:Lcom/google/android/apps/gmm/search/b;

    iget-object v3, v3, Lcom/google/android/apps/gmm/search/b;->a:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/a;->e:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/search/d/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Landroid/view/View;Landroid/view/View$OnClickListener;Lcom/google/android/apps/gmm/map/r/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/a;->f:Lcom/google/android/apps/gmm/search/d/a;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/a;->c:Lcom/google/android/apps/gmm/search/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/b;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/a;->f:Lcom/google/android/apps/gmm/search/d/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 53
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/a;->f:Lcom/google/android/apps/gmm/search/d/a;

    if-eqz v0, :cond_0

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/a;->f:Lcom/google/android/apps/gmm/search/d/a;

    iput-object p1, v0, Lcom/google/android/apps/gmm/search/d/a;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/d/a;->w()V

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 83
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/apps/gmm/search/a;->d:I

    return v0
.end method
