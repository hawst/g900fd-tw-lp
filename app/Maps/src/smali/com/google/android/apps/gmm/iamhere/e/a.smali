.class public Lcom/google/android/apps/gmm/iamhere/e/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/iamhere/e/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/e/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/iamhere/c/o;)Lcom/google/j/d/a/b;
    .locals 1
    .param p0    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/j/d/a/w;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/iamhere/c/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 44
    .line 46
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 44
    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/iamhere/c/o;Ljava/util/List;)Lcom/google/j/d/a/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/iamhere/c/o;Ljava/util/List;)Lcom/google/j/d/a/b;
    .locals 11
    .param p0    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/j/d/a/w;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/iamhere/c/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/j/d/a/e;",
            "Lcom/google/j/d/a/w;",
            "Lcom/google/b/f/t;",
            "Lcom/google/android/apps/gmm/iamhere/c/o;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;)",
            "Lcom/google/j/d/a/b;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 84
    invoke-static {}, Lcom/google/j/d/a/b;->newBuilder()Lcom/google/j/d/a/d;

    move-result-object v1

    .line 85
    if-eqz p1, :cond_1

    .line 86
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, v1, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/j/d/a/d;->a:I

    iget v0, p1, Lcom/google/j/d/a/w;->k:I

    iput v0, v1, Lcom/google/j/d/a/d;->b:I

    .line 88
    :cond_1
    if-eqz p0, :cond_3

    .line 89
    if-nez p0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v0, v1, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/j/d/a/d;->a:I

    iget v0, p0, Lcom/google/j/d/a/e;->h:I

    iput v0, v1, Lcom/google/j/d/a/d;->d:I

    .line 91
    :cond_3
    if-eqz p2, :cond_4

    .line 92
    iget v0, p2, Lcom/google/b/f/t;->gy:I

    iget v2, v1, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/j/d/a/d;->a:I

    iput v0, v1, Lcom/google/j/d/a/d;->c:I

    .line 95
    :cond_4
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 96
    invoke-static {}, Lcom/google/j/d/a/k;->newBuilder()Lcom/google/j/d/a/m;

    move-result-object v2

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/l;

    invoke-static {}, Lcom/google/j/d/a/g;->newBuilder()Lcom/google/j/d/a/i;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    invoke-virtual {v4, v5}, Lcom/google/j/d/a/i;->a(Ljava/lang/Iterable;)Lcom/google/j/d/a/i;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/iamhere/c/a;->a:Lcom/google/android/apps/gmm/iamhere/c/k;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v5, v4, Lcom/google/j/d/a/i;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, v4, Lcom/google/j/d/a/i;->a:I

    iput-object v0, v4, Lcom/google/j/d/a/i;->b:Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/j/d/a/m;->c()V

    iget-object v0, v2, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    invoke-virtual {v4}, Lcom/google/j/d/a/i;->g()Lcom/google/n/t;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    invoke-virtual {v2}, Lcom/google/j/d/a/m;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/k;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget-object v2, v1, Lcom/google/j/d/a/d;->g:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v10, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v9, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v1, Lcom/google/j/d/a/d;->a:I

    .line 99
    :cond_8
    if-nez p3, :cond_9

    .line 100
    invoke-virtual {v1}, Lcom/google/j/d/a/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/b;

    .line 116
    :goto_1
    return-object v0

    .line 103
    :cond_9
    iget-object v0, p3, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    if-eqz v0, :cond_c

    .line 104
    invoke-static {}, Lcom/google/j/d/a/o;->newBuilder()Lcom/google/j/d/a/q;

    move-result-object v0

    .line 105
    iget-object v2, p3, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iput-object v2, v0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    iget v2, v0, Lcom/google/j/d/a/q;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/j/d/a/q;->a:I

    .line 106
    invoke-virtual {v0}, Lcom/google/j/d/a/q;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/o;

    .line 104
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget-object v2, v1, Lcom/google/j/d/a/d;->e:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v10, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v9, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/j/d/a/d;->a:I

    .line 108
    :cond_c
    iget-object v0, p3, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    if-eqz v0, :cond_e

    .line 109
    iget-object v0, p3, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget v2, v1, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v1, Lcom/google/j/d/a/d;->a:I

    iput-object v0, v1, Lcom/google/j/d/a/d;->h:Lcom/google/n/f;

    .line 112
    :cond_e
    iget-object v0, p3, Lcom/google/android/apps/gmm/iamhere/c/o;->k:Lcom/google/r/b/a/amd;

    .line 113
    if-eqz v0, :cond_f

    .line 114
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/j/d/a/d;Lcom/google/r/b/a/amd;)V

    .line 116
    :cond_f
    invoke-virtual {v1}, Lcom/google/j/d/a/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/b;

    goto :goto_1
.end method

.method public static a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Ljava/util/List;)Lcom/google/j/d/a/b;
    .locals 1
    .param p0    # Lcom/google/j/d/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/j/d/a/w;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/j/d/a/e;",
            "Lcom/google/j/d/a/w;",
            "Lcom/google/b/f/t;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;)",
            "Lcom/google/j/d/a/b;"
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/iamhere/e/a;->a(Lcom/google/j/d/a/e;Lcom/google/j/d/a/w;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/iamhere/c/o;Ljava/util/List;)Lcom/google/j/d/a/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/j/d/a/d;Lcom/google/r/b/a/amd;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alu;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alu;

    .line 122
    invoke-static {}, Lcom/google/j/d/a/s;->newBuilder()Lcom/google/j/d/a/u;

    move-result-object v3

    .line 124
    iget v1, v0, Lcom/google/r/b/a/alu;->b:I

    if-ne v1, v8, :cond_1

    iget-object v1, v0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/ads;

    :goto_2
    invoke-virtual {v1}, Lcom/google/r/b/a/ads;->d()Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 124
    invoke-static {}, Lcom/google/o/b/a/h;->newBuilder()Lcom/google/o/b/a/j;

    move-result-object v4

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v5, v4, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/o/b/a/j;->a:I

    iput-wide v6, v4, Lcom/google/o/b/a/j;->b:J

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v1, v4, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v4, Lcom/google/o/b/a/j;->a:I

    iput-wide v6, v4, Lcom/google/o/b/a/j;->c:J

    invoke-virtual {v4}, Lcom/google/o/b/a/j;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/h;

    .line 123
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 124
    :cond_1
    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v1

    goto :goto_2

    .line 123
    :cond_2
    iput-object v1, v3, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    iget v1, v3, Lcom/google/j/d/a/u;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v3, Lcom/google/j/d/a/u;->a:I

    .line 126
    iget v0, v0, Lcom/google/r/b/a/alu;->d:I

    invoke-static {v0}, Lcom/google/maps/g/ta;->a(I)Lcom/google/maps/g/ta;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    :cond_3
    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/c/q;->a(Lcom/google/maps/g/ta;)Lcom/google/android/apps/gmm/iamhere/c/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/q;->a()I

    move-result v0

    .line 125
    iget v1, v3, Lcom/google/j/d/a/u;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v3, Lcom/google/j/d/a/u;->a:I

    iput v0, v3, Lcom/google/j/d/a/u;->c:I

    .line 127
    invoke-virtual {v3}, Lcom/google/j/d/a/u;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/s;

    .line 122
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {p0}, Lcom/google/j/d/a/d;->c()V

    iget-object v1, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 129
    :cond_5
    return-void
.end method

.method public static a(Lcom/google/android/gms/location/reporting/c;Landroid/accounts/Account;)Z
    .locals 12
    .param p0    # Lcom/google/android/gms/location/reporting/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 51
    if-nez p0, :cond_0

    .line 52
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/e/a;->a:Ljava/lang/String;

    .line 75
    :goto_0
    return v0

    .line 55
    :cond_0
    if-nez p1, :cond_1

    .line 56
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/e/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/c;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 60
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/e/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/location/reporting/c;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->c()Z

    move-result v2

    if-nez v2, :cond_3

    .line 65
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/e/a;->a:Ljava/lang/String;

    const-string v2, "place report failed: reporting location history is not allowed reporting state: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 66
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 67
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->a()I

    move-result v4

    .line 68
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->b()I

    move-result v5

    .line 69
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->d()Z

    move-result v6

    .line 70
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->h()Z

    move-result v7

    .line 71
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->e()Z

    move-result v8

    .line 72
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit16 v10, v10, 0xb0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reporting enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " history enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reporting is active: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reporting is ambiguous: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reporting is ambiguous: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reporting is opted in: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 75
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
