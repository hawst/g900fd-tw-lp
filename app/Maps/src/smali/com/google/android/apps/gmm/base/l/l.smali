.class public abstract Lcom/google/android/apps/gmm/base/l/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/j;


# instance fields
.field a:Z

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/gmm/base/l/n;

.field private d:Lcom/google/android/apps/gmm/base/l/k;

.field private e:I

.field private f:Lcom/google/android/libraries/curvular/aw;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/apps/gmm/z/b/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private i:Z

.field private j:F

.field private final k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/n;Lcom/google/android/apps/gmm/base/l/k;ILjava/lang/String;Lcom/google/android/apps/gmm/z/b/l;ZI)V
    .locals 1
    .param p6    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/l;->b:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/l;->c:Lcom/google/android/apps/gmm/base/l/n;

    .line 77
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/l;->d:Lcom/google/android/apps/gmm/base/l/k;

    .line 78
    iput p4, p0, Lcom/google/android/apps/gmm/base/l/l;->e:I

    .line 79
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/l/l;->g:Ljava/lang/String;

    .line 80
    iput-object p6, p0, Lcom/google/android/apps/gmm/base/l/l;->h:Lcom/google/android/apps/gmm/z/b/l;

    .line 81
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/base/l/l;->i:Z

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/l;->a:Z

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/l/l;->j:F

    .line 84
    iput p8, p0, Lcom/google/android/apps/gmm/base/l/l;->k:I

    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/l/l;->l()V

    .line 86
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)F
    .locals 5

    .prologue
    .line 246
    const/16 v0, 0x40

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    .line 247
    neg-int v1, v0

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 249
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v2

    .line 250
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v3, :cond_0

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-eqz v2, :cond_0

    .line 253
    mul-float v0, p2, v1

    .line 261
    :goto_0
    return v0

    .line 258
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/base/f/b;->c()I

    move-result v2

    invoke-static {p0, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v2

    .line 259
    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v4, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v3, v4, v3

    int-to-float v3, v3

    mul-float/2addr v3, p2

    .line 260
    add-int/2addr v0, v2

    neg-int v0, v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    .line 261
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)F
    .locals 2

    .prologue
    .line 209
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_0

    .line 212
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/gmm/base/l/l;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)F

    move-result v0

    .line 217
    :goto_0
    return v0

    .line 216
    :cond_0
    const/16 v0, 0x40

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    .line 217
    neg-int v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->d:Lcom/google/android/apps/gmm/base/l/k;

    iget v0, v0, Lcom/google/android/apps/gmm/base/l/k;->h:I

    if-eqz v0, :cond_0

    .line 179
    iget v0, p0, Lcom/google/android/apps/gmm/base/l/l;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/l;->d:Lcom/google/android/apps/gmm/base/l/k;

    iget v1, v1, Lcom/google/android/apps/gmm/base/l/k;->h:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->f:Lcom/google/android/libraries/curvular/aw;

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/l/l;->e:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->f:Lcom/google/android/libraries/curvular/aw;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/google/android/apps/gmm/base/l/l;->e:I

    if-eq p1, v0, :cond_0

    .line 162
    iput p1, p0, Lcom/google/android/apps/gmm/base/l/l;->e:I

    .line 163
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/l/l;->l()V

    .line 165
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/l/k;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->d:Lcom/google/android/apps/gmm/base/l/k;

    if-eq p1, v0, :cond_0

    .line 144
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/l;->d:Lcom/google/android/apps/gmm/base/l/k;

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/l/l;->l()V

    .line 147
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 6

    .prologue
    const/16 v5, 0x40

    const/4 v2, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 188
    sget-object v0, Lcom/google/android/apps/gmm/base/l/m;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/l;->c:Lcom/google/android/apps/gmm/base/l/n;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/l/n;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/l/l;->j:F

    .line 200
    :goto_0
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 201
    return-void

    .line 190
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->b:Landroid/content/Context;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v3, :cond_2

    invoke-static {v0, p1, p3}, Lcom/google/android/apps/gmm/base/l/l;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)F

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/base/l/l;->j:F

    .line 191
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getTop()I

    move-result v0

    if-gez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v0

    aget v0, v3, v0

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v4, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v3, v4, v3

    sub-int/2addr v0, v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getTop()I

    move-result v3

    add-int/2addr v3, v0

    int-to-float v0, v0

    mul-float/2addr v0, p3

    int-to-float v3, v3

    div-float p3, v0, v3

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    iget v3, p2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_4

    const/high16 v0, 0x3f000000    # 0.5f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_4

    :cond_1
    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/l;->i:Z

    goto :goto_0

    .line 190
    :cond_2
    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    goto :goto_1

    :cond_3
    move v0, v1

    .line 191
    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    .line 194
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->b:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v2, :cond_5

    invoke-static {v0, p1, p3}, Lcom/google/android/apps/gmm/base/l/l;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)F

    move-result v0

    :goto_4
    iput v0, p0, Lcom/google/android/apps/gmm/base/l/l;->j:F

    goto :goto_0

    :cond_5
    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    iget v2, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->i:I

    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_6

    :goto_5
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v2

    aget v2, v3, v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v3

    sub-int/2addr v3, v1

    int-to-float v3, v3

    sub-int/2addr v2, v1

    int-to-float v2, v2

    div-float v2, v3, v2

    int-to-float v1, v1

    mul-float/2addr v1, v2

    int-to-float v0, v0

    div-float/2addr v0, v4

    sub-float v0, v1, v0

    goto :goto_4

    :cond_6
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_5

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/l;->h:Lcom/google/android/apps/gmm/z/b/l;

    .line 157
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/l;->g:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/l/l;->a:Z

    .line 175
    return-void
.end method

.method public b()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/l;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->f:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->d:Lcom/google/android/apps/gmm/base/l/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/k;->g:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/l;->h:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/l;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/apps/gmm/base/l/l;->j:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/apps/gmm/base/l/l;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
