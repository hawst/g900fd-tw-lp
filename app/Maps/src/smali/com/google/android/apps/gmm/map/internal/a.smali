.class public Lcom/google/android/apps/gmm/map/internal/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field private final d:Lcom/google/android/apps/gmm/shared/c/f;

.field private final e:Lcom/google/android/apps/gmm/map/internal/c;

.field private final f:Lcom/google/android/apps/gmm/v/bi;

.field private g:Landroid/animation/ValueAnimator;

.field private h:J

.field private i:J

.field private j:J

.field private k:Lcom/google/android/apps/gmm/map/internal/f;

.field private final l:Lcom/google/android/apps/gmm/map/internal/d;

.field private final m:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final n:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/internal/c;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 6

    .prologue
    .line 160
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/internal/c;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/shared/net/a/b;Z)V

    .line 165
    return-void

    .line 160
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/internal/c;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/shared/net/a/b;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->k:Lcom/google/android/apps/gmm/map/internal/f;

    .line 90
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/a;->a:Z

    .line 173
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 174
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/a;->e:Lcom/google/android/apps/gmm/map/internal/c;

    .line 175
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/a;->f:Lcom/google/android/apps/gmm/v/bi;

    .line 176
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/a;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 177
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/internal/a;->n:Z

    .line 178
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->l:Lcom/google/android/apps/gmm/map/internal/d;

    .line 181
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/a;->b:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/a;->a()V

    .line 182
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 220
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->i:J

    .line 221
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 222
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->c:Z

    if-eqz v0, :cond_1

    .line 223
    const-wide/16 v0, 0x19

    .line 232
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Frame rate set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 233
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    div-long v0, v2, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->h:J

    .line 234
    return-void

    .line 224
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 226
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v0, v0, Lcom/google/r/b/a/aqg;->s:Z

    if-eqz v0, :cond_2

    .line 227
    const-wide/16 v0, 0x3c

    goto :goto_0

    .line 229
    :cond_2
    const-wide/16 v0, 0x1e

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/h;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 203
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->i:J

    .line 204
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/a;->a()V

    .line 209
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->f:Lcom/google/android/apps/gmm/v/bi;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/v/bi;->n:Z

    .line 251
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->a:Z

    if-ne v0, p1, :cond_0

    .line 267
    :goto_0
    return-void

    .line 255
    :cond_0
    if-eqz p1, :cond_1

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->g:Landroid/animation/ValueAnimator;

    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->a:Z

    goto :goto_0

    .line 260
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->g:Landroid/animation/ValueAnimator;

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->g:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->a:Z

    goto :goto_0

    .line 260
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    .line 283
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->j:J

    sub-long v2, v0, v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/a;->h:J

    const-wide/16 v6, 0x3

    sub-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 305
    :goto_0
    return-void

    .line 287
    :cond_0
    monitor-enter p0

    .line 288
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->e:Lcom/google/android/apps/gmm/map/internal/c;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 289
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->e:Lcom/google/android/apps/gmm/map/internal/c;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c;->b()V

    .line 290
    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->j:J

    .line 291
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->k:Lcom/google/android/apps/gmm/map/internal/f;

    if-eqz v2, :cond_1

    .line 292
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->k:Lcom/google/android/apps/gmm/map/internal/f;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/f;->b:Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/f;->a(J)V

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->l:Lcom/google/android/apps/gmm/map/internal/d;

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/a;->l:Lcom/google/android/apps/gmm/map/internal/d;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d;->b:J

    .line 305
    :cond_2
    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 298
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->k:Lcom/google/android/apps/gmm/map/internal/f;

    if-eqz v2, :cond_4

    .line 299
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->k:Lcom/google/android/apps/gmm/map/internal/f;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/f;->a(J)V

    .line 301
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->l:Lcom/google/android/apps/gmm/map/internal/d;

    if-eqz v2, :cond_2

    .line 302
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/a;->l:Lcom/google/android/apps/gmm/map/internal/d;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/internal/d;->b:J

    const-wide/16 v6, 0x14

    cmp-long v3, v4, v6

    if-gez v3, :cond_5

    iget-wide v0, v2, Lcom/google/android/apps/gmm/map/internal/d;->b:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/internal/d;->b:J

    goto :goto_1

    :cond_5
    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/internal/d;->c:J

    sub-long v4, v0, v4

    sget-wide v6, Lcom/google/android/apps/gmm/map/internal/d;->a:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/internal/d;->c:J

    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
