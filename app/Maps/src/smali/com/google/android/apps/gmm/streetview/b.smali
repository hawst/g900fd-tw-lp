.class Lcom/google/android/apps/gmm/streetview/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field a:Landroid/hardware/SensorManager;

.field final synthetic b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

.field private c:Landroid/view/WindowManager;

.field private d:Landroid/hardware/Sensor;

.field private e:Landroid/hardware/Sensor;

.field private f:F

.field private g:F

.field private h:J

.field private i:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;)V
    .locals 2

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/b;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->c:Landroid/view/WindowManager;

    .line 59
    return-void
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 73
    iput-wide v4, p0, Lcom/google/android/apps/gmm/streetview/b;->h:J

    .line 74
    iput-wide v4, p0, Lcom/google/android/apps/gmm/streetview/b;->i:J

    .line 75
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    .line 76
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->a:Landroid/hardware/SensorManager;

    .line 79
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/b;->d:Landroid/hardware/Sensor;

    .line 80
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/b;->e:Landroid/hardware/Sensor;

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/b;->d:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/b;->e:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 83
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 147
    :cond_0
    :goto_1
    return-void

    .line 92
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :cond_2
    const/4 v0, 0x0

    .line 97
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 99
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 100
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    .line 101
    float-to-double v2, v1

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 102
    iget-wide v2, p0, Lcom/google/android/apps/gmm/streetview/b;->h:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_6

    .line 103
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    .line 108
    :goto_2
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/streetview/b;->h:J

    .line 109
    const/4 v0, 0x1

    .line 112
    :cond_3
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    .line 113
    iget-wide v0, p0, Lcom/google/android/apps/gmm/streetview/b;->i:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 115
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 116
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 119
    iget-wide v2, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v4, p0, Lcom/google/android/apps/gmm/streetview/b;->i:J

    .line 120
    sub-long/2addr v2, v4

    long-to-float v2, v2

    const v3, 0x3089705f    # 1.0E-9f

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd    # 0.2f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    float-to-double v2, v2

    .line 121
    iget v4, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    float-to-double v4, v4

    float-to-double v6, v0

    mul-double/2addr v6, v2

    sub-double/2addr v4, v6

    double-to-float v0, v4

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    .line 122
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    float-to-double v4, v0

    float-to-double v0, v1

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    .line 124
    :cond_4
    iget-wide v0, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/streetview/b;->i:J

    .line 125
    const/4 v0, 0x1

    .line 128
    :cond_5
    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/b;->c:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 130
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    .line 131
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    .line 132
    packed-switch v2, :pswitch_data_0

    .line 145
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/b;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v1, v4

    neg-float v1, v1

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v0, v4

    neg-float v0, v0

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a(Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;FF)V

    goto/16 :goto_1

    .line 105
    :cond_6
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    float-to-double v2, v1

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    mul-double/2addr v0, v4

    iget-wide v4, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/streetview/b;->h:J

    .line 106
    sub-long/2addr v4, v6

    long-to-float v4, v4

    const v5, 0x3089705f    # 1.0E-9f

    mul-float/2addr v4, v5

    const v5, 0x3e4ccccd    # 0.2f

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    goto/16 :goto_2

    .line 134
    :pswitch_0
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    .line 135
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    neg-float v0, v0

    .line 136
    goto :goto_3

    .line 138
    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    neg-float v1, v0

    .line 139
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    neg-float v0, v0

    .line 140
    goto :goto_3

    .line 142
    :pswitch_2
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->g:F

    neg-float v1, v0

    .line 143
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/b;->f:F

    goto :goto_3

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
