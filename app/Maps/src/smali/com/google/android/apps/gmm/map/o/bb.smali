.class public Lcom/google/android/apps/gmm/map/o/bb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/ay;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z


# direct methods
.method protected constructor <init>(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bb;->a:Ljava/util/List;

    .line 31
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/o/bb;->b:Z

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/o/as;)Z
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/o/as;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/o/bb;->b(Lcom/google/android/apps/gmm/map/o/as;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/o/as;)Z
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->e()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v6

    move v3, v4

    .line 45
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bb;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/o/as;

    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/bb;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/as;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49
    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->e()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 54
    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 56
    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v4, v5

    .line 72
    :cond_1
    :goto_1
    return v4

    .line 61
    :cond_2
    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 62
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 63
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_6

    .line 64
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_6

    .line 66
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 67
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/as;->b:Lcom/google/android/apps/gmm/map/internal/c/m;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 65
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-nez v2, :cond_3

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v7

    int-to-double v8, v7

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v0

    int-to-double v10, v0

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v0, v2

    :goto_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v7, v1, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v7

    int-to-double v8, v7

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v1

    int-to-double v10, v1

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v1, v2

    :goto_3
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)D

    move-result-wide v0

    const-wide v8, 0x3fc3333333333333L    # 0.15

    cmpg-double v0, v0, v8

    if-gez v0, :cond_5

    move v0, v5

    :goto_4
    if-eqz v0, :cond_6

    move v4, v5

    .line 68
    goto :goto_1

    .line 65
    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_2

    :cond_4
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bb;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_3

    :cond_5
    move v0, v4

    goto :goto_4

    .line 45
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0
.end method
