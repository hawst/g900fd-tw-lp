.class public Lcom/google/android/apps/gmm/z/o;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/view/View;

.field private c:Lcom/google/android/apps/gmm/z/b/o;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/z/b/l;",
            "Lcom/google/android/apps/gmm/z/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/o;->d:Ljava/util/Map;

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/o;->b:Landroid/view/View;

    .line 58
    return-void
.end method

.method private a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .param p3    # Lcom/google/android/apps/gmm/z/b/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 110
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 111
    invoke-virtual {p2, v2}, Lcom/google/android/apps/gmm/z/b/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    :goto_0
    return-object p3

    .line 115
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 116
    check-cast p1, Landroid/view/ViewGroup;

    .line 117
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 118
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v2, :cond_1

    move-object v1, v2

    :goto_2
    invoke-direct {p0, v3, p2, v1}, Lcom/google/android/apps/gmm/z/o;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 120
    if-eqz v1, :cond_2

    move-object p3, v1

    .line 121
    goto :goto_0

    :cond_1
    move-object v1, p3

    .line 118
    goto :goto_2

    .line 117
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 125
    :cond_3
    const/4 p3, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/google/b/f/co;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p2    # Lcom/google/b/f/co;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/b/f/co;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/l;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/b/f/co;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    invoke-static {p1}, Lcom/google/android/apps/gmm/util/r;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    :cond_0
    return-void

    .line 199
    :cond_1
    const/4 v0, 0x0

    .line 200
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 201
    if-eqz v1, :cond_3

    .line 202
    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/t;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/b/f/co;

    move-result-object v0

    .line 205
    if-eqz p2, :cond_2

    .line 206
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    .line 207
    invoke-virtual {p2}, Lcom/google/b/f/co;->c()V

    iget-object v3, p2, Lcom/google/b/f/co;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_2
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_3
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 216
    check-cast p1, Landroid/view/ViewGroup;

    .line 217
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 218
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v0, :cond_4

    move-object v2, v0

    :goto_1
    invoke-direct {p0, v3, v2, p3, p4}, Lcom/google/android/apps/gmm/z/o;->a(Landroid/view/View;Lcom/google/b/f/co;Ljava/util/List;Ljava/util/List;)V

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    move-object v2, p2

    .line 218
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/o;->b:Landroid/view/View;

    if-nez v1, :cond_0

    .line 105
    :goto_0
    return-object v0

    .line 101
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    sget-object v1, Lcom/google/android/apps/gmm/z/o;->a:Ljava/lang/String;

    const-string v2, "getVeParent should be called on UI threads."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/o;->b:Landroid/view/View;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/gmm/z/o;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a()Lcom/google/android/apps/gmm/z/b/o;
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->c:Lcom/google/android/apps/gmm/z/b/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/String;ILcom/google/b/f/cm;)V
    .locals 2

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->d:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/gmm/z/p;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/android/apps/gmm/z/p;-><init>(Ljava/lang/String;ILcom/google/b/f/cm;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    monitor-exit p0

    return-void

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/z/b/o;)V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->c:Lcom/google/android/apps/gmm/z/b/o;

    if-eq v0, p1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 67
    :goto_1
    monitor-exit p0

    return-void

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/o;->c:Lcom/google/android/apps/gmm/z/b/o;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/l;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/b/f/cm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 183
    :cond_0
    return-void

    .line 177
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/o;->b:Landroid/view/View;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p1, v0}, Lcom/google/android/apps/gmm/z/o;->a(Landroid/view/View;Lcom/google/b/f/co;Ljava/util/List;Ljava/util/List;)V

    .line 180
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/co;

    .line 181
    invoke-virtual {v0}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 159
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/o;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/p;

    invoke-static {}, Lcom/google/b/f/b;->newBuilder()Lcom/google/b/f/d;

    move-result-object v3

    iget v4, v0, Lcom/google/android/apps/gmm/z/p;->b:I

    iget v5, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v3, Lcom/google/b/f/d;->a:I

    iput v4, v3, Lcom/google/b/f/d;->b:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/p;->c:Lcom/google/b/f/cm;

    iget v4, v4, Lcom/google/b/f/cm;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v1, :cond_2

    :goto_1
    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/z/p;->c:Lcom/google/b/f/cm;

    iget v1, v1, Lcom/google/b/f/cm;->b:I

    iget v2, v3, Lcom/google/b/f/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v3, Lcom/google/b/f/d;->a:I

    iput v1, v3, Lcom/google/b/f/d;->c:I

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/google/b/f/cq;

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/p;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/b/f/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/t;->a(Lcom/google/b/f/b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
