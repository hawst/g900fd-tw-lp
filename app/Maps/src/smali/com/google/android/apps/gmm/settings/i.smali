.class public Lcom/google/android/apps/gmm/settings/i;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/settings/a/a;


# instance fields
.field private a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 21
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/settings/SettingsFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/settings/SettingsFragment;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a(Landroid/app/Activity;Landroid/app/Fragment;)V

    .line 26
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a(Landroid/app/Activity;Landroid/app/Fragment;)V

    .line 31
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/gms/location/a/b;->a(Landroid/content/Context;)V

    .line 41
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/mylocation/k;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/mylocation/k;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/util/b;->a(ZLcom/google/android/apps/gmm/util/g;Lcom/google/android/apps/gmm/util/f;)Z

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 46
    invoke-static {}, Lcom/google/android/apps/gmm/settings/b;->b()Lcom/google/android/apps/gmm/settings/b;

    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a(Landroid/app/Activity;Landroid/app/Fragment;)V

    .line 47
    return-void
.end method
