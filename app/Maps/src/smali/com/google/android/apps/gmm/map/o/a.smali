.class Lcom/google/android/apps/gmm/map/o/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/o/b;

.field final b:Lcom/google/android/apps/gmm/map/o/b/a;

.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/map/o/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/b/a;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->d:Ljava/util/ArrayList;

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/a;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/map/o/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/o/b;-><init>(Lcom/google/android/apps/gmm/map/o/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->a:Lcom/google/android/apps/gmm/map/o/b;

    .line 78
    return-void
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/map/o/b;)V
    .locals 1

    .prologue
    .line 237
    :goto_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/o/b;->e:Z

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    .line 240
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;)V

    .line 241
    iget-object p1, p1, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    goto :goto_0
.end method

.method a(Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/b;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b;",
            ">;",
            "Lcom/google/android/apps/gmm/map/o/b/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 170
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 171
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_a

    .line 172
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/b;

    .line 173
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x5

    if-le v1, v2, :cond_9

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/o/b;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    sub-float v1, v2, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v5, v2, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    sub-float v2, v5, v2

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v2, v5

    const/high16 v5, 0x43af0000    # 350.0f

    cmpl-float v1, v1, v5

    if-ltz v1, :cond_3

    const/high16 v1, 0x43160000    # 150.0f

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_4

    :cond_0
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_9

    .line 174
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/o/b/b;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/c;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/apps/gmm/map/o/c;->b:I

    .line 178
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/o/b/b;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 179
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v1, v0, Lcom/google/android/apps/gmm/map/o/c;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/c;->b:I

    .line 171
    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 173
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v5, v1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v6, v1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v7, v1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v8, v1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    sub-float v1, v2, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v9, v1, v2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    sub-float v1, v2, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v10, v1, v2

    const/high16 v1, 0x43af0000    # 350.0f

    div-float v1, v9, v1

    const/high16 v2, 0x43160000    # 150.0f

    div-float v2, v10, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_7

    new-instance v2, Lcom/google/android/apps/gmm/map/o/b/a;

    add-float v1, v5, v9

    invoke-direct {v2, v5, v6, v1, v8}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    new-instance v1, Lcom/google/android/apps/gmm/map/o/b/a;

    add-float/2addr v5, v9

    invoke-direct {v1, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    :goto_4
    new-instance v5, Lcom/google/android/apps/gmm/map/o/b;

    invoke-direct {v5, v2}, Lcom/google/android/apps/gmm/map/o/b;-><init>(Lcom/google/android/apps/gmm/map/o/b/a;)V

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    new-instance v2, Lcom/google/android/apps/gmm/map/o/b;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/map/o/b;-><init>(Lcom/google/android/apps/gmm/map/o/b/a;)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_8

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/b/b;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/o/b/b;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/o/b/b;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :cond_7
    new-instance v2, Lcom/google/android/apps/gmm/map/o/b/a;

    add-float v1, v6, v10

    invoke-direct {v2, v5, v6, v7, v1}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    new-instance v1, Lcom/google/android/apps/gmm/map/o/b/a;

    add-float/2addr v6, v10

    invoke-direct {v1, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    goto :goto_4

    :cond_8
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/o/b;->e:Z

    const/4 v1, 0x1

    goto/16 :goto_2

    .line 183
    :cond_9
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v1, v0, Lcom/google/android/apps/gmm/map/o/c;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/c;->b:I

    goto/16 :goto_3

    .line 187
    :cond_a
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/b/b;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/o/b/b;->a()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v1, v0, Lcom/google/android/apps/gmm/map/o/c;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/c;->c:I

    .line 128
    :goto_0
    return v2

    .line 117
    :cond_0
    if-nez p2, :cond_1

    move v0, v1

    .line 118
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a;->a:Lcom/google/android/apps/gmm/map/o/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {p0, v3, p1, v0, v4}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;Lcom/google/android/apps/gmm/map/o/b/b;ZLjava/util/List;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v1, v0, Lcom/google/android/apps/gmm/map/o/c;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/map/o/c;->c:I

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 117
    goto :goto_1

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/gmm/map/o/a;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/b;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v2, v0, Lcom/google/android/apps/gmm/map/o/c;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/apps/gmm/map/o/c;->a:I

    move v2, v1

    .line 128
    goto :goto_0
.end method

.method a(Lcom/google/android/apps/gmm/map/o/b;Lcom/google/android/apps/gmm/map/o/b/b;ZLjava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/o/b;",
            "Lcom/google/android/apps/gmm/map/o/b/b;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v3, v0, Lcom/google/android/apps/gmm/map/o/c;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/android/apps/gmm/map/o/c;->d:I

    .line 207
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/map/o/b/b;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 219
    :goto_0
    return v0

    .line 211
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/o/b;->e:Z

    if-eqz v0, :cond_5

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a;->e:Lcom/google/android/apps/gmm/map/o/c;

    iget v3, v0, Lcom/google/android/apps/gmm/map/o/c;->e:I

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v0, Lcom/google/android/apps/gmm/map/o/c;->e:I

    .line 213
    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    if-eqz p3, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/b/b;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/map/o/b/b;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    move v0, v1

    .line 216
    :goto_3
    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    .line 215
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    .line 216
    goto :goto_0

    .line 218
    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->c:Lcom/google/android/apps/gmm/map/o/b;

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;Lcom/google/android/apps/gmm/map/o/b/b;ZLjava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/b;->d:Lcom/google/android/apps/gmm/map/o/b;

    .line 219
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/map/o/a;->a(Lcom/google/android/apps/gmm/map/o/b;Lcom/google/android/apps/gmm/map/o/b/b;ZLjava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0
.end method
