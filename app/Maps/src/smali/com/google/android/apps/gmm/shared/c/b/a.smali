.class public final Lcom/google/android/apps/gmm/shared/c/b/a;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/e/a/a/a/b;ID)D
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 158
    if-eqz p0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 159
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 163
    :cond_2
    :goto_1
    return-wide p2

    :cond_3
    move v2, v0

    .line 158
    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    goto :goto_1

    .line 161
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/google/e/a/a/a/b;IF)F
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 137
    if-eqz p0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 138
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 142
    :cond_2
    :goto_1
    return p2

    :cond_3
    move v2, v0

    .line 137
    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    goto :goto_1

    .line 140
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/google/e/a/a/a/b;II)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 116
    if-eqz p0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 117
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    long-to-int p2, v0

    .line 121
    :cond_2
    :goto_1
    return p2

    :cond_3
    move v2, v0

    .line 116
    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    goto :goto_1

    .line 119
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/google/e/a/a/a/b;IJ)J
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 179
    if-eqz p0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 183
    :cond_2
    :goto_1
    return-wide p2

    :cond_3
    move v2, v0

    .line 179
    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    goto :goto_1

    .line 181
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/google/e/a/a/a/b;ILcom/google/n/at;)Lcom/google/b/c/cv;
    .locals 4
    .param p0    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/e/a/a/a/b;",
            "ITT;)",
            "Lcom/google/b/c/cv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 754
    if-nez p0, :cond_0

    .line 755
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 761
    :goto_0
    return-object v0

    .line 757
    :cond_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 758
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    .line 759
    if-eqz p0, :cond_1

    const/16 v0, 0x19

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, p2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_2

    :goto_3
    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 758
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 759
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-object v0, p2

    goto :goto_3

    .line 761
    :cond_3
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a(Lcom/google/e/a/a/a/b;[I)Lcom/google/e/a/a/a/b;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 505
    .line 506
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    move-object v0, p0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p1, v1

    .line 507
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object p0

    .line 508
    if-nez p0, :cond_1

    .line 509
    const/4 v0, 0x0

    .line 512
    :cond_0
    return-object v0

    .line 506
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/d;Ljava/io/DataInput;)Lcom/google/e/a/a/a/b;
    .locals 5

    .prologue
    .line 364
    new-instance v0, Lcom/google/e/a/a/a/b;

    invoke-direct {v0, p0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 365
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    .line 366
    const v2, 0x7fffffff

    const/4 v3, 0x1

    new-instance v4, Lcom/google/e/a/a/a/c;

    invoke-direct {v4}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    .line 368
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 369
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 374
    return-object v0
.end method

.method public static a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;
    .locals 6
    .param p0    # Lcom/google/n/at;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(TT;",
            "Lcom/google/e/a/a/a/d;",
            ")",
            "Lcom/google/e/a/a/a/b;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 770
    if-nez p0, :cond_0

    .line 778
    :goto_0
    return-object v0

    .line 773
    :cond_0
    new-instance v1, Lcom/google/e/a/a/a/b;

    invoke-direct {v1, p1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 775
    :try_start_0
    invoke-interface {p0}, Lcom/google/n/at;->l()[B

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v2, v2

    const/4 v4, 0x1

    new-instance v5, Lcom/google/e/a/a/a/c;

    invoke-direct {v5}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v1, v3, v2, v4, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 776
    goto :goto_0

    .line 778
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;
    .locals 2
    .param p0    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/e/a/a/a/b;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 673
    if-eqz p0, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 675
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lcom/google/n/ax;Ljava/io/DataInput;)Lcom/google/n/at;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/n/ax",
            "<TT;>;",
            "Ljava/io/DataInput;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 387
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    .line 389
    :try_start_0
    invoke-interface {p0, v1}, Lcom/google/n/ax;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public static a([BLcom/google/n/at;)Lcom/google/n/at;
    .locals 1
    .param p0    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">([BTT;)TT;"
        }
    .end annotation

    .prologue
    .line 621
    :try_start_0
    invoke-interface {p1}, Lcom/google/n/at;->a()Lcom/google/n/ax;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 624
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([BLcom/google/n/ax;)Lcom/google/n/at;
    .locals 2
    .param p0    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">([B",
            "Lcom/google/n/ax",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 644
    if-nez p0, :cond_0

    move-object v0, v1

    .line 650
    :goto_0
    return-object v0

    .line 648
    :cond_0
    :try_start_0
    invoke-interface {p1, p0}, Lcom/google/n/ax;->b([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 650
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/io/DataInput;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 334
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    .line 335
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/b/b;

    check-cast p0, Ljava/io/InputStream;

    .line 336
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-long v2, v2

    invoke-static {p0, v2, v3}, Lcom/google/b/e/h;->a(Ljava/io/InputStream;J)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/b;-><init>(Ljava/io/InputStream;)V

    .line 348
    if-gez v0, :cond_0

    .line 349
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 351
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49
    if-eqz p0, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_3

    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 51
    :goto_1
    return-object v0

    :cond_2
    move v2, v0

    .line 49
    goto :goto_0

    :cond_3
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 51
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_1
.end method

.method public static a(Ljava/io/DataOutput;Lcom/google/e/a/a/a/b;)V
    .locals 2

    .prologue
    .line 455
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 456
    invoke-virtual {p1, v0}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    .line 457
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 458
    array-length v1, v0

    invoke-interface {p0, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 459
    invoke-interface {p0, v0}, Ljava/io/DataOutput;->write([B)V

    .line 460
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;IZ)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 283
    if-eqz p0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 287
    :cond_2
    :goto_1
    return p2

    :cond_3
    move v2, v0

    .line 283
    goto :goto_0

    .line 287
    :catch_0
    move-exception v0

    goto :goto_1

    .line 285
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 58
    if-eqz p0, :cond_3

    :try_start_0
    iget-object v3, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_2

    move v3, v2

    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    if-eqz v0, :cond_3

    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_1
    return-object v0

    :cond_2
    move v3, v0

    .line 58
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    .line 60
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public static c(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 86
    if-eqz p0, :cond_3

    iget-object v2, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_3

    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    :goto_1
    return-object v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 91
    if-nez p0, :cond_0

    .line 92
    new-array v0, v0, [Lcom/google/e/a/a/a/b;

    .line 100
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    new-array v2, v1, [Lcom/google/e/a/a/a/b;

    move v1, v0

    .line 96
    :goto_1
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 97
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    aput-object v0, v2, v1

    .line 96
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 100
    goto :goto_0
.end method

.method public static e(Lcom/google/e/a/a/a/b;I)J
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const-wide/16 v2, -0x1

    .line 249
    if-eqz p0, :cond_3

    :try_start_0
    iget-object v4, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_2

    move v4, v1

    :goto_0
    if-nez v4, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_3

    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 253
    :goto_1
    return-wide v0

    :cond_2
    move v4, v0

    .line 249
    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1

    .line 251
    :catch_0
    move-exception v0

    move-wide v0, v2

    goto :goto_1

    .line 253
    :catch_1
    move-exception v0

    move-wide v0, v2

    goto :goto_1
.end method

.method public static f(Lcom/google/e/a/a/a/b;I)[B
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 302
    if-eqz p0, :cond_3

    :try_start_0
    iget-object v3, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_2

    move v3, v2

    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    if-eqz v0, :cond_3

    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    .line 306
    :goto_1
    return-object v0

    :cond_2
    move v3, v0

    .line 302
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    .line 304
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    .line 306
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public static g(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;
    .locals 2

    .prologue
    .line 427
    new-instance v1, Lcom/google/e/a/a/a/b;

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    iget-object v0, v0, Lcom/google/e/a/a/a/d;->a:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/e;

    if-nez v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 428
    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 427
    :cond_0
    iget-object v0, v0, Lcom/google/e/a/a/a/e;->b:Ljava/lang/Object;

    goto :goto_0

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 429
    return-object v1
.end method

.method public static h(Lcom/google/e/a/a/a/b;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 611
    :goto_0
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v2

    :goto_1
    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 612
    invoke-virtual {p0, p1, v1}, Lcom/google/e/a/a/a/b;->a(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 611
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 614
    :cond_3
    return-void
.end method
