.class public Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;
.super Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;
.source "PG"


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public e:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    iput-object v3, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    .line 73
    iput-object v3, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->as:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 89
    sget v1, Lcom/google/android/apps/gmm/n;->at:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    if-eq v2, v1, :cond_0

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    iput-object v3, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->invalidate()V

    .line 90
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 91
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 148
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v0

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 155
    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_0
    return v0
.end method

.method protected final a(IIII)I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 184
    .line 185
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 184
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v2, p4

    :goto_0
    if-ltz v4, :cond_1

    .line 188
    if-le v2, p3, :cond_1

    .line 189
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 193
    instance-of v0, v1, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v0, :cond_2

    .line 194
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    move-object v0, v1

    .line 199
    check-cast v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->c:Z

    if-ne v7, v3, :cond_0

    move v0, v3

    .line 200
    :goto_1
    if-eqz v0, :cond_2

    .line 201
    invoke-virtual {p0, v1, v5, p2}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->measureChild(Landroid/view/View;II)V

    .line 204
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 206
    sub-int v0, v6, v0

    sub-int v0, v2, v0

    .line 187
    :goto_2
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    move v2, v0

    goto :goto_0

    .line 199
    :cond_0
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->c:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    const/4 v0, 0x1

    goto :goto_1

    .line 209
    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method protected final b()I
    .locals 2

    .prologue
    .line 214
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b()I

    move-result v0

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 218
    :cond_0
    return v0
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 223
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_5

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int/2addr v6, v5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getPaddingTop()I

    move-result v7

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v7

    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v7

    sub-int/2addr v0, v7

    sub-int/2addr v0, v4

    :goto_2
    add-int/2addr v4, v0

    add-int/2addr v5, v6

    invoke-virtual {v2, v0, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    move-object v2, v0

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    move-object v2, v0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v7

    add-int/2addr v0, v7

    goto :goto_2

    .line 226
    :cond_5
    return-void
.end method

.method protected final setupMeasuring()V
    .locals 4

    .prologue
    .line 163
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->setupMeasuring()V

    .line 166
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 167
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 168
    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_0

    .line 169
    check-cast v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    .line 170
    const/4 v2, 0x1

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->c:Z

    if-ne v3, v2, :cond_1

    .line 166
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_1
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->c:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    goto :goto_1

    .line 173
    :cond_2
    return-void
.end method
