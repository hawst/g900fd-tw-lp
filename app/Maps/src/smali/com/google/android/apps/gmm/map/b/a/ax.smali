.class public final Lcom/google/android/apps/gmm/map/b/a/ax;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[I

.field public final b:[I


# direct methods
.method public constructor <init>([I[I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    .line 42
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/cu;I[IILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-static {p0, p1, p4}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)[I

    move-result-object v3

    .line 151
    if-ltz p3, :cond_2

    .line 152
    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    new-array v0, v1, [I

    .line 154
    :cond_0
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ax;

    invoke-direct {v1, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/ax;-><init>([I[I)V

    return-object v1

    .line 152
    :cond_1
    add-int/lit8 v0, p1, -0x2

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [I

    move v2, v1

    :goto_1
    add-int/lit8 v4, p1, -0x2

    if-ge v1, v4, :cond_0

    add-int/lit8 v4, v2, 0x1

    aput p3, v0, v2

    add-int/lit8 v5, v4, 0x1

    add-int v2, p3, v1

    add-int/lit8 v2, v2, 0x2

    rem-int/2addr v2, p1

    aput v2, v0, v4

    add-int/lit8 v2, v5, 0x1

    add-int v4, p3, v1

    add-int/lit8 v4, v4, 0x1

    rem-int/2addr v4, p1

    aput v4, v0, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v0, p2

    goto :goto_0
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;
    .locals 4

    .prologue
    .line 84
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 85
    rem-int/lit8 v0, v1, 0x3

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Malformed TriangleList, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vertices"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    .line 90
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 91
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;[II)V

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ax;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/ax;-><init>([I[I)V

    return-object v0
.end method

.method private a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    .line 207
    mul-int/lit8 v0, p1, 0x3

    .line 208
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v1
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/aa;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 366
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 367
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    :goto_1
    if-ge v0, v2, :cond_1

    .line 368
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/android/apps/gmm/map/b/a/y;

    .line 369
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v4, v2, v1

    .line 370
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v4, v2, v7

    .line 371
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v4, v2, v8

    .line 372
    aget-object v4, v2, v1

    aget-object v5, v2, v7

    aget-object v6, v2, v8

    invoke-virtual {p0, v0, v4, v5, v6}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 375
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/aa;

    invoke-direct {v4, v2}, Lcom/google/android/apps/gmm/map/b/a/aa;-><init>([Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 367
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_1

    .line 377
    :cond_1
    return-object v3
.end method

.method public final a([I[I)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I[I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, -0x1

    .line 280
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    move-object v0, v10

    .line 347
    :goto_0
    return-object v0

    .line 286
    :cond_0
    if-eqz p2, :cond_1

    if-nez p1, :cond_2

    :cond_1
    move-object v0, v10

    .line 287
    goto :goto_0

    .line 290
    :cond_2
    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    .line 291
    invoke-direct {p0, v5}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    .line 293
    array-length v0, p2

    if-gtz v0, :cond_4

    move v0, v1

    .line 295
    :goto_1
    array-length v2, p1

    if-gtz v2, :cond_5

    move v2, v1

    .line 297
    :goto_2
    if-nez v2, :cond_7

    .line 299
    array-length v2, p1

    if-lt v4, v2, :cond_6

    move v2, v1

    :goto_3
    move v3, v4

    :goto_4
    move v9, v4

    move v12, v0

    move-object v0, v6

    move v6, v5

    move v5, v12

    .line 304
    :goto_5
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v7, v7

    div-int/lit8 v7, v7, 0x3

    if-ge v9, v7, :cond_f

    .line 305
    invoke-direct {p0, v9}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    .line 306
    if-nez v0, :cond_12

    move-object v7, v11

    .line 309
    :goto_6
    add-int/lit8 v0, v9, 0x1

    if-ne v5, v0, :cond_b

    .line 310
    add-int/lit8 v5, v6, 0x1

    .line 311
    array-length v0, p2

    if-lt v5, v0, :cond_8

    move v0, v1

    .line 313
    :goto_7
    invoke-virtual {v8, v11}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 315
    if-ne v2, v9, :cond_a

    .line 316
    add-int/lit8 v3, v3, 0x1

    .line 317
    array-length v2, p1

    if-lt v3, v2, :cond_9

    move v2, v1

    .line 322
    :goto_8
    iget v6, v8, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-le v6, v4, :cond_3

    .line 323
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v6

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_3
    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    .line 326
    const/4 v6, 0x0

    .line 304
    :goto_9
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move-object v8, v7

    move v12, v5

    move v5, v0

    move-object v0, v6

    move v6, v12

    goto :goto_5

    .line 293
    :cond_4
    aget v0, p2, v5

    goto :goto_1

    .line 295
    :cond_5
    aget v2, p1, v5

    goto :goto_2

    .line 299
    :cond_6
    aget v2, p1, v4

    goto :goto_3

    .line 302
    :cond_7
    invoke-virtual {v8, v6}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move v3, v5

    goto :goto_4

    .line 311
    :cond_8
    aget v0, p2, v5

    goto :goto_7

    .line 317
    :cond_9
    aget v2, p1, v3

    goto :goto_8

    .line 320
    :cond_a
    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    goto :goto_8

    .line 327
    :cond_b
    if-ne v2, v9, :cond_e

    .line 328
    invoke-virtual {v8, v11}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 330
    add-int/lit8 v3, v3, 0x1

    .line 331
    array-length v0, p1

    if-lt v3, v0, :cond_d

    move v0, v1

    .line 333
    :goto_a
    iget v2, v8, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-le v2, v4, :cond_c

    .line 334
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    :cond_c
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    move v12, v0

    move v0, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v2

    move v2, v12

    goto :goto_9

    .line 331
    :cond_d
    aget v0, p1, v3

    goto :goto_a

    .line 338
    :cond_e
    invoke-virtual {v8, v11}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move v0, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    goto :goto_9

    .line 341
    :cond_f
    if-ne v2, v1, :cond_10

    if-eqz v0, :cond_10

    .line 342
    invoke-virtual {v8, v0}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 344
    :cond_10
    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-le v0, v4, :cond_11

    .line 345
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    move-object v0, v10

    .line 347
    goto/16 :goto_0

    :cond_12
    move-object v7, v0

    goto/16 :goto_6
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 5

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    mul-int/lit8 v1, p1, 0x3

    aget v0, v0, v1

    mul-int/lit8 v2, v0, 0x3

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    mul-int/lit8 v1, p1, 0x3

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    mul-int/lit8 v1, v0, 0x3

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    mul-int/lit8 v3, p1, 0x3

    add-int/lit8 v3, v3, 0x2

    aget v0, v0, v3

    mul-int/lit8 v0, v0, 0x3

    .line 237
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v4, v2, 0x1

    aget v2, v3, v2

    iput v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 238
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v3, v4, 0x1

    aget v2, v2, v4

    iput v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 239
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v2, v2, v3

    iput v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 240
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v3, v1, 0x1

    aget v1, v2, v1

    iput v1, p3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 241
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v1, v1, v3

    iput v1, p3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v1, v1, v2

    iput v1, p3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v0, v1, v0

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v0, v0, v1

    iput v0, p4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 246
    return-void

    .line 233
    :cond_0
    mul-int/lit8 v2, p1, 0x9

    .line 234
    add-int/lit8 v1, v2, 0x3

    .line 235
    add-int/lit8 v0, v1, 0x3

    goto :goto_0
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 5

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    mul-int/lit8 v1, p1, 0x3

    aget v0, v0, v1

    mul-int/lit8 v2, v0, 0x3

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    mul-int/lit8 v1, p1, 0x3

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    mul-int/lit8 v1, v0, 0x3

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    mul-int/lit8 v3, p1, 0x3

    add-int/lit8 v3, v3, 0x2

    aget v0, v0, v3

    mul-int/lit8 v0, v0, 0x3

    .line 268
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v4, v2, 0x1

    aget v2, v3, v2

    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    iput v2, p3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 269
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v3, v4, 0x1

    aget v2, v2, v4

    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v4

    iput v2, p3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 270
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v2, v2, v3

    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v2, v3

    iput v2, p3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 271
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v3, v1, 0x1

    aget v1, v2, v1

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, v2

    iput v1, p4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 272
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v1, v1, v3

    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v3

    iput v1, p4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v1, v1, v2

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v1, v2

    iput v1, p4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 274
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v0, v1, v0

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v1

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    add-int/lit8 v1, v2, 0x1

    aget v0, v0, v2

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    aget v0, v0, v1

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v0, v1

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 277
    return-void

    .line 264
    :cond_0
    mul-int/lit8 v2, p1, 0x9

    .line 265
    add-int/lit8 v1, v2, 0x3

    .line 266
    add-int/lit8 v0, v1, 0x3

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/e;)V
    .locals 2

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/ax;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/aa;

    .line 357
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/e;->a(Lcom/google/android/apps/gmm/map/b/a/d;)V

    goto :goto_0

    .line 359
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 382
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/b/a/ax;

    if-eqz v1, :cond_0

    .line 383
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 384
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    .line 385
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 387
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
