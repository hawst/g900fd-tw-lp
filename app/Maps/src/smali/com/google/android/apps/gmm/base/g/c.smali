.class public Lcom/google/android/apps/gmm/base/g/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/g/b;
.implements Ljava/io/Serializable;


# static fields
.field private static final w:Ljava/lang/String;

.field private static final x:Lcom/google/b/f/cq;


# instance fields
.field private final A:Ljava/lang/String;

.field private final B:Z

.field private final C:Z

.field private final D:Lcom/google/android/apps/gmm/z/b/l;

.field private transient E:Ljava/lang/String;

.field private transient F:Lcom/google/maps/g/lf;

.field private transient G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private transient H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/pc;",
            ">;"
        }
    .end annotation
.end field

.field private transient I:Z

.field private transient J:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field private transient K:Lcom/google/maps/g/hg;

.field private transient L:Lcom/google/android/apps/gmm/map/b/a/j;

.field private transient M:Lcom/google/android/apps/gmm/map/b/a/q;

.field private transient N:Lcom/google/maps/a/a;

.field private transient O:Lcom/google/android/apps/gmm/y/f;

.field private transient P:Lcom/google/android/apps/gmm/hotels/a/c;

.field private Q:Z

.field private transient R:Lcom/google/android/apps/gmm/place/riddler/c/e;

.field private transient S:Lcom/google/android/apps/gmm/z/b/l;

.field public a:Lcom/google/r/b/a/ads;

.field public final b:Lcom/google/android/apps/gmm/base/g/a;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:Z

.field public final f:Z

.field public final g:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Z

.field public final i:Z

.field public final j:Lcom/google/android/apps/gmm/base/g/e;

.field public transient k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public transient l:Ljava/lang/String;

.field public transient m:Lcom/google/maps/g/hg;

.field public transient n:Ljava/lang/String;

.field public transient o:Ljava/lang/Boolean;

.field public transient p:[B

.field public transient q:Lcom/google/r/b/a/ade;

.field public transient r:Lcom/google/r/b/a/ade;

.field public transient s:Lcom/google/r/b/a/ade;

.field public transient t:[I

.field public transient u:Ljava/lang/String;

.field public transient v:Ljava/lang/String;

.field private final y:Ljava/lang/String;

.field private final z:Lcom/google/b/f/cq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const-class v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/g/c;->w:Ljava/lang/String;

    .line 120
    sget-object v0, Lcom/google/b/f/t;->gc:Lcom/google/b/f/t;

    sput-object v0, Lcom/google/android/apps/gmm/base/g/c;->x:Lcom/google/b/f/cq;

    return-void
.end method

.method constructor <init>(Lcom/google/r/b/a/ads;Lcom/google/android/apps/gmm/base/g/a;ZZZZLcom/google/b/c/cv;ZZLjava/lang/String;Ljava/lang/String;Lcom/google/b/f/cq;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/apps/gmm/base/g/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/r/b/a/ads;",
            "Lcom/google/android/apps/gmm/base/g/a;",
            "ZZZZ",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/b/f/cq;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/z/b/l;",
            "Lcom/google/android/apps/gmm/base/g/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->F:Lcom/google/maps/g/lf;

    .line 258
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    .line 259
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    .line 260
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    .line 261
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/base/g/c;->B:Z

    .line 262
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/base/g/c;->f:Z

    .line 263
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/base/g/c;->C:Z

    .line 264
    iput-object p7, p0, Lcom/google/android/apps/gmm/base/g/c;->g:Lcom/google/b/c/cv;

    .line 265
    iput-boolean p8, p0, Lcom/google/android/apps/gmm/base/g/c;->h:Z

    .line 266
    iput-boolean p9, p0, Lcom/google/android/apps/gmm/base/g/c;->i:Z

    .line 267
    iput-object p10, p0, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    .line 268
    iput-object p11, p0, Lcom/google/android/apps/gmm/base/g/c;->y:Ljava/lang/String;

    .line 269
    if-eqz p12, :cond_1

    :goto_0
    iput-object p12, p0, Lcom/google/android/apps/gmm/base/g/c;->z:Lcom/google/b/f/cq;

    .line 270
    iput-object p13, p0, Lcom/google/android/apps/gmm/base/g/c;->A:Ljava/lang/String;

    .line 271
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    .line 272
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->D:Lcom/google/android/apps/gmm/z/b/l;

    .line 273
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    .line 275
    if-eqz p2, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/g/c;->ak()V

    .line 278
    :cond_0
    return-void

    .line 269
    :cond_1
    sget-object p12, Lcom/google/android/apps/gmm/base/g/c;->x:Lcom/google/b/f/cq;

    goto :goto_0
.end method

.method public static a(Lcom/google/r/b/a/aje;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1022
    iget-object v0, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    .line 1024
    iget-object v0, v0, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    .line 1025
    iget v3, v0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_0

    iget v3, v0, Lcom/google/maps/a/e;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_2

    :goto_1
    if-nez v1, :cond_3

    .line 1026
    :cond_0
    const/4 v0, 0x0

    .line 1028
    :goto_2
    return-object v0

    :cond_1
    move v3, v2

    .line 1025
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 1028
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/maps/a/e;->c:D

    iget-wide v4, v0, Lcom/google/maps/a/e;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1234
    sget-object v3, Lcom/google/android/apps/gmm/base/g/d;->b:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/base/g/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/e;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 1244
    const-string v0, ""

    :cond_1
    :goto_0
    return-object v0

    .line 1237
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    if-eqz v1, :cond_4

    const-string v0, ""

    goto :goto_0

    .line 1238
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1241
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->G()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    const-string v0, ""

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    .line 1242
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    goto :goto_0

    .line 1234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 419
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.6f,%.6f"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1941
    if-eqz p0, :cond_0

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1942
    const-string v0, "http://www.google.com"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1944
    :cond_0
    :goto_0
    return-object p0

    .line 1942
    :cond_1
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/x/o;)Z
    .locals 2
    .param p0    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1865
    if-ne p0, p1, :cond_0

    .line 1866
    const/4 v0, 0x1

    .line 1870
    :goto_0
    return v0

    .line 1867
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 1868
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1870
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/c;->b(Lcom/google/android/apps/gmm/base/g/c;)Z

    move-result v0

    goto :goto_0
.end method

.method private ak()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->L:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->M:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->p:Lcom/google/android/apps/gmm/z/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->S:Lcom/google/android/apps/gmm/z/b/l;

    .line 337
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 1879
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1881
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_0

    .line 1882
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/g/c;->ak()V

    .line 1884
    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0

    .prologue
    .line 1875
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 1876
    return-void
.end method


# virtual methods
.method public final A()Lcom/google/maps/a/a;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->N:Lcom/google/maps/a/a;

    if-nez v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->N:Lcom/google/maps/a/a;

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->N:Lcom/google/maps/a/a;

    return-object v0

    .line 790
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Lcom/google/android/apps/gmm/y/f;
    .locals 5

    .prologue
    const/16 v4, 0x800

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x800

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oa;

    iget v0, v0, Lcom/google/maps/g/oa;->b:I

    invoke-static {v0}, Lcom/google/maps/g/od;->a(I)Lcom/google/maps/g/od;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    :cond_0
    sget-object v3, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x800

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oa;

    iget v0, v0, Lcom/google/maps/g/oa;->b:I

    invoke-static {v0}, Lcom/google/maps/g/od;->a(I)Lcom/google/maps/g/od;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    :cond_1
    sget-object v3, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_4
    if-eqz v0, :cond_8

    .line 804
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/y/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/y/f;-><init>(Lcom/google/r/b/a/acq;)V

    .line 811
    :goto_5
    return-object v0

    :cond_3
    move v0, v2

    .line 803
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-boolean v0, v0, Lcom/google/r/b/a/acq;->h:Z

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    .line 807
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->O:Lcom/google/android/apps/gmm/y/f;

    if-nez v0, :cond_9

    .line 809
    new-instance v1, Lcom/google/android/apps/gmm/y/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/y/f;-><init>(Lcom/google/r/b/a/acq;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->O:Lcom/google/android/apps/gmm/y/f;

    .line 811
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->O:Lcom/google/android/apps/gmm/y/f;

    goto :goto_5
.end method

.method public final C()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oa;

    iget v0, v0, Lcom/google/maps/g/oa;->b:I

    invoke-static {v0}, Lcom/google/maps/g/od;->a(I)Lcom/google/maps/g/od;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    :cond_0
    sget-object v3, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    move v0, v1

    .line 824
    :goto_2
    return v0

    :cond_1
    move v0, v1

    .line 820
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 824
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-boolean v0, v0, Lcom/google/r/b/a/acq;->h:Z

    goto :goto_2
.end method

.method public final D()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oa;

    iget v0, v0, Lcom/google/maps/g/oa;->b:I

    invoke-static {v0}, Lcom/google/maps/g/od;->a(I)Lcom/google/maps/g/od;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    :cond_0
    sget-object v3, Lcom/google/maps/g/od;->a:Lcom/google/maps/g/od;

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 831
    goto :goto_0

    :cond_2
    move v0, v2

    .line 832
    goto :goto_1
.end method

.method public final E()Ljava/lang/String;
    .locals 3

    .prologue
    .line 840
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 841
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oa;

    iget-object v1, v0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 843
    :goto_0
    return-object v0

    .line 841
    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/maps/g/oa;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0

    .line 843
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public final F()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 852
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oa;->d()Lcom/google/maps/g/oa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oa;

    iget-object v1, v0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 853
    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    return-object v0

    .line 852
    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/maps/g/oa;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method public final G()Ljava/lang/String;
    .locals 2

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->k:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/r/b/a/ads;->k:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 865
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final H()Lcom/google/maps/g/hg;
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    return-object v0
.end method

.method public I()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 987
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/c;->I:Z

    if-eqz v0, :cond_1

    .line 1019
    :cond_0
    return-void

    .line 991
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/g/c;->I:Z

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/google/r/b/a/ads;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/ads;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ade;->d()Lcom/google/r/b/a/ade;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ade;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 995
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_6

    .line 997
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->q:Lcom/google/b/c/cv;

    .line 998
    if-eqz v0, :cond_6

    .line 1003
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/r/b/a/ade;

    .line 1004
    iget-object v0, v1, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_3

    .line 1005
    sget-object v4, Lcom/google/android/apps/gmm/base/g/d;->a:[I

    iget-object v0, v1, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    iget v0, v0, Lcom/google/r/b/a/aje;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    :cond_4
    invoke-virtual {v0}, Lcom/google/r/b/a/aiy;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1009
    :pswitch_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->r:Lcom/google/r/b/a/ade;

    goto :goto_2

    .line 1004
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 1012
    :pswitch_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->q:Lcom/google/r/b/a/ade;

    goto :goto_2

    .line 1015
    :pswitch_2
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->s:Lcom/google/r/b/a/ade;

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1

    .line 1005
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final J()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/r/b/a/ads;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/ads;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final K()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1150
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/g/c;->B:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/g/c;->C:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v2, v2, Lcom/google/r/b/a/ads;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    :cond_0
    :goto_1
    if-eqz v1, :cond_4

    .line 1153
    :cond_1
    :goto_2
    return v0

    :cond_2
    move v2, v0

    .line 1150
    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1

    .line 1153
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->S:Z

    goto :goto_2
.end method

.method public final L()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1189
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/g/c;->C:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v2, v2, Lcom/google/r/b/a/ads;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final M()Lcom/google/android/apps/gmm/base/g/f;
    .locals 1

    .prologue
    .line 1207
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/c;->B:Z

    if-nez v0, :cond_0

    .line 1208
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->f:Lcom/google/android/apps/gmm/base/g/f;

    .line 1216
    :goto_0
    return-object v0

    .line 1209
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 1210
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->c:Lcom/google/android/apps/gmm/base/g/f;

    goto :goto_0

    .line 1209
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1211
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1212
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    goto :goto_0

    .line 1213
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->L()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1214
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    goto :goto_0

    .line 1216
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->b:Lcom/google/android/apps/gmm/base/g/f;

    goto :goto_0
.end method

.method public final N()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1253
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pf;

    iget v0, v0, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final O()Lcom/google/maps/g/pc;
    .locals 2

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pf;

    iget-object v0, v0, Lcom/google/maps/g/pf;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pc;->i()Lcom/google/maps/g/pc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pc;

    return-object v0
.end method

.method public final P()Z
    .locals 2

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pf;

    iget-boolean v0, v0, Lcom/google/maps/g/pf;->i:Z

    return v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aec;

    iget-object v1, v0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method public final R()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/pc;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1352
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->H:Ljava/util/List;

    if-nez v0, :cond_2

    .line 1353
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->H:Ljava/util/List;

    .line 1354
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 1355
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pf;

    .line 1356
    new-instance v3, Ljava/util/ArrayList;

    iget-object v1, v0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, v0, Lcom/google/maps/g/pf;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pc;->i()Lcom/google/maps/g/pc;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/pc;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1354
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1356
    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->H:Ljava/util/List;

    .line 1357
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    iget-object v1, v1, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/pf;

    iget-boolean v1, v1, Lcom/google/maps/g/pf;->h:Z

    if-eqz v1, :cond_3

    .line 1358
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    iget-object v1, v1, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/aec;

    iget v1, v1, Lcom/google/r/b/a/aec;->h:I

    if-lez v1, :cond_3

    .line 1359
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->o:Ljava/lang/Boolean;

    .line 1363
    :goto_2
    iget-object v0, v0, Lcom/google/maps/g/pf;->g:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->c()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->p:[B

    .line 1366
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->H:Ljava/util/List;

    return-object v0

    .line 1361
    :cond_3
    iget-boolean v1, v0, Lcom/google/maps/g/pf;->f:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->o:Ljava/lang/Boolean;

    goto :goto_2
.end method

.method public final S()Z
    .locals 2

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pf;

    iget-boolean v0, v0, Lcom/google/maps/g/pf;->h:Z

    return v0
.end method

.method public final T()Lcom/google/b/c/cv;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1434
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->J:Lcom/google/b/c/cv;

    if-nez v0, :cond_2

    .line 1435
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 1436
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/google/r/b/a/ads;->y:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/ads;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ir;->d()Lcom/google/r/b/a/ir;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ir;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1437
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ir;

    .line 1438
    sget-object v3, Lcom/google/d/a/a/b/f;->a:Lcom/google/e/a/a/a/d;

    .line 1439
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 1438
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 1441
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->J:Lcom/google/b/c/cv;

    .line 1443
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->J:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final U()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/uf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1459
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->p:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/wt;->d()Lcom/google/maps/g/wt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/wt;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/maps/g/wt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/uf;->h()Lcom/google/maps/g/uf;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/uf;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final V()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ee;->d()Lcom/google/maps/g/ee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ee;

    iget-object v0, v0, Lcom/google/maps/g/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/uf;->h()Lcom/google/maps/g/uf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/uf;

    invoke-virtual {v0}, Lcom/google/maps/g/uf;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final W()Lcom/google/r/b/a/aje;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1492
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 1493
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->r:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    .line 1497
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 1492
    goto :goto_0

    .line 1494
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    .line 1495
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->t:Lcom/google/r/b/a/aje;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1494
    goto :goto_2

    .line 1497
    :cond_3
    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v0

    goto :goto_1
.end method

.method public final X()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1519
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    if-eqz v2, :cond_1

    .line 1531
    :cond_0
    :goto_0
    return v0

    .line 1523
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v2, v2, Lcom/google/r/b/a/ads;->F:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v2, v2, Lcom/google/r/b/a/ads;->G:Z

    if-nez v2, :cond_0

    .line 1527
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_4

    .line 1529
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v0

    .line 1527
    goto :goto_1

    :cond_3
    move v2, v0

    .line 1529
    goto :goto_2

    :cond_4
    move v0, v1

    .line 1531
    goto :goto_0
.end method

.method public final Y()Lcom/google/android/apps/gmm/hotels/a/c;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/high16 v1, 0x10000

    .line 1569
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->P:Lcom/google/android/apps/gmm/hotels/a/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 1570
    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/c;

    .line 1571
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->A:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gk;->d()Lcom/google/maps/g/gk;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gk;

    sget-object v2, Lcom/google/maps/g/b/p;->b:Lcom/google/e/a/a/a/d;

    .line 1570
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/hotels/a/c;-><init>(Lcom/google/e/a/a/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->P:Lcom/google/android/apps/gmm/hotels/a/c;

    .line 1573
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->P:Lcom/google/android/apps/gmm/hotels/a/c;

    return-object v0

    .line 1569
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Z()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 1614
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->D:Lcom/google/android/apps/gmm/z/b/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->D:Lcom/google/android/apps/gmm/z/b/l;

    .line 1616
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->z:Lcom/google/b/f/cq;

    if-eqz v1, :cond_0

    .line 1617
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/g/c;->z:Lcom/google/b/f/cq;

    aput-object v3, v1, v2

    .line 1618
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 1619
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 1621
    :cond_0
    return-object v0

    .line 1615
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Lcom/google/android/apps/gmm/z/b/l;
    .locals 3

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->S:Lcom/google/android/apps/gmm/z/b/l;

    if-nez v0, :cond_0

    .line 1633
    new-instance v1, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    .line 1634
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->z:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acm;->h()Lcom/google/r/b/a/acm;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acm;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Lcom/google/r/b/a/acm;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 1635
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->S:Lcom/google/android/apps/gmm/z/b/l;

    .line 1637
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->S:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public a(Ljava/util/Locale;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 950
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 951
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    .line 952
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    .line 953
    if-nez v1, :cond_1

    .line 954
    sget-object v1, Lcom/google/android/apps/gmm/base/g/c;->w:Ljava/lang/String;

    const-string v2, "There is neither FeatureID nor LatLong."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 955
    const-string v0, ""

    .line 975
    :goto_1
    return-object v0

    :cond_0
    move v2, v0

    .line 951
    goto :goto_0

    .line 957
    :cond_1
    const-string v0, "http://maps.google.com/?q="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 958
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 971
    :goto_2
    if-eqz p1, :cond_2

    .line 972
    const-string v0, "&hl="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 973
    const-string v0, "&gl="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 975
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 959
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/g/c;->B:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/g/c;->C:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v2, v2, Lcom/google/r/b/a/ads;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_6

    move v2, v1

    :goto_3
    if-eqz v2, :cond_7

    :cond_4
    :goto_4
    if-eqz v1, :cond_8

    :cond_5
    :goto_5
    if-eqz v0, :cond_9

    .line 962
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 966
    const-string v1, "http://maps.google.com/?q="

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 967
    const-string v0, "&ftid="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_6
    move v2, v0

    .line 959
    goto :goto_3

    :cond_7
    move v1, v0

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->S:Z

    goto :goto_5

    .line 963
    :catch_0
    move-exception v0

    .line 964
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 969
    :cond_9
    const-string v0, "http://maps.google.com/?cid="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v4, v5}, Lcom/google/b/h/c;->a(J)Lcom/google/b/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/b/h/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 6
    .param p1    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1555
    iput-object v4, p0, Lcom/google/android/apps/gmm/base/g/c;->P:Lcom/google/android/apps/gmm/hotels/a/c;

    .line 1558
    if-nez p1, :cond_0

    .line 1559
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-static {v0}, Lcom/google/r/b/a/ads;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v0

    iget-object v1, v0, Lcom/google/r/b/a/adu;->h:Lcom/google/n/ao;

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v4, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/adu;->a:I

    const v2, -0x1000001

    and-int/2addr v1, v2

    iput v1, v0, Lcom/google/r/b/a/adu;->a:I

    invoke-virtual {v0}, Lcom/google/r/b/a/adu;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    .line 1565
    :goto_0
    return-void

    .line 1561
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-static {v0}, Lcom/google/r/b/a/ads;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v2

    .line 1562
    invoke-static {}, Lcom/google/maps/g/gk;->d()Lcom/google/maps/g/gk;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    check-cast v0, Lcom/google/maps/g/gk;

    .line 1561
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, v1

    .line 1562
    goto :goto_1

    .line 1561
    :cond_2
    iget-object v1, v2, Lcom/google/r/b/a/adu;->h:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/adu;->a:I

    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    iput v0, v2, Lcom/google/r/b/a/adu;->a:I

    .line 1563
    invoke-virtual {v2}, Lcom/google/r/b/a/adu;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/g/pc;)V
    .locals 6
    .param p1    # Lcom/google/maps/g/pc;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1266
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pf;

    invoke-static {}, Lcom/google/maps/g/pf;->newBuilder()Lcom/google/maps/g/ph;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ph;->a(Lcom/google/maps/g/pf;)Lcom/google/maps/g/ph;

    move-result-object v1

    .line 1267
    if-nez p1, :cond_0

    .line 1268
    iget-object v0, v1, Lcom/google/maps/g/ph;->b:Lcom/google/n/ao;

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v4, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/maps/g/ph;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, v1, Lcom/google/maps/g/ph;->a:I

    .line 1273
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    invoke-static {}, Lcom/google/r/b/a/acq;->newBuilder()Lcom/google/r/b/a/acs;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/acs;->a(Lcom/google/r/b/a/acq;)Lcom/google/r/b/a/acs;

    move-result-object v0

    .line 1274
    iget-object v2, v0, Lcom/google/r/b/a/acs;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/g/ph;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/acs;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Lcom/google/r/b/a/acs;->a:I

    .line 1275
    invoke-virtual {v0}, Lcom/google/r/b/a/acs;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    .line 1278
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-static {v1}, Lcom/google/r/b/a/ads;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v1

    .line 1279
    invoke-virtual {v1, v0}, Lcom/google/r/b/a/adu;->a(Lcom/google/r/b/a/acq;)Lcom/google/r/b/a/adu;

    move-result-object v0

    .line 1280
    invoke-virtual {v0}, Lcom/google/r/b/a/adu;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    .line 1281
    return-void

    .line 1270
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v1, Lcom/google/maps/g/ph;->b:Lcom/google/n/ao;

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/maps/g/ph;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/maps/g/ph;->a:I

    goto :goto_0
.end method

.method public final aa()Lcom/google/maps/g/om;
    .locals 2

    .prologue
    .line 1653
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget v0, v0, Lcom/google/maps/g/oj;->b:I

    invoke-static {v0}, Lcom/google/maps/g/om;->a(I)Lcom/google/maps/g/om;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/om;->g:Lcom/google/maps/g/om;

    :cond_0
    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1668
    .line 1669
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gg;->d()Lcom/google/maps/g/gg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gg;

    iget-object v0, v0, Lcom/google/maps/g/gg;->b:Lcom/google/n/aq;

    .line 1670
    const/16 v1, 0xa

    new-instance v2, Lcom/google/b/a/ab;

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ac()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1677
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/po;->d()Lcom/google/maps/g/po;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/po;

    iget-object v1, v0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/maps/g/po;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method public final ad()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1684
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/mo;->d()Lcom/google/maps/g/mo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mo;

    iget-object v1, v0, Lcom/google/maps/g/mo;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/maps/g/mo;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method public final ae()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1691
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ja;->d()Lcom/google/maps/g/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ja;

    iget-object v1, v0, Lcom/google/maps/g/ja;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/maps/g/ja;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method public final af()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/we;->d()Lcom/google/maps/g/we;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/we;

    iget-object v1, v0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/maps/g/we;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method public final ag()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1706
    .line 1707
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/we;->d()Lcom/google/maps/g/we;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/we;

    iget-object v0, v0, Lcom/google/maps/g/we;->c:Lcom/google/n/aq;

    .line 1708
    const/16 v1, 0xa

    new-instance v2, Lcom/google/b/a/ab;

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ah()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1730
    .line 1731
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->C:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oj;->d()Lcom/google/maps/g/oj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oj;

    iget-object v0, v0, Lcom/google/maps/g/oj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/eu;->d()Lcom/google/maps/g/eu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/eu;

    iget-object v0, v0, Lcom/google/maps/g/eu;->c:Lcom/google/n/aq;

    .line 1732
    const/16 v1, 0xa

    new-instance v2, Lcom/google/b/a/ab;

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ai()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1793
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/c;->Q:Z

    if-eqz v0, :cond_0

    .line 1815
    :goto_0
    return-void

    .line 1796
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/maps/g/ki;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/maps/g/ki;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/tp;->d()Lcom/google/maps/g/tp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tp;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/maps/g/tp;

    .line 1797
    iget v0, v2, Lcom/google/maps/g/tp;->c:I

    invoke-static {v0}, Lcom/google/maps/g/ts;->a(I)Lcom/google/maps/g/ts;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/ts;->a:Lcom/google/maps/g/ts;

    :cond_3
    sget-object v3, Lcom/google/maps/g/ts;->b:Lcom/google/maps/g/ts;

    if-ne v0, v3, :cond_2

    .line 1798
    iget-object v0, v2, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/tu;->d()Lcom/google/maps/g/tu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tu;

    iget-object v1, v0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_8

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->u:Ljava/lang/String;

    .line 1800
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->u:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    :cond_4
    move v0, v4

    :goto_3
    if-nez v0, :cond_5

    .line 1801
    iget-object v0, v2, Lcom/google/maps/g/tp;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/tu;->d()Lcom/google/maps/g/tu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tu;

    iget-object v1, v0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    if-nez v1, :cond_b

    invoke-static {}, Lcom/google/maps/g/tx;->d()Lcom/google/maps/g/tx;

    move-result-object v0

    move-object v1, v0

    :goto_4
    iget-object v0, v1, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->v:Ljava/lang/String;

    .line 1807
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->u:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1808
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->u:Ljava/lang/String;

    .line 1810
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->v:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1811
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->v:Ljava/lang/String;

    .line 1814
    :cond_7
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/g/c;->Q:Z

    goto/16 :goto_0

    .line 1798
    :cond_8
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    iput-object v3, v0, Lcom/google/maps/g/tu;->b:Ljava/lang/Object;

    :cond_9
    move-object v0, v3

    goto :goto_2

    .line 1800
    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    .line 1801
    :cond_b
    iget-object v0, v0, Lcom/google/maps/g/tu;->c:Lcom/google/maps/g/tx;

    move-object v1, v0

    goto :goto_4

    :cond_c
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    iput-object v2, v1, Lcom/google/maps/g/tx;->c:Ljava/lang/Object;

    :cond_d
    move-object v0, v2

    goto :goto_5
.end method

.method public final aj()Lcom/google/android/apps/gmm/place/riddler/c/e;
    .locals 3

    .prologue
    .line 1821
    .line 1822
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->R:Lcom/google/android/apps/gmm/place/riddler/c/e;

    if-nez v0, :cond_0

    .line 1823
    new-instance v1, Lcom/google/android/apps/gmm/place/riddler/c/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->V:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/adv;->d()Lcom/google/r/b/a/adv;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/adv;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/riddler/c/e;-><init>(Lcom/google/r/b/a/adv;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->R:Lcom/google/android/apps/gmm/place/riddler/c/e;

    .line 1825
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->R:Lcom/google/android/apps/gmm/place/riddler/c/e;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1508
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/base/g/c;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1834
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 1835
    :goto_0
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-nez v4, :cond_2

    .line 1836
    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_4

    .line 1837
    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 1856
    :goto_2
    return v0

    .line 1834
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    goto :goto_0

    .line 1835
    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    goto :goto_1

    :cond_3
    move v0, v2

    .line 1840
    goto :goto_2

    .line 1845
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    .line 1846
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_4
    if-nez v3, :cond_7

    move v0, v2

    .line 1847
    goto :goto_2

    :cond_5
    move v0, v2

    .line 1845
    goto :goto_3

    :cond_6
    move v3, v2

    .line 1846
    goto :goto_4

    .line 1849
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    const-wide v2, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;D)Z

    move-result v0

    goto :goto_2

    .line 1850
    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_a

    move v0, v2

    .line 1851
    goto :goto_2

    :cond_9
    move v0, v2

    .line 1850
    goto :goto_5

    .line 1856
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_b

    move v0, v3

    goto :goto_2

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method public final c()Landroid/content/Intent;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1889
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->F:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v1, :cond_1

    .line 1893
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v1

    .line 1894
    if-nez v1, :cond_2

    .line 1903
    :cond_1
    :goto_0
    return-object v0

    .line 1897
    :cond_2
    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v1

    .line 1898
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1901
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1909
    .line 1910
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->c()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1911
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->F:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v0, :cond_1

    .line 1912
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->eJ:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1914
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1919
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v2

    .line 1920
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 1921
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 1922
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v3

    .line 1923
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 1924
    iput-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 1926
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1927
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 1929
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1931
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->e:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 1933
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v1

    .line 1923
    goto :goto_0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/g/g;
    .locals 3

    .prologue
    .line 285
    new-instance v0, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    .line 286
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    .line 287
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/g/h;->d:Lcom/google/android/apps/gmm/base/g/a;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    .line 288
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/c;->B:Z

    .line 289
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/c;->f:Z

    .line 290
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->g:Z

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/c;->C:Z

    .line 291
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->h:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->g:Lcom/google/b/c/cv;

    .line 292
    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->k:Lcom/google/b/c/cv;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/c;->h:Z

    .line 293
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->i:Z

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/c;->i:Z

    .line 294
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->j:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    .line 295
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->y:Ljava/lang/String;

    .line 296
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->z:Lcom/google/b/f/cq;

    .line 297
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->p:Lcom/google/b/f/cq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->A:Ljava/lang/String;

    .line 298
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->q:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->D:Lcom/google/android/apps/gmm/z/b/l;

    .line 299
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->s:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    .line 300
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->d:Ljava/lang/String;

    .line 301
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/g;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 350
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    return-object v0

    .line 349
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->E:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    .line 362
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v2}, Lcom/google/r/b/a/ads;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_2
    move v2, v1

    :goto_1
    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    return v0

    :cond_5
    move v2, v0

    .line 361
    goto :goto_0

    :cond_6
    move v2, v0

    .line 362
    goto :goto_1
.end method

.method public final i()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 370
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    .line 375
    :goto_1
    return-object v0

    :cond_1
    move v2, v0

    .line 370
    goto :goto_0

    .line 372
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    if-nez v0, :cond_5

    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 375
    :cond_5
    const-string v0, ""

    goto :goto_1
.end method

.method public final j()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 398
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->y:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->y:Ljava/lang/String;

    .line 415
    :goto_1
    return-object v0

    :cond_1
    move v2, v0

    .line 398
    goto :goto_0

    .line 403
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v2, v0

    .line 403
    goto :goto_2

    .line 406
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    move v2, v1

    :goto_3
    if-nez v2, :cond_7

    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    move v2, v0

    .line 406
    goto :goto_3

    .line 412
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_9

    :cond_8
    move v0, v1

    :cond_9
    if-nez v0, :cond_a

    .line 413
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 415
    :cond_a
    const-string v0, ""

    goto :goto_1
.end method

.method public final k()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 426
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->A:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    .line 427
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->A:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 436
    :goto_1
    return-object v0

    :cond_1
    move v2, v1

    .line 426
    goto :goto_0

    .line 430
    :cond_2
    new-instance v3, Lcom/google/b/c/cx;

    invoke-direct {v3}, Lcom/google/b/c/cx;-><init>()V

    .line 431
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 432
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_3
    move v2, v0

    :goto_2
    if-nez v2, :cond_7

    :goto_3
    if-eqz v0, :cond_4

    .line 433
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 435
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    invoke-virtual {v3, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    .line 436
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_1

    :cond_6
    move v2, v1

    .line 432
    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public final l()Ljava/lang/String;
    .locals 3

    .prologue
    .line 484
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v2, Lcom/google/r/b/a/ads;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/ads;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->k:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->n:Ljava/lang/String;

    .line 526
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    move-object v0, v1

    .line 529
    :goto_2
    return-object v0

    :cond_1
    move-object v0, v1

    .line 525
    goto :goto_0

    :cond_2
    move v0, v2

    .line 526
    goto :goto_1

    .line 529
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->b:Lcom/google/n/aq;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->b:Lcom/google/n/aq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 545
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->N:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/m;->d()Lcom/google/maps/g/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/m;

    iget-object v0, v0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/o;->d()Lcom/google/maps/g/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/o;

    iget-object v0, v0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Lcom/google/maps/g/q;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 582
    const/4 v0, 0x0

    .line 584
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->N:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/m;->d()Lcom/google/maps/g/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/m;

    iget-object v0, v0, Lcom/google/maps/g/m;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/o;->d()Lcom/google/maps/g/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/o;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/maps/g/o;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/q;

    goto :goto_0
.end method

.method public final r()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/bi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aec;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/bi;->g()Lcom/google/maps/g/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/bi;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aec;

    iget v0, v0, Lcom/google/r/b/a/aec;->h:I

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aec;

    iget-object v0, v0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aec;

    iget-object v1, v0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 676
    :goto_0
    const/4 v1, 0x0

    .line 677
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v2

    if-eqz v2, :cond_4

    move v2, v4

    :goto_1
    if-eqz v2, :cond_0

    .line 678
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/hotels/a/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 681
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_1
    move v2, v4

    :goto_2
    if-eqz v2, :cond_6

    :goto_3
    return-object v0

    .line 675
    :cond_2
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    iput-object v2, v0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    :cond_3
    move-object v0, v2

    goto :goto_0

    :cond_4
    move v2, v3

    .line 677
    goto :goto_1

    :cond_5
    move v2, v3

    .line 681
    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_3
.end method

.method public final v()F
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/high16 v1, 0x7fc00000    # NaNf

    .line 696
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aec;

    .line 697
    iget v3, v0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v2, :cond_1

    :goto_0
    if-eqz v2, :cond_2

    iget v0, v0, Lcom/google/r/b/a/aec;->c:F

    .line 703
    :goto_1
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    move v0, v1

    .line 706
    :cond_0
    return v0

    .line 697
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final w()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 715
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->G:Ljava/util/List;

    if-nez v0, :cond_3

    .line 716
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->G:Ljava/util/List;

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    invoke-virtual {v0}, Lcom/google/r/b/a/acq;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aeg;

    .line 718
    iget-object v1, v0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 719
    iget-object v0, v0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hc;->h()Lcom/google/maps/g/hc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hc;

    .line 720
    iget-object v1, v0, Lcom/google/maps/g/hc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hg;

    .line 721
    iget v2, v1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v6, 0x8

    if-ne v2, v6, :cond_1

    move v2, v3

    :goto_1
    if-eqz v2, :cond_0

    iget v2, v1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v6, 0x2

    if-ne v2, v6, :cond_2

    move v2, v3

    :goto_2
    if-eqz v2, :cond_0

    .line 722
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 725
    invoke-virtual {v0}, Lcom/google/maps/g/hc;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 726
    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 727
    invoke-virtual {v0}, Lcom/google/maps/g/hc;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 728
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/c;->G:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Lcom/google/b/a/an;

    invoke-direct {v6, v0, v1}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v2, v4

    .line 721
    goto :goto_1

    :cond_2
    move v2, v4

    goto :goto_2

    .line 732
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->G:Ljava/util/List;

    return-object v0
.end method

.method public final x()Lcom/google/maps/g/hg;
    .locals 3

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    if-nez v0, :cond_3

    .line 741
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    invoke-static {v0}, Lcom/google/maps/g/hg;->a(Lcom/google/maps/g/hg;)Lcom/google/maps/g/hi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    .line 744
    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 741
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 744
    :cond_1
    iget v2, v0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/hi;->a:I

    iput-object v1, v0, Lcom/google/maps/g/hi;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/maps/g/hi;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    .line 747
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    if-nez v0, :cond_3

    .line 748
    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    .line 751
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->K:Lcom/google/maps/g/hg;

    return-object v0
.end method

.method public final y()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 2

    .prologue
    .line 758
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->L:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v0, :cond_1

    .line 759
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->d()Ljava/lang/String;

    move-result-object v1

    .line 760
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    .line 761
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->L:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 767
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->L:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0

    .line 760
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 764
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->L:Lcom/google/android/apps/gmm/map/b/a/j;

    goto :goto_1
.end method

.method public final z()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 775
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->M:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v1, :cond_0

    .line 776
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v1, v1, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    .line 778
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/maps/a/e;->c:D

    iget-wide v4, v0, Lcom/google/maps/a/e;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->M:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 781
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->M:Lcom/google/android/apps/gmm/map/b/a/q;

    return-object v0

    .line 776
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
