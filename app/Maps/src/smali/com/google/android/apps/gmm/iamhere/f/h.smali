.class public Lcom/google/android/apps/gmm/iamhere/f/h;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/base/l/a/y;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 13

    .prologue
    const/4 v5, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 45
    const/16 v0, 0xc

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 47
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v7

    const/16 v0, 0x10

    .line 48
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v10

    sget v0, Lcom/google/android/apps/gmm/f;->bk:I

    .line 49
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v11

    sget v0, Lcom/google/android/apps/gmm/e;->bh:I

    .line 51
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v12

    sget v0, Lcom/google/android/apps/gmm/e;->bh:I

    .line 52
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v5

    const/4 v0, 0x5

    sget v2, Lcom/google/android/apps/gmm/e;->bg:I

    .line 53
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    const/4 v2, -0x1

    .line 54
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x7

    sget v2, Lcom/google/android/apps/gmm/e;->bg:I

    .line 55
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->aH:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v2, 0x8

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/base/l/a/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/y;->ah_()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    const/4 v0, 0x0

    if-eqz v3, :cond_0

    new-array v0, v7, [Ljava/lang/Class;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v3

    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v3, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/16 v2, 0x9

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/base/l/a/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/y;->f()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/16 v2, 0xa

    new-array v3, v12, [Lcom/google/android/libraries/curvular/cu;

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/base/l/a/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/y;->a()Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bI:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v7

    .line 62
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->e()Lcom/google/android/libraries/curvular/ar;

    move-result-object v0

    aput-object v0, v3, v10

    .line 63
    sget v0, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v11

    .line 60
    invoke-static {v3}, Lcom/google/android/apps/gmm/base/k/aa;->t([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    const/16 v2, 0xb

    new-array v3, v5, [Lcom/google/android/libraries/curvular/cu;

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    .line 66
    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v7

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/base/l/a/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/y;->c()Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bI:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v10

    .line 68
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->f()Lcom/google/android/libraries/curvular/ar;

    move-result-object v0

    aput-object v0, v3, v11

    .line 69
    sget v0, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v12

    .line 65
    invoke-static {v3}, Lcom/google/android/apps/gmm/base/k/aa;->t([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    .line 45
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 66
    :cond_1
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v4, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method
