.class public Lcom/google/android/apps/gmm/map/ui/l;
.super Landroid/text/style/ReplacementSpan;
.source "PG"


# instance fields
.field private a:I

.field private b:F

.field private c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(IFLandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 22
    iput p1, p0, Lcom/google/android/apps/gmm/map/ui/l;->a:I

    .line 23
    iput p2, p0, Lcom/google/android/apps/gmm/map/ui/l;->b:F

    .line 24
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    .line 25
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 8

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v7

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/map/ui/l;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/map/ui/l;->b:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 36
    int-to-float v5, p7

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 42
    int-to-float v5, p7

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 43
    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p5, :cond_0

    iget v1, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v1, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v1, v0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/l;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p2, p3, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method
