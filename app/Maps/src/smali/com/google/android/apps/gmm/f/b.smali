.class public Lcom/google/android/apps/gmm/f/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/gmm/f/b;->a:I

    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 2

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 49
    sget v0, Lcom/google/android/apps/gmm/f/b;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/gmm/f/b;->a:I

    .line 50
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/gmm/l;->iT:I

    .line 51
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->kG:I

    .line 52
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->od:I

    .line 53
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    .line 54
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/google/android/apps/gmm/f/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/f/c;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 59
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 60
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/f/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/f/a;-><init>(Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/net/i;)V

    .line 39
    invoke-static {p0, v0, v0}, Lcom/google/android/apps/gmm/f/b;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 40
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 24
    sget v0, Lcom/google/android/apps/gmm/f/b;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
