.class public Lcom/google/android/apps/gmm/background/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:I


# instance fields
.field private final c:Landroid/content/Intent;

.field private d:Ljava/lang/String;

.field private e:Landroid/os/Messenger;

.field private f:[D

.field private g:J

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 94
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 100
    sput-object v0, Lcom/google/android/apps/gmm/background/a;->a:Ljava/util/Map;

    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "m"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/background/a;->a:Ljava/util/Map;

    const-string v1, "com.google.android.apps.gmm.tools.intent"

    const-string v2, "i"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/background/a;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/google/android/apps/gmm/background/a;->c:Landroid/content/Intent;

    .line 135
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->e:Landroid/os/Messenger;

    const/4 v1, 0x0

    invoke-static {v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 276
    const-string v1, "PrefetchIntentTask"

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x39

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "RemoteException attempting to send resultCode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/shared/c/f;)Z
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->c:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 140
    const-string v0, "PrefetchIntentTask"

    const-string v2, "no intent"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    :goto_0
    return v1

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->c:Landroid/content/Intent;

    const-string v3, "messenger"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    iput-object v0, p0, Lcom/google/android/apps/gmm/background/a;->e:Landroid/os/Messenger;

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->e:Landroid/os/Messenger;

    if-nez v0, :cond_1

    .line 146
    const-string v0, "PrefetchIntentTask"

    const-string v2, "No clientMessenger specified"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->c:Landroid/content/Intent;

    const-string v3, "sender"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 151
    if-nez v0, :cond_2

    .line 152
    const-string v0, "PrefetchIntentTask"

    const-string v3, "Invalid request: no sender"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto :goto_0

    .line 156
    :cond_2
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/background/a;->d:Ljava/lang/String;

    .line 157
    sget-object v0, Lcom/google/android/apps/gmm/background/a;->a:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/apps/gmm/background/a;->d:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 158
    const-string v0, "PrefetchIntentTask"

    const-string v3, "whitelist: %s "

    new-array v4, v2, [Ljava/lang/Object;

    sget-object v5, Lcom/google/android/apps/gmm/background/a;->a:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    const-string v0, "PrefetchIntentTask"

    const-string v3, "Package not authorized %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/background/a;->d:Ljava/lang/String;

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    invoke-direct {p0, v6}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto :goto_0

    .line 165
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "locations"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    if-nez v0, :cond_4

    .line 167
    const-string v0, "PrefetchIntentTask"

    const-string v3, "No %s specified"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "locations"

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    invoke-direct {p0, v8}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 171
    :cond_4
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 174
    array-length v0, v3

    if-eq v0, v6, :cond_5

    array-length v0, v3

    if-eq v0, v9, :cond_5

    .line 175
    const-string v0, "PrefetchIntentTask"

    const-string v3, "Incorrect %s specified"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "locations"

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-direct {p0, v8}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 184
    :cond_5
    aget-object v0, v3, v1

    aget-object v4, v3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 185
    const-string v0, "PrefetchIntentTask"

    const-string v4, "Incorrect lat, lat location (%s, %s) found"

    new-array v5, v6, [Ljava/lang/Object;

    aget-object v6, v3, v1

    aput-object v6, v5, v1

    aget-object v3, v3, v2

    aput-object v3, v5, v2

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    invoke-direct {p0, v8}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 191
    :cond_6
    array-length v0, v3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    move v0, v1

    .line 192
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_7

    .line 194
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    aget-object v5, v3, v0

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    aput-wide v6, v4, v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    const-string v3, "PrefetchIntentTask"

    const-string v4, "Location format error: %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    invoke-direct {p0, v8}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 202
    :goto_2
    array-length v4, v3

    if-ge v0, v4, :cond_a

    .line 203
    iget-object v4, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    aget-wide v4, v4, v0

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/u;->b(D)Z

    move-result v4

    if-nez v4, :cond_8

    .line 204
    const-string v3, "PrefetchIntentTask"

    const-string v4, "Incorrect latitude %f found"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    aget-wide v6, v5, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    invoke-direct {p0, v8}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 208
    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    add-int/lit8 v5, v0, 0x1

    aget-wide v4, v4, v5

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/u;->a(D)Z

    move-result v4

    if-nez v4, :cond_9

    .line 209
    const-string v3, "PrefetchIntentTask"

    const-string v4, "Incorrect longitude %f found"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    add-int/lit8 v0, v0, 0x1

    aget-wide v6, v5, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    invoke-direct {p0, v8}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 202
    :cond_9
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 216
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/background/a;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "deadline_seconds"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/background/a;->h:J

    .line 218
    if-eqz v0, :cond_c

    .line 220
    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/background/a;->g:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 227
    iget-wide v4, p0, Lcom/google/android/apps/gmm/background/a;->g:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_b

    .line 228
    const-string v0, "PrefetchIntentTask"

    const-string v2, "Invalid prefetch deadline time"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-direct {p0, v9}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 221
    :catch_1
    move-exception v0

    .line 222
    const-string v3, "PrefetchIntentTask"

    const-string v4, "Deadline format error: %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    invoke-direct {p0, v9}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    .line 233
    :cond_b
    iget-wide v4, p0, Lcom/google/android/apps/gmm/background/a;->g:J

    iget-wide v6, p0, Lcom/google/android/apps/gmm/background/a;->h:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_c

    .line 234
    const-string v0, "PrefetchIntentTask"

    const-string v2, "Already passed prefetch deadline time"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    invoke-direct {p0, v9}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto/16 :goto_0

    :cond_c
    move v1, v2

    .line 239
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 10

    .prologue
    const-wide v8, 0x412e848000000000L    # 1000000.0

    const/4 v2, 0x0

    .line 243
    const-string v0, "PrefetchIntentTask"

    const-string v1, "execute"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/background/a;->a(Lcom/google/android/apps/gmm/shared/c/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 248
    :cond_0
    const/4 v0, 0x0

    .line 249
    iget-wide v4, p0, Lcom/google/android/apps/gmm/background/a;->g:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    .line 250
    iget-wide v0, p0, Lcom/google/android/apps/gmm/background/a;->g:J

    iget-wide v4, p0, Lcom/google/android/apps/gmm/background/a;->h:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 255
    :cond_1
    :goto_1
    sget-object v3, Lcom/google/android/apps/gmm/prefetch/u;->a:Lcom/google/android/apps/gmm/prefetch/t;

    .line 256
    invoke-interface {v3, p1}, Lcom/google/android/apps/gmm/prefetch/s;->a(Lcom/google/android/apps/gmm/map/c/a;)V

    move v1, v2

    .line 257
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 258
    iget-object v4, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    aget-wide v4, v4, v1

    mul-double/2addr v4, v8

    double-to-int v4, v4

    .line 259
    iget-object v5, p0, Lcom/google/android/apps/gmm/background/a;->f:[D

    add-int/lit8 v6, v1, 0x1

    aget-wide v6, v5, v6

    mul-double/2addr v6, v8

    double-to-int v5, v6

    .line 261
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-direct {v6, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/android/apps/gmm/background/a;->d:Ljava/lang/String;

    invoke-interface {v3, v6, v4, v5, v0}, Lcom/google/android/apps/gmm/prefetch/s;->a(Lcom/google/android/apps/gmm/map/b/a/u;ILjava/lang/String;Ljava/lang/Integer;)V

    .line 257
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/background/a;->d:Ljava/lang/String;

    const-string v3, "com.google.android.googlequicksearchbox"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    sget v0, Lcom/google/android/apps/gmm/background/a;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 265
    :cond_3
    invoke-interface {v3}, Lcom/google/android/apps/gmm/prefetch/s;->a()V

    .line 267
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/background/a;->a(I)V

    goto :goto_0
.end method
