.class public Lcom/google/android/apps/gmm/directions/i/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/a;


# static fields
.field static final a:Ljava/lang/String;

.field private static final d:Landroid/view/View$OnClickListener;


# instance fields
.field private final b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/directions/i/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/a;->a:Ljava/lang/String;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/i/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/a;->d:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/b/c/cv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/a;->b:Lcom/google/b/c/cv;

    .line 35
    sget v0, Lcom/google/android/apps/gmm/l;->aU:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/a;->c:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Lcom/google/android/apps/gmm/directions/i/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/gk;",
            ">;)",
            "Lcom/google/android/apps/gmm/directions/i/a;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/a;

    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/directions/i/a;->b(Landroid/content/Context;Ljava/util/List;)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/directions/i/a;-><init>(Landroid/content/Context;Lcom/google/b/c/cv;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/util/List;)Lcom/google/b/c/cv;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/gk;",
            ">;)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 67
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gk;

    .line 68
    new-instance v3, Lcom/google/android/apps/gmm/directions/i/al;

    invoke-direct {v3, v0, p0}, Lcom/google/android/apps/gmm/directions/i/al;-><init>(Lcom/google/maps/g/a/gk;Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/a;->c:Ljava/lang/String;

    .line 54
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/a;->d:Landroid/view/View$OnClickListener;

    .line 55
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 56
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    return-object v1
.end method

.method public final b()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/a;->b:Lcom/google/b/c/cv;

    return-object v0
.end method
