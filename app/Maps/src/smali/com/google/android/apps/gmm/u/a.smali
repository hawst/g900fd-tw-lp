.class public Lcom/google/android/apps/gmm/u/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/u/a/a;
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;
.implements Lcom/google/android/gms/people/m;
.implements Lcom/google/android/gms/people/n;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/gms/people/l;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/u/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/apps/gmm/u/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/u/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 57
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/a;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/u/b;

    .line 99
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/u/b;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/people/l;->a(Lcom/google/android/gms/people/m;I)Z

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/l;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/people/l;->a(Lcom/google/android/gms/people/n;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 6

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/google/android/gms/people/l;

    const/16 v4, 0x89

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/l;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    .line 71
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/apps/gmm/u/a;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "GooglePlayServicesClient Connection failed. Result:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/people/model/b;)V
    .locals 7

    .prologue
    .line 140
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 142
    invoke-virtual {p1}, Lcom/google/android/gms/people/model/b;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    .line 143
    iget-object v2, p0, Lcom/google/android/apps/gmm/u/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/u/b;

    .line 145
    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->i()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->v()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/apps/gmm/u/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/gms/people/model/b;->d()V

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    monitor-exit p0

    return-void
.end method

.method public final am_()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/u/b;

    .line 106
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/u/b;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/l;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/people/l;->a(Lcom/google/android/gms/people/n;Z)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/l;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/l;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/l;->a()V

    .line 85
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/u/a;->b:Lcom/google/android/gms/people/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/l;->b()V

    .line 92
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->g()V

    .line 93
    return-void
.end method
