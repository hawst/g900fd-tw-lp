.class public Lcom/google/android/apps/gmm/place/j/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/a;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Landroid/view/View$OnClickListener;

.field public e:Landroid/view/View$OnLongClickListener;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/a;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/a;->f:Ljava/lang/Runnable;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/a;->d:Landroid/view/View$OnClickListener;

    .line 65
    return-void
.end method

.method public final a(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/a;->e:Landroid/view/View$OnLongClickListener;

    .line 70
    return-void
.end method

.method public final a(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/a;->c:Ljava/lang/Integer;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/a;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 82
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/a;->b:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/a;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 76
    return-void
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/a;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-static {}, Lcom/google/android/apps/gmm/place/j/b;->a()Lcom/google/android/apps/gmm/place/j/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/a;->d:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/a;->d:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 34
    :cond_0
    return-object v2
.end method
