.class public Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/maps/g/xi;",
        ">;"
    }
.end annotation


# instance fields
.field c:Lcom/google/android/apps/gmm/feedback/d/c;

.field private d:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Lcom/google/android/apps/gmm/feedback/a/d;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Runnable;

.field private i:Z

.field private j:Lcom/google/android/apps/gmm/base/g/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private k:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/maps/g/xd;",
            "Lcom/google/maps/g/xi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 387
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-static {p0, p1, p2, v0, v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;
    .locals 4
    .param p3    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;-><init>()V

    .line 125
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 126
    const-string v2, "is_shake"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 127
    const-string v2, "report_state"

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/feedback/a/d;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    if-eqz p3, :cond_0

    .line 129
    const-string v2, "placemark"

    invoke-virtual {p0, v1, v2, p3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 131
    :cond_0
    if-eqz p4, :cond_1

    .line 132
    const-string v2, "report_a_problem_url"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 135
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->a(Lcom/google/android/apps/gmm/x/a;ZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 54
    check-cast p1, Lcom/google/maps/g/xi;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/maps/g/xi;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/wx;

    iget v0, v0, Lcom/google/maps/g/wx;->b:I

    invoke-static {v0}, Lcom/google/maps/g/xa;->a(I)Lcom/google/maps/g/xa;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/xa;->a:Lcom/google/maps/g/xa;

    :cond_0
    sget-object v7, Lcom/google/maps/g/xa;->f:Lcom/google/maps/g/xa;

    if-ne v0, v7, :cond_1

    move v4, v5

    :cond_1
    sget-object v7, Lcom/google/maps/g/xa;->j:Lcom/google/maps/g/xa;

    if-ne v0, v7, :cond_2

    move v3, v5

    :cond_2
    sget-object v7, Lcom/google/maps/g/xa;->i:Lcom/google/maps/g/xa;

    if-ne v0, v7, :cond_12

    move v0, v5

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v2

    move v3, v2

    move v4, v2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    sget-object v6, Lcom/google/android/apps/gmm/feedback/a/d;->c:Lcom/google/android/apps/gmm/feedback/a/d;

    if-eq v0, v6, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    sget-object v6, Lcom/google/android/apps/gmm/feedback/a/d;->e:Lcom/google/android/apps/gmm/feedback/a/d;

    if-ne v0, v6, :cond_c

    :cond_5
    move v0, v5

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->i:Z

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->i:Z

    if-eqz v4, :cond_6

    if-nez v3, :cond_d

    :cond_6
    move v0, v5

    :goto_3
    or-int/2addr v0, v6

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->i:Z

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    iget-object v7, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->j:Lcom/google/android/apps/gmm/base/g/c;

    sget-object v8, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    if-eq v0, v8, :cond_7

    sget-object v8, Lcom/google/android/apps/gmm/feedback/a/d;->m:Lcom/google/android/apps/gmm/feedback/a/d;

    if-ne v0, v8, :cond_e

    :cond_7
    move v3, v2

    :cond_8
    :goto_4
    if-eqz v3, :cond_9

    sget v0, Lcom/google/android/apps/gmm/l;->lf:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    if-ne v0, v2, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->g:Ljava/lang/String;

    if-eqz v0, :cond_a

    sget v0, Lcom/google/android/apps/gmm/l;->lj:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_10

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/r/b/a/apq;->e:Z

    and-int/2addr v0, v1

    if-eqz v0, :cond_b

    sget v0, Lcom/google/android/apps/gmm/l;->aJ:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_b
    sget v0, Lcom/google/android/apps/gmm/l;->mR:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    sget v0, Lcom/google/android/apps/gmm/l;->mU:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {v6}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->d:Lcom/google/b/c/cv;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->c:Lcom/google/android/apps/gmm/feedback/d/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->d:Lcom/google/b/c/cv;

    iget-object v2, v1, Lcom/google/android/apps/gmm/feedback/d/c;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iput-object v0, v1, Lcom/google/android/apps/gmm/feedback/d/c;->g:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/feedback/d/c;->f:Ljava/lang/Boolean;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v3, v1, Lcom/google/android/apps/gmm/feedback/d/c;->e:Ljava/util/List;

    iget-object v4, v1, Lcom/google/android/apps/gmm/feedback/d/c;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_c
    move v0, v2

    goto/16 :goto_2

    :cond_d
    move v0, v2

    goto/16 :goto_3

    :cond_e
    if-eqz v7, :cond_f

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v0

    sget-object v7, Lcom/google/android/apps/gmm/base/g/f;->b:Lcom/google/android/apps/gmm/base/g/f;

    if-ne v0, v7, :cond_f

    move v2, v5

    :cond_f
    if-nez v2, :cond_8

    move v3, v4

    goto/16 :goto_4

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto/16 :goto_5

    :cond_11
    iget-object v0, v1, Lcom/google/android/apps/gmm/feedback/d/c;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void

    :cond_12
    move v0, v1

    goto/16 :goto_1
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 140
    sget v0, Lcom/google/android/apps/gmm/m;->b:I

    return v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 241
    sget-object v0, Lcom/google/b/f/t;->eu:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 147
    const-string v0, "is_shake"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->e:Z

    .line 148
    const-string v0, "report_state"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/a/d;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v3, "placemark"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->j:Lcom/google/android/apps/gmm/base/g/c;

    .line 150
    const-string v0, "report_a_problem_url"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->g:Ljava/lang/String;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/feedback/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/feedback/j;-><init>(Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->h:Ljava/lang/Runnable;

    .line 159
    new-instance v2, Lcom/google/android/apps/gmm/feedback/d/c;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 160
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    sget v3, Lcom/google/android/apps/gmm/l;->mT:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->h:Ljava/lang/Runnable;

    invoke-direct {v2, v0, p0, v1, v3}, Lcom/google/android/apps/gmm/feedback/d/c;-><init>(Landroid/content/Context;Landroid/widget/AdapterView$OnItemClickListener;Lcom/google/android/apps/gmm/base/views/c/g;Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->c:Lcom/google/android/apps/gmm/feedback/d/c;

    .line 163
    return-void

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 159
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    .line 160
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/feedback/b/b;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v3, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 236
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDestroy()V

    .line 237
    return-void

    .line 232
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->d:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    cmp-long v0, p4, v0

    if-gtz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->d:Lcom/google/b/c/cv;

    long-to-int v1, p4

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 309
    sget v1, Lcom/google/android/apps/gmm/l;->lf:I

    if-ne v0, v1, :cond_7

    .line 310
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->e:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 311
    :goto_1
    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "UNEXPECTED: Unknown entry point: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    sget-object v0, Lcom/google/maps/g/hs;->a:Lcom/google/maps/g/hs;

    move-object v1, v0

    .line 313
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->i:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->j:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_4

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->dismiss()V

    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v2

    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->j:Lcom/google/android/apps/gmm/base/g/c;

    sget-object v3, Lcom/google/maps/g/hq;->b:Lcom/google/maps/g/hq;

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;Lcom/google/maps/g/hq;)V

    goto :goto_0

    .line 310
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/feedback/k;->a:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/a/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x3

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x4

    goto :goto_1

    .line 311
    :pswitch_2
    sget-object v0, Lcom/google/maps/g/hs;->f:Lcom/google/maps/g/hs;

    move-object v1, v0

    goto :goto_2

    :pswitch_3
    sget-object v0, Lcom/google/maps/g/hs;->c:Lcom/google/maps/g/hs;

    move-object v1, v0

    goto :goto_2

    :pswitch_4
    sget-object v0, Lcom/google/maps/g/hs;->g:Lcom/google/maps/g/hs;

    move-object v1, v0

    goto :goto_2

    .line 315
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_3

    .line 318
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v3, v2

    .line 319
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_6

    :goto_5
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->j:Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;)Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;

    move-result-object v0

    .line 318
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    move-object v3, v0

    goto :goto_4

    .line 319
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_5

    .line 322
    :cond_7
    sget v1, Lcom/google/android/apps/gmm/l;->mR:I

    if-ne v0, v1, :cond_9

    .line 323
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_8

    :goto_6
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/e;->d()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_6

    .line 324
    :cond_9
    sget v1, Lcom/google/android/apps/gmm/l;->lj:I

    if-ne v0, v1, :cond_a

    .line 325
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 326
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 327
    :cond_a
    sget v1, Lcom/google/android/apps/gmm/l;->lc:I

    if-ne v0, v1, :cond_b

    .line 328
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "Report direction issue is not implemented yet."

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 330
    :cond_b
    sget v1, Lcom/google/android/apps/gmm/l;->ll:I

    if-ne v0, v1, :cond_c

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "Report suggest issue is not implemented yet."

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 332
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 333
    :cond_c
    sget v1, Lcom/google/android/apps/gmm/l;->aJ:I

    if-ne v0, v1, :cond_11

    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->dismiss()V

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_d

    move-object v0, v2

    :goto_7
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_e

    move-object v0, v2

    :goto_8
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 337
    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v1

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v3, v1, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/maps/g/ha;->a:I

    iput-wide v4, v1, Lcom/google/maps/g/ha;->b:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v0, v1, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/maps/g/ha;->a:I

    iput-wide v4, v1, Lcom/google/maps/g/ha;->c:D

    invoke-virtual {v1}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    .line 338
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->e:Z

    if-eqz v1, :cond_f

    sget-object v1, Lcom/google/maps/g/hs;->f:Lcom/google/maps/g/hs;

    :goto_9
    invoke-static {v1}, Lcom/google/android/apps/gmm/addaplace/a/a;->a(Lcom/google/maps/g/hs;)Lcom/google/android/apps/gmm/addaplace/a/a;

    move-result-object v1

    .line 340
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-nez v3, :cond_10

    :goto_a
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->h()Lcom/google/android/apps/gmm/addaplace/a/b;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/gmm/addaplace/a/b;->a(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;)V

    goto/16 :goto_0

    .line 336
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_7

    :cond_e
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_8

    .line 338
    :cond_f
    sget-object v1, Lcom/google/maps/g/hs;->g:Lcom/google/maps/g/hs;

    goto :goto_9

    .line 340
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_a

    .line 341
    :cond_11
    sget v1, Lcom/google/android/apps/gmm/l;->mU:I

    if-ne v0, v1, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->dismiss()V

    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_12

    :goto_b
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/a/e;->c()V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_b

    .line 311
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 310
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onPause()V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->k:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->k:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/b;->a()Lcom/google/android/apps/gmm/shared/net/b;

    .line 194
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-static {}, Lcom/google/maps/g/xd;->newBuilder()Lcom/google/maps/g/xg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/maps/g/xg;->a(Lcom/google/maps/a/a;)Lcom/google/maps/g/xg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/xg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/xd;

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->k:Lcom/google/android/apps/gmm/shared/net/b;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    const-class v2, Lcom/google/maps/g/xd;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->k:Lcom/google/android/apps/gmm/shared/net/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->k:Lcom/google/android/apps/gmm/shared/net/b;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, p0, v2}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->k:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->J:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/FeedbackTypeFragment;->getView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a:Landroid/view/View;

    .line 173
    return-void

    .line 168
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_1
.end method
