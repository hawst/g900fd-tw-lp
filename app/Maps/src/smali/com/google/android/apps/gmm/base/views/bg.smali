.class Lcom/google/android/apps/gmm/base/views/bg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/util/webimageview/f;

.field final synthetic b:Landroid/graphics/Bitmap;

.field final synthetic c:Lcom/google/android/apps/gmm/base/views/WebImageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/WebImageView;Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/bg;->c:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/bg;->a:Lcom/google/android/apps/gmm/util/webimageview/f;

    iput-object p3, p0, Lcom/google/android/apps/gmm/base/views/bg;->b:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/bg;->c:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/bg;->a:Lcom/google/android/apps/gmm/util/webimageview/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    aput-object v3, v5, v9

    :goto_0
    const/4 v3, 0x1

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v6, v5, v3

    new-instance v3, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v3, v5}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/base/views/bh;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/bh;-><init>(Lcom/google/android/apps/gmm/base/views/WebImageView;Lcom/google/android/apps/gmm/util/webimageview/f;Landroid/graphics/drawable/TransitionDrawable;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v4, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 62
    return-void

    .line 61
    :cond_0
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v5, v9

    goto :goto_0
.end method
