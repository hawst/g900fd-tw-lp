.class public Lcom/google/android/apps/gmm/q/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/q/d;


# instance fields
.field final a:Lcom/google/android/apps/gmm/q/e;

.field b:Lcom/google/android/apps/gmm/q/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:Z

.field d:Z

.field private final e:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final f:Z

.field private final g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Z)V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/q/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/q/e;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/q/g;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;ZLcom/google/android/apps/gmm/q/e;)V

    .line 32
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;ZLcom/google/android/apps/gmm/q/e;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/q/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/q/h;-><init>(Lcom/google/android/apps/gmm/q/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/q/g;->g:Ljava/lang/Object;

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/q/g;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 37
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/q/g;->f:Z

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/gmm/q/g;->a:Lcom/google/android/apps/gmm/q/e;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/q/a;)V
    .locals 2

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/apps/gmm/q/g;->b:Lcom/google/android/apps/gmm/q/a;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/g;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/q/g;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/q/c;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/q/g;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/q/g;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/q/g;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/q/g;->b:Lcom/google/android/apps/gmm/q/a;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/g;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/q/g;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 51
    return-void
.end method
