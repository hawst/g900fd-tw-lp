.class public Lcom/google/android/apps/gmm/car/e/az;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/ad;

.field final b:Lcom/google/android/apps/gmm/navigation/navui/d/m;

.field final c:Lcom/google/android/apps/gmm/directions/f/a/c;

.field d:Landroid/view/View;

.field private final e:Landroid/os/Handler;

.field private final f:J

.field private final g:Lcom/google/android/apps/gmm/z/b/j;

.field private h:Z

.field private final i:Lcom/google/android/apps/gmm/navigation/navui/d/o;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;)V
    .locals 4

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->e:Landroid/os/Handler;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ag:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->g:Lcom/google/android/apps/gmm/z/b/j;

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ba;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/ba;-><init>(Lcom/google/android/apps/gmm/car/e/az;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->i:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    .line 122
    new-instance v0, Lcom/google/android/apps/gmm/car/e/bc;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/bc;-><init>(Lcom/google/android/apps/gmm/car/e/az;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->j:Ljava/lang/Runnable;

    .line 41
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;

    .line 44
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/az;->i:Lcom/google/android/apps/gmm/navigation/navui/d/o;

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/car/ad;->o:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/d/m;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/navui/d/o;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->b:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    .line 46
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    .line 47
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget-object v2, Lcom/google/android/apps/gmm/car/n/b;->e:Lcom/google/android/libraries/curvular/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v1

    .line 46
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/aw;->a(Lcom/google/android/apps/gmm/car/v;II)Lcom/google/android/apps/gmm/directions/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->c:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 50
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    .line 51
    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->V:I

    int-to-long v2, v1

    .line 49
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/car/e/az;->f:J

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/e/az;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/e/az;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 61
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/e/az;->h:Z

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->b:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a:Ljava/lang/String;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->m:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->p:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/az;->j:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/car/e/az;->f:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/az;->g:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/e/ay;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->d:Landroid/view/View;

    .line 57
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/az;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->b:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/m;->p:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/e/az;->h:Z

    .line 75
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/az;->d:Landroid/view/View;

    .line 80
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
