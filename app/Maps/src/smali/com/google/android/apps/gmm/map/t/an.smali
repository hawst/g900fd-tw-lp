.class public Lcom/google/android/apps/gmm/map/t/an;
.super Lcom/google/android/apps/gmm/v/ce;
.source "PG"


# instance fields
.field public b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/ce;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/ce;->a(I)V

    .line 117
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/an;->b:I

    .line 118
    const-string v0, "ShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/an;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get brightnessScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    return-void
.end method
