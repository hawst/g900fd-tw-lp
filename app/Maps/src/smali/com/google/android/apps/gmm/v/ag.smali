.class public Lcom/google/android/apps/gmm/v/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/v/af;

.field private b:Lcom/google/android/apps/gmm/v/af;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method declared-synchronized a()Lcom/google/android/apps/gmm/v/af;
    .locals 2

    .prologue
    .line 796
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ag;->a:Lcom/google/android/apps/gmm/v/af;

    .line 797
    if-eqz v0, :cond_0

    .line 798
    iget-object v1, v0, Lcom/google/android/apps/gmm/v/af;->b:Lcom/google/android/apps/gmm/v/af;

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/ag;->a:Lcom/google/android/apps/gmm/v/af;

    .line 799
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ag;->a:Lcom/google/android/apps/gmm/v/af;

    if-nez v1, :cond_0

    .line 800
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/ag;->b:Lcom/google/android/apps/gmm/v/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803
    :cond_0
    monitor-exit p0

    return-object v0

    .line 796
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/v/af;)V
    .locals 1

    .prologue
    .line 810
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ag;->b:Lcom/google/android/apps/gmm/v/af;

    .line 811
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ag;->b:Lcom/google/android/apps/gmm/v/af;

    .line 812
    if-nez v0, :cond_0

    .line 813
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ag;->a:Lcom/google/android/apps/gmm/v/af;

    .line 817
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/google/android/apps/gmm/v/af;->b:Lcom/google/android/apps/gmm/v/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 818
    monitor-exit p0

    return-void

    .line 815
    :cond_0
    :try_start_1
    iput-object p1, v0, Lcom/google/android/apps/gmm/v/af;->b:Lcom/google/android/apps/gmm/v/af;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 810
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 2

    .prologue
    .line 822
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ag;->a:Lcom/google/android/apps/gmm/v/af;

    .line 823
    const/4 v0, 0x0

    .line 824
    :goto_0
    if-eqz v1, :cond_0

    .line 825
    add-int/lit8 v0, v0, 0x1

    .line 826
    iget-object v1, v1, Lcom/google/android/apps/gmm/v/af;->b:Lcom/google/android/apps/gmm/v/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 828
    :cond_0
    monitor-exit p0

    return v0

    .line 822
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
