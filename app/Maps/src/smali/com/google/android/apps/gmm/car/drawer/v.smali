.class Lcom/google/android/apps/gmm/car/drawer/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/drawer/s;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/drawer/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/drawer/t;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/ad;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 196
    sget v0, Lcom/google/android/apps/gmm/car/drawer/t;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/t;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    new-instance v1, Lcom/google/android/apps/gmm/car/drawer/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/v;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/drawer/g;-><init>(Lcom/google/android/apps/gmm/car/ad;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/j;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 203
    const/4 v0, 0x0

    return-object v0
.end method
