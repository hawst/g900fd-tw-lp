.class Lcom/google/android/apps/gmm/shared/net/ah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q::",
        "Lcom/google/n/at;",
        "S::",
        "Lcom/google/n/at;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/shared/net/b",
        "<TQ;TS;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/apps/gmm/shared/net/a;

.field final b:Lcom/google/android/apps/gmm/shared/c/a/j;

.field final c:Lcom/google/android/apps/gmm/shared/net/ak;

.field d:Lcom/google/n/at;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/shared/net/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/ag",
            "<TQ;TS;>;"
        }
    .end annotation
.end field

.field final f:Ljava/lang/Object;

.field final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/c",
            "<TS;>;",
            "Lcom/google/android/apps/gmm/shared/c/a/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/net/ak;)V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->f:Ljava/lang/Object;

    .line 138
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->g:Ljava/util/Map;

    .line 172
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/ah;->a:Lcom/google/android/apps/gmm/shared/net/a;

    .line 173
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/ah;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 174
    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    .line 175
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/gmm/shared/net/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<TQ;TS;>;"
        }
    .end annotation

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/ag;->f()V

    .line 210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    .line 212
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->d:Lcom/google/n/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    monitor-exit p0

    return-object p0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/net/c",
            "<TS;>;",
            "Lcom/google/android/apps/gmm/shared/c/a/p;",
            ")",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<TQ;TS;>;"
        }
    .end annotation

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-ne p2, v0, :cond_0

    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot execute ResponseListener on Threads.CURRENT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 195
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->d:Lcom/google/n/at;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    iget-object v6, p0, Lcom/google/android/apps/gmm/shared/net/ah;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/ah;->d:Lcom/google/n/at;

    new-instance v5, Lcom/google/android/apps/gmm/shared/net/aj;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/gmm/shared/net/aj;-><init>(Lcom/google/android/apps/gmm/shared/net/ah;Lcom/google/android/apps/gmm/shared/net/ag;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    new-instance v0, Lcom/google/android/apps/gmm/shared/net/ai;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/shared/net/ai;-><init>(Lcom/google/android/apps/gmm/shared/net/ah;Lcom/google/android/apps/gmm/shared/net/ag;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V

    invoke-interface {v6, v0, p2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    monitor-exit p0

    return-object p0
.end method

.method public final declared-synchronized a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<TQ;TS;>;"
        }
    .end annotation

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/ag;->f()V

    .line 182
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->d:Lcom/google/n/at;

    .line 183
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/ag;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/shared/net/ag;-><init>(Lcom/google/android/apps/gmm/shared/net/ah;Lcom/google/n/at;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ah;->a:Lcom/google/android/apps/gmm/shared/net/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/a;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    monitor-exit p0

    return-object p0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/shared/net/ag;Lcom/google/n/at;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/net/ag",
            "<TQ;TS;>;TS;)V"
        }
    .end annotation

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v1, :cond_1

    .line 230
    :cond_0
    monitor-exit p0

    return-void

    .line 222
    :cond_1
    :try_start_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/ah;->d:Lcom/google/n/at;

    .line 224
    new-instance v6, Lcom/google/android/apps/gmm/shared/net/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/gmm/shared/net/aj;-><init>(Lcom/google/android/apps/gmm/shared/net/ah;Lcom/google/android/apps/gmm/shared/net/ag;)V

    .line 225
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ah;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map$Entry;

    move-object v7, v0

    .line 226
    iget-object v9, p0, Lcom/google/android/apps/gmm/shared/net/ah;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 227
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/shared/net/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/ah;->e:Lcom/google/android/apps/gmm/shared/net/ag;

    new-instance v1, Lcom/google/android/apps/gmm/shared/net/ai;

    move-object v2, p0

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/shared/net/ai;-><init>(Lcom/google/android/apps/gmm/shared/net/ah;Lcom/google/android/apps/gmm/shared/net/ag;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V

    .line 228
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 226
    invoke-interface {v9, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 218
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
