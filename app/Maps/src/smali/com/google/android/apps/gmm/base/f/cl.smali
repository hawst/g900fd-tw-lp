.class public Lcom/google/android/apps/gmm/base/f/cl;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/i/l;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 56
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 23
    const/4 v0, 0x3

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    .line 24
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 25
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/i/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/l;->d()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v4

    const/4 v0, 0x2

    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/f/cl;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 23
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20
    check-cast p2, Lcom/google/android/apps/gmm/place/i/l;

    const-class v3, Lcom/google/android/apps/gmm/base/f/bw;

    new-instance v4, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    sget v0, Lcom/google/android/apps/gmm/l;->pt:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-static {p2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v5, "An actual ViewModel instance is required, not a proxy."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput-object p2, v4, Lcom/google/android/apps/gmm/base/l/al;->d:Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    invoke-virtual {p4, v3, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    const-class v0, Lcom/google/android/apps/gmm/base/f/cm;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/i/l;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p4, v0, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    const-class v3, Lcom/google/android/apps/gmm/base/f/ah;

    new-instance v4, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    sget v0, Lcom/google/android/apps/gmm/l;->ps:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-static {p2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v5, "An actual ViewModel instance is required, not a proxy."

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object p2, v4, Lcom/google/android/apps/gmm/base/l/al;->d:Lcom/google/android/libraries/curvular/ce;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/l/al;->c:Lcom/google/android/libraries/curvular/ah;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/i/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/l;->a()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/l/al;->e:Lcom/google/android/libraries/curvular/b/i;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    invoke-virtual {p4, v3, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    return-void
.end method
