.class public Lcom/google/android/apps/gmm/map/aj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/s;


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/gmm/shared/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/aj;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/aj;->c:Lcom/google/android/apps/gmm/shared/c/a;

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/aj;->b:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/aj;->b:Landroid/content/Context;

    const-string v1, "camera"

    const/4 v2, 0x0

    .line 48
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 49
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 50
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 6

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/aj;->b:Landroid/content/Context;

    const-string v1, "camera"

    const/4 v2, 0x0

    .line 39
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/google/android/apps/gmm/map/f/a/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/aj;->c:Lcom/google/android/apps/gmm/shared/c/a;

    .line 41
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a;->b()J

    move-result-wide v2

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/apps/gmm/map/f/a/i;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;J)V

    .line 42
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/f/a/i;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "lat"

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    double-to-float v4, v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "lng"

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    double-to-float v4, v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "zoom"

    iget v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "tilt"

    iget v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "bearing"

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "cameraTimestampMs"

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/f/a/i;->b:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 43
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/c;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 55
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/aj;->b:Landroid/content/Context;

    const-string v3, "camera"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 58
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/n/ak;->a(Landroid/content/SharedPreferences;)Lcom/google/android/apps/gmm/map/f/a/i;

    move-result-object v2

    .line 61
    if-eqz v2, :cond_2

    .line 63
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/f/a/i;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {p1, v3}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 64
    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/f/a/i;->b:J

    .line 65
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/aj;->c:Lcom/google/android/apps/gmm/shared/c/a;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/c/a;->b()J

    move-result-wide v4

    .line 66
    cmp-long v6, v4, v2

    if-ltz v6, :cond_0

    sub-long v2, v4, v2

    sget-wide v4, Lcom/google/android/apps/gmm/map/aj;->a:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    :cond_0
    move v0, v1

    .line 75
    :cond_1
    :goto_0
    return v0

    .line 70
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/n/e;->a()Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move v0, v1

    .line 73
    goto :goto_0
.end method
