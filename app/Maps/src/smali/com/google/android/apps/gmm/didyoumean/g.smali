.class public Lcom/google/android/apps/gmm/didyoumean/g;
.super Lcom/google/android/apps/gmm/didyoumean/a;
.source "PG"


# instance fields
.field private final c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/x/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/didyoumean/a/a;",
            ">;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/didyoumean/a;-><init>(Ljava/util/List;)V

    .line 20
    iput-object p2, p0, Lcom/google/android/apps/gmm/didyoumean/g;->c:Lcom/google/android/apps/gmm/x/o;

    .line 21
    return-void
.end method


# virtual methods
.method public final b()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/b/f/t;->av:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->dismiss()V

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v2, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/didyoumean/g;->c:Lcom/google/android/apps/gmm/x/o;

    .line 38
    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 40
    :cond_0
    return-object v1

    .line 37
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/g;->b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->dismiss()V

    .line 48
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
