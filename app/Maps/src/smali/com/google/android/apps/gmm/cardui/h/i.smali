.class public Lcom/google/android/apps/gmm/cardui/h/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/i;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/util/b/ag;

.field private final b:Lcom/google/o/h/a/gp;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/gp;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget v1, v1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final b(I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 39
    const-string v0, ""

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 67
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 66
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 68
    return-object v4
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 74
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 73
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 75
    return-object v4
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 3

    .prologue
    .line 80
    new-instance v1, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 81
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    invoke-virtual {v0}, Lcom/google/o/h/a/gp;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    .line 87
    if-eqz v0, :cond_1

    .line 88
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    .line 90
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 82
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 111
    sget v0, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/i;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
