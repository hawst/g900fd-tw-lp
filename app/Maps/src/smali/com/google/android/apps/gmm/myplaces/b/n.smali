.class Lcom/google/android/apps/gmm/myplaces/b/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/apps/gmm/myplaces/b/ab;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/myplaces/b/ab;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->a:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    .line 58
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/myplaces/c/f;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/apps/gmm/myplaces/c/f;",
            ">(",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<TT;>;",
            "Lcom/google/e/a/a/a/b;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 125
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/myplaces/c/f;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    const-string v1, "MyPlaces"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 130
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/e/a/a/a/b;)Z
    .locals 11

    .prologue
    .line 46
    const/4 v1, 0x5

    iget-object v0, p2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p2, v1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    const-string v0, "MyPlaces"

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "There was an error in corpus #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    :goto_2
    return v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    const/4 v1, 0x4

    iget-object v2, p2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(IZ)I

    move-result v1

    if-lez v1, :cond_4

    const/4 v0, 0x1

    :cond_4
    :goto_3
    const/4 v1, 0x0

    move v3, v1

    move v2, v0

    :goto_4
    if-ge v3, v4, :cond_10

    const/4 v0, 0x4

    const/16 v1, 0x1a

    invoke-virtual {p2, v0, v3, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/4 v5, 0x2

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_9

    const/4 v1, 0x1

    :goto_5
    if-nez v1, :cond_5

    invoke-virtual {v0, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    :cond_5
    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_f

    const/4 v1, 0x2

    const/16 v5, 0x1a

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/4 v1, 0x7

    const-wide/16 v6, 0x0

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v5

    invoke-virtual {v1, v5, v6, v7}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(IJ)V

    :cond_6
    const/4 v1, 0x2

    const/16 v5, 0x1c

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->a(Lcom/google/e/a/a/a/b;)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v5

    const-string v6, "corpus = ? AND key_string = ?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x1

    aput-object v1, v7, v5

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "sync_item"

    invoke-virtual {v0, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    :cond_7
    :goto_7
    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_4

    :cond_8
    if-lez v4, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(IZ)I

    move-result v1

    if-lez v1, :cond_4

    const/4 v0, 0x1

    goto/16 :goto_3

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    :cond_a
    const/4 v1, 0x0

    goto :goto_6

    :cond_b
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->c(Lcom/google/e/a/a/a/b;)J

    move-result-wide v8

    new-instance v5, Lcom/google/android/apps/gmm/myplaces/b/ad;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/myplaces/b/ad;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v10

    iput v10, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->a:I

    iput-object v1, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->b:Ljava/lang/String;

    iput-wide v8, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->c:J

    iput-wide v6, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->d:J

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->d(Lcom/google/e/a/a/a/b;)Lcom/google/b/h/c;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/google/b/h/c;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_a
    iput-object v1, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->e:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->e(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v6

    if-eqz v6, :cond_d

    iget v1, v6, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_b
    iput-object v1, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->f:Ljava/lang/Integer;

    if-eqz v6, :cond_e

    iget v1, v6, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_c
    iput-object v1, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->g:Ljava/lang/Integer;

    const/4 v1, 0x0

    iput-boolean v1, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->h:Z

    iput-object v0, v5, Lcom/google/android/apps/gmm/myplaces/b/ad;->i:Lcom/google/e/a/a/a/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ad;)V

    goto :goto_7

    :cond_c
    const/4 v1, 0x0

    goto :goto_a

    :cond_d
    const/4 v1, 0x0

    goto :goto_b

    :cond_e
    const/4 v1, 0x0

    goto :goto_c

    :cond_f
    const/4 v0, 0x0

    goto :goto_8

    :cond_10
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-static {p2, v0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IJ)J

    move-result-wide v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "corpus"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "last_sync_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "sync_corpus"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_2

    :cond_11
    move v0, v2

    goto/16 :goto_9
.end method
