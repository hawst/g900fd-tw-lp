.class public Lcom/google/android/apps/gmm/map/internal/d/b/ab;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:J

.field public final b:[B


# direct methods
.method constructor <init>(J[B)V
    .locals 1

    .prologue
    .line 1367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1368
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    .line 1369
    if-nez p3, :cond_0

    .line 1370
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a:[B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    .line 1374
    :goto_0
    return-void

    .line 1372
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1399
    if-ne p0, p1, :cond_1

    .line 1400
    const/4 v0, 0x1

    .line 1413
    :cond_0
    :goto_0
    return v0

    .line 1403
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 1407
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    .line 1409
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1413
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 1418
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 1419
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ab;->b:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1420
    return v0
.end method
