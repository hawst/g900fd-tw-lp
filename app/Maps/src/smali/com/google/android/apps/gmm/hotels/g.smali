.class public Lcom/google/android/apps/gmm/hotels/g;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/hotels/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/e/a/a/a/b;

.field private volatile d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/e/a/a/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/util/Collection;Lcom/google/e/a/a/a/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ">;",
            "Lcom/google/e/a/a/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    sget-object v0, Lcom/google/r/b/a/el;->bZ:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/as;->f:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/g;->b:Ljava/util/Collection;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/gmm/hotels/g;->c:Lcom/google/e/a/a/a/b;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/g;->a:Ljava/util/List;

    .line 52
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 11

    .prologue
    const/16 v10, 0x1a

    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 80
    .line 81
    invoke-virtual {p1, v2, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 80
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v4

    .line 83
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/g;->d:Ljava/util/Map;

    .line 84
    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    .line 85
    iget-object v0, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v9}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v2

    :goto_1
    if-nez v0, :cond_0

    invoke-virtual {v6, v9}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_2
    if-eqz v0, :cond_1

    .line 86
    iget-object v7, p0, Lcom/google/android/apps/gmm/hotels/g;->d:Ljava/util/Map;

    const/16 v0, 0x1c

    invoke-virtual {v6, v2, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    .line 87
    invoke-virtual {v6, v9, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 86
    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 85
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 90
    :cond_4
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 63
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/g/b/aa;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/g;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/j;

    .line 67
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-virtual {v1, v4, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_0
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/g;->c:Lcom/google/e/a/a/a/b;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 72
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/as;->e:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 73
    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 75
    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/g;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/hotels/a/g;

    .line 101
    if-nez p1, :cond_2

    .line 102
    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/g;->d:Ljava/util/Map;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/hotels/a/g;->a(Ljava/util/Map;)V

    goto :goto_0

    .line 104
    :cond_2
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failure "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/g;->a()V

    goto :goto_0
.end method
