.class public Lcom/google/android/apps/gmm/directions/i/be;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/directions/h/x;

.field public b:Lcom/google/android/apps/gmm/directions/h/y;

.field public c:Z

.field d:Z

.field e:Lcom/google/android/apps/gmm/directions/i/bi;

.field public f:Lcom/google/android/apps/gmm/directions/h/z;

.field private final h:Landroid/content/Context;

.field private final i:Lcom/google/android/apps/gmm/map/r/a/ao;

.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/directions/i/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/be;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bi;->c:Lcom/google/android/apps/gmm/directions/i/bi;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/be;->e:Lcom/google/android/apps/gmm/directions/i/bi;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 43
    iput p3, p0, Lcom/google/android/apps/gmm/directions/i/be;->j:I

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/directions/h/w;
    .locals 10
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/d/h;->b(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/be;->g:Ljava/lang/String;

    const-string v2, "travel mode unavailable"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    :goto_0
    if-eqz v0, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/directions/h/w;->a(Lcom/google/android/apps/gmm/directions/h/x;)V

    .line 84
    :cond_0
    return-object v0

    .line 80
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->b:Lcom/google/android/apps/gmm/directions/h/y;

    if-nez v2, :cond_2

    new-instance v2, Lcom/google/android/apps/gmm/directions/i/bj;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/directions/i/bj;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->b:Lcom/google/android/apps/gmm/directions/h/y;

    :cond_2
    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    sget-object v3, Lcom/google/android/apps/gmm/directions/h/x;->d:Lcom/google/android/apps/gmm/directions/h/x;

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    sget-object v3, Lcom/google/android/apps/gmm/directions/h/x;->e:Lcom/google/android/apps/gmm/directions/h/x;

    if-ne v2, v3, :cond_4

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/i/be;->j:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/be;->b:Lcom/google/android/apps/gmm/directions/h/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/i/be;->e:Lcom/google/android/apps/gmm/directions/i/bi;

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/directions/i/be;->d:Z

    iget-object v7, p0, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/directions/i/q;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/android/apps/gmm/directions/h/z;)V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/gmm/directions/i/bf;->b:[I

    invoke-virtual {v1}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lcom/google/android/apps/gmm/directions/i/be;->g:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "travel mode not supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bf;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/h/x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ae;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/i/be;->j:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/be;->b:Lcom/google/android/apps/gmm/directions/h/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/i/be;->e:Lcom/google/android/apps/gmm/directions/i/bi;

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/directions/i/be;->d:Z

    sget-object v7, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    iget-object v8, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    const/16 v9, 0xe

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/directions/i/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/r/b/a/acy;ILcom/google/android/apps/gmm/directions/h/z;)V

    goto/16 :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/i/be;->j:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/be;->b:Lcom/google/android/apps/gmm/directions/h/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/i/be;->e:Lcom/google/android/apps/gmm/directions/i/bi;

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/directions/i/be;->d:Z

    iget-object v7, p0, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/directions/i/s;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/android/apps/gmm/directions/h/z;)V

    goto/16 :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ak;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/i/be;->j:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/directions/i/ak;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/z;)V

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ba;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/be;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/be;->i:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/i/be;->j:I

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/directions/i/be;->c:Z

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/ba;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;IZLcom/google/android/apps/gmm/directions/h/z;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
