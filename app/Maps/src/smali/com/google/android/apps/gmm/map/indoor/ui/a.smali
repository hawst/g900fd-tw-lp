.class public Lcom/google/android/apps/gmm/map/indoor/ui/a;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/gmm/map/indoor/ui/d;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field b:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/map/indoor/ui/c;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 66
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->c:Ljava/util/Set;

    .line 70
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/indoor/d/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 135
    if-nez p1, :cond_0

    .line 144
    :goto_0
    return v0

    .line 138
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 139
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->c:Ljava/util/Set;

    monitor-enter v2

    .line 140
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->c:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    const/4 v0, 0x1

    monitor-exit v2

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    if-nez p2, :cond_0

    .line 99
    sget v0, Lcom/google/android/apps/gmm/h;->T:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v0, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 100
    new-instance v3, Lcom/google/android/apps/gmm/map/indoor/ui/c;

    sget v0, Lcom/google/android/apps/gmm/g;->dV:I

    .line 101
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/g;->bO:I

    .line 102
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/g;->da:I

    .line 103
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/apps/gmm/map/indoor/ui/c;-><init>(Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 110
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/ui/d;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/indoor/ui/d;->toString()Ljava/lang/String;

    move-result-object v4

    .line 112
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/ui/c;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/ui/d;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    .line 115
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->b:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->b:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 116
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/map/indoor/d/f;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 117
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/ui/c;->b:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 122
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/e;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/ui/c;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 127
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/ui/c;->c:Landroid/view/View;

    .line 128
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v6, :cond_3

    move v0, v1

    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/ui/c;->b:Landroid/view/View;

    .line 129
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v6, :cond_4

    move v3, v1

    .line 127
    :goto_4
    if-eqz v0, :cond_5

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/l;->C:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {p2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    return-object p2

    .line 107
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    goto :goto_0

    .line 119
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/ui/c;->b:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->d:Lcom/google/android/apps/gmm/map/indoor/ui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/ui/c;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v0, v2

    .line 128
    goto :goto_3

    :cond_4
    move v3, v2

    .line 129
    goto :goto_4

    .line 127
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/l;->B:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/l;->A:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/a;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/l;->z:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
