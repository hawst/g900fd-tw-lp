.class Lcom/google/android/apps/gmm/map/o/bf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/o/bf;


# instance fields
.field final b:[S

.field c:I

.field d:Ljava/nio/ShortBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 517
    new-instance v0, Lcom/google/android/apps/gmm/map/o/bf;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/bf;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/bf;->a:Lcom/google/android/apps/gmm/map/o/bf;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 519
    const/4 v0, 0x6

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    .line 522
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/bf;->c:I

    .line 527
    const/16 v0, 0x600

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/o/bf;->a(I)V

    .line 528
    return-void
.end method

.method static a()Lcom/google/android/apps/gmm/map/o/bf;
    .locals 1

    .prologue
    .line 580
    sget-object v0, Lcom/google/android/apps/gmm/map/o/bf;->a:Lcom/google/android/apps/gmm/map/o/bf;

    return-object v0
.end method


# virtual methods
.method final a(I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 549
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/bf;->c:I

    if-ge v0, p1, :cond_1

    .line 551
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/bf;->c:I

    shl-int/lit8 v0, v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 553
    sget-object v2, Lcom/google/android/apps/gmm/map/o/bc;->a:Ljava/lang/String;

    const-string v3, "expand buffer: %d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 555
    div-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v2, v0, 0x6

    .line 557
    shl-int/lit8 v0, v2, 0x1

    .line 559
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v3

    move v0, v1

    .line 560
    :goto_0
    div-int/lit8 v4, v2, 0x6

    if-ge v0, v4, :cond_0

    shl-int/lit8 v4, v0, 0x2

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    int-to-short v6, v4

    aput-short v6, v5, v1

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    add-int/lit8 v6, v4, 0x3

    int-to-short v6, v6

    aput-short v6, v5, v8

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    const/4 v6, 0x2

    add-int/lit8 v7, v4, 0x1

    int-to-short v7, v7

    aput-short v7, v5, v6

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    const/4 v6, 0x3

    add-int/lit8 v7, v4, 0x1

    int-to-short v7, v7

    aput-short v7, v5, v6

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    const/4 v6, 0x4

    add-int/lit8 v7, v4, 0x3

    int-to-short v7, v7

    aput-short v7, v5, v6

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    const/4 v6, 0x5

    add-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    aput-short v4, v5, v6

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/bf;->b:[S

    invoke-virtual {v3, v4}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 561
    :cond_0
    invoke-virtual {v3}, Ljava/nio/ShortBuffer;->rewind()Ljava/nio/Buffer;

    .line 562
    iput v2, p0, Lcom/google/android/apps/gmm/map/o/bf;->c:I

    .line 563
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/o/bf;->d:Ljava/nio/ShortBuffer;

    .line 565
    :cond_1
    return-void
.end method
