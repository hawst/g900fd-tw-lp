.class final Lcom/google/android/apps/gmm/map/internal/d/b/ac;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[B

.field b:I

.field final c:Ljava/io/RandomAccessFile;

.field d:I


# direct methods
.method constructor <init>(Ljava/io/RandomAccessFile;I[B)V
    .locals 1

    .prologue
    .line 3561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3562
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a:[B

    .line 3563
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    .line 3564
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    .line 3565
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    .line 3566
    return-void
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3591
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    if-nez v0, :cond_0

    .line 3601
    :goto_0
    return-void

    .line 3594
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    monitor-enter v1

    .line 3595
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3596
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a:[B

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    invoke-virtual {v0, v2, v3, v4}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 3597
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 3598
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3599
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    .line 3600
    iput v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    goto :goto_0

    .line 3598
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final a([BI)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3573
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    add-int/2addr v0, p2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 3574
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a()V

    .line 3577
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a:[B

    array-length v0, v0

    if-le p2, v0, :cond_1

    .line 3578
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    monitor-enter v1

    .line 3579
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3580
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, p2}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 3581
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 3582
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3583
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->d:I

    .line 3588
    :goto_0
    return-void

    .line 3582
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 3585
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->a:[B

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    invoke-static {p1, v2, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3586
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ac;->b:I

    goto :goto_0
.end method
