.class public Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/navui/aw;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private c:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/ah;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/base/l/ah;


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/f;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->a:Lcom/google/android/apps/gmm/navigation/navui/aw;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/aw;->a()V

    .line 74
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/bh;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->c:Lcom/google/android/libraries/curvular/ae;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->c:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->d:Lcom/google/android/apps/gmm/base/l/ah;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 54
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->c:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 56
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 68
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 69
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/SavePowerInterstitialDialog;->a:Lcom/google/android/apps/gmm/navigation/navui/aw;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/aw;->a()V

    .line 63
    return-void
.end method
