.class public final Lcom/google/android/apps/gmm/map/b/a/ab;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:[I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private volatile d:Lcom/google/android/apps/gmm/map/b/a/ae;

.field private volatile e:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ac;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ac;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/ab;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>([I)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->c:Ljava/util/List;

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    .line 57
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->e:F

    .line 58
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ab;II)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 5

    .prologue
    .line 129
    sub-int v0, p2, p1

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [I

    .line 130
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    mul-int/lit8 v2, p1, 0x3

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    return-object v1
.end method

.method public static a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 2

    .prologue
    .line 210
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)[I

    move-result-object v0

    .line 211
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    return-object v1
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 3

    .prologue
    .line 184
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 185
    mul-int/lit8 v0, v1, 0x3

    new-array v2, v0, [I

    .line 186
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 187
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;[II)V

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    return-object v0
.end method

.method public static a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/b/a/ab;"
        }
    .end annotation

    .prologue
    .line 112
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    new-array v2, v0, [I

    .line 113
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 114
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 115
    mul-int/lit8 v3, v1, 0x3

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    aput v4, v2, v3

    add-int/lit8 v4, v3, 0x1

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    aput v5, v2, v4

    add-int/lit8 v3, v3, 0x2

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    aput v0, v2, v3

    .line 113
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 117
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    return-object v0
.end method

.method public static a([I)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    return-object v0
.end method

.method public static a(Lcom/google/maps/b/a/cu;ILcom/google/maps/b/a/cy;Lcom/google/android/apps/gmm/map/b/a/aw;)[Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 216
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)[I

    move-result-object v5

    .line 218
    iget v0, p2, Lcom/google/maps/b/a/cy;->b:I

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/ab;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    aput-object v2, v0, v1

    .line 235
    :goto_0
    return-object v0

    .line 221
    :cond_0
    iget v0, p2, Lcom/google/maps/b/a/cy;->b:I

    add-int/lit8 v0, v0, 0x1

    new-array v4, v0, [Lcom/google/android/apps/gmm/map/b/a/ab;

    move v0, v1

    move v2, v1

    .line 223
    :goto_1
    iget v3, p2, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v3, :cond_1

    .line 224
    iget-object v3, p2, Lcom/google/maps/b/a/cy;->a:[I

    aget v3, v3, v0

    .line 225
    sub-int v6, v3, v2

    mul-int/lit8 v6, v6, 0x3

    new-array v6, v6, [I

    .line 226
    mul-int/lit8 v7, v2, 0x3

    sub-int v2, v3, v2

    mul-int/lit8 v2, v2, 0x3

    invoke-static {v5, v7, v6, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v2, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    aput-object v2, v4, v0

    .line 223
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    .line 230
    :cond_1
    array-length v0, v5

    .line 231
    mul-int/lit8 v3, v2, 0x3

    sub-int v3, v0, v3

    new-array v3, v3, [I

    .line 232
    mul-int/lit8 v6, v2, 0x3

    mul-int/lit8 v2, v2, 0x3

    sub-int/2addr v0, v2

    invoke-static {v5, v6, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 233
    iget v0, p2, Lcom/google/maps/b/a/cy;->b:I

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    aput-object v1, v4, v0

    move-object v0, v4

    .line 235
    goto :goto_0
.end method


# virtual methods
.method public a(FIIILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Z)I
    .locals 15

    .prologue
    .line 564
    move/from16 v0, p3

    move-object/from16 v1, p5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 565
    move/from16 v0, p4

    move-object/from16 v1, p6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 567
    const/4 v8, -0x1

    .line 569
    add-int v6, p3, p2

    move/from16 v5, p1

    :goto_0
    add-int/lit8 v4, p4, -0x1

    if-gt v6, v4, :cond_0

    .line 571
    move-object/from16 v0, p8

    invoke-virtual {p0, v6, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 572
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move-object/from16 v2, p8

    move-object/from16 v3, p7

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    .line 574
    cmpl-float v7, v4, v5

    if-lez v7, :cond_3

    move v8, v6

    .line 570
    :goto_1
    add-int v6, v6, p2

    move v5, v4

    goto :goto_0

    .line 582
    :cond_0
    const/4 v4, 0x0

    .line 583
    if-ltz v8, :cond_2

    .line 584
    const/4 v4, 0x1

    .line 585
    const/4 v5, 0x1

    aput-boolean v5, p9, v8

    .line 586
    add-int/lit8 v5, p3, 0x1

    if-le v8, v5, :cond_1

    move-object v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    .line 587
    invoke-virtual/range {v4 .. v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FIIILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Z)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 590
    :cond_1
    add-int/lit8 v5, p4, -0x1

    if-ge v8, v5, :cond_2

    move-object v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    .line 591
    invoke-virtual/range {v5 .. v14}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FIIILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Z)I

    move-result v5

    add-int/2addr v4, v5

    .line 595
    :cond_2
    return v4

    :cond_3
    move v4, v5

    goto :goto_1
.end method

.method public final a(FLcom/google/android/apps/gmm/map/b/a/y;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 398
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 399
    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move v0, v1

    .line 427
    :goto_0
    return v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    .line 404
    add-int/lit8 v4, v0, -0x1

    .line 405
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    .line 406
    invoke-virtual {p0, v4, p2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 407
    add-int/lit8 v0, v4, -0x1

    goto :goto_0

    .line 409
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v0

    mul-float v2, v0, p1

    .line 410
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ab;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    move v3, v2

    move v2, v1

    .line 412
    :goto_1
    if-ge v2, v4, :cond_3

    .line 413
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->b(I)F

    move-result v5

    .line 414
    cmpl-float v6, v5, v3

    if-ltz v6, :cond_2

    .line 415
    div-float/2addr v3, v5

    .line 416
    aget-object v1, v0, v1

    .line 417
    const/4 v4, 0x1

    aget-object v0, v0, v4

    .line 418
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 419
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 420
    invoke-static {v1, v0, v3, p2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move v0, v2

    .line 421
    goto :goto_0

    .line 423
    :cond_2
    sub-float/2addr v3, v5

    .line 412
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 426
    :cond_3
    invoke-virtual {p0, v4, p2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 427
    add-int/lit8 v0, v4, -0x1

    goto :goto_0
.end method

.method public final a()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-lez v0, :cond_1

    .line 66
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 71
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    return-object v0

    .line 68
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    .line 269
    mul-int/lit8 v0, p1, 0x3

    .line 270
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v1
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 276
    mul-int/lit8 v0, p1, 0x3

    .line 277
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v1, v1, v0

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 278
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iput v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 279
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iput v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 280
    return-void
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 298
    mul-int/lit8 v0, p1, 0x3

    .line 299
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v1, v1, v0

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, v2

    iput v1, p3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v2

    iput v1, p3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v1, v0

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    sub-int/2addr v0, v1

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 302
    return-void
.end method

.method public final b(I)F
    .locals 6

    .prologue
    .line 345
    mul-int/lit8 v0, p1, 0x3

    .line 346
    add-int/lit8 v1, v0, 0x3

    .line 347
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v2, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v4, v1, 0x1

    aget v1, v2, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v5, v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v1, v3

    int-to-float v1, v1

    .line 349
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v2, v3, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v3, v3, v5

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 350
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public final c(I)F
    .locals 4

    .prologue
    .line 930
    mul-int/lit8 v0, p1, 0x3

    .line 931
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v2, v0, 0x3

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v2, v2, v0

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v0, v0, 0x1

    aget v0, v3, v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v0

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x3

    .line 291
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v1
.end method

.method public final d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->c:Ljava/util/List;

    if-nez v1, :cond_2

    .line 311
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v2, v1, 0x3

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->c:Ljava/util/List;

    .line 312
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    if-ge v0, v1, :cond_2

    .line 313
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->c:Ljava/util/List;

    return-object v0
.end method

.method public final e()F
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 330
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->e:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v2, v0, -0x1

    .line 333
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 334
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->b(I)F

    move-result v3

    add-float/2addr v1, v3

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 336
    :cond_0
    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->e:F

    .line 338
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->e:F

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 432
    if-ne p1, p0, :cond_0

    .line 433
    const/4 v0, 0x1

    .line 439
    :goto_0
    return v0

    .line 435
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eqz v0, :cond_1

    .line 436
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0

    .line 439
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 358
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 359
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x3

    .line 360
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v3, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v4, v4, v2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v2, v2, 0x2

    aget v2, v4, v2

    if-ne v3, v2, :cond_0

    .line 364
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 360
    goto :goto_0

    :cond_1
    move v0, v1

    .line 364
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 818
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 819
    const-string v0, "Polyline{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 821
    if-lez v0, :cond_0

    .line 822
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    :cond_0
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    aget v3, v3, v0

    .line 825
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    .line 826
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    .line 827
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    .line 828
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    add-int/lit8 v4, v0, 0x2

    aget v3, v3, v4

    .line 829
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    .line 830
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 832
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 833
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
