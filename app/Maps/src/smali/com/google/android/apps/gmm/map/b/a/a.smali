.class public Lcom/google/android/apps/gmm/map/b/a/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public d:D


# direct methods
.method public constructor <init>(D)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    .line 35
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    .line 24
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    .line 25
    iput-wide p5, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    .line 26
    iput-wide p7, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    .line 27
    return-void
.end method

.method public static a(DDDD)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    .line 72
    neg-double v0, p0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4018000000000000L    # 6.0

    div-double/2addr v0, v2

    mul-double v2, p2, v6

    add-double/2addr v0, v2

    mul-double v2, p4, v6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    div-double v2, p6, v6

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(DDDDD)D
    .locals 10

    .prologue
    .line 92
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_0

    .line 93
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v2, p0, v2

    mul-double/2addr v0, v2

    add-double v0, v0, p8

    .line 102
    :goto_0
    return-wide v0

    .line 95
    :cond_0
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_1

    .line 96
    const-wide/16 v0, 0x0

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v0

    mul-double/2addr v0, p0

    add-double/2addr v0, p2

    goto :goto_0

    .line 99
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, p0

    mul-double v2, v0, v0

    mul-double/2addr v0, v2

    mul-double/2addr v0, p2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, p0

    .line 100
    mul-double/2addr v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v2, p0

    mul-double/2addr v2, p4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, p0

    mul-double/2addr v2, v4

    .line 101
    mul-double v4, p0, p0

    mul-double/2addr v2, v4

    mul-double v2, v2, p6

    add-double/2addr v0, v2

    .line 102
    mul-double v2, p0, p0

    mul-double/2addr v2, p0

    mul-double v2, v2, p8

    add-double/2addr v0, v2

    goto :goto_0
.end method

.method public static b(DDDD)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    .line 82
    div-double v0, p0, v6

    mul-double v2, p2, v6

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double v2, p4, v6

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    mul-double/2addr v2, p6

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public static b(DDDDD)D
    .locals 8

    .prologue
    .line 119
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p0, p1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 120
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 121
    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v0

    mul-double/2addr v4, v4

    mul-double/2addr v2, v4

    sub-double v4, p4, p2

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v6, v0

    mul-double/2addr v4, v6

    mul-double/2addr v4, v0

    sub-double v6, p6, p4

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    .line 123
    mul-double/2addr v0, v0

    mul-double/2addr v0, v4

    sub-double v4, p8, p6

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(DDD)V
    .locals 13

    .prologue
    .line 160
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v8, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide v0, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v10

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v8, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide v0, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v4

    move-object v1, p0

    move-wide v2, v10

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    .line 161
    return-void
.end method

.method public final c(DDDD)V
    .locals 7

    .prologue
    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    .line 144
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    .line 145
    iput-wide p5, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    .line 148
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    div-double v2, p3, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    .line 151
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    div-double v2, p7, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    .line 152
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 209
    if-ne p0, p1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 212
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/a;

    if-eqz v2, :cond_3

    .line 213
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/a;

    .line 214
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 216
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 221
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
