.class public Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    .line 21
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    .line 32
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a(Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    .line 21
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    .line 37
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a(Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const v3, 0x7fffffff

    .line 41
    if-eqz p1, :cond_1

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->h:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    sget v1, Lcom/google/android/apps/gmm/n;->i:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    if-eq v2, v1, :cond_0

    if-lez v1, :cond_0

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->requestLayout()V

    .line 46
    :cond_0
    sget v1, Lcom/google/android/apps/gmm/n;->j:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    if-eq v1, v0, :cond_1

    if-lez v0, :cond_1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->requestLayout()V

    .line 50
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->setOrientation(I)V

    .line 51
    return-void
.end method


# virtual methods
.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 76
    instance-of v0, p1, Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 77
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 89
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 82
    check-cast v0, Landroid/widget/TextView;

    .line 83
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->c:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 84
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 86
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->c:I

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 87
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->c:I

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    sub-int v0, v1, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->c:I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->c:I

    .line 70
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 71
    return-void
.end method
