.class public Lcom/google/android/apps/gmm/map/internal/c/cg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/bi;

.field public final c:I

.field private final d:J

.field private final e:I

.field private final f:I

.field private final g:[I


# direct methods
.method private constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/internal/c/bi;III[I)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->d:J

    .line 67
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->e:I

    .line 68
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->f:I

    .line 69
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 70
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 71
    iput p9, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->c:I

    .line 74
    iput-object p10, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->g:[I

    .line 75
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/ay;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    const/4 v2, 0x1

    .line 95
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v4, 0xc

    if-lt v3, v4, :cond_0

    .line 96
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 98
    :cond_0
    new-array v15, v2, [Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 99
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 100
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v4

    aput-object v4, v15, v3

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 104
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v4

    new-instance v16, Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v3}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 106
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v11

    .line 107
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v12

    .line 110
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 111
    new-array v13, v4, [I

    .line 112
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    .line 113
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v13, v3

    .line 112
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 116
    :cond_2
    const/4 v3, 0x0

    move v14, v3

    :goto_2
    if-ge v14, v2, :cond_3

    .line 117
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/cg;

    .line 118
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v4, v4

    .line 119
    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 120
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    aget-object v8, v15, v14

    .line 122
    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 123
    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/gmm/map/internal/c/cg;-><init>(JIILcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/internal/c/bi;III[I)V

    .line 117
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    goto :goto_2

    .line 128
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 184
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->d:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_0
    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->e:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->f:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 174
    const/16 v0, 0x9

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->g:[I

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    add-int/lit16 v0, v0, 0xa0

    add-int/lit8 v1, v0, 0x2c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 210
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cg;->b:Lcom/google/android/apps/gmm/map/internal/c/bi;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/c/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0x2

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x28

    goto :goto_0
.end method
