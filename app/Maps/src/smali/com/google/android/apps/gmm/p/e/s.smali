.class public Lcom/google/android/apps/gmm/p/e/s;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/p/e/a/g;

.field private b:Lcom/google/android/apps/gmm/map/b/a/ab;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)Lcom/google/android/apps/gmm/map/b/a/ad;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 150
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/p/e/t;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/gmm/p/e/t;-><init>(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/t;)V

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_0
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 153
    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/p/e/t;

    .line 154
    iget-object v0, v1, Lcom/google/android/apps/gmm/p/e/t;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    if-ne v0, p1, :cond_1

    .line 155
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/p/e/t;->a()Lcom/google/android/apps/gmm/map/b/a/ad;

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    .line 157
    :cond_1
    iget v0, v1, Lcom/google/android/apps/gmm/p/e/t;->c:I

    const/4 v4, 0x6

    if-ge v0, v4, :cond_0

    .line 158
    iget-object v0, v1, Lcom/google/android/apps/gmm/p/e/t;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/e;

    .line 159
    new-instance v5, Lcom/google/android/apps/gmm/p/e/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/gmm/p/e/t;-><init>(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/t;)V

    .line 160
    iget v0, v5, Lcom/google/android/apps/gmm/p/e/t;->d:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v6, 0x43870000    # 270.0f

    cmpg-float v0, v0, v6

    if-gtz v0, :cond_2

    .line 161
    invoke-virtual {v3, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 166
    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/ad;Lcom/google/android/apps/gmm/p/e/a/g;)V
    .locals 10

    .prologue
    .line 136
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    const/4 v0, 0x6

    if-ge v6, v0, :cond_4

    .line 137
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/e;

    iget-object v8, v0, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    const-wide/16 v2, 0x0

    iget-boolean v9, v8, Lcom/google/android/apps/gmm/p/e/a/g;->l:Z

    if-eqz v9, :cond_2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    :cond_0
    :goto_2
    iget-object v9, p1, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-object v8, v8, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    if-ne v9, v8, :cond_1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v8

    :cond_1
    cmpl-double v8, v2, v4

    if-lez v8, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    :goto_3
    move-object v1, v0

    move-wide v4, v2

    goto :goto_1

    :cond_2
    iget-boolean v9, v8, Lcom/google/android/apps/gmm/p/e/a/g;->m:Z

    if-eqz v9, :cond_0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    goto :goto_2

    .line 138
    :cond_3
    if-eqz v1, :cond_4

    .line 139
    iget-object v0, v1, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 136
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object p1, v1

    goto :goto_0

    .line 143
    :cond_4
    return-void

    :cond_5
    move-object v0, v1

    move-wide v2, v4

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/map/r/b/c;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-nez p1, :cond_0

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 120
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    if-eq v0, p1, :cond_6

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/p/e/s;->a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)Lcom/google/android/apps/gmm/map/b/a/ad;

    move-result-object v3

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/p/e/s;->a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)Lcom/google/android/apps/gmm/map/b/a/ad;

    move-result-object v4

    .line 71
    if-eqz v3, :cond_9

    move v6, v1

    .line 72
    :goto_1
    if-eqz v4, :cond_a

    move v0, v1

    .line 73
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 74
    iget v5, v5, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    iget v7, p1, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    .line 73
    sub-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v7, 0x43340000    # 180.0f

    cmpl-float v7, v5, v7

    if-lez v7, :cond_1

    const/high16 v7, 0x43b40000    # 360.0f

    sub-float v5, v7, v5

    :cond_1
    const/high16 v7, 0x41f00000    # 30.0f

    cmpg-float v5, v5, v7

    if-gtz v5, :cond_b

    move v5, v1

    .line 77
    :goto_3
    if-nez v6, :cond_2

    if-eqz v0, :cond_c

    if-eqz v5, :cond_c

    .line 79
    :cond_2
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v1, :cond_3

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_3
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    .line 80
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v1, :cond_4

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_4
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    .line 93
    :goto_4
    if-eqz v6, :cond_13

    if-eqz v0, :cond_13

    .line 94
    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    iget v1, v4, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-gt v0, v1, :cond_12

    move-object v0, v3

    :goto_5
    move-object v3, v0

    .line 107
    :cond_5
    :goto_6
    if-ne v3, v4, :cond_15

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    :goto_7
    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/p/e/s;->a(Lcom/google/android/apps/gmm/map/b/a/ad;Lcom/google/android/apps/gmm/p/e/a/g;)V

    .line 109
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 112
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    if-nez v0, :cond_7

    .line 113
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    iget-object v1, p1, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    iget-object v1, p1, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 114
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/p/e/s;->a(Lcom/google/android/apps/gmm/map/b/a/ad;Lcom/google/android/apps/gmm/p/e/a/g;)V

    .line 115
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 118
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/s;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v1, :cond_8

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_8
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/e;->l:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 119
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/s;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    goto/16 :goto_0

    :cond_9
    move v6, v2

    .line 71
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 72
    goto/16 :goto_2

    :cond_b
    move v5, v2

    .line 73
    goto :goto_3

    .line 81
    :cond_c
    if-eqz v0, :cond_f

    .line 83
    iget-object v5, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_d

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v5, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_d
    iget-object v5, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v1, v5, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    .line 84
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v1, :cond_e

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_e
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    goto :goto_4

    .line 87
    :cond_f
    iget-object v5, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_10

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v5, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_10
    iget-object v5, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v2, v5, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    .line 88
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v2, :cond_11

    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v2, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_11
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    goto/16 :goto_4

    :cond_12
    move-object v0, v4

    .line 94
    goto/16 :goto_5

    .line 96
    :cond_13
    if-nez v6, :cond_5

    .line 98
    if-eqz v0, :cond_14

    move-object v3, v4

    .line 99
    goto/16 :goto_6

    .line 102
    :cond_14
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    iget-object v1, p1, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    iget-object v1, p1, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-object v3, v0

    goto/16 :goto_6

    :cond_15
    move-object v0, p1

    .line 107
    goto/16 :goto_7
.end method
