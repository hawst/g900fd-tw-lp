.class public Lcom/google/android/apps/gmm/map/internal/c/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/google/android/apps/gmm/map/b/a/j;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/ax;

.field public final e:[B

.field public final f:[I

.field public final g:[I

.field public final h:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:I

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Lcom/google/android/apps/gmm/map/indoor/d/a;

.field public final p:Lcom/google/android/apps/gmm/map/indoor/d/g;

.field public final q:[I

.field public final r:Z

.field private final t:J


# direct methods
.method private constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ax;[B[I[ILcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[ILcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/g;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->t:J

    .line 107
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->a:I

    .line 108
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->b:I

    .line 109
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 110
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 111
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    .line 112
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->f:[I

    .line 113
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    .line 114
    iput-object p10, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 115
    iput p11, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->i:I

    .line 116
    iput-object p12, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->j:Ljava/lang/String;

    .line 117
    iput p13, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->k:I

    .line 118
    iput p14, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->l:I

    .line 119
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->q:[I

    .line 120
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->o:Lcom/google/android/apps/gmm/map/indoor/d/a;

    .line 121
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->p:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 122
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->m:Ljava/lang/String;

    .line 123
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->n:Ljava/lang/String;

    .line 124
    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->r:Z

    .line 125
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/e;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/h;
    .locals 24

    .prologue
    .line 202
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/e;->a()Lcom/google/maps/b/a/bo;

    move-result-object v4

    .line 204
    invoke-virtual {v4}, Lcom/google/maps/b/a/bo;->a()Lcom/google/maps/b/a/cu;

    move-result-object v2

    .line 205
    iget-object v3, v4, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    .line 206
    iget-object v5, v4, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    iget v6, v5, Lcom/google/maps/b/a/cy;->b:I

    new-array v6, v6, [I

    iget-object v7, v5, Lcom/google/maps/b/a/cy;->a:[I

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v5, v5, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v7, v8, v6, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    iget-object v5, v4, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iget v5, v5, Lcom/google/maps/b/a/cz;->b:I

    .line 208
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 203
    invoke-static {v2, v3, v6, v5, v7}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Lcom/google/maps/b/a/cu;I[IILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;

    move-result-object v9

    .line 209
    const/4 v2, 0x0

    .line 210
    const/4 v12, 0x0

    .line 211
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/e;->a:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_d

    .line 212
    iget-object v2, v4, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iget v2, v2, Lcom/google/maps/b/a/cy;->b:I

    if-lez v2, :cond_5

    .line 213
    iget-object v3, v4, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iget v2, v3, Lcom/google/maps/b/a/cy;->b:I

    new-array v2, v2, [I

    iget-object v5, v3, Lcom/google/maps/b/a/cy;->a:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v5, v6, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 217
    :goto_1
    iget-object v3, v4, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    if-lez v3, :cond_6

    .line 218
    iget-object v3, v4, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iget v4, v3, Lcom/google/maps/b/a/cy;->b:I

    new-array v12, v4, [I

    iget-object v4, v3, Lcom/google/maps/b/a/cy;->a:[I

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v4, v5, v12, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v11, v2

    .line 224
    :goto_2
    const/4 v2, 0x0

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/e;->h:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_0

    .line 226
    const/4 v2, 0x4

    .line 229
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/e;->b:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v15

    .line 230
    iget-object v3, v15, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v3, v3

    if-nez v3, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v13

    .line 232
    :goto_4
    const/16 v19, 0x0

    .line 233
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/e;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v3, :cond_c

    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/e;->d()Lcom/google/maps/b/a/ab;

    move-result-object v3

    const-wide/16 v4, -0x1

    .line 234
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(Lcom/google/maps/b/a/ab;J)Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-result-object v19

    .line 236
    or-int/lit8 v17, v2, 0x8

    .line 238
    :goto_5
    const/16 v20, 0x0

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/e;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_1

    .line 240
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/e;->e()Lcom/google/maps/b/a/ag;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/indoor/d/g;->a(Lcom/google/maps/b/a/ag;)Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-result-object v20

    .line 243
    :cond_1
    const/4 v8, 0x0

    .line 244
    const-string v2, ""

    .line 245
    const-string v3, ""

    .line 246
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/b/a/e;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v4, v4, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v4, :cond_b

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/e;->b()Lcom/google/maps/b/a/ai;

    move-result-object v4

    .line 248
    iget-object v5, v4, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v5, v5, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v5, :cond_2

    .line 249
    invoke-virtual {v4}, Lcom/google/maps/b/a/ai;->b()Ljava/lang/String;

    move-result-object v2

    .line 251
    :cond_2
    iget-object v5, v4, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v5, v5, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v5, :cond_3

    .line 252
    invoke-virtual {v4}, Lcom/google/maps/b/a/ai;->a()Ljava/lang/String;

    move-result-object v3

    .line 254
    :cond_3
    iget-object v5, v4, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-boolean v5, v5, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v5, :cond_b

    .line 255
    iget-object v5, v4, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-boolean v5, v5, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v5, :cond_9

    .line 256
    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v5, v4, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v6, v5, Lcom/google/maps/b/a/db;->b:J

    iget-object v4, v4, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-wide v4, v4, Lcom/google/maps/b/a/db;->b:J

    invoke-direct {v8, v6, v7, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    move-object/from16 v22, v3

    move-object/from16 v21, v2

    .line 265
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/e;->d:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v3, -0x80000000

    xor-int v7, v2, v3

    .line 266
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/h;

    const-wide/16 v4, 0x0

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/e;->c:Lcom/google/maps/b/a/cz;

    iget v6, v2, Lcom/google/maps/b/a/cz;->b:I

    const/4 v10, 0x0

    const/4 v14, 0x0

    .line 277
    iget-object v15, v15, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    const/16 v16, 0x0

    const/4 v2, 0x0

    new-array v0, v2, [I

    move-object/from16 v18, v0

    .line 285
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v23, Lcom/google/android/apps/gmm/map/internal/c/bv;->g:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v23

    aget-object v2, v2, v23

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    invoke-direct/range {v3 .. v23}, Lcom/google/android/apps/gmm/map/internal/c/h;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ax;[B[I[ILcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[ILcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/g;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v3

    .line 211
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 215
    :cond_5
    const/4 v2, 0x0

    new-array v2, v2, [I

    goto/16 :goto_1

    .line 220
    :cond_6
    const/4 v3, 0x0

    new-array v12, v3, [I

    move-object v11, v2

    goto/16 :goto_2

    .line 225
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 230
    :cond_8
    iget-object v3, v15, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v4, 0x0

    aget-object v13, v3, v4

    goto/16 :goto_4

    .line 258
    :cond_9
    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v4, v4, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v4, v4, Lcom/google/maps/b/a/db;->b:J

    invoke-direct {v8, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(J)V

    move-object/from16 v22, v3

    move-object/from16 v21, v2

    goto :goto_6

    .line 285
    :cond_a
    const/4 v2, 0x0

    goto :goto_7

    :cond_b
    move-object/from16 v22, v3

    move-object/from16 v21, v2

    goto :goto_6

    :cond_c
    move/from16 v17, v2

    goto/16 :goto_5

    :cond_d
    move-object v11, v2

    goto/16 :goto_2
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/h;
    .locals 24

    .prologue
    .line 143
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;

    move-result-object v9

    .line 145
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    :goto_0
    new-array v10, v2, [B

    .line 147
    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Ljava/io/DataInput;->readFully([B)V

    .line 150
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    new-instance v15, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v15, v3, v2}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 153
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v16

    .line 155
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v17

    .line 158
    const/4 v8, 0x0

    .line 159
    and-int/lit8 v2, v17, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 160
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    .line 166
    :cond_0
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    .line 167
    new-array v0, v3, [I

    move-object/from16 v18, v0

    .line 168
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_5

    .line 169
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v18, v2

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 145
    :cond_1
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_0

    .line 159
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 161
    :cond_3
    and-int/lit8 v2, v17, 0x2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_0

    .line 162
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->e()Lcom/google/android/apps/gmm/map/b/a/k;

    move-result-object v8

    goto :goto_2

    .line 161
    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    .line 172
    :cond_5
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->b:Lcom/google/android/apps/gmm/map/internal/c/n;

    .line 173
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_6
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/n;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-object/from16 v19, v0

    .line 174
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_7
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/n;->e:Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-object/from16 v20, v0

    .line 176
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/h;

    .line 177
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v4, v2

    .line 178
    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 179
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 185
    iget-object v2, v15, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v2, :cond_8

    const/4 v13, 0x0

    .line 186
    :goto_5
    iget v14, v15, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    .line 187
    iget-object v2, v15, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v2, :cond_a

    const/4 v15, 0x0

    :goto_6
    const-string v21, ""

    const-string v22, ""

    const/16 v23, 0x0

    invoke-direct/range {v3 .. v23}, Lcom/google/android/apps/gmm/map/internal/c/h;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ax;[B[I[ILcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[ILcom/google/android/apps/gmm/map/indoor/d/a;Lcom/google/android/apps/gmm/map/indoor/d/g;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v3

    .line 185
    :cond_8
    iget-object v2, v15, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v13, v13

    if-nez v13, :cond_9

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v13

    goto :goto_5

    :cond_9
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v13, 0x0

    aget-object v13, v2, v13

    goto :goto_5

    .line 187
    :cond_a
    iget-object v2, v15, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v15, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_6
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 290
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->t:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->a:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->b:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    .line 336
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/l/k;->a(Lcom/google/android/apps/gmm/map/b/a/ax;[B)Ljava/util/List;

    move-result-object v0

    .line 339
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->f:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->g:[I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ax;->a([I[I)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x3

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 367
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->k:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->q:[I

    return-object v0
.end method

.method public final k()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->d:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1c

    add-int/lit8 v0, v0, 0x3c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->e:[B

    array-length v2, v2

    add-int v3, v0, v2

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->j:Ljava/lang/String;

    .line 415
    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/h;->h:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 416
    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    add-int/2addr v0, v3

    .line 417
    return v0

    .line 413
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v0, v0

    goto :goto_0

    .line 414
    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->d()I

    move-result v0

    goto :goto_1

    .line 415
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x4

    shl-int/lit8 v2, v2, 0x2

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x28

    goto :goto_2

    .line 416
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v1

    goto :goto_3
.end method
