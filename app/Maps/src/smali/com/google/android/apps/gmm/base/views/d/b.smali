.class public Lcom/google/android/apps/gmm/base/views/d/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/base/views/d/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/views/d/b;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Runnable;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-static {p0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    .line 97
    if-eqz p2, :cond_0

    .line 98
    new-instance v0, Lcom/google/android/apps/gmm/base/views/d/c;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/base/views/d/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 127
    :goto_0
    return-object v0

    .line 107
    :cond_0
    const-string v0, "https://plus.google.com/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v0, 0x2f

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :cond_1
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 108
    :goto_1
    if-eqz v2, :cond_3

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/base/views/d/d;

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/gmm/base/views/d/d;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 107
    goto :goto_1

    .line 122
    :cond_3
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p1, v0}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/base/views/d/e;

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/gmm/base/views/d/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/content/Intent;)V

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/d/b;->a:Ljava/lang/String;

    const-string v0, "Failed to parse profile link: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move-object v0, v1

    .line 125
    goto :goto_0

    .line 124
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method
