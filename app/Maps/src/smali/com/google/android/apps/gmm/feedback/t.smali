.class Lcom/google/android/apps/gmm/feedback/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/d/e;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field final b:Lcom/google/android/apps/gmm/feedback/c/a;

.field private final c:Ljava/lang/Runnable;

.field private final d:Lcom/google/android/apps/gmm/base/l/a/n;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;Lcom/google/android/apps/gmm/feedback/c/a;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/t;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/t;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/gmm/feedback/t;->c:Ljava/lang/Runnable;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/feedback/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/feedback/u;-><init>(Lcom/google/android/apps/gmm/feedback/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/t;->d:Lcom/google/android/apps/gmm/base/l/a/n;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/l/a/n;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/t;->d:Lcom/google/android/apps/gmm/base/l/a/n;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/feedback/c/b;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/t;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    iput-object p1, v0, Lcom/google/android/apps/gmm/feedback/c/a;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/t;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/feedback/c/b;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/t;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/c/a;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    if-eq v0, p1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
