.class public Lcom/google/android/apps/gmm/cardui/f/e;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/o/h/a/ph;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 5
    .param p0    # Lcom/google/o/h/a/ph;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Landroid/content/Context;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20
    if-nez p0, :cond_0

    .line 21
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 23
    :cond_0
    iget-boolean v0, p0, Lcom/google/o/h/a/ph;->i:Z

    if-eqz v0, :cond_1

    .line 24
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_0

    .line 26
    :cond_1
    invoke-virtual {p0}, Lcom/google/o/h/a/ph;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 27
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 26
    goto :goto_1

    .line 29
    :cond_4
    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 30
    invoke-static {p0}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 31
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_9

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 32
    invoke-virtual {p0}, Lcom/google/o/h/a/ph;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 34
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 35
    iget-object v0, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 37
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_7

    .line 38
    iget-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 40
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_c

    move v0, v1

    :goto_5
    if-eqz v0, :cond_8

    .line 41
    iget-object v0, p0, Lcom/google/o/h/a/ph;->n:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->c()[B

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/aq;->h:[B

    .line 43
    :cond_8
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 31
    goto :goto_2

    :cond_a
    move v0, v2

    .line 34
    goto :goto_3

    :cond_b
    move v0, v2

    .line 37
    goto :goto_4

    :cond_c
    move v0, v2

    .line 40
    goto :goto_5
.end method

.method public static a(Lcom/google/o/h/a/ph;)Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 52
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    .line 53
    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_7

    :cond_1
    move v4, v2

    :goto_2
    if-nez v4, :cond_8

    move v5, v2

    .line 54
    :goto_3
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_9

    :cond_2
    move v4, v2

    :goto_4
    if-nez v4, :cond_a

    .line 55
    :goto_5
    if-eqz v5, :cond_b

    if-eqz v2, :cond_b

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " loc:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 63
    :cond_3
    :goto_6
    return-object v1

    .line 51
    :cond_4
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    goto :goto_0

    .line 52
    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v4, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    :cond_6
    move-object v0, v4

    goto :goto_1

    :cond_7
    move v4, v3

    .line 53
    goto :goto_2

    :cond_8
    move v5, v3

    goto :goto_3

    :cond_9
    move v4, v3

    .line 54
    goto :goto_4

    :cond_a
    move v2, v3

    goto :goto_5

    .line 58
    :cond_b
    if-nez v5, :cond_3

    .line 60
    if-eqz v2, :cond_c

    move-object v1, v0

    .line 61
    goto :goto_6

    .line 63
    :cond_c
    const/4 v1, 0x0

    goto :goto_6
.end method
