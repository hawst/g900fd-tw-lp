.class public Lcom/google/android/apps/gmm/map/e/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/e/a/b;

.field private final b:Lcom/google/android/apps/gmm/map/e/a/a;


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/e/a/e;Ljava/lang/String;BLjava/lang/Object;)V
    .locals 6

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/e/a;->b:Lcom/google/android/apps/gmm/map/e/a/a;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 59
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v4

    .line 60
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/e/a/e;->i:Ljava/lang/String;

    .line 58
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/e/a/a;->b:J

    iput-wide v4, v0, Lcom/google/android/apps/gmm/map/e/a/a;->c:J

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/e/a/a;->d:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/android/apps/gmm/map/e/a/a;->e:Ljava/lang/String;

    iput-byte p3, v0, Lcom/google/android/apps/gmm/map/e/a/a;->f:B

    iput-object p4, v0, Lcom/google/android/apps/gmm/map/e/a/a;->g:Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/e/a;->a:Lcom/google/android/apps/gmm/map/e/a/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/e/a;->b:Lcom/google/android/apps/gmm/map/e/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/e/a/b;->a(Lcom/google/android/apps/gmm/map/e/a/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
