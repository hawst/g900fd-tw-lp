.class public final enum Lcom/google/android/apps/gmm/k/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/k/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/k/f;

.field public static final enum b:Lcom/google/android/apps/gmm/k/f;

.field public static final enum c:Lcom/google/android/apps/gmm/k/f;

.field public static final enum d:Lcom/google/android/apps/gmm/k/f;

.field public static final enum e:Lcom/google/android/apps/gmm/k/f;

.field public static final enum f:Lcom/google/android/apps/gmm/k/f;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/k/f;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/k/f;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->a:Lcom/google/android/apps/gmm/k/f;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/k/f;

    const-string v1, "FETCHING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->b:Lcom/google/android/apps/gmm/k/f;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/k/f;

    const-string v1, "SHOWING_ENTRYPOINT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->c:Lcom/google/android/apps/gmm/k/f;

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/k/f;

    const-string v1, "SHOWING_SURVEY"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->d:Lcom/google/android/apps/gmm/k/f;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/k/f;

    const-string v1, "SHOWING_THANKS"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->e:Lcom/google/android/apps/gmm/k/f;

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/k/f;

    const-string v1, "DISMISSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/k/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    .line 61
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/k/f;

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->a:Lcom/google/android/apps/gmm/k/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->b:Lcom/google/android/apps/gmm/k/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->c:Lcom/google/android/apps/gmm/k/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->d:Lcom/google/android/apps/gmm/k/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/k/f;->e:Lcom/google/android/apps/gmm/k/f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/k/f;->g:[Lcom/google/android/apps/gmm/k/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/k/f;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/gmm/k/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/k/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/k/f;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/k/f;->g:[Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/k/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/k/f;

    return-object v0
.end method
