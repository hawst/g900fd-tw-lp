.class public Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGpsStatusChanged(I)V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->e:Landroid/location/GpsStatus;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->e:Landroid/location/GpsStatus;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->e:Landroid/location/GpsStatus;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->fromGpsStatus(Landroid/location/GpsStatus;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/SatelliteStatusEvent;->getNumUsedInFix()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onGpsStatusChanged("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 37
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onLocationChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    if-eqz p1, :cond_1

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;->a:Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->f:J

    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 41
    :cond_1
    return-void

    .line 39
    :cond_2
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->f:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->f:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->a:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onProviderDisabled("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x13

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onProviderEnabled("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1e

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onStatusChanged("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    return-void
.end method
