.class public Lcom/google/android/apps/gmm/map/t/ar;
.super Lcom/google/android/apps/gmm/map/t/p;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/v/bt;

.field private B:[I

.field final a:I

.field final d:Lcom/google/android/apps/gmm/map/t/as;

.field public e:F

.field private f:Lcom/google/android/apps/gmm/v/cj;

.field private g:Lcom/google/android/apps/gmm/v/cj;

.field private final h:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private final i:Lcom/google/android/apps/gmm/map/b/a/ae;

.field private final j:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final k:Lcom/google/android/apps/gmm/map/b/a/y;

.field private z:[F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V
    .locals 8

    .prologue
    const/16 v3, 0x38

    const/16 v5, 0x1e00

    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/t/p;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->f:Lcom/google/android/apps/gmm/v/cj;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->g:Lcom/google/android/apps/gmm/v/cj;

    .line 78
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    .line 94
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->e:F

    .line 109
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    .line 110
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/t/ar;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 111
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->i:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->i:Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->i:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 114
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/t/ar;->d:Lcom/google/android/apps/gmm/map/t/as;

    .line 115
    if-nez p3, :cond_0

    const-class v0, Lcom/google/android/apps/gmm/map/t/j;

    .line 116
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GmmTileEntity in BaseTileDrawOrder does not specify sort values."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    if-eqz p4, :cond_1

    .line 121
    new-instance v0, Lcom/google/android/apps/gmm/v/bt;

    const/16 v1, 0x202

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/ar;->b()I

    move-result v2

    move v4, v3

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/v/bt;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->A:Lcom/google/android/apps/gmm/v/bt;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->A:Lcom/google/android/apps/gmm/v/bt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 131
    :cond_1
    return-void

    .line 89
    nop

    :array_0
    .array-data 4
        0x1
        0x1
        0x1
    .end array-data
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 4

    .prologue
    .line 139
    const/high16 v0, 0x40000000    # 2.0f

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int v1, v0, v1

    .line 140
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 142
    if-gtz v0, :cond_0

    move v0, v1

    .line 143
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    const/4 v3, 0x0

    aput v0, v2, v3

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    const/4 v3, 0x1

    aput v0, v2, v3

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    const/4 v2, 0x2

    aput v1, v0, v2

    .line 146
    return-void

    .line 142
    :cond_0
    shr-int v0, v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 175
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->n:Z

    if-nez v0, :cond_0

    iget v0, p3, Lcom/google/android/apps/gmm/v/n;->I:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->p:[I

    iget v2, p2, Lcom/google/android/apps/gmm/v/bh;->g:I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    :cond_0
    move-object v1, p3

    .line 177
    check-cast v1, Lcom/google/android/apps/gmm/map/f/o;

    .line 180
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ar;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/ar;->i:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 181
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v4, v5, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    .line 180
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v6}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    aget v1, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    aget v2, v2, v8

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    aget v3, v3, v9

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v5, 0xc

    aput v1, v4, v5

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v4, 0xd

    aput v2, v1, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v1, 0xe

    aput v3, v0, v1

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    aget v1, v1, v7

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->e:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    aget v2, v2, v6

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    aget v2, v2, v7

    iget v3, p0, Lcom/google/android/apps/gmm/map/t/ar;->e:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    aget v3, v3, v8

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ar;->z:[F

    aget v3, v3, v7

    iget v4, p0, Lcom/google/android/apps/gmm/map/t/ar;->e:F

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    aget v4, v4, v9

    int-to-float v4, v4

    div-float/2addr v3, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v6, v1, v2, v3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->f:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ar;->g:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v4, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    move v1, v6

    move v3, v6

    move v5, v6

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 193
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V

    .line 194
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/cj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 201
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->t:Z

    if-eqz v0, :cond_0

    .line 202
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->g:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v2, 0x10

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/ar;->n:Z

    .line 206
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 222
    const/4 v0, 0x0

    .line 224
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    .line 225
    const/16 v0, 0x8

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_1

    .line 229
    or-int/lit8 v0, v0, 0x10

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->h:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_2

    .line 233
    or-int/lit8 v0, v0, 0x20

    .line 235
    :cond_2
    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 3

    .prologue
    .line 154
    const/high16 v0, 0x40000000    # 2.0f

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int/2addr v0, v1

    .line 155
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 157
    if-gtz v1, :cond_0

    .line 158
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    const/4 v2, 0x0

    aput v0, v1, v2

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    const/4 v2, 0x1

    aput v0, v1, v2

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ar;->B:[I

    const/4 v2, 0x2

    aput v0, v1, v2

    .line 161
    return-void

    .line 157
    :cond_0
    shr-int/2addr v0, v1

    goto :goto_0
.end method
