.class Lcom/google/android/apps/gmm/startpage/f/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/startpage/f/i;

.field private b:Lcom/google/android/libraries/curvular/ce;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/startpage/f/i;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/util/b/h;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->e:Lcom/google/android/apps/gmm/util/b/h;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Lcom/google/android/libraries/curvular/ce;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->b:Lcom/google/android/libraries/curvular/ce;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->g:Lcom/google/android/apps/gmm/cardui/g/c;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->g:Lcom/google/android/apps/gmm/cardui/g/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/cardui/g/c;->c()V

    .line 162
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->e:Lcom/google/android/apps/gmm/util/b/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/h;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->e:Lcom/google/android/apps/gmm/util/b/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/h;->a()Ljava/util/List;

    move-result-object v0

    .line 137
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/q;->b()Lcom/google/o/h/a/bw;

    move-result-object v0

    sget-object v3, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/f/i;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->a:Lcom/google/android/apps/gmm/startpage/f/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/f/i;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->i:Lcom/google/android/apps/gmm/startpage/e/g;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e()Lcom/google/android/libraries/curvular/ce;
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/startpage/f/n;-><init>(Lcom/google/android/apps/gmm/startpage/f/m;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->b:Lcom/google/android/libraries/curvular/ce;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/m;->b:Lcom/google/android/libraries/curvular/ce;

    return-object v0
.end method
