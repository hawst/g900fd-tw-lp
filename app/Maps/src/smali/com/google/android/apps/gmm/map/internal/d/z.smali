.class public Lcom/google/android/apps/gmm/map/internal/d/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/ac;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field c:I


# direct methods
.method constructor <init>(Lcom/google/maps/b/a;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/z;->a:Ljava/util/Map;

    .line 72
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    move v1, v0

    .line 77
    :goto_0
    iget-object v0, p1, Lcom/google/maps/b/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 78
    iget-object v0, p1, Lcom/google/maps/b/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/cw;->g()Lcom/google/maps/b/cw;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/cw;

    .line 79
    iget v2, v0, Lcom/google/maps/b/cw;->b:I

    invoke-static {v2}, Lcom/google/maps/b/cz;->a(I)Lcom/google/maps/b/cz;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/maps/b/cz;->a:Lcom/google/maps/b/cz;

    :cond_0
    iget v2, v2, Lcom/google/maps/b/cz;->n:I

    .line 80
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/w;->b:[Lcom/google/android/apps/gmm/map/internal/c/ac;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 81
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/z;->a:Ljava/util/Map;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/w;->b:[Lcom/google/android/apps/gmm/map/internal/c/ac;

    aget-object v2, v4, v2

    invoke-virtual {v0}, Lcom/google/maps/b/cw;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 84
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/z;->c:I

    .line 85
    return-void
.end method
