.class public Lcom/google/android/apps/gmm/suggest/e/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:I

.field public e:I

.field f:Lcom/google/android/apps/gmm/map/b/a/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field g:Lcom/google/android/apps/gmm/map/b/a/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public h:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field l:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public m:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public n:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public o:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public p:[B
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public r:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/e/e;->d:I

    .line 52
    iput v0, p0, Lcom/google/android/apps/gmm/suggest/e/e;->e:I

    .line 53
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 54
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 55
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    .line 56
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/e;->i:Lcom/google/b/c/cv;

    .line 57
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->j:Ljava/lang/Integer;

    .line 58
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->k:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->l:Ljava/lang/Integer;

    .line 60
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->m:Lcom/google/e/a/a/a/b;

    .line 61
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->n:Lcom/google/e/a/a/a/b;

    .line 62
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->o:Lcom/google/e/a/a/a/b;

    .line 63
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->p:[B

    .line 64
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->q:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/e;->r:Lcom/google/e/a/a/a/b;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/suggest/e/d;
    .locals 20

    .prologue
    .line 175
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    .line 177
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->b:Ljava/lang/String;

    .line 179
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    .line 181
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    if-nez v1, :cond_2

    .line 182
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    .line 184
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->i:Lcom/google/b/c/cv;

    if-nez v1, :cond_3

    .line 185
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/suggest/e/e;->i:Lcom/google/b/c/cv;

    .line 187
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/suggest/e/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/suggest/e/e;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/suggest/e/e;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/suggest/e/e;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/suggest/e/e;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/suggest/e/e;->e:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/suggest/e/e;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/suggest/e/e;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/suggest/e/e;->i:Lcom/google/b/c/cv;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/suggest/e/e;->j:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/suggest/e/e;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/suggest/e/e;->l:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/suggest/e/e;->m:Lcom/google/e/a/a/a/b;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/suggest/e/e;->n:Lcom/google/e/a/a/a/b;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/e;->o:Lcom/google/e/a/a/a/b;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/e;->p:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/e;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/e;->r:Lcom/google/e/a/a/a/b;

    move-object/from16 v19, v0

    invoke-direct/range {v1 .. v19}, Lcom/google/android/apps/gmm/suggest/e/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/b/c/cv;Lcom/google/b/c/cv;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;[BLjava/lang/String;Lcom/google/e/a/a/a/b;)V

    return-object v1

    .line 175
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
