.class public Lcom/google/android/apps/gmm/p/f/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/p/b/g;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field final b:Lcom/google/android/apps/gmm/p/b/i;

.field final c:Lcom/google/android/apps/gmm/p/b/i;

.field d:Lcom/google/android/apps/gmm/p/f/e;

.field final e:Lcom/google/android/apps/gmm/p/f/f;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/apps/gmm/p/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/p/b/h;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/google/android/apps/gmm/p/b/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/p/b/i;Lcom/google/android/apps/gmm/p/b/i;)V
    .locals 3

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->f:Ljava/util/Map;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z

    .line 138
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/f/d;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 139
    iput-object p2, p0, Lcom/google/android/apps/gmm/p/f/d;->c:Lcom/google/android/apps/gmm/p/b/i;

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/p/f/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/f/e;-><init>(Lcom/google/android/apps/gmm/p/f/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->d:Lcom/google/android/apps/gmm/p/f/e;

    .line 141
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/p/f/d;->a(Lcom/google/android/apps/gmm/p/b/i;)V

    .line 143
    iput-object p3, p0, Lcom/google/android/apps/gmm/p/f/d;->b:Lcom/google/android/apps/gmm/p/b/i;

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/p/f/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/f/f;-><init>(Lcom/google/android/apps/gmm/p/f/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->e:Lcom/google/android/apps/gmm/p/f/f;

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->e:Lcom/google/android/apps/gmm/p/f/f;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 150
    const-class v0, Lcom/google/android/apps/gmm/p/b/h;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    return-void
.end method

.method private b()Lcom/google/android/apps/gmm/p/b/h;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    .line 230
    :goto_0
    return-object v0

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 228
    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    goto :goto_0

    .line 230
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/b/h;

    .line 203
    if-eqz v0, :cond_0

    .line 204
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 206
    :cond_0
    return-void
.end method


# virtual methods
.method declared-synchronized a(Lcom/google/android/apps/gmm/p/b/i;)V
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    if-eq p1, v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    if-eqz v0, :cond_1

    .line 181
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/i;->b()V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/d;->d:Lcom/google/android/apps/gmm/p/f/e;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/p/b/i;->b(Lcom/google/android/apps/gmm/p/b/j;)V

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->d:Lcom/google/android/apps/gmm/p/f/e;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/p/b/i;->a(Lcom/google/android/apps/gmm/p/b/j;)V

    .line 187
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z

    if-eqz v0, :cond_2

    .line 188
    invoke-interface {p1}, Lcom/google/android/apps/gmm/p/b/i;->a()V

    .line 190
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/f/d;->b()Lcom/google/android/apps/gmm/p/b/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/h;->c:Lcom/google/android/apps/gmm/p/b/l;

    .line 189
    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/p/b/i;->a(Lcom/google/android/apps/gmm/p/b/l;)V

    .line 192
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_3
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 255
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 267
    :goto_0
    monitor-exit p0

    return-void

    .line 258
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/p/f/d;->b(Ljava/lang/Object;)V

    .line 259
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/f/d;->b()Lcom/google/android/apps/gmm/p/b/h;

    move-result-object v0

    .line 260
    if-nez v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/i;->b()V

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 265
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/h;->c:Lcom/google/android/apps/gmm/p/b/l;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/p/b/i;->a(Lcom/google/android/apps/gmm/p/b/l;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Lcom/google/android/apps/gmm/p/b/h;)V
    .locals 2

    .prologue
    .line 236
    monitor-enter p0

    .line 237
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    if-ne p2, v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->p:Z

    if-eqz v0, :cond_0

    .line 238
    sget-object p2, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 241
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/p/f/d;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z

    if-nez v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/i;->a()V

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/f/d;->h:Z

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    .line 249
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/f/d;->b()Lcom/google/android/apps/gmm/p/b/h;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/p/b/h;->c:Lcom/google/android/apps/gmm/p/b/l;

    .line 248
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/p/b/i;->a(Lcom/google/android/apps/gmm/p/b/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/d;->i:Lcom/google/android/apps/gmm/p/b/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/i;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
