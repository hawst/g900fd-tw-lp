.class public Lcom/google/android/apps/gmm/navigation/a/bj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/av;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/navigation/a/bn;

.field private final c:Lcom/google/android/apps/gmm/base/a;

.field private final d:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final e:Lcom/google/r/b/a/amk;

.field private final f:Lcom/google/android/apps/gmm/shared/b/a;

.field private final g:Lcom/google/android/apps/gmm/aa/d/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/aa/d/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/bj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/logging/m;)V
    .locals 18

    .prologue
    .line 231
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 234
    new-instance v6, Lcom/google/android/apps/gmm/aa/d/t;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v6, v0, v1}, Lcom/google/android/apps/gmm/aa/d/t;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;)V

    .line 235
    new-instance v5, Lcom/google/android/apps/gmm/navigation/a/bf;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/gmm/navigation/a/bf;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)V

    .line 236
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v17

    .line 237
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->y()Lcom/google/r/b/a/amh;

    move-result-object v2

    .line 238
    iget v2, v2, Lcom/google/r/b/a/amh;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/amk;->a(I)Lcom/google/r/b/a/amk;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    move-object/from16 v16, v2

    .line 239
    :goto_0
    const/4 v8, 0x0

    .line 240
    const/4 v11, 0x0

    .line 241
    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/bk;->a:[I

    invoke-virtual/range {v16 .. v16}, Lcom/google/r/b/a/amk;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 258
    new-instance v2, Lcom/google/android/apps/gmm/aa/d/a;

    new-instance v3, Lcom/google/android/apps/gmm/aa/d/p;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/aa/d/p;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/aa/d/a;-><init>(Lcom/google/android/apps/gmm/aa/d/p;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/navigation/a/a/c;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/navigation/logging/m;)V

    .line 261
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/gmm/navigation/a/bo;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/apps/gmm/navigation/a/bo;-><init>(Lcom/google/android/apps/gmm/navigation/a/bj;)V

    new-instance v12, Lcom/google/android/apps/gmm/aa/d/o;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v12, v0, v1}, Lcom/google/android/apps/gmm/aa/d/o;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;)V

    .line 262
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v13

    .line 263
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v14

    move-object/from16 v7, p1

    move-object/from16 v8, v17

    move-object v11, v6

    move-object/from16 v15, p3

    .line 260
    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/gmm/aa/d/h;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/logging/m;)Lcom/google/android/apps/gmm/aa/d/h;

    move-result-object v3

    .line 267
    :goto_1
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/a/bj;->c:Lcom/google/android/apps/gmm/base/a;

    .line 268
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/a/bj;->d:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 269
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    .line 270
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    .line 271
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/bj;->f:Lcom/google/android/apps/gmm/shared/b/a;

    .line 272
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    .line 273
    return-void

    :cond_0
    move-object/from16 v16, v2

    .line 238
    goto :goto_0

    .line 243
    :pswitch_0
    new-instance v2, Lcom/google/android/apps/gmm/aa/d/a;

    new-instance v3, Lcom/google/android/apps/gmm/aa/d/p;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/aa/d/p;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/google/android/apps/gmm/navigation/a/bo;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/navigation/a/bo;-><init>(Lcom/google/android/apps/gmm/navigation/a/bj;)V

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/aa/d/a;-><init>(Lcom/google/android/apps/gmm/aa/d/p;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/navigation/a/a/c;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/navigation/logging/m;)V

    move-object v3, v8

    .line 245
    goto :goto_1

    .line 248
    :pswitch_1
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/navigation/a/bo;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/navigation/a/bo;-><init>(Lcom/google/android/apps/gmm/navigation/a/bj;)V

    new-instance v7, Lcom/google/android/apps/gmm/aa/d/o;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v7, v0, v1}, Lcom/google/android/apps/gmm/aa/d/o;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;)V

    .line 249
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v8

    .line 250
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v9

    move-object/from16 v2, p1

    move-object/from16 v3, v17

    move-object/from16 v10, p3

    .line 247
    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/gmm/aa/d/h;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/logging/m;)Lcom/google/android/apps/gmm/aa/d/h;

    move-result-object v2

    move-object v3, v2

    move-object v2, v11

    .line 251
    goto :goto_1

    .line 241
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 318
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/a/bj;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/bl;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/bl;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 323
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v4, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v4, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    if-ne v0, v4, :cond_4

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_6

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/aa/d/q;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 327
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v5, Lcom/google/r/b/a/amk;->b:Lcom/google/r/b/a/amk;

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v5, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    if-ne v4, v5, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    if-eqz v2, :cond_3

    .line 328
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    invoke-interface {v2, p1}, Lcom/google/android/apps/gmm/aa/d/q;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 329
    if-eqz v2, :cond_3

    move-object v0, v2

    .line 333
    :cond_3
    if-nez v0, :cond_5

    .line 336
    :goto_2
    return-object v1

    :cond_4
    move v0, v2

    .line 324
    goto :goto_0

    .line 336
    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/bl;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->d:Lcom/google/android/apps/gmm/shared/c/a/j;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->f:Lcom/google/android/apps/gmm/shared/b/a;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/apps/gmm/navigation/a/bl;-><init>(Landroid/content/Context;Ljava/io/File;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/b/a;)V

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 342
    .line 343
    monitor-enter p0

    .line 344
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->b:Lcom/google/android/apps/gmm/navigation/a/bn;

    if-eqz v0, :cond_3

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->b:Lcom/google/android/apps/gmm/navigation/a/bn;

    .line 346
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->b:Lcom/google/android/apps/gmm/navigation/a/bn;

    .line 348
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    if-eqz v0, :cond_0

    .line 350
    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/bj;->a:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/bn;->c:Lcom/google/android/apps/gmm/navigation/a/aw;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/bn;->c:Lcom/google/android/apps/gmm/navigation/a/aw;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/a/bn;->a:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/a/aw;->e()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/bn;->c:Lcom/google/android/apps/gmm/navigation/a/aw;

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/aa/d/q;->a()V

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/aa/d/q;->a()V

    .line 358
    :cond_2
    return-void

    .line 348
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/aw;Lcom/google/android/apps/gmm/aa/d/s;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/navigation/a/bj;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/bl;

    .line 291
    if-eqz v0, :cond_1

    .line 292
    if-eqz p2, :cond_0

    .line 293
    invoke-interface {p2}, Lcom/google/android/apps/gmm/navigation/a/aw;->e()V

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    new-instance v3, Lcom/google/android/apps/gmm/navigation/a/bn;

    invoke-direct {v3, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/a/bn;-><init>(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/aw;Lcom/google/android/apps/gmm/aa/d/s;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/a/bn;->b:Lcom/google/android/apps/gmm/aa/d/s;

    sget-object v4, Lcom/google/android/apps/gmm/aa/d/s;->c:Lcom/google/android/apps/gmm/aa/d/s;

    if-ne v0, v4, :cond_3

    monitor-enter p0

    :try_start_0
    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->b:Lcom/google/android/apps/gmm/navigation/a/bn;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v4, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v4, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    if-ne v0, v4, :cond_5

    :cond_2
    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    new-instance v4, Lcom/google/android/apps/gmm/aa/d/k;

    iget-object v5, v3, Lcom/google/android/apps/gmm/navigation/a/bn;->a:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    iget-object v6, v3, Lcom/google/android/apps/gmm/navigation/a/bn;->b:Lcom/google/android/apps/gmm/aa/d/s;

    iget-object v7, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    invoke-direct {v4, v5, v6, v8, v9}, Lcom/google/android/apps/gmm/aa/d/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/aa/d/s;J)V

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/aa/d/q;->a(Lcom/google/android/apps/gmm/aa/d/k;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v4, Lcom/google/r/b/a/amk;->b:Lcom/google/r/b/a/amk;

    if-eq v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->e:Lcom/google/r/b/a/amk;

    sget-object v4, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    if-ne v0, v4, :cond_6

    :cond_4
    move v0, v2

    :goto_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    new-instance v1, Lcom/google/android/apps/gmm/aa/d/k;

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/a/bn;->a:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/a/bn;->b:Lcom/google/android/apps/gmm/aa/d/s;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/aa/d/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/aa/d/s;J)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/aa/d/q;->a(Lcom/google/android/apps/gmm/aa/d/k;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->g:Lcom/google/android/apps/gmm/aa/d/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/aa/d/q;->b()V

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    if-eqz v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bj;->h:Lcom/google/android/apps/gmm/aa/d/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/aa/d/q;->b()V

    .line 368
    :cond_1
    return-void
.end method
