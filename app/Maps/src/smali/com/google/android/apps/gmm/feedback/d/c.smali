.class public Lcom/google/android/apps/gmm/feedback/d/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/d/b;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/widget/AdapterView$OnItemClickListener;

.field public final c:Ljava/lang/Runnable;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/feedback/d/d;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/google/android/apps/gmm/feedback/d/d;

.field public final i:Lcom/google/android/apps/gmm/feedback/d/d;

.field public final j:Lcom/google/android/apps/gmm/feedback/d/d;

.field public final k:Lcom/google/android/apps/gmm/feedback/d/d;

.field public final l:Lcom/google/android/apps/gmm/feedback/d/d;

.field public final m:Lcom/google/android/apps/gmm/feedback/d/d;

.field public final n:Lcom/google/android/apps/gmm/feedback/d/d;

.field private final o:Lcom/google/android/apps/gmm/base/views/c/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AdapterView$OnItemClickListener;Lcom/google/android/apps/gmm/base/views/c/g;Ljava/lang/Runnable;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->lf:I

    .line 100
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->lg:I

    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v1, Lcom/google/android/apps/gmm/f;->ed:I

    .line 102
    sget v4, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget-object v5, Lcom/google/b/f/t;->ew:Lcom/google/b/f/t;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->h:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->mR:I

    .line 106
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->mS:I

    .line 107
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v1, Lcom/google/android/apps/gmm/f;->cu:I

    .line 108
    sget v4, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget-object v5, Lcom/google/b/f/t;->ez:Lcom/google/b/f/t;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->i:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->aJ:I

    .line 112
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->aK:I

    .line 113
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v1, Lcom/google/android/apps/gmm/f;->eE:I

    .line 114
    sget v4, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget-object v5, Lcom/google/b/f/t;->ev:Lcom/google/b/f/t;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->j:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->mU:I

    .line 118
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->mV:I

    .line 119
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v1, Lcom/google/android/apps/gmm/f;->dg:I

    .line 120
    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/b/f/t;->eA:Lcom/google/b/f/t;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->k:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->lj:I

    .line 125
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->lk:I

    .line 126
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v5, Lcom/google/b/f/t;->ey:Lcom/google/b/f/t;

    move-object v1, p0

    move-object v4, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->l:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->lc:I

    .line 132
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->ld:I

    .line 133
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object v1, p0

    move-object v4, v6

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->m:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/feedback/d/d;

    sget v1, Lcom/google/android/apps/gmm/l;->ll:I

    .line 139
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget v1, Lcom/google/android/apps/gmm/l;->lm:I

    .line 140
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object v1, p0

    move-object v4, v6

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/feedback/d/d;-><init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->n:Lcom/google/android/apps/gmm/feedback/d/d;

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/d/c;->a:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->b:Landroid/widget/AdapterView$OnItemClickListener;

    .line 39
    iput-object p4, p0, Lcom/google/android/apps/gmm/feedback/d/c;->c:Ljava/lang/Runnable;

    .line 40
    iput-object p3, p0, Lcom/google/android/apps/gmm/feedback/d/c;->o:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 41
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->f:Ljava/lang/Boolean;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->e:Ljava/util/List;

    .line 43
    new-instance v0, Lcom/google/b/c/dd;

    invoke-direct {v0}, Lcom/google/b/c/dd;-><init>()V

    sget v1, Lcom/google/android/apps/gmm/l;->lf:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->h:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->mR:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->i:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->lj:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->l:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->lc:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->m:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->ll:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->n:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->aJ:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->j:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->mU:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/c;->k:Lcom/google/android/apps/gmm/feedback/d/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/dd;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dd;

    invoke-virtual {v0}, Lcom/google/b/c/dd;->a()Lcom/google/b/c/dc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->d:Ljava/util/Map;

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->o:Lcom/google/android/apps/gmm/base/views/c/g;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/c;->e:Ljava/util/List;

    return-object v0
.end method
