.class Lcom/google/android/apps/gmm/aa/d/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lcom/google/android/apps/gmm/aa/d/r;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lcom/google/android/apps/gmm/aa/d/k;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/aa/d/t;

.field private final e:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final f:Lcom/google/android/apps/gmm/aa/d/o;

.field private final g:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final h:Lcom/google/android/apps/gmm/navigation/logging/m;

.field private i:Lcom/google/android/apps/gmm/map/r/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/PriorityBlockingQueue;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/navigation/logging/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lcom/google/android/apps/gmm/aa/d/k;",
            ">;",
            "Lcom/google/android/apps/gmm/aa/d/r;",
            "Lcom/google/android/apps/gmm/aa/d/t;",
            "Lcom/google/android/apps/gmm/shared/net/a/b;",
            "Lcom/google/android/apps/gmm/aa/d/o;",
            "Lcom/google/android/apps/gmm/shared/c/a/j;",
            "Lcom/google/android/apps/gmm/navigation/logging/m;",
            ")V"
        }
    .end annotation

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/i;->b:Landroid/content/Context;

    .line 213
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/d/i;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 214
    iput-object p3, p0, Lcom/google/android/apps/gmm/aa/d/i;->a:Lcom/google/android/apps/gmm/aa/d/r;

    .line 215
    iput-object p4, p0, Lcom/google/android/apps/gmm/aa/d/i;->d:Lcom/google/android/apps/gmm/aa/d/t;

    .line 216
    iput-object p5, p0, Lcom/google/android/apps/gmm/aa/d/i;->e:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 217
    iput-object p6, p0, Lcom/google/android/apps/gmm/aa/d/i;->f:Lcom/google/android/apps/gmm/aa/d/o;

    .line 218
    iput-object p7, p0, Lcom/google/android/apps/gmm/aa/d/i;->g:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 219
    iput-object p8, p0, Lcom/google/android/apps/gmm/aa/d/i;->h:Lcom/google/android/apps/gmm/navigation/logging/m;

    .line 220
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 295
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/i;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/aa/b/a;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/aa/d/i;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/d/i;->e:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-static {p1, v0, v3, v4}, Lcom/google/android/apps/gmm/aa/d/h;->a(Ljava/lang/String;Ljava/util/Locale;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;

    move-result-object v0

    .line 299
    if-nez v0, :cond_0

    move v0, v1

    .line 336
    :goto_0
    return v0

    .line 302
    :cond_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 303
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v4

    .line 304
    const/4 v0, 0x0

    const/4 v5, 0x0

    new-instance v6, Ljava/security/SecureRandom;

    invoke-direct {v6}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v4, v0, v5, v6}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 305
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 306
    invoke-virtual {v4}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 307
    sget v3, Lcom/google/android/apps/gmm/aa/d/h;->b:I

    invoke-virtual {v0, v3}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 309
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4

    .line 311
    const/16 v2, 0x400

    :try_start_1
    new-array v4, v2, [B

    .line 312
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 313
    :goto_1
    const/4 v5, -0x1

    if-eq v2, v5, :cond_1

    .line 314
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 315
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    goto :goto_1

    .line 318
    :cond_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 336
    const/4 v0, 0x1

    goto :goto_0

    .line 318
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/security/KeyManagementException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_4

    .line 335
    :catch_0
    move-exception v0

    .line 321
    const-string v2, "Could not synthesize text, malformed URL"

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 322
    goto :goto_0

    .line 324
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    move v0, v1

    .line 325
    goto :goto_0

    .line 327
    :catch_2
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    const-string v0, "IO exception while synthesising "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move v0, v1

    .line 328
    goto :goto_0

    .line 327
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 330
    :catch_3
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    move v0, v1

    .line 331
    goto :goto_0

    .line 333
    :catch_4
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    move v0, v1

    .line 334
    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 224
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/i;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 225
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 230
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/i;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/d/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/aa/d/k;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/i;->d:Lcom/google/android/apps/gmm/aa/d/t;

    iget-object v3, v1, Lcom/google/android/apps/gmm/aa/d/t;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    iget-object v1, v1, Lcom/google/android/apps/gmm/aa/d/t;->c:Ljava/io/File;

    invoke-direct {v4, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/i;->f:Lcom/google/android/apps/gmm/aa/d/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/d/i;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/d/i;->e:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/gmm/aa/d/h;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/a/b;Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/d/l;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/aa/d/o;->a(Lcom/google/android/apps/gmm/aa/d/l;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    const-string v1, "Synthesis success for "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/aa/d/i;->f:Lcom/google/android/apps/gmm/aa/d/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/d/i;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/d/i;->e:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/gmm/aa/d/h;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/a/b;Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/d/l;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/aa/d/o;->b(Lcom/google/android/apps/gmm/aa/d/l;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lcom/google/android/apps/gmm/aa/d/o;->a:Lcom/google/android/apps/gmm/aa/d/d;

    invoke-virtual {v5, v1, v4}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/io/File;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/io/File;

    iget-object v3, v3, Lcom/google/android/apps/gmm/aa/d/o;->a:Lcom/google/android/apps/gmm/aa/d/d;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_2
    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    :cond_0
    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/d/k;->b:Lcom/google/android/apps/gmm/aa/d/s;

    sget-object v1, Lcom/google/android/apps/gmm/aa/d/s;->c:Lcom/google/android/apps/gmm/aa/d/s;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/i;->g:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/aa/d/j;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/aa/d/j;-><init>(Lcom/google/android/apps/gmm/aa/d/i;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :cond_1
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/i;->g:Lcom/google/android/apps/gmm/shared/c/a/j;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_TTS_SYNTHESIS:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 235
    return-void

    .line 230
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/i;->h:Lcom/google/android/apps/gmm/navigation/logging/m;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/logging/m;->a()V

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/gmm/aa/d/i;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 231
    :catch_0
    move-exception v0

    .line 232
    const-string v1, "Interrupted while waiting for an element in the queue."

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 230
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    :try_start_2
    sget-object v1, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method
