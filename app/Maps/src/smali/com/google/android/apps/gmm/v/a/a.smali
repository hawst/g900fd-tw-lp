.class public Lcom/google/android/apps/gmm/v/a/a;
.super Lcom/google/android/apps/gmm/v/c;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/v/r;

.field private b:[F

.field private c:[F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/r;II)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x437f0000    # 255.0f

    .line 55
    const/4 v0, 0x4

    new-array v0, v0, [F

    .line 57
    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    aput v1, v0, v4

    .line 58
    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    aput v1, v0, v5

    .line 59
    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    aput v1, v0, v6

    .line 60
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    aput v1, v0, v7

    const/4 v1, 0x4

    new-array v1, v1, [F

    .line 63
    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v1, v4

    .line 64
    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v1, v5

    .line 65
    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v1, v6

    .line 66
    invoke-static {p3}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v1, v7

    .line 55
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/v/a/a;-><init>(Lcom/google/android/apps/gmm/v/r;[F[F)V

    .line 68
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/v/r;[F[F)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/c;-><init>()V

    .line 27
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    .line 33
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/a/a;->c:[F

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/a/a;->a:Lcom/google/android/apps/gmm/v/r;

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    invoke-static {p2, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a/a;->c:[F

    invoke-static {p3, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 72
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/v/c;->getTransformation(JLandroid/view/animation/Transformation;)Z

    iget v0, p0, Lcom/google/android/apps/gmm/v/c;->e:F

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/a/a;->a:Lcom/google/android/apps/gmm/v/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v2, v2, v5

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a/a;->c:[F

    aget v3, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v4, v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v3, v3, v6

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a/a;->c:[F

    aget v4, v4, v6

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v5, v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v4, v4, v7

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/a/a;->c:[F

    aget v5, v5, v7

    iget-object v6, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v6, v6, v7

    sub-float/2addr v5, v6

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v5, v5, v8

    iget-object v6, p0, Lcom/google/android/apps/gmm/v/a/a;->c:[F

    aget v6, v6, v8

    iget-object v7, p0, Lcom/google/android/apps/gmm/v/a/a;->b:[F

    aget v7, v7, v8

    sub-float/2addr v6, v7

    mul-float/2addr v0, v6

    add-float/2addr v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/apps/gmm/v/r;->a(FFFF)V

    .line 78
    return-void
.end method
