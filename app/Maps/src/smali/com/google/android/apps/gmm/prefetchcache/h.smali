.class Lcom/google/android/apps/gmm/prefetchcache/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->d:Z

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    sget v2, Lcom/google/android/apps/gmm/l;->jf:I

    .line 224
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    goto :goto_0

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    sget v2, Lcom/google/android/apps/gmm/l;->jl:I

    .line 228
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 227
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/h;->b:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    goto :goto_0
.end method
