.class public Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field private c:Lcom/google/android/apps/gmm/directions/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 43
    return-void
.end method

.method public static a(III)Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;
    .locals 4

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;-><init>()V

    .line 59
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    const-string v2, "initial state"

    new-instance v3, Lcom/google/android/apps/gmm/directions/d;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/apps/gmm/directions/d;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 62
    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->dismiss()V

    .line 90
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initial state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    .line 72
    :goto_0
    new-instance v0, Landroid/app/DatePickerDialog;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    iget v3, v2, Lcom/google/android/apps/gmm/directions/d;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    iget v4, v2, Lcom/google/android/apps/gmm/directions/d;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    iget v5, v2, Lcom/google/android/apps/gmm/directions/d;->c:I

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    return-object v0

    .line 70
    :cond_0
    const-string v0, "state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    goto :goto_0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    iput p2, v0, Lcom/google/android/apps/gmm/directions/d;->a:I

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    iput p3, v0, Lcom/google/android/apps/gmm/directions/d;->b:I

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    iput p4, v0, Lcom/google/android/apps/gmm/directions/d;->c:I

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/directions/c;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/google/android/apps/gmm/directions/c;-><init>(Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;III)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->dismiss()V

    .line 102
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 80
    const-string v0, "state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/directions/d;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 81
    return-void
.end method
