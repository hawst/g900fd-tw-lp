.class Lcom/google/android/apps/gmm/ulr/b;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/ulr/OptInService;

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/ulr/OptInService;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    .line 151
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 148
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/b;->b:Ljava/util/Queue;

    .line 152
    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v0, v0, Lcom/google/android/apps/gmm/ulr/OptInService;->a:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    :cond_0
    return-void

    .line 196
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/b;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    if-eqz v0, :cond_0

    .line 197
    iget-object v1, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    .line 199
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v2, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v6, v2, Lcom/google/android/apps/gmm/ulr/OptInService;->a:Lcom/google/android/gms/location/reporting/c;

    const-string v2, "sender"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/app/PendingIntent;

    if-nez v2, :cond_3

    move v2, v3

    :cond_2
    :goto_1
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/ulr/OptInService;->a(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1a

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "stopping self for startId "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/ulr/OptInService;->stopSelfResult(I)Z

    goto :goto_0

    .line 199
    :cond_3
    :try_start_1
    invoke-virtual {v2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    goto :goto_1

    :cond_4
    const-string v7, "com.google.android.googlequicksearchbox"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    const-string v5, "Package not authorized: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move v2, v4

    goto :goto_1

    :cond_5
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 201
    :catch_0
    move-exception v2

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/ulr/OptInService;->a(Landroid/content/Intent;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 206
    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1a

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "stopping self for startId "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/ulr/OptInService;->stopSelfResult(I)Z

    goto/16 :goto_0

    .line 199
    :cond_6
    :try_start_3
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/ulr/OptInService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_8

    const-string v5, "Package not signed with Google cert: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_3
    move v2, v4

    goto/16 :goto_1

    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 206
    :catchall_0
    move-exception v1

    iget-object v2, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "stopping self for startId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/ulr/OptInService;->stopSelfResult(I)Z

    throw v1

    .line 199
    :cond_8
    :try_start_4
    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    if-nez v2, :cond_9

    const/4 v2, 0x2

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v6, v2}, Lcom/google/android/gms/location/reporting/c;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/location/reporting/ReportingState;->c()Z

    move-result v5

    if-nez v5, :cond_a

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x18

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "is not allowed to opt in"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v6, v2}, Lcom/google/android/gms/location/reporting/c;->b(Landroid/accounts/Account;)I

    move-result v2

    if-eqz v2, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x19

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Opt in error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const v5, 0x297832e

    .line 156
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unknown what: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/ulr/OptInService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 159
    invoke-direct {p0}, Lcom/google/android/apps/gmm/ulr/b;->a()V

    .line 180
    :goto_0
    return-void

    .line 163
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/a;

    .line 164
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    iget-object v2, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    const-string v1, "notification"

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/ulr/OptInService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a;->b()I

    move-result v3

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/gmm/l;->gP:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    packed-switch v3, :pswitch_data_1

    sget v0, Lcom/google/android/apps/gmm/l;->gQ:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const v4, 0x108008a

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v4, Lcom/google/android/apps/gmm/l;->gO:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/common/g;->a(ILandroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 169
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/b;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    if-eqz v0, :cond_1

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/ulr/OptInService;->a(Landroid/content/Intent;I)V

    goto :goto_2

    .line 165
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->gN:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/l;->gR:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    sget v0, Lcom/google/android/apps/gmm/l;->gM:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/b;->a:Lcom/google/android/apps/gmm/ulr/OptInService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/ulr/OptInService;->stopSelf()V

    goto/16 :goto_0

    .line 178
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/b;->b:Ljava/util/Queue;

    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-static {v2, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/gmm/ulr/b;->a()V

    goto/16 :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 165
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
