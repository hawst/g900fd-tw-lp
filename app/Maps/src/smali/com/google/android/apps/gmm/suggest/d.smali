.class public Lcom/google/android/apps/gmm/suggest/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/view/View;

.field c:Lcom/google/android/apps/gmm/suggest/f/a;

.field d:Lcom/google/android/apps/gmm/suggest/b;

.field public final e:Lcom/google/android/apps/gmm/suggest/l;

.field public f:Lcom/google/android/apps/gmm/suggest/a/a;

.field public final g:Lcom/google/android/apps/gmm/suggest/d/d;

.field public h:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/suggest/l;Lcom/google/android/apps/gmm/suggest/d/d;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/gmm/suggest/d;->g:Lcom/google/android/apps/gmm/suggest/d/d;

    .line 85
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/suggest/b/a;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v0

    iget-object v3, p1, Lcom/google/android/apps/gmm/suggest/b/a;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/suggest/e/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/b/a;->d:Lcom/google/android/apps/gmm/suggest/d/g;

    .line 177
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/apps/gmm/suggest/b/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 178
    const-string v3, "Suggestions are being updated for [%s] but the query is [%s] now"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 179
    iget-object v5, p1, Lcom/google/android/apps/gmm/suggest/b/a;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 178
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 180
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/d/g;->b()V

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/b/a;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v4

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v3, v0, Lcom/google/android/apps/gmm/suggest/l;->b:Lcom/google/android/apps/gmm/x/o;

    .line 189
    if-eqz v3, :cond_7

    .line 190
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 191
    if-eqz v0, :cond_7

    .line 192
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 193
    if-eqz v0, :cond_7

    .line 194
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    .line 195
    if-eqz v0, :cond_3

    .line 196
    iget-object v5, p1, Lcom/google/android/apps/gmm/suggest/b/a;->b:Ljava/lang/String;

    if-eq v0, v5, :cond_3

    move v0, v1

    .line 201
    :goto_1
    if-eqz v0, :cond_6

    .line 202
    const/4 v0, 0x0

    .line 204
    :goto_2
    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/gmm/suggest/d;->a(Ljava/util/List;Lcom/google/android/apps/gmm/x/o;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/b/c/cv;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->c:Lcom/google/android/apps/gmm/suggest/f/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/suggest/f/a;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->c:Lcom/google/android/apps/gmm/suggest/f/a;

    .line 209
    invoke-interface {v0}, Lcom/google/android/apps/gmm/suggest/f/a;->e()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->c:Lcom/google/android/apps/gmm/suggest/f/a;

    .line 210
    invoke-interface {v0}, Lcom/google/android/apps/gmm/suggest/f/a;->b()Lcom/google/android/apps/gmm/search/e/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/e/b;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 211
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    if-eqz v1, :cond_5

    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/suggest/l;->d(Z)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 196
    goto :goto_1

    :cond_4
    move v1, v2

    .line 210
    goto :goto_3

    .line 211
    :cond_5
    const/16 v2, 0x8

    goto :goto_4

    :cond_6
    move-object v0, v3

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method a(Ljava/util/List;Lcom/google/android/apps/gmm/x/o;)V
    .locals 9
    .param p2    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/search/aq;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v6

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/suggest/g/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    .line 102
    iget-object v2, v2, Lcom/google/android/apps/gmm/suggest/l;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v4, 0x0

    :goto_0
    new-instance v7, Lcom/google/android/apps/gmm/suggest/g/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    .line 106
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    .line 107
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    .line 108
    iget-object v8, v8, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/base/j/b;->h()Lcom/google/android/apps/gmm/addaplace/a/b;

    move-result-object v8

    invoke-direct {v7, v3, v2, v8}, Lcom/google/android/apps/gmm/suggest/g/a;-><init>(Lcom/google/android/apps/gmm/map/t;Lcom/google/r/b/a/apq;Lcom/google/android/apps/gmm/addaplace/a/b;)V

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/suggest/g/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/suggest/d;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/search/aq;Lcom/google/android/apps/gmm/place/b/b;Lcom/google/android/apps/gmm/suggest/g/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->c:Lcom/google/android/apps/gmm/suggest/f/a;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d;->c:Lcom/google/android/apps/gmm/suggest/f/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 110
    return-void

    :cond_0
    move-object v4, p2

    .line 102
    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 270
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->d:Lcom/google/android/apps/gmm/suggest/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->d:Lcom/google/android/apps/gmm/suggest/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/suggest/b;->a()V

    .line 264
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->d:Lcom/google/android/apps/gmm/suggest/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d;->d:Lcom/google/android/apps/gmm/suggest/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/suggest/b;->a()V

    .line 255
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
