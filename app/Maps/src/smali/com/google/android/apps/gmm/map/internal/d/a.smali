.class public abstract Lcom/google/android/apps/gmm/map/internal/d/a;
.super Lcom/google/android/apps/gmm/map/internal/d/d;
.source "PG"


# static fields
.field static final a:[B


# instance fields
.field final b:I

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final d:I

.field public volatile e:I

.field f:Z

.field private volatile q:Z

.field private final r:I

.field private final s:F

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/a;->a:[B

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/ai;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/ac;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IIFZ",
            "Ljava/util/Locale;",
            "Z",
            "Ljava/io/File;",
            "Lcom/google/android/apps/gmm/map/internal/d/r;",
            "Lcom/google/android/apps/gmm/map/internal/d/ag;",
            ")V"
        }
    .end annotation

    .prologue
    .line 316
    .line 319
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/aw;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;)Lcom/google/android/apps/gmm/map/internal/d/b/as;

    move-result-object v5

    .line 320
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/aw;

    move-result-object v1

    move/from16 v0, p11

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/aw;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object v4, p2

    move/from16 v7, p9

    move/from16 v8, p7

    move-object/from16 v9, p10

    move-object/from16 v10, p12

    move-object/from16 v11, p13

    move-object/from16 v12, p14

    .line 316
    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/d/d;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/as;Lcom/google/android/apps/gmm/map/internal/d/b/s;ZILjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V

    .line 226
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->q:Z

    .line 265
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->e:I

    .line 273
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->u:Z

    .line 279
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->f:Z

    .line 327
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->b:I

    .line 328
    invoke-static/range {p5 .. p5}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    .line 329
    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->r:I

    .line 330
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->b:I

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/a;->a(Ljava/util/Collection;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->d:I

    .line 331
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->s:F

    .line 332
    return-void
.end method

.method static a(Ljava/util/Collection;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/16 v2, 0x80

    const/4 v0, 0x0

    .line 340
    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0xe

    .line 341
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0xf

    .line 342
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x9

    .line 343
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x10

    .line 344
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    :cond_0
    return v0

    :cond_1
    move v1, p1

    :goto_0
    if-le v1, v2, :cond_2

    shr-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-ge v1, v2, :cond_0

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method static a(Lcom/google/e/a/a/a/b;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 493
    const/16 v0, 0xb

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 495
    if-nez p1, :cond_0

    .line 496
    const/16 v0, 0xf

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 499
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/d/b;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 941
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/map/internal/d/a;->b(Z)Lcom/google/android/apps/gmm/map/internal/d/i;

    move-result-object v0

    .line 949
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v3, v4, v4, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    invoke-direct {v1, v2, v3, v5}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    invoke-virtual {v0, v1, v5, v4}, Lcom/google/android/apps/gmm/map/internal/d/i;->a(Lcom/google/android/apps/gmm/map/internal/d/p;Lcom/google/b/a/an;Z)V

    .line 956
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c;)Lcom/google/e/a/a/a/b;
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x3

    .line 399
    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/bg;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 402
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->b:I

    int-to-long v6, v0

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 403
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->r:I

    int-to-long v6, v0

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v11, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 404
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->s:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 405
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->s:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 408
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    monitor-enter v5

    .line 409
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 411
    const/4 v7, 0x2

    int-to-long v8, v0

    invoke-static {v8, v9}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 409
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 413
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->z_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 421
    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 426
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 427
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v3, :cond_3

    .line 428
    int-to-long v6, v11

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v10, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 432
    :cond_3
    const/16 v3, 0x8

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v10, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 435
    const/16 v3, 0xb

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v10, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    const/16 v3, 0xf

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v10, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 438
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v0, v0, Lcom/google/r/b/a/aqg;->o:Z

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 439
    const/16 v0, 0xd

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 443
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget-boolean v0, v0, Lcom/google/r/b/a/ou;->N:Z

    if-eqz v0, :cond_a

    .line 444
    int-to-long v0, v10

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 451
    :goto_2
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->a:Lcom/google/android/apps/gmm/map/internal/d/c;

    if-eq p1, v0, :cond_5

    .line 452
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c;->f:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v12, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 455
    :cond_5
    int-to-long v0, v12

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 458
    const/4 v0, 0x6

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 461
    const/16 v0, 0x9

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 464
    const/16 v0, 0xa

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 467
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->u:Z

    if-eqz v0, :cond_6

    .line 468
    const/16 v1, 0x8

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v4, v1, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 471
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->f:Z

    if-eqz v0, :cond_7

    .line 472
    const/16 v0, 0xe

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 476
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/a;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 477
    const/16 v0, 0x11

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 481
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->e:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 484
    :cond_8
    return-object v4

    .line 413
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_9
    move v0, v2

    .line 438
    goto/16 :goto_1

    .line 446
    :cond_a
    const/16 v0, 0x10

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v10, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method protected final a(Landroid/os/Message;)Z
    .locals 21

    .prologue
    .line 1041
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 1043
    const/4 v2, 0x0

    .line 1062
    :goto_0
    return v2

    .line 1047
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/b/a/an;

    .line 1048
    iget-object v3, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    move-object v15, v3

    check-cast v15, Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 1049
    iget-object v2, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/e/a/a/a/b;

    .line 1050
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->what:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_9

    .line 1051
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;[I)Lcom/google/e/a/a/a/b;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/android/apps/gmm/map/internal/d/b;->a(Lcom/google/e/a/a/a/b;)V

    const/16 v3, 0x9

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v19

    const/4 v3, 0x0

    move/from16 v18, v3

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    const/16 v3, 0x9

    const/16 v4, 0x1a

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/google/e/a/a/a/b;

    const/4 v3, 0x1

    const/16 v5, 0x1a

    invoke-virtual {v4, v3, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v16, v3

    check-cast v16, Lcom/google/e/a/a/a/b;

    const/4 v3, 0x2

    const/16 v5, 0x15

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v5, v6

    const/4 v3, 0x3

    const/16 v6, 0x15

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v3, 0x4

    const/16 v7, 0x15

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v7, v8

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v3, v7, v5, v6}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    const/4 v5, 0x2

    const/16 v6, 0x19

    invoke-virtual {v4, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    if-nez v4, :cond_2

    const-string v4, "DashServerMapTileStore"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1d

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "There\'s no data in the tile: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_2
    add-int/lit8 v3, v18, 0x1

    move/from16 v18, v3

    goto/16 :goto_1

    :cond_2
    const/16 v6, 0x8

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    if-lez v5, :cond_5

    const/4 v5, 0x1

    :goto_3
    if-nez v5, :cond_3

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_6

    :cond_3
    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_7

    const/16 v5, 0x8

    const/16 v6, 0x15

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v5, v6

    move v14, v5

    :goto_5
    const/16 v5, 0x1b

    const/16 v6, 0x1a

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v17, v5

    check-cast v17, Lcom/google/e/a/a/a/b;

    if-eqz v17, :cond_1

    const/4 v5, 0x0

    :try_start_0
    array-length v6, v4

    sget-object v7, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    iget-object v12, v15, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v12}, Lcom/google/android/apps/gmm/map/internal/d/ac;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v12

    new-instance v13, Lcom/google/android/apps/gmm/map/internal/c/cr;

    iget v0, v15, Lcom/google/android/apps/gmm/map/internal/d/b;->a:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-direct {v13, v0, v14}, Lcom/google/android/apps/gmm/map/internal/c/cr;-><init>(II)V

    const/4 v14, 0x0

    invoke-static/range {v3 .. v14}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cr;I)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v4

    if-nez v4, :cond_8

    const-string v3, "DashServerMapTileStore"

    const-string v4, "Tile unpack failed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1062
    :cond_4
    :goto_6
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1051
    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    :cond_6
    const/4 v5, 0x0

    goto :goto_4

    :cond_7
    const/4 v5, -0x1

    move v14, v5

    goto :goto_5

    :cond_8
    check-cast v4, Lcom/google/android/apps/gmm/map/internal/c/cm;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/internal/c/aj;->a(Lcom/google/android/apps/gmm/map/internal/c/cm;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v5

    const/16 v4, 0x1c

    const/16 v6, 0x1c

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/b;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/e/a/a/a/b;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/a;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v4, v3, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_2

    :catch_0
    move-exception v3

    const-string v4, "DashServerMapTileStore"

    invoke-static {v4, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :catch_1
    move-exception v3

    const-string v4, "DashServerMapTileStore"

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1052
    :cond_9
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->what:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 1053
    invoke-virtual {v15, v2}, Lcom/google/android/apps/gmm/map/internal/d/b;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;

    .line 1057
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Lcom/google/android/apps/gmm/map/internal/d/b;->a(Z)V

    .line 1058
    iget-object v2, v15, Lcom/google/android/apps/gmm/map/internal/d/b;->f:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 1059
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, v15, Lcom/google/android/apps/gmm/map/internal/d/b;->f:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/a;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/List;)V

    goto :goto_6
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1123
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v2, :cond_3

    .line 1124
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/cm;

    .line 1125
    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/internal/c/cm;->k:Z

    if-eqz v2, :cond_2

    .line 1126
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    const/16 v3, 0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1131
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1126
    goto :goto_0

    .line 1128
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1131
    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1092
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->e:I

    if-lez v1, :cond_1

    .line 1101
    :cond_0
    :goto_0
    return v0

    .line 1095
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/a;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1098
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    .line 1111
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1112
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/a;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
