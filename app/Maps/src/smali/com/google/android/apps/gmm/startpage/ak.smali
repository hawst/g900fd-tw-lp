.class Lcom/google/android/apps/gmm/startpage/ak;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field final b:Lcom/google/android/apps/gmm/startpage/y;

.field final c:Lcom/google/android/apps/gmm/startpage/ag;

.field final d:Lcom/google/android/apps/gmm/startpage/j;

.field e:Lcom/google/android/apps/gmm/startpage/a;

.field f:J

.field g:Z

.field h:J

.field i:Z

.field private final j:Lcom/google/android/apps/gmm/map/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private k:J

.field private l:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/android/apps/gmm/startpage/aa;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/startpage/y;Lcom/google/android/apps/gmm/startpage/ag;Lcom/google/android/apps/gmm/startpage/j;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->l:Ljava/util/WeakHashMap;

    .line 808
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->i:Z

    .line 814
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    .line 815
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/ak;->j:Lcom/google/android/apps/gmm/map/t;

    .line 816
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/ak;->b:Lcom/google/android/apps/gmm/startpage/y;

    .line 817
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    .line 818
    iput-object p5, p0, Lcom/google/android/apps/gmm/startpage/ak;->d:Lcom/google/android/apps/gmm/startpage/j;

    .line 819
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 990
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->g:Z

    if-eqz v0, :cond_0

    .line 1015
    :goto_0
    return-void

    .line 993
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/startpage/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/startpage/an;-><init>(Lcom/google/android/apps/gmm/startpage/ak;)V

    .line 1012
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2, p1, p2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 1014
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->g:Z

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/pk;)J
    .locals 6

    .prologue
    .line 1020
    iget-wide v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->k:J

    .line 1021
    iget v2, p2, Lcom/google/r/b/a/pk;->c:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1023
    iget-wide v2, p0, Lcom/google/android/apps/gmm/startpage/ak;->f:J

    .line 1024
    iget v4, p2, Lcom/google/r/b/a/pk;->d:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 1025
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1027
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1028
    return-wide v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 8

    .prologue
    .line 929
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    .line 931
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ak;->b:Lcom/google/android/apps/gmm/startpage/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    .line 932
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Landroid/accounts/Account;)Z

    move-result v3

    .line 931
    iget-boolean v4, v1, Lcom/google/android/apps/gmm/startpage/y;->d:Z

    if-nez v4, :cond_1

    const/4 v0, 0x0

    .line 933
    :goto_0
    if-eqz v0, :cond_0

    .line 934
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ak;->l:Ljava/util/WeakHashMap;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 935
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->k:J

    .line 937
    new-instance v0, Lcom/google/android/apps/gmm/startpage/am;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/startpage/am;-><init>(Lcom/google/android/apps/gmm/startpage/ak;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->t()Lcom/google/r/b/a/pk;

    move-result-object v3

    iget v3, v3, Lcom/google/r/b/a/pk;->e:I

    int-to-long v4, v3

    invoke-interface {v1, v0, v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 939
    :cond_0
    monitor-exit p0

    return-void

    .line 931
    :cond_1
    :try_start_1
    sget-object v4, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    sget-object v4, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    invoke-static {v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/startpage/af;->d:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v6, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v7, v1, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v7

    invoke-static {v0, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)Lcom/google/android/apps/gmm/startpage/ac;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/startpage/y;->a(Lcom/google/android/apps/gmm/startpage/ac;Lcom/google/android/apps/gmm/startpage/ae;Z)Lcom/google/android/apps/gmm/startpage/aa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 929
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 972
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    .line 973
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    .line 974
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->t()Lcom/google/r/b/a/pk;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/startpage/ak;->k:J

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/ak;->l:Ljava/util/WeakHashMap;

    .line 975
    invoke-virtual {v3}, Ljava/util/WeakHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 972
    if-nez v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget v2, v2, Lcom/google/r/b/a/pk;->e:I

    int-to-long v2, v2

    add-long/2addr v2, v4

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 959
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/ag;->a(Z)V

    .line 964
    :goto_0
    return-void

    .line 962
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->c:Lcom/google/android/apps/gmm/startpage/ag;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/ag;->a(Z)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 860
    iget-wide v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->h:J

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    .line 862
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->t()Lcom/google/r/b/a/pk;

    move-result-object v4

    .line 860
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    iget v4, v4, Lcom/google/r/b/a/pk;->b:I

    int-to-long v4, v4

    add-long/2addr v0, v4

    sub-long/2addr v0, v6

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    move-wide v0, v2

    .line 863
    :cond_0
    new-instance v4, Lcom/google/android/apps/gmm/startpage/al;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/startpage/al;-><init>(Lcom/google/android/apps/gmm/startpage/ak;)V

    .line 873
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 874
    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 879
    :goto_0
    return-void

    .line 876
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v4, v3, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/pk;)V
    .locals 4

    .prologue
    .line 912
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 913
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/startpage/ak;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/pk;)J

    move-result-wide v0

    .line 914
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 915
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->e:Lcom/google/android/apps/gmm/startpage/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ak;->j:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/startpage/a;->b(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->f:J

    .line 918
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/startpage/ak;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/pk;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/startpage/ak;->a(J)V

    .line 925
    :goto_0
    return-void

    .line 920
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/startpage/ak;->b(Lcom/google/android/apps/gmm/shared/c/f;)V

    goto :goto_0

    .line 923
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/startpage/ak;->a(J)V

    goto :goto_0
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/startpage/aa;)V
    .locals 1

    .prologue
    .line 967
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ak;->l:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/ak;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 969
    monitor-exit p0

    return-void

    .line 967
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
