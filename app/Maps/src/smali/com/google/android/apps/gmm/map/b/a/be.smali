.class public final Lcom/google/android/apps/gmm/map/b/a/be;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:I

.field private static d:I

.field private static final e:[Lcom/google/android/apps/gmm/map/b/a/be;


# instance fields
.field final a:I

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/16 v5, 0x16

    .line 38
    sput v0, Lcom/google/android/apps/gmm/map/b/a/be;->c:I

    .line 49
    sput v5, Lcom/google/android/apps/gmm/map/b/a/be;->d:I

    .line 56
    new-array v1, v5, [Lcom/google/android/apps/gmm/map/b/a/be;

    sput-object v1, Lcom/google/android/apps/gmm/map/b/a/be;->e:[Lcom/google/android/apps/gmm/map/b/a/be;

    .line 70
    const/16 v1, 0x100

    .line 71
    :goto_0
    if-gt v0, v5, :cond_0

    .line 72
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/be;->e:[Lcom/google/android/apps/gmm/map/b/a/be;

    add-int/lit8 v3, v0, -0x1

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/be;

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/be;-><init>(II)V

    aput-object v4, v2, v3

    .line 73
    shl-int/lit8 v1, v1, 0x1

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    .line 65
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/be;->a:I

    .line 66
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/b/a/be;
    .locals 2

    .prologue
    .line 82
    const/4 v0, 0x0

    .line 83
    sget v1, Lcom/google/android/apps/gmm/map/b/a/be;->c:I

    if-lt p0, v1, :cond_1

    const/16 v1, 0x16

    if-gt p0, v1, :cond_1

    .line 86
    sget v0, Lcom/google/android/apps/gmm/map/b/a/be;->d:I

    if-le p0, v0, :cond_0

    .line 87
    sget p0, Lcom/google/android/apps/gmm/map/b/a/be;->d:I

    .line 89
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/be;->e:[Lcom/google/android/apps/gmm/map/b/a/be;

    add-int/lit8 v1, p0, -0x1

    aget-object v0, v0, v1

    .line 92
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
