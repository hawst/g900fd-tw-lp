.class public Lcom/google/android/apps/gmm/car/i/h;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:F

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/gmm/car/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/car/i/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/car/c;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/h;->c:Landroid/content/Context;

    .line 61
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/h;->d:Lcom/google/android/apps/gmm/car/c;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bg;Ljava/util/List;Lcom/google/android/apps/gmm/map/r/b/a;I)Ljava/util/List;
    .locals 16
    .param p3    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/car/bg;",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Lcom/google/android/apps/gmm/map/r/b/a;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/bg;->c:Ljava/util/List;

    sget-object v2, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x28

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "homeWork list contains "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/h;->d:Lcom/google/android/apps/gmm/car/c;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/car/c;->a()Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc

    if-ge v2, v3, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/map/k/b;->c:Lcom/google/android/apps/gmm/map/k/b;

    move-object v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/google/android/apps/gmm/map/k/a;

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    if-ne v2, v3, :cond_0

    if-eqz p3, :cond_6

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/k/a;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    const v5, 0x44bb8000    # 1500.0f

    cmpg-float v5, v2, v5

    if-ltz v5, :cond_1

    const v5, 0x48c70600    # 407600.0f

    cmpl-float v5, v2, v5

    if-lez v5, :cond_3

    :cond_1
    sget-object v5, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    float-to-int v2, v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x28

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " removed due to distance ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " m)"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :sswitch_0
    sget-object v2, Lcom/google/android/apps/gmm/map/k/b;->b:Lcom/google/android/apps/gmm/map/k/b;

    move-object v3, v2

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/map/k/b;->b:Lcom/google/android/apps/gmm/map/k/b;

    move-object v3, v2

    goto :goto_0

    :cond_3
    sget-object v4, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    float-to-int v2, v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x25

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " meters away, showing it."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/h;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    sget-object v4, Lcom/google/android/apps/gmm/map/k/b;->b:Lcom/google/android/apps/gmm/map/k/b;

    if-ne v3, v4, :cond_7

    sget v3, Lcom/google/android/apps/gmm/l;->gV:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v2

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/k/a;->a:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/k/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/k/a;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v7, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v7

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v10

    invoke-direct {v4, v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    new-instance v2, Lcom/google/android/apps/gmm/car/bm;

    iget-object v4, v6, Lcom/google/android/apps/gmm/map/k/a;->a:Ljava/lang/String;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/k/a;->a:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    move-object v13, v2

    .line 83
    :goto_4
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 84
    if-eqz v13, :cond_4

    .line 85
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/bg;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v2

    .line 89
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/google/o/h/a/gp;

    .line 90
    if-lez p4, :cond_5

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p4

    if-ge v2, v0, :cond_14

    .line 91
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/i/h;->c:Landroid/content/Context;

    if-nez v3, :cond_9

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 82
    :cond_6
    sget-object v2, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Don\'t have location fix, showing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_7
    sget v3, Lcom/google/android/apps/gmm/l;->pc:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    move-object v13, v2

    goto :goto_4

    .line 91
    :cond_9
    iget-object v2, v6, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/a;

    iget-object v2, v2, Lcom/google/o/h/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ph;->g()Lcom/google/o/h/a/ph;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/ph;

    invoke-static {v2}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    iget-object v2, v6, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v7

    iget-object v2, v6, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v8

    if-lez v7, :cond_a

    const/4 v2, 0x0

    iget-object v5, v6, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v5, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v5, v2

    :goto_6
    const/4 v2, 0x1

    if-le v7, v2, :cond_c

    const/4 v2, 0x1

    iget-object v6, v6, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v6, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v6, v2

    :goto_7
    new-instance v2, Lcom/google/android/apps/gmm/car/bm;

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    .line 94
    if-eqz p3, :cond_f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/car/i/h;->a:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_f

    .line 95
    iget-object v3, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v3, :cond_f

    .line 97
    iget-object v3, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v4, 0x1

    new-array v12, v4, [F

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    iget-wide v8, v3, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v10, v3, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    const/4 v3, 0x0

    aget v3, v12, v3

    .line 98
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/car/i/h;->a:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_f

    .line 99
    sget-object v4, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2a

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\' too close ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "m), removing."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 91
    :cond_a
    if-lez v8, :cond_b

    const/4 v2, 0x0

    iget-object v5, v6, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v5, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v5, v2

    goto :goto_6

    :cond_b
    const/4 v2, 0x0

    move-object v5, v2

    goto/16 :goto_6

    :cond_c
    const/4 v2, 0x1

    if-ne v7, v2, :cond_d

    const/4 v2, 0x0

    :goto_8
    if-le v8, v2, :cond_e

    iget-object v6, v6, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v6, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v6, v2

    goto/16 :goto_7

    :cond_d
    const/4 v2, 0x1

    goto :goto_8

    :cond_e
    const/4 v2, 0x0

    move-object v6, v2

    goto/16 :goto_7

    .line 105
    :cond_f
    if-nez v13, :cond_10

    const/high16 v3, -0x40800000    # -1.0f

    .line 106
    :goto_9
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_13

    const/high16 v4, 0x42480000    # 50.0f

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_13

    .line 107
    sget-object v4, Lcom/google/android/apps/gmm/car/i/h;->b:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    .line 108
    iget-object v4, v13, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x3d

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Removed \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\' due to distance to home/work \'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\' ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "m)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 105
    :cond_10
    iget-object v3, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v4, v13, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v10, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v3, :cond_11

    if-nez v10, :cond_12

    :cond_11
    const/high16 v3, -0x40800000    # -1.0f

    goto :goto_9

    :cond_12
    const/4 v4, 0x1

    new-array v12, v4, [F

    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v8, v10, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v10, v10, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v4 .. v12}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v3, 0x0

    aget v3, v12, v3

    goto :goto_9

    .line 111
    :cond_13
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 113
    :cond_14
    return-object v14

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7 -> :sswitch_0
    .end sparse-switch
.end method
