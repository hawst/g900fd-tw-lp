.class public interface abstract Lcom/google/android/apps/gmm/map/internal/c/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final s:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "UNUSED"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TYPE_EMPTY_FEATURE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TYPE_ROAD"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TYPE_AREA"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "TYPE_BUILDING"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TYPE_LINE_MESH"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "TYPE_RASTER"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TYPE_POINT_OF_INTEREST"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TYPE_LINE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TYPE_TRAFFIC_ROAD"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "UNUSED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TYPE_LINE_LABEL"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TYPE_STRIPIFIED_MESH"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TYPE_SHADER_OP"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/m;->s:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract a()J
.end method

.method public abstract b()I
.end method

.method public abstract c()I
.end method

.method public abstract d()I
.end method

.method public abstract e()Lcom/google/android/apps/gmm/map/b/a/j;
.end method

.method public abstract g()Lcom/google/android/apps/gmm/map/internal/c/be;
.end method

.method public abstract h()I
.end method

.method public abstract i()I
.end method

.method public abstract j()[I
.end method

.method public abstract k()I
.end method
