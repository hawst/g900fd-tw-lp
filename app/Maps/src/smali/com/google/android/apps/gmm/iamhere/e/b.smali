.class public Lcom/google/android/apps/gmm/iamhere/e/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field private final c:F


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget v0, Lcom/google/android/apps/gmm/e;->bA:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 24
    sget v1, Lcom/google/android/apps/gmm/e;->ar:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 26
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->a:Landroid/graphics/Paint;

    .line 27
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->a:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/gmm/d;->e:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 30
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->b:Landroid/graphics/Paint;

    .line 31
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->b:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/gmm/d;->aR:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->b:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 35
    sget v0, Lcom/google/android/apps/gmm/e;->az:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->c:F

    .line 36
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v7, 0x43af0000    # 350.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/16 v5, 0x15e

    .line 49
    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 50
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->a:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, p1, v3, v4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 51
    const/16 v2, 0x15e

    const/16 v3, 0x15e

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 52
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 53
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-le v5, v4, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    rsub-int v0, v0, 0x15e

    div-int/lit8 v0, v0, 0x2

    .line 57
    :cond_0
    int-to-float v1, v0

    iget v4, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->c:F

    sub-float v4, v7, v4

    div-float/2addr v4, v6

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->b:Landroid/graphics/Paint;

    invoke-virtual {v3, p1, v1, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 58
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->c:F

    sub-float v1, v7, v1

    div-float/2addr v1, v6

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/e/b;->a:Landroid/graphics/Paint;

    invoke-virtual {v3, p1, v0, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit p0

    return-object v2

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
