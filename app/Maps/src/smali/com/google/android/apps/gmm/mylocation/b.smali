.class public Lcom/google/android/apps/gmm/mylocation/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/gmm/mylocation/b/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/android/apps/gmm/base/a;

.field private final d:Lcom/google/android/apps/gmm/base/j/b;

.field private e:Lcom/google/android/apps/gmm/base/g/b;

.field private f:Lcom/google/android/apps/gmm/map/r/a/ap;

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/place/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/android/apps/gmm/directions/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private i:Lcom/google/android/apps/gmm/directions/a/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private j:J

.field private k:Lcom/google/android/apps/gmm/map/r/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private l:Lcom/google/android/apps/gmm/mylocation/d;

.field private m:Lcom/google/maps/g/a/hm;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/google/android/apps/gmm/mylocation/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/j/b;)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->a:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 128
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    .line 129
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/b;->d:Lcom/google/android/apps/gmm/base/j/b;

    .line 130
    return-void
.end method

.method private e()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v0

    .line 319
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/mylocation/b;->j:J

    sub-long/2addr v2, v4

    .line 320
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const-wide/32 v4, 0x2bf20

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v1

    .line 325
    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/b;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v2, :cond_0

    .line 328
    const/high16 v2, 0x41c80000    # 25.0f

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceTo(Landroid/location/Location;)F

    move-result v1

    cmpg-float v1, v2, v1

    if-ltz v1, :cond_0

    .line 331
    const/4 v0, 0x1

    goto :goto_0

    .line 324
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/b/a;

    .line 377
    if-eqz v0, :cond_0

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/g/b;->d()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/b/a;->a(Ljava/lang/Integer;)V

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/g/b;->c()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->fq:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    :pswitch_0
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/place/b/a;->a(Ljava/lang/String;)V

    .line 382
    :cond_0
    return-void

    .line 378
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/c;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/mylocation/d;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x10

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/d/f;->b(Lcom/google/maps/g/a/hm;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    goto :goto_2

    :pswitch_2
    sget-object v1, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    goto :goto_2

    .line 379
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/c;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/mylocation/d;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_1

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x10

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/b;->n:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->fL:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 378
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 379
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->a:Lcom/google/android/apps/gmm/mylocation/d;

    if-ne v0, v1, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->d:Lcom/google/android/apps/gmm/base/j/b;

    .line 159
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 160
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->h:Lcom/google/android/apps/gmm/directions/a/a;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    .line 162
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->h:Lcom/google/android/apps/gmm/directions/a/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    if-nez v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->h:Lcom/google/android/apps/gmm/directions/a/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/a;->a()Lcom/google/maps/g/a/hm;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    if-nez v1, :cond_3

    .line 167
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b;->a:Ljava/lang/String;

    const-string v1, "TravelMode is not available."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->d:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    .line 183
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->o:Z

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->f()V

    .line 186
    return-void

    .line 161
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 169
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-wide v2, 0x4122ebc000000000L    # 620000.0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 173
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    cmpg-double v0, v2, v0

    if-gez v0, :cond_1

    .line 174
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->d:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/b;)V
    .locals 1

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 135
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/g/b;->e()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 136
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/directions/b/b;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 339
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/b/b;->a:Lcom/google/android/apps/gmm/directions/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    if-eq v0, v1, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->a:Lcom/google/android/apps/gmm/mylocation/d;

    if-ne v0, v1, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->k()Lcom/google/maps/g/a/be;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v1

    if-nez v1, :cond_2

    if-nez v0, :cond_3

    .line 350
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->c:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    .line 351
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->f()V

    goto :goto_0

    .line 355
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->l()Lcom/google/maps/g/a/hm;

    move-result-object v1

    .line 356
    if-nez v1, :cond_4

    .line 357
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b;->a:Ljava/lang/String;

    const-string v2, "Unknown travel to display."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    .line 365
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/be;Lcom/google/android/apps/gmm/shared/c/c/m;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 366
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->n:Ljava/lang/String;

    .line 367
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->b:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    .line 368
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->f()V

    goto :goto_0

    .line 359
    :cond_4
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/place/b/a;)V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->g:Ljava/lang/ref/WeakReference;

    .line 142
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/place/b/a;->a(Landroid/view/View$OnClickListener;)V

    .line 143
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/place/b/a;->a(Landroid/view/View$OnLongClickListener;)V

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->f()V

    .line 146
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->o:Z

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 192
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->o:Z

    return v0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget-boolean v0, v0, Lcom/google/r/b/a/ou;->P:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/d;->a:Lcom/google/android/apps/gmm/mylocation/d;

    if-ne v0, v1, :cond_0

    .line 216
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 218
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/g/b;->c()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->h:Lcom/google/android/apps/gmm/directions/a/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-nez v0, :cond_3

    .line 223
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->c:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->f()V

    goto :goto_0

    .line 230
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->h:Lcom/google/android/apps/gmm/directions/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 232
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/g/b;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 233
    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/g/b;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    .line 230
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/hy;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/directions/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l_()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    if-nez v0, :cond_5

    .line 237
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/d;->c:Lcom/google/android/apps/gmm/mylocation/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->l:Lcom/google/android/apps/gmm/mylocation/d;

    .line 238
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->f()V

    goto :goto_0

    .line 242
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->j:J

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 248
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->o:Z

    if-nez v0, :cond_0

    .line 276
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/g/b;->c()Landroid/content/Intent;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_1

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 257
    :catch_0
    move-exception v0

    .line 258
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->d:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 266
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 267
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->p()V

    .line 268
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;)V

    goto :goto_0

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 273
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/g/b;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;

    move-result-object v7

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 274
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/g/b;->b()Ljava/lang/String;

    move-result-object v8

    move-object v4, v2

    move-object v6, v2

    .line 271
    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 280
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->o:Z

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 306
    :goto_0
    return v0

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/b;->d:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 291
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/b;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->p()V

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->i:Lcom/google/android/apps/gmm/directions/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 306
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/b;->m:Lcom/google/maps/g/a/hm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/b;->c:Lcom/google/android/apps/gmm/base/a;

    .line 298
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/b;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 303
    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/g/b;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;

    move-result-object v7

    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/b;->e:Lcom/google/android/apps/gmm/base/g/b;

    .line 304
    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/g/b;->b()Ljava/lang/String;

    move-result-object v8

    move-object v6, v4

    .line 296
    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto :goto_1
.end method
