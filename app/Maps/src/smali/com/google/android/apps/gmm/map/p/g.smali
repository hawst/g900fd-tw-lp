.class public Lcom/google/android/apps/gmm/map/p/g;
.super Lcom/google/android/apps/gmm/map/p/f;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/p/f",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/SharedPreferences;)V
    .locals 5

    .prologue
    .line 82
    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/p/f;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/p/f;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 93
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/p/g;->a(Lcom/google/android/apps/gmm/map/util/b/g;Ljava/lang/Object;)V

    .line 94
    return-void

    .line 85
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/p/f;->d:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/p/f;->b:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit16 v3, v3, 0x82

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "The type of Preference used within lab "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is incompatible with the Lab\'s type T. Using the lab\'s default value "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " instead of crashing."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/p/g;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    goto :goto_0

    .line 90
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/p/g;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/p/f;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ": number"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
