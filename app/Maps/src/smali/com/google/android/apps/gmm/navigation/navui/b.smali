.class public Lcom/google/android/apps/gmm/navigation/navui/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final m:[J


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Landroid/app/NotificationManager;

.field final d:Ljava/lang/Object;

.field e:Lcom/google/android/apps/gmm/navigation/g/b/f;

.field f:Z

.field g:Lcom/google/android/apps/gmm/navigation/navui/e;

.field h:Lcom/google/android/apps/gmm/navigation/j/a/a;

.field i:Z

.field j:Z

.field final k:Ljava/lang/Object;

.field final l:Ljava/lang/Runnable;

.field private final n:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/b;->a:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/b;->m:[J

    return-void
.end method

.method public constructor <init>(Landroid/app/Service;Lcom/google/android/apps/gmm/base/a;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/e;->a:Lcom/google/android/apps/gmm/navigation/navui/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->g:Lcom/google/android/apps/gmm/navigation/navui/e;

    .line 289
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/c;-><init>(Lcom/google/android/apps/gmm/navigation/navui/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->k:Ljava/lang/Object;

    .line 374
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/d;-><init>(Lcom/google/android/apps/gmm/navigation/navui/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->l:Ljava/lang/Runnable;

    .line 94
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    .line 96
    const-string v0, "notification"

    .line 97
    invoke-virtual {p1, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    .line 98
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ag;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/16 v2, 0x80

    const/4 v5, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/gmm/directions/views/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ag;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 134
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 136
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 137
    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 138
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 140
    return-object v1
.end method

.method private a(I)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 192
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 197
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v0

    move v1, p1

    move v4, v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, v5

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/car/a/i;Landroid/app/Notification;)V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/a/g;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/car/a/g;->b:Lcom/google/android/apps/gmm/car/a/h;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/a/g;->b:Lcom/google/android/apps/gmm/car/a/h;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/car/a/h;->a(Lcom/google/android/apps/gmm/car/a/i;Landroid/app/Notification;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    iget v1, p1, Lcom/google/android/apps/gmm/car/a/i;->c:I

    invoke-virtual {v0, v1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 287
    return-void

    .line 283
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/navui/b;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 40
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/b;->a:Ljava/lang/String;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v0, v3, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/j/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    new-instance v3, Landroid/support/v4/app/ax;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/support/v4/app/ax;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ax;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/navigation/j/a/a;->b:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ax;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->fV:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/ax;->a(I)Landroid/support/v4/app/ax;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/ax;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/ax;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ax;->b(I)Landroid/support/v4/app/ax;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/d;->x:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/support/v4/app/ax;->c(I)Landroid/support/v4/app/ax;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/ax;->a(J)Landroid/support/v4/app/ax;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    const-class v5, Lcom/google/android/maps/MapsActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    const/high16 v5, 0x8000000

    invoke-static {v4, v1, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ax;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/ax;->d(I)Landroid/support/v4/app/ax;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->i:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/b;->m:[J

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ax;->a([J)Landroid/support/v4/app/ax;

    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/b;->a:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    invoke-virtual {v0}, Landroid/support/v4/app/ax;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(Lcom/google/android/apps/gmm/car/a/i;Landroid/app/Notification;)V

    :cond_1
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->i:Z

    if-nez v0, :cond_3

    move v0, v1

    goto/16 :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->f:Z

    if-eqz v0, :cond_4

    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/j/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v4, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-eq v3, v4, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v3, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    if-eq v0, v3, :cond_5

    move v0, v1

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/map/r/a/ag;)Ljava/lang/String;
    .locals 16

    .prologue
    .line 144
    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/ay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    .line 145
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    .line 146
    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    .line 147
    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/ay;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;)V

    .line 148
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Lcom/google/android/apps/gmm/navigation/navui/az;

    move-result-object v15

    .line 150
    iget-object v2, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 188
    :goto_0
    return-object v1

    .line 157
    :cond_0
    iget-object v2, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    const/4 v3, 0x1

    const v4, 0x7fffffff

    const/4 v5, 0x0

    iget v6, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->c:I

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v13}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;IILandroid/text/TextPaint;IZIZFFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 169
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    .line 170
    iget-object v2, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->b:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 171
    iget-object v2, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->b:Ljava/util/Collection;

    const/4 v3, 0x1

    const v4, 0x7fffffff

    const/4 v5, 0x0

    iget v6, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->d:I

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v13}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;IILandroid/text/TextPaint;IZIZFFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 183
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->gS:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v14, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, v14

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/navigation/navui/b;)V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/support/v4/app/ax;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/ax;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ax;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ax;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->d:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/ax;->a(I)Landroid/support/v4/app/ax;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ax;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ax;->a()Landroid/app/Notification;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/car/a/i;->b:Lcom/google/android/apps/gmm/car/a/i;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(Lcom/google/android/apps/gmm/car/a/i;Landroid/app/Notification;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/b;->a:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->k:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    .line 119
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 118
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->b:Lcom/google/android/apps/gmm/car/a/i;

    .line 121
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 120
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 122
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/e;->a:Lcom/google/android/apps/gmm/navigation/navui/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->g:Lcom/google/android/apps/gmm/navigation/navui/e;

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b;->j:Z

    .line 124
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
