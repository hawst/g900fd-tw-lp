.class public Lcom/google/android/apps/gmm/navigation/navui/views/d;
.super Lcom/google/android/apps/gmm/navigation/navui/views/a;
.source "PG"


# instance fields
.field final d:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/views/e;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field e:Landroid/graphics/Bitmap;

.field final f:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/views/e;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/res/Resources;

.field private final h:Landroid/content/Context;

.field private final i:Z

.field private final j:F

.field private final k:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/a/j;Landroid/content/Context;ZFF)V
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/navui/views/a;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;)V

    .line 179
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 180
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->d:Ljava/util/EnumMap;

    .line 185
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->f:Ljava/util/EnumMap;

    .line 198
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->h:Landroid/content/Context;

    .line 199
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    .line 200
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->i:Z

    .line 201
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->j:F

    .line 202
    iput p5, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->k:F

    .line 203
    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;)F
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/high16 v8, 0x437f0000    # 255.0f

    .line 399
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 402
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    invoke-static {p0, v3, v0, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 406
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    if-eq v1, v2, :cond_0

    .line 407
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->extractAlpha()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 410
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 411
    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 413
    const/4 v1, -0x1

    .line 414
    const/4 v0, 0x0

    .line 416
    :goto_0
    if-ge v3, v4, :cond_4

    .line 417
    :goto_1
    if-ge v3, v4, :cond_1

    aget-byte v2, v5, v3

    if-nez v2, :cond_1

    .line 418
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v2, v3

    .line 421
    :goto_2
    if-ge v2, v4, :cond_2

    aget-byte v6, v5, v2

    if-eqz v6, :cond_2

    .line 422
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 424
    :cond_2
    add-int/lit8 v6, v2, -0x1

    .line 425
    sub-int v7, v6, v3

    if-le v7, v1, :cond_3

    .line 426
    sub-int v1, v6, v3

    .line 431
    add-int/lit8 v0, v3, 0x1

    int-to-float v0, v0

    aget-byte v3, v5, v3

    and-int/lit16 v3, v3, 0xff

    int-to-float v3, v3

    div-float/2addr v3, v8

    sub-float/2addr v0, v3

    .line 432
    int-to-float v3, v6

    aget-byte v6, v5, v6

    and-int/lit16 v6, v6, 0xff

    int-to-float v6, v6

    div-float/2addr v6, v8

    add-float/2addr v3, v6

    .line 433
    add-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    :cond_3
    move v3, v2

    .line 435
    goto :goto_0

    .line 436
    :cond_4
    return v0
.end method

.method private static a(ILandroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 332
    if-nez p0, :cond_0

    .line 334
    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 335
    invoke-virtual {v0, p2}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 390
    :goto_0
    return-object v0

    .line 339
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 342
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 345
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 346
    invoke-static {p1, p0, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 347
    if-nez v2, :cond_2

    .line 348
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Couldn\'t load resource #"

    .line 349
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 352
    :cond_2
    int-to-float v0, p2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v0, v3

    .line 358
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 359
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 360
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 363
    invoke-virtual {v4, v10, v8}, Landroid/graphics/Canvas;->scale(FF)V

    .line 364
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v4, v9, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 365
    const/4 v7, 0x0

    invoke-virtual {v4, v2, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 368
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 371
    invoke-virtual {v5, v3, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 373
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    .line 372
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 375
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 380
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    .line 379
    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 381
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 384
    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 385
    invoke-virtual {v2, v10, v8}, Landroid/graphics/Canvas;->scale(FF)V

    .line 386
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v2, v9, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 387
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 389
    invoke-virtual {v0, p2}, Landroid/graphics/Bitmap;->setDensity(I)V

    goto/16 :goto_0
.end method

.method public static b()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 154
    const-wide/high16 v0, 0x4040000000000000L    # 32.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static c()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 161
    const-wide/high16 v0, 0x4018000000000000L    # 6.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method private d()V
    .locals 7

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 229
    invoke-static {}, Lcom/google/android/apps/gmm/navigation/navui/views/e;->values()[Lcom/google/android/apps/gmm/navigation/navui/views/e;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 230
    iget v5, v4, Lcom/google/android/apps/gmm/navigation/navui/views/e;->l:I

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    invoke-static {v5, v6, v1}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->a(ILandroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 231
    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->d:Ljava/util/EnumMap;

    invoke-virtual {v6, v4, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->f:Ljava/util/EnumMap;

    invoke-static {v5}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->a(Landroid/graphics/Bitmap;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->aQ:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->a(ILandroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    .line 235
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 15

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 211
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->i:Z

    if-eqz v0, :cond_7

    .line 212
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v6

    invoke-static {}, Lcom/google/android/apps/gmm/navigation/navui/views/e;->values()[Lcom/google/android/apps/gmm/navigation/navui/views/e;

    move-result-object v7

    array-length v8, v7

    move v3, v1

    move v4, v5

    :goto_0
    if-ge v3, v8, :cond_2

    aget-object v9, v7, v3

    iget-object v10, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    iget v11, v9, Lcom/google/android/apps/gmm/navigation/navui/views/e;->m:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    sget v12, Lcom/google/android/apps/gmm/d;->aR:I

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A()Lcom/google/android/apps/gmm/map/c/c;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    new-instance v13, Lcom/google/android/apps/gmm/util/m;

    invoke-direct {v13, v10, v11, v12}, Lcom/google/android/apps/gmm/util/m;-><init>(Landroid/content/res/Resources;II)V

    invoke-virtual {v0, v10, v11, v12, v13}, Lcom/google/android/apps/gmm/map/c/c;->b(Landroid/content/res/Resources;IILcom/google/b/a/bw;)Landroid/graphics/Picture;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Landroid/graphics/Picture;->getHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v6, v9, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    goto :goto_1

    :cond_1
    invoke-static {v10, v11, v12}, Lcom/google/android/apps/gmm/util/l;->a(Landroid/content/res/Resources;II)Lcom/a/a/b;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/b;->a:Landroid/graphics/Picture;

    goto :goto_2

    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->j:F

    div-float v4, v0, v4

    invoke-static {}, Lcom/google/android/apps/gmm/navigation/navui/views/e;->values()[Lcom/google/android/apps/gmm/navigation/navui/views/e;

    move-result-object v7

    array-length v8, v7

    move v3, v1

    :goto_3
    if-ge v3, v8, :cond_3

    aget-object v9, v7, v3

    invoke-virtual {v6, v9}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Picture;

    new-instance v10, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Picture;->getWidth()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v11, v4

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-virtual {v0}, Landroid/graphics/Picture;->getHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v12, v4

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-direct {v10, v1, v1, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v11, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v12

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    new-instance v12, Landroid/graphics/Canvas;

    invoke-direct {v12, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v12, v0, v10}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->d:Ljava/util/EnumMap;

    invoke-virtual {v0, v9, v11}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->f:Ljava/util/EnumMap;

    invoke-static {v11}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->a(Landroid/graphics/Bitmap;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/apps/gmm/k;->o:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    sget v7, Lcom/google/android/apps/gmm/d;->aR:I

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A()Lcom/google/android/apps/gmm/map/c/c;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_6

    new-instance v2, Lcom/google/android/apps/gmm/util/m;

    invoke-direct {v2, v3, v6, v7}, Lcom/google/android/apps/gmm/util/m;-><init>(Landroid/content/res/Resources;II)V

    invoke-virtual {v0, v3, v6, v7, v2}, Lcom/google/android/apps/gmm/map/c/c;->b(Landroid/content/res/Resources;IILcom/google/b/a/bw;)Landroid/graphics/Picture;

    move-result-object v0

    :goto_5
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Picture;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Picture;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v2, v1, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->g:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v0, v2}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;Landroid/graphics/Rect;)V

    .line 217
    :goto_6
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->k:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_4

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->k:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_4

    .line 219
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 220
    const/high16 v0, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->k:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    .line 222
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    .line 221
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/d;->e:Landroid/graphics/Bitmap;

    .line 225
    :cond_4
    return-void

    :cond_5
    move-object v0, v2

    .line 212
    goto/16 :goto_4

    :cond_6
    invoke-static {v3, v6, v7}, Lcom/google/android/apps/gmm/util/l;->a(Landroid/content/res/Resources;II)Lcom/a/a/b;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/b;->a:Landroid/graphics/Picture;

    goto :goto_5

    .line 214
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->d()V

    goto :goto_6
.end method
