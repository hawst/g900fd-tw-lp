.class public final enum Lcom/google/android/apps/gmm/directions/option/m;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/option/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/option/m;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/option/m;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/option/m;

.field public static final enum d:Lcom/google/android/apps/gmm/directions/option/m;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/directions/option/m;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/directions/option/m;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/directions/option/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/option/m;->a:Lcom/google/android/apps/gmm/directions/option/m;

    new-instance v0, Lcom/google/android/apps/gmm/directions/option/m;

    const-string v1, "EMPTY_HEADER"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/directions/option/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/option/m;->b:Lcom/google/android/apps/gmm/directions/option/m;

    new-instance v0, Lcom/google/android/apps/gmm/directions/option/m;

    const-string v1, "BOOLEAN_OPTION"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/gmm/directions/option/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/option/m;->c:Lcom/google/android/apps/gmm/directions/option/m;

    new-instance v0, Lcom/google/android/apps/gmm/directions/option/m;

    const-string v1, "CHOICE_OPTION"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/apps/gmm/directions/option/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/option/m;->d:Lcom/google/android/apps/gmm/directions/option/m;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/option/m;

    sget-object v1, Lcom/google/android/apps/gmm/directions/option/m;->a:Lcom/google/android/apps/gmm/directions/option/m;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/option/m;->b:Lcom/google/android/apps/gmm/directions/option/m;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/directions/option/m;->c:Lcom/google/android/apps/gmm/directions/option/m;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/directions/option/m;->d:Lcom/google/android/apps/gmm/directions/option/m;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/directions/option/m;->f:[Lcom/google/android/apps/gmm/directions/option/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/google/android/apps/gmm/directions/option/m;->e:I

    .line 48
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/apps/gmm/directions/option/m;->values()[Lcom/google/android/apps/gmm/directions/option/m;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/option/m;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/directions/option/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/option/m;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/option/m;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/m;->f:[Lcom/google/android/apps/gmm/directions/option/m;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/option/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/option/m;

    return-object v0
.end method
