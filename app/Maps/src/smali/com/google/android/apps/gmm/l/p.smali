.class public Lcom/google/android/apps/gmm/l/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public b:Lcom/google/android/apps/gmm/l/o;

.field private final d:Lcom/google/android/apps/gmm/l/i;

.field private final e:Lcom/google/android/apps/gmm/l/am;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/l/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/i;Lcom/google/android/apps/gmm/l/am;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/gmm/l/p;->d:Lcom/google/android/apps/gmm/l/i;

    .line 40
    iput-object p3, p0, Lcom/google/android/apps/gmm/l/p;->e:Lcom/google/android/apps/gmm/l/am;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/o;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x13

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Considering intent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/p;->e:Lcom/google/android/apps/gmm/l/am;

    sget-object v4, Lcom/google/android/apps/gmm/l/am;->a:Ljava/lang/String;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/l/am;->c:Z

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x1b

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Is in projected mode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/l/am;->c:Z

    if-eqz v4, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/am;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v4, Lcom/google/android/apps/gmm/l/n;

    invoke-direct {v4, p1}, Lcom/google/android/apps/gmm/l/n;-><init>(Landroid/content/Intent;)V

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    move-object v0, v1

    .line 170
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 61
    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v4, 0x100000

    and-int/2addr v0, v4

    if-eqz v0, :cond_2

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 67
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    move-object v0, v1

    .line 68
    goto :goto_1

    :cond_2
    move v0, v2

    .line 66
    goto :goto_2

    .line 81
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 83
    if-nez v0, :cond_4

    const-string v0, ""

    .line 84
    :goto_3
    const-string v4, "DestinationActivity"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/l/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/l/q;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/l/q;-><init>(Lcom/google/android/apps/gmm/l/p;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/l/aq;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 83
    :cond_4
    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 92
    :cond_5
    const-string v4, "PlacesActivity"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 93
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/l/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/l/r;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/l/r;-><init>(Lcom/google/android/apps/gmm/l/p;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/l/aq;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/Runnable;)V

    goto :goto_1

    .line 102
    :cond_6
    const-string v0, "com.google.android.gms.actions.SEARCH_ACTION"

    .line 103
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/l/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->d:Lcom/google/android/apps/gmm/l/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/l/b;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/l/i;Landroid/content/Context;)V

    goto :goto_1

    .line 107
    :cond_7
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/bg;->c(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 127
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 128
    new-instance v0, Lcom/google/android/apps/gmm/l/be;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/p;->d:Lcom/google/android/apps/gmm/l/i;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/l/be;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/i;)V

    goto :goto_1

    .line 131
    :cond_8
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/i;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    .line 133
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    move v0, v3

    :goto_4
    if-eqz v0, :cond_b

    .line 134
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    move-object v0, v1

    .line 135
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 133
    goto :goto_4

    .line 138
    :cond_b
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 140
    const-string v1, "googlenav.tos"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 142
    new-instance v0, Lcom/google/android/apps/gmm/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/l/aj;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/base/activities/c;)V

    goto/16 :goto_1

    .line 145
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 154
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->j()Lcom/google/android/apps/gmm/shared/net/a/i;

    move-result-object v0

    .line 153
    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/invocation/a/c;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/net/a/i;)Z

    move-result v0

    .line 157
    if-eqz v0, :cond_d

    .line 158
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/l/au;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 160
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->n()Lcom/google/android/apps/gmm/invocation/a/b;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/l/au;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/invocation/a/b;)V

    goto/16 :goto_1

    .line 164
    :cond_d
    invoke-static {v4}, Lcom/google/android/apps/gmm/invocation/a/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 165
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 166
    new-instance v0, Lcom/google/android/apps/gmm/l/aw;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, p1, v4, v1}, Lcom/google/android/apps/gmm/l/aw;-><init>(Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V

    goto/16 :goto_1

    .line 169
    :cond_e
    sget-object v0, Lcom/google/android/apps/gmm/l/p;->c:Ljava/lang/String;

    .line 170
    new-instance v0, Lcom/google/android/apps/gmm/l/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/p;->d:Lcom/google/android/apps/gmm/l/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/p;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/l/b;-><init>(Landroid/content/Intent;Lcom/google/android/apps/gmm/l/i;Landroid/content/Context;)V

    goto/16 :goto_1
.end method
