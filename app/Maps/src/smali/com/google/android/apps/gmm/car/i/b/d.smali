.class public Lcom/google/android/apps/gmm/car/i/b/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/i/b/c;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/car/ad;

.field private final b:Lcom/google/android/apps/gmm/car/bm;

.field private final c:Lcom/google/android/apps/gmm/car/i/b/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/i/b/e;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/ad;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/b/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 26
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/bm;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    .line 27
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/i/b/e;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/i/b/d;->c:Lcom/google/android/apps/gmm/car/i/b/e;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/d/h;->e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/h;->e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;

    move-result-object v0

    .line 48
    :goto_0
    if-eqz v0, :cond_1

    .line 49
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/d;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 50
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    .line 49
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/be;Lcom/google/android/apps/gmm/shared/c/c/m;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    :goto_1
    return-object v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 53
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/b/d;->c:Lcom/google/android/apps/gmm/car/i/b/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/b/d;->b:Lcom/google/android/apps/gmm/car/bm;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/i/b/e;->a(Lcom/google/android/apps/gmm/car/bm;)V

    .line 60
    const/4 v0, 0x0

    return-object v0
.end method
