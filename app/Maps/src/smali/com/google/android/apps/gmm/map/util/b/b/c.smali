.class public Lcom/google/android/apps/gmm/map/util/b/b/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/b/e;

.field private final b:Lcom/google/android/apps/gmm/map/util/b/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/h;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/b/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/util/b/b/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/b/c;->a:Lcom/google/android/apps/gmm/map/util/b/b/e;

    .line 96
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/util/b/b/c;->b:Lcom/google/android/apps/gmm/map/util/b/h;

    .line 97
    return-void
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/map/util/b/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    :try_start_0
    const-class v0, Lcom/google/android/apps/gmm/map/util/b/b/a;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 199
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Class;Lcom/google/android/apps/gmm/map/util/b/b/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/map/util/b/b/a;",
            ">;",
            "Lcom/google/android/apps/gmm/map/util/b/b/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 247
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/util/b/b/c;->b(Ljava/lang/Class;Lcom/google/android/apps/gmm/map/util/b/b/b;)Lcom/google/android/apps/gmm/map/util/b/b/a;

    move-result-object v0

    .line 248
    sget-object v1, Lcom/google/android/apps/gmm/map/util/b/b/b;->a:Lcom/google/android/apps/gmm/map/util/b/b/b;

    if-ne p2, v1, :cond_0

    iget v1, v0, Lcom/google/android/apps/gmm/map/util/b/b/a;->b:I

    if-nez v1, :cond_0

    .line 256
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/b/c;->b:Lcom/google/android/apps/gmm/map/util/b/h;

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/util/b/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/map/util/b/d;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/util/b/d;->a:Ljava/util/Set;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/map/util/b/h;->a(Ljava/lang/Object;Ljava/util/Set;)Z

    goto :goto_0
.end method

.method private b(Ljava/lang/Class;Lcom/google/android/apps/gmm/map/util/b/b/b;)Lcom/google/android/apps/gmm/map/util/b/b/a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/map/util/b/b/a;",
            ">;",
            "Lcom/google/android/apps/gmm/map/util/b/b/b;",
            ")",
            "Lcom/google/android/apps/gmm/map/util/b/b/a;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 275
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    .line 276
    invoke-virtual {p1, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 278
    const/4 v1, 0x0

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/b/a;

    .line 279
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/util/b/b/c;->b:Lcom/google/android/apps/gmm/map/util/b/h;

    .line 280
    const-class v1, Lcom/google/android/apps/gmm/map/util/b/a;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/util/b/a;

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/b/d;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/b/d;-><init>(Ljava/lang/Class;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 285
    instance-of v2, v1, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    .line 286
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    .line 295
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 296
    new-instance v2, Ljava/lang/Error;

    const-string v3, "While instantiating "

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 280
    :cond_0
    :try_start_3
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/util/b/a;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/b/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/map/util/b/d;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/b/d;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/util/b/h;->a:Lcom/google/b/c/jo;

    invoke-interface {v5, v1}, Lcom/google/b/c/jo;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v1, v2

    move v2, v1

    goto :goto_1

    .line 279
    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1

    .line 290
    :catch_2
    move-exception v0

    .line 291
    :try_start_4
    new-instance v2, Ljava/lang/Error;

    const-string v3, "While instantiating "

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-direct {v2, v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_1

    .line 279
    :cond_2
    :try_start_5
    iput-object p2, v0, Lcom/google/android/apps/gmm/map/util/b/b/a;->a:Lcom/google/android/apps/gmm/map/util/b/b/b;

    iput v2, v0, Lcom/google/android/apps/gmm/map/util/b/b/a;->b:I
    :try_end_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_1

    .line 281
    return-object v0

    .line 289
    :cond_3
    :try_start_6
    new-instance v2, Ljava/lang/Error;

    const-string v3, "While instantiating "

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {v2, v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 291
    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 292
    :catch_3
    move-exception v0

    .line 293
    new-instance v2, Ljava/lang/Error;

    const-string v3, "While instantiating "

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-direct {v2, v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_6
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_4

    .line 296
    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Set;)Lcom/google/android/apps/gmm/map/util/b/b/e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;)",
            "Lcom/google/android/apps/gmm/map/util/b/b/e;"
        }
    .end annotation

    .prologue
    .line 169
    new-instance v1, Lcom/google/android/apps/gmm/map/util/b/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/util/b/b/e;-><init>()V

    .line 171
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 172
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v3

    .line 173
    if-eqz v3, :cond_0

    .line 175
    const-class v0, Lcom/google/android/apps/gmm/map/util/b/a;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/map/util/b/b/d;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/b/d;-><init>(Ljava/lang/Class;)V

    throw v0

    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/util/b/a;->a()Ljava/lang/Class;

    move-result-object v0

    .line 179
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/b/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/map/util/b/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/b/d;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 180
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/c/a;->a(Ljava/lang/Object;)Lcom/google/b/c/hz;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/b/c/hz;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_2
    return-object v1
.end method

.method public final a(Ljava/util/Set;Lcom/google/android/apps/gmm/map/util/b/b/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;",
            "Lcom/google/android/apps/gmm/map/util/b/b/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 212
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/b/b/c;->a:Lcom/google/android/apps/gmm/map/util/b/b/e;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/util/b/b/e;->a(Ljava/lang/Object;)Lcom/google/b/c/hz;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/b/c/hz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 215
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/map/util/b/b/b;)V

    goto :goto_0

    .line 218
    :cond_1
    return-void
.end method

.method public final b(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 229
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_0

    .line 232
    sget-object v2, Lcom/google/android/apps/gmm/map/util/b/b/b;->a:Lcom/google/android/apps/gmm/map/util/b/b/b;

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/map/util/b/b/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/map/util/b/b/b;)V

    goto :goto_0

    .line 236
    :cond_1
    return-void
.end method
