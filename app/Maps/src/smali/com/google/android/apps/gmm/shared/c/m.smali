.class public Lcom/google/android/apps/gmm/shared/c/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile a:I

.field private static b:Lcom/google/android/apps/gmm/shared/a/a;

.field private static final c:Lcom/google/android/apps/gmm/shared/c/o;

.field private static d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 40
    sput v3, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/o;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/o;-><init>(Z)V

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/o;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/shared/c/o;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/m;->c:Lcom/google/android/apps/gmm/shared/c/o;

    .line 161
    sput-boolean v3, Lcom/google/android/apps/gmm/shared/c/m;->d:Z

    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 80
    :catch_0
    move-exception v1

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Error formatting log message: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string v0, ": ["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    array-length v3, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 87
    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 90
    :cond_0
    const-string v0, "]: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/a/a;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/gmm/shared/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 47
    sput-object p0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    .line 48
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/shared/c/o;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Throwable;",
            ">(",
            "Lcom/google/android/apps/gmm/shared/c/o;",
            "Ljava/lang/String;",
            "TT;)V^TT;"
        }
    .end annotation

    .prologue
    .line 296
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    add-int/lit16 v2, v1, 0x400

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move v1, v2

    goto :goto_0

    .line 297
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/shared/a/a;->a(Ljava/lang/Throwable;)V

    .line 304
    :cond_2
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 277
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->c:Lcom/google/android/apps/gmm/shared/c/o;

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/n;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/gmm/shared/c/n;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Lcom/google/android/apps/gmm/shared/c/o;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 278
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Throwable;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V^TT;"
        }
    .end annotation

    .prologue
    .line 260
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->c:Lcom/google/android/apps/gmm/shared/c/o;

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Lcom/google/android/apps/gmm/shared/c/o;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 261
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 378
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 379
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 336
    sget v0, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/a/a;->a(Ljava/lang/Throwable;)V

    .line 338
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 391
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 392
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 344
    sget v0, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/gmm/shared/c/m;->a:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/a/a;->a(Ljava/lang/Throwable;)V

    .line 346
    :cond_0
    return-void
.end method

.method public static varargs d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 405
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    if-eqz v0, :cond_0

    .line 356
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/m;->b:Lcom/google/android/apps/gmm/shared/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/shared/a/a;->b(Ljava/lang/Throwable;)V

    .line 358
    :cond_0
    return-void
.end method
