.class Lcom/google/android/apps/gmm/myplaces/b/w;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/e/a/a/a/b;

.field private b:Lcom/google/e/a/a/a/b;

.field private final c:Lcom/google/android/apps/gmm/myplaces/b/v;

.field private d:I


# direct methods
.method constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/b/v;)V
    .locals 2

    .prologue
    .line 316
    sget-object v0, Lcom/google/r/b/a/el;->bA:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ab;->e:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 318
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->a:Lcom/google/e/a/a/a/b;

    .line 319
    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->c:Lcom/google/android/apps/gmm/myplaces/b/v;

    .line 320
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 2

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->b:Lcom/google/e/a/a/a/b;

    .line 340
    const/4 v0, 0x1

    const/16 v1, 0x15

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->d:I

    .line 341
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->d:I

    packed-switch v0, :pswitch_data_0

    .line 349
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_0
    return-object v0

    .line 343
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 345
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    .line 347
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    return v0
.end method

.method protected final al_()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 356
    if-nez p1, :cond_5

    .line 357
    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->b:Lcom/google/e/a/a/a/b;

    iget-object v2, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {v3, v1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-nez v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->c:Lcom/google/android/apps/gmm/myplaces/b/v;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/b/v;->a()V

    .line 361
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->d:I

    if-nez v0, :cond_4

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->b:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x2

    .line 363
    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 364
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->c:Lcom/google/android/apps/gmm/myplaces/b/v;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/myplaces/b/v;->a(Ljava/lang/String;)V

    .line 371
    :goto_1
    return-void

    :cond_3
    move v2, v0

    .line 357
    goto :goto_0

    .line 366
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->c:Lcom/google/android/apps/gmm/myplaces/b/v;

    iget v1, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->d:I

    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/b/v;->a()V

    goto :goto_1

    .line 369
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->c:Lcom/google/android/apps/gmm/myplaces/b/v;

    iget v1, p0, Lcom/google/android/apps/gmm/myplaces/b/w;->d:I

    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/b/v;->a()V

    goto :goto_1
.end method
