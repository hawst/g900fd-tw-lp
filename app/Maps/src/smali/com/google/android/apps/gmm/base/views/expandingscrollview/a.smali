.class public final Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/view/MotionEvent;

.field private final b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;->a(Landroid/view/MotionEvent;)Z

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    .line 164
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 57
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 58
    if-eq v3, v8, :cond_9

    .line 60
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 61
    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    if-nez v3, :cond_1

    .line 64
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 68
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    if-nez v3, :cond_4

    move v3, v1

    :cond_2
    :goto_0
    if-ge v3, v0, :cond_7

    if-nez v3, :cond_3

    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    invoke-interface {v5, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;->a(Landroid/view/MotionEvent;)Z

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    add-int/lit8 v3, v3, 0x1

    :cond_3
    :goto_1
    if-ge v3, v0, :cond_9

    shl-int/lit8 v4, v3, 0x8

    or-int/lit8 v4, v4, 0x5

    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;->a(Landroid/view/MotionEvent;)Z

    invoke-virtual {v5}, Landroid/view/MotionEvent;->recycle()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    if-ne v4, v8, :cond_5

    move v3, v1

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    const/4 v5, 0x6

    if-eq v4, v5, :cond_6

    if-ne v4, v2, :cond_2

    :cond_6
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_7
    if-le v3, v0, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    :goto_2
    if-le v3, v5, :cond_8

    add-int/lit8 v3, v3, -0x1

    shl-int/lit8 v6, v3, 0x8

    or-int/lit8 v6, v6, 0x6

    invoke-static {v4}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    invoke-interface {v6, v7}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;->a(Landroid/view/MotionEvent;)Z

    invoke-virtual {v7}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_2

    :cond_8
    if-nez v0, :cond_9

    invoke-static {v4}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;->a(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 71
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eq v0, v2, :cond_a

    if-ne v0, v8, :cond_c

    :cond_a
    move v0, v2

    :goto_3
    if-eqz v0, :cond_d

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    .line 82
    :cond_b
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_c
    move v0, v1

    .line 71
    goto :goto_3

    .line 78
    :cond_d
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    goto :goto_4
.end method
