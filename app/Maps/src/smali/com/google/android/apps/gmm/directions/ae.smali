.class public Lcom/google/android/apps/gmm/directions/ae;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/google/android/apps/gmm/directions/ag;

.field private static final c:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/hm;",
            "[",
            "Lcom/google/android/apps/gmm/directions/ai;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/hm;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 32
    const-class v0, Lcom/google/android/apps/gmm/directions/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/ae;->a:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/directions/ag;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/ag;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/ae;->b:Lcom/google/android/apps/gmm/directions/ag;

    .line 43
    const-class v0, Lcom/google/maps/g/a/hm;

    .line 44
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    .line 48
    sput-object v0, Lcom/google/android/apps/gmm/directions/ae;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/apps/gmm/directions/ai;

    new-instance v3, Lcom/google/android/apps/gmm/directions/ah;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/ah;-><init>()V

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/apps/gmm/directions/ae;->b:Lcom/google/android/apps/gmm/directions/ag;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    new-array v2, v5, [Lcom/google/android/apps/gmm/directions/ai;

    new-instance v3, Lcom/google/android/apps/gmm/directions/aj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/aj;-><init>()V

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    new-array v2, v5, [Lcom/google/android/apps/gmm/directions/ai;

    sget-object v3, Lcom/google/android/apps/gmm/directions/ae;->b:Lcom/google/android/apps/gmm/directions/ag;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->c:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    new-array v2, v5, [Lcom/google/android/apps/gmm/directions/ai;

    sget-object v3, Lcom/google/android/apps/gmm/directions/ae;->b:Lcom/google/android/apps/gmm/directions/ag;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-class v0, Lcom/google/maps/g/a/hm;

    .line 61
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/ae;->d:Ljava/util/EnumMap;

    .line 63
    sget v0, Lcom/google/android/apps/gmm/l;->fy:I

    .line 64
    sget v1, Lcom/google/android/apps/gmm/l;->fy:I

    .line 65
    sget-object v2, Lcom/google/android/apps/gmm/directions/ae;->d:Ljava/util/EnumMap;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v2, Lcom/google/android/apps/gmm/directions/ae;->d:Ljava/util/EnumMap;

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v1, Lcom/google/android/apps/gmm/directions/ae;->d:Ljava/util/EnumMap;

    sget-object v2, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v1, Lcom/google/android/apps/gmm/directions/ae;->d:Ljava/util/EnumMap;

    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/afz;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 101
    iget v0, p2, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_0

    move v0, v6

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v7

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p2, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    .line 103
    iget v1, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v6, :cond_3

    move v1, v6

    :goto_1
    if-eqz v1, :cond_4

    .line 104
    iget v1, v0, Lcom/google/r/b/a/agt;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hc;->a(I)Lcom/google/maps/g/a/hc;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    .line 105
    :cond_2
    :goto_2
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v4, v2

    .line 109
    iget v2, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v8, :cond_5

    move v2, v6

    :goto_3
    if-eqz v2, :cond_d

    .line 110
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v1, v7

    .line 103
    goto :goto_1

    .line 104
    :cond_4
    sget-object v1, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    goto :goto_2

    :cond_5
    move v2, v7

    .line 109
    goto :goto_3

    .line 110
    :cond_6
    iget v2, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v8, :cond_7

    move v2, v6

    :goto_4
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_7
    move v2, v7

    goto :goto_4

    :cond_8
    iget v2, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v8, 0x2

    if-ne v2, v8, :cond_b

    move v2, v6

    :goto_5
    if-eqz v2, :cond_c

    iget v2, v0, Lcom/google/r/b/a/agt;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    :cond_9
    sget-object v8, Lcom/google/maps/g/a/fu;->b:Lcom/google/maps/g/a/fu;

    if-ne v2, v8, :cond_c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v8, v0, Lcom/google/r/b/a/agt;->d:J

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 112
    :goto_6
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/directions/f/d/c;->b(J)Ljava/util/Calendar;

    move-result-object v4

    .line 113
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/directions/f/d/c;->b(J)Ljava/util/Calendar;

    move-result-object v5

    .line 115
    const v0, 0x80001

    .line 116
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/c/b;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 117
    const v0, 0x80013

    .line 118
    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 119
    const v0, 0x80017

    .line 123
    :cond_a
    sget-object v4, Lcom/google/android/apps/gmm/directions/af;->a:[I

    invoke-virtual {v1}, Lcom/google/maps/g/a/hc;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 138
    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown TransitTimeAnchoring was found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string v0, ""

    :goto_7
    return-object v0

    :cond_b
    move v2, v7

    .line 110
    goto :goto_5

    :cond_c
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v8, v0, Lcom/google/r/b/a/agt;->d:J

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v2, v0

    add-long/2addr v2, v8

    goto :goto_6

    :cond_d
    move-wide v2, v4

    goto :goto_6

    .line 125
    :pswitch_0
    sget v1, Lcom/google/android/apps/gmm/l;->fk:I

    new-array v4, v6, [Ljava/lang/Object;

    .line 127
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/directions/f/d/c;->b(J)Ljava/util/Calendar;

    move-result-object v2

    .line 126
    invoke-static {p0, v2, v7, v0}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 125
    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 130
    :pswitch_1
    sget v1, Lcom/google/android/apps/gmm/l;->ff:I

    new-array v4, v6, [Ljava/lang/Object;

    .line 132
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/directions/f/d/c;->b(J)Ljava/util/Calendar;

    move-result-object v2

    .line 131
    invoke-static {p0, v2, v7, v0}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 130
    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 135
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->fr:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/maps/g/a/hm;Landroid/content/Context;Lcom/google/r/b/a/afz;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->c:Ljava/util/EnumMap;

    invoke-virtual {v0, p0}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->c:Ljava/util/EnumMap;

    invoke-virtual {v0, p0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/ai;

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 80
    invoke-interface {v4, p2, p1}, Lcom/google/android/apps/gmm/directions/ai;->a(Lcom/google/r/b/a/afz;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/apps/gmm/directions/ae;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/directions/ae;->d:Ljava/util/EnumMap;

    .line 84
    invoke-virtual {v0, p0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 148
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 149
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    .line 150
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :cond_3
    return-void

    .line 149
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
