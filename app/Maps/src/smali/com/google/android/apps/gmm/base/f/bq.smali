.class final Lcom/google/android/apps/gmm/base/f/bq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/h;


# instance fields
.field final synthetic a:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 409
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/f/bq;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/bq;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    int-to-float v0, v0

    .line 415
    sget-object v1, Lcom/google/android/apps/gmm/base/f/bp;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {p1, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    .line 419
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 420
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/h/a;->a:Landroid/view/animation/Interpolator;

    .line 421
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 422
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/f/br;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/f/br;-><init>(Lcom/google/android/apps/gmm/base/f/bq;Landroid/view/View;)V

    .line 423
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 434
    return-void

    .line 412
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x30

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method
