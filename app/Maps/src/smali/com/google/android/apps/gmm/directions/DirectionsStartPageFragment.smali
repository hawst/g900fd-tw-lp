.class public Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/gmm/base/activities/y;
.implements Lcom/google/android/apps/gmm/cardui/a/d;
.implements Lcom/google/android/apps/gmm/directions/al;
.implements Lcom/google/android/apps/gmm/directions/h/z;
.implements Lcom/google/android/apps/gmm/directions/i/ad;
.implements Lcom/google/android/apps/gmm/directions/option/h;
.implements Lcom/google/android/apps/gmm/l/av;
.implements Lcom/google/android/apps/gmm/suggest/a/a;


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field public final d:Lcom/google/android/apps/gmm/directions/av;

.field final e:Lcom/google/android/apps/gmm/directions/df;

.field f:Lcom/google/android/apps/gmm/directions/a/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/startpage/d/d;

.field private m:Lcom/google/android/apps/gmm/directions/au;

.field private final n:Lcom/google/android/apps/gmm/directions/dn;

.field private o:Lcom/google/android/apps/gmm/startpage/m;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final p:Lcom/google/android/apps/gmm/directions/i/k;

.field private final q:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    const-class v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/directions/am;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/am;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->q:Ljava/lang/Object;

    .line 216
    new-instance v0, Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/av;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 218
    new-instance v0, Lcom/google/android/apps/gmm/directions/df;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/df;-><init>(Lcom/google/android/apps/gmm/directions/al;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iput-object p0, v0, Lcom/google/android/apps/gmm/directions/df;->c:Lcom/google/android/apps/gmm/directions/h/z;

    .line 220
    new-instance v0, Lcom/google/android/apps/gmm/directions/dn;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/dn;-><init>(Lcom/google/android/apps/gmm/directions/al;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->n:Lcom/google/android/apps/gmm/directions/dn;

    .line 221
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/directions/i/k;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;Lcom/google/android/apps/gmm/directions/av;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->p:Lcom/google/android/apps/gmm/directions/i/k;

    .line 222
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;
    .locals 4
    .param p3    # Lcom/google/android/apps/gmm/directions/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 227
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    .line 228
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/directions/av;->s()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 229
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/directions/av;->r()Lcom/google/o/h/a/eq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/eq;)V

    .line 230
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/directions/av;->q()Lcom/google/o/h/a/dq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 231
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/directions/av;->t()Lcom/google/o/h/a/en;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/en;)V

    .line 232
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 233
    sget-object v1, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 234
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->d(Z)V

    .line 236
    new-instance v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;-><init>()V

    .line 237
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 238
    const-string v3, "directions_start_page_state"

    invoke-virtual {p0, v2, v3, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 239
    const-string v3, "directions_start_page_odelay_state"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 240
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 241
    iput-object p3, v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    .line 242
    return-object v1
.end method

.method private static a(Lcom/google/android/apps/gmm/directions/av;Lcom/google/maps/g/hy;Z)Lcom/google/android/apps/gmm/directions/f/a;
    .locals 3
    .param p1    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 732
    monitor-enter p0

    .line 733
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    .line 734
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->x()Lcom/google/maps/a/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    .line 735
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    .line 736
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->u()Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 737
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 738
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->c()Lcom/google/o/b/a/v;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    .line 739
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->i()Lcom/google/maps/g/a/al;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/b;->f:Lcom/google/maps/g/a/al;

    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/f/b;->g:Ljava/lang/String;

    .line 741
    iput-object p1, v0, Lcom/google/android/apps/gmm/directions/f/b;->h:Lcom/google/maps/g/hy;

    .line 742
    iput-boolean p2, v0, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    .line 743
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 744
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1232
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/au;->d:Landroid/view/View;

    if-nez p1, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1233
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/au;->e:Landroid/widget/TextView;

    if-nez p1, :cond_2

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1234
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/au;->f:Landroid/view/View;

    if-eqz p1, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1235
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/au;->g:Landroid/widget/TextView;

    if-eqz p1, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1236
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->h:Landroid/widget/TextView;

    if-eqz p1, :cond_6

    :goto_6
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1237
    return-void

    :cond_0
    move v0, v1

    .line 1232
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1233
    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1234
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1235
    goto :goto_5

    :cond_6
    move v1, v2

    .line 1236
    goto :goto_6
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 251
    if-nez p1, :cond_0

    move v0, v2

    .line 266
    :goto_0
    return v0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "directions_start_page_state"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 255
    instance-of v1, v0, Lcom/google/android/apps/gmm/directions/av;

    if-nez v1, :cond_1

    move v0, v2

    .line 256
    goto :goto_0

    .line 259
    :cond_1
    const-string v1, "directions_start_page_odelay_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 260
    instance-of v3, v1, Lcom/google/android/apps/gmm/startpage/d/d;

    if-nez v3, :cond_2

    move v0, v2

    .line 261
    goto :goto_0

    .line 264
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    check-cast v0, Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/av;)V

    .line 265
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 266
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/google/android/apps/gmm/map/r/a/ap;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 698
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 699
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 700
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    if-eqz v2, :cond_2

    move v2, v0

    :goto_2
    if-nez v2, :cond_3

    :goto_3
    return v0

    :cond_0
    move v2, v1

    .line 698
    goto :goto_0

    :cond_1
    move v2, v1

    .line 699
    goto :goto_1

    :cond_2
    move v2, v1

    .line 700
    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3
.end method

.method private c(Lcom/google/android/apps/gmm/map/r/a/ap;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 992
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->p()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 993
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->o()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v2

    .line 996
    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    if-ne v2, v1, :cond_1

    sget-object v1, Lcom/google/b/f/t;->aJ:Lcom/google/b/f/t;

    :goto_0
    if-nez v1, :cond_3

    .line 998
    :goto_1
    invoke-virtual {p0, p1, v2, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/hy;)V

    .line 999
    return-void

    .line 996
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    if-ne v2, v1, :cond_2

    sget-object v1, Lcom/google/b/f/t;->aH:Lcom/google/b/f/t;

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    const/4 v3, 0x0

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v5, v4, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, v4, Lcom/google/maps/g/ia;->a:I

    iput-boolean v3, v4, Lcom/google/maps/g/ia;->f:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    goto :goto_1
.end method


# virtual methods
.method public final H_()V
    .locals 1

    .prologue
    .line 576
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 581
    :goto_0
    return-void

    .line 579
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    .line 580
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o()V

    goto :goto_0
.end method

.method public final I_()Lcom/google/android/apps/gmm/directions/dj;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->f()Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v0

    return-object v0
.end method

.method public final J_()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 1390
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/l/aa;->a:Lcom/google/android/apps/gmm/l/aa;

    .line 1389
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/l/e;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/aa;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/a;)Lcom/google/android/apps/gmm/cardui/a/e;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 867
    new-instance v0, Lcom/google/android/apps/gmm/directions/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/ar;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    new-instance v1, Lcom/google/android/apps/gmm/directions/as;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/as;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    new-instance v2, Lcom/google/android/apps/gmm/directions/at;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/at;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    invoke-static {v0, v1, v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 872
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/a/e;

    .line 873
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/cardui/a/e;->a(Lcom/google/o/h/a/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 877
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->a()V

    .line 483
    :cond_0
    return-void
.end method

.method public final a(IIF)V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/df;->a()V

    .line 504
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 389
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    :goto_0
    return-void

    .line 392
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->b(IZ)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1025
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    sget v1, Lcom/google/android/apps/gmm/l;->fz:I

    .line 1026
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 1027
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 1028
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 1029
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 1025
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 1030
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1031
    return-void
.end method

.method a(Lcom/google/android/apps/gmm/map/r/a/ap;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1068
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1078
    :cond_0
    :goto_0
    return-void

    .line 1071
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    .line 1074
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 1076
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 1077
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    .line 1075
    new-instance v0, Lcom/google/android/apps/gmm/startpage/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/b/b;->d:Lcom/google/android/apps/gmm/startpage/b/b;

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/b/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/apps/gmm/startpage/b/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 1074
    invoke-interface {v6, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 1071
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/hy;)V
    .locals 3
    .param p3    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 828
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/suggest/e/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 829
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 833
    :cond_0
    :goto_0
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    .line 834
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c()Z

    .line 837
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/ap;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/ap;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 838
    return-void

    .line 830
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/suggest/e/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 3

    .prologue
    .line 1015
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 1016
    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 1017
    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 1018
    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->m:[B

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->h:[B

    .line 1019
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 1015
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 1020
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1021
    return-void
.end method

.method public final a(Lcom/google/r/b/a/afz;Lcom/google/b/f/t;)V
    .locals 4

    .prologue
    .line 1049
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1050
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1051
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/r/b/a/afz;)V

    .line 1052
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    const/4 v1, 0x0

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v3, v2, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v2, Lcom/google/maps/g/ia;->a:I

    iput-boolean v1, v2, Lcom/google/maps/g/ia;->f:Z

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 1053
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/ap;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/ap;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 1055
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1004
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 1005
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 1006
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 1007
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 1008
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 1004
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 1009
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1010
    return-void
.end method

.method public final a(Lcom/google/maps/g/hy;)Z
    .locals 7
    .param p1    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 758
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v3

    .line 759
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 761
    :cond_0
    monitor-exit v3

    move v0, v1

    .line 819
    :goto_0
    return v0

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 765
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/ao;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/directions/ao;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v4, v5, :cond_3

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(Lcom/google/android/apps/gmm/mylocation/b/c;)V

    .line 766
    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    .line 767
    monitor-exit v3

    move v0, v1

    goto :goto_0

    .line 765
    :cond_3
    const/4 v4, 0x0

    invoke-interface {v0, v4, v2}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(ZLcom/google/android/apps/gmm/mylocation/b/c;)V

    goto :goto_1

    .line 804
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 770
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    :cond_5
    move v0, v2

    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    :cond_6
    move v0, v2

    :goto_4
    if-nez v0, :cond_a

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->fo:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 772
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 773
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3a

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot get directions because the start point is invalid: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    monitor-exit v3

    move v0, v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 770
    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_4

    .line 777
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_d

    :cond_b
    move v0, v2

    :goto_5
    if-eqz v0, :cond_c

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_e

    move v0, v2

    :goto_6
    if-eqz v0, :cond_f

    :cond_c
    move v0, v2

    :goto_7
    if-nez v0, :cond_10

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->fn:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 779
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 780
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x38

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot get directions because the end point is invalid: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    monitor-exit v3

    move v0, v1

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 777
    goto :goto_5

    :cond_e
    move v0, v1

    goto :goto_6

    :cond_f
    move v0, v1

    goto :goto_7

    .line 784
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v5

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v6, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v6, :cond_13

    move v0, v2

    :goto_8
    if-eqz v0, :cond_11

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v6, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v6, :cond_14

    move v0, v2

    :goto_9
    if-nez v0, :cond_12

    :cond_11
    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_12
    move v0, v2

    :goto_a
    if-eqz v0, :cond_16

    .line 785
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    .line 786
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v2, Lcom/google/android/apps/gmm/directions/aw;->a:Lcom/google/android/apps/gmm/directions/aw;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/aw;)V

    .line 787
    monitor-exit v3

    move v0, v1

    goto/16 :goto_0

    :cond_13
    move v0, v1

    .line 784
    goto :goto_8

    :cond_14
    move v0, v1

    goto :goto_9

    :cond_15
    move v0, v1

    goto :goto_a

    .line 790
    :cond_16
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    .line 790
    invoke-static {v4, p1, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/directions/av;Lcom/google/maps/g/hy;Z)Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v4

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->m()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v5

    if-eqz v5, :cond_1b

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    iget-object v6, v5, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v6, :cond_1b

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    invoke-virtual {v0}, Lcom/google/r/b/a/afz;->k()Lcom/google/n/f;

    move-result-object v0

    iget-object v6, v5, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    invoke-virtual {v6}, Lcom/google/r/b/a/afz;->k()Lcom/google/n/f;

    move-result-object v6

    if-eq v0, v6, :cond_17

    if-eqz v0, :cond_19

    invoke-virtual {v0, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    :cond_17
    move v0, v2

    :goto_b
    if-eqz v0, :cond_1b

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    iget-object v6, v5, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    if-eq v0, v6, :cond_18

    if-eqz v0, :cond_1a

    invoke-virtual {v0, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_18
    move v0, v2

    :goto_c
    if-eqz v0, :cond_1b

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    iget-object v5, v5, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/directions/f/a;->a(Lcom/google/b/c/cv;Lcom/google/b/c/cv;)Z

    move-result v0

    if-eqz v0, :cond_1b

    move v0, v2

    :goto_d
    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 793
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->f()Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v5, Lcom/google/android/apps/gmm/directions/dl;->d:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v5, :cond_1c

    move v0, v2

    :goto_e
    if-nez v0, :cond_1d

    .line 794
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    .line 796
    monitor-exit v3

    move v0, v1

    goto/16 :goto_0

    :cond_19
    move v0, v1

    .line 792
    goto :goto_b

    :cond_1a
    move v0, v1

    goto :goto_c

    :cond_1b
    move v0, v1

    goto :goto_d

    :cond_1c
    move v0, v1

    .line 793
    goto :goto_e

    .line 798
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/f/a;)V

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v1, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/aw;)V

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dj;->b:Lcom/google/android/apps/gmm/directions/dj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/dj;)V

    .line 804
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 805
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/directions/dg;

    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/directions/dg;-><init>(Lcom/google/android/apps/gmm/directions/df;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 809
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v0, :cond_1e

    .line 810
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v1, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 814
    :cond_1e
    new-instance v1, Lcom/google/android/apps/gmm/directions/aa;

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/gmm/directions/aa;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcom/google/android/apps/gmm/directions/aa;->a(Lcom/google/android/apps/gmm/directions/f/a;Ljava/lang/String;)V

    .line 817
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    move v0, v2

    .line 819
    goto/16 :goto_0
.end method

.method public final b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 397
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->l()Z

    move-result v1

    if-nez v1, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v0

    .line 402
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v1

    .line 403
    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->b:Lcom/google/android/apps/gmm/directions/a/g;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->c:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 406
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v2

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v2, v3, :cond_0

    .line 407
    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x35

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Auto-loading trip details, because ResultViewMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v1, v2, :cond_3

    const/4 v0, 0x1

    .line 409
    :cond_3
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->b(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method b(IZ)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->f()Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dl;->c:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v1, :cond_0

    move v0, v8

    :goto_0
    if-nez v0, :cond_1

    move v0, v7

    .line 694
    :goto_1
    return v0

    :cond_0
    move v0, v7

    .line 642
    goto :goto_0

    .line 646
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->f()Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/f;

    .line 647
    if-nez v1, :cond_2

    .line 648
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    const-string v1, "storageItem is null"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v7

    .line 649
    goto :goto_1

    .line 651
    :cond_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 652
    if-nez v2, :cond_3

    .line 653
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    const-string v1, "Cannot retrieve directions from storage"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v7

    .line 654
    goto :goto_1

    .line 656
    :cond_3
    if-ltz p1, :cond_4

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, p1, :cond_5

    :cond_4
    const/4 v0, 0x0

    move-object v3, v0

    .line 657
    :goto_2
    if-nez v3, :cond_7

    .line 658
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    const-string v1, "Cannot retrieve trip from storage"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v7

    .line 659
    goto :goto_1

    .line 656
    :cond_5
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    if-nez v0, :cond_6

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v4, v0, v2}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v4, v3, p1

    :cond_6
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p1

    move-object v3, v0

    goto :goto_2

    .line 664
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 666
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v5

    move v2, v7

    move v0, v7

    :goto_3
    if-ge v2, v5, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v6

    invoke-interface {v6}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    add-int/lit8 v0, v0, 0x1

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_9
    if-le v0, v8, :cond_b

    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    sget-object v4, Lcom/google/android/apps/gmm/base/layout/w;->a:Lcom/google/android/apps/gmm/base/layout/w;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setMapRenderingMode(Lcom/google/android/apps/gmm/base/layout/w;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    if-eqz v0, :cond_a

    iput-boolean v7, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->e:Z

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v4, v0}, Landroid/app/FragmentManager;->saveFragmentInstanceState(Landroid/app/Fragment;)Landroid/app/Fragment$SavedState;

    move-result-object v5

    invoke-virtual {v4}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v8}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->setInitialSavedState(Landroid/app/Fragment$SavedState;)V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    sget-object v2, Lcom/google/android/apps/gmm/base/layout/w;->c:Lcom/google/android/apps/gmm/base/layout/w;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setMapRenderingMode(Lcom/google/android/apps/gmm/base/layout/w;)V

    .line 668
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->saveFragmentInstanceState(Landroid/app/Fragment;)Landroid/app/Fragment$SavedState;

    move-result-object v4

    .line 671
    sget-object v2, Lcom/google/android/apps/gmm/directions/aq;->b:[I

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v5, v0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    if-nez v5, :cond_d

    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v0

    :goto_4
    iget v0, v0, Lcom/google/maps/g/a/id;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/ig;->a(I)Lcom/google/maps/g/a/ig;

    move-result-object v0

    if-nez v0, :cond_c

    sget-object v0, Lcom/google/maps/g/a/ig;->a:Lcom/google/maps/g/a/ig;

    :cond_c
    invoke-virtual {v0}, Lcom/google/maps/g/a/ig;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 687
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->l()Z

    move-result v6

    move v2, p1

    move v5, p2

    .line 685
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/map/r/a/f;ILjava/lang/String;Landroid/app/Fragment$SavedState;ZZ)Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    move-result-object v0

    .line 688
    if-eqz v0, :cond_10

    .line 689
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v8

    .line 690
    goto/16 :goto_1

    .line 671
    :cond_d
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    goto :goto_4

    .line 681
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    if-nez v2, :cond_e

    invoke-static {}, Lcom/google/maps/g/a/id;->d()Lcom/google/maps/g/a/id;

    move-result-object v0

    :goto_5
    iget-object v2, v0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    if-nez v2, :cond_f

    invoke-static {}, Lcom/google/maps/g/mg;->d()Lcom/google/maps/g/mg;

    move-result-object v0

    .line 680
    :goto_6
    iget-object v0, v0, Lcom/google/maps/g/mg;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/aw;->d()Lcom/google/maps/g/aw;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/aw;

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/a/s;->a(Lcom/google/maps/g/aw;)Landroid/content/Intent;

    move-result-object v0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_7
    move v0, v8

    .line 682
    goto/16 :goto_1

    .line 681
    :cond_e
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->q:Lcom/google/maps/g/a/id;

    goto :goto_5

    :cond_f
    iget-object v0, v0, Lcom/google/maps/g/a/id;->c:Lcom/google/maps/g/mg;

    goto :goto_6

    .line 680
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    sget v2, Lcom/google/android/apps/gmm/l;->gx:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_10
    move v0, v7

    .line 694
    goto/16 :goto_1

    .line 671
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final c()Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 606
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v1

    .line 607
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->g()Lcom/google/android/apps/gmm/directions/aw;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/directions/aw;->a:Lcom/google/android/apps/gmm/directions/aw;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/directions/aw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    if-eqz v2, :cond_0

    .line 609
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 610
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/av;->q()Lcom/google/o/h/a/dq;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/av;->r()Lcom/google/o/h/a/eq;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 611
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/av;->t()Lcom/google/o/h/a/en;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/directions/av;->s()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v6

    .line 609
    iget-object v7, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v8, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/startpage/d/d;->r()V

    iget-object v8, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v8, v3}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/eq;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/en;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->c(Lcom/google/android/apps/gmm/startpage/d/e;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/m;->e()V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/startpage/m;->a(Z)V

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 615
    :goto_0
    return v0

    .line 609
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_0
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 615
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v1, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 515
    :cond_0
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 508
    sget-object v0, Lcom/google/b/f/t;->aF:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;
    .locals 0

    .prologue
    .line 524
    return-object p0
.end method

.method i()V
    .locals 3

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/ap;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/ap;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 1179
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 519
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->l()Z

    move-result v0

    return v0
.end method

.method public o()V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1185
    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 1186
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1197
    :goto_0
    return-void

    .line 1189
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v4

    .line 1190
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/au;->b:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->p:Lcom/google/android/apps/gmm/directions/i/k;

    invoke-static {v3, v5}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 1193
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->n:Lcom/google/android/apps/gmm/directions/dn;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/dn;->a()V

    .line 1194
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v5

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/av;->g()Lcom/google/android/apps/gmm/directions/aw;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    .line 1195
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v6

    .line 1194
    iget-object v7, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v7, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/directions/av;->w()Lcom/google/android/apps/gmm/directions/option/g;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/gmm/directions/option/g;->c:Lcom/google/android/apps/gmm/directions/option/g;

    if-eq v8, v9, :cond_4

    sget-object v8, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    if-ne v3, v8, :cond_4

    move v3, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v8, v0, Lcom/google/android/apps/gmm/directions/au;->c:Landroid/view/View;

    if-eqz v3, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_1

    invoke-static {v5, v7, v6}, Lcom/google/android/apps/gmm/directions/ae;->a(Lcom/google/maps/g/a/hm;Landroid/content/Context;Lcom/google/r/b/a/afz;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v5, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Z)V

    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->b:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 1196
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->g()Lcom/google/android/apps/gmm/directions/aw;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/au;->i:Landroid/view/View;

    move v0, v2

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/au;->j:Landroid/view/View;

    move v0, v2

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/aw;->a:Lcom/google/android/apps/gmm/directions/aw;

    if-ne v3, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1197
    :cond_3
    :goto_4
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    move v3, v1

    .line 1194
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/au;->g:Landroid/widget/TextView;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-static {v7, v0, v6}, Lcom/google/android/apps/gmm/directions/ae;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/afz;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Z)V

    goto :goto_3

    .line 1196
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    if-ne v3, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/au;->j:Landroid/view/View;

    move v0, v1

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 271
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onAttach(Landroid/app/Activity;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 273
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 371
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    const/4 v0, 0x0

    .line 376
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/au;->d:Landroid/view/View;

    if-eq p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/au;->h:Landroid/widget/TextView;

    if-ne p1, v1, :cond_3

    .line 377
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    .line 378
    new-instance v0, Lcom/google/android/apps/gmm/directions/av;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/directions/av;-><init>(Lcom/google/android/apps/gmm/directions/av;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v1

    sget-object v2, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->w()Lcom/google/android/apps/gmm/directions/option/g;

    move-result-object v0

    invoke-static {v1, v2, v0, p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->a(Landroid/content/Context;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/option/g;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 379
    :goto_1
    const/4 v0, 0x1

    .line 382
    :cond_3
    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v1, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    goto :goto_0

    .line 378
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->w()Lcom/google/android/apps/gmm/directions/option/g;

    invoke-static {v1, v2, v3, p0}, Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;->a(Landroid/content/Context;Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/directions/QuDirectionsOptionsDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 494
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o()V

    .line 498
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/dg;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/directions/dg;-><init>(Lcom/google/android/apps/gmm/directions/df;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 499
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 280
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "state loaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->n:Lcom/google/android/apps/gmm/directions/dn;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    .line 285
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/dg;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/directions/dg;-><init>(Lcom/google/android/apps/gmm/directions/df;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 286
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 312
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 314
    new-instance v3, Lcom/google/android/apps/gmm/startpage/f/i;

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/startpage/f/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 316
    new-instance v0, Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v5, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/m;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    .line 320
    sget v0, Lcom/google/android/apps/gmm/h;->r:I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 321
    sget v0, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 324
    sget v0, Lcom/google/android/apps/gmm/g;->aj:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 325
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v3, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/directions/c/l;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->p:Lcom/google/android/apps/gmm/directions/i/k;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 330
    new-instance v0, Lcom/google/android/apps/gmm/directions/au;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/directions/au;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->n:Lcom/google/android/apps/gmm/directions/dn;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/au;->b:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/google/android/apps/gmm/directions/dq;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/directions/dq;-><init>(Landroid/view/View;)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/au;->i:Landroid/view/View;

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/suggest/c/d;

    .line 342
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 343
    new-instance v0, Lcom/google/android/apps/gmm/directions/an;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/directions/an;-><init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;Landroid/view/View;)V

    .line 350
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/libraries/curvular/ag;)V

    .line 353
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/au;->j:Landroid/view/View;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    sget v0, Lcom/google/android/apps/gmm/g;->ar:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    sget v0, Lcom/google/android/apps/gmm/g;->ai:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v3, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/directions/c/i;

    iget-object v4, v1, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    sget v3, Lcom/google/android/apps/gmm/g;->cM:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->ak:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->g:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/directions/c/o;

    iget-object v3, v1, Lcom/google/android/apps/gmm/directions/df;->g:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->g:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/directions/di;->a:Lcom/google/android/apps/gmm/directions/di;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/directions/c/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 356
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/dg;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/directions/dg;-><init>(Lcom/google/android/apps/gmm/directions/df;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/au;->a:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 365
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroyView()V

    .line 366
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->m:Lcom/google/android/apps/gmm/directions/au;

    .line 367
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 452
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 455
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/f/a;)V

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->q:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 461
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 462
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 416
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->n()V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->q:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->c()Lcom/google/o/b/a/v;

    move-result-object v0

    if-nez v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v2, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/aw;)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v2, Lcom/google/android/apps/gmm/directions/dj;->b:Lcom/google/android/apps/gmm/directions/dj;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/dj;)V

    .line 434
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v2, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/directions/dg;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/directions/dg;-><init>(Lcom/google/android/apps/gmm/directions/df;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->f()Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v2, Lcom/google/android/apps/gmm/directions/dl;->c:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v2, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->v()Lcom/google/maps/g/hy;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/hy;)V

    .line 443
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o()V

    .line 445
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->u:Lcom/google/android/apps/gmm/base/activities/z;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->b()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    const-class v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 447
    sget-object v0, Lcom/google/android/apps/gmm/directions/aq;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 448
    :cond_2
    return-void

    .line 438
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 447
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->E:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->D:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->G:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/l;->F:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 487
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "directions_start_page_state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 489
    const-string v0, "directions_start_page_odelay_state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->g:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 490
    return-void
.end method
