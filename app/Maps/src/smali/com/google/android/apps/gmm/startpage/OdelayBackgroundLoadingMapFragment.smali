.class public Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;
.super Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/startpage/m;

.field private x:Lcom/google/android/apps/gmm/startpage/d/d;

.field private y:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/startpage/d/d;)Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/cardui/b/b;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/cardui/f/d;",
            ">;",
            "Lcom/google/android/apps/gmm/startpage/d/d;",
            ")",
            "Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;-><init>()V

    .line 46
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    const-string v2, "placeItemListProviderRef"

    invoke-virtual {p0, v1, v2, p2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 48
    const-string v2, "pageType"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 49
    const-string v2, "odelayState"

    invoke-virtual {p0, v1, v2, p3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 50
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->setArguments(Landroid/os/Bundle;)V

    .line 51
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 52
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/b/b;Ljava/lang/String;Ljava/util/List;Lcom/google/o/h/a/nt;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/cardui/b/b;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;",
            "Lcom/google/o/h/a/nt;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 105
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->y:Z

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f/d;

    iget-object v3, v0, Lcom/google/android/apps/gmm/cardui/f/d;->a:Lcom/google/android/apps/gmm/cardui/f/c;

    .line 110
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/cardui/f/c;->a()I

    move-result v0

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 131
    :goto_0
    return v0

    .line 113
    :cond_0
    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/cardui/f/c;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v4

    .line 115
    iget-object v0, p4, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v5

    .line 116
    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 117
    goto :goto_0

    .line 121
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->w:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f/d;

    iput-object p4, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    .line 123
    invoke-virtual {v3, p2}, Lcom/google/android/apps/gmm/cardui/f/c;->b(Ljava/lang/String;)V

    .line 124
    invoke-static {p3}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    :cond_4
    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/cardui/f/c;->a(Ljava/util/List;)V

    .line 128
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->a(Z)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setCurrentItem(I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->r()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)V

    move v0, v2

    .line 131
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    if-eqz p1, :cond_0

    .line 60
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "odelayState"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->x:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 61
    const-string v0, "isFetched"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->y:Z

    .line 62
    return-void

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->onPause()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->A:Lcom/google/android/apps/gmm/startpage/m;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->A:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->A:Lcom/google/android/apps/gmm/startpage/m;

    .line 83
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->onResume()V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->x:Lcom/google/android/apps/gmm/startpage/d/d;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->y:Z

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->x:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/startpage/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->A:Lcom/google/android/apps/gmm/startpage/m;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->A:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 73
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->x:Lcom/google/android/apps/gmm/startpage/d/d;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "odelayState"

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->x:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 92
    :cond_0
    const-string v0, "isFetched"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/startpage/OdelayBackgroundLoadingMapFragment;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 93
    return-void
.end method
