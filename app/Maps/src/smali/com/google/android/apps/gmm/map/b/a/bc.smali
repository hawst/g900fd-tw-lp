.class public final Lcom/google/android/apps/gmm/map/b/a/bc;
.super Lcom/google/android/apps/gmm/map/b/a/bd;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/b/a/ae;

.field public b:Lcom/google/android/apps/gmm/map/b/a/y;

.field public c:Lcom/google/android/apps/gmm/map/b/a/y;

.field private d:I

.field private e:I

.field private g:I

.field private volatile h:Lcom/google/android/apps/gmm/map/b/a/y;

.field private volatile i:Lcom/google/android/apps/gmm/map/b/a/y;

.field private volatile j:Lcom/google/android/apps/gmm/map/b/a/y;

.field private volatile k:Lcom/google/android/apps/gmm/map/b/a/y;

.field private volatile l:Lcom/google/android/apps/gmm/map/b/a/y;

.field private volatile m:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/bd;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 40
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 41
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/bc;Lcom/google/android/apps/gmm/map/b/a/bc;)[I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 223
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 224
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 225
    aput v2, v0, v2

    .line 226
    aput v2, v0, v1

    .line 228
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v3, v4, :cond_4

    .line 229
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    aput v3, v0, v2

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    aput v2, v0, v1

    .line 235
    :cond_3
    :goto_1
    return-object v0

    .line 231
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v3, v4, :cond_3

    .line 232
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    aput v3, v0, v2

    .line 233
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    aput v2, v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(II)Lcom/google/android/apps/gmm/map/b/a/bc;
    .locals 5

    .prologue
    .line 375
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "new size cannot be negative or zero"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 377
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    .line 379
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, p2

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v4, p1

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v0, p2

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    .line 245
    packed-switch p1, :pswitch_data_0

    .line 258
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 247
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 257
    :goto_0
    return-object v0

    .line 251
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 253
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_1

    .line 254
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 257
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(I[Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 6

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x2

    const v2, -0x20000001

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 274
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-eqz v0, :cond_4

    .line 275
    packed-switch p1, :pswitch_data_0

    .line 317
    :goto_0
    return-void

    .line 277
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v3

    .line 278
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    .line 281
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v3

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 283
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v0, p2, v4

    goto :goto_0

    .line 288
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_1

    .line 289
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v0, p2, v3

    .line 292
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    .line 295
    :pswitch_3
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v3

    .line 296
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v4

    goto :goto_0

    .line 299
    :pswitch_4
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v3

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_2

    .line 301
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v0, p2, v4

    goto :goto_0

    .line 306
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->m:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_3

    .line 307
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->m:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 309
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->m:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v0, p2, v3

    .line 310
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    .line 314
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v3

    .line 315
    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    aput-object v0, p2, v4

    goto/16 :goto_0

    .line 275
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ae;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 45
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 46
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 47
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gez v0, :cond_1

    .line 48
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    neg-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->d:I

    .line 52
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    .line 55
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->e:I

    .line 56
    iget v0, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->g:I

    .line 57
    return-void

    .line 49
    :cond_1
    iget v0, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v0, v3, :cond_0

    .line 50
    iget v0, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->d:I

    goto :goto_0

    .line 54
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 148
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-nez v1, :cond_1

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    .line 160
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;

    if-eqz v1, :cond_4

    .line 152
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v1, v2, :cond_0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v1, v2, :cond_2

    const/high16 v1, 0x20000000

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v1, v2, :cond_3

    :cond_2
    const/high16 v1, -0x20000000

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 160
    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/bd;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 2

    .prologue
    .line 104
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->d:I

    add-int/2addr v0, v1

    const v1, 0x3fffffff    # 1.9999999f

    and-int/2addr v0, v1

    .line 105
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->e:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->g:I

    if-gt v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/bc;
    .locals 0

    .prologue
    .line 111
    return-object p0
.end method

.method public final bridge synthetic c()Lcom/google/android/apps/gmm/map/b/a/af;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 337
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/bc;

    if-nez v2, :cond_1

    .line 348
    :cond_0
    :goto_0
    return v0

    .line 341
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 342
    goto :goto_0

    .line 345
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 347
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 348
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 354
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
