.class public Lcom/google/android/apps/gmm/cardui/h/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/h;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/o/h/a/op;

.field final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/gmm/util/b/ag;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/op;)V
    .locals 6

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/t;->a:Landroid/content/Context;

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->aF:I

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 29
    iget-object v0, p3, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    aput-object v0, v4, v5

    .line 28
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->c:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/t;->d:Lcom/google/android/apps/gmm/util/b/ag;

    .line 31
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    .line 32
    return-void

    .line 29
    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p3, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 132
    const-string v0, "%d %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/t;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/j;->G:I

    invoke-virtual {v3, v4, p1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 140
    const-string v0, "%d %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/t;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/j;->F:I

    invoke-virtual {v3, v4, p1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/l/a/o;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/cardui/h/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/cardui/h/u;-><init>(Lcom/google/android/apps/gmm/cardui/h/t;)V

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 94
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    iget v0, v0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    iget-object v0, v0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/l;->d()Lcom/google/o/h/a/l;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/l;

    iget v2, v0, Lcom/google/o/h/a/l;->b:I

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    iget-object v0, v0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/l;->d()Lcom/google/o/h/a/l;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/l;

    iget v0, v0, Lcom/google/o/h/a/l;->c:I

    .line 102
    :goto_1
    if-lez v2, :cond_1

    if-lez v0, :cond_1

    .line 103
    const-string v4, "%s \u00b7 %s"

    new-array v5, v5, [Ljava/lang/Object;

    .line 104
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/cardui/h/t;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/cardui/h/t;->b(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v3

    .line 103
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 108
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 97
    goto :goto_0

    .line 105
    :cond_1
    if-lez v0, :cond_2

    .line 106
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/cardui/h/t;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 108
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/cardui/h/t;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    iget-object v0, v0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/l;->d()Lcom/google/o/h/a/l;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/l;

    iget v0, v0, Lcom/google/o/h/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    .line 115
    iget-object v0, v0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/l;->d()Lcom/google/o/h/a/l;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/l;

    iget v0, v0, Lcom/google/o/h/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 114
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 115
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    iget v0, v0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->d:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/t;->b:Lcom/google/o/h/a/op;

    .line 122
    iget-object v0, v0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/t;->d:Lcom/google/android/apps/gmm/util/b/ag;

    .line 123
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/t;->d:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 121
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 125
    :cond_0
    return-object v4

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
