.class public Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;
.super Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/b/a/q;

.field private b:Landroid/app/Fragment;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/reportmapissue/m;-><init>(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;
    .locals 3

    .prologue
    .line 63
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;-><init>()V

    .line 67
    if-eqz p2, :cond_0

    .line 68
    const-string v2, "placemark"

    invoke-virtual {p1, v0, v2, p2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 70
    :cond_0
    iput-object p0, v1, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->b:Landroid/app/Fragment;

    .line 71
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 72
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 118
    sget v0, Lcom/google/android/apps/gmm/l;->la:I

    return v0
.end method

.method protected final a(Z)I
    .locals 1

    .prologue
    .line 143
    if-eqz p1, :cond_0

    const/16 v0, 0xa

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public final a_()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->b:Landroid/app/Fragment;

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 98
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 123
    sget v0, Lcom/google/android/apps/gmm/l;->gG:I

    return v0
.end method

.method protected final b_()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    .line 154
    :cond_0
    return-void
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 128
    sget v0, Lcom/google/android/apps/gmm/d;->aM:I

    return v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 133
    sget v0, Lcom/google/android/apps/gmm/l;->fW:I

    return v0
.end method

.method protected final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 103
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentKey"

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->b:Landroid/app/Fragment;

    .line 83
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->g:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    const/4 v0, 0x1

    .line 112
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentKey"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->b:Landroid/app/Fragment;

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 89
    return-void
.end method
