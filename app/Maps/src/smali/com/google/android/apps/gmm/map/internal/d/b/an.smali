.class Lcom/google/android/apps/gmm/map/internal/d/b/an;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

.field final b:J

.field final c:Ljava/lang/String;

.field final d:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

.field final f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

.field g:I

.field h:Lcom/google/android/apps/gmm/map/internal/d/a/c;


# direct methods
.method constructor <init>(JLjava/lang/String;ILcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/ao;->b:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 193
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->b:J

    .line 194
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->c:Ljava/lang/String;

    .line 195
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->g:I

    .line 196
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 197
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->h:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 198
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 199
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 200
    return-void
.end method

.method constructor <init>(JLjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b/ao;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/b/z;Lcom/google/android/apps/gmm/map/internal/d/b/n;)V
    .locals 5

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->a:Lcom/google/android/apps/gmm/map/internal/d/b/ao;

    .line 206
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->b:J

    .line 207
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->c:Ljava/lang/String;

    .line 208
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 209
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    .line 210
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->f:Lcom/google/android/apps/gmm/map/internal/d/b/n;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->b:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/z;->a(JLjava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/an;->e:Lcom/google/android/apps/gmm/map/internal/d/b/z;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/z;->d:Lcom/google/android/apps/gmm/map/util/f;

    .line 216
    iget-object v1, p7, Lcom/google/android/apps/gmm/map/internal/d/b/n;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 217
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V

    .line 218
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v0

    invoke-interface {v1, p7, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V

    .line 219
    return-void
.end method
