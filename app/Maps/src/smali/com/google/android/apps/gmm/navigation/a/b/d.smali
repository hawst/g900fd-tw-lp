.class public Lcom/google/android/apps/gmm/navigation/a/b/d;
.super Lcom/google/android/apps/gmm/navigation/a/b/a;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Lcom/google/maps/g/a/al;


# direct methods
.method public constructor <init>(ILcom/google/maps/g/a/al;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/b/a;-><init>()V

    .line 96
    iput p1, p0, Lcom/google/android/apps/gmm/navigation/a/b/d;->a:I

    .line 97
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/b/d;->b:Lcom/google/maps/g/a/al;

    .line 98
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 115
    instance-of v1, p1, Lcom/google/android/apps/gmm/navigation/a/b/d;

    if-eqz v1, :cond_0

    .line 116
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/b/d;

    iget v1, p1, Lcom/google/android/apps/gmm/navigation/a/b/d;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/a/b/d;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 118
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/d;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/d;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/b/d;->b:Lcom/google/maps/g/a/al;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x32

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "<distance_message distance=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\" units=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
