.class public Lcom/google/android/apps/gmm/map/internal/d/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/concurrent/CountDownLatch;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->a:Ljava/util/ArrayList;

    .line 25
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, p1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->b:Ljava/util/concurrent/CountDownLatch;

    .line 26
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->c:I

    .line 27
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->d:I

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 37
    :cond_0
    if-nez p2, :cond_2

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 39
    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 40
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->c:I

    goto :goto_1

    .line 41
    :cond_3
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 42
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/a/a;->d:I

    goto :goto_1
.end method
