.class public Lcom/google/android/apps/gmm/util/i;
.super Landroid/text/style/ReplacementSpan;
.source "PG"


# static fields
.field private static final a:Landroid/graphics/Rect;

.field private static final b:Landroid/graphics/RectF;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    .line 18
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/util/i;->b:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/i;->c:Ljava/lang/String;

    .line 36
    iput p2, p0, Lcom/google/android/apps/gmm/util/i;->d:I

    .line 37
    iput p3, p0, Lcom/google/android/apps/gmm/util/i;->e:I

    .line 38
    iput p4, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    .line 39
    iput p5, p0, Lcom/google/android/apps/gmm/util/i;->g:I

    .line 40
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 45
    invoke-virtual {p9}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    .line 46
    invoke-virtual {p9}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v1

    .line 48
    iget v2, p0, Lcom/google/android/apps/gmm/util/i;->d:I

    invoke-virtual {p9, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p9, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 50
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/i;->c:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/util/i;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    invoke-virtual {p9, v2, v3, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 51
    sget-object v2, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    shl-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 52
    sub-int v3, p8, p6

    sub-int v2, v3, v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, p8, v2

    .line 53
    sget-object v3, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    iget v4, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    neg-int v4, v4

    iget v5, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    neg-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->inset(II)V

    .line 54
    sget-object v3, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    float-to-int v4, p5

    iget v5, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    sub-int v5, v2, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 55
    sget-object v3, Lcom/google/android/apps/gmm/util/i;->b:Landroid/graphics/RectF;

    sget-object v4, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 57
    sget-object v3, Lcom/google/android/apps/gmm/util/i;->b:Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/apps/gmm/util/i;->g:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/gmm/util/i;->g:I

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, p9}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 59
    iget v3, p0, Lcom/google/android/apps/gmm/util/i;->e:I

    invoke-virtual {p9, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    invoke-virtual {p9, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 61
    invoke-virtual {p9}, Landroid/graphics/Paint;->clearShadowLayer()V

    .line 62
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/i;->c:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    int-to-float v3, v3

    add-float/2addr v3, p5

    iget v4, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1, v1, v3, v2, p9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 63
    invoke-virtual {p9, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/i;->c:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/i;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 70
    sget-object v0, Lcom/google/android/apps/gmm/util/i;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/util/i;->f:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method
