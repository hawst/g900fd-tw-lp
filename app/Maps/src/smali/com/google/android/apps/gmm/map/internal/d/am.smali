.class Lcom/google/android/apps/gmm/map/internal/d/am;
.super Lcom/google/android/apps/gmm/map/internal/d/b;
.source "PG"


# instance fields
.field final synthetic o:Lcom/google/android/apps/gmm/map/internal/d/al;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/al;Z)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->o:Lcom/google/android/apps/gmm/map/internal/d/al;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/a;Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 95
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v3, v3, v0

    array-length v3, v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/u/a/c;->a([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 105
    :goto_1
    return v0

    :catch_0
    move-exception v1

    .line 95
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected final a(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 6

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v3, v3, p1

    array-length v3, v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->o:Lcom/google/android/apps/gmm/map/internal/d/al;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/al;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->o:Lcom/google/android/apps/gmm/map/internal/d/al;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/d/al;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 80
    invoke-virtual {v4, p2, v5}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v4

    .line 79
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/u/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJ)Lcom/google/android/apps/gmm/map/u/a/c;

    move-result-object v0

    goto :goto_0
.end method

.method protected final b(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 8

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 86
    const/4 v1, 0x0

    .line 90
    :goto_0
    return-object v1

    .line 88
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bt;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->o:Lcom/google/android/apps/gmm/map/internal/d/al;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/al;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v4

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/am;->o:Lcom/google/android/apps/gmm/map/internal/d/al;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/al;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v6}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJ)V

    goto :goto_0
.end method
