.class public final enum Lcom/google/android/apps/gmm/map/internal/d/n;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/d/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/d/n;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/d/n;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/d/n;

.field public static final enum d:Lcom/google/android/apps/gmm/map/internal/d/n;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/map/internal/d/n;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/n;

    const-string v1, "LOCAL_ONLY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->a:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/n;

    const-string v1, "LOCAL_ONLY_WITH_SERVER_UPDATES"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->b:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/n;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/d/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->c:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 93
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/n;

    const-string v1, "SERVER_ONLY"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/internal/d/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 85
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/d/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/n;->a:Lcom/google/android/apps/gmm/map/internal/d/n;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/n;->b:Lcom/google/android/apps/gmm/map/internal/d/n;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/n;->c:Lcom/google/android/apps/gmm/map/internal/d/n;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/n;->d:Lcom/google/android/apps/gmm/map/internal/d/n;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->e:[Lcom/google/android/apps/gmm/map/internal/d/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/n;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/n;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/d/n;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/n;->e:[Lcom/google/android/apps/gmm/map/internal/d/n;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/d/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/d/n;

    return-object v0
.end method
