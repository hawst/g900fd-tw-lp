.class public Lcom/google/android/apps/gmm/base/views/MultiColumnListView;
.super Landroid/widget/ScrollView;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/listview/a;


# static fields
.field static final a:Ljava/lang/String;

.field static b:I

.field static c:I

.field static d:I


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/widget/Adapter;

.field public final g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

.field final h:Lcom/google/android/apps/gmm/base/views/aw;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/aq;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/android/apps/gmm/base/views/av;

.field private k:Z

.field private l:Z

.field private m:Lcom/google/android/apps/gmm/base/i/a;

.field private final n:Landroid/widget/ListAdapter;

.field private o:Z

.field private p:Ljava/lang/reflect/Field;

.field private q:Landroid/widget/OverScroller;

.field private final r:Landroid/database/DataSetObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    const-class v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    .line 73
    sput v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->b:I

    .line 74
    sput v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->c:I

    .line 75
    sput v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->k:Z

    .line 88
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->l:Z

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->e:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/ak;-><init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->r:Landroid/database/DataSetObserver;

    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;-><init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    .line 161
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-super {p0, v1, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 165
    new-instance v0, Lcom/google/android/apps/gmm/base/views/aw;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/aw;-><init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->h:Lcom/google/android/apps/gmm/base/views/aw;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->ae:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    sget v2, Lcom/google/android/apps/gmm/n;->ah:I

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a:I

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    sget v2, Lcom/google/android/apps/gmm/n;->ag:I

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->b:I

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    sget v2, Lcom/google/android/apps/gmm/n;->af:I

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->f:Z

    .line 175
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->R:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 180
    sget v1, Lcom/google/android/apps/gmm/n;->U:I

    .line 181
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 182
    sget v2, Lcom/google/android/apps/gmm/n;->W:I

    .line 183
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 184
    sget v3, Lcom/google/android/apps/gmm/n;->T:I

    .line 185
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 186
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 188
    new-instance v0, Lcom/google/android/apps/gmm/base/i/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/i/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/a;->g:Z

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/i/a;->i:Z

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    iput v4, v0, Lcom/google/android/apps/gmm/base/i/a;->j:I

    .line 195
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->n:Landroid/widget/ListAdapter;

    .line 198
    :try_start_0
    const-class v0, Landroid/widget/ScrollView;

    const-string v1, "mIsBeingDragged"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->p:Ljava/lang/reflect/Field;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->p:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_0
    :try_start_1
    const-class v0, Landroid/widget/ScrollView;

    const-string v1, "mScroller"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 206
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 207
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/OverScroller;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->q:Landroid/widget/OverScroller;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 211
    :goto_1
    return-void

    .line 201
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    goto :goto_0

    .line 209
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->p:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->q:Landroid/widget/OverScroller;

    if-eqz v0, :cond_2

    .line 403
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->p:Ljava/lang/reflect/Field;

    .line 404
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->q:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    move v2, v0

    .line 405
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->o:Z

    if-eq v0, v2, :cond_2

    .line 406
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->o:Z

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_2

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/aq;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/base/views/aq;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v2, v1

    .line 404
    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a:Ljava/lang/String;

    .line 415
    :cond_2
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 259
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 260
    check-cast v0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/b/b;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/i/a;->a(Lcom/google/android/apps/gmm/base/b/b;)Lcom/google/android/apps/gmm/base/i/a;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/i/c;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/i/c;->q:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V

    .line 276
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/views/ao;

    if-eqz v0, :cond_5

    .line 277
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ao;

    .line 278
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/views/ao;->a:Z

    if-eqz v0, :cond_5

    .line 279
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    const/4 v2, 0x1

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/i/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/i/c;->a:Lcom/google/android/apps/gmm/base/i/e;

    sget-object v3, Lcom/google/android/apps/gmm/base/i/e;->a:Lcom/google/android/apps/gmm/base/i/e;

    if-eq v0, v3, :cond_4

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "last item is not card"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 262
    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->n:Landroid/widget/ListAdapter;

    new-instance v3, Lcom/google/android/apps/gmm/base/i/c;

    sget-object v4, Lcom/google/android/apps/gmm/base/i/e;->b:Lcom/google/android/apps/gmm/base/i/e;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/base/i/c;-><init>(Lcom/google/android/apps/gmm/base/i/e;)V

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/i/a;->a(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;)Lcom/google/android/apps/gmm/base/i/a;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/i/c;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/i/c;->q:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V

    goto :goto_0

    .line 263
    :cond_2
    instance-of v0, p1, Landroid/view/ViewStub;

    if-eqz v0, :cond_3

    .line 266
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 267
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 268
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/i/a;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/i/a;

    move-object p1, v0

    .line 272
    goto/16 :goto_0

    .line 273
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/i/a;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/i/a;

    goto/16 :goto_0

    .line 279
    :cond_4
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/i/c;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/c;->p:Z

    .line 284
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 289
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->addView(Landroid/view/View;)V

    .line 291
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 254
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/ao;

    return v0
.end method

.method public computeScroll()V
    .locals 0

    .prologue
    .line 395
    invoke-super {p0}, Landroid/widget/ScrollView;->computeScroll()V

    .line 396
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a()V

    .line 397
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 237
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ao;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/ao;-><init>()V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 4

    .prologue
    .line 242
    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->ae:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 244
    new-instance v1, Lcom/google/android/apps/gmm/base/views/ao;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/ao;-><init>()V

    .line 245
    sget v2, Lcom/google/android/apps/gmm/n;->ai:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/views/ao;->a:Z

    .line 247
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 249
    return-object v1
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 596
    invoke-super {p0}, Landroid/widget/ScrollView;->onAttachedToWindow()V

    .line 598
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->l:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 599
    :cond_1
    const-string v0, "This view can\'t be reattached to a window after being destroyed"

    .line 602
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->l:Z

    .line 603
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 607
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    .line 609
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->l:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 610
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->l:Z

    .line 612
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/i/a;->a()Lcom/google/android/apps/gmm/base/b/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setAdapter(Landroid/widget/Adapter;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/al;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/views/al;-><init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;)V

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 228
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->m:Lcom/google/android/apps/gmm/base/i/a;

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 376
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 377
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 378
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a()V

    .line 383
    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 545
    check-cast p1, Lcom/google/android/apps/gmm/base/views/at;

    .line 546
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/at;->a:Landroid/os/Parcelable;

    invoke-super {p0, v0}, Landroid/widget/ScrollView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 547
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/at;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/base/views/at;->c:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setFirstVisiblePosition(II)V

    .line 549
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 535
    new-instance v1, Lcom/google/android/apps/gmm/base/views/at;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/at;-><init>()V

    .line 536
    invoke-super {p0}, Landroid/widget/ScrollView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/at;->a:Landroid/os/Parcelable;

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a(Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;)Lcom/google/b/a/an;

    move-result-object v2

    .line 538
    iget-object v0, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/base/views/at;->b:I

    .line 539
    iget-object v0, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/base/views/at;->c:I

    .line 540
    return-object v1
.end method

.method protected onScrollChanged(IIII)V
    .locals 2

    .prologue
    .line 440
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a()V

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->j:Lcom/google/android/apps/gmm/base/views/av;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->j:Lcom/google/android/apps/gmm/base/views/av;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/base/views/av;->onScrollChanged(IIII)V

    .line 445
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 388
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 389
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->a()V

    .line 390
    return v0
.end method

.method public sendAccessibilityEvent(I)V
    .locals 1

    .prologue
    .line 296
    const v0, 0x8000

    if-ne p1, v0, :cond_0

    .line 300
    :goto_0
    return-void

    .line 299
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public final setAdapter(Landroid/widget/Adapter;)V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->r:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 342
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->r:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 345
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/base/views/am;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/views/am;-><init>(Lcom/google/android/apps/gmm/base/views/MultiColumnListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 346
    return-void
.end method

.method public final setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    .line 309
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    if-eqz p2, :cond_1

    .line 308
    :goto_0
    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/gmm/base/b/b;->b(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setAdapter(Landroid/widget/Adapter;)V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 313
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 314
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 316
    :cond_0
    return-void

    .line 309
    :cond_1
    iget-object p2, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->n:Landroid/widget/ListAdapter;

    goto :goto_0
.end method

.method public final setFirstVisiblePosition(II)V
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    iput p1, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->d:I

    iput p2, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->e:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->requestLayout()V

    .line 493
    return-void
.end method
