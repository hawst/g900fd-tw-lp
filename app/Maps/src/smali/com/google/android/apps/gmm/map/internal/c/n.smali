.class public Lcom/google/android/apps/gmm/map/internal/c/n;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final h:Lcom/google/android/apps/gmm/map/internal/c/n;


# instance fields
.field a:Lcom/google/e/a/a/a/b;

.field b:Lcom/google/android/apps/gmm/map/indoor/d/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:Lcom/google/android/apps/gmm/map/internal/c/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/map/internal/c/cf;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/map/indoor/d/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/map/internal/c/cu;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field g:Lcom/google/android/apps/gmm/map/internal/c/bb;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/n;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/n;-><init>(Lcom/google/e/a/a/a/b;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/n;->h:Lcom/google/android/apps/gmm/map/internal/c/n;

    return-void
.end method

.method private constructor <init>(Lcom/google/e/a/a/a/b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    .line 34
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    .line 35
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->c:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 36
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->d:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 37
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->e:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 38
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->f:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 39
    return-void
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/n;
    .locals 5

    .prologue
    .line 138
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    .line 139
    if-lez v0, :cond_0

    .line 140
    new-array v0, v0, [B

    .line 141
    invoke-interface {p0, v0}, Ljava/io/DataInput;->readFully([B)V

    .line 142
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/maps/b/b/a;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 144
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v0, v0

    const/4 v3, 0x1

    new-instance v4, Lcom/google/e/a/a/a/c;

    invoke-direct {v4}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/n;-><init>(Lcom/google/e/a/a/a/b;)V

    .line 147
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/n;->h:Lcom/google/android/apps/gmm/map/internal/c/n;

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 128
    :goto_0
    monitor-exit p0

    return-void

    .line 94
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x1

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_7

    move v0, v3

    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {v1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    :cond_1
    move v0, v3

    :goto_2
    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x1

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 100
    const-wide/16 v4, -0x1

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(Lcom/google/e/a/a/a/b;J)Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x2

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_9

    move v0, v3

    :goto_3
    if-nez v0, :cond_3

    invoke-virtual {v1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    :cond_3
    move v0, v3

    :goto_4
    if-eqz v0, :cond_4

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x2

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 105
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/c;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->c:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 107
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x3

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_b

    move v0, v3

    :goto_5
    if-nez v0, :cond_5

    invoke-virtual {v1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    :cond_5
    move v0, v3

    :goto_6
    if-eqz v0, :cond_e

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x3

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 110
    const/4 v1, 0x2

    iget-object v4, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v6

    move v4, v2

    :goto_7
    if-ge v4, v5, :cond_d

    const/4 v1, 0x2

    const/16 v7, 0x1a

    invoke-virtual {v0, v1, v4, v7}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/indoor/d/f;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v6, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7

    :cond_7
    move v0, v2

    .line 94
    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_2

    :cond_9
    move v0, v2

    .line 103
    goto :goto_3

    :cond_a
    move v0, v2

    goto :goto_4

    :cond_b
    move v0, v2

    .line 107
    goto :goto_5

    :cond_c
    move v0, v2

    goto :goto_6

    .line 110
    :cond_d
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/g;

    invoke-virtual {v6}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/d/g;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->e:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 112
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x4

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_18

    move v0, v3

    :goto_8
    if-nez v0, :cond_f

    invoke-virtual {v1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_19

    :cond_f
    move v0, v3

    :goto_9
    if-eqz v0, :cond_10

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x4

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 115
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/cf;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/cf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->d:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 117
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x5

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1a

    move v0, v3

    :goto_a
    if-nez v0, :cond_11

    invoke-virtual {v1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1b

    :cond_11
    move v0, v3

    :goto_b
    if-eqz v0, :cond_15

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x5

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 120
    const/4 v4, 0x1

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_1c

    move v1, v3

    :goto_c
    if-nez v1, :cond_12

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1d

    :cond_12
    move v1, v3

    :goto_d
    if-eqz v1, :cond_1e

    const/4 v1, 0x1

    const/16 v4, 0x1c

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v5, v1

    :goto_e
    const/4 v4, 0x2

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_1f

    move v1, v3

    :goto_f
    if-nez v1, :cond_13

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_20

    :cond_13
    move v1, v3

    :goto_10
    if-eqz v1, :cond_21

    const/4 v1, 0x2

    const/16 v4, 0x1c

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :goto_11
    const/4 v6, 0x3

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_22

    move v1, v3

    :goto_12
    if-nez v1, :cond_14

    invoke-virtual {v0, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_23

    :cond_14
    move v1, v3

    :goto_13
    if-eqz v1, :cond_24

    const/4 v1, 0x3

    const/16 v6, 0x1c

    invoke-virtual {v0, v1, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_14
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cu;

    invoke-direct {v1, v5, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/cu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->f:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 122
    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x6

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_25

    move v0, v3

    :goto_15
    if-nez v0, :cond_16

    invoke-virtual {v1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_26

    :cond_16
    move v0, v3

    :goto_16
    if-eqz v0, :cond_17

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x6

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 124
    const/4 v1, 0x1

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    if-nez v1, :cond_27

    const/4 v0, 0x0

    :goto_17
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->g:Lcom/google/android/apps/gmm/map/internal/c/bb;

    .line 127
    :cond_17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_18
    move v0, v2

    .line 112
    goto/16 :goto_8

    :cond_19
    move v0, v2

    goto/16 :goto_9

    :cond_1a
    move v0, v2

    .line 117
    goto/16 :goto_a

    :cond_1b
    move v0, v2

    goto/16 :goto_b

    :cond_1c
    move v1, v2

    .line 120
    goto/16 :goto_c

    :cond_1d
    move v1, v2

    goto/16 :goto_d

    :cond_1e
    :try_start_2
    const-string v1, ""

    move-object v5, v1

    goto/16 :goto_e

    :cond_1f
    move v1, v2

    goto/16 :goto_f

    :cond_20
    move v1, v2

    goto/16 :goto_10

    :cond_21
    const-string v1, ""

    move-object v4, v1

    goto/16 :goto_11

    :cond_22
    move v1, v2

    goto :goto_12

    :cond_23
    move v1, v2

    goto :goto_13

    :cond_24
    const-string v0, ""

    goto :goto_14

    :cond_25
    move v0, v2

    .line 122
    goto :goto_15

    :cond_26
    move v0, v2

    goto :goto_16

    .line 124
    :cond_27
    const-string v4, ""

    const/4 v6, 0x2

    iget-object v5, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    if-lez v5, :cond_2d

    move v5, v3

    :goto_18
    if-nez v5, :cond_28

    invoke-virtual {v1, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2e

    :cond_28
    move v5, v3

    :goto_19
    if-eqz v5, :cond_29

    const/4 v4, 0x2

    const/16 v5, 0x1c

    invoke-virtual {v1, v4, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :cond_29
    const/4 v5, 0x2

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_2f

    move v1, v3

    :goto_1a
    if-nez v1, :cond_2a

    invoke-virtual {v0, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_30

    :cond_2a
    move v1, v3

    :goto_1b
    if-eqz v1, :cond_2c

    const/4 v1, 0x2

    const/16 v5, 0x1a

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_2c

    const/4 v1, 0x1

    const/16 v5, 0x15

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    if-eq v0, v3, :cond_2b

    if-ne v0, v8, :cond_31

    :cond_2b
    move v0, v3

    :goto_1c
    move v2, v0

    :cond_2c
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bb;

    invoke-direct {v0, v4, v2}, Lcom/google/android/apps/gmm/map/internal/c/bb;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_17

    :cond_2d
    move v5, v2

    goto :goto_18

    :cond_2e
    move v5, v2

    goto :goto_19

    :cond_2f
    move v1, v2

    goto :goto_1a

    :cond_30
    move v1, v2

    goto :goto_1b

    :cond_31
    move v0, v2

    goto :goto_1c
.end method
