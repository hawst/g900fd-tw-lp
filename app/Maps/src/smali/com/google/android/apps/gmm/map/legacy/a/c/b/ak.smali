.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field static final a:Landroid/graphics/Paint;


# instance fields
.field b:Landroid/graphics/BitmapFactory$Options;

.field c:I

.field public d:Lcom/google/android/apps/gmm/map/t/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a:Landroid/graphics/Paint;

    return-void
.end method

.method private constructor <init>([BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/t/as;ZLjava/util/List;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/map/internal/c/be;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Lcom/google/android/apps/gmm/map/t/as;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->b:Landroid/graphics/BitmapFactory$Options;

    .line 138
    invoke-static {p1}, Lcom/google/android/apps/gmm/v/s;->a([B)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 143
    new-instance v2, Lcom/google/android/apps/gmm/v/s;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/v/s;-><init>([B)V

    .line 144
    new-instance v3, Lcom/google/android/apps/gmm/v/ar;

    new-instance v4, Lcom/google/android/apps/gmm/v/u;

    invoke-direct {v4, v2}, Lcom/google/android/apps/gmm/v/u;-><init>(Lcom/google/android/apps/gmm/v/s;)V

    .line 145
    iget v5, v2, Lcom/google/android/apps/gmm/v/s;->a:I

    iget v2, v2, Lcom/google/android/apps/gmm/v/s;->b:I

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v2, v6}, Lcom/google/android/apps/gmm/v/ar;-><init>(Lcom/google/android/apps/gmm/v/at;IIZ)V

    .line 147
    array-length v2, p1

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->c:I

    .line 150
    const/4 v2, 0x1

    .line 174
    :goto_0
    new-instance v4, Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-direct {v4, p3, p2, v0, v1}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    .line 175
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Raster "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 176
    if-eqz v2, :cond_4

    .line 177
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 183
    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/v/ci;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    .line 184
    const/16 v3, 0x2601

    const/16 v4, 0x2601

    iget-boolean v5, v2, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput v3, v2, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, v2, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 185
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 187
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p10

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/4 v4, 0x1

    const/16 v5, 0x303

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p10

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x207

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 190
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p9

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    const/4 v3, 0x0

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 192
    :goto_2
    return-void

    .line 152
    :cond_2
    iget-object v2, p4, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a([B[F)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 153
    if-nez v2, :cond_3

    .line 154
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Can\'t load GLRaster bitmap for tile "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 155
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 158
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 159
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 162
    move-object/from16 v0, p10

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    iget-boolean v9, v3, Lcom/google/android/apps/gmm/v/ap;->d:Z

    .line 163
    if-nez v9, :cond_5

    .line 164
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/gmm/v/ar;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v8, v2

    .line 166
    :goto_3
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 167
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 168
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->c:I

    .line 170
    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    new-instance v3, Lcom/google/android/apps/gmm/v/au;

    invoke-direct {v3, v8}, Lcom/google/android/apps/gmm/v/au;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/v/ar;-><init>(Lcom/google/android/apps/gmm/v/as;IIIIZ)V

    move-object v3, v2

    move v2, v9

    goto/16 :goto_0

    .line 180
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->d:Lcom/google/android/apps/gmm/map/t/ar;

    const/high16 v4, 0x40000000    # 2.0f

    iget v5, v3, Lcom/google/android/apps/gmm/v/ar;->e:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    div-float v4, v5, v4

    const/high16 v5, 0x40000000    # 2.0f

    iget v6, v3, Lcom/google/android/apps/gmm/v/ar;->e:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    div-float v5, v6, v5

    const/high16 v6, 0x40000000    # 2.0f

    iget v7, v3, Lcom/google/android/apps/gmm/v/ar;->c:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    iget v7, v3, Lcom/google/android/apps/gmm/v/ar;->f:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    const/high16 v7, 0x3f800000    # 1.0f

    div-float v6, v7, v6

    const/high16 v7, 0x40000000    # 2.0f

    iget v8, v3, Lcom/google/android/apps/gmm/v/ar;->f:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f800000    # 1.0f

    div-float v7, v8, v7

    const/high16 v8, 0x40000000    # 2.0f

    iget v9, v3, Lcom/google/android/apps/gmm/v/ar;->d:I

    int-to-float v9, v9

    mul-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    const/16 v8, 0x14

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v8, v9

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x3

    aput v4, v8, v9

    const/4 v9, 0x4

    aput v6, v8, v9

    const/4 v9, 0x5

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x6

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x7

    const/4 v10, 0x0

    aput v10, v8, v9

    const/16 v9, 0x8

    aput v4, v8, v9

    const/16 v4, 0x9

    aput v7, v8, v4

    const/16 v4, 0xa

    const/high16 v9, 0x3f800000    # 1.0f

    aput v9, v8, v4

    const/16 v4, 0xb

    const/high16 v9, 0x3f800000    # 1.0f

    aput v9, v8, v4

    const/16 v4, 0xc

    const/4 v9, 0x0

    aput v9, v8, v4

    const/16 v4, 0xd

    aput v5, v8, v4

    const/16 v4, 0xe

    aput v6, v8, v4

    const/16 v4, 0xf

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v8, v4

    const/16 v4, 0x10

    const/4 v6, 0x0

    aput v6, v8, v4

    const/16 v4, 0x11

    const/4 v6, 0x0

    aput v6, v8, v4

    const/16 v4, 0x12

    aput v5, v8, v4

    const/16 v4, 0x13

    aput v7, v8, v4

    new-instance v4, Lcom/google/android/apps/gmm/v/av;

    const/16 v5, 0x11

    const/4 v6, 0x5

    invoke-direct {v4, v8, v5, v6}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    goto/16 :goto_1

    :cond_5
    move-object v8, v2

    goto/16 :goto_3
.end method

.method private declared-synchronized a([B[F)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->b:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->b:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->b:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 204
    const/4 v0, 0x0

    array-length v1, p1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->b:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p1, v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 205
    if-eqz p2, :cond_0

    array-length v0, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v2, 0x14

    if-eq v0, v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 213
    :goto_0
    monitor-exit p0

    return-object v0

    .line 208
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 210
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v4, Landroid/graphics/ColorMatrix;

    invoke-direct {v4, p2}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v3, v4}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 211
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 212
    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/ax;

    .line 75
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/internal/c/ax;->c:[I

    array-length v4, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v2, v1

    .line 76
    if-ltz v5, :cond_0

    array-length v6, p1

    if-ge v5, v6, :cond_0

    .line 77
    aget-object v5, p1, v5

    move-object/from16 v0, p8

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/internal/c/ax;->a:[B

    .line 83
    iget-object v5, v3, Lcom/google/android/apps/gmm/map/internal/c/ax;->b:Lcom/google/android/apps/gmm/map/internal/c/be;

    new-instance v8, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v8, v3}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    const/4 v9, 0x1

    move-object v3, p0

    move-object/from16 v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v10, p7

    move-object v11, p3

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;-><init>([BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/t/as;ZLjava/util/List;Lcom/google/android/apps/gmm/v/ad;)V

    return-object v1
.end method

.method public static a([BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;"
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/m;->a:Lcom/google/android/apps/gmm/map/t/m;

    .line 98
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    move-object v9, p4

    move-object/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;-><init>([BLcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/t/as;ZLjava/util/List;Lcom/google/android/apps/gmm/v/ad;)V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ak;->c:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 233
    const/16 v0, 0x60

    return v0
.end method
