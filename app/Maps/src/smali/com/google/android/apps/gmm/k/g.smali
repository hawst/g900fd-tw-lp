.class public Lcom/google/android/apps/gmm/k/g;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/k/a/a;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/k/a;

.field private final c:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/r/b/a/zt;",
            "Lcom/google/android/apps/gmm/k/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/k/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/k/g;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 30
    const-class v0, Lcom/google/r/b/a/zt;

    .line 31
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/g;->c:Ljava/util/EnumMap;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/r/b/a/zt;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 38
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 58
    :cond_0
    :goto_1
    return-void

    .line 40
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 44
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/k/g;->b:Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->v()Lcom/google/r/b/a/zn;

    move-result-object v0

    if-eqz v0, :cond_a

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/google/r/b/a/zn;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/r/b/a/zn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/zq;->d()Lcom/google/r/b/a/zq;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zq;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zq;

    iget v1, v0, Lcom/google/r/b/a/zq;->i:I

    invoke-static {v1}, Lcom/google/r/b/a/zt;->a(I)Lcom/google/r/b/a/zt;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    :cond_5
    if-ne v1, p1, :cond_4

    move-object v1, v0

    .line 46
    :goto_3
    if-eqz v1, :cond_0

    .line 50
    iget-object v3, p0, Lcom/google/android/apps/gmm/k/g;->c:Ljava/util/EnumMap;

    iget v0, v1, Lcom/google/r/b/a/zq;->i:I

    invoke-static {v0}, Lcom/google/r/b/a/zt;->a(I)Lcom/google/r/b/a/zt;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    :cond_6
    invoke-virtual {v3, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/k/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    sget-object v3, Lcom/google/android/apps/gmm/k/f;->f:Lcom/google/android/apps/gmm/k/f;

    if-ne v0, v3, :cond_9

    .line 53
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/k/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1, p2, v3}, Lcom/google/android/apps/gmm/k/a;-><init>(Lcom/google/r/b/a/zq;Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/k/a;->f:Lcom/google/android/libraries/a/c;

    invoke-virtual {v3}, Lcom/google/android/libraries/a/c;->b()V

    sget-object v3, Lcom/google/android/apps/gmm/k/f;->b:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    .line 55
    iget-object v3, p0, Lcom/google/android/apps/gmm/k/g;->c:Ljava/util/EnumMap;

    iget v0, v1, Lcom/google/r/b/a/zq;->i:I

    invoke-static {v0}, Lcom/google/r/b/a/zt;->a(I)Lcom/google/r/b/a/zt;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    invoke-virtual {v3, v0, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/k/a;->e:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    goto/16 :goto_1

    .line 45
    :cond_a
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_3
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 71
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 64
    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/g;->a:Lcom/google/android/apps/gmm/k/a;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/k/a;->e:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/k/a;->d:Lcom/google/android/apps/gmm/k/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/k/a;->a(Lcom/google/android/apps/gmm/k/f;)V

    .line 70
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/k/g;->b:Ljava/lang/String;

    goto :goto_1
.end method
