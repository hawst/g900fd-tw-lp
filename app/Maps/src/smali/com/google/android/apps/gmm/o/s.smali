.class public Lcom/google/android/apps/gmm/o/s;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/o/w;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 185
    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/o/w;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/w;->a()Ljava/lang/Boolean;

    move-result-object v1

    .line 187
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/o/w;

    invoke-static {v10}, Lcom/google/android/libraries/curvular/b/e;->c(I)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/o/w;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v2

    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/o/w;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/w;->c()Lcom/google/android/libraries/curvular/aw;

    move-result-object v3

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/o/w;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/w;->b()Ljava/lang/CharSequence;

    move-result-object v4

    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/o/w;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/w;->b()Ljava/lang/CharSequence;

    move-result-object v5

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/o/w;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/w;->d()Ljava/lang/Integer;

    move-result-object v0

    .line 185
    const/16 v6, 0x13

    new-array v6, v6, [Lcom/google/android/libraries/curvular/cu;

    sget-object v7, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v6, v11

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v6, v10

    sget-object v0, Lcom/google/android/apps/gmm/o/k;->a:Lcom/google/android/libraries/curvular/b;

    sget-object v7, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v6, v12

    const/4 v0, 0x3

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x4

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x5

    sget v7, Lcom/google/android/apps/gmm/d;->al:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v9, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    invoke-static {v1, v7, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x6

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bE:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x7

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->e:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v0

    const/16 v0, 0x8

    sget-object v7, Lcom/google/android/libraries/curvular/g;->K:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0x9

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->g()Lcom/google/android/libraries/curvular/au;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/curvular/ad;

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v3, v9, v11

    aput-object v7, v9, v10

    invoke-direct {v8, v9, v3, v7}, Lcom/google/android/libraries/curvular/ad;-><init>([Ljava/lang/Object;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)V

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->p()Lcom/google/android/libraries/curvular/au;

    move-result-object v3

    new-instance v7, Lcom/google/android/libraries/curvular/ad;

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v8, v9, v11

    aput-object v3, v9, v10

    invoke-direct {v7, v9, v8, v3}, Lcom/google/android/libraries/curvular/ad;-><init>([Ljava/lang/Object;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)V

    sget-object v3, Lcom/google/android/libraries/curvular/g;->I:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0xa

    const v3, 0x800013

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v7, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0xb

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v7, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0xc

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bQ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0xd

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bP:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0xe

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    sget-object v4, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0xf

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->e()Lcom/google/android/libraries/curvular/ar;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0x10

    sget v3, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v6, v0

    const/16 v0, 0x11

    sget-object v3, Lcom/google/android/libraries/curvular/g;->r:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v0

    const/16 v0, 0x12

    invoke-static {v2}, Lcom/google/android/libraries/curvular/t;->b(Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v6}, Lcom/google/android/apps/gmm/base/k/aa;->h([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method
