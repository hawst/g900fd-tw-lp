.class public Lcom/google/android/apps/gmm/map/internal/c/bn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static g:Lcom/google/android/apps/gmm/map/internal/c/bn;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:F

.field public final e:F

.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/high16 v1, -0x1000000

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bn;

    const/16 v3, 0xc

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->g:Lcom/google/android/apps/gmm/map/internal/c/bn;

    return-void
.end method

.method public constructor <init>(IIIFFI)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    .line 70
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    .line 71
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    .line 72
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    .line 73
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->e:F

    .line 74
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    .line 75
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/map/internal/c/bn;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->g:Lcom/google/android/apps/gmm/map/internal/c/bn;

    return-object v0
.end method

.method public static a(Lcom/google/maps/b/a/av;)Lcom/google/android/apps/gmm/map/internal/c/bn;
    .locals 7

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/maps/b/a/av;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 105
    iget-object v0, p0, Lcom/google/maps/b/a/av;->b:Lcom/google/maps/b/a/cz;

    iget v2, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 108
    invoke-virtual {p0}, Lcom/google/maps/b/a/av;->a()Lcom/google/maps/b/a/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/b/a/w;->a:Lcom/google/maps/b/a/cz;

    iget v3, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 112
    invoke-virtual {p0}, Lcom/google/maps/b/a/av;->a()Lcom/google/maps/b/a/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/b/a/w;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 111
    int-to-float v0, v0

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v4, v0, v4

    .line 116
    invoke-virtual {p0}, Lcom/google/maps/b/a/av;->a()Lcom/google/maps/b/a/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/b/a/w;->d:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 115
    int-to-float v0, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float v5, v0, v5

    .line 121
    invoke-virtual {p0}, Lcom/google/maps/b/a/av;->a()Lcom/google/maps/b/a/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/b/a/w;->b:Lcom/google/maps/b/a/cz;

    iget v6, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 123
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bn;
    .locals 7

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    .line 79
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 82
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 85
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    .line 88
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v0

    int-to-float v0, v0

    div-float v4, v0, v5

    .line 91
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v0

    int-to-float v0, v0

    div-float v5, v0, v5

    .line 94
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v6

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 193
    if-ne p0, p1, :cond_1

    .line 218
    :cond_0
    :goto_0
    return v0

    .line 196
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 197
    goto :goto_0

    .line 199
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 200
    goto :goto_0

    .line 202
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 203
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 204
    goto :goto_0

    .line 206
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 207
    goto :goto_0

    .line 209
    :cond_5
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_6

    move v0, v1

    .line 210
    goto :goto_0

    .line 212
    :cond_6
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_7
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_8
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->e:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bn;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    add-int/lit8 v0, v0, 0x1f

    .line 183
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    add-int/2addr v0, v1

    .line 184
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    add-int/2addr v0, v1

    .line 186
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    add-int/2addr v0, v1

    .line 187
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 228
    const-string v1, "TextStyle{color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 229
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", outlineColor="

    .line 230
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size="

    .line 231
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", leadingRatio="

    .line 232
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", trackingRatio="

    .line 233
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->e:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attributes="

    .line 234
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x7d

    .line 235
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 236
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
