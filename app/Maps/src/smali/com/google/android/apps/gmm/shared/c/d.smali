.class public Lcom/google/android/apps/gmm/shared/c/d;
.super Ljava/util/AbstractCollection;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:I

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field c:I

.field d:I


# direct methods
.method private constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 30
    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->c:I

    .line 32
    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    .line 66
    iput p1, p0, Lcom/google/android/apps/gmm/shared/c/d;->a:I

    .line 67
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    .line 68
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/shared/c/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/google/android/apps/gmm/shared/c/d",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/c/d;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/shared/c/d;->a:I

    if-ge v0, v1, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->c:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/android/apps/gmm/shared/c/d;->a:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->c:I

    .line 102
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    .line 103
    const/4 v0, 0x1

    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/shared/c/d;->c:I

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->c:I

    .line 110
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    .line 111
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/c/e;-><init>(Lcom/google/android/apps/gmm/shared/c/d;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
