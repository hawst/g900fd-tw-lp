.class public Lcom/google/android/apps/gmm/car/n/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lcom/google/android/apps/gmm/base/k/af;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 26
    sget v0, Lcom/google/android/apps/gmm/f;->x:I

    sget v1, Lcom/google/android/apps/gmm/f;->y:I

    new-instance v2, Lcom/google/android/libraries/curvular/a;

    const v3, -0xbd7a0c

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    new-instance v3, Lcom/google/android/libraries/curvular/a;

    const v4, -0xbcbcbd

    invoke-direct {v3, v4}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    new-instance v4, Lcom/google/android/libraries/curvular/a;

    const v5, -0x212122

    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    .line 27
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/k/c;->a(IILcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/af;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/n/c;->a:Lcom/google/android/apps/gmm/base/k/af;

    .line 26
    return-void
.end method

.method public static A()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 210
    sget v0, Lcom/google/android/apps/gmm/f;->C:I

    const/high16 v1, -0x22000000

    const v2, -0x59000001

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static B()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 215
    sget v0, Lcom/google/android/apps/gmm/f;->ab:I

    const/high16 v1, -0x22000000

    const v2, -0x59000001

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/aw;)Lcom/google/android/apps/gmm/base/k/an;
    .locals 4
    .param p0    # Lcom/google/android/libraries/curvular/aw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 93
    new-instance v0, Lcom/google/android/apps/gmm/base/k/an;

    const/high16 v1, 0x2e000000

    .line 94
    invoke-static {p0, v1, v3, v3}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    const v2, 0x2effffff

    .line 95
    invoke-static {p0, v2, v3, v3}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/apps/gmm/base/k/an;
    .locals 4
    .param p0    # Lcom/google/android/libraries/curvular/aw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/base/k/an;

    const/high16 v1, 0x17000000

    .line 55
    invoke-static {p0, v1, p1, v3}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    const v2, 0x27ffffff

    .line 56
    invoke-static {p0, v2, p1, v3}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    return-object v0
.end method

.method public static a()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/apps/gmm/f;->aq:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 220
    const v0, -0xbcbcbd

    const v1, -0x212122

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static a(III)Lcom/google/android/libraries/curvular/aw;
    .locals 4

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/apps/gmm/base/k/an;

    new-instance v1, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 226
    invoke-static {p0, v1, v2}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v2, p2}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 228
    invoke-static {p0, v2, v3}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    return-object v0
.end method

.method private static a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 236
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_1

    .line 237
    new-instance v0, Lcom/google/android/apps/gmm/car/n/d;

    invoke-direct {v0, p2, p3, p1, p0}, Lcom/google/android/apps/gmm/car/n/d;-><init>(ZZILcom/google/android/libraries/curvular/aw;)V

    move-object p0, v0

    .line 253
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    if-nez p0, :cond_0

    new-instance p0, Lcom/google/android/libraries/curvular/a;

    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    goto :goto_0
.end method

.method public static b()Lcom/google/android/apps/gmm/base/k/an;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    sget-object v0, Lcom/google/android/apps/gmm/car/n/a;->a:Lcom/google/android/apps/gmm/base/k/am;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/gmm/base/k/an;

    const/high16 v3, 0x17000000

    invoke-static {v0, v3, v1, v5}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v3

    const v4, 0x27ffffff

    invoke-static {v0, v4, v1, v5}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    return-object v2
.end method

.method public static b(Lcom/google/android/libraries/curvular/aw;)Lcom/google/android/libraries/curvular/aw;
    .locals 2
    .param p0    # Lcom/google/android/libraries/curvular/aw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 103
    const v0, 0x2effffff

    invoke-static {p0, v0, v1, v1}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/libraries/curvular/aw;
    .locals 2
    .param p0    # Lcom/google/android/libraries/curvular/aw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 65
    const v0, 0x27ffffff

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/google/android/apps/gmm/base/k/an;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 45
    sget-object v0, Lcom/google/android/apps/gmm/car/n/a;->a:Lcom/google/android/apps/gmm/base/k/am;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/gmm/base/k/an;

    const/high16 v3, 0x2e000000

    invoke-static {v0, v3, v1, v5}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v3

    const v4, 0x2effffff

    invoke-static {v0, v4, v1, v5}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    return-object v2
.end method

.method public static c(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/apps/gmm/base/k/an;
    .locals 4
    .param p0    # Lcom/google/android/libraries/curvular/aw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/base/k/an;

    const/high16 v1, 0x2e000000

    .line 75
    invoke-static {p0, v1, p1, v3}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    const v2, 0x2effffff

    .line 76
    invoke-static {p0, v2, p1, v3}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;IZZ)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/k/an;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V

    return-object v0
.end method

.method public static d()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 107
    sget v0, Lcom/google/android/apps/gmm/f;->P:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static e()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 111
    sget v0, Lcom/google/android/apps/gmm/f;->O:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static f()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 116
    sget v0, Lcom/google/android/apps/gmm/f;->T:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 121
    sget v0, Lcom/google/android/apps/gmm/f;->T:I

    new-instance v1, Lcom/google/android/libraries/curvular/a;

    const v2, -0xbd7a0c

    invoke-direct {v1, v2}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static h()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 126
    sget v0, Lcom/google/android/apps/gmm/f;->W:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static i()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 130
    sget v0, Lcom/google/android/apps/gmm/f;->ad:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static j()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 134
    sget v0, Lcom/google/android/apps/gmm/f;->R:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static k()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 138
    sget v0, Lcom/google/android/apps/gmm/f;->E:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static l()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 143
    sget v0, Lcom/google/android/apps/gmm/f;->Y:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static m()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 147
    sget v0, Lcom/google/android/apps/gmm/f;->ab:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static n()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 151
    sget v0, Lcom/google/android/apps/gmm/f;->ae:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static o()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 155
    sget v0, Lcom/google/android/apps/gmm/f;->U:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static p()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 159
    sget v0, Lcom/google/android/apps/gmm/f;->U:I

    const v1, 0x4c434343    # 5.1186956E7f

    const v2, 0x4cdedede    # 1.16848368E8f

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static q()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 164
    sget v0, Lcom/google/android/apps/gmm/f;->ah:I

    const v1, -0x666667

    const v2, -0x9e9e9f

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static r()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 169
    sget v0, Lcom/google/android/apps/gmm/f;->S:I

    const v1, -0x666667

    const v2, -0x9e9e9f

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static s()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 174
    sget v0, Lcom/google/android/apps/gmm/f;->V:I

    new-instance v1, Lcom/google/android/libraries/curvular/a;

    const v2, -0xbd7a0c

    invoke-direct {v1, v2}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static t()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 179
    sget v0, Lcom/google/android/apps/gmm/f;->C:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static u()Lcom/google/android/apps/gmm/base/k/ad;
    .locals 1

    .prologue
    .line 183
    sget-object v0, Lcom/google/android/apps/gmm/car/n/c;->a:Lcom/google/android/apps/gmm/base/k/af;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/k/af;->a()Lcom/google/android/apps/gmm/base/k/ad;

    move-result-object v0

    return-object v0
.end method

.method public static v()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 187
    sget v0, Lcom/google/android/apps/gmm/f;->ai:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static w()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 191
    sget v0, Lcom/google/android/apps/gmm/f;->aj:I

    const v1, -0xbcbcbd

    const v2, -0x212122

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static x()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 195
    sget v0, Lcom/google/android/apps/gmm/f;->X:I

    const/high16 v1, -0x22000000

    const v2, -0x59000001

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static y()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 200
    sget v0, Lcom/google/android/apps/gmm/f;->ag:I

    const/high16 v1, -0x22000000

    const v2, -0x59000001

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public static z()Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 205
    sget v0, Lcom/google/android/apps/gmm/f;->af:I

    const/high16 v1, -0x22000000

    const v2, -0x59000001

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/n/c;->a(III)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method
