.class public final Lcom/google/android/apps/gmm/base/l/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/libraries/curvular/aw;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/libraries/curvular/ce;

.field private final f:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/apps/gmm/z/b/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/libraries/curvular/ah;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/libraries/curvular/aw;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/apps/gmm/z/b/l;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/ak;->a:Lcom/google/android/libraries/curvular/ah;

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/ak;->b:Lcom/google/android/libraries/curvular/ah;

    .line 129
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/ak;->c:Lcom/google/android/libraries/curvular/ah;

    .line 130
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/l/ak;->d:Lcom/google/android/libraries/curvular/ah;

    .line 131
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    .line 132
    iput-object p6, p0, Lcom/google/android/apps/gmm/base/l/ak;->f:Lcom/google/android/libraries/curvular/b/i;

    .line 133
    iput-object p7, p0, Lcom/google/android/apps/gmm/base/l/ak;->g:Lcom/google/android/libraries/curvular/ah;

    .line 134
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 144
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ak;->a:Lcom/google/android/libraries/curvular/ah;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ak;->f:Lcom/google/android/libraries/curvular/b/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ak;->b:Lcom/google/android/libraries/curvular/ah;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ak;->d:Lcom/google/android/libraries/curvular/ah;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ak;->g:Lcom/google/android/libraries/curvular/ah;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/ak;->c:Lcom/google/android/libraries/curvular/ah;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ak;->e:Lcom/google/android/libraries/curvular/ce;

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method
