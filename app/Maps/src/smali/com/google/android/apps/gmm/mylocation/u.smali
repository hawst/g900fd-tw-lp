.class public Lcom/google/android/apps/gmm/mylocation/u;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/apps/gmm/mylocation/b/i;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/mylocation/n;

.field b:Lcom/google/android/apps/gmm/base/activities/c;

.field private f:I

.field private g:Lcom/google/android/apps/gmm/base/l/ae;

.field private h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

.field private i:Lcom/google/android/apps/gmm/mylocation/c/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/gmm/mylocation/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/u;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->f:I

    .line 113
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/n;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/mylocation/u;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/u;->k()V

    return-void
.end method

.method private j()V
    .locals 14
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/high16 v3, 0x41700000    # 15.0f

    const/4 v6, 0x0

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 270
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/w;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/mylocation/w;-><init>(Lcom/google/android/apps/gmm/mylocation/u;)V

    invoke-interface {v0, v5, v1}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(ZLcom/google/android/apps/gmm/mylocation/b/c;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_1

    move v0, v4

    :goto_0
    if-nez v0, :cond_2

    .line 340
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v5

    .line 283
    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v2

    .line 293
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/x;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v1, v1, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 333
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/u;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled autopan mode switch "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move-object v0, v2

    .line 336
    :goto_2
    if-eqz v0, :cond_0

    .line 337
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    .line 338
    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/b/a/a;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/maps/a/a;)V

    goto :goto_1

    .line 295
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v8

    .line 297
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v6, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    if-gez v6, :cond_4

    const/high16 v0, 0x3f800000    # 1.0f

    .line 296
    :goto_3
    const/high16 v6, 0x40000000    # 2.0f

    cmpg-float v6, v6, v0

    if-gtz v6, :cond_5

    const/high16 v6, 0x41a80000    # 21.0f

    cmpg-float v6, v0, v6

    if-gtz v6, :cond_5

    :goto_4
    if-eqz v4, :cond_6

    :goto_5
    cmpl-float v4, v1, v0

    if-lez v4, :cond_7

    .line 298
    :goto_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;F)V

    .line 302
    if-eqz v2, :cond_3

    .line 303
    invoke-static {v2}, Lcom/google/maps/a/a;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v1

    .line 304
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    .line 306
    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v4

    mul-double/2addr v2, v12

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    .line 307
    iget-object v4, v4, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    float-to-double v6, v0

    .line 309
    iget-object v0, v8, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    int-to-float v0, v0

    iget v8, v8, Lcom/google/android/apps/gmm/map/f/o;->i:F

    div-float/2addr v0, v8

    float-to-int v8, v0

    .line 304
    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/maps/a/c;DDDI)V

    .line 310
    invoke-virtual {v1}, Lcom/google/maps/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    goto/16 :goto_2

    .line 297
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v10

    int-to-double v6, v6

    mul-double/2addr v6, v10

    mul-double/2addr v6, v12

    double-to-float v0, v6

    iget-object v6, v8, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v6

    iget-object v7, v8, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x41f00000    # 30.0f

    div-float/2addr v0, v6

    const/high16 v6, 0x43800000    # 256.0f

    iget v9, v8, Lcom/google/android/apps/gmm/map/f/o;->i:F

    mul-float/2addr v6, v9

    mul-float/2addr v0, v6

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/s;->d(F)F

    move-result v0

    sub-float v0, v7, v0

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v0, v6

    goto/16 :goto_3

    :cond_5
    move v4, v5

    .line 296
    goto/16 :goto_4

    :cond_6
    move v0, v3

    goto/16 :goto_5

    :cond_7
    const/high16 v4, 0x41500000    # 13.0f

    cmpg-float v4, v1, v4

    if-gez v4, :cond_8

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/16 :goto_6

    :cond_8
    move v0, v1

    goto/16 :goto_6

    .line 314
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/g;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/e/b;

    .line 318
    new-instance v3, Lcom/google/android/apps/gmm/map/f/a/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/f/a/g;-><init>()V

    sget-object v5, Lcom/google/android/apps/gmm/map/f/a/h;->b:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 319
    iput-object v5, v3, Lcom/google/android/apps/gmm/map/f/a/g;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    const/high16 v5, 0x41880000    # 17.0f

    .line 320
    iput v5, v3, Lcom/google/android/apps/gmm/map/f/a/g;->a:F

    const/high16 v5, 0x42340000    # 45.0f

    iput v5, v3, Lcom/google/android/apps/gmm/map/f/a/g;->b:F

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/f/a/g;->a()Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/gmm/mylocation/e/b;-><init>(Lcom/google/android/apps/gmm/map/f/a/f;Z)V

    .line 316
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    move-object v0, v2

    goto/16 :goto_2

    .line 325
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/e/b;

    .line 327
    new-instance v3, Lcom/google/android/apps/gmm/map/f/a/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/f/a/g;-><init>()V

    sget-object v5, Lcom/google/android/apps/gmm/map/f/a/h;->a:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 328
    iput-object v5, v3, Lcom/google/android/apps/gmm/map/f/a/g;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    const/high16 v5, 0x41800000    # 16.0f

    .line 329
    iput v5, v3, Lcom/google/android/apps/gmm/map/f/a/g;->a:F

    iput v6, v3, Lcom/google/android/apps/gmm/map/f/a/g;->b:F

    iput v6, v3, Lcom/google/android/apps/gmm/map/f/a/g;->c:F

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/f/a/g;->a()Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/gmm/mylocation/e/b;-><init>(Lcom/google/android/apps/gmm/map/f/a/f;Z)V

    .line 325
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    move-object v0, v2

    .line 331
    goto/16 :goto_2

    .line 293
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private k()V
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->B()Lcom/google/r/b/a/dz;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/r/b/a/dz;->b:Z

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->f:I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->f:I

    .line 357
    invoke-static {v1}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->b(I)Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;

    move-result-object v1

    .line 356
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)Z

    .line 359
    :cond_0
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 5

    .prologue
    .line 176
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 178
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    .line 177
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Z)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->g:Lcom/google/android/apps/gmm/base/l/ae;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->e()V

    .line 189
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 5

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 129
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    .line 128
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;F)V

    .line 130
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->a(Landroid/app/Activity;)Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    .line 132
    sget v0, Lcom/google/android/apps/gmm/g;->cB:I

    .line 141
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 142
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/base/f/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 144
    new-instance v1, Lcom/google/android/apps/gmm/mylocation/v;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/mylocation/v;-><init>(Lcom/google/android/apps/gmm/mylocation/u;Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/gmm/base/l/ae;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/base/l/ae;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/libraries/curvular/ag;)V

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->g:Lcom/google/android/apps/gmm/base/l/ae;

    .line 145
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/mylocation/e/c;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/u;->j()V

    .line 244
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/p/c/a;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 248
    iget v0, p1, Lcom/google/android/apps/gmm/p/c/a;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->f:I

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->V:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->f:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Magnetometer acc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->b(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    .line 258
    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    if-ne v1, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    if-ne v0, v1, :cond_1

    .line 259
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/u;->k()V

    .line 261
    :cond_1
    return-void

    .line 258
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->f()V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->g:Lcom/google/android/apps/gmm/base/l/ae;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mylocation/n;->a()V

    .line 201
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 202
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_0

    .line 384
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/u;->j()V

    .line 386
    :cond_0
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/mylocation/b/f;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->h:Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->a:Lcom/google/android/apps/gmm/mylocation/n;

    iput-object v1, v0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    iput-object v1, v0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    .line 214
    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 215
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 216
    return-void
.end method

.method public final i()Lcom/google/android/apps/gmm/mylocation/b/a;
    .locals 3

    .prologue
    .line 405
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/j/b;)V

    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 220
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->E:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->i:Lcom/google/android/apps/gmm/mylocation/c/g;

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 224
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 223
    new-instance v3, Lcom/google/android/apps/gmm/mylocation/c/g;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/mylocation/c/g;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;)V

    iget-object v1, v3, Lcom/google/android/apps/gmm/mylocation/c/g;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/mylocation/c/g;->c:Z

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/c/n;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/mylocation/c/n;-><init>(Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/mylocation/c/g;->a(Lcom/google/android/apps/gmm/mylocation/c/f;)V

    new-instance v1, Lcom/google/android/apps/gmm/mylocation/c/l;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/mylocation/c/l;-><init>(Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/mylocation/c/g;->a(Lcom/google/android/apps/gmm/mylocation/c/f;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/mylocation/u;->i:Lcom/google/android/apps/gmm/mylocation/c/g;

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->i:Lcom/google/android/apps/gmm/mylocation/c/g;

    if-eqz v0, :cond_0

    .line 228
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/u;->i:Lcom/google/android/apps/gmm/mylocation/c/g;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/mylocation/c/g;->a(Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/u;->i:Lcom/google/android/apps/gmm/mylocation/c/g;

    goto :goto_0
.end method
