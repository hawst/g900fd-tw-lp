.class public Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;
.super Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;
.source "PG"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field c:Lcom/google/android/apps/gmm/hotels/a/e;

.field d:Landroid/view/View;

.field private final m:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;-><init>()V

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/hotels/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/hotels/a;-><init>(Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->m:Ljava/lang/Object;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;-><init>()V

    .line 91
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "placemark"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 92
    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/base/g/c;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v3, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/hotels/b/a;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->d:Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->g:Ljava/lang/String;

    const-string v1, "Hotel Placemark no longer has HotelBookingProto"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->d:Landroid/view/View;

    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v3, v0, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    invoke-static {v3, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/hotels/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    new-instance v0, Lcom/google/android/apps/gmm/hotels/c/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    invoke-direct {v0, v1, p1, p0, v2}, Lcom/google/android/apps/gmm/hotels/c/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;Lcom/google/android/apps/gmm/hotels/a/e;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->d:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->d:Landroid/view/View;

    goto :goto_1
.end method

.method protected final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->e:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 133
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->gX:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/google/b/f/t;->cH:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;->onPause()V

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 117
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->m:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 118
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->f()V

    .line 119
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacePageSubPageFragment;->onResume()V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->m:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 105
    return-void
.end method
