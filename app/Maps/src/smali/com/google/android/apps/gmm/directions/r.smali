.class Lcom/google/android/apps/gmm/directions/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V
    .locals 0

    .prologue
    .line 737
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/r;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/r;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 752
    :goto_0
    return-void

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/r;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/r;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget v2, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    if-ltz v2, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v0

    .line 744
    :goto_3
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gk;->g()Lcom/google/maps/g/a/gk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gk;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 743
    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v2

    if-nez v0, :cond_3

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v4, v3, v2

    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v2

    goto :goto_1

    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto :goto_3

    .line 745
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 746
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a:Ljava/lang/String;

    const-string v1, "Empty agency info should not be clickable."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 749
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/r;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    .line 750
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 749
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/directions/QuAgencyInfoFragment;->a(Ljava/util/List;Landroid/content/Context;)Lcom/google/android/apps/gmm/directions/QuAgencyInfoFragment;

    move-result-object v0

    .line 751
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/r;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_0
.end method
