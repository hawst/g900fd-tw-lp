.class Lcom/google/android/apps/gmm/place/cb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/i;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/place/g/a/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/g/a/d;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->c()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->c()Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->j()Lcom/google/android/apps/gmm/base/l/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/cb;->a:Lcom/google/android/apps/gmm/place/g/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/g/a/d;->a()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 113
    sget v0, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
