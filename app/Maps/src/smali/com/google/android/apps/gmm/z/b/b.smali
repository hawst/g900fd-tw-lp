.class public Lcom/google/android/apps/gmm/z/b/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/z/b/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/b/b;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/b/f/b/a/bc;)Lcom/google/b/f/b/a/bc;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    const/4 v2, 0x1

    .line 173
    new-instance v0, Lcom/google/android/libraries/memorymonitor/c;

    invoke-direct {v0}, Lcom/google/android/libraries/memorymonitor/c;-><init>()V

    .line 175
    sget-object v1, Lcom/google/android/apps/gmm/z/b/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/libraries/memorymonitor/c;->toString()Ljava/lang/String;

    .line 176
    iget-wide v4, v0, Lcom/google/android/libraries/memorymonitor/c;->f:J

    invoke-static {v12, v13, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 177
    const-wide/32 v6, 0x7fffffff

    iget-wide v0, v0, Lcom/google/android/libraries/memorymonitor/c;->h:J

    long-to-double v0, v0

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v8

    .line 178
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 177
    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 179
    invoke-static {}, Lcom/google/b/f/b/a/be;->newBuilder()Lcom/google/b/f/b/a/bg;

    move-result-object v1

    .line 180
    iget v3, v1, Lcom/google/b/f/b/a/bg;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/b/f/b/a/bg;->a:I

    iput-wide v4, v1, Lcom/google/b/f/b/a/bg;->b:J

    .line 181
    iget v3, v1, Lcom/google/b/f/b/a/bg;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Lcom/google/b/f/b/a/bg;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/bg;->c:I

    .line 182
    invoke-virtual {v1}, Lcom/google/b/f/b/a/bg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/be;

    .line 184
    iget v1, p0, Lcom/google/b/f/b/a/bc;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/aa;->d()Lcom/google/b/f/b/a/aa;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/b/f/b/a/aa;

    if-eqz v1, :cond_1

    .line 185
    iget-object v1, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/aa;->d()Lcom/google/b/f/b/a/aa;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/b/f/b/a/aa;

    invoke-static {v1}, Lcom/google/b/f/b/a/aa;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/ac;

    move-result-object v1

    .line 189
    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 184
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 187
    :cond_1
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v1

    goto :goto_1

    .line 189
    :cond_2
    iget-object v3, v1, Lcom/google/b/f/b/a/ac;->d:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v10, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v3, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, v1, Lcom/google/b/f/b/a/ac;->a:I

    .line 190
    iget-object v0, p0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/b/f/b/a/ac;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v10, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    iget v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/b/f/b/a/bc;->a:I

    .line 192
    return-object p0
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/b/f/b/a/cy;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 142
    invoke-static {}, Lcom/google/b/f/b/a/cy;->newBuilder()Lcom/google/b/f/b/a/da;

    move-result-object v1

    .line 144
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 145
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 146
    :goto_0
    iget v3, v1, Lcom/google/b/f/b/a/da;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v1, Lcom/google/b/f/b/a/da;->a:I

    iput-boolean v0, v1, Lcom/google/b/f/b/a/da;->c:Z

    .line 147
    if-nez v0, :cond_1

    .line 148
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    iget v2, v1, Lcom/google/b/f/b/a/da;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/b/f/b/a/da;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/da;->b:F

    .line 160
    :goto_1
    invoke-virtual {v1}, Lcom/google/b/f/b/a/da;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cy;

    return-object v0

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "screen_brightness"

    .line 152
    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 154
    const/4 v2, 0x0

    int-to-float v0, v0

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v0, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v2, v1, Lcom/google/b/f/b/a/da;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/b/f/b/a/da;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/da;->b:F
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Lcom/google/b/f/b/a/w;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 37
    .line 42
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/h/a;->c(Landroid/content/Context;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 46
    :try_start_1
    const-string v0, "status"

    const/4 v2, -0x1

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v4, Lcom/google/r/b/a/bc;->a:Lcom/google/r/b/a/bc;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    .line 50
    :goto_0
    :try_start_2
    invoke-static {v5}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/content/Intent;)Lcom/google/r/b/a/gd;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v3

    .line 51
    :try_start_3
    invoke-static {v5}, Lcom/google/android/apps/gmm/map/h/a;->a(Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v2

    .line 52
    :try_start_4
    invoke-static {}, Lcom/google/b/f/b/a/w;->newBuilder()Lcom/google/b/f/b/a/y;

    move-result-object v0

    .line 53
    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    const-string v7, "Intent: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", BatteryChargingState: "

    .line 60
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ExternalPowerSource: "

    .line 61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", chargeLevelPercent: "

    .line 62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 63
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 64
    :goto_2
    return-object v0

    .line 46
    :pswitch_0
    :try_start_5
    sget-object v4, Lcom/google/r/b/a/bc;->b:Lcom/google/r/b/a/bc;

    goto :goto_0

    :pswitch_1
    sget-object v4, Lcom/google/r/b/a/bc;->d:Lcom/google/r/b/a/bc;

    goto :goto_0

    :pswitch_2
    sget-object v4, Lcom/google/r/b/a/bc;->e:Lcom/google/r/b/a/bc;

    goto :goto_0

    :pswitch_3
    sget-object v4, Lcom/google/r/b/a/bc;->c:Lcom/google/r/b/a/bc;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 53
    :cond_0
    :try_start_6
    iget v6, v0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Lcom/google/b/f/b/a/y;->a:I

    iget v6, v4, Lcom/google/r/b/a/bc;->f:I

    iput v6, v0, Lcom/google/b/f/b/a/y;->b:I

    .line 54
    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v6, v0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v0, Lcom/google/b/f/b/a/y;->a:I

    iget v6, v3, Lcom/google/r/b/a/gd;->e:I

    iput v6, v0, Lcom/google/b/f/b/a/y;->c:I

    .line 55
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget v7, v0, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v7, v7, 0x4

    iput v7, v0, Lcom/google/b/f/b/a/y;->a:I

    iput v6, v0, Lcom/google/b/f/b/a/y;->d:I

    .line 56
    invoke-virtual {v0}, Lcom/google/b/f/b/a/y;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/w;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_2

    .line 57
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_1

    :catch_4
    move-exception v0

    move-object v2, v1

    goto/16 :goto_1

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Z)Lcom/google/b/f/b/a/w;
    .locals 5

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/h/a;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 75
    if-eqz p1, :cond_1

    .line 76
    sget-object v1, Lcom/google/r/b/a/bc;->b:Lcom/google/r/b/a/bc;

    .line 77
    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/content/Intent;)Lcom/google/r/b/a/gd;

    move-result-object v0

    .line 78
    sget-object v3, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    if-ne v0, v3, :cond_0

    .line 82
    sget-object v0, Lcom/google/r/b/a/gd;->c:Lcom/google/r/b/a/gd;

    .line 88
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/b/f/b/a/w;->newBuilder()Lcom/google/b/f/b/a/y;

    move-result-object v3

    .line 89
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 85
    :cond_1
    sget-object v1, Lcom/google/r/b/a/bc;->d:Lcom/google/r/b/a/bc;

    .line 86
    sget-object v0, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 89
    :cond_2
    iget v4, v3, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/b/f/b/a/y;->a:I

    iget v1, v1, Lcom/google/r/b/a/bc;->f:I

    iput v1, v3, Lcom/google/b/f/b/a/y;->b:I

    .line 90
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v1, v3, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v3, Lcom/google/b/f/b/a/y;->a:I

    iget v0, v0, Lcom/google/r/b/a/gd;->e:I

    iput v0, v3, Lcom/google/b/f/b/a/y;->c:I

    .line 91
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/a;->a(Landroid/content/Intent;)I

    move-result v0

    iget v1, v3, Lcom/google/b/f/b/a/y;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v3, Lcom/google/b/f/b/a/y;->a:I

    iput v0, v3, Lcom/google/b/f/b/a/y;->d:I

    .line 92
    invoke-virtual {v3}, Lcom/google/b/f/b/a/y;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/w;

    return-object v0
.end method

.method private static a(Landroid/content/Intent;)Lcom/google/r/b/a/gd;
    .locals 2

    .prologue
    .line 122
    const-string v0, "plugged"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 123
    packed-switch v0, :pswitch_data_0

    .line 131
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    :goto_0
    return-object v0

    .line 125
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/gd;->b:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 127
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/gd;->c:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 129
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/gd;->d:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
