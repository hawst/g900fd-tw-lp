.class public Lcom/google/android/apps/gmm/z/b/q;
.super Lcom/google/android/apps/gmm/z/b/a;
.source "PG"


# instance fields
.field private final a:Lcom/google/maps/a/a;

.field private final b:Lcom/google/maps/g/f/a;


# direct methods
.method public constructor <init>(Lcom/google/maps/a/a;Lcom/google/maps/g/f/a;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 25
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/maps/a/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/z/b/q;->a:Lcom/google/maps/a/a;

    .line 26
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/b/q;->b:Lcom/google/maps/g/f/a;

    .line 27
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/r/b/a/apf;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 32
    invoke-static {}, Lcom/google/r/b/a/aqm;->newBuilder()Lcom/google/r/b/a/aqo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/q;->a:Lcom/google/maps/a/a;

    .line 33
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v2, v0, Lcom/google/r/b/a/aqo;->b:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/aqo;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/r/b/a/aqo;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/q;->b:Lcom/google/maps/g/f/a;

    .line 34
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v2, v0, Lcom/google/r/b/a/aqo;->c:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/aqo;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/r/b/a/aqo;->a:I

    .line 32
    iget-object v1, p1, Lcom/google/r/b/a/apf;->k:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/r/b/a/aqo;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 35
    return-void
.end method
