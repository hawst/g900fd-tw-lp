.class public abstract Lcom/google/android/apps/gmm/map/legacy/internal/a/b;
.super Lcom/google/android/apps/gmm/map/legacy/internal/a/d;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/gmm/map/legacy/internal/a/d;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/a/d;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->d:Z

    .line 31
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 59
    if-nez p1, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->d:Z

    if-nez v0, :cond_2

    .line 64
    if-eqz p1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->b(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->c(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->d(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->d:Z

    goto :goto_0

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->b(Ljava/lang/Object;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method protected abstract c(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method protected abstract d(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->d:Z

    return v0
.end method
