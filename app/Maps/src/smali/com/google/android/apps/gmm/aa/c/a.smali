.class public Lcom/google/android/apps/gmm/aa/c/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/aa/c/a/a;


# static fields
.field static final a:Landroid/content/Intent;


# instance fields
.field private b:Lcom/google/android/apps/gmm/base/activities/c;

.field private c:Lcom/google/android/apps/gmm/aa/a/b/e;

.field private f:Lcom/google/android/apps/gmm/aa/a/c;

.field private g:Z

.field private h:Z

.field private final i:Lcom/google/android/apps/gmm/aa/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.speech.extra.MAX_RESULTS"

    const/4 v2, 0x5

    .line 48
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/c/a;->a:Landroid/content/Intent;

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/aa/c/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/aa/c/b;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/aa/c/a;-><init>(Lcom/google/android/apps/gmm/aa/c/b;)V

    .line 62
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/aa/c/b;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/c/a;->i:Lcom/google/android/apps/gmm/aa/c/b;

    .line 67
    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 118
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 119
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "handleRecognitionResult: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/aa/c/a;->h:Z

    if-eqz v0, :cond_8

    .line 125
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/b/f;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/aa/a/b/f;-><init>(Landroid/content/Intent;)V

    .line 126
    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/c/a;->c:Lcom/google/android/apps/gmm/aa/a/b/e;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/aa/a/b/e;->a(Lcom/google/android/apps/gmm/aa/a/b/f;)Landroid/content/Intent;

    move-result-object v4

    .line 128
    if-eqz v4, :cond_2

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/c/a;->f:Lcom/google/android/apps/gmm/aa/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/c;->a:Ljava/lang/String;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/aa/a/c;->g:Z

    iput v2, v0, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 133
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/c/a;->f:Lcom/google/android/apps/gmm/aa/a/c;

    iget-object v5, v0, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    invoke-virtual {v5}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    move-object v0, v1

    :goto_1
    sget-object v5, Lcom/google/android/apps/gmm/aa/a/c;->a:Ljava/lang/String;

    const-string v5, "onVoiceCommandFailure: failureCount: %d cycleIndex: %d"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, v4, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    iget v7, v4, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/aa/a/c;->g:Z

    iget-object v5, v4, Lcom/google/android/apps/gmm/aa/a/c;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x32

    if-le v5, v6, :cond_6

    :cond_3
    iget-object v0, v4, Lcom/google/android/apps/gmm/aa/a/c;->b:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/gmm/l;->oE:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v5, v4, Lcom/google/android/apps/gmm/aa/a/c;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v5, v0, v1, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    iget v0, v4, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    if-lt v0, v8, :cond_4

    iget v0, v4, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_7

    iget-object v0, v4, Lcom/google/android/apps/gmm/aa/a/c;->f:Ljava/lang/String;

    :goto_3
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/aa/a/c;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v4, Lcom/google/android/apps/gmm/aa/a/c;->b:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->oJ:I

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v2

    aput-object v5, v8, v3

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/a/c;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/a/c;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/aa/a/c;->e:Lcom/google/android/apps/gmm/z/a/b;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v2, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v2, Lcom/google/b/f/t;->bQ:Lcom/google/b/f/t;

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0, v2}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/a/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/a/b/g;->a:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    iget-object v5, v4, Lcom/google/android/apps/gmm/aa/a/c;->b:Landroid/content/Context;

    sget v6, Lcom/google/android/apps/gmm/l;->oF:I

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/aa/a/c;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 140
    :cond_8
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 141
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/aa/c/a;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 142
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 143
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_a

    :cond_9
    move v1, v3

    :goto_4
    if-nez v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v5, Lcom/google/r/b/a/a;->i:Lcom/google/r/b/a/a;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 146
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v5

    new-array v3, v3, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->gn:Lcom/google/b/f/t;

    aput-object v6, v3, v2

    .line 147
    iput-object v3, v5, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 148
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 144
    invoke-interface {v1, v4, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v2

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/base/e/e;

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/gmm/base/e/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_a
    move v1, v2

    .line 143
    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 4

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 75
    new-instance v1, Lcom/google/android/apps/gmm/aa/a/b/e;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    new-instance v2, Lcom/google/android/apps/gmm/aa/a/b/c;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/aa/a/b/c;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/aa/a/b/e;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/aa/a/b/c;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/aa/c/a;->c:Lcom/google/android/apps/gmm/aa/a/b/e;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/c/a;->i:Lcom/google/android/apps/gmm/aa/c/b;

    new-instance v1, Lcom/google/android/apps/gmm/aa/a/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v2, p1, v3, v0}, Lcom/google/android/apps/gmm/aa/a/c;-><init>(Lcom/google/android/apps/gmm/navigation/a/d;Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/aa/c/a;->f:Lcom/google/android/apps/gmm/aa/a/c;

    .line 77
    return-void
.end method

.method public final a(ZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/aa/c/a;->g:Z

    .line 86
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/aa/c/a;->h:Z

    .line 88
    if-eqz p2, :cond_1

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/c/a;->f:Lcom/google/android/apps/gmm/aa/a/c;

    sget-object v3, Lcom/google/android/apps/gmm/aa/a/c;->a:Ljava/lang/String;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/aa/a/c;->g:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/apps/gmm/aa/a/c;->a:Ljava/lang/String;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/aa/a/c;->g:Z

    iput v1, v2, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    :cond_0
    iput-boolean v0, v2, Lcom/google/android/apps/gmm/aa/a/c;->g:Z

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/aa/a/g;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/g;->a:Landroid/content/Intent;

    .line 104
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v2, Lcom/google/android/apps/gmm/l/al;->c:Lcom/google/android/apps/gmm/l/al;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/al;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivityForResult(Landroid/content/Intent;I)V

    .line 109
    :goto_1
    return-void

    .line 97
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/c/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v3, Lcom/google/android/apps/gmm/aa/c/a;->a:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v4, 0x10000

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    :goto_2
    if-eqz v0, :cond_4

    .line 98
    sget-object v0, Lcom/google/android/apps/gmm/aa/c/a;->a:Landroid/content/Intent;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 97
    goto :goto_2

    .line 100
    :cond_4
    const-string v0, "VoiceRecognitionVeneerImpl"

    const-string v2, "Unable to start voice recognition intent."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/c/a;->f:Lcom/google/android/apps/gmm/aa/a/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/aa/a/c;->c:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aG:Lcom/google/android/apps/gmm/shared/b/c;

    iget v3, v0, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/aa/a/c;->c:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aH:Lcom/google/android/apps/gmm/shared/b/c;

    iget v0, v0, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    .line 157
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 158
    return-void
.end method
