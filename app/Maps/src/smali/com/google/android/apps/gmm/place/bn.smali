.class Lcom/google/android/apps/gmm/place/bn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/am;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/x/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    return-object v0
.end method

.method public final a(ILcom/google/android/apps/gmm/place/PlacePageView;)V
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/place/b/a;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->o:Lcom/google/android/apps/gmm/place/m;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/af;->A()Lcom/google/android/apps/gmm/base/l/a/ab;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/m;->a()V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->n:Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 161
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bn;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/place/af;)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public final aa_()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method
