.class Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field private final c:Lcom/google/android/apps/gmm/map/internal/c/bn;

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FIII)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->a:Ljava/lang/String;

    .line 91
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 92
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 93
    iput p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->d:F

    .line 94
    iput p5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->e:I

    .line 95
    iput p6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->f:I

    .line 96
    iput p7, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->g:I

    .line 97
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 116
    if-ne p1, p0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 119
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;

    if-eqz v2, :cond_4

    .line 120
    check-cast p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;

    .line 121
    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->d:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->e:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->f:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->g:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->g:I

    if-ne v2, v3, :cond_3

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-ne v2, v3, :cond_3

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eq v2, v3, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 127
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->a:Ljava/lang/String;

    .line 128
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 130
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 103
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v1, :cond_0

    .line 105
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->c:Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/bn;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->e:I

    add-int/2addr v0, v1

    .line 109
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->f:I

    add-int/2addr v0, v1

    .line 110
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/e;->g:I

    add-int/2addr v0, v1

    .line 111
    return v0
.end method
