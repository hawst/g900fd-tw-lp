.class public Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;
.super Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;
.source "PG"


# instance fields
.field private final d:Lcom/google/android/apps/gmm/map/t;

.field private final e:Lcom/google/android/apps/gmm/map/util/b/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/util/b/a/a;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p3, Lcom/google/android/apps/gmm/map/t;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->d:Lcom/google/android/apps/gmm/map/t;

    .line 31
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p4, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->e:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 32
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/widget/ImageView;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 37
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 38
    sget v1, Lcom/google/android/apps/gmm/f;->bz:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 39
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 40
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 42
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    return-object v0
.end method

.method protected final a()Lcom/google/android/apps/gmm/map/f/o;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 72
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/c;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 64
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/c;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 65
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 66
    return-void
.end method

.method protected final b()Lcom/google/android/apps/gmm/map/util/b/a/a;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->e:Lcom/google/android/apps/gmm/map/util/b/a/a;

    return-object v0
.end method

.method protected final c()Lcom/google/android/apps/gmm/v/ad;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    return-object v0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 79
    return-void
.end method
