.class public Lcom/google/android/apps/gmm/base/l/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/q;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/o;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/base/l/q;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/android/apps/gmm/map/indoor/d/a;

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/ag;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/base/l/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/r;->h:Z

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/r;->c:Lcom/google/android/libraries/curvular/ag;

    .line 60
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 61
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/indoor/d/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 126
    if-nez p1, :cond_0

    .line 135
    :goto_0
    return v0

    .line 129
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 130
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    monitor-enter v2

    .line 131
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    const/4 v0, 0x1

    monitor-exit v2

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 239
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 240
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 241
    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v2, v0

    .line 243
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->g:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eq v0, v2, :cond_0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v4

    :goto_1
    if-nez v0, :cond_5

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/l/r;->g:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/l/o;

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/base/l/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/l/r;->g:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/indoor/d/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    check-cast v0, Lcom/google/android/apps/gmm/base/l/p;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/l/p;->d:Z

    goto :goto_2

    .line 242
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-object v2, v0

    goto :goto_0

    :cond_2
    move v0, v3

    .line 243
    goto :goto_1

    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/base/l/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/l/p;->d:Z

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/r;->c()V

    .line 245
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/r;->h:Z

    if-eqz v0, :cond_6

    if-eqz v2, :cond_6

    .line 246
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/l/r;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/r;->c()V

    .line 250
    :cond_6
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/s/b;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 255
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 256
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/b;->a:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/r;->h:Z

    .line 257
    return-void

    .line 256
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    monitor-enter v1

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    if-eq v0, p1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 147
    monitor-exit v1

    .line 154
    :goto_1
    return-void

    .line 146
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/r;->d()V

    .line 152
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/r;->c()V

    goto :goto_1

    .line 152
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-nez v0, :cond_0

    move v0, v2

    .line 198
    :goto_0
    return v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->d:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(Lcom/google/android/apps/gmm/map/b/a/l;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/e;)I

    move-result v1

    .line 181
    if-gez v1, :cond_1

    move v0, v2

    .line 182
    goto :goto_0

    .line 185
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/base/l/r;->f:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/android/apps/gmm/base/l/r;->f:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/l/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/o;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 186
    goto :goto_0

    .line 189
    :cond_2
    iput v1, p0, Lcom/google/android/apps/gmm/base/l/r;->f:I

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/l/o;

    move-object v1, v0

    .line 191
    check-cast v1, Lcom/google/android/apps/gmm/base/l/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/indoor/d/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    check-cast v0, Lcom/google/android/apps/gmm/base/l/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/l/p;->b:Z

    goto :goto_1

    .line 194
    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/base/l/p;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/l/p;->b:Z

    goto :goto_1

    .line 197
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/r;->c()V

    move v0, v3

    .line 198
    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->b()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/l/r;->a(Ljava/util/Set;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->c:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/r;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/l/o;

    move-object v1, v0

    .line 112
    check-cast v1, Lcom/google/android/apps/gmm/base/l/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/l/r;->a(Lcom/google/android/apps/gmm/map/indoor/d/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    check-cast v0, Lcom/google/android/apps/gmm/base/l/p;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/l/p;->c:Z

    goto :goto_0

    .line 115
    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/base/l/p;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/l/p;->c:Z

    goto :goto_0

    .line 118
    :cond_1
    return-void
.end method
