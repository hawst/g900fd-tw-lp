.class public Lcom/google/android/apps/gmm/p/g/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;
.implements Lcom/google/android/gms/location/f;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/p/g/a;

.field private final b:Lcom/google/android/gms/location/LocationRequest;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/p/g/a;)V
    .locals 4

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    .line 46
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/16 v1, 0x64

    .line 47
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->b:Lcom/google/android/gms/location/LocationRequest;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 91
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/g/a;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 94
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 52
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConnected("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/p/g/a;->d:Z

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->a()Landroid/location/Location;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "initialLocation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/p/g/a;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/g/b;->b:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/location/e;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;)V

    .line 71
    :goto_0
    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/g/a;->b:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 69
    const-string v1, "GCoreRawLocationEventPoster"

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 5

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 76
    const-string v0, "GCoreRawLocationEventPoster"

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onConnectionFailed("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/p/g/a;->c:Z

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/g/b;->a:Lcom/google/android/apps/gmm/p/g/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/g/a;->c()V

    .line 81
    return-void
.end method

.method public final am_()V
    .locals 0

    .prologue
    .line 85
    return-void
.end method
