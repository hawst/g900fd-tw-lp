.class public Lcom/google/android/apps/gmm/streetview/internal/ay;
.super Lcom/google/android/apps/gmm/streetview/internal/az;
.source "PG"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 205
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nuniform mat3 uDepthMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec3 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture1;\nvoid main() {\n  vec2 depthCoord = (uDepthMatrix * vec3(aTextureCoord, 1.0)).xy;\n  float maxDepth = 100.0;\n  float depth = texture2D(sTexture1, depthCoord).r;\n  vec4 p = vec4(aPosition.xyz * depth * maxDepth, aPosition.w);\n  vTextureCoord = uTextureMatrix * vec3(aTextureCoord, 1.0) * depth;\n  gl_Position = uMVPMatrix * p;\n  vColor = aColor;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec3 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nvoid main() {\n  vec4 c = texture2DProj(sTexture0, vTextureCoord);\n  gl_FragColor = vColor * c;\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/az;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/az;->a(I)V

    .line 211
    const-string v0, "sTexture1"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ay;->a:I

    .line 212
    const-string v0, "uDepthMatrix"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/streetview/internal/ay;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ay;->b:I

    .line 213
    return-void
.end method
