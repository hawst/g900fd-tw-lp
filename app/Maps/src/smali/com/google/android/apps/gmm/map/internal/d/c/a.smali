.class public Lcom/google/android/apps/gmm/map/internal/d/c/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/a/g;


# static fields
.field static final h:J

.field static final i:J

.field private static final j:Ljava/lang/String;

.field private static final m:Ljava/util/regex/Pattern;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field final b:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/a;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/a;",
            ">;>;"
        }
    .end annotation
.end field

.field final d:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field volatile f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

.field final g:Ljava/util/concurrent/CountDownLatch;

.field private final k:Lcom/google/android/apps/gmm/shared/net/r;

.field private final l:Lcom/google/android/apps/gmm/shared/c/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 132
    const-class v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    .line 157
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->h:J

    .line 160
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->i:J

    .line 162
    const-string v0, "https?://paint\\.sandbox\\.google\\.com/.*"

    .line 163
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->m:Ljava/util/regex/Pattern;

    .line 162
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 225
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->k:Lcom/google/android/apps/gmm/shared/net/r;

    .line 226
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->l:Lcom/google/android/apps/gmm/shared/c/f;

    .line 227
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 228
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 229
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 230
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

    .line 231
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->g:Ljava/util/concurrent/CountDownLatch;

    .line 232
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    .line 233
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/c/a/a;)V
    .locals 8

    .prologue
    const/16 v7, 0x20

    .line 171
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v0, 0x40

    const-string v1, "ResourceManager resource cache"

    .line 175
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v5

    invoke-direct {v4, v0, v1, v5}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    new-instance v5, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v0, "ResourceManager soft resource cache"

    .line 179
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    invoke-direct {v5, v7, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    new-instance v6, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v0, "ResourceManager bitmap cache"

    .line 183
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    invoke-direct {v6, v7, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    move-object v0, p0

    move-object v1, p1

    move-object v7, p2

    .line 171
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/d/c/a;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/util/a/e;Lcom/google/android/apps/gmm/map/util/a/e;Lcom/google/android/apps/gmm/map/util/a/e;Lcom/google/android/apps/gmm/map/internal/d/c/a/a;)V

    .line 185
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/util/a/e;Lcom/google/android/apps/gmm/map/util/a/e;Lcom/google/android/apps/gmm/map/util/a/e;Lcom/google/android/apps/gmm/map/internal/d/c/a/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/d/ac;",
            "Lcom/google/android/apps/gmm/shared/net/r;",
            "Lcom/google/android/apps/gmm/shared/c/f;",
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/a;",
            ">;",
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/a;",
            ">;>;",
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/d/c/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 200
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->k:Lcom/google/android/apps/gmm/shared/net/r;

    .line 201
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->l:Lcom/google/android/apps/gmm/shared/c/f;

    .line 202
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 203
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 204
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

    .line 206
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->g:Ljava/util/concurrent/CountDownLatch;

    .line 207
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    .line 209
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/c/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;)V

    const-string v2, "ResourceManager"

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 418
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/c/a;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/util/c/b;

    move-result-object v0

    .line 419
    iget-object v1, v0, Lcom/google/android/apps/gmm/util/c/b;->c:[B

    const/4 v2, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/c/b;->c:[B

    array-length v0, v0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/google/android/apps/gmm/util/c/c; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 422
    :goto_0
    return-object v0

    .line 420
    :catch_0
    move-exception v0

    .line 421
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DecodingException in dataGetImageInternal:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 4

    .prologue
    .line 792
    const/4 v1, 0x0

    .line 794
    if-eqz p2, :cond_1

    .line 795
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v2

    .line 796
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 797
    if-eqz v0, :cond_4

    .line 798
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 800
    :goto_0
    if-nez v0, :cond_0

    .line 801
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;-><init>()V

    .line 805
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->c:Z

    .line 806
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v3}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 808
    :cond_0
    monitor-exit v2

    .line 826
    :goto_1
    return-object v0

    .line 808
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 810
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 811
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 812
    if-nez v0, :cond_2

    .line 814
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

    if-eqz v2, :cond_2

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->f:Lcom/google/android/apps/gmm/map/internal/d/b/q;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/q;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 818
    :cond_2
    if-nez v0, :cond_3

    .line 820
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;-><init>()V

    .line 821
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->c:Z

    .line 823
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824
    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method static b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 437
    .line 438
    const-string v0, "//"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https:"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 441
    :cond_0
    :goto_0
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    :try_start_1
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452
    :try_start_2
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 453
    :try_start_3
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    .line 458
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 464
    :goto_1
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v1

    .line 466
    :goto_2
    return-object v0

    .line 438
    :cond_1
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 442
    :catch_0
    move-exception v0

    .line 443
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MalformedURLException in loadThumbnail:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 444
    goto :goto_2

    .line 461
    :catch_1
    move-exception v2

    .line 462
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x38

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "IOException closing InputStream in BitmapRetriever.load:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 454
    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 455
    :goto_3
    :try_start_5
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x24

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "IOException in BitmapRetriever.load:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 458
    if-eqz v2, :cond_2

    .line 459
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 464
    :cond_2
    :goto_4
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v1

    .line 465
    goto :goto_2

    .line 461
    :catch_3
    move-exception v0

    .line 462
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x38

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "IOException closing InputStream in BitmapRetriever.load:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 457
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 458
    :goto_5
    if-eqz v2, :cond_3

    .line 459
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 464
    :cond_3
    :goto_6
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0

    .line 461
    :catch_4
    move-exception v1

    .line 462
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x38

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "IOException closing InputStream in BitmapRetriever.load:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 457
    :catchall_1
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    move-object v2, v1

    goto :goto_5

    :catchall_2
    move-exception v1

    move-object v2, v3

    move-object v3, v0

    move-object v0, v1

    goto :goto_5

    :catchall_3
    move-exception v0

    goto :goto_5

    .line 454
    :catch_5
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    move-object v2, v1

    goto/16 :goto_3

    :catch_6
    move-exception v2

    move-object v6, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v6

    goto/16 :goto_3
.end method

.method private b([Ljava/lang/String;I[I[ILjava/lang/String;FILjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 15

    .prologue
    .line 665
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 668
    const/4 v5, 0x0

    .line 669
    const/4 v1, 0x0

    :goto_0
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 670
    const-string v3, "&name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    .line 671
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&highlight="

    .line 672
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, p3, v1

    .line 673
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&filter="

    .line 674
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, p4, v1

    .line 675
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 677
    aget-object v3, p1, v1

    if-eqz v3, :cond_0

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 678
    add-int/lit8 v5, v5, 0x1

    .line 669
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 681
    :cond_1
    const-string v1, "&scale="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&text="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&size="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 682
    move/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&color="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 684
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v3

    .line 685
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a:Ljava/util/ArrayList;

    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 686
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 687
    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    .line 728
    :goto_1
    return-object v1

    .line 691
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Z)V

    .line 695
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/c/c;

    move-object/from16 v0, p1

    array-length v6, v0

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->l:Lcom/google/android/apps/gmm/shared/c/f;

    move-object v2, p0

    move-object/from16 v4, p9

    move-object/from16 v8, p1

    move/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    invoke-direct/range {v1 .. v14}, Lcom/google/android/apps/gmm/map/internal/d/c/c;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;IILcom/google/android/apps/gmm/shared/c/f;[Ljava/lang/String;I[I[ILjava/lang/String;FI)V

    .line 705
    move-object/from16 v0, p1

    array-length v2, v0

    add-int/lit8 v2, v2, 0x1

    new-array v4, v2, [Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 706
    const/4 v2, 0x0

    aput-object v3, v4, v2

    .line 707
    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v2, v3, :cond_6

    .line 708
    aget-object v3, p1, v2

    .line 709
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 711
    monitor-enter v1

    .line 717
    :try_start_0
    aget-object v3, p1, v2

    move-object/from16 v0, p8

    invoke-virtual {p0, v3, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v3

    .line 718
    iget-object v5, v1, Lcom/google/android/apps/gmm/map/internal/d/c/g;->i:[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    aput-object v3, v5, v2

    .line 719
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 721
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 724
    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    .line 726
    :cond_4
    add-int/lit8 v5, v2, 0x1

    aput-object v3, v4, v5

    .line 707
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 719
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_6
    move-object v1, v4

    .line 728
    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    .line 751
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v1

    .line 752
    monitor-enter v1

    .line 770
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->c()Z

    move-result v2

    if-nez v2, :cond_4

    .line 771
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->l:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 772
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->f()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 773
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I

    move-result v6

    if-eq v6, v0, :cond_0

    .line 774
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I

    move-result v6

    if-ne v6, v7, :cond_6

    .line 775
    :cond_0
    :goto_0
    sget-wide v6, Lcom/google/android/apps/gmm/map/internal/d/c/a;->h:J

    cmp-long v6, v6, v4

    if-ltz v6, :cond_1

    .line 776
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_4

    sget-wide v6, Lcom/google/android/apps/gmm/map/internal/d/c/a;->i:J

    cmp-long v0, v6, v4

    if-gez v0, :cond_4

    .line 778
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Z)V

    .line 779
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(J)V

    .line 780
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/ah;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v2, 0x4

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, p1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/16 v2, 0x9

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, p2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    if-eqz p1, :cond_2

    if-nez p2, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/c/a;->j:Ljava/lang/String;

    const-string v2, "referer is null for url="

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->f:J

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/c/h;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/h;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->k:Lcom/google/android/apps/gmm/shared/net/r;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 784
    :cond_4
    if-eqz p3, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 785
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    :cond_5
    monitor-exit v1

    return-object v1

    .line 774
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 780
    :cond_7
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 788
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a([Ljava/lang/String;I[I[ILjava/lang/String;FILjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 2

    .prologue
    .line 651
    invoke-direct/range {p0 .. p9}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b([Ljava/lang/String;I[I[ILjava/lang/String;FILjava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 655
    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/c/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/d;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;Ljava/io/File;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 901
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/k;Z)V
    .locals 3

    .prologue
    .line 322
    if-eqz p3, :cond_0

    .line 323
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 326
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/c/e;

    invoke-direct {v1, p2}, Lcom/google/android/apps/gmm/map/internal/d/c/e;-><init>(Lcom/google/android/apps/gmm/util/webimageview/k;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 331
    if-eqz v0, :cond_2

    .line 333
    if-eqz p3, :cond_1

    .line 334
    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/util/webimageview/k;->a(Landroid/graphics/Bitmap;)V

    .line 346
    :goto_0
    return-void

    .line 336
    :cond_1
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/c/e;->a:Landroid/graphics/Bitmap;

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0

    .line 343
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/f;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/f;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/e;)V

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 496
    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 499
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    invoke-interface {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/a/a;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 500
    if-eqz v1, :cond_0

    .line 501
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->l:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(J)V

    .line 502
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Landroid/graphics/Bitmap;)V

    .line 503
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(I)V

    .line 518
    :goto_0
    return-object v0

    .line 508
    :cond_0
    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 510
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/t;->a()Ljava/lang/String;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_1

    .line 512
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object p1, v0

    .line 518
    :cond_1
    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    goto :goto_0

    .line 512
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
