.class public final enum Lcom/google/android/apps/gmm/navigation/i/a/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/i/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/i/a/b;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/i/a/b;

.field public static final enum c:Lcom/google/android/apps/gmm/navigation/i/a/b;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/navigation/i/a/b;


# instance fields
.field public d:Lcom/google/b/f/b/a/bq;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/a/b;

    const-string v1, "OKAY"

    sget-object v2, Lcom/google/b/f/b/a/bq;->a:Lcom/google/b/f/b/a/bq;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/navigation/i/a/b;-><init>(Ljava/lang/String;ILcom/google/b/f/b/a/bq;)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/a/b;->a:Lcom/google/android/apps/gmm/navigation/i/a/b;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/a/b;

    const-string v1, "NO_ENDPOINTS_FOUND"

    sget-object v2, Lcom/google/b/f/b/a/bq;->b:Lcom/google/b/f/b/a/bq;

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/navigation/i/a/b;-><init>(Ljava/lang/String;ILcom/google/b/f/b/a/bq;)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/a/b;->b:Lcom/google/android/apps/gmm/navigation/i/a/b;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/a/b;

    const-string v1, "NO_PATH_FOUND"

    sget-object v2, Lcom/google/b/f/b/a/bq;->c:Lcom/google/b/f/b/a/bq;

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/navigation/i/a/b;-><init>(Ljava/lang/String;ILcom/google/b/f/b/a/bq;)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/a/b;->c:Lcom/google/android/apps/gmm/navigation/i/a/b;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/i/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/a/b;->a:Lcom/google/android/apps/gmm/navigation/i/a/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/a/b;->b:Lcom/google/android/apps/gmm/navigation/i/a/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/a/b;->c:Lcom/google/android/apps/gmm/navigation/i/a/b;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/a/b;->e:[Lcom/google/android/apps/gmm/navigation/i/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/b/f/b/a/bq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/f/b/a/bq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/a/b;->d:Lcom/google/b/f/b/a/bq;

    .line 40
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/i/a/b;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/gmm/navigation/i/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/a/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/i/a/b;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/a/b;->e:[Lcom/google/android/apps/gmm/navigation/i/a/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/i/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/i/a/b;

    return-object v0
.end method
