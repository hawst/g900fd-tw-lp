.class public Lcom/google/android/apps/gmm/car/ax;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/graphics/Point;

.field public final b:Lcom/google/android/apps/gmm/directions/f/a/c;

.field public final c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-static {p2, p3, v0}, Lcom/google/android/apps/gmm/car/aw;->a(Lcom/google/android/apps/gmm/car/v;II)Lcom/google/android/apps/gmm/directions/f/a/c;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/directions/f/a/c;)V

    .line 58
    return-void
.end method

.method private constructor <init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/directions/f/a/c;)V
    .locals 6

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/ax;->a:Landroid/graphics/Point;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/ax;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 46
    new-instance v0, Landroid/graphics/Rect;

    .line 47
    iget v1, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->a:I

    .line 48
    iget v2, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->c:I

    iget v3, p1, Landroid/graphics/Point;->x:I

    .line 49
    iget v4, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->b:I

    sub-int/2addr v3, v4

    iget v4, p1, Landroid/graphics/Point;->y:I

    .line 50
    iget v5, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->d:I

    sub-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    .line 51
    return-void
.end method
