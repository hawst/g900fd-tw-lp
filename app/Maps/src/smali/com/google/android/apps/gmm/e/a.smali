.class public Lcom/google/android/apps/gmm/e/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field public c:Lcom/google/android/apps/gmm/e/b;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/apps/gmm/util/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/android/apps/gmm/e/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/e/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/util/k;)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/e/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/e/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    .line 88
    iput-object p1, p0, Lcom/google/android/apps/gmm/e/a;->d:Landroid/content/Context;

    .line 89
    iput-object p2, p0, Lcom/google/android/apps/gmm/e/a;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 90
    iput-object p3, p0, Lcom/google/android/apps/gmm/e/a;->e:Lcom/google/android/apps/gmm/util/k;

    .line 93
    invoke-interface {p2, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 4

    .prologue
    .line 82
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/util/k;

    .line 83
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/util/k;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/e/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/util/k;)V

    .line 84
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/e/a;->e:Lcom/google/android/apps/gmm/util/k;

    const-string v1, "com.google.android.googlequicksearchbox"

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/k;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    sget-object v0, Lcom/google/android/apps/gmm/e/a;->a:Ljava/lang/String;

    .line 157
    :goto_0
    return-void

    .line 143
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.googlequicksearchbox"

    .line 144
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fg"

    iget-object v2, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/e/b;->a:Z

    .line 145
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "nav"

    iget-object v2, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/e/b;->b:Z

    .line 146
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/e/b;->c:Lcom/google/maps/g/a/hm;

    if-eqz v1, :cond_1

    .line 148
    const-string v1, "mode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/e/b;->c:Lcom/google/maps/g/a/hm;

    .line 149
    invoke-static {v2}, Lcom/google/android/apps/gmm/l/a/a;->a(Lcom/google/maps/g/a/hm;)Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/e/a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "GMM->GSA intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/gmm/e/a;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    .line 120
    const/4 v0, 0x0

    .line 121
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v3, :cond_0

    move v3, v1

    :goto_0
    if-eqz v3, :cond_4

    .line 123
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v3, v2

    .line 121
    goto :goto_0

    .line 123
    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 124
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v2, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 125
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    .line 130
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/e/b;->b:Z

    if-ne v2, v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/e/b;->c:Lcom/google/maps/g/a/hm;

    if-eq v2, v0, :cond_3

    .line 131
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/e/b;->b:Z

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/gmm/e/a;->c:Lcom/google/android/apps/gmm/e/b;

    iput-object v0, v1, Lcom/google/android/apps/gmm/e/b;->c:Lcom/google/maps/g/a/hm;

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/e/a;->a()V

    .line 135
    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto :goto_1
.end method
