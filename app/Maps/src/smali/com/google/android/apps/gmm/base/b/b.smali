.class public Lcom/google/android/apps/gmm/base/b/b;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/b/j;
.implements Lcom/google/android/apps/gmm/base/views/listview/d;


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/bk",
            "<*>;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/database/DataSetObserver;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z

.field private final f:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/b/b;-><init>(Z)V

    .line 57
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/base/b/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/b/c;-><init>(Lcom/google/android/apps/gmm/base/b/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->c:Landroid/database/DataSetObserver;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->a:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->b:Ljava/util/Set;

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    .line 66
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/b/b;->e:Z

    .line 67
    return-void

    .line 53
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private a(I[I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    aput v2, p2, v2

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 140
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge p1, v4, :cond_0

    .line 141
    aput p1, p2, v1

    move v0, v1

    .line 147
    :goto_1
    return v0

    .line 144
    :cond_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    sub-int/2addr p1, v0

    .line 145
    aget v0, p2, v2

    add-int/lit8 v0, v0, 0x1

    aput v0, p2, v2

    goto :goto_0

    :cond_1
    move v0, v2

    .line 147
    goto :goto_1
.end method


# virtual methods
.method public final a(II)I
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 156
    instance-of v2, v0, Lcom/google/android/apps/gmm/base/b/j;

    if-eqz v2, :cond_0

    .line 157
    check-cast v0, Lcom/google/android/apps/gmm/base/b/j;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/base/b/j;->a(II)I

    move-result v0

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/bk",
            "<*>;",
            "Landroid/widget/ListAdapter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->c:Landroid/database/DataSetObserver;

    invoke-interface {p2, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v2, v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 303
    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/listview/d;

    if-eqz v2, :cond_0

    .line 304
    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/views/listview/d;->a(I)Z

    move-result v0

    .line 307
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 284
    invoke-interface {v0}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    const/4 v0, 0x0

    .line 288
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(I)Lcom/google/android/libraries/curvular/bk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/libraries/curvular/bk",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bk;

    .line 314
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/bk",
            "<*>;",
            "Landroid/widget/ListAdapter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 91
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 92
    :goto_0
    instance-of v2, v0, Lcom/google/android/apps/gmm/base/b/k;

    if-eqz v2, :cond_1

    .line 94
    check-cast v0, Lcom/google/android/apps/gmm/base/b/k;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/base/b/k;->a(Landroid/widget/ListAdapter;)V

    .line 104
    :goto_1
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/b/b;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->c:Landroid/database/DataSetObserver;

    invoke-interface {p2, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 167
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 168
    goto :goto_0

    .line 169
    :cond_0
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 185
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 190
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v3, v3, v1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 197
    :goto_0
    if-ne v0, v2, :cond_1

    move v0, v2

    .line 214
    :cond_0
    :goto_1
    return v0

    .line 201
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/b/b;->e:Z

    if-nez v3, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 207
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge p1, v4, :cond_2

    .line 208
    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 209
    if-ltz v0, :cond_0

    add-int/2addr v0, v1

    goto :goto_1

    .line 211
    :cond_2
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v4

    add-int/2addr v1, v4

    .line 212
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    sub-int/2addr p1, v0

    .line 213
    goto :goto_2

    :cond_3
    move v0, v2

    .line 214
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    if-eqz p2, :cond_0

    .line 222
    sget v0, Lcom/google/android/apps/gmm/g;->eA:I

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 223
    if-eqz v0, :cond_0

    move-object p2, v1

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v1, v1, v3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v1, v1, v6

    .line 228
    invoke-interface {v0, v1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v0, v0, v3

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v3, v3, v6

    if-nez v3, :cond_1

    new-instance v3, Lcom/google/android/apps/gmm/base/b/d;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/gmm/base/b/d;-><init>(Lcom/google/android/apps/gmm/base/b/b;ILandroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    sget v0, Lcom/google/android/apps/gmm/g;->eA:I

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 236
    :cond_2
    return-object v1
.end method

.method public getViewTypeCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 264
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/b/b;->e:Z

    if-eqz v1, :cond_1

    .line 265
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    :goto_0
    if-lez v0, :cond_2

    :goto_1
    return v0

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    goto :goto_0

    .line 268
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 269
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 270
    goto :goto_2

    .line 273
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/gmm/base/b/b;->a(I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    aget v0, v2, v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/b;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    .line 296
    :cond_0
    return v0
.end method
