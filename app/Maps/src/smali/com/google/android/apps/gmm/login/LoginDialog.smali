.class public final Lcom/google/android/apps/gmm/login/LoginDialog;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/login/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/login/d/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private c:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    new-instance v1, Lcom/google/android/apps/gmm/login/LoginDialog;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/login/LoginDialog;-><init>()V

    const-string v2, "loginDialog"

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, v1, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    .line 58
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/login/LoginDialog;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/login/LoginDialog;-><init>()V

    .line 71
    const-string v1, "loginDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    iput-object p1, v0, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    .line 74
    :cond_0
    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/b;->b()V

    .line 107
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/login/c/a;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/LoginDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    new-instance v3, Lcom/google/android/apps/gmm/login/l;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/login/l;-><init>(Lcom/google/android/apps/gmm/login/LoginDialog;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/login/c/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/login/a/b;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->b:Lcom/google/android/apps/gmm/login/d/b;

    .line 86
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method public final declared-synchronized onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/LoginDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 92
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 92
    :cond_0
    :try_start_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/login/b/b;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 93
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->c:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->c:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->b:Lcom/google/android/apps/gmm/login/d/b;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 95
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 96
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    monitor-exit p0

    return-object v0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/LoginDialog;->a:Lcom/google/android/apps/gmm/login/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/b;->b()V

    .line 115
    :cond_0
    return-void
.end method
