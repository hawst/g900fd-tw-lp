.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 263
    const-string v0, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 userData;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sStyleTexture;\nuniform vec3 uTextureInfo;\nuniform float uRoadAlpha;\nvarying float vRoadAlpha;\nuniform vec3 uStrokeInfo;\nuniform float reciprocal2xBitmapWidth;\nuniform float minimumTextureMinificationScale;\nvoid main() {\n  float texHeight = uTextureInfo[0];\n  vec2 textureScale = vec2(uTextureInfo[1], uTextureInfo[2]);\n  vec2 texCoord;\n  float styleIndex = (256.0 * userData.x) + userData.y;\n  float strokeIndex = userData.z;\n  if (strokeIndex < 0.5 && uRoadAlpha < 1.0)\n    vRoadAlpha = 0.0;\n  else\n    vRoadAlpha = uRoadAlpha;\n  float widthVecIndex = strokeIndex;\n  if (strokeIndex > 2.5)\n    widthVecIndex = strokeIndex - 3.0;\n  float widthPixel = 4.0;\n  if (strokeIndex > 2.5)\n    widthPixel = 5.0;\n  float colorTexX = (1.0 + 2.0 * strokeIndex) * reciprocal2xBitmapWidth;\n  float widthTexX = (1.0 + 2.0 * widthPixel) * reciprocal2xBitmapWidth;\n  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0.0, 1.0);\n  float texY = (styleIndex + 0.5) / texHeight;\n  vec4 roadWidthVec = texture2D(sStyleTexture, vec2(widthTexX, texY));\n  float roadScale;\n  if (widthVecIndex < 0.5)\n    roadScale = max(minimumTextureMinificationScale, roadWidthVec[0]);\n  else if (widthVecIndex < 1.5)\n    roadScale = max(minimumTextureMinificationScale, roadWidthVec[1]);\n  else if (widthVecIndex < 2.5)\n    roadScale = max(minimumTextureMinificationScale, roadWidthVec[2]);\n  else\n    roadScale = 0.001;\n  texCoord = aTextureCoord;\n  texCoord -= vec2(0.5);\n  textureScale.x /= roadScale;\n  vTextureCoord = (texCoord * 0.5 * textureScale) + vec2(0.5);\n  vColor = texture2D(sStyleTexture, vec2(colorTexX, texY));\n}\n"

    const-string v1, "precision lowp float;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nvarying float vRoadAlpha;\nuniform float brightnessScale;\nvoid main() {\n  float t = texture2D(sTexture0, vTextureCoord, -0.25).a;  t *= vRoadAlpha * vColor.a;\n  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 272
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 273
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->a:I

    .line 274
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->a:I

    if-ne v0, v2, :cond_0

    .line 276
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->a:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 284
    const-string v0, "sStyleTexture"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->b:I

    .line 289
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->b:I

    if-ne v0, v2, :cond_1

    .line 291
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sStyleTexture handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->b:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 297
    const-string v0, "uTextureInfo"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->c:I

    .line 302
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->c:I

    if-ne v0, v2, :cond_2

    .line 304
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uTextureInfo handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_2
    const-string v0, "uRoadAlpha"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->d:I

    .line 308
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->d:I

    if-ne v0, v2, :cond_3

    .line 310
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uRoadAlpha handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :cond_3
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->e:I

    .line 314
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->e:I

    if-ne v0, v2, :cond_4

    .line 316
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get brightnessScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_4
    const-string v0, "reciprocal2xBitmapWidth"

    .line 320
    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->f:I

    .line 321
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->f:I

    if-ne v0, v2, :cond_5

    .line 323
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get reciprocal2xBitmapWidth handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_5
    const-string v0, "minimumTextureMinificationScale"

    .line 327
    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->g:I

    .line 328
    const-string v0, "DashedRoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;->g:I

    if-ne v0, v2, :cond_6

    .line 330
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get minimumTextureMinificationScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_6
    return-void
.end method

.method protected final b(I)V
    .locals 2

    .prologue
    .line 336
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->b(I)V

    .line 337
    const/4 v0, 0x5

    const-string v1, "userData"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 338
    return-void
.end method
