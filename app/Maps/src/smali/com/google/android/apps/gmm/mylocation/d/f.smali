.class public abstract Lcom/google/android/apps/gmm/mylocation/d/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/d/c;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/mylocation/d/e;

.field public final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field public c:Z

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/mylocation/d/e;ZZ)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/d/f;->c:Z

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/d/f;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 70
    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/d/f;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    .line 71
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/mylocation/d/f;->c:Z

    .line 72
    invoke-virtual {p0, p2, p5}, Lcom/google/android/apps/gmm/mylocation/d/f;->a(Landroid/content/res/Resources;Z)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/f;->d:Ljava/util/List;

    .line 73
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;ZZ)V
    .locals 6

    .prologue
    .line 62
    new-instance v3, Lcom/google/android/apps/gmm/mylocation/d/e;

    invoke-direct {v3, p2, p3, p4}, Lcom/google/android/apps/gmm/mylocation/d/e;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/mylocation/d/f;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/mylocation/d/e;ZZ)V

    .line 64
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/mylocation/d/d;Lcom/google/android/apps/gmm/mylocation/g/a;)F
    .locals 2

    .prologue
    .line 88
    if-nez p0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 92
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 95
    iget v1, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/content/res/Resources;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/b;",
            ">;"
        }
    .end annotation
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/gmm/map/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
