.class public Lcom/google/android/apps/gmm/prefetchcache/y;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/y;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;)I
    .locals 4

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 70
    invoke-static {p0}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 72
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_0

    .line 73
    add-int/lit8 v0, v1, 0x14

    move v1, v0

    goto :goto_0

    .line 75
    :cond_0
    add-int/lit8 v0, v1, 0x28

    move v1, v0

    .line 77
    goto :goto_0

    .line 78
    :cond_1
    div-int/lit16 v0, v1, 0x400

    .line 79
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public static a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 8

    .prologue
    .line 88
    .line 89
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/al;->d()Lcom/google/r/b/a/a/al;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/al;

    .line 90
    iget-object v1, v0, Lcom/google/r/b/a/a/al;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hw;->d()Lcom/google/r/b/a/hw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/hw;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/r/b/a/hw;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    .line 92
    iget-object v0, v0, Lcom/google/r/b/a/a/al;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hw;->d()Lcom/google/r/b/a/hw;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hw;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/r/b/a/hw;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 94
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/s;

    move-result-object v2

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/s;->a()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/map/internal/c/cv;
    .locals 3

    .prologue
    .line 164
    .line 165
    invoke-static {p0}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/r;)Lcom/google/android/apps/gmm/map/b/a/bc;

    move-result-object v0

    .line 166
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/cw;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/y;->a:Ljava/lang/String;

    .line 169
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/r;",
            "Lcom/google/android/apps/gmm/map/internal/c/cv;",
            "Lcom/google/android/apps/gmm/map/internal/c/cd;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 121
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 123
    sget v0, Lcom/google/android/apps/gmm/prefetch/a/a;->c:I

    if-ltz v0, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    array-length v2, v2

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 125
    :goto_0
    if-ltz v0, :cond_5

    .line 127
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/r;)Lcom/google/android/apps/gmm/map/b/a/bc;

    move-result-object v2

    .line 129
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 130
    sget v5, Lcom/google/android/apps/gmm/prefetch/a/a;->c:I

    const/4 v6, 0x0

    invoke-static {v2, v5, p2, v4, v6}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V

    .line 132
    invoke-interface {v3, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move v2, v0

    .line 134
    :goto_1
    const/4 v0, 0x4

    if-lt v2, v0, :cond_5

    if-eq v2, v1, :cond_5

    .line 136
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 137
    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 123
    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    aget v0, v2, v0

    goto :goto_0

    .line 135
    :cond_2
    if-ltz v2, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    array-length v0, v0

    if-lt v2, v0, :cond_4

    :cond_3
    move v0, v1

    :goto_3
    move v2, v0

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    aget v0, v0, v2

    goto :goto_3

    .line 141
    :cond_5
    return-object v3
.end method

.method static a(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/r/b/a/a/af;",
            "Lcom/google/android/apps/gmm/map/internal/c/cv;",
            "Lcom/google/android/apps/gmm/map/internal/c/cd;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {p0}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/internal/c/cv;Lcom/google/android/apps/gmm/map/internal/c/cd;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/android/apps/gmm/map/b/a/r;)Z
    .locals 8

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/r;->b()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 178
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/r;)Lcom/google/android/apps/gmm/map/b/a/bc;

    move-result-object v2

    .line 177
    sget v3, Lcom/google/android/apps/gmm/prefetch/a/a;->c:I

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b(I)I

    move-result v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/b/a/bc;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v4, v5, v4

    int-to-double v4, v4

    int-to-double v6, v3

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int v2, v5, v2

    int-to-double v6, v2

    int-to-double v2, v3

    div-double v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v2, v4

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v4

    sget-wide v6, Lcom/google/android/apps/gmm/prefetch/a/a;->a:D

    mul-double/2addr v0, v6

    cmpl-double v0, v4, v0

    if-gez v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/prefetch/a/a;->b:I

    if-le v2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/google/r/b/a/a/af;Lcom/google/r/b/a/a/af;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 151
    if-ne p0, p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v0

    .line 153
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 154
    goto :goto_0

    .line 156
    :cond_3
    iget-wide v2, p0, Lcom/google/r/b/a/a/af;->f:J

    iget-wide v4, p1, Lcom/google/r/b/a/a/af;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
