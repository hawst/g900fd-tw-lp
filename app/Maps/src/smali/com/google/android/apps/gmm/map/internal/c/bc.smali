.class public Lcom/google/android/apps/gmm/map/internal/c/bc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/l/p;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/l/a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field private final d:J

.field private final e:I

.field private final f:I

.field private final g:Lcom/google/android/apps/gmm/map/internal/c/bk;

.field private final h:I

.field private final i:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final j:[I


# direct methods
.method private constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/bk;II[I)V
    .locals 3
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JII",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/l/p;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/l/a;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/c/bk;",
            "II[I)V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->b:Ljava/util/List;

    .line 111
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->d:J

    .line 112
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->e:I

    .line 113
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->f:I

    .line 114
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->i:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 115
    const-string v0, "styleInfo"

    if-nez p8, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p8, Lcom/google/android/apps/gmm/map/internal/c/bk;

    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->g:Lcom/google/android/apps/gmm/map/internal/c/bk;

    .line 116
    iput p9, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->h:I

    .line 117
    iput p10, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->c:I

    .line 118
    iput-object p11, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->j:[I

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v0, p6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->b:Ljava/util/List;

    invoke-interface {v0, p7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 121
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/de;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/bc;
    .locals 19

    .prologue
    .line 228
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/de;->a()Lcom/google/maps/b/a/ci;

    move-result-object v8

    .line 230
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 231
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 233
    new-instance v11, Lcom/google/android/apps/gmm/v/cn;

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    invoke-direct {v11, v2, v3, v4}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    .line 234
    new-instance v12, Lcom/google/android/apps/gmm/v/cn;

    const v2, -0x800001

    const v3, -0x800001

    const v4, -0x800001

    invoke-direct {v12, v2, v3, v4}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    .line 236
    invoke-virtual {v8}, Lcom/google/maps/b/a/ci;->a()Lcom/google/maps/b/a/cu;

    move-result-object v4

    .line 238
    iget-object v2, v4, Lcom/google/maps/b/a/cu;->a:[B

    iget v3, v4, Lcom/google/maps/b/a/cu;->b:I

    iget v4, v4, Lcom/google/maps/b/a/cu;->c:I

    .line 239
    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->c:I

    const/4 v6, 0x3

    iget-object v7, v8, Lcom/google/maps/b/a/ci;->a:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    mul-int/lit8 v7, v7, 0x3

    .line 237
    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/b/a/o;->a([BIIIII)[I

    move-result-object v7

    .line 241
    const/4 v2, 0x0

    :goto_0
    array-length v3, v7

    if-ge v2, v3, :cond_0

    aget v3, v7, v2

    int-to-float v3, v3

    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v2, 0x1

    aget v5, v7, v5

    rsub-int v5, v5, 0x1000

    aput v5, v7, v4

    add-int/lit8 v4, v2, 0x1

    aget v4, v7, v4

    int-to-float v4, v4

    add-int/lit8 v5, v2, 0x2

    aget v5, v7, v5

    int-to-float v5, v5

    invoke-virtual {v11, v3, v4, v5}, Lcom/google/android/apps/gmm/v/cn;->a(FFF)Lcom/google/android/apps/gmm/v/cn;

    invoke-virtual {v12, v3, v4, v5}, Lcom/google/android/apps/gmm/v/cn;->b(FFF)Lcom/google/android/apps/gmm/v/cn;

    add-int/lit8 v2, v2, 0x3

    goto :goto_0

    .line 243
    :cond_0
    iget-object v2, v8, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    iget v2, v2, Lcom/google/maps/b/a/cy;->b:I

    add-int/lit8 v13, v2, 0x1

    .line 244
    new-array v14, v13, [[I

    .line 245
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v5, v3

    move v3, v4

    move v4, v2

    :goto_1
    if-ge v4, v13, :cond_3

    add-int/lit8 v2, v13, -0x1

    if-ge v4, v2, :cond_1

    iget-object v2, v8, Lcom/google/maps/b/a/ci;->c:Lcom/google/maps/b/a/cy;

    iget-object v2, v2, Lcom/google/maps/b/a/cy;->a:[I

    aget v2, v2, v4

    :goto_2
    sub-int v15, v2, v5

    new-array v6, v15, [I

    aput-object v6, v14, v4

    add-int/lit8 v6, v15, -0x2

    add-int/2addr v6, v3

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v15, :cond_2

    aget-object v16, v14, v4

    add-int v17, v5, v3

    iget-object v0, v8, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/maps/b/a/cy;->a:[I

    move-object/from16 v18, v0

    aget v17, v18, v17

    aput v17, v16, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_1
    iget-object v2, v8, Lcom/google/maps/b/a/ci;->b:Lcom/google/maps/b/a/cy;

    iget v2, v2, Lcom/google/maps/b/a/cy;->b:I

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v5, v2

    move v3, v6

    goto :goto_1

    .line 247
    :cond_3
    mul-int/lit8 v2, v3, 0x9

    new-array v6, v2, [F

    .line 248
    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_4
    array-length v4, v14

    if-ge v2, v4, :cond_5

    const/4 v4, 0x0

    :goto_5
    aget-object v5, v14, v2

    array-length v5, v5

    add-int/lit8 v5, v5, -0x2

    if-ge v4, v5, :cond_4

    rem-int/lit8 v5, v4, 0x2

    aget-object v13, v14, v2

    mul-int/lit8 v15, v5, 0x2

    add-int/2addr v15, v4

    aget v13, v13, v15

    aget-object v15, v14, v2

    add-int/lit8 v16, v4, 0x1

    aget v15, v15, v16

    aget-object v16, v14, v2

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x2

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v4

    aget v16, v16, v5

    add-int/lit8 v5, v3, 0x1

    mul-int/lit8 v17, v13, 0x3

    aget v17, v7, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    aput v17, v6, v3

    add-int/lit8 v3, v5, 0x1

    mul-int/lit8 v17, v13, 0x3

    add-int/lit8 v17, v17, 0x1

    aget v17, v7, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    aput v17, v6, v5

    add-int/lit8 v5, v3, 0x1

    mul-int/lit8 v13, v13, 0x3

    add-int/lit8 v13, v13, 0x2

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v3

    add-int/lit8 v3, v5, 0x1

    mul-int/lit8 v13, v15, 0x3

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v5

    add-int/lit8 v5, v3, 0x1

    mul-int/lit8 v13, v15, 0x3

    add-int/lit8 v13, v13, 0x1

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v3

    add-int/lit8 v3, v5, 0x1

    mul-int/lit8 v13, v15, 0x3

    add-int/lit8 v13, v13, 0x2

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v5

    add-int/lit8 v5, v3, 0x1

    mul-int/lit8 v13, v16, 0x3

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v3

    add-int/lit8 v3, v5, 0x1

    mul-int/lit8 v13, v16, 0x3

    add-int/lit8 v13, v13, 0x1

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v5

    add-int/lit8 v5, v3, 0x1

    mul-int/lit8 v13, v16, 0x3

    add-int/lit8 v13, v13, 0x2

    aget v13, v7, v13

    int-to-float v13, v13

    aput v13, v6, v3

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto/16 :goto_5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    .line 249
    :cond_5
    new-instance v2, Lcom/google/android/apps/gmm/map/l/p;

    invoke-direct {v2, v6, v11, v12}, Lcom/google/android/apps/gmm/map/l/p;-><init>([FLcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    iget-object v2, v8, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    iget v2, v2, Lcom/google/maps/b/a/cy;->b:I

    new-array v3, v2, [I

    .line 252
    const/4 v2, 0x0

    :goto_6
    array-length v4, v3

    if-ge v2, v4, :cond_6

    .line 253
    iget-object v4, v8, Lcom/google/maps/b/a/ci;->d:Lcom/google/maps/b/a/cy;

    iget-object v4, v4, Lcom/google/maps/b/a/cy;->a:[I

    aget v4, v4, v2

    aput v4, v3, v2

    .line 252
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 255
    :cond_6
    new-instance v2, Lcom/google/android/apps/gmm/map/l/a;

    invoke-direct {v2, v6, v3, v11, v12}, Lcom/google/android/apps/gmm/map/l/a;-><init>([F[ILcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v3, -0x80000000

    xor-int v7, v2, v3

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    new-instance v11, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v11, v3, v2}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 262
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bc;

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    iget-wide v4, v2, Lcom/google/maps/b/a/db;->b:J

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    iget v6, v2, Lcom/google/maps/b/a/cz;->b:I

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v2, 0x0

    new-array v14, v2, [I

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/gmm/map/internal/c/bc;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/bk;II[I)V

    return-object v3
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/ay;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 172
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 173
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 175
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    invoke-static {p0, v9, v10}, Lcom/google/android/apps/gmm/map/l/p;->a(Ljava/io/DataInput;Ljava/util/List;Ljava/util/List;)Z

    move-result v4

    .line 179
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/bk;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-result-object v11

    .line 182
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v12

    .line 185
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v13

    .line 188
    const/4 v8, 0x0

    .line 189
    and-int/lit8 v2, v13, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_2

    .line 190
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    .line 196
    :cond_0
    :goto_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 197
    new-array v14, v5, [I

    .line 198
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_4

    .line 199
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    aput v6, v14, v2

    .line 198
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 189
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 191
    :cond_2
    and-int/lit8 v2, v13, 0x2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_0

    .line 192
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->e()Lcom/google/android/apps/gmm/map/b/a/k;

    move-result-object v8

    goto :goto_1

    .line 191
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 205
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v4, :cond_6

    .line 206
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bc;

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v4, v2

    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 207
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/gmm/map/internal/c/bc;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/bk;II[I)V

    .line 206
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_5
    :goto_4
    return-void

    .line 209
    :cond_6
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 214
    const-string v2, "StripifiedMesh"

    .line 215
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Stripified Mesh has a non-empty resource string for renderOp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 214
    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 391
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->d:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/bc;->g()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 423
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->e:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 428
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->f:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->i:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->g:Lcom/google/android/apps/gmm/map/internal/c/bk;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 366
    const/16 v0, 0xc

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->h:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->j:[I

    return-object v0
.end method

.method public final k()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 401
    move v1, v2

    move v3, v2

    .line 402
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/l/p;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/l/p;->c:[F

    array-length v4, v4

    shl-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x28

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    if-nez v5, :cond_0

    move v0, v2

    :goto_1
    shl-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 402
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 403
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/p;->d:[F

    array-length v0, v0

    goto :goto_1

    .line 405
    :cond_1
    add-int/lit8 v1, v3, 0x44

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->i:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 406
    if-nez v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v1, v0

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bc;->g:Lcom/google/android/apps/gmm/map/internal/c/bk;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v1

    .line 408
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/bc;->g()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_4
    add-int/2addr v0, v2

    .line 409
    return v0

    .line 406
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->d()I

    move-result v0

    goto :goto_2

    .line 407
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0x2

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x28

    goto :goto_3

    .line 408
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v2

    goto :goto_4
.end method
