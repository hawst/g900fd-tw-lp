.class final enum Lcom/google/android/apps/gmm/navigation/navui/views/e;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/navui/views/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum c:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum d:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum e:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum f:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum g:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum h:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum i:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum j:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field public static final enum k:Lcom/google/android/apps/gmm/navigation/navui/views/e;

.field private static final synthetic n:[Lcom/google/android/apps/gmm/navigation/navui/views/e;


# instance fields
.field public final l:I

.field public final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 103
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "STRAIGHT"

    sget v2, Lcom/google/android/apps/gmm/f;->aL:I

    sget v3, Lcom/google/android/apps/gmm/k;->j:I

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->a:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "STRAIGHT_TALL"

    sget v2, Lcom/google/android/apps/gmm/f;->aM:I

    sget v3, Lcom/google/android/apps/gmm/k;->k:I

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->b:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "SLIGHT"

    sget v2, Lcom/google/android/apps/gmm/f;->aJ:I

    sget v3, Lcom/google/android/apps/gmm/k;->h:I

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->c:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "SLIGHT_TALL"

    sget v2, Lcom/google/android/apps/gmm/f;->aK:I

    sget v3, Lcom/google/android/apps/gmm/k;->i:I

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->d:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "NORMAL"

    sget v2, Lcom/google/android/apps/gmm/f;->aF:I

    sget v3, Lcom/google/android/apps/gmm/k;->d:I

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->e:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "NORMAL_SHORT"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/gmm/f;->aG:I

    sget v4, Lcom/google/android/apps/gmm/k;->e:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->f:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "SHARP"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/apps/gmm/f;->aH:I

    sget v4, Lcom/google/android/apps/gmm/k;->f:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->g:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "SHARP_SHORT"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/apps/gmm/f;->aI:I

    sget v4, Lcom/google/android/apps/gmm/k;->g:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->h:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "UTURN"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/apps/gmm/f;->aO:I

    sget v4, Lcom/google/android/apps/gmm/k;->m:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->i:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 113
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "UTURN_SHORT"

    const/16 v2, 0x9

    sget v3, Lcom/google/android/apps/gmm/f;->aP:I

    sget v4, Lcom/google/android/apps/gmm/k;->n:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->j:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    const-string v1, "STUB"

    const/16 v2, 0xa

    sget v3, Lcom/google/android/apps/gmm/f;->aN:I

    sget v4, Lcom/google/android/apps/gmm/k;->l:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/e;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->k:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    .line 101
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/navui/views/e;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/views/e;->a:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/views/e;->b:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/views/e;->c:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/views/e;->d:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/views/e;->e:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/views/e;->f:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/views/e;->g:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/views/e;->h:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/views/e;->i:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/views/e;->j:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/views/e;->k:Lcom/google/android/apps/gmm/navigation/navui/views/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->n:[Lcom/google/android/apps/gmm/navigation/navui/views/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput p3, p0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->l:I

    .line 122
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->m:I

    .line 123
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/navui/views/e;
    .locals 1

    .prologue
    .line 101
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/navui/views/e;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/views/e;->n:[Lcom/google/android/apps/gmm/navigation/navui/views/e;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/navui/views/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/navui/views/e;

    return-object v0
.end method
