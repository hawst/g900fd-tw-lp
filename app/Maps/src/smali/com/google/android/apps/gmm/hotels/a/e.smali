.class public Lcom/google/android/apps/gmm/hotels/a/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/hotels/a/i;

.field public final b:Lcom/google/android/apps/gmm/hotels/a/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/i;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    .line 57
    iput-object p2, p0, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p1}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    .line 52
    invoke-static {p2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/hotels/a/i;)Lcom/google/android/apps/gmm/hotels/a/e;
    .locals 3

    .prologue
    const/16 v2, 0xe

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/i;)I

    move-result v1

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 141
    if-le v1, v2, :cond_1

    .line 144
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 149
    :cond_0
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/e;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/hotels/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 145
    :cond_1
    if-nez v1, :cond_0

    .line 147
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/hotels/a/i;)Lcom/google/android/apps/gmm/hotels/a/e;
    .locals 3

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    .line 157
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/i;)I

    move-result v1

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 159
    const/16 v2, 0xe

    if-le v1, v2, :cond_1

    .line 161
    const/16 v0, -0xe

    .line 162
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 167
    :cond_0
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/e;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/hotels/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 163
    :cond_1
    if-nez v1, :cond_0

    .line 165
    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    const-string v0, "HotelDates{checkInDate=%s, checkOutDate=%s}"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
