.class public Lcom/google/android/apps/gmm/map/internal/c/ch;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/j;

.field final b:[Ljava/lang/Integer;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/b/a/j;[Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 45
    invoke-static {p2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->c:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 8

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_0

    .line 68
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 71
    const/16 v4, 0xc

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v4, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    array-length v0, v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/ch;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/ch;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-ne p0, p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    if-nez p1, :cond_3

    .line 109
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    array-length v2, v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 111
    :cond_3
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/ch;

    if-nez v2, :cond_4

    move v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_4
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/ch;

    .line 115
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_5
    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    .line 116
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v2, v1

    .line 115
    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 92
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_0
    return v0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string v0, "|"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 81
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 82
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
