.class public abstract Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;
.super Landroid/widget/ImageButton;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/gmm/base/l/a/c;",
        ">",
        "Landroid/widget/ImageButton;",
        "Lcom/google/android/libraries/curvular/cl",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Landroid/widget/PopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    sget v0, Lcom/google/android/apps/gmm/g;->cc:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setId(I)V

    .line 28
    sget v0, Lcom/google/android/apps/gmm/f;->cq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setImageResource(I)V

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setBackgroundResource(I)V

    .line 30
    sget v0, Lcom/google/android/apps/gmm/l;->af:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 32
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p1, p0}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->a:Landroid/widget/PopupMenu;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/base/views/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/g;-><init>(Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/apps/gmm/base/l/a/c;)Landroid/widget/PopupMenu$OnMenuItemClickListener;
    .param p1    # Lcom/google/android/apps/gmm/base/l/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/widget/PopupMenu$OnMenuItemClickListener;"
        }
    .end annotation
.end method

.method protected abstract a(Landroid/view/MenuItem;)V
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/widget/ImageButton;->onDetachedFromWindow()V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->a:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 46
    return-void
.end method

.method public final synthetic setViewModel(Lcom/google/android/libraries/curvular/ce;)V
    .locals 4
    .param p1    # Lcom/google/android/libraries/curvular/ce;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 20
    check-cast p1, Lcom/google/android/apps/gmm/base/l/a/c;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/l/a/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->a:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/l/a/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v3, v0, v3, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->a(Landroid/view/MenuItem;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->a:Landroid/widget/PopupMenu;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/BaseOverflowMenu;->a(Lcom/google/android/apps/gmm/base/l/a/c;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    goto :goto_0
.end method
