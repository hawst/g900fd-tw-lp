.class public Lcom/google/android/apps/gmm/suggest/g/c;
.super Lcom/google/android/apps/gmm/base/l/b;
.source "PG"


# static fields
.field private static final p:Ljava/lang/String;


# instance fields
.field public a:Landroid/widget/SearchView$OnQueryTextListener;

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:I

.field public l:Lcom/google/android/apps/gmm/z/b/l;

.field public m:I

.field public n:I

.field public o:Z

.field private final q:Lcom/google/android/apps/gmm/base/activities/c;

.field private final r:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/suggest/g/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/suggest/g/c;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/l/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->i:Z

    .line 43
    const v0, 0x2000003

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->n:I

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->o:Z

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/base/l/c;->b:Lcom/google/android/apps/gmm/base/l/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 52
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->j:Ljava/lang/String;

    .line 53
    sget v0, Lcom/google/android/apps/gmm/f;->cv:I

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->k:I

    .line 54
    sget v0, Lcom/google/android/apps/gmm/l;->iK:I

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->m:I

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/z/b/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/l;-><init>()V

    sget-object v0, Lcom/google/b/f/t;->bp:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->l:Lcom/google/android/apps/gmm/z/b/l;

    .line 57
    sget-object v0, Lcom/google/b/f/t;->k:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->r:Lcom/google/android/apps/gmm/z/b/l;

    .line 59
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->k:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 117
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string v1, ""

    .line 120
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/g/c;->a:Landroid/widget/SearchView$OnQueryTextListener;

    if-eqz v2, :cond_4

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/g/c;->a:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-interface {v2, v0}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    .line 123
    :cond_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    .line 127
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 128
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/f/bp;->a(Landroid/view/View;)V

    .line 130
    :cond_6
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->l:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 135
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 136
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/g/c;->a:Landroid/widget/SearchView$OnQueryTextListener;

    if-eqz v1, :cond_3

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/g/c;->a:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-interface {v1, v0}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 140
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 85
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    sget-object v0, Lcom/google/android/apps/gmm/suggest/g/c;->p:Ljava/lang/String;

    .line 93
    :goto_0
    return-object v1

    .line 88
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/suggest/g/c;->p:Ljava/lang/String;

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d()V

    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 172
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/suggest/g/c;->a(Ljava/lang/String;)V

    .line 173
    return-object v1
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/g/c;->i()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->n:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    iget v1, p0, Lcom/google/android/apps/gmm/suggest/g/c;->m:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 98
    sget v0, Lcom/google/android/apps/gmm/f;->en:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->q:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ix:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/c;->r:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
