.class public Lcom/google/android/apps/gmm/map/indoor/c/r;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/v/ad;

.field public b:Lcom/google/android/apps/gmm/v/g;

.field public final c:Lcom/google/android/apps/gmm/v/ck;

.field public final d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/v/r;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field private final g:F

.field private final h:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(FLcom/google/android/apps/gmm/v/ad;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/v/ck;

    new-instance v1, Lcom/google/android/apps/gmm/v/a/e;

    new-instance v2, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v2, v4, v4, p1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    new-instance v3, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v3, v4, v4, p1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/v/a/e;-><init>(Lcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V

    invoke-direct {v0, v1, p2}, Lcom/google/android/apps/gmm/v/ck;-><init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/indoor/c/r;-><init>(FLcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ck;)V

    .line 66
    return-void
.end method

.method private constructor <init>(FLcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ck;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->f:Z

    .line 71
    iput p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->g:F

    .line 72
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->a:Lcom/google/android/apps/gmm/v/ad;

    .line 73
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->c:Lcom/google/android/apps/gmm/v/ck;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->e:Ljava/util/Collection;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->h:Ljava/util/Collection;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->d:Ljava/util/Collection;

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/ar;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->c:Lcom/google/android/apps/gmm/v/ck;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/v/ck;->a(Lcom/google/android/apps/gmm/v/cl;)V

    .line 89
    iget v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->g:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->h:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 95
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    .line 163
    if-eqz p1, :cond_0

    .line 164
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 166
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 12

    .prologue
    const/16 v5, 0x1e01

    const/16 v9, 0x1e00

    const/4 v8, 0x1

    const/4 v3, 0x3

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/r;

    .line 124
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->f:Z

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/v/r;->a(F)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 128
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->f:Z

    if-eqz v0, :cond_4

    move v2, v3

    .line 131
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/v/bt;

    const/16 v1, 0x207

    move v4, v3

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/v/bt;-><init>(IIIIIII)V

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->d:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/t/ar;

    .line 142
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    goto :goto_3

    .line 145
    :cond_2
    new-instance v4, Lcom/google/android/apps/gmm/v/bt;

    const/16 v5, 0x202

    move v6, v8

    move v7, v8

    move v8, v3

    move v10, v9

    move v11, v9

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/gmm/v/bt;-><init>(IIIIIII)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/r;->h:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/ar;

    .line 155
    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    goto :goto_4

    .line 157
    :cond_3
    return-void

    :cond_4
    move v2, v8

    goto :goto_2
.end method
