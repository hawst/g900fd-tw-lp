.class public Lcom/google/android/apps/gmm/base/views/c/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    .line 18
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    .line 19
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    .line 20
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    .line 24
    iput p2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    .line 25
    iput p3, p0, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    if-ne p1, p0, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 32
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/views/c/a;

    if-eqz v2, :cond_3

    .line 33
    check-cast p1, Lcom/google/android/apps/gmm/base/views/c/a;

    .line 34
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 36
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
