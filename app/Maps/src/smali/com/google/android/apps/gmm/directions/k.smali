.class Lcom/google/android/apps/gmm/directions/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/c;


# instance fields
.field a:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V
    .locals 1

    .prologue
    .line 324
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->c:Landroid/content/res/Resources;

    .line 329
    new-instance v0, Lcom/google/android/apps/gmm/directions/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/l;-><init>(Lcom/google/android/apps/gmm/directions/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->a:Lcom/google/android/libraries/curvular/cg;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/hm;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->m:Lcom/google/maps/g/a/hm;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->n:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->o:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 353
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "previousDSPState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment$SavedState;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->setInitialSavedState(Landroid/app/Fragment$SavedState;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 354
    :cond_0
    :goto_0
    return-object v6

    .line 353
    :cond_1
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget v2, v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/r/a/f;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    iget-object v2, v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/f;->e:Lcom/google/r/b/a/afz;

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v7, v6

    move-object v8, v6

    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->b:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->r:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 359
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 363
    sget v0, Lcom/google/android/apps/gmm/l;->bR:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/k;->c:Landroid/content/res/Resources;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/k;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/k;->a:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 371
    sget v0, Lcom/google/android/apps/gmm/l;->af:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/k;->c:Landroid/content/res/Resources;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/k;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
