.class public Lcom/google/android/apps/gmm/reportmapissue/d/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/reportmapissue/c/d;


# instance fields
.field final a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

.field private final b:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

.field private final c:Lcom/google/android/apps/gmm/base/g/c;

.field private final d:Lcom/google/android/apps/gmm/base/activities/c;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/Runnable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/a/b;Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;Lcom/google/android/apps/gmm/base/g/c;IIIILjava/lang/Runnable;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->j:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->k:Z

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->b:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->c:Lcom/google/android/apps/gmm/base/g/c;

    .line 46
    iput p4, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->e:I

    .line 47
    iput p5, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->f:I

    .line 48
    iput p6, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->g:I

    .line 49
    iput p7, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->h:I

    .line 50
    iput-object p8, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->i:Ljava/lang/Runnable;

    .line 51
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 52
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->k:Z

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->j:Z

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iput-object p1, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 138
    return-void
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->j:Z

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->k:Z

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iput-object v3, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 98
    return-object v3

    :cond_1
    move v0, v1

    .line 89
    goto :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->k:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->g:I

    .line 73
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 68
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->f:I

    goto :goto_0

    .line 71
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->e:I

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->j:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->b:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->b:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 113
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 114
    const/4 v0, 0x0

    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->b:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->b:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueForResultFeaturePickerFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0

    .line 109
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->j:Z

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->h:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_1
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/b;->a:Ljava/lang/Boolean;

    return-object v0
.end method
