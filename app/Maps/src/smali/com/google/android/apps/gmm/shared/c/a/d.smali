.class public Lcom/google/android/apps/gmm/shared/c/a/d;
.super Ljava/lang/Thread;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/c/a/r;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/shared/c/a/p;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 156
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/gmm/shared/c/a/q;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/q;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lacks an @OnThread annotation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/a/q;->a()Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    const-string v2, "Thread cannot run @OnThread(CURRENT)"

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/shared/c/a/d;->setName(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 157
    const/4 v0, 0x4

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 158
    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V
    .locals 0
    .param p3    # Lcom/google/android/apps/gmm/shared/c/a/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 231
    invoke-direct {p0, p1, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 232
    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/c/a/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 233
    invoke-static {p0, p5, p4}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 234
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/shared/c/a/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 182
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V

    .line 183
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;I)V
    .locals 0
    .param p2    # Lcom/google/android/apps/gmm/shared/c/a/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 198
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 199
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/c/a/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 200
    invoke-static {p0, p4, p3}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 201
    return-void
.end method

.method public static a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 5
    .param p2    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 258
    const/4 v0, 0x5

    if-le p1, v0, :cond_0

    .line 259
    const-string v0, "GmmThread"

    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 260
    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x68

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Hey, don\'t create a thread ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") with a thread priority higher than the UI thread\'s priority of 5"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 265
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/Thread;->setPriority(I)V

    .line 267
    if-nez p2, :cond_1

    .line 268
    const-string v0, "GmmThread"

    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x6b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Got a null environment when starting thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Fancy exception handling will be unavailable for this thread."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    :cond_1
    invoke-static {p0, p2}, Lcom/google/android/apps/gmm/shared/c/a/e;->a(Ljava/lang/Thread;Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 272
    return-void
.end method

.method public static a(Ljava/lang/Thread;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 252
    const/4 v0, 0x4

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;ILcom/google/android/apps/gmm/map/c/a/a;)V

    .line 253
    return-void
.end method

.method static a(Ljava/lang/Throwable;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 281
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 282
    const-string v0, "GmmThread"

    invoke-static {v0, p0}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 288
    return-void
.end method


# virtual methods
.method public final P_()Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    return-object v0
.end method
