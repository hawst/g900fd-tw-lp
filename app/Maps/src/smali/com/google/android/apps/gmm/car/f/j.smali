.class Lcom/google/android/apps/gmm/car/f/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/ac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/f/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/f/d;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bm;)V
    .locals 3

    .prologue
    .line 359
    sget-object v0, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fetchCompleteCallback: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    if-eq p1, v0, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 365
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    .line 366
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fetchCompleteRunnable: waypointInfo state is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/car/bn;->a:Lcom/google/android/apps/gmm/car/bn;

    if-eq v0, v1, :cond_2

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/p;->a()V

    .line 374
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/d;->f()V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->e:Lcom/google/android/apps/gmm/car/h/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/h/r;->c()V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/j;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/d;->e()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method
