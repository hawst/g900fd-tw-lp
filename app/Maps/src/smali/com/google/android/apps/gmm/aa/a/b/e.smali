.class public Lcom/google/android/apps/gmm/aa/a/b/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/aa/a/b/c;

.field private final b:Lcom/google/android/apps/gmm/map/c/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/aa/a/b/c;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/a/b/e;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/a/b/e;->a:Lcom/google/android/apps/gmm/aa/a/b/c;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/aa/a/b/f;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 48
    const/4 v2, 0x0

    iget-object v0, p1, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v8

    move-object v4, v5

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/aa/a/b/g;

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b/e;->a:Lcom/google/android/apps/gmm/aa/a/b/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/a/b/c;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/a/b/b;

    iget-object v3, v1, Lcom/google/android/apps/gmm/aa/a/b/g;->a:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/aa/a/b/b;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/a/b/d;

    move-result-object v3

    if-eqz v3, :cond_1

    iget v0, v1, Lcom/google/android/apps/gmm/aa/a/b/g;->b:F

    iget v10, v3, Lcom/google/android/apps/gmm/aa/a/b/d;->b:F

    mul-float/2addr v0, v10

    cmpl-float v10, v0, v2

    if-lez v10, :cond_8

    move-object v2, v3

    :goto_1
    move-object v4, v2

    move v2, v0

    goto :goto_0

    :cond_2
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, v2, v0

    if-lez v0, :cond_4

    move v0, v6

    :goto_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "parseInternal: bestMatch: %s bestScore: %1.2f recoResult: %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v4, v8, v7

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v8, v6

    const/4 v2, 0x2

    aput-object p1, v8, v2

    invoke-static {v1, v3, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 52
    :goto_3
    if-eqz v4, :cond_3

    .line 54
    iget-object v0, v4, Lcom/google/android/apps/gmm/aa/a/b/d;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->y:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-ne v0, v1, :cond_7

    .line 55
    const-string v1, "http://maps.google.com/maps?q="

    iget-object v0, v4, Lcom/google/android/apps/gmm/aa/a/b/d;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    :goto_4
    new-instance v5, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v5, v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 62
    :cond_3
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x7

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    return-object v5

    :cond_4
    move v0, v7

    .line 48
    goto :goto_2

    :cond_5
    move-object v4, v5

    goto :goto_3

    .line 55
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 57
    :cond_7
    const-string v0, "google.maps:?act=%d&entry=fp"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, v4, Lcom/google/android/apps/gmm/aa/a/b/d;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/aa/a/a/a;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_8
    move v0, v2

    move-object v2, v4

    goto/16 :goto_1
.end method
