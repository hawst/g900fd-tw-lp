.class public Lcom/google/android/apps/gmm/directions/i/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/f;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/directions/i/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/f;Lcom/google/android/apps/gmm/directions/dj;Lcom/google/android/apps/gmm/directions/h/x;Lcom/google/android/apps/gmm/directions/h/z;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/apps/gmm/directions/i/m;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/f;Lcom/google/android/apps/gmm/directions/dj;Lcom/google/android/apps/gmm/directions/h/x;Lcom/google/android/apps/gmm/directions/h/z;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/m;->b:Ljava/util/List;

    .line 40
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/f;Lcom/google/android/apps/gmm/directions/dj;Lcom/google/android/apps/gmm/directions/h/x;Lcom/google/android/apps/gmm/directions/h/z;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/map/r/a/f;",
            "Lcom/google/android/apps/gmm/directions/dj;",
            "Lcom/google/android/apps/gmm/directions/h/x;",
            "Lcom/google/android/apps/gmm/directions/h/z;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/g;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/m;->a:Ljava/lang/String;

    move v0, v2

    :goto_0
    if-nez v0, :cond_5

    .line 75
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 95
    :goto_1
    return-object v0

    .line 74
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/a;->a(Lcom/google/android/apps/gmm/map/r/a/e;)Lcom/google/maps/g/a/z;

    move-result-object v0

    sget-object v3, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-eq v0, v3, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/m;->a:Ljava/lang/String;

    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    iget-object v0, p2, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, v3, :cond_3

    sget-object v4, Lcom/google/android/apps/gmm/directions/i/m;->a:Ljava/lang/String;

    const-string v5, "Invalid trip display order: tripIndex=%d, total=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v4, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 78
    :cond_5
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v4

    .line 79
    iget-object v5, p1, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v6

    .line 82
    iget-object v0, p2, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v7

    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 83
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-ltz v8, :cond_7

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, v8, :cond_8

    :cond_7
    const/4 v0, 0x0

    move-object v3, v0

    .line 84
    :goto_3
    new-instance v9, Lcom/google/android/apps/gmm/directions/i/be;

    invoke-direct {v9, p0, v3, v8}, Lcom/google/android/apps/gmm/directions/i/be;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;I)V

    .line 85
    invoke-static {v3}, Lcom/google/android/apps/gmm/directions/f/d/h;->b(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/f;->c(Lcom/google/maps/g/a/hm;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    if-eq p3, v0, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->c:Lcom/google/android/apps/gmm/directions/h/x;

    if-eq p3, v0, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_c

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v10, 0x1000

    if-ne v0, v10, :cond_b

    move v0, v1

    :goto_5
    if-eqz v0, :cond_c

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bi;->b:Lcom/google/android/apps/gmm/directions/i/bi;

    :goto_6
    iput-object v0, v9, Lcom/google/android/apps/gmm/directions/i/be;->e:Lcom/google/android/apps/gmm/directions/i/bi;

    .line 87
    if-nez v8, :cond_10

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/gmm/directions/g/c;->a(Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/p/b/a;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    :goto_7
    iput-boolean v0, v9, Lcom/google/android/apps/gmm/directions/i/be;->d:Z

    .line 89
    iput-object p3, v9, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    .line 90
    iput-object p4, v9, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/directions/i/be;->a()Lcom/google/android/apps/gmm/directions/h/w;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_6

    .line 92
    invoke-virtual {v4, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_2

    .line 83
    :cond_8
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v8

    if-nez v0, :cond_9

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v10, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v10, v0, v3}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v10, v9, v8

    :cond_9
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v8

    move-object v3, v0

    goto :goto_3

    :cond_a
    move v0, v2

    .line 85
    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_5

    :cond_c
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->o:Z

    if-nez v0, :cond_d

    invoke-static {p0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_d

    move v0, v1

    :goto_8
    if-eqz v0, :cond_f

    if-nez v8, :cond_e

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/gmm/directions/g/c;->a(Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/p/b/a;)Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    :goto_9
    if-eqz v0, :cond_f

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bi;->a:Lcom/google/android/apps/gmm/directions/i/bi;

    goto :goto_6

    :cond_d
    move v0, v2

    goto :goto_8

    :cond_e
    move v0, v2

    goto :goto_9

    :cond_f
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bi;->c:Lcom/google/android/apps/gmm/directions/i/bi;

    goto :goto_6

    :cond_10
    move v0, v2

    .line 87
    goto :goto_7

    .line 95
    :cond_11
    invoke-virtual {v4}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/m;->b:Ljava/util/List;

    return-object v0
.end method
