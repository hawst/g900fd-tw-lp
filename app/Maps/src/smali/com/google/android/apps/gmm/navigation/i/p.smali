.class public Lcom/google/android/apps/gmm/navigation/i/p;
.super Lcom/google/android/apps/gmm/navigation/i/i;
.source "PG"


# static fields
.field private static final d:[I


# instance fields
.field private final c:Lcom/google/maps/g/a/fb;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 183
    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/gmm/l;->et:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/gmm/l;->eu:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/gmm/l;->ev:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/gmm/l;->ew:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/gmm/l;->ex:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/gmm/l;->ey:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/p;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/google/maps/g/a/fb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;",
            "Lcom/google/maps/g/a/fb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/i/i;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 194
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/p;->c:Lcom/google/maps/g/a/fb;

    .line 195
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    return-object v0
.end method

.method public final b()Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/p;->c:Lcom/google/maps/g/a/fb;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 204
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/p;->c:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    if-ne v0, v2, :cond_0

    .line 207
    const/4 v0, 0x2

    .line 212
    :goto_0
    sget-object v2, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/navigation/i/p;->a(Lcom/google/maps/g/a/af;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v2

    .line 213
    if-eqz v2, :cond_1

    .line 214
    add-int/lit8 v0, v0, 0x1

    .line 215
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/p;->a:Landroid/content/Context;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/i/p;->d:[I

    aget v0, v4, v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_1
    return-object v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/p;->c:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    if-ne v0, v2, :cond_2

    .line 209
    const/4 v0, 0x4

    goto :goto_0

    .line 217
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/i/p;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/p;->d:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method
