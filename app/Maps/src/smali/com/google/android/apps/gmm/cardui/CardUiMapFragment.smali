.class public Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;
.super Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment",
        "<",
        "Lcom/google/android/apps/gmm/cardui/f/d;",
        ">;"
    }
.end annotation


# instance fields
.field public w:Lcom/google/android/apps/gmm/cardui/b/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/cardui/b/b;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/cardui/f/d;",
            ">;)",
            "Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v1, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;-><init>()V

    .line 54
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    const-string v2, "placeItemListProviderRef"

    invoke-virtual {p0, v0, v2, p2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 56
    const-string v2, "pageType"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 57
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->setArguments(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    iget v0, v0, Lcom/google/o/h/a/nt;->f:I

    invoke-static {v0}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/o/h/a/ny;->a:Lcom/google/o/h/a/ny;

    :cond_0
    sget-object v2, Lcom/google/o/h/a/ny;->b:Lcom/google/o/h/a/ny;

    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    :goto_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 60
    return-object v1

    .line 58
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/base/l/aj;)V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->w:Lcom/google/android/apps/gmm/cardui/b/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/b/b;->f:Lcom/google/b/f/t;

    return-object v0
.end method

.method protected final i()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f/d;

    .line 189
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    if-eqz v2, :cond_0

    .line 190
    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    iget-object v0, v0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mr;->h()Lcom/google/o/h/a/mr;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mr;

    .line 193
    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    const-string v3, ""

    .line 194
    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    sget v3, Lcom/google/android/apps/gmm/l;->r:I

    .line 195
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    const/4 v3, 0x2

    .line 196
    iput v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    sget v3, Lcom/google/android/apps/gmm/f;->cy:I

    .line 197
    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    new-instance v3, Lcom/google/android/apps/gmm/cardui/l;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/cardui/l;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;)V

    .line 198
    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 208
    new-instance v3, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 210
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    .line 211
    invoke-virtual {v0}, Lcom/google/o/h/a/mr;->d()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    .line 212
    invoke-virtual {v0}, Lcom/google/o/h/a/mr;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/views/c/i;->b:Ljava/lang/CharSequence;

    .line 213
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/gmm/cardui/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/cardui/m;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;)V

    .line 214
    iput-object v0, v2, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    .line 224
    new-instance v2, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 225
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->setProperties(Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 226
    return-object v2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 235
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    if-eqz p1, :cond_0

    .line 76
    :goto_0
    const-string v0, "pageType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/b/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->w:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 77
    return-void

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 97
    const-string v0, "pageType"

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->w:Lcom/google/android/apps/gmm/cardui/b/b;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 98
    return-void
.end method

.method protected final q()Lcom/google/android/apps/gmm/base/activities/ag;
    .locals 5

    .prologue
    .line 107
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v1

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->r()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 118
    :goto_0
    return-object v0

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    .line 113
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 114
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 115
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117
    :cond_1
    iput-object v3, v1, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    move-object v0, v1

    .line 118
    goto :goto_0
.end method

.method public final r()Lcom/google/android/apps/gmm/map/b/a/t;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f/d;

    .line 131
    iget-object v3, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    if-nez v3, :cond_0

    move-object v0, v1

    .line 143
    :goto_0
    return-object v0

    .line 134
    :cond_0
    iget-object v3, v0, Lcom/google/android/apps/gmm/cardui/f/d;->b:Lcom/google/o/h/a/nt;

    .line 135
    iget v0, v3, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    .line 136
    iget-object v0, v3, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move-object v0, v1

    .line 137
    goto :goto_0

    :cond_2
    move v0, v2

    .line 135
    goto :goto_1

    .line 142
    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/t;

    .line 144
    iget-object v0, v3, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    sget-object v2, Lcom/google/maps/b/b/c;->a:Lcom/google/e/a/a/a/d;

    .line 143
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/t;-><init>(Lcom/google/e/a/a/a/b;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected final s()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final t()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method protected final w()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 81
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->w()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    .line 88
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->e()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->e()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v2

    const/4 v4, 0x0

    move-object v0, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/CardUiMapFragment;->a(Lcom/google/android/apps/gmm/base/g/c;IZZZ)V

    .line 92
    :cond_0
    return-void
.end method
