.class public Lcom/google/android/apps/gmm/navigation/logging/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/r/b/a/vc;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:Lcom/google/android/apps/gmm/shared/c/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/c/d",
            "<",
            "Lcom/google/p/a/b;",
            ">;"
        }
    .end annotation
.end field

.field public d:J

.field public e:J

.field public f:Z

.field public final g:Lcom/google/android/apps/gmm/shared/net/ad;

.field public h:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/uy;",
            "Lcom/google/r/b/a/vc;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/vg;",
            "Lcom/google/r/b/a/vk;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/navigation/logging/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/logging/e;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;)V
    .locals 2

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/logging/e;->d:J

    .line 54
    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/logging/e;->e:J

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/logging/e;->g:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 84
    const/16 v0, 0x258

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/d;->a(I)Lcom/google/android/apps/gmm/shared/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/e;->c:Lcom/google/android/apps/gmm/shared/c/d;

    .line 85
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/e;->i:Ljava/util/Set;

    .line 86
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mv;
    .locals 1

    .prologue
    .line 274
    const/16 v0, 0x10

    if-gt p0, v0, :cond_0

    .line 275
    sget-object v0, Lcom/google/d/a/a/mv;->a:Lcom/google/d/a/a/mv;

    .line 291
    :goto_0
    return-object v0

    .line 276
    :cond_0
    const/16 v0, 0x20

    if-gt p0, v0, :cond_1

    .line 277
    sget-object v0, Lcom/google/d/a/a/mv;->b:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 278
    :cond_1
    const/16 v0, 0x30

    if-gt p0, v0, :cond_2

    .line 279
    sget-object v0, Lcom/google/d/a/a/mv;->c:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 280
    :cond_2
    const/16 v0, 0x40

    if-gt p0, v0, :cond_3

    .line 281
    sget-object v0, Lcom/google/d/a/a/mv;->d:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 282
    :cond_3
    const/16 v0, 0x50

    if-gt p0, v0, :cond_4

    .line 283
    sget-object v0, Lcom/google/d/a/a/mv;->e:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 284
    :cond_4
    const/16 v0, 0x60

    if-gt p0, v0, :cond_5

    .line 285
    sget-object v0, Lcom/google/d/a/a/mv;->f:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 286
    :cond_5
    const/16 v0, 0x70

    if-gt p0, v0, :cond_6

    .line 287
    sget-object v0, Lcom/google/d/a/a/mv;->g:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 288
    :cond_6
    const/16 v0, 0x80

    if-gt p0, v0, :cond_7

    .line 289
    sget-object v0, Lcom/google/d/a/a/mv;->h:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 291
    :cond_7
    sget-object v0, Lcom/google/d/a/a/mv;->i:Lcom/google/d/a/a/mv;

    goto :goto_0
.end method
