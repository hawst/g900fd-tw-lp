.class public Lcom/google/android/apps/gmm/util/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/os/Handler$Callback;


# instance fields
.field public a:Landroid/os/Handler;

.field public b:Landroid/animation/AnimatorSet;

.field public c:Z

.field private d:Landroid/app/Fragment;

.field private e:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/h;->c:Z

    return-void
.end method

.method public static a(Landroid/app/Fragment;Landroid/view/View;)Lcom/google/android/apps/gmm/util/h;
    .locals 2

    .prologue
    .line 41
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/util/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/util/h;-><init>()V

    iput-object p0, v1, Lcom/google/android/apps/gmm/util/h;->d:Landroid/app/Fragment;

    iput-object v0, v1, Lcom/google/android/apps/gmm/util/h;->e:Ljava/util/Collection;

    return-object v1
.end method

.method public static a(Landroid/app/Fragment;Ljava/util/Collection;)Lcom/google/android/apps/gmm/util/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Lcom/google/android/apps/gmm/util/h;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/util/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/h;-><init>()V

    .line 49
    iput-object p0, v0, Lcom/google/android/apps/gmm/util/h;->d:Landroid/app/Fragment;

    .line 50
    iput-object p1, v0, Lcom/google/android/apps/gmm/util/h;->e:Ljava/util/Collection;

    .line 51
    return-object v0
.end method

.method private a(F)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    if-ltz v3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 125
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x14

    if-lt v5, v6, :cond_3

    .line 126
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 129
    :cond_3
    const/4 v5, 0x0

    cmpl-float v5, p1, v5

    if-eqz v5, :cond_4

    .line 130
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :cond_4
    const-string v5, "alpha"

    new-array v6, v1, [F

    aput p1, v6, v2

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 134
    :cond_5
    iput p1, p0, Lcom/google/android/apps/gmm/util/h;->f:F

    .line 135
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 139
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 63
    if-eqz p1, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 67
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/util/h;->a(F)V

    .line 68
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/util/h;->c:Z

    .line 69
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/gmm/util/h;->d:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    :goto_0
    return v0

    .line 98
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 100
    :pswitch_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/util/h;->a(F)V

    .line 101
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/h;->c:Z

    .line 102
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    .line 158
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    .line 148
    iget v0, p0, Lcom/google/android/apps/gmm/util/h;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/h;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 150
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 153
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method
