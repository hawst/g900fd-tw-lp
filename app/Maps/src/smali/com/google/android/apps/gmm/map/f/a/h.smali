.class public final enum Lcom/google/android/apps/gmm/map/f/a/h;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/f/a/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/f/a/h;

.field public static final enum b:Lcom/google/android/apps/gmm/map/f/a/h;

.field public static final enum c:Lcom/google/android/apps/gmm/map/f/a/h;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/map/f/a/h;


# instance fields
.field public d:Lcom/google/android/apps/gmm/map/s/a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/h;

    const-string v1, "LOCATION_ONLY"

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/f/a/h;-><init>(Ljava/lang/String;ILcom/google/android/apps/gmm/map/s/a;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/h;->a:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/h;

    const-string v1, "LOCATION_AND_ORIENTATION"

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/f/a/h;-><init>(Ljava/lang/String;ILcom/google/android/apps/gmm/map/s/a;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/h;->b:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/h;

    const-string v1, "LOCATION_AND_BEARING"

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/map/f/a/h;-><init>(Ljava/lang/String;ILcom/google/android/apps/gmm/map/s/a;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/h;->c:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/f/a/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/h;->a:Lcom/google/android/apps/gmm/map/f/a/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/h;->b:Lcom/google/android/apps/gmm/map/f/a/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/h;->c:Lcom/google/android/apps/gmm/map/f/a/h;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/h;->e:[Lcom/google/android/apps/gmm/map/f/a/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/gmm/map/s/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/s/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/f/a/h;->d:Lcom/google/android/apps/gmm/map/s/a;

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/f/a/h;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/map/f/a/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/f/a/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/f/a/h;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/h;->e:[Lcom/google/android/apps/gmm/map/f/a/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/f/a/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/f/a/h;

    return-object v0
.end method
