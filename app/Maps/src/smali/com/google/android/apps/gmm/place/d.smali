.class public Lcom/google/android/apps/gmm/place/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/d;


# instance fields
.field final a:Landroid/content/Context;

.field b:I

.field private final c:Lcom/google/maps/g/cs;

.field private final d:Lcom/google/android/apps/gmm/base/views/c/k;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/cs;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/d;->c:Lcom/google/maps/g/cs;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/d;->a:Landroid/content/Context;

    .line 32
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    .line 33
    iget-object v0, p1, Lcom/google/maps/g/cs;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->i()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v3, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/d;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d;->c:Lcom/google/maps/g/cs;

    invoke-virtual {v0}, Lcom/google/maps/g/cs;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/place/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/f;-><init>(Lcom/google/android/apps/gmm/place/d;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/d;->c:Lcom/google/maps/g/cs;

    invoke-virtual {v3}, Lcom/google/maps/g/cs;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    const v2, -0x21524111

    .line 63
    sget-object v1, Lcom/google/android/apps/gmm/place/e;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d;->c:Lcom/google/maps/g/cs;

    iget v0, v0, Lcom/google/maps/g/cs;->c:I

    invoke-static {v0}, Lcom/google/maps/g/cu;->a(I)Lcom/google/maps/g/cu;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/cu;->a:Lcom/google/maps/g/cu;

    :cond_0
    invoke-virtual {v0}, Lcom/google/maps/g/cu;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 74
    const-string v0, ""

    .line 77
    :goto_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 65
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 71
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
