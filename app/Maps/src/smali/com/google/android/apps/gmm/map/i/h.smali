.class public Lcom/google/android/apps/gmm/map/i/h;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/b;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/i/j;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/res/Resources;

.field private d:Lcom/google/android/apps/gmm/map/b/a/q;

.field private e:Lcom/google/android/apps/gmm/map/t/p;

.field private f:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field private g:Landroid/content/Context;

.field private i:Lcom/google/android/apps/gmm/map/f/o;

.field private j:Lcom/google/android/apps/gmm/map/o/p;

.field private final k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

.field private final l:Ljava/lang/Object;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/i/i;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/google/android/apps/gmm/map/o/b/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/al;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b/c;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/google/android/apps/gmm/map/r/a/v;

.field private r:Lcom/google/android/apps/gmm/map/o/b/h;

.field private s:Z

.field private final t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/map/j/w;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lcom/google/android/apps/gmm/map/x;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/google/android/apps/gmm/map/i/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/i/h;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/i/d;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/x;ZZ)V
    .locals 11

    .prologue
    .line 135
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    .line 97
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->m:Ljava/util/List;

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    .line 110
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    .line 112
    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/v;->b:Lcom/google/android/apps/gmm/map/r/a/v;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 115
    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/h;->a:Lcom/google/android/apps/gmm/map/o/b/h;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->r:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 121
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->t:Ljava/util/Map;

    .line 137
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    .line 138
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    .line 139
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->u:Lcom/google/android/apps/gmm/map/x;

    .line 141
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/i/d;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 143
    const/4 v7, 0x0

    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    .line 146
    iget-object v9, p1, Lcom/google/android/apps/gmm/map/i/d;->a:Ljava/util/List;

    .line 147
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_4

    .line 148
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/r;

    .line 150
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    new-instance v1, Lcom/google/android/apps/gmm/map/i/j;

    move-object v3, p2

    move-object v4, p4

    move/from16 v5, p6

    move/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/i/j;-><init>(Lcom/google/android/apps/gmm/map/r/a/r;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;ZZ)V

    .line 156
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->f()Lcom/google/android/apps/gmm/map/r/a/t;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 159
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->f()Lcom/google/android/apps/gmm/map/r/a/t;

    move-result-object v3

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->m:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/map/i/i;

    invoke-direct {v4, v3, v8}, Lcom/google/android/apps/gmm/map/i/i;-><init>(Lcom/google/android/apps/gmm/map/r/a/t;I)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/t;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eqz v1, :cond_1

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/t;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->r:I

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->g()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 163
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->f()Lcom/google/android/apps/gmm/map/r/a/t;

    move-result-object v1

    .line 167
    :goto_2
    new-instance v4, Lcom/google/android/apps/gmm/map/j/w;

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/gmm/map/j/r;->a:Lcom/google/android/apps/gmm/map/j/r;

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/j/w;-><init>(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    .line 169
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 171
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->i()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 172
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->j()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    .line 173
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/r/a/r;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    .line 169
    iput-object v5, v4, Lcom/google/android/apps/gmm/map/j/q;->c:[Ljava/lang/Object;

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/h;->t:Ljava/util/Map;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/c;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v1

    .line 147
    :cond_0
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_0

    .line 161
    :cond_1
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/t;->d:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v5, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v5, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    :goto_3
    iget-object v5, v1, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v5, :cond_3

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v1

    :goto_4
    iget v1, v1, Lcom/google/maps/g/a/be;->b:I

    goto :goto_1

    :cond_2
    iget-object v1, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_3

    :cond_3
    iget-object v1, v1, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto :goto_4

    .line 179
    :cond_4
    iput-object v7, p0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    .line 181
    if-eqz p3, :cond_6

    .line 182
    invoke-static {p1, p3}, Lcom/google/android/apps/gmm/map/i/h;->a(Lcom/google/android/apps/gmm/map/i/d;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v2

    .line 183
    if-eqz v2, :cond_5

    .line 184
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x40c80000    # 6.25f

    sget-object v7, Lcom/google/android/apps/gmm/map/t/l;->r:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v9, 0x1

    move-object v8, p4

    move/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;II[Lcom/google/android/apps/gmm/map/internal/c/aq;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/v/ad;ZZ)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    const/4 v2, 0x1

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->b(Z)V

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->b(Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d()V

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    const/4 v2, 0x1

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->c(Z)V

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->c(Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d()V

    .line 198
    :goto_5
    return-void

    .line 192
    :cond_5
    sget-object v1, Lcom/google/android/apps/gmm/map/i/h;->b:Ljava/lang/String;

    .line 193
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    goto :goto_5

    .line 196
    :cond_6
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    goto :goto_5

    :cond_7
    move-object v1, v7

    goto/16 :goto_2
.end method

.method private static a(Lcom/google/android/apps/gmm/map/i/d;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/r;

    .line 513
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/r/a/r;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 514
    const-wide/high16 v6, 0x4022000000000000L    # 9.0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 519
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/b;

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/map/b/a/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/af;)V

    .line 520
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 521
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/r/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Ljava/util/List;)V

    .line 523
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v3

    .line 538
    :cond_1
    :goto_0
    return-object v0

    .line 525
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x1

    if-ne v0, v5, :cond_3

    .line 526
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    goto :goto_0

    .line 529
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    move v1, v2

    .line 530
    :goto_1
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-ge v1, v6, :cond_4

    .line 531
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 530
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    move-object v0, v3

    .line 538
    goto :goto_0
.end method

.method private d()V
    .locals 24

    .prologue
    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 218
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/i/h;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->i:Lcom/google/android/apps/gmm/map/f/o;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->g:Landroid/content/Context;

    if-eqz v2, :cond_11

    .line 220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 222
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_0
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/map/i/i;

    move-object/from16 v16, v0

    .line 223
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/i;->a:Lcom/google/android/apps/gmm/map/r/a/t;

    move-object/from16 v22, v0

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v0, v22

    if-eq v0, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    .line 229
    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 231
    :goto_1
    if-eqz v19, :cond_12

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int v2, v3, v2

    move/from16 v18, v2

    .line 235
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/i/h;->r:Lcom/google/android/apps/gmm/map/o/b/h;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/o/b/e;->a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/o/b/h;Z)Lcom/google/android/apps/gmm/map/o/b/e;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v0, v22

    if-eq v0, v2, :cond_2

    move-object v4, v5

    .line 239
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/v;->d:Lcom/google/android/apps/gmm/map/r/a/v;

    if-ne v2, v3, :cond_c

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v0, v22

    if-ne v0, v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->mq:I

    .line 243
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 244
    :goto_4
    const/4 v3, 0x0

    .line 240
    invoke-static {v4, v2, v3}, Lcom/google/android/apps/gmm/map/o/b/c;->a(Lcom/google/android/apps/gmm/map/o/b/e;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v5

    .line 247
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v0, v22

    if-ne v0, v2, :cond_b

    const/4 v2, 0x1

    .line 248
    :goto_5
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v6

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v2

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    aput-object v6, v3, v2

    .line 247
    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v7

    .line 276
    :goto_6
    new-instance v2, Lcom/google/android/apps/gmm/map/o/al;

    .line 280
    move-object/from16 v0, v22

    iget v6, v0, Lcom/google/android/apps/gmm/map/o/b/c;->b:I

    move-object/from16 v3, v22

    move-object/from16 v8, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/o/al;-><init>(Lcom/google/android/apps/gmm/map/o/b/c;Lcom/google/android/apps/gmm/map/o/b/e;Lcom/google/android/apps/gmm/map/internal/c/z;IILcom/google/android/apps/gmm/map/o/ak;)V

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 288
    :catchall_0
    move-exception v2

    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 229
    :cond_1
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_1

    .line 235
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    if-eqz v2, :cond_5

    sget v2, Lcom/google/android/apps/gmm/d;->p:I

    :goto_7
    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/t;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eqz v2, :cond_6

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/t;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    :goto_8
    sget-object v7, Lcom/google/android/apps/gmm/map/r/a/v;->c:Lcom/google/android/apps/gmm/map/r/a/v;

    if-ne v6, v7, :cond_4

    if-eqz v2, :cond_4

    sget-object v6, Lcom/google/android/apps/gmm/map/r/a/u;->a:[I

    iget v2, v2, Lcom/google/maps/g/a/fw;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_3
    invoke-virtual {v2}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v2

    aget v2, v6, v2

    packed-switch v2, :pswitch_data_0

    :cond_4
    :goto_9
    new-instance v17, Lcom/google/android/apps/gmm/map/o/b/e;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bn;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    sget-object v7, Lcom/google/android/apps/gmm/map/r/a/v;->c:Lcom/google/android/apps/gmm/map/r/a/v;

    if-ne v6, v7, :cond_7

    const/16 v5, 0x14

    :goto_a
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    const/16 v23, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/map/o/b/g;

    sget-object v4, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    sget-object v5, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    sget-object v6, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v6, v6, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    sget-object v7, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v7, v7, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    sget-object v8, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v8, v8, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    sget-object v9, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v9, v9, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    sget-object v10, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v10, v10, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    sget-object v11, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v11, v11, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    if-eqz v12, :cond_8

    sget v12, Lcom/google/android/apps/gmm/f;->t:I

    :goto_b
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    if-eqz v13, :cond_9

    sget v13, Lcom/google/android/apps/gmm/f;->p:I

    :goto_c
    sget-object v14, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v14, v14, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/gmm/map/o/b/g;-><init>(FFFFFFFFIIFLcom/google/b/c/dn;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/map/o/b/e;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/o/b/g;)V

    move-object/from16 v4, v17

    goto/16 :goto_3

    :cond_5
    sget v2, Lcom/google/android/apps/gmm/d;->q:I

    goto/16 :goto_7

    :cond_6
    const/4 v2, 0x0

    goto :goto_8

    :pswitch_0
    sget v2, Lcom/google/android/apps/gmm/d;->m:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto :goto_9

    :pswitch_1
    sget v2, Lcom/google/android/apps/gmm/d;->s:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto :goto_9

    :pswitch_2
    sget v2, Lcom/google/android/apps/gmm/d;->o:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    goto :goto_9

    :cond_7
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/o/b/e;->a:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    goto :goto_a

    :cond_8
    sget v12, Lcom/google/android/apps/gmm/f;->w:I

    goto :goto_b

    :cond_9
    sget v13, Lcom/google/android/apps/gmm/f;->s:I

    goto :goto_c

    .line 243
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->mp:I

    .line 244
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 247
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 249
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/v;->e:Lcom/google/android/apps/gmm/map/r/a/v;

    if-ne v2, v3, :cond_f

    .line 250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v0, v22

    if-ne v0, v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->ml:I

    .line 253
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 254
    :goto_d
    const/4 v3, 0x0

    .line 250
    invoke-static {v4, v2, v3}, Lcom/google/android/apps/gmm/map/o/b/c;->a(Lcom/google/android/apps/gmm/map/o/b/e;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v5

    .line 257
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v0, v22

    if-ne v0, v2, :cond_e

    const/4 v2, 0x1

    .line 258
    :goto_e
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v6

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v2

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    aput-object v6, v3, v2

    .line 257
    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v7

    goto/16 :goto_6

    .line 253
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->mk:I

    .line 254
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_d

    .line 257
    :cond_e
    const/4 v2, 0x0

    goto :goto_e

    .line 259
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/v;->f:Lcom/google/android/apps/gmm/map/r/a/v;

    if-ne v2, v3, :cond_10

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->g:Landroid/content/Context;

    .line 262
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, v16

    iget v3, v0, Lcom/google/android/apps/gmm/map/i/i;->b:I

    sget v5, Lcom/google/android/apps/gmm/c;->a:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    array-length v5, v2

    rem-int/2addr v3, v5

    aget-object v2, v2, v3

    const/4 v3, 0x0

    .line 260
    invoke-static {v4, v2, v3}, Lcom/google/android/apps/gmm/map/o/b/c;->a(Lcom/google/android/apps/gmm/map/o/b/e;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v5

    .line 264
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/apps/gmm/map/i/i;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    aput-object v6, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v7

    goto/16 :goto_6

    .line 267
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/h;->g:Landroid/content/Context;

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;IZ)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v5, 0x1

    aget-object v2, v2, v5

    invoke-static {v4, v3, v2}, Lcom/google/android/apps/gmm/map/r/a/t;->a(Lcom/google/android/apps/gmm/map/o/b/e;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v5

    .line 272
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    .line 273
    move-object/from16 v0, v22

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    aput-object v6, v2, v3

    .line 272
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v7

    goto/16 :goto_6

    .line 288
    :cond_11
    monitor-exit v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_12
    move/from16 v18, v3

    goto/16 :goto_2

    .line 235
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/j;

    .line 470
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/i/j;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/i/j;->b()V

    goto :goto_0

    .line 472
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/i/h;->b()V

    .line 473
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 4

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/apps/gmm/map/i/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->c:Landroid/content/res/Resources;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/map/i/e;-><init>(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;)V

    const-string v1, "destination marker dot"

    .line 344
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/i/e;->a:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->A:Lcom/google/android/apps/gmm/map/t/l;

    .line 345
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/i/e;->b:Lcom/google/android/apps/gmm/map/t/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->u:Lcom/google/android/apps/gmm/map/x;

    .line 346
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/x;->a()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/i/e;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 347
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/i/e;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/i/e;->a()Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->e:Lcom/google/android/apps/gmm/map/t/p;

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/j;

    .line 350
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/i/j;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V

    goto :goto_0

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 357
    :cond_1
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 358
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->g:Landroid/content/Context;

    .line 359
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/i/h;->i:Lcom/google/android/apps/gmm/map/f/o;

    .line 361
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->e:Lcom/google/android/apps/gmm/map/t/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 364
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    if-eq v0, v1, :cond_0

    .line 414
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/h;->s:Z

    .line 415
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 416
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/h;->d()V

    .line 418
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o/p;->c(Lcom/google/android/apps/gmm/map/o/ak;)V

    .line 423
    :cond_0
    return-void

    .line 416
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 418
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/am;)V
    .locals 5

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/j;

    .line 433
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/i/j;->a(Lcom/google/android/apps/gmm/map/o/am;)V

    goto :goto_0

    .line 437
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 438
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/v;->b:Lcom/google/android/apps/gmm/map/r/a/v;

    if-ne v0, v2, :cond_2

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/al;

    .line 440
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/al;->a:Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    if-eq v3, v4, :cond_1

    .line 441
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/am;->e:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 451
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 444
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/v;->a:Lcom/google/android/apps/gmm/map/r/a/v;

    if-eq v0, v2, :cond_3

    .line 445
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/am;->e:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 448
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    if-eqz v0, :cond_4

    .line 449
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/am;->f:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/h;->n:Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/o/b/d;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->t:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/b/d;->a:Lcom/google/android/apps/gmm/map/o/b/c;

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/c;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/j/w;

    .line 406
    if-eqz v0, :cond_0

    .line 407
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    .line 409
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 2

    .prologue
    .line 477
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    .line 478
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/h;->d()V

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 481
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/o/p;)V

    goto :goto_0

    .line 483
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/r/a/af;)V
    .locals 10
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 368
    const/4 v2, 0x0

    .line 370
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v3

    .line 371
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/i;

    .line 372
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/i/i;->a:Lcom/google/android/apps/gmm/map/r/a/t;

    .line 373
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/t;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 375
    if-eqz v0, :cond_0

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/r/a/af;->a:Ljava/util/Map;

    .line 376
    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 377
    iget-object v6, p1, Lcom/google/android/apps/gmm/map/r/a/af;->a:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x3b

    div-int/lit8 v0, v0, 0x3c

    .line 380
    const-wide v8, 0x404d800000000000L    # 59.0

    add-double/2addr v8, v6

    double-to-int v8, v8

    div-int/lit8 v8, v8, 0x3c

    .line 381
    if-eq v0, v8, :cond_5

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->p:Ljava/util/Map;

    double-to-int v2, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    :goto_1
    move v2, v0

    .line 385
    goto :goto_0

    :cond_1
    move v2, v1

    .line 386
    goto :goto_0

    .line 391
    :cond_2
    if-eqz v2, :cond_3

    .line 392
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/h;->d()V

    .line 395
    :cond_3
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v0, :cond_4

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o/p;->c(Lcom/google/android/apps/gmm/map/o/ak;)V

    .line 400
    :cond_4
    return-void

    .line 392
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 395
    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V
    .locals 3

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/i/h;->q:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 203
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/i/h;->r:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/i/h;->d()V

    .line 208
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o/p;->c(Lcom/google/android/apps/gmm/map/o/ak;)V

    .line 213
    :cond_0
    return-void

    .line 206
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/af;)V
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/j;

    .line 463
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/i/j;->a(Lcom/google/android/apps/gmm/map/t/af;)V

    goto :goto_0

    .line 465
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->e:Lcom/google/android/apps/gmm/map/t/p;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->e:Lcom/google/android/apps/gmm/map/t/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->e:Lcom/google/android/apps/gmm/map/t/p;

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->k:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b()V

    .line 506
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    if-eqz v0, :cond_2

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 509
    :cond_2
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 2

    .prologue
    .line 487
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/h;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->j:Lcom/google/android/apps/gmm/map/o/p;

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 491
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->b(Lcom/google/android/apps/gmm/map/o/p;)V

    goto :goto_0

    .line 487
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 493
    :cond_0
    return-void
.end method
