.class public Lcom/google/android/apps/gmm/base/views/bc;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

.field private final b:Lcom/google/android/apps/gmm/map/f/o;

.field private final c:Lcom/google/android/apps/gmm/v/cp;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/ScalebarView;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 1

    .prologue
    .line 520
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 521
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/bc;->b:Lcom/google/android/apps/gmm/map/f/o;

    .line 522
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->c:Lcom/google/android/apps/gmm/v/cp;

    .line 523
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 577
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->d:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 528
    if-eqz p1, :cond_0

    .line 529
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 531
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->b:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->b:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, v0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v4, v5

    iget v5, v0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    div-float v0, v4, v0

    float-to-double v4, v0

    div-double/2addr v2, v4

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->n:D

    float-to-double v6, v1

    cmpl-double v0, v4, v6

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->m:D

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->b(ID)F

    move-result v0

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-object v6, v6, Lcom/google/android/apps/gmm/base/views/ScalebarView;->k:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6, v2, v3}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->b(ID)F

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-object v7, v7, Lcom/google/android/apps/gmm/base/views/ScalebarView;->j:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7, v4, v5}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(ID)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/views/ScalebarView;->j:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5, v2, v3}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(ID)F

    move-result v5

    sub-float v0, v6, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v6, v0

    cmpl-double v0, v6, v8

    if-gez v0, :cond_0

    sub-float v0, v5, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v4, v0

    cmpl-double v0, v4, v8

    if-ltz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    new-instance v4, Lcom/google/android/apps/gmm/base/views/bd;

    invoke-direct {v4, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/bd;-><init>(Lcom/google/android/apps/gmm/base/views/bc;FD)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->post(Ljava/lang/Runnable;)Z

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bc;->c:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 553
    return-void

    .line 541
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
