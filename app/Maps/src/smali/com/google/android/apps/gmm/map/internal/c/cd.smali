.class public Lcom/google/android/apps/gmm/map/internal/c/cd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/cd;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bv;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            "Lcom/google/android/apps/gmm/map/internal/c/cd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/bu;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    .line 37
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v0

    array-length v0, v0

    new-instance v1, Ljava/util/HashSet;

    invoke-static {v0}, Lcom/google/b/c/hj;->b(I)I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    .line 43
    const/4 v0, 0x4

    .line 45
    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/cd;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/bu;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    .line 37
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v0

    array-length v0, v0

    new-instance v1, Ljava/util/HashSet;

    invoke-static {v0}, Lcom/google/b/c/hj;->b(I)I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    .line 43
    const/4 v0, 0x4

    .line 45
    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    .line 57
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 58
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_1
    return-void
.end method

.method public varargs constructor <init>([Lcom/google/android/apps/gmm/map/internal/c/bu;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/bu;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    .line 37
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v0

    array-length v0, v0

    new-instance v1, Ljava/util/HashSet;

    invoke-static {v0}, Lcom/google/b/c/hj;->b(I)I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    .line 43
    const/4 v0, 0x4

    .line 45
    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    .line 51
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 52
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/cd;Lcom/google/android/apps/gmm/map/internal/c/bv;)Lcom/google/android/apps/gmm/map/internal/c/bu;
    .locals 2

    .prologue
    .line 93
    if-nez p0, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/cd;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 184
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v6

    aget-object v5, v5, v6

    .line 185
    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v0

    aget-object v0, v6, v0

    .line 186
    if-nez v5, :cond_1

    .line 187
    if-eqz v0, :cond_2

    .line 188
    const/4 v0, -0x1

    .line 197
    :cond_0
    :goto_1
    return v0

    .line 192
    :cond_1
    invoke-interface {v5, v0}, Lcom/google/android/apps/gmm/map/internal/c/bu;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 193
    if-nez v0, :cond_0

    .line 183
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 197
    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cd;
    .locals 4

    .prologue
    .line 241
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    monitor-enter v2

    .line 242
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    .line 243
    if-nez v0, :cond_2

    .line 244
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    .line 245
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 246
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    .line 247
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v3, v3, v1

    invoke-interface {v3, p1}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 248
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v3, v3, v1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    .line 245
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 252
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    :cond_2
    monitor-exit v2

    return-object v0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a()Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v1

    aput-object p1, v0, v1

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a()Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    monitor-enter v1

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 115
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/c/bv;)V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    monitor-enter v1

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 126
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/cd;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 140
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 155
    :cond_0
    :goto_0
    return v1

    .line 143
    :cond_1
    if-nez p1, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    goto :goto_0

    .line 146
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;

    if-eqz v0, :cond_0

    .line 149
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/cd;

    move v0, v1

    .line 150
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 151
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v3, v3, v0

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v4, v4, v0

    if-eq v3, v4, :cond_3

    if-eqz v3, :cond_4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v3, v2

    :goto_2
    if-eqz v3, :cond_0

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v3, v1

    .line 151
    goto :goto_2

    :cond_5
    move v1, v2

    .line 155
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    const-string v0, ""

    .line 178
    :goto_0
    return-object v0

    .line 163
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    const-string v1, "{"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 166
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 167
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    .line 168
    if-eqz v1, :cond_1

    .line 169
    const-string v1, ", "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/bv;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string v1, "="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const/4 v1, 0x1

    .line 166
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :cond_3
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
