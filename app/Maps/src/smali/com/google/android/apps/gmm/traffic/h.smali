.class public Lcom/google/android/apps/gmm/traffic/h;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/traffic/a/a;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/h;->a:Z

    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 26
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/t;)V
    .locals 8
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/e;

    if-nez v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/e;

    .line 46
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/e;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 47
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->j()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    .line 48
    if-nez v1, :cond_2

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0

    .line 56
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/h;->a:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 60
    instance-of v3, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    if-eqz v3, :cond_3

    .line 61
    check-cast v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    iput-object v1, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iput-object v2, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v2, Lcom/google/android/apps/gmm/traffic/d;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->c:Z

    iget-object v4, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v4, v4, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/google/android/apps/gmm/traffic/d;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/map/internal/c/cf;ZLcom/google/android/libraries/curvular/ag;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a(Z)V

    goto :goto_0

    .line 65
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 66
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;->a(Lcom/google/android/apps/gmm/map/internal/c/cf;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    move-result-object v1

    .line 65
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/traffic/h;->a:Z

    .line 74
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/traffic/TrafficIncidentFragment;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 81
    :cond_0
    return-void
.end method
