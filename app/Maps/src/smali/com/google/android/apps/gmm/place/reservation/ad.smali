.class public Lcom/google/android/apps/gmm/place/reservation/ad;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/base/g/c;

.field b:Lcom/google/r/b/a/j;

.field c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Landroid/os/Bundle;)Lcom/google/android/apps/gmm/place/reservation/ad;
    .locals 2

    .prologue
    .line 80
    const-string v0, "placemarkRef"

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 81
    new-instance v1, Lcom/google/android/apps/gmm/place/reservation/ad;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/reservation/ad;-><init>()V

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    :goto_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/ad;->a:Lcom/google/android/apps/gmm/base/g/c;

    const-string v0, "reservationInfo"

    .line 83
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/j;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/ad;->b:Lcom/google/r/b/a/j;

    const-string v0, "email"

    .line 84
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/ad;->c:Ljava/lang/String;

    return-object v1

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/place/reservation/ac;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/ac;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/reservation/ac;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ad;->a:Lcom/google/android/apps/gmm/base/g/c;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ad;->b:Lcom/google/r/b/a/j;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->b:Lcom/google/r/b/a/j;

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ad;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/ac;->c:Ljava/lang/String;

    .line 72
    return-object v0
.end method
