.class Lcom/google/android/apps/gmm/base/activities/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/StringBuilder;

.field final synthetic b:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 1773
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/activities/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/activities/m;->a:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1785
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/l/av;->J_()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1786
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/m;->a:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/l/av;->J_()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1796
    :cond_0
    :goto_0
    return-void

    .line 1790
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/MapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1794
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/m;->a:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    iget v2, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.google.com/?"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "ll="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&z"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_1
.end method
