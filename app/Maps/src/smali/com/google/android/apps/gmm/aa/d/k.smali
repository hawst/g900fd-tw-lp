.class public Lcom/google/android/apps/gmm/aa/d/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/aa/d/k;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/apps/gmm/aa/d/s;

.field final c:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/aa/d/s;J)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/k;->a:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/d/k;->b:Lcom/google/android/apps/gmm/aa/d/s;

    .line 26
    iput-wide p3, p0, Lcom/google/android/apps/gmm/aa/d/k;->c:J

    .line 27
    return-void
.end method


# virtual methods
.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 12
    check-cast p1, Lcom/google/android/apps/gmm/aa/d/k;

    invoke-static {}, Lcom/google/b/c/ap;->a()Lcom/google/b/c/ap;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/aa/d/k;->b:Lcom/google/android/apps/gmm/aa/d/s;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/k;->b:Lcom/google/android/apps/gmm/aa/d/s;

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/ap;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/google/b/c/ap;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/aa/d/k;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/aa/d/k;->c:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/b/c/ap;->a(JJ)Lcom/google/b/c/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/ap;->b()I

    move-result v0

    return v0
.end method
