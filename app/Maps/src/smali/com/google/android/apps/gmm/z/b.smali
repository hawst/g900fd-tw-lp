.class public Lcom/google/android/apps/gmm/z/b;
.super Lcom/google/android/apps/gmm/z/b/e;
.source "PG"


# instance fields
.field private final a:Lcom/google/b/f/b/a/ah;

.field private final h:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/b/f/b/a/ah;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, v0, v0, p2}, Lcom/google/android/apps/gmm/z/b;-><init>(Lcom/google/b/f/b/a/ah;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/b/f/b/a/ah;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 0
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/b;->a:Lcom/google/b/f/b/a/ah;

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/b;->h:Ljava/lang/Integer;

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/b;->i:Ljava/lang/Integer;

    .line 45
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/b/f/b/a/bc;)V
    .locals 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/bc;)V

    .line 51
    invoke-static {}, Lcom/google/b/f/b/a/ae;->newBuilder()Lcom/google/b/f/b/a/ag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b;->a:Lcom/google/b/f/b/a/ah;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/b/f/b/a/ag;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/b/f/b/a/ag;->a:I

    iget v1, v1, Lcom/google/b/f/b/a/ah;->j:I

    iput v1, v0, Lcom/google/b/f/b/a/ag;->b:I

    .line 52
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, v0, Lcom/google/b/f/b/a/ag;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/b/f/b/a/ag;->a:I

    iput v1, v0, Lcom/google/b/f/b/a/ag;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b;->i:Ljava/lang/Integer;

    .line 54
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, v0, Lcom/google/b/f/b/a/ag;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/b/f/b/a/ag;->a:I

    iput v1, v0, Lcom/google/b/f/b/a/ag;->d:I

    .line 57
    :cond_1
    iget-object v1, p1, Lcom/google/b/f/b/a/bc;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/ag;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    .line 58
    return-void
.end method
