.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/q;
.super Lcom/google/android/apps/gmm/v/ci;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:F

.field public e:F

.field private w:Z

.field private x:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;-><init>(I)V

    .line 50
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    .line 38
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->c:Z

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->w:Z

    .line 46
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->x:I

    .line 54
    return-void
.end method

.method public static a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 206
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 207
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 208
    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 209
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 210
    invoke-static {p0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lcom/google/android/apps/gmm/map/internal/vector/gl/o;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 229
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 230
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 231
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v2

    .line 232
    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v3

    .line 234
    invoke-virtual {p2, v2, v3, p1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/o;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 235
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 236
    invoke-virtual {v4, v10}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 237
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 238
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 239
    invoke-virtual {v5, p0, v7, v7, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 241
    if-le v2, v0, :cond_0

    .line 242
    new-instance v7, Landroid/graphics/Rect;

    add-int/lit8 v8, v0, -0x1

    invoke-direct {v7, v8, v10, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v0, 0x1

    invoke-direct {v8, v0, v10, v9, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 245
    :cond_0
    if-le v3, v1, :cond_1

    .line 246
    new-instance v7, Landroid/graphics/Rect;

    add-int/lit8 v8, v1, -0x1

    invoke-direct {v7, v10, v8, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    add-int/lit8 v9, v1, 0x1

    invoke-direct {v8, v10, v1, v0, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v7, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 249
    :cond_1
    if-le v2, v0, :cond_2

    if-le v3, v1, :cond_2

    .line 250
    new-instance v2, Landroid/graphics/Rect;

    add-int/lit8 v3, v0, -0x1

    add-int/lit8 v7, v1, -0x1

    invoke-direct {v2, v3, v7, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    add-int/lit8 v7, v0, 0x1

    add-int/lit8 v8, v1, 0x1

    invoke-direct {v3, v0, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p0, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 253
    :cond_2
    return-object v4
.end method

.method static a(Landroid/graphics/Bitmap;)Z
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 218
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 219
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/s;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/s;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/map/internal/vector/gl/a;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/s;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v5}, Lcom/google/android/apps/gmm/shared/c/s;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iget-object v5, p3, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/o;

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lcom/google/android/apps/gmm/map/internal/vector/gl/o;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :goto_1
    move-object v0, p0

    move v5, v4

    move v6, v4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    .line 64
    return-void

    :cond_0
    move v1, v4

    .line 63
    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_1
.end method

.method final declared-synchronized a(Landroid/graphics/Bitmap;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/a;)V
    .locals 6

    .prologue
    .line 275
    monitor-enter p0

    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    .line 276
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot have both isMipMap and autoGenerateMipMap be true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 282
    :cond_0
    if-nez p1, :cond_3

    .line 283
    :try_start_1
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a:I

    .line 284
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->b:I

    .line 285
    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v1

    .line 286
    const/4 v0, 0x1

    invoke-static {p3, v0}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v0

    move v2, v0

    move v3, v1

    .line 293
    :goto_0
    iget-object v0, p7, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget v0, v0, Lcom/google/android/apps/gmm/v/ao;->b:I

    .line 298
    if-nez v0, :cond_1

    sget v1, Lcom/google/android/apps/gmm/map/util/c;->j:I

    if-eqz v1, :cond_1

    .line 299
    sget v0, Lcom/google/android/apps/gmm/map/util/c;->j:I

    .line 300
    const-string v1, "Texture"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x48

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Device is reporting 0 max texture width/height. Use "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " instead."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 304
    :cond_1
    if-gt v3, v0, :cond_2

    if-le v2, v0, :cond_5

    .line 305
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x74

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Textures with dimensions "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " are larger than  the maximum supported size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 288
    :cond_3
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a:I

    .line 289
    if-eqz p5, :cond_4

    div-int/lit8 v0, p3, 0x2

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->b:I

    .line 290
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 291
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    move v2, v0

    move v3, v1

    goto :goto_0

    :cond_4
    move v0, p3

    .line 289
    goto :goto_1

    .line 310
    :cond_5
    int-to-float v0, p2

    int-to-float v1, v3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->d:F

    .line 311
    int-to-float v0, p3

    int-to-float v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->e:F

    .line 313
    iget v0, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    const/16 v1, 0x2901

    if-eq v0, v1, :cond_6

    .line 314
    iget v0, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    const/16 v1, 0x2901

    if-ne v0, v1, :cond_9

    :cond_6
    const/4 v0, 0x1

    move v1, v0

    .line 316
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->c:Z

    if-eqz v0, :cond_e

    .line 317
    if-nez p5, :cond_7

    if-eqz p6, :cond_c

    .line 318
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->w:Z

    if-eqz v0, :cond_a

    .line 319
    const/16 v0, 0x2703

    const/16 v4, 0x2601

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 333
    :goto_3
    if-eqz p1, :cond_13

    .line 334
    if-eqz p5, :cond_12

    .line 335
    if-eqz p4, :cond_10

    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    .line 336
    :goto_4
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/v/ar;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;)Ljava/util/List;

    move-result-object v0

    .line 337
    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/v/ar;-><init>(Ljava/util/List;Z)V

    const/4 v1, 0x0

    .line 338
    iget v3, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    iget v4, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    .line 337
    invoke-virtual {p0, v2, v1, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Lcom/google/android/apps/gmm/v/ar;ZII)V

    .line 339
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 340
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->x:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->x:I

    goto :goto_5

    .line 314
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    .line 322
    :cond_a
    const/16 v0, 0x2701

    const/16 v4, 0x2601

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_b
    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    goto :goto_3

    .line 326
    :cond_c
    const/16 v0, 0x2601

    const/16 v4, 0x2601

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_d

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_d
    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    goto :goto_3

    .line 330
    :cond_e
    const/16 v0, 0x2600

    const/16 v4, 0x2600

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_f
    iput v0, p0, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, p0, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    goto :goto_3

    .line 335
    :cond_10
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto :goto_4

    .line 342
    :cond_11
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353
    :goto_6
    monitor-exit p0

    return-void

    .line 344
    :cond_12
    :try_start_2
    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    iget-object v2, p7, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, p1, v2, v1}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    .line 345
    iget v1, p0, Lcom/google/android/apps/gmm/v/ci;->k:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/ci;->l:I

    .line 344
    invoke-virtual {p0, v0, p6, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Lcom/google/android/apps/gmm/v/ar;ZII)V

    .line 346
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->x:I

    goto :goto_6

    .line 350
    :cond_13
    mul-int v0, v3, v2

    mul-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->x:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6
.end method
