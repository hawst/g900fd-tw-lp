.class public Lcom/google/android/apps/gmm/map/b/a/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ad;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field private d:Lcom/google/android/apps/gmm/map/b/a/af;

.field private e:I

.field private f:[Lcom/google/android/apps/gmm/map/b/a/y;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/af;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->b:Ljava/util/List;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->g:Ljava/util/List;

    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/af;)V

    .line 29
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/af;)V
    .locals 3

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    .line 36
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->e:I

    .line 37
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->e:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    .line 38
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v2, v1, v0

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 120
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->e:I

    if-ne p1, v0, :cond_4

    .line 121
    if-eqz p4, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/b;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->b:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->g:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/c;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/c;

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    array-length v2, v2

    if-ne v1, v2, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    array-length v1, v1

    shl-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    aput p3, v1, v2

    .line 156
    :cond_3
    :goto_0
    return-void

    .line 125
    :cond_4
    if-nez p1, :cond_7

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 133
    :goto_1
    invoke-static {v1, v0, p2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)I

    move-result v2

    if-ltz v2, :cond_8

    .line 135
    if-nez p4, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, p1

    .line 136
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)I

    move-result v2

    if-gez v2, :cond_5

    .line 138
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 139
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v3, p1

    invoke-static {v1, v0, p2, v3, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 141
    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, p3, v1}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    .line 143
    :cond_5
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    .line 154
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, p1

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto :goto_0

    .line 129
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    goto :goto_1

    .line 145
    :cond_8
    if-nez p4, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, p1

    .line 146
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)I

    move-result v2

    if-ltz v2, :cond_6

    .line 149
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 150
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/b;->f:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v3, p1

    invoke-static {v1, v0, v3, p2, v2}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 152
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0, v2, p3, v4}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ab;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0, p1, v0, p2, v0}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ab;[ILjava/util/List;Ljava/util/List;)V

    .line 50
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ab;[ILjava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            "[I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;",
            "Ljava/util/List",
            "<[I>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    iput v2, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    .line 67
    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    .line 70
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 71
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 108
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 67
    goto :goto_0

    .line 73
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/b;->d:Lcom/google/android/apps/gmm/map/b/a/af;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->b(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    if-eqz v0, :cond_0

    .line 76
    invoke-interface {p4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 82
    :cond_3
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 83
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v5, v0, 0x3

    .line 84
    invoke-virtual {p1, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    if-eqz v0, :cond_4

    aget v0, p2, v2

    :goto_2
    invoke-virtual {p0, v2, v4, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    move v3, v1

    .line 86
    :goto_3
    if-ge v3, v5, :cond_6

    .line 87
    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 88
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    if-eqz v0, :cond_5

    aget v0, p2, v3

    :goto_4
    invoke-virtual {p0, v2, v4, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    .line 86
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 85
    goto :goto_2

    :cond_5
    move v0, v2

    .line 88
    goto :goto_4

    :cond_6
    move v3, v2

    .line 93
    :goto_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    if-ge v3, v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ad;

    .line 95
    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-le v4, v1, :cond_7

    .line 96
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_7
    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    .line 100
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    if-eqz v0, :cond_9

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/b;->g:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/c;

    .line 102
    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    if-le v4, v1, :cond_8

    .line 103
    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    new-array v4, v4, [I

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/c;->a:[I

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    invoke-static {v5, v2, v4, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-interface {p4, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_8
    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/c;->b:I

    .line 93
    :cond_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5
.end method
