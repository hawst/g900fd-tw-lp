.class public final Lcom/google/android/apps/gmm/terms/KillSwitchFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/terms/KillSwitchFragment;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;-><init>()V

    .line 29
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 30
    const-string v2, "killSwitchContentUrl"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;->setArguments(Landroid/os/Bundle;)V

    .line 32
    return-object v0
.end method


# virtual methods
.method public final k()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "killSwitchContentUrl"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;->a:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 43
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 44
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 45
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 46
    sget v0, Lcom/google/android/apps/gmm/h;->az:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/g;->dJ:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/apps/gmm/terms/a;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/terms/a;-><init>(Lcom/google/android/apps/gmm/terms/KillSwitchFragment;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/terms/KillSwitchFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 47
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 48
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 50
    return-void
.end method
