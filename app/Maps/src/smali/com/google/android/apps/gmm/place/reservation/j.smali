.class Lcom/google/android/apps/gmm/place/reservation/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/e;


# instance fields
.field final a:Lcom/google/r/b/a/j;

.field final b:Z

.field final c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/apps/gmm/place/reservation/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/reservation/g;Lcom/google/r/b/a/j;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 428
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/reservation/j;->a:Lcom/google/r/b/a/j;

    .line 431
    const/4 v0, 0x0

    .line 432
    if-eqz p2, :cond_0

    .line 433
    invoke-virtual {p2}, Lcom/google/r/b/a/j;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 436
    :cond_0
    if-eqz v0, :cond_1

    .line 437
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/place/reservation/j;->b:Z

    .line 438
    iget-object v1, p1, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->c:Ljava/lang/String;

    .line 444
    :goto_0
    return-void

    .line 441
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->b:Z

    .line 442
    const-string v0, "-"

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->c:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 464
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/g;->n()V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    invoke-static {v0}, Lcom/google/android/apps/gmm/place/reservation/g;->a(Lcom/google/android/apps/gmm/place/reservation/g;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    .line 452
    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    .line 454
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/j;->a:Lcom/google/r/b/a/j;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/ReservationLoginFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;)Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    move-result-object v0

    .line 453
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 459
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    .line 457
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/j;->a:Lcom/google/r/b/a/j;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;)Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;

    move-result-object v0

    .line 456
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/j;->d:Lcom/google/android/apps/gmm/place/reservation/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/g;->h:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
