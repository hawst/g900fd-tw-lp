.class public Lcom/google/android/apps/gmm/aa/d/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/util/Locale;

.field c:I

.field d:Lcom/google/android/apps/gmm/aa/d/n;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/aa/d/m;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iget-object v0, p1, Lcom/google/android/apps/gmm/aa/d/m;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/l;->a:Ljava/lang/String;

    .line 34
    iget-object v0, p1, Lcom/google/android/apps/gmm/aa/d/m;->b:Ljava/util/Locale;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/l;->b:Ljava/util/Locale;

    .line 35
    iget v0, p1, Lcom/google/android/apps/gmm/aa/d/m;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/aa/d/l;->c:I

    .line 36
    iget-object v0, p1, Lcom/google/android/apps/gmm/aa/d/m;->d:Lcom/google/android/apps/gmm/aa/d/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/l;->d:Lcom/google/android/apps/gmm/aa/d/n;

    .line 37
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->a:Ljava/lang/String;

    .line 58
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "locale"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->b:Ljava/util/Locale;

    .line 59
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "epoch"

    iget v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->c:I

    .line 60
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "synthesisMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/l;->d:Lcom/google/android/apps/gmm/aa/d/n;

    .line 61
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 62
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/b/a/ah;->a:Z

    .line 63
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
