.class Lcom/google/android/apps/gmm/car/i/ae;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/i/ab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/i/ab;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/a;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 354
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget v0, p1, Lcom/google/android/apps/gmm/base/e/a;->a:I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v2, Lcom/google/android/apps/gmm/car/i/ab;->f:Z

    .line 355
    sget-object v0, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    .line 356
    iget v0, p1, Lcom/google/android/apps/gmm/base/e/a;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/car/i/ab;->f:Z

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x51

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onApplicationEnvironmentStateUpdate: numActiveClients="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", isPaused="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->h:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->c:Lcom/google/android/apps/gmm/car/bg;

    sget-object v3, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/apps/gmm/car/bg;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    .line 363
    :cond_0
    return-void

    .line 354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    if-nez v0, :cond_1

    .line 332
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->c:Lcom/google/android/apps/gmm/car/bg;

    sget-object v2, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    sget-object v3, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/car/bg;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->g:Z

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    .line 335
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceTo(Landroid/location/Location;)F

    move-result v0

    const/high16 v1, 0x43fa0000    # 500.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    .line 336
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->f:Z

    if-nez v0, :cond_2

    .line 339
    sget-object v0, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceTo(Landroid/location/Location;)F

    move-result v0

    float-to-int v0, v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x39

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Moved "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "m, refreshing directions to suggestions."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/i/ab;->a(Lcom/google/android/apps/gmm/car/i/ab;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    goto :goto_0

    .line 345
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/startpage/b/c;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 312
    sget-object v0, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/i/ab;->d:Lcom/google/android/apps/gmm/car/i/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/i/ab;->c:Lcom/google/android/apps/gmm/car/bg;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/b/c;->a:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    .line 315
    iget-object v4, v4, Lcom/google/android/apps/gmm/car/i/ab;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    const/4 v5, 0x2

    .line 314
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/i/h;->a(Lcom/google/android/apps/gmm/car/bg;Ljava/util/List;Lcom/google/android/apps/gmm/map/r/b/a;I)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/i/ab;->a(Lcom/google/android/apps/gmm/car/i/ab;Z)Z

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/i/ab;->h:Z

    if-nez v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ae;->a:Lcom/google/android/apps/gmm/car/i/ab;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/i/ab;->a(Lcom/google/android/apps/gmm/car/i/ab;)V

    .line 326
    :cond_0
    return-void
.end method
