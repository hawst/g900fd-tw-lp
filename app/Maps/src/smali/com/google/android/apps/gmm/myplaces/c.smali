.class public Lcom/google/android/apps/gmm/myplaces/c;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/a/e;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/base/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/gmm/myplaces/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 194
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/myplaces/c;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/myplaces/c;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 112
    invoke-static {v1, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->a(Landroid/content/Context;ILjava/lang/String;ZZ)Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    move-result-object v1

    .line 111
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 114
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 58
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    .line 59
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 72
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 73
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->X()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c;->a:Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/c;->c()V

    .line 106
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 80
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-boolean v0, v0, Lcom/google/maps/g/ki;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/gmm/myplaces/c;->a(ZLcom/google/android/apps/gmm/x/o;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->go:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/myplaces/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/myplaces/d;-><init>(Lcom/google/android/apps/gmm/myplaces/c;Lcom/google/android/apps/gmm/x/o;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V

    goto :goto_0
.end method

.method a(ZLcom/google/android/apps/gmm/x/o;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 117
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 119
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/z/b/n;

    if-eqz p1, :cond_0

    sget-object v1, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    :goto_0
    invoke-direct {v3, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    new-array v1, v7, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->dx:Lcom/google/b/f/t;

    aput-object v6, v1, v8

    iput-object v1, v4, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 125
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v7

    :goto_2
    if-eqz v1, :cond_3

    .line 128
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    move-object v2, v5

    move-object v1, v5

    .line 136
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    .line 138
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 139
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->h:Z

    .line 141
    new-instance v6, Lcom/google/android/apps/gmm/myplaces/f;

    invoke-direct {v6, p0, p1, p2, v5}, Lcom/google/android/apps/gmm/myplaces/f;-><init>(Lcom/google/android/apps/gmm/myplaces/c;ZLcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/map/internal/d/b;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/a/a;->a()Ljava/lang/String;

    move-result-object v4

    .line 166
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c;->a:Ljava/lang/String;

    const-string v0, "Sending a StarringDataRequest. starUnstar: %b, featureId: %s, query: %s, latLng: %s, persistenceKey: %s"

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    .line 168
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v8

    aput-object v1, v9, v7

    const/4 v7, 0x2

    aput-object v2, v9, v7

    const/4 v7, 0x3

    aput-object v3, v9, v7

    const/4 v7, 0x4

    aput-object v4, v9, v7

    .line 166
    invoke-static {v0, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, p1

    .line 170
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/myplaces/g;->a(ZLcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b;Lcom/google/android/apps/gmm/myplaces/h;)Lcom/google/android/apps/gmm/myplaces/g;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 173
    return-void

    .line 120
    :cond_0
    sget-object v1, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    goto :goto_0

    :cond_1
    move-object v1, v5

    goto :goto_1

    :cond_2
    move v1, v8

    .line 125
    goto :goto_2

    .line 130
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 131
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v2

    move-object v3, v5

    .line 133
    goto :goto_3
.end method

.method c()V
    .locals 3

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/i;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->f:Lcom/google/android/apps/gmm/myplaces/c/j;

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 190
    invoke-static {v2}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/myplaces/c/i;-><init>(Lcom/google/android/apps/gmm/myplaces/c/j;Ljava/util/Set;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 192
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/a/a;->a:Ljava/util/Set;

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Ljava/util/Set;ZZ)V

    .line 67
    return-void
.end method
