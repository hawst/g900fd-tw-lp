.class Lcom/google/android/apps/gmm/map/internal/d/c/b/b;
.super Lcom/google/android/apps/gmm/map/internal/d/c/b/d;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/internal/d/c/b/d",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;[B)V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;[B)V

    .line 264
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Landroid/content/res/Resources;F)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p2    # Landroid/content/res/Resources;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 261
    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;

    invoke-direct {v0, p2, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected final synthetic a()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a:[B

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
