.class Lcom/google/android/apps/gmm/base/placelists/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->u:Z

    if-nez v0, :cond_1

    .line 436
    iget v0, p1, Lcom/google/android/apps/gmm/base/e/c;->b:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 437
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->d:Ljava/lang/String;

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 443
    :cond_1
    :goto_2
    return-void

    .line 437
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 441
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v3, :cond_5

    :goto_3
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->d:Ljava/lang/String;

    .line 442
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    if-eqz v0, :cond_6

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    move v0, v2

    :goto_4
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v4

    if-ge v0, v4, :cond_6

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v4

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 441
    :cond_5
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_3

    .line 442
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/map/indoor/b/b;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->e()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v2

    const/4 v5, 0x1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/g/c;IZZZ)V

    .line 430
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/af;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 335
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 346
    :cond_0
    :goto_1
    return-void

    .line 335
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 343
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/t;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 351
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/a;

    if-nez v0, :cond_1

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->p:Z

    .line 359
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/a;

    .line 360
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/g/a;->f:Z

    if-eqz v3, :cond_0

    .line 364
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o()V

    .line 366
    new-instance v3, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 367
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/g/a;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    .line 369
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 370
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 371
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 373
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v3

    .line 374
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(Lcom/google/android/apps/gmm/base/g/c;)Z

    move-result v0

    .line 375
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c()I

    move-result v4

    .line 376
    if-eqz v0, :cond_2

    .line 377
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    .line 381
    :cond_2
    if-ne v4, v3, :cond_5

    .line 382
    :goto_1
    if-eqz v1, :cond_3

    if-eqz v0, :cond_4

    .line 386
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/search/h;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    .line 388
    iget-object v3, v3, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->u()Z

    move-result v5

    invoke-direct {v2, v3, v5}, Lcom/google/android/apps/gmm/search/h;-><init>(IZ)V

    .line 387
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 390
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setCurrentItem(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    new-instance v2, Lcom/google/android/apps/gmm/base/placelists/g;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/base/placelists/g;-><init>(Lcom/google/android/apps/gmm/base/placelists/f;Z)V

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(ILcom/google/android/apps/gmm/base/placelists/a/b;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 381
    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/f;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/map/j/z;)V

    .line 325
    return-void
.end method
