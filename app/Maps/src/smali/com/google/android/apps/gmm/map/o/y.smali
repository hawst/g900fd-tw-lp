.class Lcom/google/android/apps/gmm/map/o/y;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LABELING_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field volatile a:Z

.field b:Z

.field final synthetic c:Lcom/google/android/apps/gmm/map/o/q;

.field private final d:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/o/q;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 638
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/y;->c:Lcom/google/android/apps/gmm/map/o/q;

    .line 639
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 625
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/y;->a:Z

    .line 630
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/y;->b:Z

    .line 636
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/y;->d:Ljava/util/concurrent/Semaphore;

    .line 640
    return-void
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 2

    .prologue
    .line 646
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/y;->c:Lcom/google/android/apps/gmm/map/o/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/o/at;->w:Z

    .line 647
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/y;->b:Z

    .line 648
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 649
    monitor-exit p0

    return-void

    .line 646
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()V
    .locals 2

    .prologue
    .line 656
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/y;->c:Lcom/google/android/apps/gmm/map/o/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/q;->a:Lcom/google/android/apps/gmm/map/o/at;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/o/at;->w:Z

    .line 657
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/y;->a:Z

    .line 658
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 659
    monitor-exit p0

    return-void

    .line 656
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 679
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 684
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/y;->a:Z

    if-nez v1, :cond_2

    .line 686
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    :goto_1
    :try_start_1
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/y;->a:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/y;->b:Z

    if-nez v1, :cond_1

    .line 690
    if-eqz v0, :cond_0

    .line 691
    const-wide/16 v2, 0x1e

    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 692
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/y;->b:Z

    goto :goto_1

    .line 701
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 704
    :catch_0
    move-exception v0

    .line 703
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 694
    :cond_0
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_1

    .line 697
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/y;->a:Z

    if-eqz v0, :cond_3

    .line 698
    monitor-exit p0

    .line 720
    :cond_2
    return-void

    .line 700
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/y;->b:Z

    .line 701
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 706
    const-string v0, "LabelControllerImpl.runLabelPlacement"

    .line 715
    :try_start_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/y;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/y;->c:Lcom/google/android/apps/gmm/map/o/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/q;->h()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    .line 718
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/y;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/y;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method
