.class public Lcom/google/android/apps/gmm/map/internal/c/ae;
.super Lcom/google/android/apps/gmm/map/internal/c/ad;
.source "PG"


# instance fields
.field public final l:F

.field public final m:Z


# direct methods
.method public constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFIF[ILcom/google/android/apps/gmm/map/indoor/d/g;Z)V
    .locals 19

    .prologue
    .line 67
    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    move-object/from16 v13, p10

    move/from16 v14, p11

    move/from16 v15, p12

    move/from16 v16, p13

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    invoke-direct/range {v3 .. v18}, Lcom/google/android/apps/gmm/map/internal/c/ad;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;)V

    .line 82
    move/from16 v0, p14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ae;->l:F

    .line 83
    move/from16 v0, p17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ae;->m:Z

    .line 84
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/ao;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/ae;
    .locals 21

    .prologue
    .line 144
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->e()Lcom/google/maps/b/a/bp;

    move-result-object v2

    .line 146
    invoke-virtual {v2}, Lcom/google/maps/b/a/bp;->a()Lcom/google/maps/b/a/cu;

    move-result-object v3

    iget-object v2, v2, Lcom/google/maps/b/a/bp;->a:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 145
    invoke-static {v3, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Lcom/google/maps/b/a/cu;ILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v9

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->i:Lcom/google/maps/b/a/cz;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v2, :cond_1

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->i:Lcom/google/maps/b/a/cz;

    iget v3, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v4

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v2, v4, v3}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 156
    :goto_0
    const/4 v3, 0x1

    new-array v10, v3, [Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v3, 0x0

    .line 157
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->a()Lcom/google/maps/b/a/al;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v4, v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Lcom/google/maps/b/a/al;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v2

    aput-object v2, v10, v3

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->e()Lcom/google/maps/b/a/bp;

    move-result-object v2

    iget-object v2, v2, Lcom/google/maps/b/a/bp;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v15, v2, v3

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->e:Lcom/google/maps/b/a/cz;

    iget v0, v2, Lcom/google/maps/b/a/cz;->b:I

    move/from16 v16, v0

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->g:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v17, v2, v3

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->k:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v3, -0x80000000

    xor-int v7, v2, v3

    .line 170
    const/16 v19, 0x0

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->q:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_0

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->k()Lcom/google/maps/b/a/ag;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/indoor/d/g;->a(Lcom/google/maps/b/a/ag;)Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-result-object v19

    .line 175
    :cond_0
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/ae;

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->h:Lcom/google/maps/b/a/db;

    iget-wide v4, v2, Lcom/google/maps/b/a/db;->b:J

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->j:Lcom/google/maps/b/a/cz;

    iget v6, v2, Lcom/google/maps/b/a/cz;->b:I

    sget-object v8, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 182
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v11

    const/4 v12, 0x0

    const-string v13, ""

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ao;->f:Lcom/google/maps/b/a/cz;

    iget v14, v2, Lcom/google/maps/b/a/cz;->b:I

    const/4 v2, 0x0

    new-array v0, v2, [I

    move-object/from16 v18, v0

    const/16 v20, 0x1

    invoke-direct/range {v3 .. v20}, Lcom/google/android/apps/gmm/map/internal/c/ae;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFIF[ILcom/google/android/apps/gmm/map/indoor/d/g;Z)V

    return-object v3

    .line 152
    :cond_1
    const-string v2, "LineLabel"

    const-string v3, "LineLabel has no style index"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/z;->a:Lcom/google/android/apps/gmm/map/internal/c/bk;

    goto :goto_0
.end method

.method public static b(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/ay;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/internal/c/ae;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;ZLjava/util/Collection;)V

    .line 140
    return-void
.end method


# virtual methods
.method public final f()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 111
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ae;->m:Z

    if-eqz v2, :cond_3

    .line 112
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ae;->a:I

    .line 113
    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 117
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 113
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 116
    :cond_3
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ae;->a:I

    .line 117
    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_0

    move v0, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 88
    const/16 v0, 0xb

    return v0
.end method
