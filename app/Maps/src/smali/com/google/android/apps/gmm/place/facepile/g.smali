.class public Lcom/google/android/apps/gmm/place/facepile/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/facepile/a/b;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/base/activities/c;

.field c:Lcom/google/maps/g/fk;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/facepile/a/a;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

.field f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/place/facepile/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/facepile/g;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/maps/g/fk;Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;Z)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/place/facepile/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/facepile/h;-><init>(Lcom/google/android/apps/gmm/place/facepile/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->f:Ljava/lang/Object;

    .line 76
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/facepile/g;->c:Lcom/google/maps/g/fk;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    .line 79
    if-eqz p2, :cond_1

    move v4, v6

    .line 80
    :goto_0
    iget-object v0, p2, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 81
    iget-object v0, p2, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fg;->h()Lcom/google/maps/g/fg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/fg;

    .line 85
    iget-boolean v0, p2, Lcom/google/maps/g/fk;->d:Z

    if-eqz v0, :cond_0

    if-nez v4, :cond_0

    const/4 v3, 0x1

    .line 86
    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/gmm/place/facepile/b;

    .line 87
    iget-object v1, p2, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/facepile/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/maps/g/fg;ZII)V

    .line 86
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    move v3, v6

    .line 85
    goto :goto_1

    .line 90
    :cond_1
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/facepile/g;->e:Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

    .line 92
    if-eqz p4, :cond_2

    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/facepile/g;->c()V

    .line 95
    :cond_2
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-static {}, Lcom/google/r/b/a/ahn;->newBuilder()Lcom/google/r/b/a/ahp;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->e:Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

    .line 134
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/r/b/a/ahp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/r/b/a/ahp;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/ahp;->b:Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/facepile/a/a;

    .line 137
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/facepile/a/a;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v1}, Lcom/google/r/b/a/ahp;->c()V

    iget-object v3, v1, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 140
    :cond_3
    invoke-virtual {v1}, Lcom/google/r/b/a/ahp;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ahn;

    .line 142
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/place/facepile/i;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/facepile/i;-><init>(Lcom/google/android/apps/gmm/place/facepile/g;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/facepile/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/facepile/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/facepile/a/a;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 109
    :goto_1
    return-object v0

    .line 104
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 109
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method
