.class public Lcom/google/android/apps/gmm/base/views/j;
.super Landroid/view/animation/Animation;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/j;->c:Landroid/view/View;

    .line 49
    iput p2, p0, Lcom/google/android/apps/gmm/base/views/j;->a:I

    .line 50
    sub-int v0, p3, p2

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/j;->b:I

    .line 51
    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/j;->setDuration(J)V

    .line 52
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/j;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/j;->a:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/j;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/j;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 58
    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method
