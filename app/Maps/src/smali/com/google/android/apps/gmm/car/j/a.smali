.class public Lcom/google/android/apps/gmm/car/j/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/car/j/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/j/a;->a:Ljava/lang/String;

    .line 120
    new-instance v0, Lcom/google/android/apps/gmm/car/j/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/b;-><init>()V

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/car/j/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/c;-><init>()V

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/car/j/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/j/d;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/a;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    .line 76
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 77
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/car/j/a;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Country code is \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v0, "# Showcase config for generic case\nGMM_SHOWCASE_SPEED_UP_FACTOR = 1.0\n# GMM_SHOWCASE_LOCATION = no location, ie. use actual location.\n\n# Categories may be one of: GAS, PARKING, RESTAURANT, CAFE, HOTEL, EMERGENCY\nGMM_SHOWCASE_DESTINATION = hotel\nGMM_SHOWCASE_CATEGORY_SEARCH = CAFE\nGMM_SHOWCASE_CATEGORY_SEARCH = RESTAURANT\nGMM_SHOWCASE_DESTINATION = airport\n"

    .line 91
    const-string v2, "AU"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    const-string v0, "# Sydney showcase\nGMM_SHOWCASE_SPEED_UP_FACTOR = 2.0\nGMM_SHOWCASE_LOCATION = -33.857814,151.205079 # 55 Windmill St The Rocks\n\n# Categories may be one of: GAS, PARKING, RESTAURANT, CAFE, HOTEL, EMERGENCY\nGMM_SHOWCASE_CATEGORY_CAFE_DESTINATION=Badde Manors, 37 Glebe Point Rd, Glebe NSW 2037\nGMM_SHOWCASE_CATEGORY_SEARCH = CAFE\nGMM_SHOWCASE_DESTINATION= Coogee Beach, Coogee, NSW\nGMM_SHOWCASE_CATEGORY_RESTAURANT_DESTINATION=12/55 Harris St, Pyrmont NSW\nGMM_SHOWCASE_CATEGORY_SEARCH = RESTAURANT\nGMM_SHOWCASE_DESTINATION = Taronga Zoo, Mosman, NSW\n"

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :cond_1
    const-string v2, "US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    const-string v0, "# Los Angeles showcase\nGMM_SHOWCASE_SPEED_UP_FACTOR = 2.0\nGMM_SHOWCASE_LOCATION = 34.037855, -118.222562 # Los Angeles\n\n# Categories may be one of: GAS, PARKING, RESTAURANT, CAFE, HOTEL, EMERGENCY\nGMM_SHOWCASE_CATEGORY_CAFE_DESTINATION=LA Cafe, 639 S Spring St, Los Angeles, CA\nGMM_SHOWCASE_CATEGORY_SEARCH = CAFE\nGMM_SHOWCASE_DESTINATION= Hollywood Blvd, Los Angeles\nGMM_SHOWCASE_CATEGORY_RESTAURANT_DESTINATION=Noe restaurant, 251 S Olive St, LA, CA\nGMM_SHOWCASE_CATEGORY_SEARCH = RESTAURANT\nGMM_SHOWCASE_DESTINATION = Santa Monica Beach\n"

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/common/api/o;)Z
    .locals 4
    .param p0    # Lcom/google/android/gms/common/api/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 38
    if-eqz p0, :cond_1

    invoke-interface {p0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/car/a;->c:Lcom/google/android/gms/car/d;

    invoke-interface {v1, p0}, Lcom/google/android/gms/car/d;->a(Lcom/google/android/gms/common/api/o;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    :try_start_0
    sget-object v1, Lcom/google/android/gms/car/a;->d:Lcom/google/android/gms/car/f;

    const-string v2, "car_app_mode"

    const-string v3, "Release"

    invoke-interface {v1, p0, v2, v3}, Lcom/google/android/gms/car/f;->a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 56
    sget-object v2, Lcom/google/android/apps/gmm/car/j/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Car.Settings.Strings.APP_MODE_KEY has value \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    if-eqz v1, :cond_0

    const-string v2, "Showcase"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    .line 43
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/car/j/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 47
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/car/j/a;->a:Ljava/lang/String;

    goto :goto_0
.end method
