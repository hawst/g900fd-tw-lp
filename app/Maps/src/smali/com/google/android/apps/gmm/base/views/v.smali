.class public Lcom/google/android/apps/gmm/base/views/v;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/views/FloatingBar;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/FloatingBar;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/views/FloatingBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    .line 68
    if-eqz p2, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->d:Lcom/google/o/h/a/hv;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->d:Lcom/google/o/h/a/hv;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->e:I

    iput v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->e:I

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->I:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->I:Ljava/lang/CharSequence;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->h:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->f:Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->g:Ljava/lang/String;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->J:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->J:Ljava/lang/CharSequence;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->K:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->K:Ljava/lang/CharSequence;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    iput v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v1, p2, Lcom/google/android/apps/gmm/base/views/FloatingBar;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->i:Ljava/lang/String;

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/FloatingBar;
    .locals 15

    .prologue
    const/4 v3, -0x2

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 228
    :try_start_0
    iget-object v12, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->d:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_0

    iget v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->e:I

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->I:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    :goto_0
    const-string v5, "Icon contentDescription must be set when Icon background is set"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :catchall_0
    move-exception v0

    iput-object v6, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    throw v0

    :cond_2
    move v0, v2

    .line 228
    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->J:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    :goto_1
    const-string v5, "Button1 contentDescription must be set when Button1 background is set"

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->K:Ljava/lang/CharSequence;

    if-eqz v0, :cond_8

    :cond_7
    move v0, v1

    :goto_2
    const-string v5, "Button2 contentDescription must be set when Button2 background is set"

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    invoke-virtual {v12}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/h;->c:I

    const/4 v7, 0x1

    invoke-virtual {v0, v5, v12, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->w:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->m:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/apps/gmm/g;->br:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/WebImageView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    sget v0, Lcom/google/android/apps/gmm/g;->bt:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->dM:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/gmm/g;->av:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    const/4 v0, 0x0

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setClickable(Z)V

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setCursorVisible(Z)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v0, v12}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    sget v0, Lcom/google/android/apps/gmm/g;->af:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v12}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/gmm/g;->W:I

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->u:Landroid/widget/TextView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->u:Landroid/widget/TextView;

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->f:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/gmm/g;->aa:I

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->v:Landroid/widget/TextView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->v:Landroid/widget/TextView;

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->g:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/gmm/g;->ev:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->ew:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->y:Landroid/widget/TextView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->dw:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->z:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/gmm/g;->cO:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->cP:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->B:Landroid/widget/TextView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->cN:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->C:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/gmm/g;->cs:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->ct:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->E:Landroid/widget/TextView;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->cu:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/gmm/g;->cn:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->bU:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->G:Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->aR:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->H:Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->G:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    invoke-virtual {v0, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/gmm/g;->as:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->w:Landroid/view/View;

    sget v0, Lcom/google/android/apps/gmm/g;->H:I

    invoke-virtual {v12, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    invoke-virtual {v0, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v12}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/f;->gd:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->M:Landroid/graphics/drawable/Drawable;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->m:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->L:Landroid/graphics/drawable/Drawable;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->m:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->a:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_f

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    if-eqz v0, :cond_35

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v1

    :goto_3
    if-eqz v0, :cond_a

    :cond_a
    iget-object v13, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->F:Lcom/google/android/search/searchplate/SearchPlate;

    if-eqz v13, :cond_b

    iget-object v13, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->F:Lcom/google/android/search/searchplate/SearchPlate;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Lcom/google/android/search/searchplate/SearchPlate;->setVisibility(I)V

    :cond_b
    iget-object v13, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_15

    move v0, v2

    :goto_4
    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v13, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    if-eqz v11, :cond_16

    move v0, v2

    :goto_5
    invoke-virtual {v13, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v11, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    if-eqz v10, :cond_17

    move v0, v2

    :goto_6
    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v10, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    if-eqz v9, :cond_18

    move v0, v2

    :goto_7
    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v10, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    if-eqz v8, :cond_19

    move v0, v2

    :goto_8
    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v8, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    if-eqz v7, :cond_1a

    move v0, v2

    :goto_9
    invoke-virtual {v8, v0}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v7, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    if-nez v9, :cond_c

    if-eqz v5, :cond_1b

    :cond_c
    move v0, v2

    :goto_a
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v7, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->G:Landroid/view/View;

    if-eqz v5, :cond_1c

    move v0, v2

    :goto_b
    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->H:Landroid/view/View;

    if-eqz v5, :cond_1d

    move v0, v2

    :goto_c
    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->d:Lcom/google/o/h/a/hv;

    if-eqz v0, :cond_2b

    iget-object v8, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->d:Lcom/google/o/h/a/hv;

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    if-eqz v8, :cond_1f

    iget v4, v8, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_1e

    move v4, v1

    :goto_d
    if-eqz v4, :cond_1f

    iget v4, v8, Lcom/google/o/h/a/hv;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v7, v4

    :goto_e
    if-eqz v8, :cond_21

    iget v4, v8, Lcom/google/o/h/a/hv;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_20

    move v4, v1

    :goto_f
    if-eqz v4, :cond_21

    iget v4, v8, Lcom/google/o/h/a/hv;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v5, v4

    :goto_10
    if-eqz v0, :cond_d

    if-nez v8, :cond_22

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/p;->a(Lcom/google/android/apps/gmm/base/views/WebImageView;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setVisibility(I)V

    :cond_d
    :goto_11
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget-object v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->I:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_12
    iget-boolean v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->c:Z

    if-eqz v0, :cond_2d

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setVisibility(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_13
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2e

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->J:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_14
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_e

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2f

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/w;->c:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v0, v1, :cond_2f

    :cond_e
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_15
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_30

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    iget-object v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    iget-object v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->K:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 229
    :goto_16
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    iput-object v6, p0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    return-object v0

    .line 228
    :cond_f
    :try_start_2
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_10

    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v1

    move v0, v2

    goto/16 :goto_3

    :cond_10
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->c:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_11

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    if-eqz v0, :cond_34

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    move v5, v2

    move v7, v1

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_11
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_12

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->h:Ljava/lang/String;

    if-eqz v0, :cond_33

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->z:Landroid/widget/TextView;

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v1

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_12
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->e:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_13

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->i:Ljava/lang/String;

    if-eqz v0, :cond_32

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->C:Landroid/widget/TextView;

    iget-object v5, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->i:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v1

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_13
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->f:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_14

    move v5, v2

    move v7, v2

    move v8, v1

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_14
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v0, v5, :cond_31

    move v5, v1

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_15
    move v0, v4

    goto/16 :goto_4

    :cond_16
    move v0, v4

    goto/16 :goto_5

    :cond_17
    move v0, v4

    goto/16 :goto_6

    :cond_18
    move v0, v4

    goto/16 :goto_7

    :cond_19
    move v0, v4

    goto/16 :goto_8

    :cond_1a
    move v0, v4

    goto/16 :goto_9

    :cond_1b
    move v0, v4

    goto/16 :goto_a

    :cond_1c
    move v0, v4

    goto/16 :goto_b

    :cond_1d
    move v0, v4

    goto/16 :goto_c

    :cond_1e
    move v4, v2

    goto/16 :goto_d

    :cond_1f
    move-object v7, v6

    goto/16 :goto_e

    :cond_20
    move v4, v2

    goto/16 :goto_f

    :cond_21
    move-object v5, v6

    goto/16 :goto_10

    :cond_22
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setVisibility(I)V

    iget v4, v8, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v9, 0x2

    if-ne v4, v9, :cond_24

    move v4, v1

    :goto_17
    if-eqz v4, :cond_27

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-eqz v7, :cond_25

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_18
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eqz v5, :cond_26

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_19
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v8}, Lcom/google/o/h/a/hv;->d()Ljava/lang/String;

    move-result-object v1

    iget v2, v8, Lcom/google/o/h/a/hv;->d:I

    invoke-static {v2}, Lcom/google/o/h/a/ia;->a(I)Lcom/google/o/h/a/ia;

    move-result-object v2

    if-nez v2, :cond_23

    sget-object v2, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    :cond_23
    invoke-static {v2}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/o/h/a/ia;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->bw:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/views/WebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/WebImageView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/util/webimageview/g;I)V

    goto/16 :goto_11

    :cond_24
    move v4, v2

    goto :goto_17

    :cond_25
    move v1, v3

    goto :goto_18

    :cond_26
    move v1, v3

    goto :goto_19

    :cond_27
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/4 v4, -0x2

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, -0x2

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v3, v8, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_28

    :goto_1a
    if-eqz v1, :cond_29

    iget v1, v8, Lcom/google/o/h/a/hv;->b:I

    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/e/d;->b(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1b
    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setImageResource(I)V

    goto/16 :goto_11

    :cond_28
    move v1, v2

    goto :goto_1a

    :cond_29
    move-object v1, v6

    goto :goto_1b

    :cond_2a
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/p;->a(Lcom/google/android/apps/gmm/base/views/WebImageView;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setVisibility(I)V

    goto/16 :goto_11

    :cond_2b
    iget v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->e:I

    if-eqz v0, :cond_2c

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setVisibility(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setImageResource(I)V

    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget-object v1, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->I:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_12

    :cond_2c
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setVisibility(I)V

    goto/16 :goto_12

    :cond_2d
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_13

    :cond_2e
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_14

    :cond_2f
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_15

    :cond_30
    iget-object v0, v12, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_16

    :cond_31
    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_32
    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v1

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_33
    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v1

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_34
    move v5, v2

    move v7, v1

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v2

    goto/16 :goto_3

    :cond_35
    move v5, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    move v0, v1

    goto/16 :goto_3
.end method
