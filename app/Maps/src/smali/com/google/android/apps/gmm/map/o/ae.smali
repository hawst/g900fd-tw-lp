.class Lcom/google/android/apps/gmm/map/o/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/o/ae;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/google/android/apps/gmm/map/o/d;

.field b:I

.field c:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    .line 234
    iput v0, p0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    .line 235
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/ae;->c:Z

    .line 236
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/o/ae;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 244
    iget v3, p0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    if-eq v3, v4, :cond_3

    .line 245
    iget v3, p0, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/ae;->b:I

    if-ge v3, v4, :cond_1

    .line 258
    :cond_0
    :goto_0
    return v0

    .line 245
    :cond_1
    if-le v3, v4, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 248
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o/d;->i()Lcom/google/android/apps/gmm/map/t/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/t/l;->ordinal()I

    move-result v3

    .line 249
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o/d;->i()Lcom/google/android/apps/gmm/map/t/l;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/t/l;->ordinal()I

    move-result v4

    .line 250
    if-eq v3, v4, :cond_5

    .line 251
    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    .line 254
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v3

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v4

    if-eq v3, v4, :cond_7

    .line 255
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v3

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/o/ae;->a:Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v4

    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0

    .line 258
    :cond_7
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    if-lt v3, v4, :cond_0

    if-le v3, v4, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    move v0, v2

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 215
    check-cast p1, Lcom/google/android/apps/gmm/map/o/ae;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/o/ae;->a(Lcom/google/android/apps/gmm/map/o/ae;)I

    move-result v0

    return v0
.end method
