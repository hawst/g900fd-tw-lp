.class public Lcom/google/android/apps/gmm/map/l/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[F

.field public final b:[I

.field public final c:Lcom/google/android/apps/gmm/v/cn;

.field public final d:Lcom/google/android/apps/gmm/v/cn;


# direct methods
.method public constructor <init>([F[ILcom/google/android/apps/gmm/v/cn;Lcom/google/android/apps/gmm/v/cn;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const-string v0, "vertices"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 98
    :cond_0
    const-string v0, "edgeIndices"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    :cond_1
    invoke-virtual {p1}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/a;->a:[F

    .line 100
    invoke-virtual {p2}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/l/a;->b:[I

    .line 101
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/l/a;->c:Lcom/google/android/apps/gmm/v/cn;

    .line 102
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/l/a;->d:Lcom/google/android/apps/gmm/v/cn;

    .line 103
    return-void
.end method
