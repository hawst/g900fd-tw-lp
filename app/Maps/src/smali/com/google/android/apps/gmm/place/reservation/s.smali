.class public Lcom/google/android/apps/gmm/place/reservation/s;
.super Lcom/google/android/apps/gmm/place/reservation/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/h;
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/place/reservation/a;",
        "Lcom/google/android/apps/gmm/place/reservation/b/h;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/r;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field final c:Lcom/google/android/apps/gmm/place/reservation/x;

.field final d:Lcom/google/android/apps/gmm/place/reservation/x;

.field final e:Lcom/google/android/apps/gmm/place/reservation/x;

.field final f:Lcom/google/android/apps/gmm/place/reservation/x;

.field g:Z

.field h:Z

.field i:Z

.field private final k:Lcom/google/android/apps/gmm/base/g/c;

.field private final l:Lcom/google/maps/g/q;

.field private final m:Lcom/google/r/b/a/j;

.field private final n:Lcom/google/android/apps/gmm/place/reservation/b/i;

.field private final o:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/n;",
            "Lcom/google/r/b/a/r;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/google/android/apps/gmm/z/b/l;

.field private q:Lcom/google/android/apps/gmm/base/l/a/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/place/reservation/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/reservation/s;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/place/reservation/ReservationDetailsFragment;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;)V
    .locals 5

    .prologue
    const/16 v3, 0x2061

    .line 85
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/place/reservation/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)V

    .line 86
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 87
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->k:Lcom/google/android/apps/gmm/base/g/c;

    .line 88
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->l:Lcom/google/maps/g/q;

    .line 90
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->m:Lcom/google/r/b/a/j;

    .line 91
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1, p3}, Lcom/google/android/apps/gmm/place/reservation/y;-><init>(Landroid/content/Context;Lcom/google/r/b/a/j;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->n:Lcom/google/android/apps/gmm/place/reservation/b/i;

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->ls:I

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/x;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->lt:I

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/x;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->lr:I

    const/16 v3, 0x21

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/x;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    .line 102
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->lu:I

    const/4 v3, 0x3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/x;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v1, Lcom/google/r/b/a/n;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->o:Lcom/google/android/apps/gmm/shared/net/b;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->o:Lcom/google/android/apps/gmm/shared/net/b;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/base/l/ao;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->ca:I

    .line 110
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/reservation/u;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/reservation/u;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;)V

    .line 111
    iput-object v2, v1, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->kp:I

    .line 119
    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    new-instance v3, Lcom/google/android/apps/gmm/place/reservation/t;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/place/reservation/t;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;)V

    .line 120
    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 126
    new-instance v3, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 117
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/base/l/ao;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->q:Lcom/google/android/apps/gmm/base/l/a/ab;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->l:Lcom/google/maps/g/q;

    sget-object v1, Lcom/google/b/f/t;->dV:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->p:Lcom/google/android/apps/gmm/z/b/l;

    .line 131
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->i:Z

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 324
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 325
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->jE:I

    new-instance v2, Lcom/google/android/apps/gmm/place/reservation/v;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/reservation/v;-><init>(Lcom/google/android/apps/gmm/place/reservation/s;)V

    .line 326
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 332
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 333
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 334
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 335
    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 336
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 337
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 52
    check-cast p1, Lcom/google/r/b/a/r;

    iput-boolean v5, p0, Lcom/google/android/apps/gmm/place/reservation/s;->h:Z

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->kG:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/reservation/s;->a(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/d;->d()Lcom/google/r/b/a/d;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/d;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/d;

    iget v1, v0, Lcom/google/r/b/a/d;->b:I

    invoke-static {v1}, Lcom/google/r/b/a/g;->a(I)Lcom/google/r/b/a/g;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/r/b/a/g;->a:Lcom/google/r/b/a/g;

    move-object v3, v1

    :goto_3
    iget-object v1, v0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_4

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_4
    sget-object v1, Lcom/google/android/apps/gmm/place/reservation/w;->a:[I

    invoke-virtual {v3}, Lcom/google/r/b/a/g;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v3, v1

    goto :goto_3

    :cond_4
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    iput-object v2, v0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    :cond_5
    move-object v0, v2

    goto :goto_4

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    goto :goto_2

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    invoke-virtual {p1}, Lcom/google/r/b/a/r;->d()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/reservation/s;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->k:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->m:Lcom/google/r/b/a/j;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/reservation/ReservationConfirmationFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->m:Lcom/google/r/b/a/j;

    invoke-virtual {v0}, Lcom/google/r/b/a/j;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    if-nez v4, :cond_c

    sget-object v1, Lcom/google/android/apps/gmm/place/reservation/s;->j:Ljava/lang/String;

    const-string v2, "failed to parse reservation time: "

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->m:Lcom/google/r/b/a/j;

    invoke-virtual {v0}, Lcom/google/r/b/a/j;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/16 v5, 0x13

    invoke-static {v0, v2, v3, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->F()Lcom/google/android/apps/gmm/place/reservation/ai;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/ai;->a:Lcom/google/android/apps/gmm/place/reservation/m;

    invoke-static {}, Lcom/google/maps/g/oy;->newBuilder()Lcom/google/maps/g/pa;

    move-result-object v3

    invoke-static {}, Lcom/google/maps/g/fc;->newBuilder()Lcom/google/maps/g/fe;

    move-result-object v5

    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget v6, v5, Lcom/google/maps/g/fe;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, v5, Lcom/google/maps/g/fe;->a:I

    iput-object v2, v5, Lcom/google/maps/g/fe;->b:Ljava/lang/Object;

    iget-object v2, v3, Lcom/google/maps/g/pa;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/maps/g/fe;->g()Lcom/google/n/t;

    move-result-object v5

    iget-object v6, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v5, 0x0

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v2, v3, Lcom/google/maps/g/pa;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v3, Lcom/google/maps/g/pa;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->m:Lcom/google/r/b/a/j;

    iget v2, v2, Lcom/google/r/b/a/j;->b:I

    iget v5, v3, Lcom/google/maps/g/pa;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/maps/g/pa;->a:I

    iput v2, v3, Lcom/google/maps/g/pa;->c:I

    invoke-virtual {v3}, Lcom/google/maps/g/pa;->g()Lcom/google/n/t;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/oy;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->k:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/reservation/m;->a(Landroid/accounts/Account;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/maps/g/oy;J)V

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c()Lcom/google/android/apps/gmm/place/reservation/b/i;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->n:Lcom/google/android/apps/gmm/place/reservation/b/i;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lq:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/reservation/s;->l:Lcom/google/maps/g/q;

    invoke-virtual {v4}, Lcom/google/maps/g/q;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/place/reservation/b/d;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/place/reservation/b/d;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/place/reservation/b/d;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/place/reservation/b/d;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/base/l/a/ab;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->q:Lcom/google/android/apps/gmm/base/l/a/ab;

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "https://support.google.com/gmm/?p=book_table"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 182
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 210
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->h:Z

    if-eqz v2, :cond_1

    .line 211
    sget-object v1, Lcom/google/android/apps/gmm/place/reservation/s;->j:Ljava/lang/String;

    const-string v2, "The confirm button should be disabled when a request is pending"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 232
    :cond_0
    :goto_0
    return-object v5

    .line 216
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2, v5}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 218
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v5, v2, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v5, v2, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v5, v2, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    iput-object v5, v2, Lcom/google/android/apps/gmm/place/reservation/x;->b:Ljava/lang/String;

    .line 219
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->g:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/reservation/x;->e()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/reservation/x;->e()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/reservation/x;->e()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/reservation/x;->e()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    if-nez v0, :cond_3

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 224
    :cond_3
    invoke-static {}, Lcom/google/r/b/a/n;->newBuilder()Lcom/google/r/b/a/p;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->l:Lcom/google/maps/g/q;

    .line 225
    iget-object v0, v0, Lcom/google/maps/g/q;->g:Lcom/google/n/f;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v3, v2, Lcom/google/r/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/r/b/a/p;->a:I

    iput-object v0, v2, Lcom/google/r/b/a/p;->b:Lcom/google/n/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->m:Lcom/google/r/b/a/j;

    .line 226
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-object v3, v2, Lcom/google/r/b/a/p;->c:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/p;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Lcom/google/r/b/a/p;->a:I

    .line 227
    invoke-static {}, Lcom/google/r/b/a/ah;->newBuilder()Lcom/google/r/b/a/aj;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->c:Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    if-nez v3, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v4, v0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/aj;->a:I

    iput-object v3, v0, Lcom/google/r/b/a/aj;->b:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->d:Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    if-nez v3, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v4, v0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/r/b/a/aj;->a:I

    iput-object v3, v0, Lcom/google/r/b/a/aj;->c:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->e:Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    if-nez v3, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v4, v0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/aj;->a:I

    iput-object v3, v0, Lcom/google/r/b/a/aj;->d:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/s;->f:Lcom/google/android/apps/gmm/place/reservation/x;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/reservation/x;->a:Ljava/lang/String;

    if-nez v3, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget v4, v0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, v0, Lcom/google/r/b/a/aj;->a:I

    iput-object v3, v0, Lcom/google/r/b/a/aj;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/r/b/a/aj;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ah;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget-object v3, v2, Lcom/google/r/b/a/p;->d:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/p;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v2, Lcom/google/r/b/a/p;->a:I

    .line 228
    invoke-virtual {v2}, Lcom/google/r/b/a/p;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/n;

    .line 229
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/s;->o:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 230
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/reservation/s;->h:Z

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/a;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0
.end method

.method public final n()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/s;->p:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
