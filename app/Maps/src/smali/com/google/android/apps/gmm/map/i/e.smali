.class public Lcom/google/android/apps/gmm/map/i/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/google/android/apps/gmm/map/t/k;

.field c:I

.field d:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final e:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

.field private final f:Landroid/content/res/Resources;

.field private g:Lcom/google/android/apps/gmm/map/i/f;

.field private h:Lcom/google/android/apps/gmm/map/i/g;

.field private i:Lcom/google/android/apps/gmm/map/t/r;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->a:Ljava/lang/String;

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/i/e;->c:I

    .line 40
    sget-object v0, Lcom/google/android/apps/gmm/map/i/f;->b:Lcom/google/android/apps/gmm/map/i/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->g:Lcom/google/android/apps/gmm/map/i/f;

    .line 43
    sget-object v0, Lcom/google/android/apps/gmm/map/i/g;->b:Lcom/google/android/apps/gmm/map/i/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->h:Lcom/google/android/apps/gmm/map/i/g;

    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/map/t/r;->a:Lcom/google/android/apps/gmm/map/t/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->i:Lcom/google/android/apps/gmm/map/t/r;

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/i/e;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/i/e;->f:Landroid/content/res/Resources;

    .line 51
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/t/q;
    .locals 12

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->b:Lcom/google/android/apps/gmm/map/t/k;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 91
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/i/e;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 93
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->g:Lcom/google/android/apps/gmm/map/i/f;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 94
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/e;->h:Lcom/google/android/apps/gmm/map/i/g;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 96
    :cond_6
    new-instance v0, Lcom/google/android/apps/gmm/map/t/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/e;->b:Lcom/google/android/apps/gmm/map/t/k;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    .line 98
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;-><init>()V

    .line 99
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->c:Z

    .line 100
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/e;->f:Landroid/content/res/Resources;

    iget v3, p0, Lcom/google/android/apps/gmm/map/i/e;->c:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/i/e;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    .line 102
    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a:I

    int-to-float v2, v2

    .line 103
    iget v3, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->b:I

    int-to-float v3, v3

    .line 104
    iget v4, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->d:F

    .line 105
    iget v5, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->e:F

    .line 106
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/i/e;->g:Lcom/google/android/apps/gmm/map/i/f;

    iget v6, v6, Lcom/google/android/apps/gmm/map/i/f;->d:F

    .line 107
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/i/e;->h:Lcom/google/android/apps/gmm/map/i/g;

    iget v7, v7, Lcom/google/android/apps/gmm/map/i/g;->d:F

    .line 108
    new-instance v8, Lcom/google/android/apps/gmm/v/av;

    const/16 v9, 0x14

    new-array v9, v9, [F

    const/4 v10, 0x0

    neg-float v11, v6

    mul-float/2addr v11, v2

    aput v11, v9, v10

    const/4 v10, 0x1

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v7

    mul-float/2addr v11, v3

    aput v11, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x0

    aput v11, v9, v10

    const/4 v10, 0x3

    const/4 v11, 0x0

    aput v11, v9, v10

    const/4 v10, 0x4

    const/4 v11, 0x0

    aput v11, v9, v10

    const/4 v10, 0x5

    neg-float v11, v6

    mul-float/2addr v11, v2

    aput v11, v9, v10

    const/4 v10, 0x6

    neg-float v11, v7

    mul-float/2addr v11, v3

    aput v11, v9, v10

    const/4 v10, 0x7

    const/4 v11, 0x0

    aput v11, v9, v10

    const/16 v10, 0x8

    const/4 v11, 0x0

    aput v11, v9, v10

    const/16 v10, 0x9

    aput v5, v9, v10

    const/16 v10, 0xa

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v6

    mul-float/2addr v11, v2

    aput v11, v9, v10

    const/16 v10, 0xb

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v7

    mul-float/2addr v11, v3

    aput v11, v9, v10

    const/16 v10, 0xc

    const/4 v11, 0x0

    aput v11, v9, v10

    const/16 v10, 0xd

    aput v4, v9, v10

    const/16 v10, 0xe

    const/4 v11, 0x0

    aput v11, v9, v10

    const/16 v10, 0xf

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float v6, v11, v6

    mul-float/2addr v2, v6

    aput v2, v9, v10

    const/16 v2, 0x10

    neg-float v6, v7

    mul-float/2addr v3, v6

    aput v3, v9, v2

    const/16 v2, 0x11

    const/4 v3, 0x0

    aput v3, v9, v2

    const/16 v2, 0x12

    aput v4, v9, v2

    const/16 v2, 0x13

    aput v5, v9, v2

    const/16 v2, 0x11

    const/4 v3, 0x5

    invoke-direct {v8, v9, v2, v3}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    .line 116
    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 117
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 118
    new-instance v1, Lcom/google/android/apps/gmm/v/cd;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/v/cd;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 119
    new-instance v1, Lcom/google/android/apps/gmm/v/m;

    const/4 v2, 0x1

    const/16 v3, 0x303

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/e;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/e;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_7
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v1, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 122
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(F)V

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/e;->i:Lcom/google/android/apps/gmm/map/t/r;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v2, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 125
    return-object v0
.end method
