.class public Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;
.super Landroid/view/View;
.source "PG"


# static fields
.field private static final f:Landroid/graphics/Paint;

.field private static g:Landroid/graphics/Path;


# instance fields
.field public final a:I

.field public final b:F

.field public final c:F

.field public final d:I

.field public final e:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 62
    sput-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->e:Landroid/content/res/Resources;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/e;->ac:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->d:I

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/e;->ae:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->a:I

    .line 76
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->a:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->b:F

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/e;->ad:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->c:F

    .line 78
    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Canvas;FFFI)V
    .locals 5

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->getWidth()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    .line 152
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->d:I

    int-to-float v0, v0

    sub-float/2addr v0, p4

    add-float/2addr v0, p2

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->b:F

    sub-float/2addr v0, v2

    .line 155
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->b:F

    add-float/2addr v2, p3

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 156
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->b:F

    sget-object v4, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 157
    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->d:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    goto :goto_0

    .line 159
    :cond_0
    return-void
.end method

.method protected final a(Landroid/graphics/Canvas;FFI)V
    .locals 6

    .prologue
    .line 85
    if-eqz p4, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 87
    iget v1, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->a:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v1, v2

    .line 88
    int-to-float v1, v0

    sub-float/2addr v1, v2

    .line 89
    int-to-float v0, v0

    add-float v3, v0, v2

    .line 92
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 95
    :cond_0
    return-void
.end method

.method protected final a(Landroid/graphics/Canvas;FFII)V
    .locals 7

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 102
    add-float v0, p2, p3

    div-float/2addr v0, v4

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->a:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->c:F

    div-float/2addr v3, v4

    int-to-float v4, v1

    sub-float/2addr v4, v2

    int-to-float v1, v1

    add-float/2addr v1, v2

    if-eqz p4, :cond_0

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {v5, p4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->rewind()V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5, v4, p2}, Landroid/graphics/Path;->moveTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5, v1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    sub-float v6, v0, v2

    sub-float/2addr v6, v3

    invoke-virtual {v5, v1, v6}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    add-float v6, v0, v2

    sub-float/2addr v6, v3

    invoke-virtual {v5, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5, v4, p2}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    sget-object v6, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    if-eqz p5, :cond_1

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {v5, p5}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->rewind()V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5, v4, p3}, Landroid/graphics/Path;->moveTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v5, v1, p3}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v5, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    sub-float v6, v0, v2

    add-float/2addr v6, v3

    invoke-virtual {v5, v1, v6}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v1, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    add-float/2addr v0, v2

    add-float/2addr v0, v3

    invoke-virtual {v1, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    invoke-virtual {v0, v4, p3}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->g:Landroid/graphics/Path;

    sget-object v1, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 104
    :cond_1
    return-void
.end method

.method protected final b(Landroid/graphics/Canvas;FFI)V
    .locals 2

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 146
    int-to-float v0, v0

    sget-object v1, Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, p2, p3, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 147
    return-void
.end method
