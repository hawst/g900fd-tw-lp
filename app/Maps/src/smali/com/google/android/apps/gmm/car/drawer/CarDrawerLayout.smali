.class public Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field static final a:[I


# instance fields
.field public final b:Landroid/support/v4/widget/bk;

.field public final c:Ljava/lang/Runnable;

.field final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/car/drawer/f;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/os/Handler;

.field final f:Lcom/google/android/apps/gmm/car/drawer/c;

.field final g:Landroid/view/ViewGroup;

.field final h:Landroid/widget/FrameLayout;

.field i:Z

.field public j:I

.field private final k:Landroid/graphics/Paint;

.field private final l:Lcom/google/android/apps/gmm/car/drawer/d;

.field private final m:Lcom/google/android/gms/car/support/bc;

.field private final n:Lcom/google/android/gms/car/support/bc;

.field private final o:Lcom/google/android/gms/car/support/bc;

.field private final p:Lcom/google/android/gms/car/support/bc;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/car/drawer/c;Landroid/view/ViewGroup;Landroid/widget/FrameLayout;)V
    .locals 10

    .prologue
    const v4, 0x3edc28f6    # 0.43f

    const v9, 0x3e8f5c29    # 0.28f

    const/4 v8, 0x0

    const/high16 v7, 0x3e800000    # 0.25f

    const v6, 0x3e0f5c29    # 0.14f

    .line 126
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->k:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/d;-><init>(Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->l:Lcom/google/android/apps/gmm/car/drawer/d;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/a;-><init>(Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->c:Ljava/lang/Runnable;

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e:Landroid/os/Handler;

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->r:Z

    .line 88
    const v0, -0xff01

    iput v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    .line 128
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/car/drawer/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->f:Lcom/google/android/apps/gmm/car/drawer/c;

    .line 130
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->addView(Landroid/view/View;)V

    .line 132
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->addView(Landroid/view/View;)V

    .line 135
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/b;-><init>(Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 147
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->d:Ljava/util/Set;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 150
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    .line 152
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->l:Lcom/google/android/apps/gmm/car/drawer/d;

    invoke-static {p0, v1, v2}, Landroid/support/v4/widget/bk;->a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/bn;)Landroid/support/v4/widget/bk;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/bk;->a(F)V

    .line 155
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v4/view/bl;->a(Landroid/view/ViewGroup;Z)V

    .line 157
    const-string v0, "drawer_shadow"

    invoke-static {p1, v0}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    .line 161
    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->a:[F

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v1, v2, v7, v7, v3}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->a:[F

    invoke-direct {v2, v3, v4, v6, v4}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->m:Lcom/google/android/gms/car/support/bc;

    .line 166
    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->c:[F

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-direct {v1, v2, v8, v7, v3}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->b:[F

    const v4, 0x3e947ae1    # 0.29f

    const v5, 0x3f11eb85    # 0.57f

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->n:Lcom/google/android/gms/car/support/bc;

    .line 170
    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->a:[F

    const/high16 v3, 0x3f200000    # 0.625f

    const/high16 v4, 0x3e000000    # 0.125f

    invoke-direct {v1, v2, v3, v7, v4}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->c:[F

    const v4, 0x3f147ae1    # 0.58f

    invoke-direct {v2, v3, v4, v6, v9}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->o:Lcom/google/android/gms/car/support/bc;

    .line 175
    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->b:[F

    const/high16 v3, 0x3f200000    # 0.625f

    const/high16 v4, 0x3ec00000    # 0.375f

    invoke-direct {v1, v2, v3, v4, v8}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->c:[F

    const v4, 0x3f147ae1    # 0.58f

    invoke-direct {v2, v3, v4, v6, v9}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->p:Lcom/google/android/gms/car/support/bc;

    .line 180
    return-void
.end method

.method private f()Landroid/view/View;
    .locals 2

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    if-eqz v0, :cond_0

    .line 495
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->o:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/bc;->b(F)F

    move-result v0

    .line 499
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    .line 500
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 501
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 500
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 497
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->o:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/bc;->a(F)F

    move-result v0

    goto :goto_0

    .line 503
    :cond_1
    return-void
.end method

.method a(I)Z
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->a:I

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    .line 273
    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    .line 666
    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/FrameLayout;->addFocusables(Ljava/util/ArrayList;II)V

    .line 671
    :goto_0
    return-void

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 507
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    if-eqz v0, :cond_0

    .line 508
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->n:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/bc;->b(F)F

    move-result v0

    sub-float v0, v2, v0

    .line 512
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 513
    return-void

    .line 510
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->n:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/bc;->a(F)F

    move-result v0

    sub-float v0, v2, v0

    goto :goto_0
.end method

.method c()V
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    .line 529
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    if-eqz v0, :cond_0

    .line 530
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->m:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/bc;->b(F)F

    move-result v0

    move v1, v0

    .line 534
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/f;

    .line 536
    iget v3, v0, Lcom/google/android/apps/gmm/car/drawer/f;->b:I

    .line 537
    iget v4, v0, Lcom/google/android/apps/gmm/car/drawer/f;->c:I

    .line 538
    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    .line 539
    invoke-static {v4}, Landroid/graphics/Color;->alpha(I)I

    move-result v6

    .line 538
    sub-float v7, v10, v1

    int-to-float v5, v5

    mul-float/2addr v5, v7

    int-to-float v6, v6

    mul-float/2addr v6, v1

    add-float/2addr v5, v6

    float-to-int v5, v5

    .line 540
    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v6

    .line 541
    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v7

    .line 540
    sub-float v8, v10, v1

    int-to-float v6, v6

    mul-float/2addr v6, v8

    int-to-float v7, v7

    mul-float/2addr v7, v1

    add-float/2addr v6, v7

    float-to-int v6, v6

    .line 542
    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v7

    .line 543
    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v8

    .line 542
    sub-float v9, v10, v1

    int-to-float v7, v7

    mul-float/2addr v7, v9

    int-to-float v8, v8

    mul-float/2addr v8, v1

    add-float/2addr v7, v8

    float-to-int v7, v7

    .line 544
    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 545
    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    .line 544
    sub-float v8, v10, v1

    int-to-float v3, v3

    mul-float/2addr v3, v8

    int-to-float v4, v4

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 546
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/f;->a:Lcom/google/android/apps/gmm/car/drawer/e;

    shl-int/lit8 v4, v5, 0x18

    shl-int/lit8 v5, v6, 0x10

    or-int/2addr v4, v5

    shl-int/lit8 v5, v7, 0x8

    or-int/2addr v4, v5

    or-int/2addr v3, v4

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/car/drawer/e;->a(I)V

    goto :goto_1

    .line 532
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->m:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/bc;->a(F)F

    move-result v0

    move v1, v0

    goto :goto_0

    .line 548
    :cond_1
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 712
    instance-of v0, p1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/bk;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    invoke-static {p0}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    .line 401
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 606
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    .line 608
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->s:Z

    if-eqz v0, :cond_2

    .line 609
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    .line 611
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    .line 610
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;II)Z

    .line 622
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    const/high16 v1, 0x60000

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setDescendantFocusability(I)V

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_0

    .line 627
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 629
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    .line 630
    return-void

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    neg-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    .line 614
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    .line 613
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;II)Z

    goto :goto_0

    .line 617
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 618
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    .line 619
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 194
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    .line 195
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 215
    :goto_0
    return v0

    .line 200
    :cond_0
    const/16 v0, 0x16

    if-eq v4, v0, :cond_1

    const/4 v0, 0x2

    if-ne v4, v0, :cond_6

    :cond_1
    if-ne v3, v1, :cond_6

    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    move v0, v1

    .line 203
    goto :goto_0

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 207
    goto :goto_0

    .line 209
    :cond_3
    const/16 v0, 0x15

    if-eq v4, v0, :cond_4

    if-ne v4, v1, :cond_6

    .line 210
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    move v0, v1

    :goto_1
    if-nez v0, :cond_6

    if-ne v3, v1, :cond_6

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->d()V

    move v0, v1

    .line 212
    goto :goto_0

    :cond_5
    move v0, v2

    .line 210
    goto :goto_1

    :cond_6
    move v0, v2

    .line 215
    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 11

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getHeight()I

    move-result v4

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v1

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRight()I

    move-result v2

    .line 418
    iget v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    const/high16 v3, -0x1000000

    and-int/2addr v0, v3

    ushr-int/lit8 v5, v0, 0x18

    .line 420
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    if-ne p2, v0, :cond_6

    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getChildCount()I

    move-result v7

    .line 423
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_5

    .line 424
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 425
    if-eq v8, p2, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 426
    invoke-virtual {v8}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    const/4 v9, -0x1

    if-ne v0, v9, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    if-ne v8, v0, :cond_4

    .line 427
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lt v0, v4, :cond_4

    .line 428
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getRight()I

    move-result v0

    .line 433
    if-le v0, v1, :cond_b

    :goto_2
    move v1, v0

    move v0, v2

    .line 423
    :cond_0
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 426
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 437
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v0

    .line 438
    if-lt v0, v2, :cond_0

    :cond_4
    move v0, v2

    goto :goto_3

    .line 443
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getHeight()I

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_6
    move v3, v2

    .line 445
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v7

    .line 446
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    if-ne p2, v0, :cond_8

    .line 450
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    if-eqz v0, :cond_9

    .line 451
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->p:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/support/bc;->b(F)F

    move-result v0

    .line 455
    :goto_4
    int-to-float v2, v5

    mul-float/2addr v0, v2

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 457
    if-lez v0, :cond_7

    .line 458
    shl-int/lit8 v0, v0, 0x18

    iget v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    const v4, 0xffffff

    and-int/2addr v2, v4

    or-int/2addr v0, v2

    .line 459
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 461
    int-to-float v1, v1

    const/4 v2, 0x0

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 464
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    const/4 v0, 0x3

    .line 465
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 467
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 468
    const/4 v2, 0x0

    int-to-float v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    .line 469
    invoke-virtual {v4}, Landroid/support/v4/widget/bk;->a()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 470
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v0, v1

    .line 471
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    .line 470
    invoke-virtual {v3, v1, v4, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 489
    :cond_8
    :goto_5
    return v7

    .line 453
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->p:Lcom/google/android/gms/car/support/bc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/support/bc;->a(F)F

    move-result v0

    goto :goto_4

    .line 474
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_8

    const/4 v0, 0x5

    .line 475
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 477
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v1

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    .line 479
    const/4 v3, 0x0

    int-to-float v2, v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    .line 480
    invoke-virtual {v4}, Landroid/support/v4/widget/bk;->a()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 481
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getWidth()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v5

    .line 482
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getWidth()I

    move-result v6

    sub-int v1, v6, v1

    add-int/2addr v0, v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 481
    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 484
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 485
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_5

    :cond_b
    move v0, v1

    goto/16 :goto_2
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 661
    :goto_0
    return-void

    .line 640
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    .line 642
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->s:Z

    if-eqz v0, :cond_2

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;II)Z

    .line 650
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    const/high16 v1, 0x60000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setDescendantFocusability(I)V

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 653
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 655
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 656
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 657
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 660
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    goto :goto_0

    .line 645
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 646
    iput v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    .line 647
    iput-boolean v3, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    goto :goto_1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 697
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 717
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 703
    instance-of v0, p1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    check-cast p1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;-><init>(Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 284
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->r:Z

    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->s:Z

    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->requestLayout()V

    .line 290
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 278
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->s:Z

    .line 280
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/bk;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 567
    :goto_0
    return v0

    .line 560
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    .line 562
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/widget/bk;->b(II)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 563
    goto :goto_0

    .line 567
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 726
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->f()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 727
    invoke-static {p2}, Landroid/support/v4/view/t;->b(Landroid/view/KeyEvent;)V

    .line 728
    const/4 v0, 0x1

    .line 730
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 735
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 736
    invoke-direct {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->f()Landroid/view/View;

    move-result-object v0

    .line 737
    if-eqz v0, :cond_0

    .line 738
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    .line 740
    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 742
    :goto_0
    return v0

    .line 740
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 742
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->q:Z

    .line 327
    sub-int v3, p4, p2

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 331
    iget v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    .line 332
    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 333
    const/4 v4, 0x5

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 334
    neg-int v2, v2

    .line 336
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginStart()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v4, v2

    .line 337
    iget-object v5, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v4, v6

    iget v7, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    iget v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    iget-object v8, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    .line 339
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v1, v8

    .line 337
    invoke-virtual {v5, v6, v7, v4, v1}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 341
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    .line 342
    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    sub-int v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 343
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 344
    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginStart()I

    move-result v5

    sub-int/2addr v5, v2

    iget v6, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    .line 345
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v7

    sub-int/2addr v3, v7

    sub-int v2, v3, v2

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    .line 344
    invoke-virtual {v4, v5, v6, v2, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 353
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a()V

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b()V

    .line 355
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->c()V

    .line 357
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->r:Z

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 363
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->r:Z

    .line 366
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->q:Z

    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->s:Z

    .line 372
    const/4 v1, 0x0

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v0, :cond_4

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_5

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 382
    :goto_1
    if-eqz v0, :cond_2

    .line 383
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 384
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 387
    :cond_2
    return-void

    .line 348
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v5

    add-int/2addr v5, v2

    iget v6, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    .line 349
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginStart()I

    move-result v7

    sub-int/2addr v3, v7

    add-int/2addr v2, v3

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    .line 348
    invoke-virtual {v4, v5, v6, v2, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    goto :goto_0

    .line 378
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_5

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 294
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 295
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 296
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 297
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 299
    if-ne v0, v4, :cond_0

    if-eq v1, v4, :cond_1

    .line 300
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DrawerLayout must be measured with MeasureSpec.EXACTLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->setMeasuredDimension(II)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 308
    iget v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->leftMargin:I

    sub-int v1, v2, v1

    iget v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 310
    iget v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    sub-int v2, v3, v2

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v2, v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 312
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v0}, Landroid/view/ViewGroup;->measure(II)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 315
    iget v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    iget v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->width:I

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getChildMeasureSpec(III)I

    move-result v1

    .line 318
    iget v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->topMargin:I

    iget v3, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->height:I

    invoke-static {p2, v2, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 321
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/FrameLayout;->measure(II)V

    .line 322
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/bk;->b(Landroid/view/MotionEvent;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->a:I

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    .line 575
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    move v1, v2

    .line 582
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    move v0, v2

    .line 583
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v4, v1}, Landroid/support/v4/widget/bk;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    .line 584
    invoke-virtual {v1}, Landroid/support/v4/widget/bk;->b()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    return v3

    .line 578
    :cond_2
    const/4 v0, 0x2

    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v3

    .line 582
    goto :goto_1
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 591
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 593
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 597
    :cond_0
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 600
    :cond_1
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->q:Z

    if-nez v0, :cond_0

    .line 392
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 394
    :cond_0
    return-void
.end method
