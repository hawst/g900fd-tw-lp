.class public Lcom/google/android/apps/gmm/map/offline/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:Lcom/google/maps/b/l;

.field c:Lcom/google/maps/b/h;

.field d:Ljava/lang/Integer;

.field e:Ljava/lang/Integer;

.field private f:Lcom/google/maps/b/e;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, ".9.png"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    sget-object v0, Lcom/google/maps/b/l;->c:Lcom/google/maps/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    .line 51
    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/offline/a;->a:Landroid/graphics/Bitmap;

    .line 52
    sget-object v0, Lcom/google/maps/b/h;->g:Lcom/google/maps/b/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    .line 53
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/offline/a;->d:Ljava/lang/Integer;

    .line 54
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/offline/a;->e:Ljava/lang/Integer;

    .line 55
    return-void

    .line 49
    :cond_0
    sget-object v0, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/maps/b/e;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/offline/a;->f:Lcom/google/maps/b/e;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->a:Landroid/graphics/Bitmap;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/a/f;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->f:Lcom/google/maps/b/e;

    if-eqz v0, :cond_9

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->f:Lcom/google/maps/b/e;

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, v0, Lcom/google/maps/b/e;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/maps/b/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/n;->d()Lcom/google/maps/b/n;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/n;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v4, p1, p2, p3}, Lcom/google/android/apps/gmm/map/offline/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/d/c/a/f;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Lcom/google/maps/b/n;

    move-result-object v4

    .line 87
    iget v0, v4, Lcom/google/maps/b/n;->g:I

    invoke-static {v0}, Lcom/google/maps/b/l;->a(I)Lcom/google/maps/b/l;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    .line 88
    iget v0, v4, Lcom/google/maps/b/n;->j:I

    invoke-static {v0}, Lcom/google/maps/b/h;->a(I)Lcom/google/maps/b/h;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/b/h;->a:Lcom/google/maps/b/h;

    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    .line 89
    iget v0, v4, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v5, 0x4

    if-ne v0, v5, :cond_6

    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    .line 90
    iget v0, v4, Lcom/google/maps/b/n;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->d:Ljava/lang/Integer;

    .line 92
    :cond_3
    iget v0, v4, Lcom/google/maps/b/n;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v5, 0x8

    if-ne v0, v5, :cond_7

    move v0, v2

    :goto_2
    if-eqz v0, :cond_4

    .line 93
    iget v0, v4, Lcom/google/maps/b/n;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->e:Ljava/lang/Integer;

    .line 97
    :cond_4
    iget-object v0, v4, Lcom/google/maps/b/n;->h:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->c()[B

    move-result-object v0

    .line 98
    if-eqz v0, :cond_5

    array-length v2, v0

    if-nez v2, :cond_8

    :cond_5
    move-object v0, v1

    .line 106
    :goto_3
    return-object v0

    :cond_6
    move v0, v3

    .line 89
    goto :goto_1

    :cond_7
    move v0, v3

    .line 92
    goto :goto_2

    .line 103
    :cond_8
    array-length v2, v0

    invoke-static {v0, v3, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->a:Landroid/graphics/Bitmap;

    .line 104
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/offline/a;->f:Lcom/google/maps/b/e;

    .line 106
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/offline/a;->a:Landroid/graphics/Bitmap;

    goto :goto_3
.end method
