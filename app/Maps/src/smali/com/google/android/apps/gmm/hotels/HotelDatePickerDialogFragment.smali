.class public Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Lcom/google/android/apps/gmm/hotels/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->b:Ljava/lang/String;

    .line 30
    const-class v0, Landroid/app/DatePickerDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 35
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/f;)Landroid/app/DialogFragment;
    .locals 7

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;-><init>()V

    .line 59
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    const-string v2, "initial state"

    new-instance v3, Lcom/google/android/apps/gmm/hotels/f;

    .line 61
    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v5, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    iget v6, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-direct {v3, v4, v5, v6, p1}, Lcom/google/android/apps/gmm/hotels/f;-><init>(IIILcom/google/android/apps/gmm/hotels/a/f;)V

    .line 60
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 62
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 63
    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->dismiss()V

    .line 122
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 68
    if-nez p1, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initial state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/hotels/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    .line 74
    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/hotels/a/i;->c()Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v6

    .line 81
    new-instance v0, Landroid/app/DatePickerDialog;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v3, v6, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v2, v6, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    add-int/lit8 v4, v2, -0x1

    iget v5, v6, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 83
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v7}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 88
    sget-object v2, Lcom/google/android/apps/gmm/hotels/e;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/hotels/f;->d:Lcom/google/android/apps/gmm/hotels/a/f;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/hotels/a/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 102
    sget-object v1, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->b:Ljava/lang/String;

    const-string v2, "Unexpected date type"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget v1, v1, Lcom/google/android/apps/gmm/hotels/f;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget v2, v2, Lcom/google/android/apps/gmm/hotels/f;->b:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget v3, v3, Lcom/google/android/apps/gmm/hotels/f;->c:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->updateDate(III)V

    .line 106
    return-object v0

    .line 71
    :cond_0
    const-string v0, "state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/hotels/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    goto :goto_0

    .line 91
    :pswitch_0
    const/16 v2, 0xb4

    .line 92
    invoke-static {v6, v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    .line 91
    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 93
    invoke-static {}, Lcom/google/android/apps/gmm/hotels/a/i;->d()Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    goto :goto_1

    .line 97
    :pswitch_1
    const/16 v2, 0xc2

    .line 98
    invoke-static {v6, v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    .line 97
    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 99
    invoke-static {v6}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    goto :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 6

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iput p2, v0, Lcom/google/android/apps/gmm/hotels/f;->a:I

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    add-int/lit8 v1, p3, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/hotels/f;->b:I

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iput p4, v0, Lcom/google/android/apps/gmm/hotels/f;->c:I

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 131
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/d;

    new-instance v2, Lcom/google/android/apps/gmm/hotels/a/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget v3, v3, Lcom/google/android/apps/gmm/hotels/f;->a:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget v4, v4, Lcom/google/android/apps/gmm/hotels/f;->b:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget v5, v5, Lcom/google/android/apps/gmm/hotels/f;->c:I

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/hotels/a/i;-><init>(III)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/hotels/f;->d:Lcom/google/android/apps/gmm/hotels/a/f;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/hotels/a/d;-><init>(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 134
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    const-string v0, "state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->c:Lcom/google/android/apps/gmm/hotels/f;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 113
    return-void
.end method
