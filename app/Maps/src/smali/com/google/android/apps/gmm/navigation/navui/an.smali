.class Lcom/google/android/apps/gmm/navigation/navui/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/d/p;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

.field private b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361
    const-string v0, "resources"

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p2, Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->b:Landroid/content/res/Resources;

    .line 362
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->mm:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->mn:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    .line 372
    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 371
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->jE:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b()V

    .line 395
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/an;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
