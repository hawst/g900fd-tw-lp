.class public Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/i/av;


# instance fields
.field private c:Lcom/google/android/apps/gmm/directions/option/h;

.field private d:Lcom/google/android/apps/gmm/directions/i/au;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/option/g;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Landroid/app/Fragment;",
            ":",
            "Lcom/google/android/apps/gmm/directions/option/h;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/google/r/b/a/afz;",
            "Lcom/google/android/apps/gmm/directions/option/g;",
            "T",
            "L;",
            ")",
            "Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;"
        }
    .end annotation

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/g;->a:Lcom/google/android/apps/gmm/directions/option/g;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    .line 46
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    const-string v2, "view model"

    new-instance v3, Lcom/google/android/apps/gmm/directions/i/au;

    invoke-direct {v3, p0, p1, v0}, Lcom/google/android/apps/gmm/directions/i/au;-><init>(Landroid/content/Context;Lcom/google/r/b/a/afz;Z)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 52
    invoke-virtual {p3}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 53
    if-nez v0, :cond_1

    .line 54
    const-string v0, "not in a unit or feature test"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :cond_1
    const-string v2, "listener fragment"

    invoke-virtual {v0, v1, v2, p3}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;-><init>()V

    .line 60
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 61
    return-object v0
.end method


# virtual methods
.method public final F_()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/r/b/a/afz;)V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    sget-object v1, Lcom/google/b/f/t;->aI:Lcom/google/b/f/t;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/directions/option/h;->a(Lcom/google/r/b/a/afz;Lcom/google/b/f/t;)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->dismiss()V

    .line 105
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->dismiss()V

    .line 112
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "listener fragment"

    .line 70
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/option/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    .line 71
    if-eqz p1, :cond_0

    :goto_0
    const-string v0, "view model"

    .line 72
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/i/au;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/au;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/au;

    iput-object p0, v0, Lcom/google/android/apps/gmm/directions/i/au;->a:Lcom/google/android/apps/gmm/directions/i/av;

    .line 74
    return-void

    :cond_0
    move-object p1, v1

    .line 71
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/directions/c/ah;

    .line 80
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/au;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iput-object v2, v1, Lcom/google/android/apps/gmm/directions/i/au;->b:Lcom/google/android/libraries/curvular/ag;

    .line 82
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/au;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 84
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 95
    const-string v0, "view model"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuTransitOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/au;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 96
    return-void
.end method
