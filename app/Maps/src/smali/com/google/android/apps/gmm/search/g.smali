.class public Lcom/google/android/apps/gmm/search/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/aj;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/search/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x1

    .line 49
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VOICE_ASSIST"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    const-string v0, "android.intent.extra.ASSIST_PACKAGE"

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 52
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 51
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/map/t;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 57
    const-string v4, "com.google.android.ssb.extra.SSB_CONTEXT"

    invoke-static {}, Lcom/google/android/d/b;->newBuilder()Lcom/google/android/d/d;

    move-result-object v5

    const-wide/16 v6, 0x0

    iget v8, v5, Lcom/google/android/d/d;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v5, Lcom/google/android/d/d;->a:I

    iput-wide v6, v5, Lcom/google/android/d/d;->b:J

    invoke-static {}, Lcom/google/i/a/k;->newBuilder()Lcom/google/i/a/m;

    move-result-object v6

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v7, v6, Lcom/google/i/a/m;->a:I

    or-int/lit16 v7, v7, 0x80

    iput v7, v6, Lcom/google/i/a/m;->a:I

    iput-object v0, v6, Lcom/google/i/a/m;->c:Ljava/lang/Object;

    invoke-static {}, Lcom/google/i/a/b;->newBuilder()Lcom/google/i/a/d;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v8, v0, Lcom/google/i/a/d;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Lcom/google/i/a/d;->a:I

    iput-object v7, v0, Lcom/google/i/a/d;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/i/a/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/b;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v7, v6, Lcom/google/i/a/m;->b:Lcom/google/n/ao;

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v7, Lcom/google/n/ao;->d:Z

    iget v0, v6, Lcom/google/i/a/m;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v6, Lcom/google/i/a/m;->a:I

    invoke-virtual {v6}, Lcom/google/i/a/m;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/k;

    invoke-virtual {v5, v0}, Lcom/google/android/d/d;->a(Lcom/google/i/a/k;)Lcom/google/android/d/d;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    :goto_0
    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    iget v1, v5, Lcom/google/android/d/d;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v5, Lcom/google/android/d/d;->a:I

    iput-object v0, v5, Lcom/google/android/d/d;->c:Ljava/lang/Object;

    :cond_6
    invoke-virtual {v5}, Lcom/google/android/d/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/b;

    invoke-virtual {v0}, Lcom/google/android/d/b;->l()[B

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 59
    const-string v0, "android.intent.extra.ASSIST_CONTEXT"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 61
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 91
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.ssb.action.TEXT_ASSIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 92
    const-string v1, "android.intent.extra.ASSIST_PACKAGE"

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 93
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/t;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 97
    const-string v3, "com.google.search.assist.URI"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v1, "android.intent.extra.ASSIST_CONTEXT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 100
    return-void
.end method
