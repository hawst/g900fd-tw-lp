.class public abstract enum Lcom/google/android/apps/gmm/map/internal/d/c/a/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/d/c/a/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/map/internal/d/c/a/b;


# instance fields
.field final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/c;

    const-string v1, "ICON_ARCHIVE"

    const-string v2, "icon-archive."

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/d;

    const-string v1, "STYLE_TABLE"

    const-string v2, "legend-"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/a/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->b:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/e;

    const-string v1, "EPOCH_RESOURCES"

    const-string v2, "epoch-"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->b:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->e:[Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->d:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/c/a/b;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/d/c/a/b;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->e:[Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    return-object v0
.end method
