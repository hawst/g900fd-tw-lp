.class Lcom/google/android/apps/gmm/navigation/h/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/h/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/h/d;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/h/g;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 263
    :goto_0
    return-void

    .line 249
    :cond_0
    if-nez p3, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/navigation/h/i;->b:Lcom/google/android/apps/gmm/navigation/h/i;

    .line 253
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/g;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    monitor-enter v1

    .line 254
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/h/g;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/h/d;->a:Z

    if-nez v2, :cond_2

    .line 255
    monitor-exit v1

    goto :goto_0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 249
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/h/i;->a:Lcom/google/android/apps/gmm/navigation/h/i;

    goto :goto_1

    .line 257
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/h/g;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/h/d;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/h/h;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/navigation/h/h;-><init>(Lcom/google/android/apps/gmm/navigation/h/g;Lcom/google/android/apps/gmm/navigation/h/i;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 263
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
