.class Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;
.super Lcom/google/android/apps/gmm/map/f/t;
.source "PG"


# instance fields
.field a:Z

.field b:F

.field m:F

.field n:F

.field o:Lcom/google/android/apps/gmm/map/f/a/a;

.field p:J

.field private final q:Lcom/google/android/apps/gmm/map/f/f;

.field private r:Lcom/google/android/apps/gmm/map/f/a/a;

.field private final s:Lcom/google/android/apps/gmm/map/f/o;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V
    .locals 0

    .prologue
    .line 668
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 669
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->s:Lcom/google/android/apps/gmm/map/f/o;

    .line 670
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->q:Lcom/google/android/apps/gmm/map/f/f;

    .line 671
    return-void
.end method


# virtual methods
.method public final W_()I
    .locals 2

    .prologue
    .line 691
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->a:Z

    if-eqz v0, :cond_0

    .line 692
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    sget v1, Lcom/google/android/apps/gmm/map/f/a/a;->c:I

    or-int/2addr v0, v1

    .line 694
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->c:I

    goto :goto_0
.end method

.method public final a(J)I
    .locals 7

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    if-nez v0, :cond_0

    .line 701
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->a:Z

    if-eqz v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->s:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->r:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->q:Lcom/google/android/apps/gmm/map/f/f;

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->b:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->m:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->n:F

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/f;FFF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 707
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->r:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    .line 708
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->p:J

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/map/f/t;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 710
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/map/f/t;->a(J)I

    move-result v0

    return v0

    .line 705
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->r:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->b:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->d:J

    .line 687
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 1

    .prologue
    .line 675
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->r:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 676
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/f/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 736
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;

    .line 737
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    const/4 v0, 0x0

    .line 741
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/g;->d:J

    .line 682
    return-void
.end method
