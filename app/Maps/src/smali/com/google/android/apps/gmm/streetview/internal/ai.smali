.class public Lcom/google/android/apps/gmm/streetview/internal/ai;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# static fields
.field private static f:I


# instance fields
.field final a:Lcom/google/e/a/a/a/b;

.field b:I

.field c:Lcom/google/e/a/a/a/b;

.field d:Z

.field e:Ljava/lang/StringBuilder;

.field private final h:Lcom/google/android/apps/gmm/streetview/internal/aj;

.field private i:Ljava/lang/String;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/gmm/streetview/internal/ai;->f:I

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/aj;)V
    .locals 6

    .prologue
    const/16 v5, 0x3b

    const/4 v4, 0x4

    .line 192
    sget-object v0, Lcom/google/r/b/a/el;->N:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ao;->m:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->b:I

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->d:Z

    .line 193
    const-string v0, "listener"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->h:Lcom/google/android/apps/gmm/streetview/internal/aj;

    .line 196
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/ao;->g:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x33

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x3a

    int-to-long v2, v4

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x3

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    int-to-long v2, v4

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PR sending request for pano "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    .line 209
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Lcom/google/android/apps/gmm/map/b/a/u;)V
    .locals 3

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/ai;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aj;)V

    .line 164
    const-string v0, "mapPoint"

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 166
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/b/a/u;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    const-string v1, "@ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/streetview/internal/ai;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aj;)V

    .line 143
    const-string v0, "panoId"

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->i:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;)V
    .locals 3
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/u;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/streetview/internal/ai;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Ljava/lang/String;)V

    .line 184
    if-eqz p3, :cond_0

    .line 185
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/b/a/u;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    const-string v1, "@ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 12

    .prologue
    .line 373
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 378
    const/4 v0, 0x2

    .line 379
    const/16 v1, 0x1c

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 382
    const/4 v0, 0x3

    .line 383
    const/16 v2, 0x1a

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 384
    if-eqz v0, :cond_2

    .line 385
    new-instance v4, Lcom/google/android/apps/gmm/streetview/internal/t;

    .line 387
    invoke-static {}, Lcom/google/r/b/a/xy;->d()Lcom/google/r/b/a/xy;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    if-eqz v2, :cond_6

    :goto_0
    check-cast v2, Lcom/google/r/b/a/xy;

    invoke-direct {v4, v2}, Lcom/google/android/apps/gmm/streetview/internal/t;-><init>(Lcom/google/r/b/a/xy;)V

    .line 394
    iget-object v3, v4, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    .line 395
    iget v2, v4, Lcom/google/android/apps/gmm/streetview/internal/t;->b:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_2
    sput v2, Lcom/google/android/apps/gmm/streetview/internal/ai;->f:I

    .line 397
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 398
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x20

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "PR received config "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " != response "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->i:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->i:Ljava/lang/String;

    .line 402
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 403
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->i:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1f

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "PR received config "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " != request "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->h:Lcom/google/android/apps/gmm/streetview/internal/aj;

    invoke-interface {v2, v4, v0}, Lcom/google/android/apps/gmm/streetview/internal/aj;->a(Lcom/google/android/apps/gmm/streetview/internal/t;Lcom/google/e/a/a/a/b;)V

    .line 413
    :cond_2
    const/16 v0, 0x11

    .line 414
    const/16 v2, 0x1a

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/e/a/a/a/b;

    .line 415
    if-eqz v6, :cond_d

    .line 416
    const/16 v0, 0x13

    .line 417
    const/16 v2, 0x15

    invoke-virtual {v6, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v2, v2

    .line 418
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->b:I

    if-eq v2, v0, :cond_3

    .line 419
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x33

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "PR received zoom "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 422
    :cond_3
    const/16 v0, 0x16

    iget-object v3, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v8

    .line 428
    const/4 v0, 0x0

    move v7, v0

    :goto_3
    if-ge v7, v8, :cond_d

    .line 429
    const/16 v0, 0x16

    const/16 v3, 0x1a

    invoke-virtual {v6, v0, v7, v3}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 431
    const/16 v3, 0x17

    const/16 v4, 0x15

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v3, v4

    .line 433
    const/16 v4, 0x18

    const/16 v5, 0x15

    invoke-virtual {v0, v4, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v4, v4

    .line 435
    const/16 v9, 0x1a

    iget-object v5, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v9}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    if-lez v5, :cond_a

    const/4 v5, 0x1

    :goto_4
    if-nez v5, :cond_4

    invoke-virtual {v0, v9}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_b

    :cond_4
    const/4 v5, 0x1

    :goto_5
    if-eqz v5, :cond_5

    const/16 v5, 0x1a

    .line 436
    const/16 v9, 0x15

    invoke-virtual {v0, v5, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-int v5, v10

    .line 438
    :cond_5
    const/16 v5, 0x19

    const/16 v9, 0x19

    invoke-virtual {v0, v5, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 440
    array-length v5, v0

    const/4 v9, 0x1

    if-le v5, v9, :cond_c

    const/4 v5, 0x0

    aget-byte v5, v0, v5

    const/16 v9, 0x43

    if-ne v5, v9, :cond_c

    invoke-static {v0}, Lcom/google/h/a/a/c;->a([B)[B

    move-result-object v5

    .line 441
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->h:Lcom/google/android/apps/gmm/streetview/internal/aj;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/aj;->a(Ljava/lang/String;III[B)V

    .line 428
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    :cond_6
    move-object v2, v3

    .line 387
    goto/16 :goto_0

    .line 395
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    iget v2, v4, Lcom/google/android/apps/gmm/streetview/internal/t;->c:I

    if-nez v2, :cond_9

    const/16 v2, 0x7d0

    goto/16 :goto_2

    :cond_9
    iget v2, v4, Lcom/google/android/apps/gmm/streetview/internal/t;->c:I

    goto/16 :goto_2

    .line 435
    :cond_a
    const/4 v5, 0x0

    goto :goto_4

    :cond_b
    const/4 v5, 0x0

    goto :goto_5

    :cond_c
    move-object v5, v0

    .line 440
    goto :goto_6

    .line 447
    :cond_d
    const/16 v0, 0x22

    const/16 v1, 0x18

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->h:Lcom/google/android/apps/gmm/streetview/internal/aj;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/streetview/internal/aj;->a()V

    .line 451
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/r;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 315
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->j:Z

    if-eqz v2, :cond_0

    .line 316
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already queued"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->j:Z

    .line 320
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    .line 321
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    .line 323
    sget v2, Lcom/google/android/apps/gmm/streetview/internal/ai;->f:I

    if-lez v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/16 v4, 0x11

    .line 324
    iget-object v2, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    invoke-virtual {v3, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 328
    :try_start_0
    sget v0, Lcom/google/android/apps/gmm/streetview/internal/ai;->f:I

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_2
    sput v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->f:I

    .line 334
    :cond_2
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 335
    return-void

    :cond_3
    move v2, v1

    .line 324
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 456
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    if-ne p1, v0, :cond_0

    .line 457
    const/4 v0, 0x0

    .line 459
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    goto :goto_0
.end method

.method public final ak_()Z
    .locals 1

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->d:Z

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->h:Lcom/google/android/apps/gmm/streetview/internal/aj;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/streetview/internal/aj;->b()V

    .line 473
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->f()V

    .line 474
    return-void
.end method

.method public final g()Lcom/google/android/apps/gmm/streetview/internal/ai;
    .locals 4

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    const-string v1, " config"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x11

    .line 229
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->g(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 230
    const/16 v2, 0x38

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 231
    const/16 v2, 0x39

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 232
    return-object p0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 465
    if-eqz p1, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ai;->h:Lcom/google/android/apps/gmm/streetview/internal/aj;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/streetview/internal/aj;->a(Lcom/google/android/apps/gmm/streetview/internal/ai;)V

    .line 468
    :cond_0
    return-void
.end method
