.class public Lcom/google/android/apps/gmm/place/c/y;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/g/a/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 73
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 5

    .prologue
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/y;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v3, 0x0

    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/libraries/curvular/c/k;

    .line 33
    sget-object v4, Lcom/google/android/libraries/curvular/c/a;->g:Lcom/google/android/libraries/curvular/c/a;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    const/4 v3, -0x1

    .line 36
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    .line 37
    sget v3, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    .line 31
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/k/ao;->a(Lcom/google/android/libraries/curvular/am;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 27
    check-cast p2, Lcom/google/android/apps/gmm/place/g/a/e;

    const-class v0, Lcom/google/android/apps/gmm/base/f/cf;

    invoke-virtual {p4, v0, p2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/g/a/e;->g()Lcom/google/android/apps/gmm/base/l/a/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/v;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lcom/google/android/apps/gmm/base/f/au;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    const-class v0, Lcom/google/android/apps/gmm/base/f/t;

    new-instance v1, Lcom/google/android/apps/gmm/place/c/z;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/c/z;-><init>(Lcom/google/android/apps/gmm/place/c/y;)V

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/g/a/e;->i()Lcom/google/android/apps/gmm/place/g/a/f;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/g/a/e;->h()Lcom/google/android/apps/gmm/place/g/a/c;

    move-result-object v1

    invoke-static {p4, v0, v1}, Lcom/google/android/apps/gmm/place/bz;->a(Lcom/google/android/libraries/curvular/bc;Lcom/google/android/apps/gmm/place/g/a/f;Lcom/google/android/apps/gmm/place/g/a/c;)V

    const-class v0, Lcom/google/android/apps/gmm/base/f/t;

    new-instance v1, Lcom/google/android/apps/gmm/place/c/aa;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/c/aa;-><init>(Lcom/google/android/apps/gmm/place/c/y;)V

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/g/a/e;->j()Ljava/util/List;

    move-result-object v0

    const v1, 0x7fffffff

    invoke-static {p4, v0, v1}, Lcom/google/android/apps/gmm/place/bz;->a(Lcom/google/android/libraries/curvular/bc;Ljava/util/List;I)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/g/a/e;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/google/android/apps/gmm/place/c/ab;

    invoke-virtual {p4, v0, p2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    return-void
.end method
