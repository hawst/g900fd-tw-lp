.class public Lcom/google/android/apps/gmm/directions/av;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/o/b/a/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Lcom/google/maps/g/a/hm;

.field private e:Lcom/google/r/b/a/afz;

.field private f:Z

.field private g:Lcom/google/maps/g/a/al;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private i:Lcom/google/android/apps/gmm/directions/option/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private j:Lcom/google/maps/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private l:Lcom/google/android/apps/gmm/directions/aw;

.field private m:Lcom/google/android/apps/gmm/directions/dj;

.field private n:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Lcom/google/android/apps/gmm/directions/a/g;

.field private q:Lcom/google/android/apps/gmm/suggest/e/c;

.field private transient r:Lcom/google/android/apps/gmm/directions/f/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient s:Lcom/google/maps/g/hy;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/av;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 132
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    .line 133
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;

    .line 134
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    .line 135
    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;

    .line 136
    sget-object v0, Lcom/google/android/apps/gmm/directions/aw;->a:Lcom/google/android/apps/gmm/directions/aw;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;

    .line 137
    sget-object v0, Lcom/google/android/apps/gmm/directions/dj;->a:Lcom/google/android/apps/gmm/directions/dj;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;

    .line 138
    invoke-static {}, Lcom/google/maps/g/a/hm;->values()[Lcom/google/maps/g/a/hm;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a([Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->n:Lcom/google/b/c/cv;

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/av;->o:Z

    .line 140
    sget-object v0, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->p:Lcom/google/android/apps/gmm/directions/a/g;

    .line 141
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;

    .line 142
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->s:Lcom/google/maps/g/hy;

    .line 143
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;

    .line 144
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;

    .line 145
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/g;->c:Lcom/google/android/apps/gmm/directions/option/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;

    .line 147
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->j:Lcom/google/maps/a/a;

    .line 148
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/directions/av;)V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 204
    monitor-enter p1

    .line 205
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/av;)V

    .line 206
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized A()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 544
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v5

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_2

    .line 545
    if-ne v4, v2, :cond_0

    move v3, v2

    .line 546
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, v4}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    sget-object v6, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v3, :cond_1

    move v0, v1

    .line 550
    :goto_2
    monitor-exit p0

    return v0

    :cond_0
    move v3, v1

    .line 545
    goto :goto_1

    .line 544
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 550
    goto :goto_2

    .line 544
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized B()Lcom/google/b/c/cv;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/o/h/a/sx;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 603
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 605
    invoke-static {}, Lcom/google/o/h/a/sx;->newBuilder()Lcom/google/o/h/a/sz;

    move-result-object v5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 605
    :cond_0
    :try_start_1
    iget v6, v5, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/o/h/a/sz;->a:I

    iput-object v1, v5, Lcom/google/o/h/a/sz;->b:Ljava/lang/Object;

    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v6

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v7, v6, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/google/d/a/a/du;->a:I

    iput-wide v8, v6, Lcom/google/d/a/a/du;->b:J

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v1, v6, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v6, Lcom/google/d/a/a/du;->a:I

    iput-wide v8, v6, Lcom/google/d/a/a/du;->c:J

    invoke-virtual {v6}, Lcom/google/d/a/a/du;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/ds;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v6, v5, Lcom/google/o/h/a/sz;->c:Lcom/google/n/ao;

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v6, Lcom/google/n/ao;->d:Z

    iget v1, v5, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v5, Lcom/google/o/h/a/sz;->a:I

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/q;->c()Lcom/google/d/a/a/hp;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget-object v6, v5, Lcom/google/o/h/a/sz;->d:Lcom/google/n/ao;

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v6, Lcom/google/n/ao;->d:Z

    iget v1, v5, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v5, Lcom/google/o/h/a/sz;->a:I

    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v1, :cond_6

    move v0, v2

    :goto_1
    iget v1, v5, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v5, Lcom/google/o/h/a/sz;->a:I

    iput-boolean v0, v5, Lcom/google/o/h/a/sz;->e:Z

    invoke-virtual {v5}, Lcom/google/o/h/a/sz;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sx;

    invoke-virtual {v3, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 607
    :cond_7
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/av;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/av;-><init>()V

    .line 157
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->j()Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    .line 158
    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    .line 159
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->f()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    .line 160
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->l()Lcom/google/maps/g/a/hm;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    .line 161
    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;

    .line 162
    sget-object v2, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;

    .line 163
    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;

    .line 164
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;

    .line 165
    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/f/a;->c:Lcom/google/maps/g/a/al;

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;

    .line 166
    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/f/a;->d:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;

    .line 167
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/av;->c(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/option/g;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;

    .line 168
    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/f/a;->f:Lcom/google/maps/a/a;

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/av;->j:Lcom/google/maps/a/a;

    .line 171
    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/r/a/ap;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 336
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 337
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-ge p2, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v1, v0

    goto :goto_0

    .line 338
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/directions/av;->c(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    .line 340
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 341
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v3}, Lcom/google/b/c/cv;->size()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_4

    .line 342
    if-ne v0, p2, :cond_3

    .line 343
    invoke-virtual {v2, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 341
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 345
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v4, v0}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_2

    .line 348
    :cond_4
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349
    monitor-exit p0

    return-void
.end method

.method public static b(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/dj;
    .locals 3

    .prologue
    .line 178
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    sget-object v0, Lcom/google/android/apps/gmm/directions/dj;->b:Lcom/google/android/apps/gmm/directions/dj;

    .line 189
    :goto_0
    return-object v0

    .line 180
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    new-instance v0, Lcom/google/android/apps/gmm/directions/dj;

    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/directions/dj;-><init>(Lcom/google/android/apps/gmm/map/r/a/f;)V

    goto :goto_0

    .line 182
    :cond_1
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    sget-object v0, Lcom/google/android/apps/gmm/directions/dj;->c:Lcom/google/android/apps/gmm/directions/dj;

    goto :goto_0

    .line 185
    :cond_2
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    sget-object v0, Lcom/google/android/apps/gmm/directions/dj;->a:Lcom/google/android/apps/gmm/directions/dj;

    goto :goto_0

    .line 188
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/directions/av;->a:Ljava/lang/String;

    const-string v1, "Unknown DirectionsFetcher state."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/google/android/apps/gmm/directions/dj;->a:Lcom/google/android/apps/gmm/directions/dj;

    goto :goto_0
.end method

.method public static c(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/option/g;
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 195
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v1, :cond_1

    .line 197
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/g;->c:Lcom/google/android/apps/gmm/directions/option/g;

    .line 200
    :goto_0
    return-object v0

    .line 199
    :cond_1
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    move v1, v2

    :goto_1
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v1

    if-nez v0, :cond_2

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v6, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v6, v0, v4}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v6, v5, v1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 200
    array-length v7, v6

    move v5, v2

    :goto_2
    if-ge v5, v7, :cond_6

    aget-object v8, v6, v5

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v9

    move v4, v2

    move v1, v2

    :goto_3
    if-ge v4, v9, :cond_4

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    invoke-interface {v0, v4}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v10, "JP"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_3

    :cond_4
    if-nez v1, :cond_5

    :goto_5
    if-eqz v2, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/directions/option/g;->a:Lcom/google/android/apps/gmm/directions/option/g;

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_6
    move v2, v3

    goto :goto_5

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/g;->b:Lcom/google/android/apps/gmm/directions/option/g;

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method private declared-synchronized c(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 390
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v1, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 391
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;

    if-nez v4, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p1

    .line 393
    :cond_0
    monitor-exit p0

    return-object p1

    :cond_1
    move v0, v3

    .line 390
    goto :goto_0

    .line 391
    :cond_2
    :try_start_1
    iget v0, v4, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v5, 0x200

    if-ne v0, v5, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, v4, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    :cond_3
    iget v0, v4, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v5, 0x10

    if-ne v0, v5, :cond_6

    move v0, v2

    :goto_3
    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    :cond_4
    invoke-static {v4}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a(Lcom/google/o/b/a/v;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->e:Lcom/google/android/apps/gmm/map/indoor/d/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_2

    :cond_6
    move v0, v3

    goto :goto_3

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized z()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 530
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v5

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_2

    .line 531
    if-nez v4, :cond_0

    move v3, v2

    .line 532
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, v4}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    sget-object v6, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v3, :cond_1

    move v0, v1

    .line 536
    :goto_2
    monitor-exit p0

    return v0

    :cond_0
    move v3, v1

    .line 531
    goto :goto_1

    .line 530
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 536
    goto :goto_2

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 2

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 402
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 404
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v1, v4, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    .line 406
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/av;->c(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 404
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 408
    :cond_1
    :try_start_1
    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 411
    :cond_2
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/directions/a/g;)V
    .locals 1

    .prologue
    .line 470
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->p:Lcom/google/android/apps/gmm/directions/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    monitor-exit p0

    return-void

    .line 470
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/directions/av;)V
    .locals 1

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    .line 218
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;

    .line 219
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    .line 220
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;

    .line 221
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;

    .line 222
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;

    .line 223
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->n:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->n:Lcom/google/b/c/cv;

    .line 224
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/av;->o:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/av;->o:Z

    .line 225
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->p:Lcom/google/android/apps/gmm/directions/a/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->p:Lcom/google/android/apps/gmm/directions/a/g;

    .line 226
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;

    .line 227
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->s:Lcom/google/maps/g/hy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->s:Lcom/google/maps/g/hy;

    .line 228
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;

    .line 229
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;

    .line 230
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 231
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;

    .line 232
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->j:Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->j:Lcom/google/maps/a/a;

    .line 233
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/av;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->k:Ljava/lang/String;

    .line 234
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/av;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/av;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    monitor-exit p0

    return-void

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/directions/aw;)V
    .locals 1

    .prologue
    .line 452
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/android/apps/gmm/directions/aw;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/directions/dj;)V
    .locals 1

    .prologue
    .line 444
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/android/apps/gmm/directions/dj;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/directions/f/a;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/directions/f/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 506
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 507
    monitor-exit p0

    return-void

    .line 506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/directions/option/g;)V
    .locals 1

    .prologue
    .line 624
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/android/apps/gmm/directions/option/g;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 625
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/r/a/ap;)V
    .locals 1

    .prologue
    .line 355
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    monitor-exit p0

    return-void

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/e/c;)V
    .locals 1

    .prologue
    .line 510
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 511
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/maps/a/a;)V
    .locals 1

    .prologue
    .line 632
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->j:Lcom/google/maps/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    monitor-exit p0

    return-void

    .line 632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/maps/g/a/al;)V
    .locals 1
    .param p1    # Lcom/google/maps/g/a/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 456
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    monitor-exit p0

    return-void

    .line 456
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/maps/g/a/hm;)V
    .locals 1

    .prologue
    .line 419
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/maps/g/a/hm;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Lcom/google/maps/g/hy;)V
    .locals 1
    .param p1    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 616
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->s:Lcom/google/maps/g/hy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    monitor-exit p0

    return-void

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/o/b/a/v;)V
    .locals 3
    .param p1    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;

    .line 374
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 376
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/av;->c(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 378
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/r/b/a/afz;)V
    .locals 1

    .prologue
    .line 427
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/r/b/a/afz;

    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    monitor-exit p0

    return-void

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->n:Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    monitor-exit p0

    return-void

    .line 492
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 474
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/directions/av;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    monitor-exit p0

    return-void

    .line 474
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 2

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Lcom/google/android/apps/gmm/map/r/a/ap;)V
    .locals 1

    .prologue
    .line 362
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    monitor-exit p0

    return-void

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Lcom/google/r/b/a/afz;)V
    .locals 1

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;

    invoke-static {v0}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    .line 436
    invoke-virtual {v0, p1}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    monitor-exit p0

    return-void

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 641
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/av;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 642
    monitor-exit p0

    return-void

    .line 641
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/maps/g/a/hm;)Z
    .locals 1

    .prologue
    .line 488
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->n:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/o/b/a/v;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lcom/google/maps/g/a/hm;
    .locals 1

    .prologue
    .line 282
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lcom/google/r/b/a/afz;
    .locals 1

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lcom/google/android/apps/gmm/directions/dj;
    .locals 1

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Lcom/google/android/apps/gmm/directions/aw;
    .locals 1

    .prologue
    .line 294
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Lcom/google/android/apps/gmm/directions/a/g;
    .locals 1

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->p:Lcom/google/android/apps/gmm/directions/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Lcom/google/maps/g/a/al;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 308
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized k()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    .line 319
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 320
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 321
    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v4}, Lcom/google/b/c/cv;->size()I

    move-result v4

    :goto_0
    if-ge v2, v4, :cond_0

    .line 322
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    invoke-virtual {v5, v2}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 321
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 324
    :cond_0
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;

    .line 325
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/av;->o:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/directions/av;->o:Z

    .line 326
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/av;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    :goto_2
    monitor-exit p0

    return v0

    :cond_1
    move v2, v1

    .line 325
    goto :goto_1

    :cond_2
    move v0, v1

    .line 326
    goto :goto_2

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized l()Z
    .locals 1

    .prologue
    .line 478
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/av;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Lcom/google/android/apps/gmm/directions/f/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 502
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized n()V
    .locals 1

    .prologue
    .line 514
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 515
    monitor-exit p0

    return-void

    .line 514
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o()Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 518
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized p()Z
    .locals 2

    .prologue
    .line 522
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->q:Lcom/google/android/apps/gmm/suggest/e/c;

    sget-object v1, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized q()Lcom/google/o/h/a/dq;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 555
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->z()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v0}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/maps/g/a/hm;)Lcom/google/o/h/a/dq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 558
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized r()Lcom/google/o/h/a/eq;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 563
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    sget-object v0, Lcom/google/o/h/a/eq;->c:Lcom/google/o/h/a/eq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 568
    :goto_0
    monitor-exit p0

    return-object v0

    .line 565
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 566
    sget-object v0, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 568
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 563
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized s()Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 573
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->o()Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 583
    :goto_0
    monitor-exit p0

    return-object v0

    .line 577
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    goto :goto_0

    .line 579
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 580
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    goto :goto_0

    .line 583
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->a:Lcom/google/android/apps/gmm/suggest/e/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 573
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized t()Lcom/google/o/h/a/en;
    .locals 6

    .prologue
    .line 587
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/o/h/a/en;->newBuilder()Lcom/google/o/h/a/ep;

    move-result-object v1

    .line 588
    invoke-static {}, Lcom/google/o/h/a/et;->newBuilder()Lcom/google/o/h/a/ev;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 588
    :cond_0
    :try_start_1
    iget v3, v0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/o/h/a/ev;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hm;->h:I

    iput v2, v0, Lcom/google/o/h/a/ev;->b:I

    iget-object v2, v1, Lcom/google/o/h/a/ep;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/ev;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/ep;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/o/h/a/ep;->a:I

    .line 589
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/av;->B()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/o/h/a/ep;->c()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    iget-object v3, v1, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 590
    :cond_1
    invoke-virtual {v1}, Lcom/google/o/h/a/ep;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/en;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 239
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "startPoint"

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "endPoint"

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "userLocation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->c:Lcom/google/o/b/a/v;

    .line 242
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "travelMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->d:Lcom/google/maps/g/a/hm;

    .line 243
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "directionsOptions"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->e:Lcom/google/r/b/a/afz;

    .line 244
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "bottomContentMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->l:Lcom/google/android/apps/gmm/directions/aw;

    .line 245
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "tripCardsState"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->m:Lcom/google/android/apps/gmm/directions/dj;

    .line 246
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "enabledTravelModeList"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->n:Lcom/google/b/c/cv;

    .line 247
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "rotateSwapWaypointsIconClockwise"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/av;->o:Z

    .line 248
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "resultViewMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->p:Lcom/google/android/apps/gmm/directions/a/g;

    .line 249
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "lastRequestParams"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->r:Lcom/google/android/apps/gmm/directions/f/a;

    .line 250
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "initialLogginParams"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->s:Lcom/google/maps/g/hy;

    .line 251
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "distanceUnits"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->g:Lcom/google/maps/g/a/al;

    .line 252
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "preferredTransitPattern"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->h:Ljava/lang/String;

    .line 253
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "clientCountry"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;

    .line 254
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "adRedirectUrl"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/av;->k:Ljava/lang/String;

    .line 255
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "showFromMyLocation"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/av;->f:Z

    .line 256
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized u()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 597
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->b:Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized v()Lcom/google/maps/g/hy;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 612
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->s:Lcom/google/maps/g/hy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized w()Lcom/google/android/apps/gmm/directions/option/g;
    .locals 1

    .prologue
    .line 620
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->i:Lcom/google/android/apps/gmm/directions/option/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized x()Lcom/google/maps/a/a;
    .locals 1

    .prologue
    .line 628
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->j:Lcom/google/maps/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized y()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 637
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/av;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
