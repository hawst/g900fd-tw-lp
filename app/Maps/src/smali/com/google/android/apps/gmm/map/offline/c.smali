.class public Lcom/google/android/apps/gmm/map/offline/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final d:I


# instance fields
.field final b:Landroid/content/res/Resources;

.field final c:Lcom/google/android/apps/gmm/map/offline/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    const/16 v0, 0xff

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/offline/c;->d:I

    .line 136
    const-class v0, Lcom/google/android/apps/gmm/map/offline/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/offline/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/offline/c;->b:Landroid/content/res/Resources;

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/map/offline/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/offline/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/c;->c:Lcom/google/android/apps/gmm/map/offline/f;

    .line 154
    return-void
.end method

.method static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 308
    .line 309
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 310
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v1, v0, [I

    .line 314
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 316
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object v0, p0

    move v4, v2

    move v5, v2

    .line 311
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    move v0, v2

    .line 317
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 319
    aget v3, v1, v0

    const v4, 0x106000b

    if-eq v3, v4, :cond_0

    aget v3, v1, v0

    const v4, 0x106000c

    if-eq v3, v4, :cond_0

    .line 321
    aget v3, v1, v0

    sget v4, Lcom/google/android/apps/gmm/map/offline/c;->d:I

    and-int/2addr v3, v4

    or-int/2addr v3, p1

    aput v3, v1, v0

    .line 317
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 327
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 329
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object v0, v8

    move v4, v2

    move v5, v2

    .line 324
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 330
    return-object v8
.end method


# virtual methods
.method public final a(Ljava/lang/String;F)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 177
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 182
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move v0, v2

    .line 176
    goto :goto_0

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/offline/c;->c:Lcom/google/android/apps/gmm/map/offline/f;

    new-instance v3, Lcom/google/android/apps/gmm/map/offline/e;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/offline/e;-><init>()V

    .line 181
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/offline/e;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 182
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    :goto_2
    if-nez v1, :cond_1

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/offline/e;->a:Landroid/graphics/Paint;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method
