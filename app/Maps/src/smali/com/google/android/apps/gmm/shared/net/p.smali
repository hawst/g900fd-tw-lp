.class public Lcom/google/android/apps/gmm/shared/net/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/q;


# static fields
.field static final a:J


# instance fields
.field public final b:Landroid/accounts/Account;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/gmm/shared/c/f;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private g:J

.field private final h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/shared/net/p;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->h:Ljava/lang/Object;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->c:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/p;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 67
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p3, Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    .line 68
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p4, Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/gmm/shared/net/p;->e:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private a(Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 185
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/p;->f:Ljava/lang/String;

    .line 209
    iput-wide p2, p0, Lcom/google/android/apps/gmm/shared/net/p;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 128
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/p;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/p;->f()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 130
    if-nez v0, :cond_1

    .line 136
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/p;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/p;->c:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v2, v3, v5}, Lcom/google/android/gms/auth/e;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/gms/auth/h; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/gms/auth/d; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 141
    :goto_0
    if-nez v0, :cond_0

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->c:Landroid/content/Context;

    .line 146
    invoke-static {v2}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/shared/net/p;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 152
    :cond_0
    if-eqz v0, :cond_1

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    sget-wide v4, Lcom/google/android/apps/gmm/shared/net/p;->a:J

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/p;->a(Ljava/lang/String;J)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/d; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 163
    :cond_1
    :goto_1
    :try_start_3
    monitor-exit v1

    return-object v0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v2

    goto :goto_1

    .line 160
    :catch_1
    move-exception v2

    goto :goto_1

    .line 157
    :catch_2
    move-exception v2

    goto :goto_1

    :catch_3
    move-exception v2

    goto :goto_0
.end method

.method private declared-synchronized f()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/p;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/shared/net/p;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    const/4 v0, 0x0

    .line 199
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->f:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()Z
    .locals 4

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->g:J

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 85
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->g:J

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/e;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/p;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/p;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/p;->f()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 105
    if-nez v0, :cond_0

    .line 107
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/p;->c:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v0, v2, v4}, Lcom/google/android/gms/auth/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/gms/auth/f; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 114
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 115
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/p;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    sget-wide v4, Lcom/google/android/apps/gmm/shared/net/p;->a:J

    add-long/2addr v2, v4

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/p;->a(Ljava/lang/String;J)V

    .line 117
    :cond_1
    monitor-exit v1

    return-object v0

    .line 111
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/p;->a(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
