.class Lcom/google/android/apps/gmm/map/legacy/a/a/d;
.super Lcom/google/android/apps/gmm/map/util/a/e;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/util/a/e",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/bp;",
        "Lcom/google/android/apps/gmm/map/legacy/a/a/b;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/a/a/a;)V
    .locals 1

    .prologue
    .line 667
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    .line 670
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(I)V

    .line 665
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    .line 671
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 699
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->f()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 700
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/f;-><init>(La/a/a/a/d/am;)V

    .line 701
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/f;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 702
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/f;->a()Lcom/google/android/apps/gmm/map/util/a/g;

    move-result-object v1

    .line 703
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eq v3, v4, :cond_0

    .line 704
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 709
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 710
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 711
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 709
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 713
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 721
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->f()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 722
    new-instance v3, Lcom/google/android/apps/gmm/map/util/a/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/map/util/a/f;-><init>(La/a/a/a/d/am;)V

    .line 723
    :goto_0
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/a/f;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/a/f;->a()Lcom/google/android/apps/gmm/map/util/a/g;

    move-result-object v4

    .line 725
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    sget-object v5, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eq v0, v5, :cond_2

    .line 726
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/util/a/g;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    .line 729
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 730
    if-eqz v5, :cond_0

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->b()Z

    move-result v6

    if-nez v6, :cond_1

    .line 731
    :cond_0
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 721
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 736
    :cond_1
    :try_start_1
    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->f()V

    .line 737
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    iget v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    .line 738
    const/4 v4, 0x0

    iput v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    goto :goto_0

    .line 742
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_3

    .line 743
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 744
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 746
    :cond_3
    monitor-exit p0

    return-void
.end method

.method protected final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 657
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    check-cast p2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->c:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eqz v0, :cond_0

    iput v3, p2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    iput v3, p2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 779
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    .line 781
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->g()V

    .line 783
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    iget v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    sub-int/2addr v4, v5

    iput v4, v3, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    .line 784
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->c:I

    sub-int v0, v4, v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    .line 779
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 787
    return-void
.end method
