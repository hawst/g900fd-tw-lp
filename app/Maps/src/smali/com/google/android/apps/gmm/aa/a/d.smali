.class public Lcom/google/android/apps/gmm/aa/a/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field final b:Lcom/google/android/apps/gmm/aa/a/a;

.field c:Lcom/google/android/apps/gmm/navigation/g/b/f;

.field d:Lcom/google/android/apps/gmm/navigation/g/b/k;

.field private e:Lcom/google/android/apps/gmm/map/r/a/am;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    .line 60
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/aa/a/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->b:Lcom/google/android/apps/gmm/aa/a/a;

    .line 61
    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 236
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 237
    iget v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 234
    goto :goto_0

    :cond_1
    move v0, v1

    .line 237
    goto :goto_1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    .line 246
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/am;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    .line 249
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/gmm/aa/a/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 133
    invoke-direct {p0}, Lcom/google/android/apps/gmm/aa/a/d;->f()V

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 140
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/a;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/am;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    .line 74
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 84
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 85
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    .line 84
    goto :goto_0

    .line 85
    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 93
    :goto_1
    return-void

    .line 88
    :cond_2
    iput-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 89
    iput-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 90
    iput-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->b:Lcom/google/android/apps/gmm/aa/a/a;

    iput v1, v0, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    goto :goto_1
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 183
    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v3, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 185
    iget v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    .line 184
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/c/m;->c:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {v4, v3, v5}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->dU:I

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v2, v0, v6}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v2

    invoke-virtual {v2, v0, v6, v1}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 189
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 181
    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/gmm/aa/a/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0}, Lcom/google/android/apps/gmm/aa/a/d;->f()V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/d;->d()Ljava/lang/String;

    move-result-object v1

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->e:Lcom/google/android/apps/gmm/map/r/a/am;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/an;->a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/b/a/an;

    move-result-object v0

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 223
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 222
    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    invoke-virtual {v3, v1, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(Ljava/lang/String;ILcom/google/maps/g/a/al;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 227
    :cond_0
    return-void
.end method

.method d()Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 332
    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 333
    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    if-eq v1, v2, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-object v0

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 338
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v3, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    if-ne v2, v3, :cond_3

    .line 339
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 341
    :cond_3
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
