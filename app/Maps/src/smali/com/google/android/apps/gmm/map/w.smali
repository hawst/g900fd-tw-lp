.class public Lcom/google/android/apps/gmm/map/w;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/google/android/apps/gmm/map/b/a/r;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 30
    const-class v0, Lcom/google/android/apps/gmm/map/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/w;->a:Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/s;

    move-result-object v0

    invoke-static {v1, v1}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/s;->a()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/w;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 32
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 69
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/map/w;->a:Ljava/lang/String;

    const-string v2, "GoogleMap must be created before accessing viewport"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    const/4 v0, 0x0

    .line 75
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 70
    goto :goto_0

    .line 75
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/w;->c(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 3

    .prologue
    .line 109
    if-nez p1, :cond_0

    .line 110
    sget-object v0, Lcom/google/android/apps/gmm/map/w;->a:Ljava/lang/String;

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;F)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;FILcom/google/android/apps/gmm/map/p;)V
    .locals 6

    .prologue
    .line 144
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 145
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 147
    :cond_0
    invoke-static {p2, p6, p4}, Lcom/google/android/apps/gmm/map/c;->b(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput p7, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 146
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 215
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-wide v0, p2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, p2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/b/a/y;)[I

    move-result-object v0

    .line 153
    if-nez v0, :cond_2

    .line 156
    invoke-static {p2, p6, p4}, Lcom/google/android/apps/gmm/map/c;->b(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput p7, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 155
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_0

    .line 160
    :cond_2
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 161
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 165
    invoke-virtual {p4, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 168
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput p7, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 167
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_0

    .line 166
    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_1

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    .line 173
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_0

    .line 174
    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_2

    .line 180
    :cond_6
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v4

    .line 181
    iget-object v5, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v5

    invoke-direct {v0, v1, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 184
    invoke-static {v2, v0}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 186
    const/4 v0, 0x0

    .line 187
    const/4 v1, 0x0

    .line 188
    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, p4, Landroid/graphics/Rect;->left:I

    if-ge v3, v4, :cond_9

    .line 189
    iget v0, p4, Landroid/graphics/Rect;->left:I

    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    .line 193
    :cond_7
    :goto_3
    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, p4, Landroid/graphics/Rect;->top:I

    if-ge v3, v4, :cond_a

    .line 194
    iget v1, p4, Landroid/graphics/Rect;->top:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    .line 200
    :cond_8
    :goto_4
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    .line 199
    invoke-virtual {p1, v2, v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput p7, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 202
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto/16 :goto_0

    .line 190
    :cond_9
    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget v4, p4, Landroid/graphics/Rect;->right:I

    if-le v3, v4, :cond_7

    .line 191
    iget v0, p4, Landroid/graphics/Rect;->right:I

    iget v3, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v3

    goto :goto_3

    .line 195
    :cond_a
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, p4, Landroid/graphics/Rect;->bottom:I

    if-le v3, v4, :cond_8

    .line 196
    iget v1, p4, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    goto :goto_4

    .line 206
    :cond_b
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p5, Landroid/graphics/Point;->x:I

    .line 207
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget v2, p5, Landroid/graphics/Point;->y:I

    .line 208
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p5, Landroid/graphics/Point;->x:I

    .line 209
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v4, p5, Landroid/graphics/Point;->y:I

    .line 210
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 212
    invoke-static {p2, p6, v0}, Lcom/google/android/apps/gmm/map/c;->b(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput p7, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 211
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/t;Lcom/google/e/a/a/a/b;)V
    .locals 1

    .prologue
    .line 99
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 103
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/maps/a/c;DDDI)V
    .locals 7

    .prologue
    .line 240
    iget v0, p0, Lcom/google/maps/a/c;->e:F

    .line 241
    float-to-double v4, v0

    move-wide v0, p5

    move-wide v2, p1

    move v6, p7

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->a(DDDI)D

    move-result-wide v2

    .line 242
    iget-object v0, p0, Lcom/google/maps/a/c;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    invoke-static {v0}, Lcom/google/maps/a/e;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v0

    .line 243
    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide p1, v0, Lcom/google/maps/a/g;->c:D

    .line 244
    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide p3, v0, Lcom/google/maps/a/g;->b:D

    .line 245
    iget v1, v0, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v0, Lcom/google/maps/a/g;->d:D

    .line 246
    iget-object v1, p0, Lcom/google/maps/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p0, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/a/c;->a:I

    .line 247
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 219
    if-nez p1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 222
    :cond_1
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/b/a/y;)[I

    move-result-object v1

    .line 223
    if-eqz v1, :cond_0

    .line 226
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 227
    aget v0, v1, v0

    const/4 v3, 0x1

    aget v1, v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 229
    invoke-virtual {p3, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 82
    const/4 v0, 0x0

    .line 84
    :goto_1
    return-object v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/w;->c(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    goto :goto_1
.end method

.method private static c(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->y()Lcom/google/android/apps/gmm/map/ak;

    move-result-object v1

    .line 43
    if-nez v1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-object v0

    .line 46
    :cond_1
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/ak;->a()Lcom/google/android/apps/gmm/map/b/a/ba;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_0

    .line 50
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 54
    sget-object v2, Lcom/google/android/apps/gmm/map/w;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/b/a/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 58
    goto :goto_0
.end method
