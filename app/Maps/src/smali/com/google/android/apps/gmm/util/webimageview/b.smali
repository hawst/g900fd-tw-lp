.class public abstract enum Lcom/google/android/apps/gmm/util/webimageview/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/util/webimageview/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/util/webimageview/b;

.field public static final enum b:Lcom/google/android/apps/gmm/util/webimageview/b;

.field public static final enum c:Lcom/google/android/apps/gmm/util/webimageview/b;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/util/webimageview/b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/c;

    const-string v1, "FIFE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/util/webimageview/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 169
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/d;

    const-string v1, "FIFE_REPLACEMENT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/util/webimageview/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 181
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/e;

    const-string v1, "FULLY_QUALIFIED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/util/webimageview/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 145
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/util/webimageview/b;

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/b;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->d:[Lcom/google/android/apps/gmm/util/webimageview/b;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/util/webimageview/b;
    .locals 1

    .prologue
    .line 145
    const-class v0, Lcom/google/android/apps/gmm/util/webimageview/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/webimageview/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/util/webimageview/b;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->d:[Lcom/google/android/apps/gmm/util/webimageview/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/util/webimageview/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/util/webimageview/b;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Z)Ljava/lang/String;
.end method
