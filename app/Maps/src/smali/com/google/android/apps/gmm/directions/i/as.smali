.class public Lcom/google/android/apps/gmm/directions/i/as;
.super Lcom/google/android/apps/gmm/directions/i/at;
.source "PG"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Ljava/io/Serializable;


# instance fields
.field a:Lcom/google/maps/g/a/hc;

.field final synthetic b:Lcom/google/android/apps/gmm/directions/i/an;

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/directions/i/an;)V
    .locals 1

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/as;->b:Lcom/google/android/apps/gmm/directions/i/an;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/i/at;-><init>()V

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/i/as;->f(I)V

    .line 69
    return-void
.end method


# virtual methods
.method public final a()Landroid/widget/RadioGroup$OnCheckedChangeListener;
    .locals 0

    .prologue
    .line 129
    return-object p0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 119
    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/at;->c:[I

    aget v0, v0, p1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/gmm/directions/i/as;->e:I

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method f(I)V
    .locals 3

    .prologue
    .line 72
    iput p1, p0, Lcom/google/android/apps/gmm/directions/i/as;->e:I

    .line 73
    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/as;->a:Lcom/google/maps/g/a/hc;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/as;->b:Lcom/google/android/apps/gmm/directions/i/an;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 75
    :cond_0
    return-void

    .line 73
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/g;->o:I

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/gmm/g;->be:I

    if-ne p1, v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/an;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown timeAnchoring: checkedId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 0

    .prologue
    .line 134
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/directions/i/as;->f(I)V

    .line 135
    return-void
.end method
