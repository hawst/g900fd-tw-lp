.class public Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"


# static fields
.field static final c:J

.field public static final d:I

.field private static final g:J

.field private static final h:J


# instance fields
.field e:Lcom/google/android/apps/gmm/mylocation/h/a;

.field public f:Landroid/view/View;

.field private i:Landroid/app/Dialog;

.field private j:Landroid/widget/VideoView;

.field private k:Lcom/google/android/apps/gmm/util/j;

.field private l:J

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    .line 43
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->g:J

    .line 46
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    .line 47
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->h:J

    .line 57
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->c:J

    .line 60
    const-string v0, "#3186ff"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 252
    new-instance v1, Lcom/google/android/apps/gmm/mylocation/fragments/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/mylocation/fragments/c;-><init>(Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 261
    return-void

    .line 252
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 91
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v1

    .line 100
    :goto_0
    return v0

    .line 96
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 97
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->aV:Lcom/google/android/apps/gmm/shared/b/c;

    const-wide/16 v6, 0x0

    .line 98
    invoke-virtual {v0, v4, v6, v7}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;J)J

    move-result-wide v4

    .line 100
    sget-wide v6, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->h:J

    add-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(I)Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;-><init>()V

    .line 81
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 82
    const-string v2, "orientationAccuracy"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 83
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 84
    return-object v0
.end method


# virtual methods
.method public final F_()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/android/apps/gmm/p/c/a;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 272
    iget v0, p1, Lcom/google/android/apps/gmm/p/c/a;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    if-gt v0, v2, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget v0, p1, Lcom/google/android/apps/gmm/p/c/a;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    .line 278
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    const/4 v2, 0x3

    if-lt v0, v2, :cond_5

    .line 279
    iget-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_2

    :cond_4
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/mylocation/f/a;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->i:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    sget-wide v0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->g:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->a(J)V

    goto :goto_0

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->e:Lcom/google/android/apps/gmm/mylocation/h/a;

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/h/a;->a(I)V

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->e:Lcom/google/android/apps/gmm/mylocation/h/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/feedback/a/e;->a(Z)V

    .line 131
    return-void

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 111
    if-eqz p1, :cond_1

    .line 112
    :goto_0
    const-string v0, "orientationAccuracy"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    .line 113
    const-string v0, "changedToFinishedTimeMsec"

    const-wide/16 v2, -0x1

    .line 114
    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mylocation/a;-><init>(Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->e:Lcom/google/android/apps/gmm/mylocation/h/a;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->e:Lcom/google/android/apps/gmm/mylocation/h/a;

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/mylocation/h/a;->a(I)V

    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/util/j;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/util/j;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->k:Lcom/google/android/apps/gmm/util/j;

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aV:Lcom/google/android/apps/gmm/shared/b/c;

    .line 123
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 124
    :cond_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 121
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    .line 122
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_2
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 135
    iget-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/mylocation/f/a;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 138
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    .line 169
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->e:Lcom/google/android/apps/gmm/mylocation/h/a;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 171
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_6

    :goto_2
    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 172
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/Window;->requestFeature(I)Z

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 176
    invoke-virtual {v0, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 178
    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->i:Landroid/app/Dialog;

    .line 179
    return-object v0

    .line 140
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_3

    :cond_4
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/mylocation/f/b;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 141
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    sget v2, Lcom/google/android/apps/gmm/mylocation/f/b;->a:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->j:Landroid/widget/VideoView;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->j:Landroid/widget/VideoView;

    invoke-virtual {v0, v6}, Landroid/widget/VideoView;->setKeepScreenOn(Z)V

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->j:Landroid/widget/VideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_4
    sget v3, Lcom/google/android/apps/gmm/k;->a:I

    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "android.resource"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->j:Landroid/widget/VideoView;

    invoke-virtual {v0, v7}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->j:Landroid/widget/VideoView;

    new-instance v2, Lcom/google/android/apps/gmm/mylocation/fragments/a;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/mylocation/fragments/a;-><init>(Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->f:Landroid/view/View;

    sget v2, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->d:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 148
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_4

    .line 171
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/mylocation/fragments/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/mylocation/fragments/b;-><init>(Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;Lcom/google/android/apps/gmm/feedback/a/e;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    sget-wide v4, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->c:J

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 194
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDestroy()V

    .line 195
    return-void

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->k:Lcom/google/android/apps/gmm/util/j;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/util/j;->b:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/util/j;->b:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    iget v0, v0, Lcom/google/android/apps/gmm/util/j;->a:I

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 230
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 231
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onPause()V

    .line 232
    return-void

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 12

    .prologue
    const/16 v4, 0x9

    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 211
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 212
    iget-wide v6, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    .line 215
    iget-wide v8, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    sget-wide v10, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->g:J

    add-long/2addr v8, v10

    sub-long v6, v8, v6

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    new-instance v8, Lcom/google/android/apps/gmm/mylocation/fragments/c;

    invoke-direct {v8, p0}, Lcom/google/android/apps/gmm/mylocation/fragments/c;-><init>(Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;)V

    sget-object v9, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    invoke-interface {v0, v8, v9, v6, v7}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 223
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->k:Lcom/google/android/apps/gmm/util/j;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-lt v0, v6, :cond_5

    const/16 v0, 0xe

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/util/j;->b:Z

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v3

    iput v3, v1, Lcom/google/android/apps/gmm/util/j;->a:I

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/util/j;->b:Z

    :cond_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 225
    :goto_4
    return-void

    .line 214
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->j:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    goto :goto_2

    .line 223
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_3

    .line 224
    :cond_5
    iget-object v0, v1, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v6, v0, Landroid/content/res/Configuration;->orientation:I

    iget-object v0, v1, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v7

    if-eq v7, v2, :cond_6

    const/4 v0, 0x3

    if-ne v7, v0, :cond_a

    :cond_6
    move v0, v2

    :goto_5
    if-nez v0, :cond_7

    if-eq v6, v2, :cond_8

    :cond_7
    if-eqz v0, :cond_b

    const/4 v0, 0x2

    if-ne v6, v0, :cond_b

    :cond_8
    packed-switch v7, :pswitch_data_0

    move v0, v5

    :goto_6
    iget-boolean v3, v1, Lcom/google/android/apps/gmm/util/j;->b:Z

    if-nez v3, :cond_9

    iget-object v3, v1, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v3

    iput v3, v1, Lcom/google/android/apps/gmm/util/j;->a:I

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/util/j;->b:Z

    :cond_9
    iget-object v1, v1, Lcom/google/android/apps/gmm/util/j;->c:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_4

    :cond_a
    move v0, v3

    goto :goto_5

    :pswitch_0
    move v0, v2

    goto :goto_6

    :pswitch_1
    move v0, v3

    goto :goto_6

    :pswitch_2
    move v0, v4

    goto :goto_6

    :cond_b
    packed-switch v7, :pswitch_data_1

    move v0, v2

    goto :goto_6

    :pswitch_3
    move v0, v3

    goto :goto_6

    :pswitch_4
    move v0, v4

    goto :goto_6

    :pswitch_5
    move v0, v5

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 184
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 185
    const-string v0, "orientationAccuracy"

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    iget-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "changedToFinishedTimeMsec"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/mylocation/fragments/CalibrateCompassDialogFragment;->l:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 189
    :cond_0
    return-void
.end method
