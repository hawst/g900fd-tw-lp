.class Lcom/google/android/apps/gmm/place/ak;
.super Landroid/support/v4/view/ag;
.source "PG"


# instance fields
.field final synthetic b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/PlacePageViewPager;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-direct {p0}, Landroid/support/v4/view/ag;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, -0x2

    .line 411
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->j:Landroid/view/View;

    if-ne p1, v1, :cond_1

    .line 430
    :cond_0
    :goto_0
    return v0

    .line 417
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/place/PlacePageView;

    .line 418
    invoke-static {}, Lcom/google/android/apps/gmm/place/PlacePageView;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 424
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/am;->aa_()I

    move-result v2

    .line 425
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 426
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v3, v3, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v3, v1}, Lcom/google/android/apps/gmm/place/am;->a(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    if-nez v3, :cond_2

    move v0, v1

    .line 427
    goto :goto_0

    .line 425
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/am;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-static {v0, p2}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Lcom/google/android/apps/gmm/place/PlacePageViewPager;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->h:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/gmm/h;->ar:I

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->j:Landroid/view/View;

    .line 333
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    .line 334
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->j:Landroid/view/View;

    sget v2, Lcom/google/android/apps/gmm/g;->dk:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ContentLoadingProgressBar;

    .line 333
    iput-object v0, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->k:Landroid/support/v4/widget/ContentLoadingProgressBar;

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->j:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->j:Landroid/view/View;

    .line 385
    :goto_0
    return-object v1

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/place/am;->a(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v3

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->n:Lcom/google/android/apps/gmm/place/aa;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->f:Z

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/aa;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object v1, v0

    .line 343
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/place/am;->a(Lcom/google/android/apps/gmm/place/af;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->l:Lcom/google/android/apps/gmm/place/an;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->p:Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Z)V

    .line 350
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->setTag(Ljava/lang/Object;)V

    .line 351
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/place/PlacePageView;->setVisibility(I)V

    .line 352
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eq v0, v3, :cond_3

    .line 353
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 354
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 356
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 362
    :cond_3
    :goto_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 363
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    if-eqz v0, :cond_4

    .line 364
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageView;

    .line 367
    if-eqz v0, :cond_4

    .line 370
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 341
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/place/ab;->a:[I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/f;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/apps/gmm/place/QuPlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/aa;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/place/QuPlacePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object v1, v0

    goto :goto_1

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/aa;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/place/GeocodePlacePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object v1, v0

    goto :goto_1

    .line 383
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ak;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->i:Lcom/google/android/apps/gmm/place/am;

    invoke-interface {v0, p2, v1}, Lcom/google/android/apps/gmm/place/am;->a(ILcom/google/android/apps/gmm/place/PlacePageView;)V

    goto/16 :goto_0

    .line 341
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 402
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 403
    check-cast p3, Landroid/view/View;

    invoke-static {p3}, Lcom/google/android/apps/gmm/base/k/g;->a(Landroid/view/View;)V

    .line 404
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 395
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
