.class Lcom/google/android/apps/gmm/hotels/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/a;->a:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/hotels/a/d;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/a;->a:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/a;->a:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    sget-object v0, Lcom/google/android/apps/gmm/hotels/b;->a:[I

    iget-object v2, p1, Lcom/google/android/apps/gmm/hotels/a/d;->b:Lcom/google/android/apps/gmm/hotels/a/f;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/hotels/a/f;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/hotels/a/b;->a(Lcom/google/android/apps/gmm/hotels/a/e;)V

    new-instance v2, Lcom/google/android/apps/gmm/hotels/c/b;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->e:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v4, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/google/android/apps/gmm/hotels/c/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;Lcom/google/android/apps/gmm/hotels/a/e;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->d:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v2, p1, Lcom/google/android/apps/gmm/hotels/a/d;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/hotels/a/e;->a(Lcom/google/android/apps/gmm/hotels/a/i;)Lcom/google/android/apps/gmm/hotels/a/e;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    goto :goto_1

    :pswitch_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v2, p1, Lcom/google/android/apps/gmm/hotels/a/d;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/hotels/a/e;->b(Lcom/google/android/apps/gmm/hotels/a/i;)Lcom/google/android/apps/gmm/hotels/a/e;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/hotels/a/h;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/a;->a:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/a;->a:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->e:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v3, v0, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v4, 0x2

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/hotels/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    :cond_1
    new-instance v2, Lcom/google/android/apps/gmm/hotels/c/b;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->e:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v4, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->c:Lcom/google/android/apps/gmm/hotels/a/e;

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/google/android/apps/gmm/hotels/c/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;Lcom/google/android/apps/gmm/hotels/a/e;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;->d:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method
