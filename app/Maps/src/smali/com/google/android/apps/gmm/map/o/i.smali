.class public Lcom/google/android/apps/gmm/map/o/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/k;


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field final d:I

.field final e:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;IIII)V
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 302
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p2

    .line 303
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p4

    .line 304
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p3

    .line 305
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, p5

    .line 306
    iput v0, p0, Lcom/google/android/apps/gmm/map/o/i;->a:I

    .line 307
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/i;->b:I

    .line 308
    iput p3, p0, Lcom/google/android/apps/gmm/map/o/i;->c:I

    .line 309
    iput p4, p0, Lcom/google/android/apps/gmm/map/o/i;->d:I

    .line 310
    iput p5, p0, Lcom/google/android/apps/gmm/map/o/i;->e:I

    .line 311
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 320
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 327
    :goto_0
    return v0

    .line 323
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/o/i;

    if-nez v0, :cond_1

    move v0, v2

    .line 324
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 326
    check-cast v0, Lcom/google/android/apps/gmm/map/o/i;

    .line 327
    iget v3, p0, Lcom/google/android/apps/gmm/map/o/i;->a:I

    check-cast p1, Lcom/google/android/apps/gmm/map/o/i;

    iget v4, p1, Lcom/google/android/apps/gmm/map/o/i;->a:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/i;->b:I

    iget v4, v0, Lcom/google/android/apps/gmm/map/o/i;->b:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/i;->c:I

    iget v4, v0, Lcom/google/android/apps/gmm/map/o/i;->c:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/i;->d:I

    iget v4, v0, Lcom/google/android/apps/gmm/map/o/i;->d:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/i;->e:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/i;->e:I

    if-ne v3, v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/i;->a:I

    return v0
.end method
