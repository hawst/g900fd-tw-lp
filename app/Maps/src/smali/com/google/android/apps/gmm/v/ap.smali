.class public final enum Lcom/google/android/apps/gmm/v/ap;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/v/ap;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/v/ap;

.field public static final enum b:Lcom/google/android/apps/gmm/v/ap;

.field public static final enum c:Lcom/google/android/apps/gmm/v/ap;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/v/ap;


# instance fields
.field public d:Z

.field public e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/v/ap;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/apps/gmm/v/ap;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ap;->a:Lcom/google/android/apps/gmm/v/ap;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/v/ap;

    const-string v1, "CLAMP"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/android/apps/gmm/v/ap;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ap;->b:Lcom/google/android/apps/gmm/v/ap;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/v/ap;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v4, v3, v3}, Lcom/google/android/apps/gmm/v/ap;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ap;->c:Lcom/google/android/apps/gmm/v/ap;

    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/ap;

    sget-object v1, Lcom/google/android/apps/gmm/v/ap;->a:Lcom/google/android/apps/gmm/v/ap;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/v/ap;->b:Lcom/google/android/apps/gmm/v/ap;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/v/ap;->c:Lcom/google/android/apps/gmm/v/ap;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/v/ap;->f:[Lcom/google/android/apps/gmm/v/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/v/ap;->d:Z

    .line 48
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/v/ap;->e:Z

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/v/ap;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/v/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ap;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/v/ap;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/apps/gmm/v/ap;->f:[Lcom/google/android/apps/gmm/v/ap;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/v/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/ap;

    return-object v0
.end method
