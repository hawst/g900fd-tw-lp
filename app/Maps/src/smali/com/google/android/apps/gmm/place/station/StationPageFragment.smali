.class public Lcom/google/android/apps/gmm/place/station/StationPageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/c;


# instance fields
.field private c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/place/bq;

.field private e:Lcom/google/android/apps/gmm/place/ac;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Ljava/lang/String;Lcom/google/r/b/a/ael;)Lcom/google/android/apps/gmm/place/station/StationPageFragment;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 54
    new-instance v1, Lcom/google/android/apps/gmm/place/station/StationPageFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;-><init>()V

    .line 56
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 57
    const-string v0, "requestProto"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 58
    const-string v3, "placemarkRef"

    new-instance v4, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 61
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    .line 62
    invoke-virtual {p2}, Lcom/google/r/b/a/ael;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    .line 63
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 65
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v1
.end method

.method private a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->c:Lcom/google/android/apps/gmm/x/o;

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->e:Lcom/google/android/apps/gmm/place/ac;

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/place/bq;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    .line 169
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/bq;->A()Lcom/google/android/apps/gmm/base/l/a/ab;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/a/ab;->c()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v1

    .line 170
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/c/g;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->a(Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/bq;->a(Z)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 1

    .prologue
    .line 151
    invoke-static {p2}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 152
    return-void
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 76
    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, ""

    .line 76
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/b/f/t;->fi:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/place/bq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/bq;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/place/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/place/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/app/Fragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->e:Lcom/google/android/apps/gmm/place/ac;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    if-nez p1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    :cond_0
    const-string v1, "placemarkRef"

    .line 91
    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 90
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 94
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/station/d;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 101
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/aq;->h:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/bq;->e:Lcom/google/android/apps/gmm/place/riddler/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->b(Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 140
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 108
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/aq;->h:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/bq;->e:Lcom/google/android/apps/gmm/place/riddler/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->a(Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->d:Lcom/google/android/apps/gmm/place/bq;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/bq;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "requestProto"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/r/b/a/ael;

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 119
    iget-object v1, v2, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_1

    check-cast v1, Ljava/lang/String;

    .line 120
    :goto_0
    invoke-virtual {v2}, Lcom/google/r/b/a/ael;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    move-object v3, p0

    move-object v5, v4

    move-object v6, v4

    .line 117
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/ci;->a(Lcom/google/android/apps/gmm/base/activities/o;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/b/c;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;Lcom/google/maps/g/hy;)Lcom/google/android/apps/gmm/place/ci;

    move-result-object v1

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 128
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 129
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 131
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 132
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 134
    return-void

    .line 119
    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v3, v2, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    :cond_2
    move-object v1, v3

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemarkRef"

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 147
    return-void
.end method
