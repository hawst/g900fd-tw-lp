.class Lcom/google/android/apps/gmm/l/aw;
.super Lcom/google/android/apps/gmm/l/o;
.source "PG"


# static fields
.field static final b:Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/l/aw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/aw;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/o;-><init>(Landroid/content/Intent;)V

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/l/aw;->c:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/gmm/l/aw;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 36
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 3

    .prologue
    .line 54
    new-instance v1, Lcom/google/android/apps/gmm/l/ax;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/l/ax;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 91
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 92
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/aw;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/aw;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/l/ax;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/l/ax;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 42
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method
