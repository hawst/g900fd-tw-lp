.class public Lcom/google/android/apps/gmm/place/j/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/f;


# instance fields
.field public a:Lcom/google/android/apps/gmm/place/i/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Z

.field private c:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/base/activities/c;

.field private e:Lcom/google/android/apps/gmm/base/placelists/o;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 42
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/place/j/p;->b:Z

    .line 43
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/j/p;->e:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/p;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->g:Ljava/lang/String;

    .line 143
    :goto_1
    return-object v0

    :cond_1
    move v2, v1

    .line 135
    goto :goto_0

    .line 138
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/p;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    :goto_2
    if-nez v0, :cond_5

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->h:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move v0, v1

    .line 138
    goto :goto_2

    .line 142
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->k()Ljava/util/List;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string v0, ""

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->g:Ljava/lang/String;

    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->h:Ljava/lang/String;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/j/p;->j()V

    .line 57
    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/p;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->a:Lcom/google/android/apps/gmm/place/i/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->a:Lcom/google/android/apps/gmm/place/i/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/e;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/j/p;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->h()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->i:Z

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->a:Lcom/google/android/apps/gmm/place/i/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/e;->b()Lcom/google/android/libraries/curvular/cf;

    .line 176
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 183
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 184
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a()V

    .line 185
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/p;->e:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 187
    const/4 v0, 0x0

    return-object v0
.end method

.method public j()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->f:Ljava/lang/String;

    .line 116
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/j/p;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 117
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->k()Ljava/util/List;

    move-result-object v4

    .line 118
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x2

    if-lt v0, v5, :cond_1

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v3

    :goto_1
    if-nez v0, :cond_4

    .line 123
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 127
    :goto_2
    new-instance v2, Lcom/google/b/a/ab;

    invoke-direct {v2, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/b/a/ab;->a()Lcom/google/b/a/ab;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->f:Ljava/lang/String;

    .line 129
    :cond_1
    return-void

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/p;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->aI:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 120
    goto :goto_1

    .line 125
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4, v3, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method
