.class public Lcom/google/android/apps/gmm/place/d/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/d/c;


# instance fields
.field private final a:Lcom/google/maps/g/fu;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/fu;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 35
    return-void
.end method

.method private a(Lcom/google/maps/g/fx;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 149
    .line 152
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    move v0, v3

    :goto_0
    if-eqz v0, :cond_8

    .line 153
    iget-object v0, p1, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    move-object v1, v0

    .line 155
    :goto_2
    iget v0, p1, Lcom/google/maps/g/fx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v5, 0x10

    if-ne v0, v5, :cond_3

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 156
    iget-object v5, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v6, Lcom/google/android/apps/gmm/l;->jV:I

    new-array v7, v3, [Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    :goto_4
    aput-object v0, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 159
    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/l;->jT:I

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 160
    new-instance v5, Lcom/google/b/a/ab;

    invoke-direct {v5, v3}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/b/a/ab;->a()Lcom/google/b/a/ab;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v4}, Lcom/google/b/a/ab;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    :goto_6
    return-object v2

    :cond_0
    move v0, v4

    .line 152
    goto :goto_0

    .line 153
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, p1, Lcom/google/maps/g/fx;->e:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move v0, v4

    .line 155
    goto :goto_3

    .line 156
    :cond_4
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iput-object v3, p1, Lcom/google/maps/g/fx;->f:Ljava/lang/Object;

    :cond_5
    move-object v0, v3

    goto :goto_4

    :cond_6
    move-object v2, v0

    .line 161
    goto :goto_6

    :cond_7
    move-object v0, v2

    goto :goto_5

    :cond_8
    move-object v1, v2

    goto :goto_2
.end method

.method private o()Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 230
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v2, Lcom/google/maps/g/fu;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/maps/g/fu;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/d/d;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    iget-object v1, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/maps/g/fc;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->jS:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 59
    invoke-virtual {v0}, Lcom/google/maps/g/fx;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/google/maps/g/fx;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 58
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ke:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->jU:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v2, "mail"

    .line 217
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v0

    .line 216
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 220
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 2

    .prologue
    .line 235
    new-instance v0, Lcom/google/android/apps/gmm/place/d/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/d/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 6

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->jO:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 71
    invoke-virtual {v0}, Lcom/google/maps/g/fx;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/google/maps/g/fx;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 70
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    iget-object v1, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/maps/g/fc;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    iget-object v1, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/maps/g/fc;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget v0, v0, Lcom/google/maps/g/fu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 136
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->jQ:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    .line 137
    iget-object v0, v6, Lcom/google/maps/g/fu;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_1
    aput-object v0, v5, v2

    .line 136
    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 139
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0

    .line 137
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v1, v6, Lcom/google/maps/g/fu;->g:Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 139
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/d/d;->a(Lcom/google/maps/g/fx;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/d/d;->a(Lcom/google/maps/g/fx;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/d;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    const-string v0, "http://www.google.com/search?q=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/d/d;->o()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lcom/google/android/apps/gmm/util/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/d;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 207
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v0

    .line 206
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 209
    const/4 v0, 0x0

    return-object v0
.end method
