.class public Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/aov;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field c:Lcom/google/android/apps/gmm/place/station/au;

.field d:Landroid/view/View;

.field e:Lcom/google/b/a/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/aa",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/aor;",
            "Lcom/google/r/b/a/aov;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 217
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/place/station/au;)Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;-><init>()V

    .line 63
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 64
    const-string v2, "viewmodel"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 65
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 4
    .param p1    # Lcom/google/n/at;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 39
    check-cast p1, Lcom/google/r/b/a/aov;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/au;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->f:Ljava/lang/String;

    const-string v2, "This fragment must have a valid vehicle token"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    if-nez v0, :cond_5

    sget v0, Lcom/google/android/apps/gmm/l;->oi:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/place/station/au;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/place/station/au;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/station/au;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/place/station/as;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/place/station/as;-><init>(Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;Lcom/google/android/apps/gmm/place/station/au;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/r/b/a/aov;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p1, Lcom/google/r/b/a/aov;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/wj;->d()Lcom/google/maps/g/wj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/wj;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/place/station/au;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/place/station/au;Lcom/google/maps/g/wj;)Lcom/google/android/apps/gmm/place/station/au;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_5
    sget-object v3, Lcom/google/android/apps/gmm/place/station/at;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/k;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/apps/gmm/l;->cg:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->ci:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/au;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    sget-object v0, Lcom/google/android/apps/gmm/place/station/au;->e:Landroid/view/View$OnClickListener;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/google/b/f/t;->fl:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 71
    if-eqz p1, :cond_1

    move-object v0, p1

    .line 75
    :goto_0
    const-string v1, "viewmodel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/au;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    .line 77
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    const-class v1, Lcom/google/r/b/a/aor;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 83
    :cond_0
    return-void

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/station/a/m;

    invoke-virtual {v0, v1, p2, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 101
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->d:Landroid/view/View;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/station/a/k;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Lcom/google/android/apps/gmm/place/station/aq;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/station/aq;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    new-instance v1, Lcom/google/android/apps/gmm/place/station/ar;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/station/ar;-><init>(Landroid/widget/TextView;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->e:Lcom/google/b/a/aa;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/b;->a()Lcom/google/android/apps/gmm/shared/net/b;

    .line 158
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 137
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 138
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 139
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    const/4 v2, 0x0

    .line 141
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 142
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v2, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 143
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/station/au;->c:Lcom/google/maps/g/wj;

    if-eqz v1, :cond_0

    :goto_0
    if-nez v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->g:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-static {}, Lcom/google/r/b/a/aor;->newBuilder()Lcom/google/r/b/a/aot;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    .line 147
    iget-object v2, v2, Lcom/google/android/apps/gmm/place/station/au;->a:Ljava/lang/String;

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 147
    :cond_1
    invoke-virtual {v1}, Lcom/google/r/b/a/aot;->c()V

    iget-object v3, v1, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    invoke-interface {v3, v2}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-virtual {v1}, Lcom/google/r/b/a/aot;->g()Lcom/google/n/t;

    move-result-object v1

    .line 146
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 150
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 92
    const-string v0, "viewmodel"

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->c:Lcom/google/android/apps/gmm/place/station/au;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 93
    return-void
.end method
