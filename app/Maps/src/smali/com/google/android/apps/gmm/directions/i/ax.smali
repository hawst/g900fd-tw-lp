.class final enum Lcom/google/android/apps/gmm/directions/i/ax;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/i/ax;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/i/ax;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/i/ax;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/i/ax;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/directions/i/ax;


# instance fields
.field public final d:I

.field public final e:I

.field public final f:Lcom/google/maps/g/a/gs;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ax;

    const-string v1, "BEST_ROUTE"

    sget v3, Lcom/google/android/apps/gmm/g;->ed:I

    sget v4, Lcom/google/android/apps/gmm/l;->fH:I

    sget-object v5, Lcom/google/maps/g/a/gs;->a:Lcom/google/maps/g/a/gs;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/ax;-><init>(Ljava/lang/String;IIILcom/google/maps/g/a/gs;)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/ax;->a:Lcom/google/android/apps/gmm/directions/i/ax;

    .line 111
    new-instance v3, Lcom/google/android/apps/gmm/directions/i/ax;

    const-string v4, "FEWER_TRANSFERS"

    sget v6, Lcom/google/android/apps/gmm/g;->ee:I

    sget v7, Lcom/google/android/apps/gmm/l;->fI:I

    sget-object v8, Lcom/google/maps/g/a/gs;->b:Lcom/google/maps/g/a/gs;

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/directions/i/ax;-><init>(Ljava/lang/String;IIILcom/google/maps/g/a/gs;)V

    sput-object v3, Lcom/google/android/apps/gmm/directions/i/ax;->b:Lcom/google/android/apps/gmm/directions/i/ax;

    .line 115
    new-instance v3, Lcom/google/android/apps/gmm/directions/i/ax;

    const-string v4, "LESS_WALKING"

    sget v6, Lcom/google/android/apps/gmm/g;->ef:I

    sget v7, Lcom/google/android/apps/gmm/l;->fJ:I

    sget-object v8, Lcom/google/maps/g/a/gs;->c:Lcom/google/maps/g/a/gs;

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/gmm/directions/i/ax;-><init>(Ljava/lang/String;IIILcom/google/maps/g/a/gs;)V

    sput-object v3, Lcom/google/android/apps/gmm/directions/i/ax;->c:Lcom/google/android/apps/gmm/directions/i/ax;

    .line 106
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/i/ax;

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/ax;->a:Lcom/google/android/apps/gmm/directions/i/ax;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/ax;->b:Lcom/google/android/apps/gmm/directions/i/ax;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/ax;->c:Lcom/google/android/apps/gmm/directions/i/ax;

    aput-object v1, v0, v10

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/ax;->g:[Lcom/google/android/apps/gmm/directions/i/ax;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/google/maps/g/a/gs;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/maps/g/a/gs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput p3, p0, Lcom/google/android/apps/gmm/directions/i/ax;->d:I

    .line 127
    iput p4, p0, Lcom/google/android/apps/gmm/directions/i/ax;->e:I

    .line 128
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/i/ax;->f:Lcom/google/maps/g/a/gs;

    .line 129
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/i/ax;
    .locals 1

    .prologue
    .line 106
    const-class v0, Lcom/google/android/apps/gmm/directions/i/ax;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/i/ax;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/i/ax;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/ax;->g:[Lcom/google/android/apps/gmm/directions/i/ax;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/i/ax;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/i/ax;

    return-object v0
.end method
