.class public Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;
.super Landroid/widget/ListView;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/indoor/ui/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/ui/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/indoor/ui/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->a:Lcom/google/android/apps/gmm/map/indoor/ui/a;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->a:Lcom/google/android/apps/gmm/map/indoor/ui/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 39
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->setVisibility(I)V

    .line 40
    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->a:Lcom/google/android/apps/gmm/map/indoor/ui/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/indoor/ui/a;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;->smoothScrollToPosition(I)V

    .line 134
    return-void
.end method
