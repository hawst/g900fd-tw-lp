.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;
.super Lcom/google/android/apps/gmm/map/legacy/a/c/b/q;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/q",
        "<",
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 1

    .prologue
    .line 716
    const-string v0, "PointLabels"

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/q;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    .line 717
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/an;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/t/l;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;
    .locals 13

    .prologue
    .line 745
    move-object/from16 v0, p4

    iget v2, v0, Lcom/google/android/apps/gmm/map/o/h;->b:F

    .line 746
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v6, 0x0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/z;FLcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/internal/c/d;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-result-object v7

    .line 750
    if-eqz v7, :cond_0

    iget-object v1, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 751
    :cond_0
    const/4 v1, 0x0

    .line 807
    :goto_0
    return-object v1

    .line 754
    :cond_1
    const/4 v9, 0x0

    .line 755
    const/4 v8, 0x0

    .line 757
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    if-eqz v1, :cond_1d

    .line 758
    const/4 v6, 0x0

    .line 759
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 760
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    const/4 v3, 0x0

    aget-object v6, v1, v3

    .line 761
    iget v1, v6, Lcom/google/android/apps/gmm/map/internal/c/e;->a:I

    shr-int/lit8 v1, v1, 0x4

    and-int/lit8 v1, v1, 0xf

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->a(I)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    move-result-object v1

    move-object v8, v1

    .line 766
    :goto_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a(Lcom/google/android/apps/gmm/map/internal/c/z;FLcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/internal/c/d;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    move-result-object v9

    .line 768
    if-nez v9, :cond_3

    .line 769
    const/4 v1, 0x0

    goto :goto_0

    .line 763
    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    move-object v8, v1

    goto :goto_1

    .line 771
    :cond_3
    iget-object v1, v9, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 772
    const/4 v9, 0x0

    move-object v10, v8

    .line 778
    :goto_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    const/4 v2, 0x0

    aget-object v5, v1, v2

    .line 779
    iget-object v8, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->l:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 781
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_f

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v1, :cond_a

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_9

    :cond_4
    const/4 v1, 0x1

    :goto_4
    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_5
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_6

    if-nez v1, :cond_6

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_6
    if-nez v1, :cond_6

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->w:Z

    if-nez v1, :cond_6

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v1, :cond_c

    const/4 v1, 0x1

    :goto_7
    if-nez v1, :cond_6

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    :cond_5
    const/4 v1, 0x1

    :goto_8
    if-eqz v1, :cond_e

    :cond_6
    const/4 v1, 0x1

    :goto_9
    if-eqz v1, :cond_f

    const/4 v11, 0x1

    .line 782
    :goto_a
    if-nez p7, :cond_1b

    .line 783
    if-eqz p3, :cond_10

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/gmm/map/o/ak;->c()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v1, v2, :cond_10

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->s:Lcom/google/android/apps/gmm/map/t/l;

    :goto_b
    move-object v12, v1

    .line 786
    :goto_c
    const/4 v6, 0x0

    .line 790
    if-nez p8, :cond_7

    .line 791
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->f:F

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(Lcom/google/android/apps/gmm/map/o/ak;F)F

    move-result v6

    .line 793
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;->b()Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;

    move-object v2, p1

    move-object/from16 v3, p3

    move v4, p2

    .line 794
    invoke-static/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;->a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;ILcom/google/android/apps/gmm/map/internal/c/a;FLcom/google/android/apps/gmm/map/legacy/a/c/b/s;Lcom/google/android/apps/gmm/map/internal/c/e;Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;ZLcom/google/android/apps/gmm/map/t/l;)V

    .line 806
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->q:Lcom/google/b/f/cq;

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->s:Lcom/google/b/f/cq;

    goto/16 :goto_0

    .line 781
    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    goto :goto_4

    :cond_a
    const/4 v1, 0x0

    goto :goto_5

    :cond_b
    const/4 v1, 0x0

    goto :goto_6

    :cond_c
    const/4 v1, 0x0

    goto :goto_7

    :cond_d
    const/4 v1, 0x0

    goto :goto_8

    :cond_e
    const/4 v1, 0x0

    goto :goto_9

    :cond_f
    const/4 v11, 0x0

    goto :goto_a

    .line 783
    :cond_10
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v1, :cond_11

    const/4 v1, 0x1

    :goto_d
    if-eqz v1, :cond_12

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->u:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_b

    :cond_11
    const/4 v1, 0x0

    goto :goto_d

    :cond_12
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/an;->f()Z

    move-result v1

    if-eqz v1, :cond_15

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    :goto_e
    if-eqz v1, :cond_14

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->w:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_b

    :cond_13
    const/4 v1, 0x0

    goto :goto_e

    :cond_14
    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->v:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_b

    :cond_15
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-eqz v1, :cond_16

    const/4 v1, 0x1

    :goto_f
    if-eqz v1, :cond_17

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->t:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_b

    :cond_16
    const/4 v1, 0x0

    goto :goto_f

    :cond_17
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    :cond_18
    const/4 v1, 0x1

    :goto_10
    if-eqz v1, :cond_1a

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->p:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_b

    :cond_19
    const/4 v1, 0x0

    goto :goto_10

    :cond_1a
    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->n:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_b

    :cond_1b
    move-object/from16 v12, p7

    goto :goto_c

    :cond_1c
    move-object v10, v8

    goto/16 :goto_2

    :cond_1d
    move-object v10, v8

    goto/16 :goto_2
.end method

.method protected final synthetic a()Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
    .locals 1

    .prologue
    .line 714
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;-><init>()V

    return-object v0
.end method
