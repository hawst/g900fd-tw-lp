.class Lcom/google/android/apps/gmm/car/f/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/ac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/f/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/f/d;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/k;->a:Lcom/google/android/apps/gmm/car/f/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bm;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 385
    sget-object v0, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "detailsFetchCompleteCallback: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/k;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/k;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    if-eq p1, v0, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/k;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    sget-object v1, Lcom/google/android/apps/gmm/car/f/p;->a:Ljava/lang/String;

    const-string v1, "onPlaceDetailsFetched(): placemark %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/f/p;->k:Lcom/google/android/apps/gmm/car/bm;

    iget-object v4, v4, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/p;->k:Lcom/google/android/apps/gmm/car/bm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/car/f/y;->a(Z)Lcom/google/android/apps/gmm/car/f/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/f/p;->a(Lcom/google/android/apps/gmm/car/f/y;)V

    .line 393
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_3

    .line 394
    sget-object v0, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    .line 399
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/k;->a:Lcom/google/android/apps/gmm/car/f/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/d;->e()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 396
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/k;->a:Lcom/google/android/apps/gmm/car/f/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/d;->f()V

    goto :goto_1
.end method
