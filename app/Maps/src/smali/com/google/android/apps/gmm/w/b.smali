.class Lcom/google/android/apps/gmm/w/b;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/v/cp;

.field private b:Lcom/google/android/apps/gmm/v/cp;

.field private c:Lcom/google/android/apps/gmm/map/b/a/y;

.field private d:F

.field private e:Lcom/google/android/apps/gmm/map/b/a/ag;

.field private f:Lcom/google/maps/g/f/e;

.field private g:Lcom/google/maps/g/hy;

.field private h:Lcom/google/android/apps/gmm/map/b/a/ag;

.field private final i:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 272
    iput-object v1, p0, Lcom/google/android/apps/gmm/w/b;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 274
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/w/b;->d:F

    .line 276
    iput-object v1, p0, Lcom/google/android/apps/gmm/w/b;->e:Lcom/google/android/apps/gmm/map/b/a/ag;

    .line 278
    iput-object v1, p0, Lcom/google/android/apps/gmm/w/b;->f:Lcom/google/maps/g/f/e;

    .line 299
    iput-object p1, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    .line 300
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->e:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/ag;)V
    .locals 1

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/w/b;->h:Lcom/google/android/apps/gmm/map/b/a/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    monitor-exit p0

    return-void

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->A()Lcom/google/android/apps/gmm/v/cp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/b;->a:Lcom/google/android/apps/gmm/v/cp;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->B()Lcom/google/android/apps/gmm/v/cp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/b;->b:Lcom/google/android/apps/gmm/v/cp;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->a:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->b:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 308
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/maps/g/hy;)V
    .locals 1

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/w/b;->g:Lcom/google/maps/g/hy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    monitor-exit p0

    return-void

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 3

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->a:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->b:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 363
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/w/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/w/c;-><init>(Lcom/google/android/apps/gmm/w/b;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method declared-synchronized c()V
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 321
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/w/b;->g:Lcom/google/maps/g/hy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    .line 352
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 325
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 328
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v5

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v7, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/w/b;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v3, :cond_4

    sget-object v0, Lcom/google/b/f/bc;->n:Lcom/google/b/f/bc;

    .line 329
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 333
    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/w/b;->g:Lcom/google/maps/g/hy;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/f;-><init>(Lcom/google/maps/g/hy;)V

    .line 334
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 335
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 337
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    .line 338
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v3

    .line 339
    invoke-static {}, Lcom/google/maps/g/f/a;->newBuilder()Lcom/google/maps/g/f/c;

    move-result-object v5

    sget-object v1, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    iget-object v6, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v1, Lcom/google/maps/g/f/e;->d:Lcom/google/maps/g/f/e;

    :cond_3
    if-nez v1, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 328
    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/w/b;->e:Lcom/google/android/apps/gmm/map/b/a/ag;

    iget-object v8, p0, Lcom/google/android/apps/gmm/w/b;->h:Lcom/google/android/apps/gmm/map/b/a/ag;

    if-eq v3, v8, :cond_5

    if-eqz v3, :cond_6

    invoke-virtual {v3, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    move v3, v2

    :goto_2
    if-nez v3, :cond_7

    sget-object v0, Lcom/google/b/f/bc;->p:Lcom/google/b/f/bc;

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    iget-object v8, p0, Lcom/google/android/apps/gmm/w/b;->f:Lcom/google/maps/g/f/e;

    sget-object v3, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    iget-object v9, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v9, v9, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v9

    if-eqz v9, :cond_8

    sget-object v3, Lcom/google/maps/g/f/e;->d:Lcom/google/maps/g/f/e;

    :cond_8
    if-eq v8, v3, :cond_9

    if-eqz v8, :cond_a

    invoke-virtual {v8, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_9
    move v1, v2

    :cond_a
    if-nez v1, :cond_b

    sget-object v0, Lcom/google/b/f/bc;->m:Lcom/google/b/f/bc;

    goto/16 :goto_1

    :cond_b
    iget v1, p0, Lcom/google/android/apps/gmm/w/b;->d:F

    sub-float/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v2, v1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v8

    if-ltz v1, :cond_c

    sget-object v0, Lcom/google/b/f/bc;->q:Lcom/google/b/f/bc;

    goto/16 :goto_1

    :cond_c
    iget v1, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/w/b;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v2, v10

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v7, v5, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v3, v7

    div-float/2addr v2, v3

    div-float/2addr v1, v2

    float-to-double v2, v1

    iget v1, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v6, p0, Lcom/google/android/apps/gmm/w/b;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget v6, v5, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v6, v10

    iget v7, v5, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v8, v5, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    div-float/2addr v6, v7

    div-float/2addr v1, v6

    float-to-double v6, v1

    iget-object v1, v5, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    int-to-double v8, v1

    iget-object v1, v5, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    int-to-double v10, v1

    cmpl-double v1, v2, v8

    if-gez v1, :cond_d

    cmpl-double v1, v6, v10

    if-ltz v1, :cond_e

    :cond_d
    sget-object v0, Lcom/google/b/f/bc;->o:Lcom/google/b/f/bc;

    goto/16 :goto_1

    :cond_e
    mul-double v12, v8, v10

    sub-double v2, v8, v2

    sub-double v6, v10, v6

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v12

    cmpg-double v1, v2, v6

    if-gtz v1, :cond_2

    sget-object v0, Lcom/google/b/f/bc;->o:Lcom/google/b/f/bc;

    goto/16 :goto_1

    .line 339
    :cond_f
    iget v6, v5, Lcom/google/maps/g/f/c;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/maps/g/f/c;->a:I

    iget v1, v1, Lcom/google/maps/g/f/e;->f:I

    iput v1, v5, Lcom/google/maps/g/f/c;->b:I

    invoke-virtual {v5}, Lcom/google/maps/g/f/c;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/f/a;

    .line 337
    invoke-interface {v2, v3, v1, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/maps/a/a;Lcom/google/maps/g/f/a;Lcom/google/maps/g/hy;)V

    .line 342
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/b;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 343
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iput v0, p0, Lcom/google/android/apps/gmm/w/b;->d:F

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->h:Lcom/google/android/apps/gmm/map/b/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/b;->e:Lcom/google/android/apps/gmm/map/b/a/ag;

    .line 345
    sget-object v0, Lcom/google/maps/g/f/e;->a:Lcom/google/maps/g/f/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v1

    if-eqz v1, :cond_10

    sget-object v0, Lcom/google/maps/g/f/e;->d:Lcom/google/maps/g/f/e;

    :cond_10
    iput-object v0, p0, Lcom/google/android/apps/gmm/w/b;->f:Lcom/google/maps/g/f/e;

    .line 346
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/b;->g:Lcom/google/maps/g/hy;

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/b;->i:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/w/a/b;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/w/a/b;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
