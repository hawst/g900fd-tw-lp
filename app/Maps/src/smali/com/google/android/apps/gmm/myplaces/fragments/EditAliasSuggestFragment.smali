.class public Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;
.super Lcom/google/android/apps/gmm/suggest/SuggestFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/suggest/a/a;


# static fields
.field static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;-><init>()V

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ZZ)Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 57
    if-eqz p1, :cond_0

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 59
    :cond_2
    new-instance v3, Lcom/google/android/apps/gmm/suggest/k;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/suggest/k;-><init>()V

    .line 60
    if-nez p1, :cond_3

    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->e:Lcom/google/android/apps/gmm/suggest/e/c;

    iget-object v4, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 62
    sget v0, Lcom/google/android/apps/gmm/l;->mX:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/suggest/l;->b(Ljava/lang/String;)V

    .line 63
    sget v0, Lcom/google/android/apps/gmm/f;->ck:I

    sget v4, Lcom/google/android/apps/gmm/l;->mX:I

    iget-object v5, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v5, v0, v4}, Lcom/google/android/apps/gmm/suggest/l;->a(II)V

    .line 71
    :goto_1
    iget-object v0, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 72
    iget-object v0, v3, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 73
    iget-object v0, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/suggest/l;->a(Z)V

    .line 74
    iget-object v0, v3, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Z)V

    .line 75
    const v0, 0x12000006

    iget-object v2, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->b(I)V

    .line 78
    iget-object v0, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/suggest/l;->e(Z)V

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/myplaces/fragments/c;-><init>()V

    .line 83
    iput-boolean p3, v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;->b:Z

    .line 84
    iput-boolean p4, v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;->a:Z

    .line 85
    iget-object v1, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/l;->a:Ljava/io/Serializable;

    .line 87
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;-><init>()V

    .line 92
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 93
    invoke-virtual {v1, v0, v3, v6, v6}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)V

    .line 95
    return-object v1

    .line 66
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/suggest/e/c;->f:Lcom/google/android/apps/gmm/suggest/e/c;

    iget-object v4, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 67
    sget v0, Lcom/google/android/apps/gmm/l;->mY:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/suggest/l;->b(Ljava/lang/String;)V

    .line 68
    sget v0, Lcom/google/android/apps/gmm/f;->cp:I

    sget v4, Lcom/google/android/apps/gmm/l;->mY:I

    iget-object v5, v3, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v5, v0, v4}, Lcom/google/android/apps/gmm/suggest/l;->a(II)V

    goto :goto_1
.end method

.method private a(ILjava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 181
    if-eqz p1, :cond_0

    if-ne p1, v1, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v2, v0

    goto :goto_0

    .line 184
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v2, v1

    :goto_1
    if-nez v2, :cond_4

    move v1, v0

    .line 190
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    .line 191
    new-instance v2, Lcom/google/android/apps/gmm/myplaces/fragments/a;

    invoke-direct {v2, p0, p3, v0, p1}, Lcom/google/android/apps/gmm/myplaces/fragments/a;-><init>(Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;ZLcom/google/android/apps/gmm/myplaces/a/a;I)V

    .line 234
    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/a/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 233
    invoke-static {v1, p1, p2, v0, v2}, Lcom/google/android/apps/gmm/myplaces/i;->a(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/myplaces/j;)Lcom/google/android/apps/gmm/myplaces/i;

    move-result-object v1

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 236
    return-void

    :cond_5
    move v2, v0

    .line 184
    goto :goto_1
.end method

.method private i()I
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/fragments/b;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/e/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 117
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 113
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 115
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->i()I

    move-result v3

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    if-gez v3, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v4, p1, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/l;->a:Ljava/io/Serializable;

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;

    .line 151
    if-eqz v0, :cond_4

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;->a:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    invoke-direct {p0, v3, v4, v1}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->a(ILjava/lang/String;Z)V

    .line 153
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;->b:Z

    if-eqz v0, :cond_0

    .line 154
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    .line 155
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->j:Z

    .line 156
    new-instance v4, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    .line 157
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    .line 158
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->m:[B

    iput-object v0, v4, Lcom/google/android/apps/gmm/search/aj;->e:[B

    .line 159
    if-eqz p3, :cond_5

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/suggest/d/e;->a()Lcom/google/r/b/a/zd;

    move-result-object v0

    sget-object v5, Lcom/google/r/b/a/b/aq;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    :goto_2
    iput-object v0, v4, Lcom/google/android/apps/gmm/search/aj;->t:Lcom/google/e/a/a/a/b;

    .line 160
    iput-object p2, v4, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 161
    if-nez v3, :cond_6

    .line 162
    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/f;->b:Lcom/google/android/apps/gmm/base/placelists/a/f;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    .line 167
    :cond_3
    :goto_3
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_0

    .line 151
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 159
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 163
    :cond_6
    if-ne v3, v2, :cond_3

    .line 164
    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/f;->c:Lcom/google/android/apps/gmm/base/placelists/a/f;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->i()I

    move-result v1

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    if-gez v1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/l;->a:Ljava/io/Serializable;

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;

    .line 132
    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/myplaces/fragments/c;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->a(ILjava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final c()Lcom/google/android/apps/gmm/suggest/a/a;
    .locals 0

    .prologue
    .line 100
    return-object p0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->onResume()V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/suggest/g/c;->l:Lcom/google/android/apps/gmm/z/b/l;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/fragments/EditAliasSuggestFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/suggest/g/c;->i:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 108
    :cond_0
    return-void
.end method
