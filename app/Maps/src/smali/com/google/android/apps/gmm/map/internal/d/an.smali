.class public Lcom/google/android/apps/gmm/map/internal/d/an;
.super Lcom/google/android/apps/gmm/map/internal/d/be;
.source "PG"


# static fields
.field static final q:[Ljava/lang/String;


# instance fields
.field final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/k/a;",
            ">;"
        }
    .end annotation
.end field

.field volatile s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/k/a;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field private v:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http://mt0.google.com/vt/icon/name=icons/spotlight/star_M_8x.png&scale=4"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "http://mt0.google.com/vt/icon/name=icons/spotlight/home-M_8x.png&scale=4"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "http://mt0.google.com/vt/icon/name=icons/spotlight/work-M_8x.png&scale=4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/an;->q:[Ljava/lang/String;

    .line 236
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/z;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;IFLjava/util/Locale;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 81
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v8, v7

    move-object v9, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/d/be;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;IFLjava/util/Locale;ZLjava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->r:Ljava/util/List;

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->u:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 91
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/internal/d/ar;ILcom/google/android/apps/gmm/map/internal/c/bo;)V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ar;->b:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ar;->b:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ar;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-interface {v0, v1, p1, p2, v2}, Lcom/google/android/apps/gmm/map/internal/d/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 187
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v1

    .line 189
    if-nez v1, :cond_1

    .line 190
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/ar;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/ar;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 196
    :cond_0
    :goto_0
    return-object v0

    .line 193
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 196
    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Z)Lcom/google/android/apps/gmm/map/internal/d/ak;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V
    .locals 3

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ar;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/ar;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 171
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Z)V
    .locals 3

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ar;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/ar;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 178
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/k/c;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 95
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/k/c;->a:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->s:Ljava/util/List;

    .line 96
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/an;->h()V

    .line 97
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/au;->a(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/an;->h()V

    .line 109
    return-void
.end method

.method protected final b(Z)Lcom/google/android/apps/gmm/map/internal/d/i;
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/aq;-><init>()V

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, -0x1

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/an;->start()V

    .line 115
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 121
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 123
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->u:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 124
    return-void

    .line 119
    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->u:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->b:Lcom/google/android/apps/gmm/map/internal/d/b/as;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/as;->a()Z

    .line 135
    :cond_1
    return-void
.end method

.method public final k()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 140
    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 146
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ao;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/d/ao;-><init>(Lcom/google/android/apps/gmm/map/internal/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/an;->v:Landroid/os/Handler;

    .line 160
    monitor-enter p0

    .line 161
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 162
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 164
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/an;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
