.class public Lcom/google/android/apps/gmm/search/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/a/e;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/gmm/base/g/a;

.field private final c:I

.field private final d:Landroid/view/View$OnClickListener;

.field private e:Lcom/google/android/apps/gmm/search/e/a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/a;ILandroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/c;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    .line 40
    iput p3, p0, Lcom/google/android/apps/gmm/search/c;->c:I

    .line 41
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/c;->d:Landroid/view/View$OnClickListener;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 66
    sget v0, Lcom/google/android/apps/gmm/h;->an:I

    return v0
.end method

.method public final a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/views/a/f;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/search/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/d;-><init>()V

    .line 55
    iput-object p1, v0, Lcom/google/android/apps/gmm/search/d;->a:Landroid/view/View;

    .line 56
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/a/f;)V
    .locals 5

    .prologue
    .line 46
    check-cast p1, Lcom/google/android/apps/gmm/search/d;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/search/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/c;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/search/d;->a:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/c;->d:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/search/d/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/a;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/c;->e:Lcom/google/android/apps/gmm/search/e/a;

    .line 49
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/d;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/c;->e:Lcom/google/android/apps/gmm/search/e/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 50
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/gmm/search/c;->c:I

    return v0
.end method
