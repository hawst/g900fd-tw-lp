.class public final Lcom/google/android/apps/gmm/map/f/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:F

.field b:F

.field c:F

.field d:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a(F)F
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    .line 134
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/y;->c:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_0

    .line 141
    :goto_0
    return v0

    .line 138
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/y;->a:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/y;->b:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/y;->a:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    .line 139
    const v1, 0x3f8ccccd    # 1.1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/y;->c:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/y;->a:F

    sub-float v2, v0, v2

    mul-float/2addr v1, v2

    .line 140
    iget v2, p0, Lcom/google/android/apps/gmm/map/f/y;->a:F

    float-to-double v2, v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    float-to-double v4, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    sub-double/2addr v2, v4

    double-to-float v0, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 141
    goto :goto_0
.end method
