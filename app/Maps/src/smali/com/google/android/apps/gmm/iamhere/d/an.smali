.class public Lcom/google/android/apps/gmm/iamhere/d/an;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Z

.field c:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/d;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/d;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/iamhere/d/av;

.field f:Z

.field g:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/ad;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/ad;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/d;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/android/apps/gmm/iamhere/d/au;

.field private k:Lcom/google/android/apps/gmm/iamhere/d/af;

.field private l:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/d/ad;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/google/android/apps/gmm/iamhere/d/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 89
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->b:Z

    .line 97
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->f:Z

    return-void
.end method

.method private static a(Landroid/content/Intent;)Lcom/google/android/apps/gmm/iamhere/d/at;
    .locals 3

    .prologue
    .line 113
    const-string v0, "notification_activity_intent_type"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/at;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/d/at;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 120
    :goto_0
    return-object v0

    .line 118
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    const-string v1, "Unknown intent: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 120
    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/at;->a:Lcom/google/android/apps/gmm/iamhere/d/at;

    goto :goto_0

    .line 118
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/iamhere/d/an;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 38
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aY:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v3, 0x1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->f:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->a()V

    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aX:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->finish()V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const v5, -0x21524111

    .line 196
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->f:Z

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->m:Lcom/google/android/apps/gmm/iamhere/d/aw;

    new-instance v1, Lcom/google/android/apps/gmm/iamhere/d/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/d/aw;->a:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v2, v2, Lcom/google/android/apps/gmm/iamhere/d/an;->h:Lcom/google/android/libraries/curvular/cg;

    iget-object v3, v0, Lcom/google/android/apps/gmm/iamhere/d/aw;->a:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v3, v3, Lcom/google/android/apps/gmm/iamhere/d/an;->g:Lcom/google/android/libraries/curvular/cg;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/iamhere/d/ae;-><init>(Lcom/google/android/libraries/curvular/cg;Lcom/google/android/libraries/curvular/cg;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/d/aw;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v2, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/aw;->a:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/aw;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->setContentView(Landroid/view/View;)V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/as;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/iamhere/d/an;->a(Landroid/content/Intent;)Lcom/google/android/apps/gmm/iamhere/d/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/d/at;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 202
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->k:Lcom/google/android/apps/gmm/iamhere/d/af;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "card_action_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {}, Lcom/google/android/apps/gmm/iamhere/c/g;->values()[Lcom/google/android/apps/gmm/iamhere/c/g;

    move-result-object v2

    aget-object v1, v2, v1

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/al;->a:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/c/g;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Landroid/app/Activity;Ljava/lang/String;)V

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/af;->c:Lcom/google/android/apps/gmm/iamhere/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/au;->a:Lcom/google/android/apps/gmm/iamhere/d/p;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/af;->a:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Z)V

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Z)V

    goto :goto_1

    :pswitch_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/iamhere/d/ag;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/iamhere/d/ag;-><init>(Lcom/google/android/apps/gmm/iamhere/d/af;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/d/am;)V

    goto :goto_1

    .line 205
    :pswitch_4
    iget-object v7, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->e:Lcom/google/android/apps/gmm/iamhere/d/av;

    iget-object v0, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "disambiguation_debug_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/av;->b:Ljava/util/List;

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/av;->b:Ljava/util/List;

    iget-object v3, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->d:Lcom/google/android/apps/gmm/iamhere/d/au;

    iget-object v3, v3, Lcom/google/android/apps/gmm/iamhere/d/au;->a:Lcom/google/android/apps/gmm/iamhere/d/p;

    if-eqz v3, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/av;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/d/p;->d()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/d/p;->e()Ljava/util/List;

    move-result-object v0

    :cond_2
    invoke-virtual {v7, v2, v1}, Lcom/google/android/apps/gmm/iamhere/d/av;->a(ZLjava/util/List;)Ljava/util/List;

    move-result-object v3

    if-eqz v2, :cond_3

    invoke-virtual {v7, v2, v0}, Lcom/google/android/apps/gmm/iamhere/d/av;->a(ZLjava/util/List;)Ljava/util/List;

    move-result-object v4

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/h;

    iget-object v1, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/iamhere/d/an;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/gmm/iamhere/d/an;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v3, v3, Lcom/google/android/apps/gmm/iamhere/d/an;->c:Lcom/google/android/libraries/curvular/cg;

    iget-object v4, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v4, v4, Lcom/google/android/apps/gmm/iamhere/d/an;->d:Lcom/google/android/libraries/curvular/cg;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/d/h;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/curvular/cg;Lcom/google/android/libraries/curvular/cg;)V

    :goto_3
    iget-object v1, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->e:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iget-object v1, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v2, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->e:Lcom/google/android/libraries/curvular/ae;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/d/an;->setContentView(Landroid/view/View;)V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/d/d;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->f:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    goto :goto_2

    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/h;

    iget-object v1, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/iamhere/d/an;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/gmm/iamhere/d/an;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v5, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v5, v5, Lcom/google/android/apps/gmm/iamhere/d/an;->c:Lcom/google/android/libraries/curvular/cg;

    iget-object v6, v7, Lcom/google/android/apps/gmm/iamhere/d/av;->c:Lcom/google/android/apps/gmm/iamhere/d/an;

    iget-object v6, v6, Lcom/google/android/apps/gmm/iamhere/d/an;->d:Lcom/google/android/libraries/curvular/cg;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/iamhere/d/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/libraries/curvular/cg;Lcom/google/android/libraries/curvular/cg;)V

    goto :goto_3

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 202
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final a(Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V
    .locals 5

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/au;->a:Lcom/google/android/apps/gmm/iamhere/d/p;

    .line 241
    if-eqz v0, :cond_0

    .line 242
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    const-string v1, "Logging HereNotificationReport UserEvent: %s VeType: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 246
    invoke-virtual {p1}, Lcom/google/j/d/a/w;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/b/f/t;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 244
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 242
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 251
    :goto_0
    return-void

    .line 249
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->a(Landroid/content/Intent;)Lcom/google/android/apps/gmm/iamhere/d/at;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/at;->c:Lcom/google/android/apps/gmm/iamhere/d/at;

    if-ne v0, v1, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->onResume()V

    .line 110
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 125
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 127
    new-instance v0, Lcom/google/android/libraries/curvular/cd;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/cd;-><init>(Landroid/content/Context;)V

    .line 128
    new-instance v1, Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/curvular/bd;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cc;)V

    .line 130
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 131
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 132
    new-instance v3, Lcom/google/android/libraries/curvular/j;

    invoke-direct {v3, v1}, Lcom/google/android/libraries/curvular/j;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    new-instance v3, Lcom/google/android/apps/gmm/base/k/p;

    .line 134
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-direct {v3, v1, v4, v0}, Lcom/google/android/apps/gmm/base/k/p;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V

    .line 133
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v0, Lcom/google/android/libraries/curvular/an;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/an;-><init>(Ljava/util/List;)V

    iput-object v0, v1, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    .line 136
    iget-object v0, v1, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v1, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    iget-object v2, v1, Lcom/google/android/libraries/curvular/bd;->g:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v0, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 138
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/c;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->i:Lcom/google/android/libraries/curvular/ae;

    .line 139
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/au;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/au;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/av;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/gmm/iamhere/d/av;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;Lcom/google/android/apps/gmm/iamhere/d/au;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->e:Lcom/google/android/apps/gmm/iamhere/d/av;

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/af;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/gmm/iamhere/d/af;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;Lcom/google/android/apps/gmm/iamhere/d/au;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->k:Lcom/google/android/apps/gmm/iamhere/d/af;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/d/au;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/iamhere/d/v;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/ao;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/ao;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->c:Lcom/google/android/libraries/curvular/cg;

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/ap;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/ap;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->d:Lcom/google/android/libraries/curvular/cg;

    .line 158
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/ac;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->l:Lcom/google/android/libraries/curvular/ae;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/aw;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/aw;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->m:Lcom/google/android/apps/gmm/iamhere/d/aw;

    .line 161
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/aq;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/aq;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->g:Lcom/google/android/libraries/curvular/cg;

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/ar;-><init>(Lcom/google/android/apps/gmm/iamhere/d/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->h:Lcom/google/android/libraries/curvular/cg;

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->k:Lcom/google/android/apps/gmm/iamhere/d/af;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->i:Lcom/google/android/libraries/curvular/ae;

    iput-object v0, v1, Lcom/google/android/apps/gmm/iamhere/d/af;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v1, Lcom/google/android/apps/gmm/iamhere/d/af;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, v1, Lcom/google/android/apps/gmm/iamhere/d/af;->e:Lcom/google/android/apps/gmm/base/a;

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->e:Lcom/google/android/apps/gmm/iamhere/d/av;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->i:Lcom/google/android/libraries/curvular/ae;

    iput-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/av;->e:Lcom/google/android/libraries/curvular/ae;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->m:Lcom/google/android/apps/gmm/iamhere/d/aw;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->l:Lcom/google/android/libraries/curvular/ae;

    iput-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/aw;->b:Lcom/google/android/libraries/curvular/ae;

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->f:Z

    .line 179
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 220
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/an;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/au;->b:Lcom/google/android/apps/gmm/iamhere/d/an;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/iamhere/d/an;->unbindService(Landroid/content/ServiceConnection;)V

    .line 222
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 183
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/iamhere/d/an;->setIntent(Landroid/content/Intent;)V

    .line 184
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 214
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->b:Z

    .line 216
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->b:Z

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/an;->j:Lcom/google/android/apps/gmm/iamhere/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/au;->a:Lcom/google/android/apps/gmm/iamhere/d/p;

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/an;->a()V

    .line 193
    :cond_0
    return-void
.end method
