.class public Lcom/google/android/apps/gmm/directions/i/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/e;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

.field private final b:Lcom/google/android/apps/gmm/directions/av;

.field private final c:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;Lcom/google/android/apps/gmm/directions/av;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/k;->b:Lcom/google/android/apps/gmm/directions/av;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/l;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/directions/i/l;-><init>(Lcom/google/android/apps/gmm/directions/i/k;Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;Lcom/google/android/apps/gmm/directions/av;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->c:Lcom/google/android/libraries/curvular/cg;

    .line 56
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 105
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/a/hm;)Lcom/google/android/libraries/curvular/cf;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "TravelMode selected: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v0, v2, :cond_0

    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    .line 113
    :cond_1
    :goto_0
    return-object v1

    .line 111
    :cond_2
    iget-object v4, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v4

    :try_start_0
    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/maps/g/a/hm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v2

    iget-object v0, v2, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ho;->d()Lcom/google/maps/g/a/ho;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ho;

    iget v0, v0, Lcom/google/maps/g/a/ho;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/hr;->a(I)Lcom/google/maps/g/a/hr;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/a/hr;->a:Lcom/google/maps/g/a/hr;

    :cond_4
    sget-object v5, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    if-eq v0, v5, :cond_6

    invoke-static {v2}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/g/a/ho;->newBuilder()Lcom/google/maps/g/a/hq;

    move-result-object v2

    sget-object v5, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    if-nez v5, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v6, v2, Lcom/google/maps/g/a/hq;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v2, Lcom/google/maps/g/a/hq;->a:I

    iget v5, v5, Lcom/google/maps/g/a/hr;->d:I

    iput v5, v2, Lcom/google/maps/g/a/hq;->c:I

    iget-object v5, v0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/a/hq;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v5, Lcom/google/n/ao;->d:Z

    iget v2, v0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v0, Lcom/google/r/b/a/agb;->a:I

    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    move-object v2, v0

    :cond_6
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-static {p1, v0, v2}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;

    move-result-object v0

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/a/hm;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/r/b/a/afz;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    iget-object v2, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Landroid/content/Context;)V

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Travel mode is changed: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/maps/g/a/hm;)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/aq;->a:[I

    invoke-virtual {p1}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_7

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    :cond_7
    if-nez v2, :cond_8

    move-object v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->o()V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c()Z

    goto/16 :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/b/f/t;->n:Lcom/google/b/f/t;

    move-object v2, v0

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/google/b/f/t;->m:Lcom/google/b/f/t;

    move-object v2, v0

    goto :goto_1

    :pswitch_2
    sget-object v0, Lcom/google/b/f/t;->p:Lcom/google/b/f/t;

    move-object v2, v0

    goto :goto_1

    :pswitch_3
    sget-object v0, Lcom/google/b/f/t;->o:Lcom/google/b/f/t;

    move-object v2, v0

    goto :goto_1

    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v6, v5, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, v5, Lcom/google/maps/g/ia;->a:I

    iput-boolean v4, v5, Lcom/google/maps/g/ia;->f:Z

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->b:Lcom/google/android/apps/gmm/directions/av;

    .line 123
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    .line 122
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->a(Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    .line 124
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->h()Lcom/google/android/apps/gmm/directions/a/g;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    sget-object v1, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 127
    :cond_0
    return-object v3
.end method

.method public final b(Lcom/google/maps/g/a/hm;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->c:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final c(Lcom/google/maps/g/a/hm;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/k;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/maps/g/a/hm;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
