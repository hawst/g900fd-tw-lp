.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;
.super Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/q;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;)V
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/q;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;I)V

    .line 340
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;IFLcom/google/android/apps/gmm/map/internal/c/bi;Lcom/google/android/apps/gmm/map/internal/c/be;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    aput v1, v0, v1

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    aput v1, v0, v8

    move v2, v1

    .line 347
    :goto_0
    const/4 v0, 0x4

    if-ge v2, v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    aget v3, v0, v1

    shl-int/lit8 v3, v3, 0x8

    aput v3, v0, v1

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    aget v3, v0, v8

    shl-int/lit8 v3, v3, 0x8

    aput v3, v0, v8

    .line 351
    if-ge v2, p2, :cond_2

    .line 353
    iget-object v0, p5, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, v2

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    mul-float/2addr v0, p3

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v0, v3

    .line 355
    const-wide v4, 0x40efffe000000000L    # 65535.0

    const v3, 0x477fff00    # 65535.0f

    mul-float/2addr v0, v3

    float-to-double v6, v0

    .line 356
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-int v0, v4

    .line 358
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    aget v4, v3, v1

    div-int/lit16 v5, v0, 0x100

    or-int/2addr v4, v5

    aput v4, v3, v1

    .line 359
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    aget v4, v3, v8

    rem-int/lit16 v0, v0, 0x100

    or-int/2addr v0, v4

    aput v0, v3, v8

    .line 347
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/o;->a:[I

    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/n;->a:Ljava/nio/ByteBuffer;

    aget v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 362
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
