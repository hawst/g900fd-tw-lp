.class Lcom/google/android/apps/gmm/directions/i/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/curvular/cg",
        "<",
        "Lcom/google/android/libraries/curvular/ce;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

.field final synthetic b:Lcom/google/android/apps/gmm/directions/av;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/i/k;Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;Lcom/google/android/apps/gmm/directions/av;)V
    .locals 0

    .prologue
    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/l;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/l;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/l;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/l;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 46
    new-instance v1, Lcom/google/android/apps/gmm/base/support/a;

    invoke-direct {v1, v0, p2}, Lcom/google/android/apps/gmm/base/support/a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 47
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/l;->b:Lcom/google/android/apps/gmm/directions/av;

    .line 49
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v2

    .line 50
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v3

    .line 51
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 48
    new-instance v4, Lcom/google/b/c/cx;

    invoke-direct {v4}, Lcom/google/b/c/cx;-><init>()V

    invoke-static {v4, v2, v3, v0}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/b/c/cx;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)V

    invoke-virtual {v4}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    .line 47
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/support/a;->a(Ljava/util/List;)V

    .line 52
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/support/a;->show()V

    .line 54
    :cond_0
    return-void
.end method
