.class public abstract Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/ui/g;

.field public b:Lcom/google/android/apps/gmm/map/b/a/f;

.field public c:Z

.field private final d:Landroid/widget/ImageView;

.field private e:F

.field private f:F

.field private g:Landroid/graphics/Matrix;

.field private h:Landroid/graphics/Matrix;

.field private i:Lcom/google/android/apps/gmm/map/ui/h;

.field private final j:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->a:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    .line 70
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 303
    new-instance v0, Lcom/google/android/apps/gmm/map/ui/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/ui/e;-><init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->j:Landroid/view/View$OnClickListener;

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a(Landroid/content/Context;)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/widget/ImageView;
.end method

.method public abstract a()Lcom/google/android/apps/gmm/map/f/o;
.end method

.method public final a(FF)V
    .locals 0

    .prologue
    .line 154
    iput p1, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->e:F

    .line 155
    iput p2, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->f:F

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    .line 157
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b()Lcom/google/android/apps/gmm/map/util/b/a/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    .line 218
    return-void
.end method

.method public abstract b()Lcom/google/android/apps/gmm/map/util/b/a/a;
.end method

.method public abstract c()Lcom/google/android/apps/gmm/v/ad;
.end method

.method public d()V
    .locals 9

    .prologue
    const v8, 0x43b3c000    # 359.5f

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->g:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->h:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->h:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->h:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->e:F

    neg-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    .line 192
    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v7

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    .line 193
    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v7

    .line 191
    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 202
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    iget v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->e:F

    cmpg-float v4, v0, v6

    if-ltz v4, :cond_1

    cmpl-float v0, v0, v8

    if-lez v0, :cond_5

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/f;->j:I

    .line 203
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/f;->l:I

    .line 206
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->setBackgroundResource(I)V

    .line 209
    iget v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->f:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->e:F

    sget-object v4, Lcom/google/android/apps/gmm/map/ui/f;->a:[I

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/f;->h:Lcom/google/android/apps/gmm/map/b/a/g;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/g;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    cmpl-float v0, v0, v6

    if-gtz v0, :cond_3

    cmpg-float v0, v3, v6

    if-ltz v0, :cond_2

    cmpl-float v0, v3, v8

    if-lez v0, :cond_7

    :cond_2
    move v0, v2

    :goto_2
    if-nez v0, :cond_8

    :cond_3
    move v0, v2

    :goto_3
    if-nez v0, :cond_a

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    sget-object v1, Lcom/google/android/apps/gmm/map/ui/g;->d:Lcom/google/android/apps/gmm/map/ui/g;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    sget-object v1, Lcom/google/android/apps/gmm/map/ui/g;->c:Lcom/google/android/apps/gmm/map/ui/g;

    if-ne v0, v1, :cond_9

    .line 214
    :cond_4
    :goto_4
    return-void

    :cond_5
    move v0, v1

    .line 202
    goto :goto_0

    :cond_6
    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/f;->k:I

    goto :goto_1

    :pswitch_0
    move v0, v1

    .line 209
    goto :goto_3

    :pswitch_1
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    .line 210
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x640

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/h/a;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/ui/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/ui/d;-><init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/ui/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/ui/c;-><init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->c:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    goto :goto_4

    .line 211
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->c:Z

    if-nez v0, :cond_4

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    sget-object v2, Lcom/google/android/apps/gmm/map/ui/g;->b:Lcom/google/android/apps/gmm/map/ui/g;

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    sget-object v2, Lcom/google/android/apps/gmm/map/ui/g;->a:Lcom/google/android/apps/gmm/map/ui/g;

    if-ne v0, v2, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    sget-object v2, Lcom/google/android/apps/gmm/map/ui/g;->c:Lcom/google/android/apps/gmm/map/ui/g;

    if-ne v0, v2, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->a:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    goto :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/h/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/ui/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/ui/b;-><init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/ui/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/ui/a;-><init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->b:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    goto/16 :goto_4

    .line 209
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b()Lcom/google/android/apps/gmm/map/util/b/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    .line 125
    new-instance v1, Lcom/google/android/apps/gmm/map/ui/h;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/map/ui/h;-><init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->i:Lcom/google/android/apps/gmm/map/ui/h;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->c()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->i:Lcom/google/android/apps/gmm/map/ui/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 127
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iput v1, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->e:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->f:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    .line 128
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b()Lcom/google/android/apps/gmm/map/util/b/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->c()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->i:Lcom/google/android/apps/gmm/map/ui/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 136
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    if-ne v0, v1, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->a:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    .line 117
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 140
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 141
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->g:Landroid/graphics/Matrix;

    .line 142
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->h:Landroid/graphics/Matrix;

    .line 143
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 144
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d:Landroid/widget/ImageView;

    .line 145
    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->g:Landroid/graphics/Matrix;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v1, v0, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->d()V

    .line 148
    return-void
.end method
