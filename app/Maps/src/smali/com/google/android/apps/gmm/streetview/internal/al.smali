.class Lcom/google/android/apps/gmm/streetview/internal/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/streetview/internal/ag;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/streetview/internal/ak;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/ak;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/streetview/internal/z;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/16 v2, 0x2601

    const/4 v5, 0x0

    .line 190
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->c:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0, p2, v1, v5}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    .line 194
    new-instance v1, Lcom/google/android/apps/gmm/v/ci;

    invoke-direct {v1, v0, v5}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    .line 195
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    iput v2, v1, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v2, v1, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 196
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/ba;

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/ba;-><init>(III)V

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/ak;->g:Lcom/google/android/apps/gmm/streetview/internal/z;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/streetview/internal/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iput-object v1, v2, Lcom/google/android/apps/gmm/streetview/internal/ak;->h:Lcom/google/android/apps/gmm/v/ci;

    .line 205
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->k:Z

    if-eqz v1, :cond_3

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->l:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->k:Z

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/h;

    new-instance v2, Lcom/google/android/apps/gmm/map/j/i;

    sget-object v3, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/j/i;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v3, Lcom/google/b/f/bc;->k:Lcom/google/b/f/bc;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/j/h;-><init>(Lcom/google/android/apps/gmm/map/j/i;Lcom/google/b/f/cq;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->d:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0

    .line 200
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v2

    .line 201
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/al;->a:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v3, v3, Lcom/google/android/apps/gmm/streetview/internal/ak;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 202
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
