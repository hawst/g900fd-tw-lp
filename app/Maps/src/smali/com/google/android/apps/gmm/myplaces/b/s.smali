.class Lcom/google/android/apps/gmm/myplaces/b/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/b/ae;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/myplaces/b/ae",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/google/android/apps/gmm/myplaces/c/b",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/e/a/a/a/b;

.field final synthetic b:Lcom/google/android/apps/gmm/myplaces/b/n;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/e/a/a/a/b;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/s;->b:Lcom/google/android/apps/gmm/myplaces/b/n;

    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/b/s;->a:Lcom/google/e/a/a/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 317
    invoke-static {}, Lcom/google/b/c/dn;->h()Lcom/google/b/c/dp;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/s;->a:Lcom/google/e/a/a/a/b;

    iget-object v0, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/s;->a:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x1a

    invoke-virtual {v0, v8, v2, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/16 v1, 0x15

    invoke-virtual {v0, v8, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v1, v6

    invoke-static {v1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a(I)Lcom/google/android/apps/gmm/myplaces/c/b;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v0, "MyPlaces"

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x1b

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Unknown corpus #"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/s;->b:Lcom/google/android/apps/gmm/myplaces/b/n;

    invoke-static {v1, v6, v0}, Lcom/google/android/apps/gmm/myplaces/b/n;->a(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/e/a/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, v6}, Lcom/google/b/c/dp;->b(Ljava/lang/Object;)Lcom/google/b/c/dp;

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/google/b/c/dp;->a()Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method
