.class public Lcom/google/android/apps/gmm/invocation/b/l;
.super Lcom/google/android/apps/gmm/invocation/b/e;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/r/b/a/gb;->d:Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method public final b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 34
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;[I)Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 38
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/m;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/m;-><init>(Lcom/google/android/apps/gmm/invocation/b/l;Lcom/google/android/apps/gmm/base/g/c;)V

    .line 66
    :goto_0
    return-object v0

    .line 49
    :cond_0
    const/4 v3, 0x4

    if-eqz p1, :cond_4

    iget-object v1, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_2

    move v1, v0

    :goto_1
    if-nez v1, :cond_1

    invoke-virtual {p1, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    :cond_1
    :goto_2
    if-eqz v0, :cond_4

    const/16 v0, 0x1a

    invoke-virtual {p1, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    move-object v1, v0

    .line 50
    :goto_3
    if-eqz v1, :cond_5

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/n;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/invocation/b/n;-><init>(Lcom/google/android/apps/gmm/invocation/b/l;Lcom/google/e/a/a/a/b;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 49
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_3

    .line 65
    :cond_5
    const-string v0, "PlaceDetailsExecutor"

    const-string v1, "No place details request or response present."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/invocation/b/o;-><init>(Lcom/google/android/apps/gmm/invocation/b/l;)V

    goto :goto_0

    .line 34
    :array_0
    .array-data 4
        0x8
        0x1
    .end array-data
.end method
