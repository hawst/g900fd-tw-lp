.class final Lcom/google/android/apps/gmm/base/f/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/h;


# instance fields
.field final synthetic a:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/f/as;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/as;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 139
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/as;->a:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 140
    return-void

    .line 138
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 144
    if-eqz p2, :cond_0

    .line 146
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/f/as;->a(Landroid/view/View;)V

    .line 167
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/f/as;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/h/a;->b:Landroid/view/animation/Interpolator;

    .line 152
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/f/as;->a:Z

    if-eqz v1, :cond_2

    const/high16 v1, 0x3f800000    # 1.0f

    .line 153
    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x14d

    .line 155
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/f/at;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/f/at;-><init>(Lcom/google/android/apps/gmm/base/f/as;Landroid/view/View;)V

    .line 156
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 150
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/h/a;->c:Landroid/view/animation/Interpolator;

    goto :goto_1

    .line 152
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method
