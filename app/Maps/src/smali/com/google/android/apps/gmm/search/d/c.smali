.class public Lcom/google/android/apps/gmm/search/d/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/e/b;


# instance fields
.field private a:Z

.field private final b:Lcom/google/android/apps/gmm/search/aq;

.field private final c:Lcom/google/android/apps/gmm/place/b/b;

.field private final d:Lcom/google/android/apps/gmm/search/d/i;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/search/aq;Lcom/google/android/apps/gmm/place/b/b;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->e:Ljava/util/List;

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/d/c;->b:Lcom/google/android/apps/gmm/search/aq;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/d/c;->c:Lcom/google/android/apps/gmm/place/b/b;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/search/d/i;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/search/d/i;-><init>(Lcom/google/android/apps/gmm/search/aq;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->d:Lcom/google/android/apps/gmm/search/d/i;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/d/c;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 71
    if-nez p1, :cond_1

    .line 76
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/d/c;->a:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->d:Lcom/google/android/apps/gmm/search/d/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/d/i;->a(Lcom/google/android/apps/gmm/search/al;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 81
    if-eqz v0, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/c;->d:Lcom/google/android/apps/gmm/search/d/i;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/search/d/i;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/c;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 85
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->r()Ljava/util/List;

    move-result-object v3

    move v1, v2

    .line 86
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 87
    iget-object v4, p0, Lcom/google/android/apps/gmm/search/d/c;->e:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/gmm/search/d/b;

    iget-object v6, p0, Lcom/google/android/apps/gmm/search/d/c;->b:Lcom/google/android/apps/gmm/search/aq;

    iget-object v7, p0, Lcom/google/android/apps/gmm/search/d/c;->c:Lcom/google/android/apps/gmm/place/b/b;

    .line 88
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/f;

    invoke-direct {v5, v6, v7, v0}, Lcom/google/android/apps/gmm/search/d/b;-><init>(Lcom/google/android/apps/gmm/search/aq;Lcom/google/android/apps/gmm/place/b/b;Lcom/google/android/apps/gmm/search/f;)V

    .line 87
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->d:Lcom/google/android/apps/gmm/search/d/i;

    .line 92
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/d/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v2, 0x1

    :cond_4
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/search/d/c;->a:Z

    goto :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->d:Lcom/google/android/apps/gmm/search/d/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/d/i;->a:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->d:Lcom/google/android/apps/gmm/search/d/i;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/c;->e:Ljava/util/List;

    return-object v0
.end method
