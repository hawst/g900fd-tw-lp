.class public Lcom/google/android/apps/gmm/aa/d/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/aa/d/q;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/navigation/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/navigation/a/a/c;

.field d:Lcom/google/android/apps/gmm/aa/d/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Z

.field private final f:Lcom/google/android/apps/gmm/aa/d/r;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/aa/d/t;

.field private final h:Lcom/google/android/apps/gmm/aa/d/p;

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/aa/d/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/d/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/aa/d/p;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/navigation/a/a/c;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/navigation/logging/m;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    .line 55
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/a;->h:Lcom/google/android/apps/gmm/aa/d/p;

    .line 56
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/d/a;->f:Lcom/google/android/apps/gmm/aa/d/r;

    .line 57
    iput-object p4, p0, Lcom/google/android/apps/gmm/aa/d/a;->g:Lcom/google/android/apps/gmm/aa/d/t;

    .line 58
    iput-object p3, p0, Lcom/google/android/apps/gmm/aa/d/a;->c:Lcom/google/android/apps/gmm/navigation/a/a/c;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/aa/d/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/aa/d/b;-><init>(Lcom/google/android/apps/gmm/aa/d/a;)V

    invoke-interface {p3, v0, p5}, Lcom/google/android/apps/gmm/navigation/a/a/c;->a(Lcom/google/android/apps/gmm/navigation/a/a/d;Lcom/google/android/apps/gmm/navigation/logging/m;)V

    .line 66
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 156
    monitor-enter p0

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 158
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->b:Lcom/google/android/apps/gmm/navigation/a/a/b;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->b:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/a/b;->c()I

    .line 162
    :cond_0
    return-void

    .line 158
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/aa/d/k;)V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x0

    .line 99
    monitor-enter p0

    .line 101
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/aa/d/a;->e:Z

    .line 102
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/aa/d/a;->e:Z

    if-nez v2, :cond_1

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/a;->d:Lcom/google/android/apps/gmm/aa/d/k;

    if-eqz v2, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->d:Lcom/google/android/apps/gmm/aa/d/k;

    .line 106
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/a;->d:Lcom/google/android/apps/gmm/aa/d/k;

    .line 108
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    if-eqz v0, :cond_2

    .line 110
    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/d/k;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/aa/d/a;->b(Ljava/lang/String;)V

    .line 112
    :cond_2
    if-eqz v1, :cond_3

    .line 113
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/aa/d/a;->b(Lcom/google/android/apps/gmm/aa/d/k;)V

    .line 115
    :cond_3
    return-void

    .line 108
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 166
    monitor-enter p0

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 168
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->b:Lcom/google/android/apps/gmm/navigation/a/a/b;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->b:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/a/b;->c()I

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->b:Lcom/google/android/apps/gmm/navigation/a/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/a/b;->b()V

    .line 173
    :cond_0
    return-void

    .line 168
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method b(Lcom/google/android/apps/gmm/aa/d/k;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->g:Lcom/google/android/apps/gmm/aa/d/t;

    iget-object v2, v0, Lcom/google/android/apps/gmm/aa/d/t;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/d/t;->c:Ljava/io/File;

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 119
    iget-object v2, p1, Lcom/google/android/apps/gmm/aa/d/k;->a:Ljava/lang/String;

    .line 120
    const/4 v0, 0x0

    .line 122
    monitor-enter p0

    .line 123
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    move v0, v1

    .line 128
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    if-eqz v0, :cond_2

    .line 131
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/aa/d/a;->c(Ljava/lang/String;)V

    .line 146
    :cond_0
    :goto_1
    return-void

    .line 126
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->h:Lcom/google/android/apps/gmm/aa/d/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/aa/d/p;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 136
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/aa/d/a;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->b:Lcom/google/android/apps/gmm/navigation/a/a/b;

    .line 141
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 140
    invoke-interface {v0, v2, v2, v3, v1}, Lcom/google/android/apps/gmm/navigation/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 143
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 144
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/aa/d/a;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 177
    monitor-enter p0

    .line 178
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->f:Lcom/google/android/apps/gmm/aa/d/r;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->f:Lcom/google/android/apps/gmm/aa/d/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/aa/d/r;->a(Ljava/lang/String;)V

    .line 183
    :cond_0
    return-void

    .line 179
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 187
    monitor-enter p0

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 190
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/a;->h:Lcom/google/android/apps/gmm/aa/d/p;

    invoke-static {v0}, Lcom/google/android/apps/gmm/aa/d/p;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 193
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/aa/d/a;->b(Ljava/lang/String;)V

    .line 199
    :cond_1
    :goto_0
    return-void

    .line 190
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 196
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->f:Lcom/google/android/apps/gmm/aa/d/r;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/a;->f:Lcom/google/android/apps/gmm/aa/d/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/aa/d/r;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
