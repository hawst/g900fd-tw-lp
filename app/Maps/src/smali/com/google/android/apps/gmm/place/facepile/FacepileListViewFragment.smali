.class public Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# instance fields
.field c:Lcom/google/android/apps/gmm/place/facepile/g;

.field d:Ljava/lang/String;

.field e:Z

.field f:Ljava/lang/Runnable;

.field g:Landroid/view/View;

.field private m:Lcom/google/maps/g/fk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/maps/g/fk;Ljava/lang/String;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/fk;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;-><init>()V

    .line 45
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 46
    const-string v2, "INFO_KEY"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 47
    const-string v2, "FEATURE_KEY"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v2, "PLACEMARK_REF_KEY"

    invoke-virtual {p2, v1, v2, p3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 50
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v0
.end method


# virtual methods
.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/google/b/f/t;->aS:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 60
    const-string v0, "INFO_KEY"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fk;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->m:Lcom/google/maps/g/fk;

    .line 61
    const-string v0, "FEATURE_KEY"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->d:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v2, "PLACEMARK_REF_KEY"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->e:Z

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/place/facepile/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/facepile/d;-><init>(Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->f:Ljava/lang/Runnable;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 77
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    .line 97
    :goto_0
    return-object v0

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/google/android/apps/gmm/place/facepile/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->m:Lcom/google/maps/g/fk;

    invoke-direct {v1, v0, v2, p0, v7}, Lcom/google/android/apps/gmm/place/facepile/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/maps/g/fk;Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;Z)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->c:Lcom/google/android/apps/gmm/place/facepile/g;

    .line 88
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/facepile/e;

    .line 89
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    sget v1, Lcom/google/android/apps/gmm/l;->gu:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->c:Lcom/google/android/apps/gmm/place/facepile/g;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/place/facepile/g;->f:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->c:Lcom/google/maps/g/fk;

    iget-boolean v0, v0, Lcom/google/maps/g/fk;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->c:Lcom/google/maps/g/fk;

    iget-object v0, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->c:Lcom/google/maps/g/fk;

    iget-object v0, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fg;->h()Lcom/google/maps/g/fg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fg;

    invoke-virtual {v0}, Lcom/google/maps/g/fg;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/place/facepile/g;->a:Ljava/lang/String;

    const-string v1, "VeneerManager is not resumed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 97
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->g:Landroid/view/View;

    goto/16 :goto_0

    .line 94
    :cond_4
    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v3

    sget-object v0, Lcom/google/o/h/a/dq;->k:Lcom/google/o/h/a/dq;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v6, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v4, v5, v6, v1}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)Lcom/google/android/apps/gmm/startpage/ac;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v2, v1, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    invoke-interface {v3, v0, v7}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/android/apps/gmm/startpage/a/d;Z)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroy()V

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->c:Lcom/google/android/apps/gmm/place/facepile/g;

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/facepile/g;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/facepile/g;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 104
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->e:Z

    .line 109
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 110
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 114
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 115
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->e:Z

    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 120
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 121
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 122
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 124
    return-void
.end method
