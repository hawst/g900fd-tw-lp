.class Lcom/google/android/apps/gmm/map/internal/d/m;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:J

.field b:J

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/d/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/d;)V
    .locals 4

    .prologue
    .line 3276
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->c:Lcom/google/android/apps/gmm/map/internal/d/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3277
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x40b3880000000000L    # 5000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->a:J

    .line 3278
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->b:J

    .line 3279
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 8

    .prologue
    .line 3285
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->c:Lcom/google/android/apps/gmm/map/internal/d/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 3287
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->a:J

    long-to-double v0, v0

    const-wide v2, 0x3ff199999999999aL    # 1.1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 3288
    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->a:J

    .line 3289
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->c:Lcom/google/android/apps/gmm/map/internal/d/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/d;->m:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/m;->b:J

    .line 3290
    const/4 v0, 0x1

    .line 3292
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
