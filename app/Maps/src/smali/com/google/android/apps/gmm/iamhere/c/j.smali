.class public final enum Lcom/google/android/apps/gmm/iamhere/c/j;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/c/j;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/c/j;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/c/j;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/c/j;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/iamhere/c/j;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 364
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/j;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->a:Lcom/google/android/apps/gmm/iamhere/c/j;

    .line 366
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/j;

    const-string v1, "DISMISSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->b:Lcom/google/android/apps/gmm/iamhere/c/j;

    .line 368
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/j;

    const-string v1, "REMOVED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/iamhere/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->c:Lcom/google/android/apps/gmm/iamhere/c/j;

    .line 370
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/j;

    const-string v1, "GONE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/iamhere/c/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->d:Lcom/google/android/apps/gmm/iamhere/c/j;

    .line 362
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/c/j;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/j;->a:Lcom/google/android/apps/gmm/iamhere/c/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/j;->b:Lcom/google/android/apps/gmm/iamhere/c/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/j;->c:Lcom/google/android/apps/gmm/iamhere/c/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/j;->d:Lcom/google/android/apps/gmm/iamhere/c/j;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->e:[Lcom/google/android/apps/gmm/iamhere/c/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 362
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/c/j;
    .locals 1

    .prologue
    .line 362
    const-class v0, Lcom/google/android/apps/gmm/iamhere/c/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/c/j;
    .locals 1

    .prologue
    .line 362
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->e:[Lcom/google/android/apps/gmm/iamhere/c/j;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/c/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/c/j;

    return-object v0
.end method
