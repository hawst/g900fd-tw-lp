.class final Lcom/google/android/apps/gmm/map/internal/d/b/w;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field final d:I

.field final e:I

.field final f:Z

.field final g:I

.field final h:J

.field final i:Ljava/util/Locale;

.field final j:I

.field final k:I


# direct methods
.method constructor <init>(IIIIZIJLjava/util/Locale;)V
    .locals 3

    .prologue
    .line 2647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2648
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    .line 2649
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    .line 2650
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    .line 2651
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    .line 2652
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    .line 2653
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    .line 2654
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    .line 2655
    iput-wide p7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    .line 2658
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a()[B

    move-result-object v0

    .line 2659
    array-length v1, v0

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    .line 2660
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 2661
    const/4 v1, 0x0

    array-length v2, v0

    add-int/lit8 v2, v2, -0x4

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->j:I

    .line 2664
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    shl-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, -0x1

    div-int/lit16 v0, v0, 0x2000

    add-int/lit8 v0, v0, 0x1

    .line 2665
    shl-int/lit8 v0, v0, 0xd

    add-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->k:I

    .line 2666
    return-void
.end method

.method constructor <init>([BI)V
    .locals 5

    .prologue
    .line 2668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2669
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    .line 2670
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2672
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t parse header for old schema"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2674
    :cond_0
    add-int/lit8 v0, p2, 0x4

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    .line 2675
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    const/16 v1, 0x2b

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    add-int/2addr v0, p2

    array-length v1, p1

    if-le v0, v1, :cond_2

    .line 2676
    :cond_1
    new-instance v0, Ljava/io/IOException;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Wrong header size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2678
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, -0x4

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->j:I

    .line 2679
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    add-int/lit8 v0, v0, -0x4

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v0

    .line 2680
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->j:I

    if-eq v1, v0, :cond_3

    .line 2681
    new-instance v1, Ljava/io/IOException;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->j:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Checksum mismatch "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2686
    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/m/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/m/a/a;-><init>([B)V

    .line 2687
    add-int/lit8 v1, p2, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/m/a/a;->skipBytes(I)I

    .line 2689
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    .line 2690
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    .line 2691
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    .line 2692
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    .line 2693
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    .line 2694
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    .line 2698
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 2699
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readUTF()Ljava/lang/String;

    move-result-object v2

    .line 2700
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/m/a/a;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 2701
    new-instance v3, Ljava/util/Locale;

    invoke-direct {v3, v1, v2, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    .line 2704
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    shl-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, -0x1

    div-int/lit16 v0, v0, 0x2000

    add-int/lit8 v0, v0, 0x1

    .line 2705
    shl-int/lit8 v0, v0, 0xd

    add-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->k:I

    .line 2706
    return-void
.end method


# virtual methods
.method a()[B
    .locals 4

    .prologue
    .line 2718
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2719
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 2720
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->b:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeBoolean(Z)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    invoke-interface {v1, v2, v3}, Ljava/io/DataOutput;->writeLong(J)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->i:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->j:I

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 2721
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 11

    .prologue
    .line 2741
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->d:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->e:I

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->f:Z

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->g:I

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->h:J

    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/w;->j:I

    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0xce

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "SchemaVersion:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " BlockSize:"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MaxShardCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " RecordsPerBlock: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AutoConfig: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DataVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CacheCreationTimeMs:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Checksum:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
