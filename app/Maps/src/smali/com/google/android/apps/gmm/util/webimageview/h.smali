.class public Lcom/google/android/apps/gmm/util/webimageview/h;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lcom/google/b/a/be;

.field private static final b:Lcom/google/b/a/be;

.field private static final c:Lcom/google/b/a/ab;

.field private static final d:Lcom/google/b/a/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 34
    const-string v2, "="

    .line 35
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "The separator may not be the empty string."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/b/a/be;

    new-instance v3, Lcom/google/b/a/bh;

    invoke-direct {v3, v2}, Lcom/google/b/a/bh;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/google/b/a/be;-><init>(Lcom/google/b/a/bl;)V

    new-instance v2, Lcom/google/b/a/be;

    iget-object v3, v0, Lcom/google/b/a/be;->c:Lcom/google/b/a/bl;

    iget-object v4, v0, Lcom/google/b/a/be;->a:Lcom/google/b/a/f;

    iget v0, v0, Lcom/google/b/a/be;->d:I

    invoke-direct {v2, v3, v1, v4, v0}, Lcom/google/b/a/be;-><init>(Lcom/google/b/a/bl;ZLcom/google/b/a/f;I)V

    sput-object v2, Lcom/google/android/apps/gmm/util/webimageview/h;->a:Lcom/google/b/a/be;

    .line 37
    const/16 v0, 0x2f

    .line 38
    invoke-static {v0}, Lcom/google/b/a/f;->a(C)Lcom/google/b/a/f;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    new-instance v2, Lcom/google/b/a/be;

    new-instance v3, Lcom/google/b/a/bf;

    invoke-direct {v3, v0}, Lcom/google/b/a/bf;-><init>(Lcom/google/b/a/f;)V

    invoke-direct {v2, v3}, Lcom/google/b/a/be;-><init>(Lcom/google/b/a/bl;)V

    new-instance v0, Lcom/google/b/a/be;

    iget-object v3, v2, Lcom/google/b/a/be;->c:Lcom/google/b/a/bl;

    iget-object v4, v2, Lcom/google/b/a/be;->a:Lcom/google/b/a/f;

    iget v2, v2, Lcom/google/b/a/be;->d:I

    invoke-direct {v0, v3, v1, v4, v2}, Lcom/google/b/a/be;-><init>(Lcom/google/b/a/bl;ZLcom/google/b/a/f;I)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/h;->b:Lcom/google/b/a/be;

    .line 40
    const-string v0, "/"

    new-instance v1, Lcom/google/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/util/webimageview/h;->c:Lcom/google/b/a/ab;

    .line 42
    const-string v0, "-"

    new-instance v1, Lcom/google/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/util/webimageview/h;->d:Lcom/google/b/a/ab;

    return-void
.end method

.method public static a(IILcom/google/android/apps/gmm/util/webimageview/i;ZLjava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    if-ne p2, v0, :cond_2

    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v1, "w%d-h%d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz p3, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/h;->d:Lcom/google/b/a/ab;

    const-string v4, "fblur=1,80"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/b/a/ab;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 118
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    :goto_1
    return-object p4

    .line 115
    :cond_2
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v1, "w%d-h%d-%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p2, Lcom/google/android/apps/gmm/util/webimageview/i;->f:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v0, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, v0

    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/h;->b:Lcom/google/b/a/be;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    new-instance v6, Lcom/google/b/a/bj;

    invoke-direct {v6, v0, v4}, Lcom/google/b/a/bj;-><init>(Lcom/google/b/a/be;Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_f

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v7, "public"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v6, "proxy"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_5
    add-int/lit8 v0, v4, -0x1

    :goto_2
    if-lt v0, v8, :cond_c

    const/4 v4, 0x6

    if-gt v0, v4, :cond_c

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/h;->b:Lcom/google/b/a/be;

    if-nez v4, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    new-instance v6, Lcom/google/b/a/bj;

    invoke-direct {v6, v0, v4}, Lcom/google/b/a/bj;-><init>(Lcom/google/b/a/be;Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_e

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v6, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v2

    :goto_3
    const-string v2, "/"

    invoke-virtual {v4, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, ""

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v8, :cond_a

    const-string v2, ""

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    :goto_4
    invoke-interface {v6, v8, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_9

    const-string v0, "image"

    invoke-interface {v6, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_9
    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/"

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/h;->c:Lcom/google/b/a/ab;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4, v3}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_1

    :cond_a
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x5

    if-ne v2, v4, :cond_8

    const-string v2, ""

    invoke-interface {v6, v8, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_4

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_c
    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/h;->a:Lcom/google/b/a/be;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    new-instance v4, Lcom/google/b/a/bj;

    invoke-direct {v4, v0, v2}, Lcom/google/b/a/bj;-><init>(Lcom/google/b/a/be;Ljava/lang/CharSequence;)V

    invoke-static {v4}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "="

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p4

    goto/16 :goto_1

    :cond_e
    move v0, v3

    goto/16 :goto_3

    :cond_f
    move v0, v4

    goto/16 :goto_2
.end method
