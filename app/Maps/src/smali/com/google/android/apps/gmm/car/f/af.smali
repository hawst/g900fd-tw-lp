.class public Lcom/google/android/apps/gmm/car/f/af;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/place/bf;

.field final c:Lcom/google/android/apps/gmm/map/t;

.field final d:Lcom/google/android/apps/gmm/base/a;

.field final e:Landroid/content/res/Resources;

.field final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/i;",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            "Lcom/google/android/apps/gmm/car/bk;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lcom/google/android/apps/gmm/place/b/c;

.field private final i:Lcom/google/android/apps/gmm/search/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/android/apps/gmm/car/f/af;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/f/af;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/af;->f:Ljava/util/HashMap;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/af;->g:Ljava/util/HashMap;

    .line 254
    new-instance v0, Lcom/google/android/apps/gmm/car/f/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/f/aj;-><init>(Lcom/google/android/apps/gmm/car/f/af;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/af;->h:Lcom/google/android/apps/gmm/place/b/c;

    .line 67
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/search/ah;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/af;->i:Lcom/google/android/apps/gmm/search/ah;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/place/bf;

    new-instance v1, Lcom/google/android/apps/gmm/car/d;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/android/apps/gmm/car/d;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/bf;-><init>(Lcom/google/android/apps/gmm/base/activities/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/af;->b:Lcom/google/android/apps/gmm/place/bf;

    .line 70
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Lcom/google/android/apps/gmm/map/t;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/f/af;->c:Lcom/google/android/apps/gmm/map/t;

    .line 71
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p2, Lcom/google/android/apps/gmm/base/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/f/af;->d:Lcom/google/android/apps/gmm/base/a;

    .line 72
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/f/af;->e:Landroid/content/res/Resources;

    .line 73
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V
    .locals 7
    .param p2    # Lcom/google/android/apps/gmm/car/ac;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 211
    sget-object v0, Lcom/google/android/apps/gmm/car/f/af;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "prefetchPlacemarkWithSearch for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 213
    :cond_0
    new-instance v6, Lcom/google/android/apps/gmm/car/f/ai;

    invoke-direct {v6, p0, p2, p1}, Lcom/google/android/apps/gmm/car/f/ai;-><init>(Lcom/google/android/apps/gmm/car/f/af;Lcom/google/android/apps/gmm/car/ac;Lcom/google/android/apps/gmm/car/bm;)V

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/car/i/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/af;->i:Lcom/google/android/apps/gmm/search/ah;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/af;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/af;->d:Lcom/google/android/apps/gmm/base/a;

    .line 249
    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/f/af;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/i/x;-><init>(Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Lcom/google/android/apps/gmm/hotels/a/b;I)V

    .line 250
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/bm;->a:Ljava/lang/String;

    const/4 v2, 0x0

    .line 251
    iget-object v3, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    .line 250
    invoke-virtual {v0, v1, v2, v3, v6}, Lcom/google/android/apps/gmm/car/i/x;->a(Ljava/lang/String;Lcom/google/maps/a/a;[BLcom/google/android/apps/gmm/car/i/k;)V

    .line 252
    return-void
.end method
