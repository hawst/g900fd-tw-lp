.class public Lcom/google/android/apps/gmm/directions/f/d/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lcom/google/b/c/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ak",
            "<",
            "Lcom/google/maps/g/a/al;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/google/b/c/cg;

    invoke-direct {v0}, Lcom/google/b/c/cg;-><init>()V

    sget-object v1, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    const/4 v2, 0x1

    .line 36
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/cg;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cg;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    const/4 v2, 0x2

    .line 37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/cg;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cg;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/a/al;->c:Lcom/google/maps/g/a/al;

    const/4 v2, 0x3

    .line 38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/c/cg;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cg;

    move-result-object v0

    .line 39
    iget-object v0, v0, Lcom/google/b/c/dd;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/b/c/cf;->a(Ljava/util/Collection;)Lcom/google/b/c/cf;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/d/b;->a:Lcom/google/b/c/ak;

    .line 34
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/al;)I
    .locals 1
    .param p0    # Lcom/google/maps/g/a/al;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 48
    if-nez p0, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 51
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/b;->a:Lcom/google/b/c/ak;

    invoke-interface {v0, p0}, Lcom/google/b/c/ak;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a(I)Lcom/google/maps/g/a/al;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/b;->a:Lcom/google/b/c/ak;

    invoke-interface {v0}, Lcom/google/b/c/ak;->ap_()Lcom/google/b/c/ak;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/b/c/ak;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/al;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/n;)Ljava/lang/String;
    .locals 6

    .prologue
    const v2, 0x358637bd    # 1.0E-6f

    .line 116
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/n;->a:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    .line 117
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/n;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 118
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%f,%f"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/n;[Lcom/google/android/apps/gmm/map/b/a/n;Lcom/google/android/apps/gmm/map/b/a/n;Lcom/google/maps/g/a/hm;)Ljava/lang/String;
    .locals 5
    .param p1    # [Lcom/google/android/apps/gmm/map/b/a/n;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76
    if-eqz p0, :cond_5

    if-eqz p2, :cond_5

    .line 77
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/f/d/b;->a(Lcom/google/android/apps/gmm/map/b/a/n;)Ljava/lang/String;

    move-result-object v1

    .line 79
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    if-eqz p1, :cond_0

    .line 81
    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 82
    aget-object v3, p1, v0

    invoke-static {v3}, Lcom/google/android/apps/gmm/directions/f/d/b;->a(Lcom/google/android/apps/gmm/map/b/a/n;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    invoke-static {p2}, Lcom/google/android/apps/gmm/directions/f/d/b;->a(Lcom/google/android/apps/gmm/map/b/a/n;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "http"

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "maps.google.com"

    .line 88
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "/maps"

    .line 89
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "f"

    const-string v4, "d"

    .line 90
    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "saddr"

    .line 91
    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "daddr"

    .line 92
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "doflg"

    const-string v2, "ptxcluao"

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "dirflg"

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcom/google/android/apps/gmm/l/a/a;->a(Lcom/google/maps/g/a/hm;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "?"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "output"

    const-string v2, "classic"

    .line 95
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    if-eqz p1, :cond_4

    .line 101
    const/4 v0, 0x1

    :goto_1
    array-length v3, p1

    if-gt v0, v3, :cond_3

    .line 102
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    array-length v3, p1

    if-eq v0, v3, :cond_2

    .line 104
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 107
    :cond_3
    const-string v0, "via"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 110
    :cond_4
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_2
    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method
