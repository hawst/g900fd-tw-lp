.class public Lcom/google/android/apps/gmm/startpage/u;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/startpage/d/b;)Lcom/google/android/apps/gmm/cardui/c/b;
    .locals 10

    .prologue
    .line 241
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    .line 243
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a;->a()V

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    sget-object v5, Lcom/google/android/apps/gmm/cardui/f/a;->b:Lcom/google/b/c/cv;

    iget v1, v0, Lcom/google/o/h/a/br;->c:I

    invoke-static {v1}, Lcom/google/o/h/a/bw;->a(I)Lcom/google/o/h/a/bw;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/o/h/a/bw;->a:Lcom/google/o/h/a/bw;

    :cond_1
    invoke-virtual {v5, v1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v5, Lcom/google/android/apps/gmm/startpage/v;->b:[I

    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/c;

    iget v6, v4, Lcom/google/o/h/a/lh;->c:I

    iget-wide v8, v4, Lcom/google/o/h/a/lh;->d:J

    invoke-direct {v1, v6, v8, v9}, Lcom/google/android/apps/gmm/startpage/d/c;-><init>(IJ)V

    iget v1, v1, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    invoke-static {v1}, Lcom/google/o/h/a/lk;->a(I)Lcom/google/o/h/a/lk;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/o/h/a/lk;->a:Lcom/google/o/h/a/lk;

    :cond_2
    invoke-virtual {v1}, Lcom/google/o/h/a/lk;->ordinal()I

    move-result v1

    aget v1, v5, v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Lcom/google/android/apps/gmm/cardui/c/a;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/cardui/c/a;-><init>(Lcom/google/android/apps/gmm/startpage/d/b;Lcom/google/o/h/a/br;)V

    move-object v0, v1

    .line 244
    :goto_1
    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 243
    :pswitch_0
    new-instance v1, Lcom/google/android/apps/gmm/cardui/c/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/cardui/c/c;-><init>(Lcom/google/android/apps/gmm/startpage/d/b;Lcom/google/o/h/a/br;)V

    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 248
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/cardui/c/b;

    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/cardui/c/b;-><init>(Lcom/google/android/apps/gmm/startpage/d/b;Ljava/util/List;)V

    return-object v0

    .line 243
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/o/h/a/sx;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 178
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 180
    iget v0, p0, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/o/h/a/sx;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    :goto_1
    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 183
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    move v0, v2

    :goto_2
    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/google/o/h/a/sx;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 186
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/google/o/h/a/sx;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 189
    :cond_2
    iget-boolean v0, p0, Lcom/google/o/h/a/sx;->e:Z

    if-eqz v0, :cond_3

    .line 190
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 193
    :cond_3
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v3

    .line 180
    goto :goto_0

    .line 181
    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v1, p0, Lcom/google/o/h/a/sx;->b:Ljava/lang/Object;

    :cond_6
    move-object v0, v1

    goto :goto_1

    :cond_7
    move v0, v3

    .line 183
    goto :goto_2

    :cond_8
    move v0, v3

    .line 186
    goto :goto_3
.end method

.method public static a(Lcom/google/maps/g/a/hm;)Lcom/google/o/h/a/dq;
    .locals 4

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/apps/gmm/startpage/v;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 147
    const-string v0, "OdelayUtil"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown travel mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    sget-object v0, Lcom/google/o/h/a/dq;->c:Lcom/google/o/h/a/dq;

    :goto_0
    return-object v0

    .line 139
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/dq;->c:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 141
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/dq;->d:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 143
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/dq;->f:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 145
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/dq;->e:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/startpage/d/c;Ljava/util/List;)Lcom/google/o/h/a/lh;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/startpage/d/c;",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;)",
            "Lcom/google/o/h/a/lh;"
        }
    .end annotation

    .prologue
    .line 278
    invoke-static {}, Lcom/google/o/h/a/lh;->newBuilder()Lcom/google/o/h/a/lj;

    move-result-object v1

    .line 279
    iget v0, p0, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    iget v2, v1, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/o/h/a/lj;->a:I

    iput v0, v1, Lcom/google/o/h/a/lj;->b:I

    .line 280
    iget-wide v2, p0, Lcom/google/android/apps/gmm/startpage/d/c;->c:J

    iget v0, v1, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/o/h/a/lj;->a:I

    iput-wide v2, v1, Lcom/google/o/h/a/lj;->c:J

    .line 281
    invoke-virtual {v1}, Lcom/google/o/h/a/lj;->c()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    iget-object v3, v1, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    invoke-direct {v4}, Lcom/google/n/ao;-><init>()V

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 282
    :cond_0
    invoke-virtual {v1}, Lcom/google/o/h/a/lj;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    return-object v0
.end method

.method public static a(Ljava/util/List;)Lcom/google/o/h/a/nt;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;)",
            "Lcom/google/o/h/a/nt;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 126
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 133
    :goto_0
    return-object v0

    .line 129
    :cond_0
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    .line 130
    iget v3, v0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    .line 131
    iget-object v0, v0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nt;->d()Lcom/google/o/h/a/nt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nt;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 133
    goto :goto_0
.end method

.method public static a(Lcom/google/o/h/a/mj;Lcom/google/o/h/a/sa;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/o/h/a/mj;",
            "Lcom/google/o/h/a/sa;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 305
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lh;->m()Lcom/google/o/h/a/lh;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    .line 306
    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/o/h/a/lh;Lcom/google/o/h/a/sa;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 310
    :cond_2
    return-object v1
.end method

.method private static a(Lcom/google/o/h/a/lh;Lcom/google/o/h/a/sa;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 286
    invoke-virtual {p0}, Lcom/google/o/h/a/lh;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 294
    :goto_0
    return v0

    .line 289
    :cond_0
    invoke-virtual {p0}, Lcom/google/o/h/a/lh;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sa;

    .line 290
    if-nez p1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_1

    move v0, v2

    .line 291
    goto :goto_0

    .line 290
    :cond_2
    iget v1, v0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_6

    iget v1, v0, Lcom/google/o/h/a/sa;->b:I

    invoke-static {v1}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_3
    iget v4, p1, Lcom/google/o/h/a/sa;->b:I

    invoke-static {v4}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_4
    if-eq v1, v4, :cond_6

    move v0, v3

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    iget v1, v0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_9

    move v1, v2

    :goto_3
    if-eqz v1, :cond_a

    iget v1, v0, Lcom/google/o/h/a/sa;->c:I

    invoke-static {v1}, Lcom/google/o/h/a/eq;->a(I)Lcom/google/o/h/a/eq;

    move-result-object v1

    if-nez v1, :cond_7

    sget-object v1, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    :cond_7
    iget v4, p1, Lcom/google/o/h/a/sa;->c:I

    invoke-static {v4}, Lcom/google/o/h/a/eq;->a(I)Lcom/google/o/h/a/eq;

    move-result-object v4

    if-nez v4, :cond_8

    sget-object v4, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    :cond_8
    if-eq v1, v4, :cond_a

    move v0, v3

    goto :goto_1

    :cond_9
    move v1, v3

    goto :goto_3

    :cond_a
    iget-boolean v1, v0, Lcom/google/o/h/a/sa;->d:Z

    if-eqz v1, :cond_b

    iget-boolean v1, p1, Lcom/google/o/h/a/sa;->d:Z

    if-nez v1, :cond_b

    move v0, v3

    goto :goto_1

    :cond_b
    iget-boolean v0, v0, Lcom/google/o/h/a/sa;->e:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p1, Lcom/google/o/h/a/sa;->e:Z

    if-nez v0, :cond_c

    move v0, v3

    goto :goto_1

    :cond_c
    move v0, v2

    goto :goto_1

    :cond_d
    move v0, v3

    .line 294
    goto :goto_0
.end method

.method public static b(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 321
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_4

    .line 322
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    .line 323
    iget v1, v0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_3

    .line 324
    iget-object v1, v0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 327
    :goto_2
    return-object v0

    .line 323
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 324
    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_2

    .line 321
    :cond_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 327
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
