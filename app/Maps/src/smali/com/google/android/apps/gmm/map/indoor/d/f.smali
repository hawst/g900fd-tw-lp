.class public final Lcom/google/android/apps/gmm/map/indoor/d/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/l;

.field public final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/l;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 54
    iput p2, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    .line 55
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/f;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 129
    if-nez p0, :cond_0

    move-object v0, v1

    .line 143
    :goto_0
    return-object v0

    .line 132
    :cond_0
    const/16 v0, 0x1c

    invoke-virtual {p0, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v5

    .line 133
    const/high16 v0, -0x80000000

    .line 135
    iget-object v4, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_3

    move v4, v3

    :goto_1
    if-nez v4, :cond_1

    invoke-virtual {p0, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    if-eqz v2, :cond_5

    .line 136
    const/16 v0, 0x15

    invoke-virtual {p0, v6, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    move v2, v0

    .line 139
    :goto_2
    if-eqz v5, :cond_4

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-direct {v0, v5, v2}, Lcom/google/android/apps/gmm/map/indoor/d/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V

    goto :goto_0

    :cond_3
    move v4, v2

    .line 135
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 143
    goto :goto_0

    :cond_5
    move v2, v0

    goto :goto_2
.end method

.method public static a(Lcom/google/maps/b/a/af;)Lcom/google/android/apps/gmm/map/indoor/d/f;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 211
    if-nez p0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-object v0

    .line 214
    :cond_1
    invoke-virtual {p0}, Lcom/google/maps/b/a/af;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v2

    .line 215
    const/high16 v1, -0x80000000

    .line 217
    iget-object v3, p0, Lcom/google/maps/b/a/af;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v3, v3, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v3, :cond_2

    .line 218
    iget-object v1, p0, Lcom/google/maps/b/a/af;->a:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    .line 221
    :cond_2
    if-eqz v2, :cond_0

    .line 222
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/indoor/d/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/g/by;)Lcom/google/android/apps/gmm/map/indoor/d/f;
    .locals 6
    .param p0    # Lcom/google/maps/g/by;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 163
    if-eqz p0, :cond_3

    iget v2, p0, Lcom/google/maps/g/by;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 164
    invoke-virtual {p0}, Lcom/google/maps/g/by;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v2

    .line 165
    if-eqz v2, :cond_3

    .line 166
    iget v3, p0, Lcom/google/maps/g/by;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    .line 167
    iget v0, p0, Lcom/google/maps/g/by;->c:F

    float-to-double v0, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 169
    :goto_2
    new-instance v1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/indoor/d/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V

    move-object v0, v1

    .line 172
    :goto_3
    return-object v0

    :cond_0
    move v2, v1

    .line 163
    goto :goto_0

    :cond_1
    move v0, v1

    .line 166
    goto :goto_1

    .line 167
    :cond_2
    const/high16 v0, -0x80000000

    goto :goto_2

    .line 172
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(Lcom/google/o/b/a/v;)Lcom/google/android/apps/gmm/map/indoor/d/f;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 230
    if-nez p0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-object v0

    .line 233
    :cond_1
    iget v3, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_3

    move v3, v1

    :goto_1
    if-eqz v3, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    .line 237
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/l;

    iget-wide v4, v0, Lcom/google/o/b/a/h;->b:J

    iget-wide v6, v0, Lcom/google/o/b/a/h;->c:J

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/l;-><init>(JJ)V

    .line 238
    const/high16 v0, -0x80000000

    .line 239
    iget v4, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_4

    :goto_2
    if-eqz v1, :cond_2

    .line 240
    iget v0, p0, Lcom/google/o/b/a/v;->n:F

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 242
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-direct {v1, v3, v0}, Lcom/google/android/apps/gmm/map/indoor/d/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    move v3, v2

    .line 233
    goto :goto_1

    :cond_4
    move v1, v2

    .line 239
    goto :goto_2
.end method

.method public static b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/f;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 183
    if-nez p0, :cond_0

    move-object v0, v1

    .line 199
    :goto_0
    return-object v0

    .line 187
    :cond_0
    const/16 v0, 0x1c

    invoke-virtual {p0, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 186
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v5

    .line 188
    const/high16 v0, -0x80000000

    .line 190
    iget-object v4, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v4

    if-lez v4, :cond_3

    move v4, v3

    :goto_1
    if-nez v4, :cond_1

    invoke-virtual {p0, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    if-eqz v2, :cond_5

    .line 191
    const/16 v0, 0x15

    invoke-virtual {p0, v6, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    move v2, v0

    .line 195
    :goto_2
    if-eqz v5, :cond_4

    .line 196
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-direct {v0, v5, v2}, Lcom/google/android/apps/gmm/map/indoor/d/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V

    goto :goto_0

    :cond_3
    move v4, v2

    .line 190
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 199
    goto :goto_0

    :cond_5
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/by;
    .locals 3

    .prologue
    .line 150
    invoke-static {}, Lcom/google/maps/g/by;->newBuilder()Lcom/google/maps/g/ca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 151
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/l;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/ca;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/ca;->a:I

    iput-object v1, v0, Lcom/google/maps/g/ca;->b:Ljava/lang/Object;

    .line 152
    iget v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 153
    iget v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    int-to-float v1, v1

    const v2, 0x3a83126f    # 0.001f

    mul-float/2addr v1, v2

    iget v2, v0, Lcom/google/maps/g/ca;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/ca;->a:I

    iput v1, v0, Lcom/google/maps/g/ca;->c:F

    .line 155
    :cond_1
    invoke-virtual {v0}, Lcom/google/maps/g/ca;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/by;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 77
    if-ne p0, p1, :cond_0

    .line 78
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    check-cast p1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":mId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLevelNumberE3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
