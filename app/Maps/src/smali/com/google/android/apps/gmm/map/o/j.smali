.class Lcom/google/android/apps/gmm/map/o/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/cc;


# instance fields
.field final a:Landroid/graphics/Paint;

.field final b:Landroid/graphics/Rect;

.field final c:Landroid/graphics/Rect;

.field d:Landroid/graphics/Bitmap;

.field e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->a:Landroid/graphics/Paint;

    .line 400
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->b:Landroid/graphics/Rect;

    .line 401
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->c:Landroid/graphics/Rect;

    .line 407
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/by;Landroid/graphics/Canvas;)Z
    .locals 5

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 412
    const/4 v0, 0x0

    .line 418
    :goto_0
    return v0

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->a:Landroid/graphics/Paint;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/j;->e:Z

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->c:Landroid/graphics/Rect;

    iget v1, p1, Lcom/google/android/apps/gmm/v/by;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/v/by;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/v/by;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/j;->d:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/j;->b:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/j;->c:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/j;->a:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 418
    const/4 v0, 0x1

    goto :goto_0
.end method
