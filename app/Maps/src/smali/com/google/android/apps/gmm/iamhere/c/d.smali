.class public Lcom/google/android/apps/gmm/iamhere/c/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/google/android/apps/gmm/iamhere/c/f;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;

.field public final d:Lcom/google/android/apps/gmm/iamhere/c/g;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 344
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/iamhere/c/e;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/d;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/f;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/iamhere/c/g;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/iamhere/c/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/iamhere/c/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    if-nez p1, :cond_0

    sget-object p1, Lcom/google/android/apps/gmm/iamhere/c/f;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 294
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    .line 295
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    .line 296
    if-nez p4, :cond_2

    sget-object p4, Lcom/google/android/apps/gmm/iamhere/c/g;->h:Lcom/google/android/apps/gmm/iamhere/c/g;

    :cond_2
    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 297
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 309
    instance-of v1, p1, Lcom/google/android/apps/gmm/iamhere/c/d;

    if-eqz v1, :cond_0

    .line 310
    check-cast p1, Lcom/google/android/apps/gmm/iamhere/c/d;

    .line 311
    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/d;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    .line 312
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    .line 313
    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 314
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 316
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 301
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->e:I

    if-nez v0, :cond_0

    .line 302
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->e:I

    .line 304
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 321
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "icon"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    .line 322
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 323
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    .line 324
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "actionUrl"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    .line 325
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "hashCode"

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/c/d;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->a:Lcom/google/android/apps/gmm/iamhere/c/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/f;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/g;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 341
    return-void

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
