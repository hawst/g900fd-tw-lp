.class public Lcom/google/android/apps/gmm/util/b/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/util/b/q;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/o/h/a/br;

.field private final c:Lcom/google/android/apps/gmm/util/b/a;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/o/h/a/br;Ljava/util/List;Lcom/google/android/apps/gmm/util/b/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/o/h/a/br;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;",
            "Lcom/google/android/apps/gmm/util/b/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/b/s;->a:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/b/s;->b:Lcom/google/o/h/a/br;

    .line 33
    iput-object p4, p0, Lcom/google/android/apps/gmm/util/b/s;->c:Lcom/google/android/apps/gmm/util/b/a;

    .line 35
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/s;->d:Ljava/util/List;

    .line 36
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/util/b/s;->a(Ljava/util/List;)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/s;->d:Ljava/util/List;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/gmm/util/b/m;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/s;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-instance v0, Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/b/s;->b:Lcom/google/o/h/a/br;

    iget-object v2, v7, Lcom/google/android/apps/gmm/util/b/m;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/util/b/s;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v6, v7, Lcom/google/android/apps/gmm/util/b/m;->e:Lcom/google/r/b/a/tf;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/util/b/ag;-><init>(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/android/apps/gmm/util/b/a;ZILcom/google/r/b/a/tf;)V

    iget-object v3, v7, Lcom/google/android/apps/gmm/util/b/m;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    new-instance v1, Lcom/google/android/apps/gmm/util/b/v;

    iget-object v2, p0, Lcom/google/android/apps/gmm/util/b/s;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0, v7}, Lcom/google/android/apps/gmm/util/b/v;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/android/apps/gmm/util/b/m;)V

    const-class v6, Lcom/google/android/apps/gmm/util/b/t;

    move v10, v4

    move-object v7, v1

    :goto_1
    new-instance v5, Lcom/google/android/apps/gmm/util/b/r;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/b/af;->a(Lcom/google/o/h/a/iv;)Z

    move-result v8

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/b/af;->b(Lcom/google/o/h/a/iv;)Z

    move-result v9

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/gmm/util/b/r;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;ZZZ)V

    .line 42
    :goto_2
    if-eqz v5, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/s;->d:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/b/s;->a:Landroid/content/Context;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/iv;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/util/b/af;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    iget-object v0, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    iget-object v1, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Class;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/iv;

    invoke-static {v2}, Lcom/google/android/apps/gmm/util/b/af;->c(Lcom/google/o/h/a/iv;)Z

    move-result v10

    move-object v6, v1

    move-object v7, v0

    goto :goto_1

    .line 48
    :cond_3
    return-void
.end method

.method public final b()Lcom/google/o/h/a/bw;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/s;->b:Lcom/google/o/h/a/br;

    iget v0, v0, Lcom/google/o/h/a/br;->c:I

    invoke-static {v0}, Lcom/google/o/h/a/bw;->a(I)Lcom/google/o/h/a/bw;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/o/h/a/bw;->a:Lcom/google/o/h/a/bw;

    :cond_0
    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/s;->b:Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
