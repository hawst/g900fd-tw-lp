.class public Lcom/google/android/apps/gmm/cardui/c/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/startpage/d/b;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/google/o/h/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/cardui/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/o/h/a/ln;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/google/android/apps/gmm/cardui/c/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/c/b;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/startpage/d/b;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/startpage/d/b;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/cardui/c/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-array v3, v1, [Lcom/google/android/apps/gmm/startpage/d/b;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/b;

    aput-object v0, v3, v2

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    array-length v4, v3

    if-ltz v4, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const-wide/16 v6, 0x5

    int-to-long v8, v4

    add-long/2addr v6, v8

    div-int/lit8 v0, v4, 0xa

    int-to-long v4, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    const v0, 0x7fffffff

    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iput-object v4, p0, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    .line 105
    if-nez p2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 103
    :cond_4
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_5
    long-to-int v0, v4

    goto :goto_1

    .line 105
    :cond_6
    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->d:Ljava/util/List;

    .line 106
    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    .line 107
    iget v0, v3, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    iget-object v0, v3, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->b:Lcom/google/o/h/a/a;

    .line 108
    iget-object v0, v3, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ln;->d()Lcom/google/o/h/a/ln;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ln;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->e:Lcom/google/o/h/a/ln;

    .line 109
    return-void

    :cond_7
    move v0, v2

    .line 107
    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/cardui/c/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->d:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/cardui/c/b;)V
    .locals 2

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    sget-object v0, Lcom/google/android/apps/gmm/cardui/c/b;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :goto_0
    monitor-exit p0

    return-void

    .line 122
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/cardui/c/b;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 125
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c/b;->b:Lcom/google/o/h/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->b:Lcom/google/o/h/a/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/google/android/apps/gmm/startpage/d/c;
    .locals 6

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/c;

    iget v2, v0, Lcom/google/o/h/a/lh;->c:I

    iget-wide v4, v0, Lcom/google/o/h/a/lh;->d:J

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/apps/gmm/startpage/d/c;-><init>(IJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/cardui/c/b;)V
    .locals 5

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    sget-object v0, Lcom/google/android/apps/gmm/cardui/c/b;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :goto_0
    monitor-exit p0

    return-void

    .line 140
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/cardui/c/b;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x34

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cannot merge multiple cards. # of cards: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->a()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/c/a;

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c/b;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/c/b;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/cardui/c/a;

    .line 148
    iget-object v2, p1, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    const/4 v3, 0x0

    .line 149
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/startpage/d/b;

    iget-object v3, v2, Lcom/google/android/apps/gmm/startpage/d/b;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    const/4 v4, 0x0

    .line 150
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/startpage/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/d/b;->c:Lcom/google/r/b/a/tf;

    .line 148
    invoke-virtual {v1, v0, v3, v2}, Lcom/google/android/apps/gmm/cardui/c/a;->a(Lcom/google/android/apps/gmm/cardui/c/a;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 152
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c/b;->b:Lcom/google/o/h/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->b:Lcom/google/o/h/a/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->e:Lcom/google/o/h/a/ln;

    if-nez v0, :cond_0

    move v0, v1

    .line 189
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->e:Lcom/google/o/h/a/ln;

    iget v0, v0, Lcom/google/o/h/a/ln;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/lq;->a(I)Lcom/google/o/h/a/lq;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/lq;->a:Lcom/google/o/h/a/lq;

    :cond_1
    sget-object v2, Lcom/google/o/h/a/lq;->c:Lcom/google/o/h/a/lq;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->e:Lcom/google/o/h/a/ln;

    if-nez v0, :cond_0

    move v0, v1

    .line 197
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c/b;->e:Lcom/google/o/h/a/ln;

    iget v0, v0, Lcom/google/o/h/a/ln;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/lq;->a(I)Lcom/google/o/h/a/lq;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/lq;->a:Lcom/google/o/h/a/lq;

    :cond_1
    sget-object v2, Lcom/google/o/h/a/lq;->b:Lcom/google/o/h/a/lq;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
