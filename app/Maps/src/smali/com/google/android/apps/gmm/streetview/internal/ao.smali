.class public Lcom/google/android/apps/gmm/streetview/internal/ao;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/streetview/internal/ao;->a:D

    return-void
.end method

.method public static a(F)F
    .locals 2

    .prologue
    .line 38
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, p0, v0

    return v0
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 47
    invoke-static {p1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public static a(IIII)I
    .locals 6

    .prologue
    .line 57
    int-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v0, v1

    int-to-float v1, p0

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/4 v1, 0x0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sget-wide v4, Lcom/google/android/apps/gmm/streetview/internal/ao;->a:D

    div-double/2addr v2, v4

    double-to-float v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static a(FF)Lcom/google/android/apps/gmm/v/cj;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 185
    const/high16 v0, 0x43340000    # 180.0f

    sub-float/2addr v0, p0

    .line 186
    const v2, 0x3c8efa35

    mul-float/2addr v0, v2

    .line 187
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 188
    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 189
    new-instance v6, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    new-instance v5, Lcom/google/android/apps/gmm/v/cn;

    const/4 v3, 0x0

    neg-float v0, v0

    invoke-direct {v5, v2, v3, v0}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    neg-float v2, p1

    iget-object v0, v6, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v7, 0x1

    aget v4, v4, v7

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v7, 0x2

    aget v5, v5, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    return-object v6
.end method

.method public static a(FFF[F)V
    .locals 7

    .prologue
    const v6, 0x3e22f983

    .line 163
    const/4 v0, 0x0

    neg-float v1, p0

    float-to-double v2, v1

    float-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v6

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v2, v2

    sub-float/2addr v1, v2

    aput v1, p3, v0

    .line 164
    const/4 v0, 0x1

    mul-float v1, p0, p0

    mul-float v2, p2, p2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    neg-float v2, p1

    float-to-double v4, v1

    float-to-double v2, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v6

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v2, v2

    sub-float/2addr v1, v2

    aput v1, p3, v0

    .line 165
    return-void
.end method

.method public static a(FF[FI)V
    .locals 6

    .prologue
    const v4, 0x40c90fdb

    .line 139
    mul-float v0, p0, v4

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 140
    mul-float v1, p0, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 141
    mul-float v2, p1, v4

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 142
    mul-float v3, p1, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    .line 144
    neg-float v4, v2

    mul-float/2addr v0, v4

    .line 145
    neg-float v3, v3

    .line 146
    mul-float/2addr v1, v2

    .line 148
    aput v0, p2, p3

    .line 149
    add-int/lit8 v0, p3, 0x1

    aput v3, p2, v0

    .line 150
    add-int/lit8 v0, p3, 0x2

    aput v1, p2, v0

    .line 151
    return-void
.end method

.method public static b(F)F
    .locals 2

    .prologue
    .line 67
    const v0, 0x40c90fdb

    mul-float/2addr v0, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static c(F)F
    .locals 2

    .prologue
    .line 72
    const v0, 0x40c90fdb

    mul-float/2addr v0, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static d(F)F
    .locals 1

    .prologue
    .line 83
    const v0, 0x3e22f983

    mul-float/2addr v0, p0

    return v0
.end method

.method public static e(F)F
    .locals 1

    .prologue
    .line 88
    const v0, 0x40c90fdb

    mul-float/2addr v0, p0

    return v0
.end method

.method public static f(F)F
    .locals 1

    .prologue
    .line 93
    const v0, 0x3b360b61

    mul-float/2addr v0, p0

    return v0
.end method

.method public static g(F)F
    .locals 1

    .prologue
    .line 98
    const/high16 v0, 0x43b40000    # 360.0f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static h(F)F
    .locals 1

    .prologue
    .line 103
    const v0, 0x3c8efa35

    mul-float/2addr v0, p0

    return v0
.end method

.method public static i(F)I
    .locals 4

    .prologue
    .line 170
    float-to-double v0, p0

    const-wide v2, 0x4036800000000000L    # 22.5

    add-double/2addr v0, v2

    const-wide v2, 0x4046800000000000L    # 45.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 172
    and-int/lit8 v0, v0, 0x7

    .line 173
    return v0
.end method
