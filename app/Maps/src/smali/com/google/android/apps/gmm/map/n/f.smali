.class public Lcom/google/android/apps/gmm/map/n/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field private a:I

.field private b:J

.field private final c:Lcom/google/android/apps/gmm/shared/c/f;

.field private final d:Lcom/google/android/apps/gmm/map/a/a;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/a/a;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/n/f;->a:I

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/f;->d:Lcom/google/android/apps/gmm/map/a/a;

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/n/f;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 65
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/n/f;->b:J

    .line 66
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/a/a;)Lcom/google/android/apps/gmm/map/n/f;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/map/n/f;

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/map/n/f;-><init>(Lcom/google/android/apps/gmm/map/a/a;Lcom/google/android/apps/gmm/shared/c/f;)V

    return-object v0
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 75
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 91
    :goto_1
    return v1

    :pswitch_0
    move v0, v2

    .line 75
    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 83
    const/16 v0, 0x19

    iget v3, p0, Lcom/google/android/apps/gmm/map/n/f;->a:I

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/n/f;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/n/f;->b:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0xfa

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    const/16 v0, 0xc

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 84
    :goto_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/n/f;->a:I

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/f;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/n/f;->b:J

    :cond_2
    :goto_3
    move v1, v2

    .line 91
    goto :goto_1

    .line 83
    :pswitch_1
    neg-int v0, v0

    :goto_4
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/n/f;->d:Lcom/google/android/apps/gmm/map/a/a;

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-interface {v3, v1, v0}, Lcom/google/android/apps/gmm/map/a/a;->a(FF)V

    goto :goto_2

    :pswitch_3
    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_4

    :pswitch_4
    neg-int v0, v0

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_4

    .line 86
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 87
    iget v0, p0, Lcom/google/android/apps/gmm/map/n/f;->a:I

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/n/f;->a:I

    goto :goto_3

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 83
    :pswitch_data_1
    .packed-switch 0x13
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
