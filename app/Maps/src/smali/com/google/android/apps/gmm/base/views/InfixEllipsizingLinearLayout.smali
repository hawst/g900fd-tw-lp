.class public Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->a:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->b:Ljava/util/List;

    .line 72
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getChildCount()I

    move-result v7

    move v5, v6

    move v2, v6

    move v4, v6

    .line 90
    :goto_0
    if-ge v5, v7, :cond_4

    .line 91
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 93
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 94
    check-cast v0, Landroid/widget/TextView;

    .line 95
    invoke-virtual {v0}, Landroid/widget/TextView;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    .line 99
    :goto_1
    if-eqz v0, :cond_2

    .line 100
    if-eqz v2, :cond_0

    .line 101
    const-string v0, "InfixEllipsizingLinearLayout"

    const-string v4, "Unsupported ellipsizing configuration inside InfixEllipsizingLinearLayout. Your children will not be ellipsized correctly. Please read class docs and fix."

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v0, v4, v8}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    move v1, v3

    .line 90
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v4, v1

    move v2, v0

    goto :goto_0

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 107
    :cond_2
    if-eqz v4, :cond_3

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v3

    move v1, v4

    goto :goto_2

    .line 111
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    move v1, v4

    goto :goto_2

    .line 114
    :cond_4
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingBottom()I

    move-result v1

    sub-int v5, v0, v1

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    .line 198
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->c:I

    .line 199
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->d:I

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getChildCount()I

    move-result v6

    .line 202
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_9

    .line 203
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 204
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 202
    :cond_0
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 204
    :cond_1
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    and-int/lit8 v1, v1, 0x30

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingTop()I

    move-result v1

    :goto_2
    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    add-int v9, v2, v3

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int v3, v2, v9

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->d:I

    iget v10, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->c:I

    sub-int/2addr v2, v10

    if-le v3, v2, :cond_4

    if-lez v2, :cond_0

    :goto_3
    const/4 v3, 0x0

    sub-int v9, v2, v9

    invoke-static {v3, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v10

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    const/4 v11, -0x1

    if-eq v3, v11, :cond_5

    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_7

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    const v11, 0x800005

    and-int/2addr v3, v11

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_5
    xor-int/2addr v3, v10

    if-nez v3, :cond_8

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->c:I

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v3

    add-int v3, v0, v9

    add-int/2addr v8, v1

    invoke-virtual {v7, v0, v1, v3, v8}, Landroid/view/View;->layout(IIII)V

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->c:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->c:I

    goto :goto_1

    :cond_2
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    and-int/lit8 v1, v1, 0x11

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingTop()I

    move-result v1

    sub-int v2, v5, v8

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingTop()I

    move-result v1

    sub-int v2, v5, v8

    add-int/2addr v1, v2

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    goto :goto_5

    :cond_8
    iget v3, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->d:I

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    sub-int v0, v3, v0

    sub-int v3, v0, v9

    add-int/2addr v8, v1

    invoke-virtual {v7, v3, v1, v0, v8}, Landroid/view/View;->layout(IIII)V

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->d:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->d:I

    goto/16 :goto_1

    .line 206
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v4, 0x0

    .line 118
    .line 122
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const v0, 0x7fffffff

    move v2, v0

    .line 128
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    sub-int v0, v2, v0

    move v3, v4

    move v5, v0

    move v6, v4

    move v7, v4

    .line 133
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 135
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v10, :cond_5

    .line 136
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->measureChild(Landroid/view/View;II)V

    .line 141
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 142
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    iget v9, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    add-int/2addr v8, v9

    iget v1, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    add-int/2addr v1, v8

    .line 143
    sub-int/2addr v5, v1

    .line 146
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v6, v1}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->combineMeasuredStates(II)I

    move-result v6

    .line 147
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v7

    move v1, v5

    move v5, v6

    move v6, v7

    .line 133
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v7, v6

    move v6, v5

    move v5, v1

    goto :goto_1

    .line 125
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_1
    move v3, v4

    .line 151
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 153
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v10, :cond_3

    .line 154
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 158
    iget v8, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v1, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    add-int/2addr v8, v1

    .line 159
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 160
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    if-eqz v9, :cond_2

    .line 161
    sub-int v1, v5, v8

    const/high16 v9, -0x80000000

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 164
    :cond_2
    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->measureChild(Landroid/view/View;II)V

    .line 165
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v8

    .line 166
    sub-int/2addr v5, v1

    .line 169
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v6, v1}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->combineMeasuredStates(II)I

    move-result v6

    .line 170
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 151
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 173
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getSuggestedMinimumWidth()I

    move-result v0

    .line 174
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 175
    invoke-static {v0, p1, v6}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->resolveSizeAndState(III)I

    move-result v0

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, v7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 179
    shl-int/lit8 v2, v6, 0x10

    .line 180
    invoke-static {v1, p2, v2}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->resolveSizeAndState(III)I

    move-result v1

    .line 182
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/InfixEllipsizingLinearLayout;->setMeasuredDimension(II)V

    .line 183
    return-void

    :cond_5
    move v1, v5

    move v5, v6

    move v6, v7

    goto/16 :goto_2
.end method
