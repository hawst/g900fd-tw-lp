.class public Lcom/google/android/apps/gmm/feedback/b/e;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/feedback/d/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 21
    sget v0, Lcom/google/android/apps/gmm/l;->na:I

    .line 22
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v3

    new-array v0, v1, [Lcom/google/android/libraries/curvular/cu;

    sget v4, Lcom/google/android/apps/gmm/f;->fN:I

    .line 24
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v0, v2

    .line 23
    new-instance v4, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.ImageView"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v4

    sget v0, Lcom/google/android/apps/gmm/l;->mZ:I

    .line 26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v0, 0x2

    new-array v6, v0, [Lcom/google/android/apps/gmm/base/f/ce;

    sget v0, Lcom/google/android/apps/gmm/l;->mT:I

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/d/f;->a()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    .line 28
    new-instance v8, Lcom/google/android/apps/gmm/base/f/ce;

    invoke-direct {v8, v7, v0}, Lcom/google/android/apps/gmm/base/f/ce;-><init>(Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;)V

    aput-object v8, v6, v2

    sget v0, Lcom/google/android/apps/gmm/l;->fU:I

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/d/f;->b()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    .line 31
    new-instance v8, Lcom/google/android/apps/gmm/base/f/ce;

    invoke-direct {v8, v7, v0}, Lcom/google/android/apps/gmm/base/f/ce;-><init>(Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;)V

    aput-object v8, v6, v1

    .line 27
    if-nez v6, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v7, v6

    if-ltz v7, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x5

    int-to-long v8, v7

    add-long/2addr v0, v8

    div-int/lit8 v2, v7, 0xa

    int-to-long v8, v2

    add-long/2addr v0, v8

    const-wide/32 v8, 0x7fffffff

    cmp-long v2, v0, v8

    if-lez v2, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v6}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 21
    invoke-static {v3, v4, v5, v1}, Lcom/google/android/apps/gmm/base/f/cd;->a(Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cs;Ljava/lang/CharSequence;Ljava/util/List;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 27
    :cond_3
    const-wide/32 v8, -0x80000000

    cmp-long v2, v0, v8

    if-gez v2, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v0

    goto :goto_1
.end method
