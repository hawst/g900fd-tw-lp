.class public Lcom/google/android/apps/gmm/base/views/MapViewContainer;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/base/views/MapViewContainer;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/android/apps/gmm/base/views/aj;


# instance fields
.field public c:Lcom/google/android/apps/gmm/map/MapFragment;

.field public d:Lcom/google/android/apps/gmm/map/b/a/q;

.field public e:Z

.field private f:Z

.field private g:Lcom/google/android/apps/gmm/map/f/a/a;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/google/b/c/hj;->e()Ljava/util/IdentityHashMap;

    move-result-object v0

    new-instance v1, Lcom/google/b/c/jt;

    invoke-direct {v1, v0}, Lcom/google/b/c/jt;-><init>(Ljava/util/Map;)V

    sput-object v1, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a:Ljava/util/Set;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/base/views/aj;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a:Ljava/util/Set;

    .line 53
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/aj;-><init>(Ljava/util/Set;)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->b:Lcom/google/android/apps/gmm/base/views/aj;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->f:Z

    .line 81
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->h:Z

    .line 85
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 252
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->q:Z

    if-eqz v0, :cond_0

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 264
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/ai;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/views/ai;-><init>(Lcom/google/android/apps/gmm/base/views/MapViewContainer;Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 141
    instance-of v0, p1, Landroid/view/TextureView;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->h:Z

    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getWidth()I

    move-result v1

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getHeight()I

    move-result v0

    .line 147
    :cond_0
    div-int/lit8 v1, v1, 0x2

    .line 148
    div-int/lit8 v0, v0, 0x2

    .line 149
    mul-int v2, v1, v0

    shl-int/lit8 v2, v2, 0x2

    const/high16 v3, 0x200000

    if-ge v2, v3, :cond_0

    .line 151
    check-cast p1, Landroid/view/TextureView;

    invoke-virtual {p1, v1, v0}, Landroid/view/TextureView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 152
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 153
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->g:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->g:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 158
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    .line 157
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 161
    :cond_2
    iput-object v4, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->removeAllViews()V

    .line 163
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/MapFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    if-ne v0, p1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 99
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 100
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/MapFragment;->getView()Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 101
    :goto_2
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 103
    :goto_3
    if-eq v0, p0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->removeAllViews()V

    .line 109
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setBackgroundResource(I)V

    .line 111
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/MapFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 112
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    if-nez v3, :cond_8

    :goto_4
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->g:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 117
    :goto_5
    if-eqz v2, :cond_5

    .line 118
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    if-eqz v1, :cond_a

    .line 119
    check-cast v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Landroid/view/View;)V

    .line 123
    :cond_4
    :goto_6
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->addView(Landroid/view/View;)V

    .line 126
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->f:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setInteractive(Z)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setPinLatLng(Lcom/google/android/apps/gmm/map/b/a/q;)V

    goto :goto_0

    :cond_6
    move-object v2, v1

    .line 100
    goto :goto_2

    :cond_7
    move-object v0, v1

    .line 101
    goto :goto_3

    .line 112
    :cond_8
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_4

    .line 114
    :cond_9
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->g:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_5

    .line 120
    :cond_a
    if-eqz v0, :cond_4

    .line 121
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_6
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 227
    sget-object v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 228
    sget-object v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->b:Lcom/google/android/apps/gmm/base/views/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Ljava/lang/Object;)V

    .line 232
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 239
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 240
    sget-object v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 241
    sget-object v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->b:Lcom/google/android/apps/gmm/base/views/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->a(Ljava/lang/Object;)V

    .line 242
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->f:Z

    if-nez v0, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final setInteractive(Z)V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 182
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 183
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 189
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->f:Z

    .line 190
    return-void

    .line 182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setPinLatLng(Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 196
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 199
    if-eqz v3, :cond_1

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    sget-object v4, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v3, v0, v4, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 204
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 199
    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 269
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 270
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->b:Lcom/google/android/apps/gmm/base/views/aj;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 272
    :cond_0
    return-void
.end method
