.class public Lcom/google/android/apps/gmm/directions/i/bg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/w;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field final a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:Lcom/google/android/apps/gmm/directions/h/x;

.field private final d:I

.field private final e:Lcom/google/android/apps/gmm/directions/h/z;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/maps/g/a/hm;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/android/libraries/curvular/aw;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Lcom/google/android/apps/gmm/z/b/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/directions/i/bg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/bg;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/h/z;)V
    .locals 4
    .param p5    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50
    :cond_0
    iput p3, p0, Lcom/google/android/apps/gmm/directions/i/bg;->d:I

    .line 51
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/i/bg;->e:Lcom/google/android/apps/gmm/directions/h/z;

    .line 53
    invoke-static {p2}, Lcom/google/android/apps/gmm/directions/f/d/h;->b(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->f:Lcom/google/maps/g/a/hm;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->f:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/d/i;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->g:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->f:Lcom/google/maps/g/a/hm;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bg;->c:Ljava/lang/String;

    const-string v2, "Travel mode should not be null."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->h:Lcom/google/android/libraries/curvular/aw;

    .line 56
    invoke-interface {p4}, Lcom/google/android/apps/gmm/directions/h/y;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->a:Ljava/lang/String;

    .line 57
    invoke-static {p2}, Lcom/google/android/apps/gmm/directions/f/d/h;->d(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->i:Lcom/google/android/apps/gmm/z/b/l;

    .line 58
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    iget-boolean v0, v0, Lcom/google/maps/g/a/fk;->i:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->j:Z

    .line 59
    return-void

    .line 54
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->fX:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->nV:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->oR:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/l;->be:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 55
    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/directions/i/bh;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :pswitch_4
    sget v0, Lcom/google/android/apps/gmm/f;->cZ:I

    sget v1, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    goto :goto_1

    :pswitch_5
    sget v0, Lcom/google/android/apps/gmm/f;->fx:I

    sget v1, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    goto :goto_1

    :pswitch_6
    sget v0, Lcom/google/android/apps/gmm/f;->cG:I

    sget v1, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    goto :goto_1

    :pswitch_7
    sget v0, Lcom/google/android/apps/gmm/f;->fs:I

    sget v1, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    goto :goto_1

    .line 58
    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 55
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final B()Lcom/google/android/apps/gmm/directions/h/x;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->b:Lcom/google/android/apps/gmm/directions/h/x;

    return-object v0
.end method

.method public final C()Lcom/google/maps/g/a/hm;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->f:Lcom/google/maps/g/a/hm;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/google/android/libraries/curvular/aw;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->h:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->e:Lcom/google/android/apps/gmm/directions/h/z;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->e:Lcom/google/android/apps/gmm/directions/h/z;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->d:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/h/z;->a(IZ)V

    .line 116
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final H()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->e:Lcom/google/android/apps/gmm/directions/h/z;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->e:Lcom/google/android/apps/gmm/directions/h/z;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->d:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/h/z;->a(IZ)V

    .line 126
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final I()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a([Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bg;->i:Lcom/google/android/apps/gmm/z/b/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 105
    iput-object p1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 106
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/directions/h/x;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->b:Lcom/google/android/apps/gmm/directions/h/x;

    .line 64
    return-void
.end method

.method public j()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method
