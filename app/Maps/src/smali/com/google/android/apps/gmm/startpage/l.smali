.class Lcom/google/android/apps/gmm/startpage/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/startpage/d/e;

.field private b:Lcom/google/o/h/a/dq;

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/l;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 285
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/l;->b:Lcom/google/o/h/a/dq;

    .line 286
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/l;->c:Ljava/lang/String;

    .line 287
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 291
    if-ne p0, p1, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v0

    .line 294
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/startpage/l;

    if-eqz v2, :cond_5

    .line 295
    check-cast p1, Lcom/google/android/apps/gmm/startpage/l;

    .line 296
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/l;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/l;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/startpage/d/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/l;->b:Lcom/google/o/h/a/dq;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/l;->b:Lcom/google/o/h/a/dq;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/l;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/l;->c:Ljava/lang/String;

    .line 297
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 299
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 304
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/l;->b:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/l;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/l;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
