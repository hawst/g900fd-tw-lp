.class public final enum Lcom/google/android/apps/gmm/directions/di;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/di;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum d:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum e:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum f:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum g:Lcom/google/android/apps/gmm/directions/di;

.field public static final enum h:Lcom/google/android/apps/gmm/directions/di;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/directions/di;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->a:Lcom/google/android/apps/gmm/directions/di;

    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "RESULT_LIST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->b:Lcom/google/android/apps/gmm/directions/di;

    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "NO_ROUTES_FOUND"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->c:Lcom/google/android/apps/gmm/directions/di;

    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "BAD_WAYPOINT_COUNT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->d:Lcom/google/android/apps/gmm/directions/di;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "WAYPOINT_FAILURE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->e:Lcom/google/android/apps/gmm/directions/di;

    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "NO_TRIPS_ON_GIVEN_DATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->f:Lcom/google/android/apps/gmm/directions/di;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "NAVIGATION_NOT_ALLOWED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->g:Lcom/google/android/apps/gmm/directions/di;

    new-instance v0, Lcom/google/android/apps/gmm/directions/di;

    const-string v1, "NETWORK_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/di;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->h:Lcom/google/android/apps/gmm/directions/di;

    .line 73
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/di;

    sget-object v1, Lcom/google/android/apps/gmm/directions/di;->a:Lcom/google/android/apps/gmm/directions/di;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/directions/di;->b:Lcom/google/android/apps/gmm/directions/di;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/directions/di;->c:Lcom/google/android/apps/gmm/directions/di;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/directions/di;->d:Lcom/google/android/apps/gmm/directions/di;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/directions/di;->e:Lcom/google/android/apps/gmm/directions/di;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/directions/di;->f:Lcom/google/android/apps/gmm/directions/di;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/directions/di;->g:Lcom/google/android/apps/gmm/directions/di;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/directions/di;->h:Lcom/google/android/apps/gmm/directions/di;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/directions/di;->i:[Lcom/google/android/apps/gmm/directions/di;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/di;
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/google/android/apps/gmm/directions/di;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/di;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/di;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->i:[Lcom/google/android/apps/gmm/directions/di;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/di;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/di;

    return-object v0
.end method
