.class public Lcom/google/android/apps/gmm/map/r/a/aj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final t:Lcom/google/android/apps/gmm/map/b/a/y;


# instance fields
.field final a:Lcom/google/maps/g/a/fk;

.field final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field final c:Lcom/google/maps/g/a/ez;

.field final d:Lcom/google/maps/g/a/fb;

.field final e:Lcom/google/maps/g/a/fd;

.field final f:I

.field final g:I

.field final h:I

.field public final i:I

.field public final j:I

.field final k:F

.field final l:F

.field final m:Ljava/lang/String;

.field final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field

.field final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            ">;"
        }
    .end annotation
.end field

.field final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;"
        }
    .end annotation
.end field

.field final r:Lcom/google/maps/g/by;

.field final s:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 86
    invoke-static {v0, v1, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/aj;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IIIIIFFLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;)V
    .locals 1
    .param p12    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p17    # Lcom/google/maps/g/a/fk;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p18    # Lcom/google/maps/g/by;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p19    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Lcom/google/maps/g/a/ez;",
            "Lcom/google/maps/g/a/fb;",
            "Lcom/google/maps/g/a/fd;",
            "IIIIIFF",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;",
            "Lcom/google/maps/g/a/fk;",
            "Lcom/google/maps/g/by;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 102
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->c:Lcom/google/maps/g/a/ez;

    .line 103
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->d:Lcom/google/maps/g/a/fb;

    .line 104
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->e:Lcom/google/maps/g/a/fd;

    .line 105
    iput p5, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->f:I

    .line 106
    iput p6, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->g:I

    .line 107
    iput p7, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->h:I

    .line 108
    iput p8, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->i:I

    .line 109
    iput p9, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->j:I

    .line 110
    iput p10, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->k:F

    .line 111
    iput p11, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->l:F

    .line 112
    if-nez p12, :cond_0

    const-string p12, ""

    :cond_0
    iput-object p12, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->m:Ljava/lang/String;

    .line 113
    iput-object p13, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->n:Ljava/util/List;

    .line 114
    iput-object p14, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->o:Ljava/util/List;

    .line 115
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->p:Ljava/util/List;

    .line 116
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->q:Ljava/util/List;

    .line 117
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->a:Lcom/google/maps/g/a/fk;

    .line 118
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->r:Lcom/google/maps/g/by;

    .line 119
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->s:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/eu;Lcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/aj;
    .locals 34
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/y;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 137
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/r/a/aj;->a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    .line 138
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_7

    .line 139
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->p:I

    move v13, v2

    .line 140
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget v2, v2, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget-object v3, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v3, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_6
    iget v2, v2, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget-object v3, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v3, :cond_c

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    :goto_8
    iget v2, v2, Lcom/google/maps/g/a/ai;->b:I

    move/from16 v22, v2

    .line 141
    :goto_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    const/4 v2, 0x1

    :goto_a
    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget v2, v2, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget-object v3, v2, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_10

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_c
    iget v2, v2, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_11

    const/4 v2, 0x1

    :goto_d
    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget-object v3, v2, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_12

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_e
    iget v11, v2, Lcom/google/maps/g/a/be;->b:I

    .line 142
    :goto_f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->f:I

    invoke-static {v2}, Lcom/google/maps/g/a/ez;->a(I)Lcom/google/maps/g/a/ez;

    move-result-object v2

    if-nez v2, :cond_14

    sget-object v2, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    move-object/from16 v23, v2

    .line 143
    :goto_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->h:I

    invoke-static {v2}, Lcom/google/maps/g/a/fb;->a(I)Lcom/google/maps/g/a/fb;

    move-result-object v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    move-object/from16 v20, v2

    .line 144
    :goto_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->g:I

    invoke-static {v2}, Lcom/google/maps/g/a/fd;->a(I)Lcom/google/maps/g/a/fd;

    move-result-object v2

    if-nez v2, :cond_16

    sget-object v2, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    move-object/from16 v24, v2

    .line 145
    :goto_12
    sget-object v3, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->f:I

    invoke-static {v2}, Lcom/google/maps/g/a/ez;->a(I)Lcom/google/maps/g/a/ez;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    :cond_0
    if-ne v3, v2, :cond_18

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_17

    const/4 v2, 0x1

    :goto_13
    if-eqz v2, :cond_18

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->k:I

    move/from16 v25, v2

    .line 146
    :goto_14
    sget-object v3, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->f:I

    invoke-static {v2}, Lcom/google/maps/g/a/ez;->a(I)Lcom/google/maps/g/a/ez;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    :cond_1
    if-ne v3, v2, :cond_1a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_19

    const/4 v2, 0x1

    :goto_15
    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->j:I

    move/from16 v26, v2

    .line 147
    :goto_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fk;

    iget-object v0, v2, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    move-object/from16 v16, v0

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v19

    check-cast v19, Lcom/google/maps/g/a/fk;

    .line 153
    if-nez p1, :cond_2

    .line 154
    sget-object p1, Lcom/google/android/apps/gmm/map/r/a/aj;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 157
    :cond_2
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v28

    const/4 v2, 0x0

    move v14, v2

    :goto_17
    if-ge v14, v15, :cond_26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->o:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/bu;->d()Lcom/google/maps/g/a/bu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/bu;

    iget v3, v2, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1b

    const/4 v3, 0x1

    :goto_18
    if-nez v3, :cond_1c

    const/4 v2, 0x0

    :goto_19
    if-eqz v2, :cond_3

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_17

    .line 137
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_5
    const-string v2, ""

    move-object v12, v2

    goto/16 :goto_1

    .line 138
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 139
    :cond_7
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_3

    .line 140
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_a
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_6

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_c
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_8

    :cond_d
    const/4 v2, 0x0

    move/from16 v22, v2

    goto/16 :goto_9

    .line 141
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_10
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_c

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_12
    iget-object v2, v2, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_e

    :cond_13
    const/4 v11, 0x0

    goto/16 :goto_f

    :cond_14
    move-object/from16 v23, v2

    .line 142
    goto/16 :goto_10

    :cond_15
    move-object/from16 v20, v2

    .line 143
    goto/16 :goto_11

    :cond_16
    move-object/from16 v24, v2

    .line 144
    goto/16 :goto_12

    .line 145
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_13

    :cond_18
    const/4 v2, -0x1

    move/from16 v25, v2

    goto/16 :goto_14

    .line 146
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_15

    :cond_1a
    const/4 v2, -0x1

    move/from16 v26, v2

    goto/16 :goto_16

    .line 157
    :cond_1b
    const/4 v3, 0x0

    goto :goto_18

    :cond_1c
    iget v3, v2, Lcom/google/maps/g/a/bu;->b:I

    invoke-static {v3}, Lcom/google/maps/g/a/bx;->a(I)Lcom/google/maps/g/a/bx;

    move-result-object v3

    if-nez v3, :cond_1d

    sget-object v3, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    :cond_1d
    iget v4, v2, Lcom/google/maps/g/a/bu;->c:I

    int-to-double v4, v4

    mul-double v30, v4, v28

    iget v5, v2, Lcom/google/maps/g/a/bu;->d:I

    iget v4, v2, Lcom/google/maps/g/a/bu;->e:I

    int-to-double v6, v4

    mul-double v32, v6, v28

    iget-boolean v7, v2, Lcom/google/maps/g/a/bu;->f:Z

    iget v4, v2, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v6, 0x20

    if-ne v4, v6, :cond_1f

    const/4 v4, 0x1

    :goto_1a
    if-eqz v4, :cond_21

    iget-object v4, v2, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    instance-of v6, v4, Ljava/lang/String;

    if-eqz v6, :cond_20

    check-cast v4, Ljava/lang/String;

    move-object v8, v4

    :cond_1e
    :goto_1b
    iget v4, v2, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v6, 0x40

    if-ne v4, v6, :cond_22

    const/4 v4, 0x1

    :goto_1c
    if-eqz v4, :cond_23

    iget v9, v2, Lcom/google/maps/g/a/bu;->h:I

    :goto_1d
    iget v4, v2, Lcom/google/maps/g/a/bu;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v6, 0x80

    if-ne v4, v6, :cond_24

    const/4 v4, 0x1

    :goto_1e
    if-eqz v4, :cond_25

    iget v10, v2, Lcom/google/maps/g/a/bu;->i:I

    :goto_1f
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/am;

    move-wide/from16 v0, v30

    double-to-int v4, v0

    move-wide/from16 v0, v32

    double-to-int v6, v0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/r/a/am;-><init>(Lcom/google/maps/g/a/bx;IIIZLjava/lang/String;II)V

    goto/16 :goto_19

    :cond_1f
    const/4 v4, 0x0

    goto :goto_1a

    :cond_20
    check-cast v4, Lcom/google/n/f;

    invoke-virtual {v4}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lcom/google/n/f;->e()Z

    move-result v4

    if-eqz v4, :cond_1e

    iput-object v8, v2, Lcom/google/maps/g/a/bu;->g:Ljava/lang/Object;

    goto :goto_1b

    :cond_21
    const/4 v8, 0x0

    goto :goto_1b

    :cond_22
    const/4 v4, 0x0

    goto :goto_1c

    :cond_23
    const/4 v9, -0x1

    goto :goto_1d

    :cond_24
    const/4 v4, 0x0

    goto :goto_1e

    :cond_25
    const/4 v10, -0x1

    goto :goto_1f

    .line 158
    :cond_26
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/a/aj;->b(Lcom/google/maps/g/a/eu;)Ljava/util/List;

    move-result-object v18

    .line 159
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/a/aj;->a(Lcom/google/maps/g/a/eu;)Ljava/util/ArrayList;

    move-result-object v15

    .line 165
    sget-object v2, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, v23

    if-ne v0, v2, :cond_30

    if-eqz v12, :cond_27

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2b

    :cond_27
    const/4 v2, 0x1

    :goto_20
    if-eqz v2, :cond_30

    .line 166
    invoke-static {v15}, Lcom/google/android/apps/gmm/map/r/a/aj;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v12

    .line 167
    if-eqz p4, :cond_30

    if-eqz v12, :cond_28

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2c

    :cond_28
    const/4 v2, 0x1

    :goto_21
    if-eqz v2, :cond_30

    .line 169
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v12

    move-object v14, v12

    .line 179
    :goto_22
    sget-object v2, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, v23

    if-ne v0, v2, :cond_29

    .line 180
    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/ak;->a:[I

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/gmm/map/r/a/n;->a(Ljava/util/List;)Lcom/google/maps/g/a/df;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/maps/g/a/df;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_29
    move-object/from16 v5, v20

    .line 186
    :goto_23
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/g/a/eu;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_2d

    const/4 v2, 0x1

    :goto_24
    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/by;->g()Lcom/google/maps/g/by;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/by;

    move-object/from16 v20, v2

    .line 199
    :goto_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_2f

    check-cast v2, Ljava/lang/String;

    move-object/from16 v21, v2

    .line 201
    :cond_2a
    :goto_26
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/aj;

    move-object/from16 v3, p1

    move-object/from16 v4, v23

    move-object/from16 v6, v24

    move/from16 v7, v25

    move/from16 v8, v26

    move v9, v13

    move/from16 v10, v22

    move/from16 v12, p2

    move/from16 v13, p3

    invoke-direct/range {v2 .. v21}, Lcom/google/android/apps/gmm/map/r/a/aj;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IIIIIFFLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;)V

    return-object v2

    .line 165
    :cond_2b
    const/4 v2, 0x0

    goto :goto_20

    .line 167
    :cond_2c
    const/4 v2, 0x0

    goto :goto_21

    .line 182
    :pswitch_0
    sget-object v20, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    move-object/from16 v5, v20

    .line 183
    goto :goto_23

    .line 185
    :pswitch_1
    sget-object v20, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    move-object/from16 v5, v20

    goto :goto_23

    .line 186
    :cond_2d
    const/4 v2, 0x0

    goto :goto_24

    :cond_2e
    const/16 v20, 0x0

    goto :goto_25

    .line 199
    :cond_2f
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_2a

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/maps/g/a/eu;->b:Ljava/lang/Object;

    goto :goto_26

    :cond_30
    move-object v14, v12

    goto :goto_22

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 478
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 479
    :goto_0
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 480
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 481
    iget v3, v0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v5, 0x2

    if-ne v3, v5, :cond_1

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_0

    .line 482
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 481
    goto :goto_1

    .line 485
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 497
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 499
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 500
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v1, v1, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    :cond_1
    sget-object v6, Lcom/google/maps/g/a/af;->g:Lcom/google/maps/g/a/af;

    if-ne v1, v6, :cond_0

    .line 501
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_8

    :cond_2
    move v1, v3

    :goto_0
    if-nez v1, :cond_0

    .line 502
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    :cond_3
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 510
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v1, v1, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    :cond_5
    sget-object v6, Lcom/google/maps/g/a/af;->h:Lcom/google/maps/g/a/af;

    if-ne v1, v6, :cond_4

    .line 511
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_9

    :cond_6
    move v1, v3

    :goto_2
    if-nez v1, :cond_4

    .line 512
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 513
    const/16 v1, 0xa

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 515
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_8
    move v1, v2

    .line 501
    goto :goto_0

    :cond_9
    move v1, v2

    .line 511
    goto :goto_2

    .line 520
    :cond_a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/maps/g/a/eu;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/eu;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/maps/g/a/eu;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ac;->d()Lcom/google/maps/g/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ac;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 369
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 370
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ac;

    .line 371
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a(Lcom/google/maps/g/a/ac;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v0

    .line 372
    if-eqz v0, :cond_1

    .line 373
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 376
    :cond_2
    return-object v2
.end method

.method private static b(Lcom/google/maps/g/a/eu;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/eu;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 457
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 458
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 459
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 460
    iget-object v0, p0, Lcom/google/maps/g/a/eu;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/cg;->d()Lcom/google/maps/g/a/cg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/cg;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/j;->a(Lcom/google/maps/g/a/cg;)Lcom/google/android/apps/gmm/map/r/a/j;

    move-result-object v0

    .line 461
    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 471
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v3, v0, :cond_2

    .line 472
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 474
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 343
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "location"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 344
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maneuverType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->c:Lcom/google/maps/g/a/ez;

    .line 345
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "turnSide"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->d:Lcom/google/maps/g/a/fb;

    .line 346
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "roundaboutTurnAngle"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->f:I

    .line 347
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "polylineVertexOffset"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->h:I

    .line 348
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "incomingBearing"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->k:F

    .line 349
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "outgoingBearing"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->l:F

    .line 350
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->m:Ljava/lang/String;

    .line 351
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "distanceFromPrevStepMeters"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->i:I

    .line 352
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "timeFromPrevStepSeconds"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->j:I

    .line 353
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepCues"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->n:Ljava/util/List;

    .line 354
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "notices"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->o:Ljava/util/List;

    .line 355
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepGuidances"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->p:Ljava/util/List;

    .line 356
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "summary"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->a:Lcom/google/maps/g/a/fk;

    .line 357
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "level"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->r:Lcom/google/maps/g/by;

    .line 358
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "ved"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->s:Ljava/lang/String;

    .line 359
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "laneGuidances"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/aj;->q:Ljava/util/List;

    .line 360
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 361
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
