.class public Lcom/google/android/apps/gmm/map/legacy/internal/b/h;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:F

.field private static final c:F


# instance fields
.field a:Lcom/google/android/apps/gmm/map/t/q;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/internal/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private f:F

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:F

.field private p:Z

.field private q:Lcom/google/android/apps/gmm/v/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 65
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->b:F

    .line 80
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->c:F

    .line 79
    return-void
.end method

.method public constructor <init>(IFIZZ)V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    .line 156
    iput p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->e:I

    .line 157
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->f:F

    .line 158
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->g:I

    .line 159
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->h:Z

    .line 160
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->i:Z

    .line 161
    return-void
.end method

.method public static a(FZZ)F
    .locals 4

    .prologue
    const/16 v1, 0x9

    .line 348
    if-nez p1, :cond_0

    .line 349
    const/high16 v0, 0x3f800000    # 1.0f

    .line 358
    :goto_0
    return v0

    .line 352
    :cond_0
    if-eqz p2, :cond_2

    const/4 v0, 0x6

    .line 353
    :goto_1
    int-to-float v2, v0

    const/high16 v3, 0x41900000    # 18.0f

    invoke-static {p0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 355
    if-eqz p2, :cond_1

    const/16 v1, 0xc

    .line 356
    :cond_1
    int-to-float v0, v0

    sub-float v0, v2, v0

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 357
    if-eqz p2, :cond_3

    sget v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->b:F

    .line 358
    :goto_2
    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 352
    goto :goto_1

    .line 357
    :cond_3
    sget v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->c:F

    goto :goto_2
.end method

.method private a(Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/high16 v4, 0x3fa00000    # 1.25f

    const/4 v1, 0x0

    .line 256
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 267
    :cond_1
    :goto_0
    return v0

    .line 260
    :cond_2
    monitor-enter p0

    .line 261
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z

    if-eqz v2, :cond_3

    .line 262
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z

    .line 263
    monitor-exit p0

    goto :goto_0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/o;->g:F

    .line 267
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->o:F

    mul-float/2addr v3, v4

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->o:F

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->q:Lcom/google/android/apps/gmm/v/ad;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :cond_0
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ae;)V
    .locals 54

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->q:Lcom/google/android/apps/gmm/v/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    .line 249
    :goto_0
    monitor-exit p0

    return-void

    .line 233
    :cond_0
    :try_start_1
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Lcom/google/android/apps/gmm/map/f/o;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->k:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->h()V

    const/4 v5, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v6, v5

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/l/c;->a(Lcom/google/android/apps/gmm/map/b/a/ab;)I

    move-result v4

    add-int/2addr v6, v4

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    :cond_1
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(I)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v18, v4

    :goto_2
    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v8, v5, v6

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v5, v5, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->h:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->i:Z

    invoke-static {v5, v6, v9}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(FZZ)F

    move-result v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->f:F

    const/4 v9, 0x0

    cmpl-float v5, v5, v9

    if-nez v5, :cond_3

    const/4 v5, 0x0

    :goto_3
    const/high16 v9, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v9, v10

    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v10, v11

    div-float/2addr v9, v10

    mul-float/2addr v5, v9

    mul-float v9, v5, v6

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->m:Z

    if-nez v5, :cond_2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->n:Z

    if-eqz v5, :cond_4

    :cond_2
    const/4 v5, 0x1

    move v6, v5

    :goto_4
    if-eqz v6, :cond_5

    sget-object v5, Lcom/google/android/apps/gmm/map/l/j;->b:Lcom/google/android/apps/gmm/map/l/j;

    :goto_5
    new-instance v10, Lcom/google/android/apps/gmm/map/l/i;

    invoke-direct {v10, v5}, Lcom/google/android/apps/gmm/map/l/i;-><init>(Lcom/google/android/apps/gmm/map/l/j;)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->l:Z

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->e:I

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    array-length v5, v5

    new-array v12, v5, [I

    const/4 v5, 0x0

    :goto_6
    array-length v13, v12

    if-ge v5, v13, :cond_6

    iget-object v13, v4, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v13, v13, v5

    iget-byte v13, v13, Lcom/google/android/apps/gmm/map/internal/c/aq;->b:B

    aput v13, v12, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_3
    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->f:F

    mul-float/2addr v5, v9

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    move v6, v5

    goto :goto_4

    :cond_5
    sget-object v5, Lcom/google/android/apps/gmm/map/l/j;->a:Lcom/google/android/apps/gmm/map/l/j;

    goto :goto_5

    :cond_6
    iput v11, v10, Lcom/google/android/apps/gmm/map/l/i;->d:I

    iput-object v12, v10, Lcom/google/android/apps/gmm/map/l/i;->e:[I

    :cond_7
    new-instance v21, Lcom/google/android/apps/gmm/map/l/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/legacy/internal/b/a;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v21

    invoke-direct {v0, v7, v8, v4, v10}, Lcom/google/android/apps/gmm/map/l/h;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;ILcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/l/i;)V

    move-object/from16 v0, v21

    iput v9, v0, Lcom/google/android/apps/gmm/map/l/h;->e:F

    move-object/from16 v0, v21

    iput v9, v0, Lcom/google/android/apps/gmm/map/l/h;->f:F

    if-eqz v6, :cond_8

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->m:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->n:Z

    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/l/h;->g:Z

    move-object/from16 v0, v21

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/map/l/h;->h:Z

    move-object/from16 v0, v21

    iput v9, v0, Lcom/google/android/apps/gmm/map/l/h;->i:F

    :cond_8
    invoke-static {}, Lcom/google/android/apps/gmm/map/l/c;->a()Lcom/google/android/apps/gmm/map/l/c;

    move-result-object v22

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/l/h;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v21

    iget v5, v0, Lcom/google/android/apps/gmm/map/l/h;->e:F

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/apps/gmm/map/l/h;->f:F

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    const/4 v8, 0x1

    if-le v4, v8, :cond_a

    const/4 v4, 0x0

    cmpl-float v4, v5, v4

    if-gez v4, :cond_9

    const/4 v4, 0x0

    cmpl-float v4, v6, v4

    if-ltz v4, :cond_a

    :cond_9
    sget-object v4, Lcom/google/android/apps/gmm/map/l/e;->a:[I

    iget-object v5, v7, Lcom/google/android/apps/gmm/map/l/i;->a:Lcom/google/android/apps/gmm/map/l/j;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/l/j;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    sget-object v4, Lcom/google/android/apps/gmm/map/l/c;->a:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/l/i;->a:Lcom/google/android/apps/gmm/map/l/j;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1a

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "unsupported texture type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    add-int/lit8 v4, v18, 0x1

    move/from16 v18, v4

    goto/16 :goto_2

    :pswitch_0
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/h;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v23, v0

    move-object/from16 v0, v21

    iget v5, v0, Lcom/google/android/apps/gmm/map/l/h;->e:F

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/apps/gmm/map/l/h;->f:F

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/h;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v24, v0

    move-object/from16 v0, v21

    iget v4, v0, Lcom/google/android/apps/gmm/map/l/h;->d:I

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/l/h;->g:Z

    move/from16 v25, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/l/h;->h:Z

    move/from16 v26, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/google/android/apps/gmm/map/l/h;->i:F

    move/from16 v27, v0

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    iget v0, v7, Lcom/google/android/apps/gmm/map/l/i;->c:I

    move/from16 v28, v0

    const/4 v7, 0x0

    mul-float/2addr v7, v6

    move/from16 v0, v28

    int-to-float v8, v0

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    add-float v8, v5, v6

    div-float/2addr v7, v8

    float-to-int v0, v7

    move/from16 v29, v0

    move-object/from16 v0, v23

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v7, v7

    div-int/lit8 v30, v7, 0x3

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/gmm/map/b/a/ab;->f()Z

    move-result v31

    new-instance v32, Lcom/google/android/apps/gmm/map/l/f;

    move-object/from16 v0, v32

    move-object/from16 v1, v19

    move-object/from16 v2, v19

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/gmm/map/l/f;-><init>(ILcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;)V

    move-object/from16 v0, v32

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/l/f;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->b()I

    move-result v4

    move-object/from16 v0, v32

    iput v4, v0, Lcom/google/android/apps/gmm/map/l/f;->d:I

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v33, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v34, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v35, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v36, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v37, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->g:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v38, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v39, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v40, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v41, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v42, v0

    const/4 v11, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v43, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v44, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->n:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v45, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->o:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v46, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/c;->p:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v47, v0

    const/4 v9, -0x1

    const/4 v8, -0x1

    const/4 v7, -0x1

    const/4 v4, 0x0

    move v12, v7

    move v14, v8

    move v15, v9

    move-object v7, v10

    move-object v8, v11

    move v11, v4

    :goto_7
    move/from16 v0, v30

    if-ge v11, v0, :cond_a

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v13, v4, 0x3

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/apps/gmm/map/b/a/ab;->f()Z

    move-result v10

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v34

    invoke-virtual {v0, v11, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v9, v11, -0x1

    add-int/lit8 v4, v11, 0x1

    if-eqz v10, :cond_1f

    if-gez v9, :cond_b

    add-int/lit8 v9, v13, -0x2

    :cond_b
    if-lt v4, v13, :cond_1f

    const/4 v4, 0x1

    move v10, v9

    move v9, v4

    :goto_8
    sget-object v4, Lcom/google/android/apps/gmm/map/l/g;->b:Lcom/google/android/apps/gmm/map/l/g;

    if-ltz v10, :cond_f

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v33

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    :goto_9
    if-ge v9, v13, :cond_10

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v35

    invoke-virtual {v0, v9, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object v10, v4

    :goto_a
    const/4 v4, 0x1

    sget-object v9, Lcom/google/android/apps/gmm/map/l/e;->b:[I

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/l/g;->ordinal()I

    move-result v13

    aget v9, v9, v13

    packed-switch v9, :pswitch_data_1

    move-object v9, v8

    move-object v8, v7

    :goto_b
    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    invoke-static {v7, v11}, Lcom/google/android/apps/gmm/map/l/c;->a(Lcom/google/android/apps/gmm/map/l/i;I)I

    move-result v48

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    add-int/lit8 v13, v11, 0x1

    invoke-static {v7, v13}, Lcom/google/android/apps/gmm/map/l/c;->a(Lcom/google/android/apps/gmm/map/l/i;I)I

    move-result v49

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move/from16 v2, v29

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v50

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move/from16 v2, v29

    move/from16 v3, v49

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v13

    if-eqz v4, :cond_13

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v4, v5

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v4

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v4, v6

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v6

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v37

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v48

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v17

    const/4 v4, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v49

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v7

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    move/from16 v2, v28

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v4

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    move/from16 v2, v28

    move/from16 v3, v49

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v16

    sget-object v48, Lcom/google/android/apps/gmm/map/l/g;->a:Lcom/google/android/apps/gmm/map/l/g;

    move-object/from16 v0, v48

    if-ne v10, v0, :cond_c

    if-eqz v25, :cond_c

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    move-object/from16 v48, v0

    const/16 v49, -0x1

    invoke-static/range {v48 .. v49}, Lcom/google/android/apps/gmm/map/l/c;->a(Lcom/google/android/apps/gmm/map/l/i;I)I

    move-result v48

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v49, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v51, v0

    move/from16 v0, v51

    neg-float v0, v0

    move/from16 v51, v0

    move/from16 v0, v51

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v0, v27

    neg-float v0, v0

    move/from16 v49, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v51, v0

    mul-float v51, v51, v49

    move/from16 v0, v51

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v51, v0

    mul-float v49, v49, v51

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v41

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v0, v5

    move/from16 v49, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v51, v0

    mul-float v51, v51, v49

    move/from16 v0, v51

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v51, v0

    mul-float v49, v49, v51

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v41

    move-object/from16 v1, v47

    move-object/from16 v2, v40

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v49, v0

    mul-float v49, v49, v6

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v49, v0

    mul-float v49, v49, v6

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v41

    move-object/from16 v1, v47

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    move/from16 v2, v29

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v49

    const/16 v51, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v40

    move/from16 v2, v51

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v51

    move-object/from16 v0, v32

    move-object/from16 v1, v42

    move/from16 v2, v28

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v48

    move-object/from16 v0, v19

    move/from16 v1, v49

    move/from16 v2, v51

    invoke-interface {v0, v1, v13, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v51

    invoke-interface {v0, v1, v13, v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v49

    move/from16 v2, v48

    invoke-interface {v0, v1, v2, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v48

    move/from16 v2, v16

    invoke-interface {v0, v1, v2, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    :cond_c
    sget-object v48, Lcom/google/android/apps/gmm/map/l/g;->c:Lcom/google/android/apps/gmm/map/l/g;

    move-object/from16 v0, v48

    if-ne v10, v0, :cond_21

    if-eqz v26, :cond_21

    move-object/from16 v0, v21

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/l/h;->b:Lcom/google/android/apps/gmm/map/l/i;

    const/16 v48, -0x2

    move/from16 v0, v48

    invoke-static {v10, v0}, Lcom/google/android/apps/gmm/map/l/c;->a(Lcom/google/android/apps/gmm/map/l/i;I)I

    move-result v10

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v48, v0

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v48, v0

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v48, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v49, v0

    move/from16 v0, v49

    neg-float v0, v0

    move/from16 v49, v0

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v48, v0

    mul-float v48, v48, v27

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v48, v0

    mul-float v48, v48, v27

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v41

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v48, v0

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v48, v0

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v0, v5

    move/from16 v48, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v49, v0

    mul-float v49, v49, v48

    move/from16 v0, v49

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v49, v0

    mul-float v48, v48, v49

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v41

    move-object/from16 v1, v47

    move-object/from16 v2, v40

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v48, v0

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v48, v0

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v48, v0

    mul-float v48, v48, v6

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v48, v0

    mul-float v48, v48, v6

    move/from16 v0, v48

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v41

    move-object/from16 v1, v47

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v32

    move-object/from16 v1, v41

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2, v10}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v48

    const/16 v49, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v40

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2, v10}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v49

    move-object/from16 v0, v32

    move-object/from16 v1, v42

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2, v10}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v10

    move-object/from16 v0, v19

    move/from16 v1, v48

    move/from16 v2, v49

    invoke-interface {v0, v1, v2, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v49

    invoke-interface {v0, v1, v7, v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v48

    invoke-interface {v0, v1, v13, v10}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v0, v10, v13, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move v10, v7

    move/from16 v7, v16

    move/from16 v16, v17

    :cond_d
    :goto_c
    if-lez v11, :cond_e

    move-object/from16 v0, v19

    move/from16 v1, v50

    invoke-interface {v0, v15, v12, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v50

    invoke-interface {v0, v12, v14, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v50

    move/from16 v2, v16

    invoke-interface {v0, v15, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v50

    invoke-interface {v0, v1, v14, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    :cond_e
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    move v12, v13

    move v14, v7

    move v15, v10

    move-object v7, v8

    move-object v8, v9

    goto/16 :goto_7

    :cond_f
    sget-object v4, Lcom/google/android/apps/gmm/map/l/g;->a:Lcom/google/android/apps/gmm/map/l/g;

    goto/16 :goto_9

    :cond_10
    sget-object v4, Lcom/google/android/apps/gmm/map/l/g;->c:Lcom/google/android/apps/gmm/map/l/g;

    move-object v10, v4

    goto/16 :goto_a

    :pswitch_1
    const/4 v8, 0x0

    move-object/from16 v0, v22

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/c;->r:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v35

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v34

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v9

    int-to-float v4, v4

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v35

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v34

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v9

    int-to-float v4, v4

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v9, v9

    move-object/from16 v0, v45

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v4

    iget v9, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    neg-float v9, v9

    iput v9, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v9, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v9, v9

    iput v9, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    const/4 v4, 0x1

    move-object v9, v8

    move-object v8, v7

    goto/16 :goto_b

    :pswitch_2
    move-object/from16 v0, v22

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/l/c;->q:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v33

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v34

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v7

    int-to-float v4, v4

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v33

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v34

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v7

    int-to-float v4, v4

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    const/4 v7, 0x0

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v9, v9

    move-object/from16 v0, v45

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    const/4 v4, 0x1

    move-object v9, v8

    move-object v8, v7

    goto/16 :goto_b

    :pswitch_3
    move-object/from16 v0, v22

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/l/c;->q:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v33

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v34

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v7

    int-to-float v4, v4

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v33

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v34

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v7

    int-to-float v4, v4

    iput v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v22

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/l/c;->r:Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v35

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, v34

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v4, v9

    int-to-float v4, v4

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v35

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move-object/from16 v0, v34

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v9

    int-to-float v4, v4

    iput v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v43

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v43

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v44

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v7, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v44

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, v43

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v43

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v45

    iget v13, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    cmpl-float v4, v13, v4

    if-nez v4, :cond_11

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    cmpl-float v4, v4, v9

    if-nez v4, :cond_11

    const/4 v4, 0x1

    :goto_d
    if-eqz v4, :cond_12

    move-object/from16 v0, v43

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v43

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v9, v9

    move-object/from16 v0, v45

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    const/4 v4, 0x1

    move-object v9, v8

    move-object v8, v7

    goto/16 :goto_b

    :cond_11
    const/4 v4, 0x0

    goto :goto_d

    :cond_12
    invoke-virtual/range {v45 .. v45}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    const/4 v4, 0x0

    move-object v9, v8

    move-object v8, v7

    goto/16 :goto_b

    :cond_13
    iget v4, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v7, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    iget v7, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v10, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v7, v10

    sub-float/2addr v4, v7

    const/4 v7, 0x0

    cmpg-float v4, v4, v7

    if-gez v4, :cond_15

    const/4 v4, 0x1

    move v7, v4

    :goto_e
    if-eqz v7, :cond_16

    move v4, v5

    :goto_f
    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v10, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v10, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v16, v0

    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v10, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v10

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v16, v0

    iget v0, v10, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v17, v0

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float v10, v10, v17

    add-float v10, v10, v16

    neg-float v0, v10

    move/from16 v16, v0

    div-float v4, v4, v16

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, v46

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, v46

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v16, v0

    mul-float v16, v16, v4

    move/from16 v0, v16

    move-object/from16 v1, v46

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v16, v0

    mul-float v16, v16, v4

    move/from16 v0, v16

    move-object/from16 v1, v46

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v0, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v16

    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v17, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v51, v0

    mul-float v17, v17, v51

    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v51, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v16, v0

    mul-float v16, v16, v51

    add-float v16, v16, v17

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, v47

    iput v0, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v17

    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v51, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v52, v0

    mul-float v51, v51, v52

    move-object/from16 v0, v46

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v52, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v17, v0

    mul-float v17, v17, v52

    add-float v17, v17, v51

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v17

    iget v0, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v51, v0

    iget v0, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v52, v0

    mul-float v51, v51, v52

    iget v0, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v52, v0

    iget v0, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v53, v0

    mul-float v52, v52, v53

    add-float v51, v51, v52

    move/from16 v0, v51

    float-to-double v0, v0

    move-wide/from16 v52, v0

    invoke-static/range {v52 .. v53}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v52

    move-wide/from16 v0, v52

    double-to-float v0, v0

    move/from16 v51, v0

    div-float v16, v51, v16

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v51, v0

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v52, v0

    mul-float v51, v51, v52

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v52, v0

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move/from16 v53, v0

    mul-float v52, v52, v53

    add-float v51, v51, v52

    move/from16 v0, v51

    float-to-double v0, v0

    move-wide/from16 v52, v0

    invoke-static/range {v52 .. v53}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v52

    move-wide/from16 v0, v52

    double-to-float v0, v0

    move/from16 v51, v0

    div-float v17, v51, v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(FF)F

    move-result v16

    const/high16 v17, 0x3f800000    # 1.0f

    cmpg-float v17, v16, v17

    if-gez v17, :cond_20

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    move/from16 v0, v16

    invoke-static {v10, v0}, Ljava/lang/Math;->max(FF)F

    move-result v10

    mul-float/2addr v4, v10

    move v10, v4

    :goto_10
    if-eqz v31, :cond_14

    add-int/lit8 v4, v30, -0x1

    if-eq v11, v4, :cond_17

    :cond_14
    const/4 v4, 0x1

    move/from16 v17, v4

    :goto_11
    if-eqz v7, :cond_18

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v4, v6

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v4

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v4, v10

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v4

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v37

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v7, v7

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v4

    neg-float v7, v6

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v10, v7

    iput v10, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v7, v10

    iput v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v38

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v7, v7

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v4

    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v6

    iput v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v7, v6

    iput v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v28

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v51

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v28

    move/from16 v3, v49

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v52

    const/4 v4, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    move/from16 v2, v48

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v16

    const/4 v4, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    move/from16 v2, v49

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v10

    move-object/from16 v0, v32

    move-object/from16 v1, v38

    move/from16 v2, v28

    move/from16 v3, v49

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v7

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    move/from16 v2, v28

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v4

    if-eqz v17, :cond_d

    move-object/from16 v0, v19

    move/from16 v1, v52

    invoke-interface {v0, v7, v13, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v50

    move/from16 v2, v51

    invoke-interface {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_c

    .line 229
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 234
    :cond_15
    const/4 v4, 0x0

    move v7, v4

    goto/16 :goto_e

    :cond_16
    move v4, v6

    goto/16 :goto_f

    :cond_17
    const/4 v4, 0x0

    move/from16 v17, v4

    goto/16 :goto_11

    :cond_18
    :try_start_2
    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v4, v10

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v10

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v37

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v45

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v4, v5

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v4

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v4, v7

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v47

    move-object/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v9, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v7, v7

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v4

    neg-float v7, v5

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v10, v7

    iput v10, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v7, v10

    iput v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v38

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v8, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v47

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    neg-float v7, v7

    move-object/from16 v0, v47

    iput v7, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, v47

    iput v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    invoke-virtual/range {v47 .. v47}, Lcom/google/android/apps/gmm/map/b/a/ay;->c()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v4

    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v7, v5

    iput v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v7, v5

    iput v7, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object/from16 v0, v34

    move-object/from16 v1, v39

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v48

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v51

    const/4 v4, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v49

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v52

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    move/from16 v2, v28

    move/from16 v3, v48

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v4

    move-object/from16 v0, v32

    move-object/from16 v1, v37

    move/from16 v2, v28

    move/from16 v3, v49

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v7

    const/4 v10, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v38

    move/from16 v2, v48

    invoke-virtual {v0, v1, v10, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v16

    const/4 v10, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v39

    move/from16 v2, v49

    invoke-virtual {v0, v1, v10, v2}, Lcom/google/android/apps/gmm/map/l/f;->a(Lcom/google/android/apps/gmm/map/b/a/y;II)I

    move-result v10

    if-eqz v17, :cond_d

    move-object/from16 v0, v19

    move/from16 v1, v16

    move/from16 v2, v50

    move/from16 v3, v51

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    move-object/from16 v0, v19

    move/from16 v1, v52

    invoke-interface {v0, v13, v10, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(III)V

    goto/16 :goto_c

    :cond_19
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v4

    if-lez v4, :cond_1d

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->o:F

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v4

    .line 235
    :goto_12
    if-nez v4, :cond_1e

    .line 236
    const-string v4, "PolylineProcesser"

    const-string v5, "Null VertexData"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    :cond_1a
    :goto_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-boolean v6, v4, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v6, :cond_1b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1b
    iget-object v6, v4, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v7, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v7, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v6, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget-boolean v6, v4, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v6, :cond_1c

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1c
    iput v5, v4, Lcom/google/android/apps/gmm/map/t/q;->d:F

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    goto/16 :goto_0

    .line 234
    :cond_1d
    const/4 v4, 0x0

    goto :goto_12

    .line 238
    :cond_1e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 239
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->j:Z

    if-nez v4, :cond_1a

    .line 240
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->q:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v6, Lcom/google/android/apps/gmm/v/af;

    const/4 v7, 0x1

    invoke-direct {v6, v5, v7}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 241
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_13

    :cond_1f
    move v10, v9

    move v9, v4

    goto/16 :goto_8

    :cond_20
    move v10, v4

    goto/16 :goto_10

    :cond_21
    move v10, v7

    move/from16 v7, v16

    move/from16 v16, v17

    goto/16 :goto_c

    .line 234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/internal/b/a;)V
    .locals 1

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    monitor-exit p0

    return-void

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/q;Lcom/google/android/apps/gmm/v/ci;)V
    .locals 4

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->q:Lcom/google/android/apps/gmm/v/ad;

    .line 197
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    const-string v1, "Polyline"

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    new-instance v1, Lcom/google/android/apps/gmm/v/m;

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 202
    new-instance v0, Lcom/google/android/apps/gmm/v/cd;

    const/4 v1, 0x2

    .line 203
    iget v2, p3, Lcom/google/android/apps/gmm/v/ci;->t:I

    const v3, 0x84c0

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/v/cd;-><init>(II)V

    .line 204
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/cd;->a(I)V

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 208
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v1, 0x0

    const/16 v2, 0x11

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;-><init>(IIZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->k:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    monitor-exit p0

    return-void

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->l:Z

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    monitor-exit p0

    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    monitor-exit p0

    return-void

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->m:Z

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    monitor-exit p0

    return-void

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Z)V
    .locals 1

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->n:Z

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    monitor-exit p0

    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
