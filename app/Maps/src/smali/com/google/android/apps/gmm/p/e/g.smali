.class Lcom/google/android/apps/gmm/p/e/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/p/d/d;


# instance fields
.field final b:Lcom/google/android/apps/gmm/p/d/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/b/a/y;

.field final d:Lcom/google/android/apps/gmm/p/d/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final e:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final f:Lcom/google/android/apps/gmm/p/d/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final g:Lcom/google/android/apps/gmm/p/d/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/p/d/d;

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/p/d/d;-><init>(DD)V

    sput-object v0, Lcom/google/android/apps/gmm/p/e/g;->a:Lcom/google/android/apps/gmm/p/d/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Boolean;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/p/d/d;Lcom/google/android/apps/gmm/p/d/d;Lcom/google/android/apps/gmm/p/d/a;Lcom/google/android/apps/gmm/p/d/d;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/g;->e:Ljava/lang/Boolean;

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    .line 90
    iput-object p2, p0, Lcom/google/android/apps/gmm/p/e/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 91
    iput-object p4, p0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    .line 92
    iput-object p6, p0, Lcom/google/android/apps/gmm/p/e/g;->f:Lcom/google/android/apps/gmm/p/d/d;

    .line 93
    iput-object p5, p0, Lcom/google/android/apps/gmm/p/e/g;->g:Lcom/google/android/apps/gmm/p/d/a;

    .line 94
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/p/e/g;Lcom/google/android/apps/gmm/shared/net/a/l;)Lcom/google/android/apps/gmm/p/e/g;
    .locals 22

    .prologue
    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    .line 113
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 114
    new-instance v5, Lcom/google/android/apps/gmm/p/d/d;

    const-wide/16 v6, 0x0

    .line 115
    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v10, v10, Lcom/google/r/b/a/ou;->i:I

    int-to-double v10, v10

    add-double/2addr v10, v2

    invoke-direct {v5, v6, v7, v10, v11}, Lcom/google/android/apps/gmm/p/d/d;-><init>(DD)V

    .line 118
    const/4 v6, 0x0

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 122
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v6

    float-to-double v8, v6

    .line 123
    if-eqz p1, :cond_7

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v6, :cond_6

    const/4 v6, 0x1

    :goto_1
    if-eqz v6, :cond_7

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    .line 124
    iget-wide v6, v6, Lcom/google/android/apps/gmm/p/d/d;->b:D

    .line 126
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v11

    float-to-double v12, v11

    double-to-float v6, v6

    double-to-float v7, v8

    .line 127
    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x43340000    # 180.0f

    cmpl-float v7, v6, v7

    if-lez v7, :cond_0

    const/high16 v7, 0x43b40000    # 360.0f

    sub-float v6, v7, v6

    :cond_0
    float-to-double v6, v6

    .line 128
    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v11, v11, Lcom/google/r/b/a/ou;->j:I

    int-to-double v14, v11

    .line 126
    const-wide v16, 0x4046800000000000L    # 45.0

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    neg-double v2, v2

    const-wide/high16 v20, 0x403e000000000000L    # 30.0

    div-double v2, v2, v20

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    sub-double v2, v18, v2

    mul-double v16, v16, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    div-double/2addr v6, v2

    const-wide/16 v2, 0x0

    if-eqz v10, :cond_1

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    neg-double v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    mul-double/2addr v2, v10

    :cond_1
    add-double v6, v6, v16

    add-double/2addr v2, v6

    add-double/2addr v2, v14

    .line 130
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    cmpg-double v6, v2, v6

    if-gez v6, :cond_8

    .line 132
    :goto_3
    new-instance v6, Lcom/google/android/apps/gmm/p/d/d;

    invoke-direct {v6, v8, v9, v2, v3}, Lcom/google/android/apps/gmm/p/d/d;-><init>(DD)V

    .line 147
    :cond_2
    :goto_4
    const/4 v7, 0x0

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v2

    float-to-double v8, v2

    .line 155
    if-eqz p1, :cond_c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/g;->g:Lcom/google/android/apps/gmm/p/d/a;

    if-eqz v2, :cond_c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/g;->g:Lcom/google/android/apps/gmm/p/d/a;

    .line 156
    invoke-interface {v2}, Lcom/google/android/apps/gmm/p/d/a;->a()D

    move-result-wide v2

    cmpl-double v2, v2, v8

    if-nez v2, :cond_c

    const/4 v2, 0x1

    .line 161
    :goto_5
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v3, v3, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v3, v3, 0x4000

    const/16 v10, 0x4000

    if-ne v3, v10, :cond_d

    const/4 v3, 0x1

    :goto_6
    if-eqz v3, :cond_e

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v3, v3, Lcom/google/r/b/a/ou;->q:I

    invoke-static {v3}, Lcom/google/r/b/a/ox;->a(I)Lcom/google/r/b/a/ox;

    move-result-object v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/google/r/b/a/ox;->a:Lcom/google/r/b/a/ox;

    .line 162
    :cond_3
    :goto_7
    sget-object v10, Lcom/google/android/apps/gmm/p/e/h;->a:[I

    invoke-virtual {v3}, Lcom/google/r/b/a/ox;->ordinal()I

    move-result v3

    aget v3, v10, v3

    packed-switch v3, :pswitch_data_0

    .line 187
    :cond_4
    :goto_8
    new-instance v2, Lcom/google/android/apps/gmm/p/e/g;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/p/e/g;-><init>(Ljava/lang/Boolean;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/p/d/d;Lcom/google/android/apps/gmm/p/d/d;Lcom/google/android/apps/gmm/p/d/a;Lcom/google/android/apps/gmm/p/d/d;)V

    return-object v2

    .line 110
    :cond_5
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->u:I

    int-to-float v2, v2

    const v3, 0x3f2aacda    # 0.6667f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    goto/16 :goto_0

    .line 123
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_7
    move-wide v6, v8

    .line 124
    goto/16 :goto_2

    .line 130
    :cond_8
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    goto :goto_3

    .line 133
    :cond_9
    if-eqz p1, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_2

    .line 137
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    iget-wide v10, v2, Lcom/google/android/apps/gmm/p/d/d;->b:D

    .line 138
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v2, v8

    .line 139
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    .line 140
    iget-wide v6, v6, Lcom/google/android/apps/gmm/p/d/d;->c:D

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v8

    add-double/2addr v2, v6

    .line 142
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    cmpg-double v6, v2, v6

    if-gez v6, :cond_b

    .line 144
    :goto_a
    new-instance v6, Lcom/google/android/apps/gmm/p/d/d;

    invoke-direct {v6, v10, v11, v2, v3}, Lcom/google/android/apps/gmm/p/d/d;-><init>(DD)V

    goto/16 :goto_4

    .line 133
    :cond_a
    const/4 v2, 0x0

    goto :goto_9

    .line 142
    :cond_b
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    goto :goto_a

    .line 156
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 161
    :cond_d
    const/4 v3, 0x0

    goto :goto_6

    :cond_e
    sget-object v3, Lcom/google/r/b/a/ox;->a:Lcom/google/r/b/a/ox;

    goto :goto_7

    .line 165
    :pswitch_0
    const-wide/16 v10, 0x0

    cmpl-double v3, v8, v10

    if-nez v3, :cond_f

    .line 169
    new-instance v7, Lcom/google/android/apps/gmm/p/d/b;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v7, v8, v9, v2, v3}, Lcom/google/android/apps/gmm/p/d/b;-><init>(DD)V

    goto :goto_8

    .line 170
    :cond_f
    if-nez v2, :cond_4

    .line 171
    new-instance v7, Lcom/google/android/apps/gmm/p/d/c;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v7, v8, v9, v2, v3}, Lcom/google/android/apps/gmm/p/d/c;-><init>(DD)V

    goto :goto_8

    .line 176
    :pswitch_1
    const-wide/16 v10, 0x0

    cmpl-double v3, v8, v10

    if-eqz v3, :cond_10

    if-nez v2, :cond_4

    .line 177
    :cond_10
    new-instance v7, Lcom/google/android/apps/gmm/p/d/b;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v7, v8, v9, v2, v3}, Lcom/google/android/apps/gmm/p/d/b;-><init>(DD)V

    goto/16 :goto_8

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/p/d/d;)Lcom/google/android/apps/gmm/p/e/g;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 197
    new-instance v0, Lcom/google/android/apps/gmm/p/e/g;

    const/4 v1, 0x0

    .line 198
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/p/e/g;-><init>(Ljava/lang/Boolean;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/p/d/d;Lcom/google/android/apps/gmm/p/d/d;Lcom/google/android/apps/gmm/p/d/a;Lcom/google/android/apps/gmm/p/d/d;)V

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/p/e/a/g;)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    .line 281
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v4

    mul-double/2addr v2, v6

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    .line 284
    const-wide/16 v2, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/p/e/a/g;->O_()D

    move-result-wide v4

    div-double/2addr v4, v6

    sub-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 285
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/p/d/d;->a(D)D

    move-result-wide v0

    .line 287
    :goto_1
    return-wide v0

    .line 279
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 287
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 389
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "positionMean"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 390
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "distanceFromPosition"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->b:Lcom/google/android/apps/gmm/p/d/d;

    .line 391
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "bearing"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->d:Lcom/google/android/apps/gmm/p/d/d;

    .line 392
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "gpsSpeed"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->g:Lcom/google/android/apps/gmm/p/d/a;

    .line 393
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "tunnelSpeed"

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/g;->f:Lcom/google/android/apps/gmm/p/d/d;

    .line 394
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
