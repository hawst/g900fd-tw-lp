.class public Lcom/google/android/apps/gmm/cardui/s;
.super Lcom/google/android/apps/gmm/cardui/c;
.source "PG"


# instance fields
.field public i:Lcom/google/android/libraries/curvular/ag;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/util/b/h;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/android/apps/gmm/util/b/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/cardui/b/a;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/cardui/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/cardui/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/cardui/b/a;)V

    .line 35
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/c;->b()V

    new-instance v0, Lcom/google/android/apps/gmm/util/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/cardui/f;Ljava/util/List;)Lcom/google/android/apps/gmm/cardui/e/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/cardui/f;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;)",
            "Lcom/google/android/apps/gmm/cardui/e/c;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/util/b/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/s;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 73
    iget-object v2, p1, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    invoke-direct {v0, v1, v2, p2, p0}, Lcom/google/android/apps/gmm/util/b/s;-><init>(Landroid/content/Context;Lcom/google/o/h/a/br;Ljava/util/List;Lcom/google/android/apps/gmm/util/b/a;)V

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/i;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v1, Lcom/google/android/apps/gmm/cardui/t;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/cardui/t;-><init>(Lcom/google/android/apps/gmm/util/b/s;)V

    return-object v1
.end method

.method protected final a()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 81
    :cond_0
    return-void
.end method

.method protected final a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/i;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 48
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/c;->b()V

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/util/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    .line 42
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 88
    :cond_0
    return-void
.end method
