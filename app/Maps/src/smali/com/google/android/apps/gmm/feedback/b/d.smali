.class public Lcom/google/android/apps/gmm/feedback/b/d;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/feedback/d/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/libraries/curvular/bi;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/cs;
    .locals 4

    .prologue
    .line 107
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/gmm/e;->I:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/gmm/e;->J:I

    .line 108
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/gmm/e;->I:I

    .line 109
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/gmm/e;->J:I

    .line 110
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->an:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 111
    sget-object v2, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, -0x1

    .line 112
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/apps/gmm/m;->g:I

    .line 113
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 114
    sget-object v2, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 115
    sget-object v2, Lcom/google/android/libraries/curvular/g;->r:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, p2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 116
    invoke-static {p3}, Lcom/google/android/libraries/curvular/t;->b(Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    .line 107
    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.RadioButton"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 14

    .prologue
    .line 48
    const/4 v0, 0x7

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    const/4 v2, -0x1

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    const/4 v2, -0x1

    .line 51
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    .line 52
    sget v2, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    sget v2, Lcom/google/android/apps/gmm/l;->hW:I

    .line 55
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 54
    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/base/f/o;->a(Ljava/lang/CharSequence;Ljava/lang/Boolean;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x5

    const/4 v0, 0x3

    new-array v3, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 58
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 59
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    const/4 v0, 0x4

    new-array v5, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 61
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x1

    const/4 v6, -0x1

    .line 62
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    const/4 v6, -0x2

    .line 63
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x3

    .line 64
    const/4 v0, 0x5

    new-array v7, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v8, 0x0

    sget v0, Lcom/google/android/apps/gmm/l;->hU:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v9

    sget v0, Lcom/google/android/apps/gmm/g;->bv:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v11, Lcom/google/android/apps/gmm/feedback/c/b;->b:Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;)Ljava/lang/Boolean;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v12, Lcom/google/android/apps/gmm/feedback/c/b;->b:Lcom/google/android/apps/gmm/feedback/c/b;

    const/4 v13, 0x1

    invoke-static {v13}, Lcom/google/android/libraries/curvular/b/e;->c(I)Ljava/lang/Boolean;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    invoke-static {v9, v10, v11, v0}, Lcom/google/android/apps/gmm/feedback/b/d;->a(Lcom/google/android/libraries/curvular/bi;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v8, 0x1

    sget v0, Lcom/google/android/apps/gmm/l;->hT:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v9

    sget v0, Lcom/google/android/apps/gmm/g;->bu:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v11, Lcom/google/android/apps/gmm/feedback/c/b;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;)Ljava/lang/Boolean;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v12, Lcom/google/android/apps/gmm/feedback/c/b;->a:Lcom/google/android/apps/gmm/feedback/c/b;

    const/4 v13, 0x1

    invoke-static {v13}, Lcom/google/android/libraries/curvular/b/e;->c(I)Ljava/lang/Boolean;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    invoke-static {v9, v10, v11, v0}, Lcom/google/android/apps/gmm/feedback/b/d;->a(Lcom/google/android/libraries/curvular/bi;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v8, 0x2

    sget v0, Lcom/google/android/apps/gmm/l;->hX:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v9

    sget v0, Lcom/google/android/apps/gmm/g;->bw:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v11, Lcom/google/android/apps/gmm/feedback/c/b;->c:Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;)Ljava/lang/Boolean;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v12, Lcom/google/android/apps/gmm/feedback/c/b;->c:Lcom/google/android/apps/gmm/feedback/c/b;

    const/4 v13, 0x1

    invoke-static {v13}, Lcom/google/android/libraries/curvular/b/e;->c(I)Ljava/lang/Boolean;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    invoke-static {v9, v10, v11, v0}, Lcom/google/android/apps/gmm/feedback/b/d;->a(Lcom/google/android/libraries/curvular/bi;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v8, 0x3

    sget v0, Lcom/google/android/apps/gmm/l;->hR:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v9

    sget v0, Lcom/google/android/apps/gmm/g;->bx:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v11, Lcom/google/android/apps/gmm/feedback/c/b;->d:Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;)Ljava/lang/Boolean;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v12, Lcom/google/android/apps/gmm/feedback/c/b;->d:Lcom/google/android/apps/gmm/feedback/c/b;

    const/4 v13, 0x1

    invoke-static {v13}, Lcom/google/android/libraries/curvular/b/e;->c(I)Ljava/lang/Boolean;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    invoke-static {v9, v10, v11, v0}, Lcom/google/android/apps/gmm/feedback/b/d;->a(Lcom/google/android/libraries/curvular/bi;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v8, 0x4

    sget v0, Lcom/google/android/apps/gmm/l;->hV:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v9

    sget v0, Lcom/google/android/apps/gmm/g;->by:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v11, Lcom/google/android/apps/gmm/feedback/c/b;->g:Lcom/google/android/apps/gmm/feedback/c/b;

    invoke-interface {v0, v11}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;)Ljava/lang/Boolean;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    sget-object v12, Lcom/google/android/apps/gmm/feedback/c/b;->g:Lcom/google/android/apps/gmm/feedback/c/b;

    const/4 v13, 0x1

    invoke-static {v13}, Lcom/google/android/libraries/curvular/b/e;->c(I)Ljava/lang/Boolean;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Lcom/google/android/apps/gmm/feedback/d/e;->a(Lcom/google/android/apps/gmm/feedback/c/b;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    invoke-static {v9, v10, v11, v0}, Lcom/google/android/apps/gmm/feedback/b/d;->a(Lcom/google/android/libraries/curvular/bi;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v7, v8

    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.RadioGroup"

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v5, v6

    .line 60
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.LinearLayout"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v3, v4

    .line 57
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.ScrollView"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/android/apps/gmm/base/f/ad;

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/feedback/d/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/feedback/d/e;->a()Lcom/google/android/apps/gmm/base/l/a/n;

    move-result-object v0

    .line 66
    new-instance v4, Lcom/google/android/libraries/curvular/ao;

    const/4 v5, 0x0

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-direct {v4, v3, v0}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v4, v1, v2

    .line 48
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method
