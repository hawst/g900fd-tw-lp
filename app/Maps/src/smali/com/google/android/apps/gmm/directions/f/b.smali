.class public Lcom/google/android/apps/gmm/directions/f/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/g/a/hm;

.field public b:Lcom/google/r/b/a/afz;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/maps/a/a;

.field public e:Lcom/google/o/b/a/v;

.field public f:Lcom/google/maps/g/a/al;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/maps/g/hy;

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 279
    const/4 v0, 0x2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    .line 288
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/directions/f/a;)V
    .locals 2

    .prologue
    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 279
    const/4 v0, 0x2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    .line 291
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    .line 292
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->b:Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 294
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->f:Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    .line 295
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    .line 298
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/b;->h:Lcom/google/maps/g/hy;

    .line 301
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/directions/f/a;
    .locals 10

    .prologue
    .line 358
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-static {v3}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/f/b;->f:Lcom/google/maps/g/a/al;

    iget-object v7, p0, Lcom/google/android/apps/gmm/directions/f/b;->g:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/gmm/directions/f/b;->h:Lcom/google/maps/g/hy;

    iget-boolean v9, p0, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/directions/f/a;-><init>(Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;Lcom/google/b/c/cv;Lcom/google/maps/a/a;Lcom/google/o/b/a/v;Lcom/google/maps/g/a/al;Ljava/lang/String;Lcom/google/maps/g/hy;Z)V

    return-object v0
.end method
