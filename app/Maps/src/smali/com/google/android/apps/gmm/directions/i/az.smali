.class public Lcom/google/android/apps/gmm/directions/i/az;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/r;


# instance fields
.field private final a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/views/c/b;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/az;->a:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/az;->b:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 33
    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->c:Lcom/google/android/libraries/curvular/a;

    .line 34
    if-nez p4, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->d:Lcom/google/android/libraries/curvular/a;

    .line 35
    if-nez p5, :cond_2

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->e:Lcom/google/android/libraries/curvular/a;

    .line 36
    if-nez p6, :cond_3

    :goto_3
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/i/az;->f:Lcom/google/android/libraries/curvular/a;

    .line 37
    iput-object p7, p0, Lcom/google/android/apps/gmm/directions/i/az;->g:Ljava/lang/String;

    .line 38
    iput-object p8, p0, Lcom/google/android/apps/gmm/directions/i/az;->h:Ljava/lang/String;

    .line 39
    return-void

    .line 33
    :cond_0
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    goto :goto_1

    .line 35
    :cond_2
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    goto :goto_2

    .line 36
    :cond_3
    new-instance v1, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    goto :goto_3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->b:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final bridge synthetic c()Lcom/google/android/libraries/curvular/aq;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->d:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final bridge synthetic d()Lcom/google/android/libraries/curvular/aq;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->e:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final bridge synthetic e()Lcom/google/android/libraries/curvular/aq;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/az;->f:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method
