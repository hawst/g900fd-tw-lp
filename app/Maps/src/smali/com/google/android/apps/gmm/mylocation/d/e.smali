.class public Lcom/google/android/apps/gmm/mylocation/d/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/v/ad;

.field public final b:Landroid/content/res/Resources;

.field private final c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->a:Lcom/google/android/apps/gmm/v/ad;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    .line 36
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/v/ar;Ljava/lang/String;Lcom/google/android/apps/gmm/mylocation/d/b;)Lcom/google/android/apps/gmm/mylocation/d/a;
    .locals 3

    .prologue
    .line 276
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v0, v1, p0, p2}, Lcom/google/android/apps/gmm/mylocation/d/a;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ar;Lcom/google/android/apps/gmm/mylocation/d/b;)V

    .line 278
    const/4 v1, 0x4

    invoke-static {v0, p1, v1, p0}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(Lcom/google/android/apps/gmm/mylocation/d/d;Ljava/lang/String;ILcom/google/android/apps/gmm/v/ar;)V

    .line 279
    sget-object v1, Lcom/google/android/apps/gmm/map/t/r;->b:Lcom/google/android/apps/gmm/map/t/r;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 281
    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/mylocation/d/d;Ljava/lang/String;ILcom/google/android/apps/gmm/v/ar;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, -0x40800000    # -1.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 58
    iget v0, p3, Lcom/google/android/apps/gmm/v/ar;->e:I

    int-to-float v0, v0

    mul-float/2addr v0, v5

    div-float v0, v6, v0

    iget v1, p3, Lcom/google/android/apps/gmm/v/ar;->e:I

    int-to-float v1, v1

    mul-float/2addr v1, v5

    div-float v1, v6, v1

    iget v2, p3, Lcom/google/android/apps/gmm/v/ar;->c:I

    int-to-float v2, v2

    mul-float/2addr v2, v5

    sub-float/2addr v2, v6

    mul-float/2addr v1, v2

    iget v2, p3, Lcom/google/android/apps/gmm/v/ar;->f:I

    int-to-float v2, v2

    mul-float/2addr v2, v5

    div-float v2, v6, v2

    iget v3, p3, Lcom/google/android/apps/gmm/v/ar;->f:I

    int-to-float v3, v3

    mul-float/2addr v3, v5

    div-float v3, v6, v3

    iget v4, p3, Lcom/google/android/apps/gmm/v/ar;->d:I

    int-to-float v4, v4

    mul-float/2addr v4, v5

    sub-float/2addr v4, v6

    mul-float/2addr v3, v4

    const/16 v4, 0x14

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v7, v4, v5

    aput v6, v4, v9

    const/4 v5, 0x2

    aput v8, v4, v5

    const/4 v5, 0x3

    aput v0, v4, v5

    const/4 v5, 0x4

    aput v2, v4, v5

    const/4 v5, 0x5

    aput v7, v4, v5

    const/4 v5, 0x6

    aput v7, v4, v5

    const/4 v5, 0x7

    aput v8, v4, v5

    const/16 v5, 0x8

    aput v0, v4, v5

    const/16 v0, 0x9

    aput v3, v4, v0

    const/16 v0, 0xa

    aput v6, v4, v0

    const/16 v0, 0xb

    aput v6, v4, v0

    const/16 v0, 0xc

    aput v8, v4, v0

    const/16 v0, 0xd

    aput v1, v4, v0

    const/16 v0, 0xe

    aput v2, v4, v0

    const/16 v0, 0xf

    aput v6, v4, v0

    const/16 v0, 0x10

    aput v7, v4, v0

    const/16 v0, 0x11

    aput v8, v4, v0

    const/16 v0, 0x12

    aput v1, v4, v0

    const/16 v0, 0x13

    aput v3, v4, v0

    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    const/16 v1, 0x11

    const/4 v2, 0x5

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/v/ci;

    invoke-direct {v0, p3, p2, v9}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;IZ)V

    .line 63
    const/16 v1, 0x2703

    const/16 v2, 0x2601

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput v1, v0, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v2, v0, Lcom/google/android/apps/gmm/v/ci;->i:I

    iput-boolean v9, v0, Lcom/google/android/apps/gmm/v/ci;->j:Z

    .line 65
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/v/cd;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/v/cd;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/v/m;

    const/16 v1, 0x303

    invoke-direct {v0, v9, v1}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 68
    return-void
.end method


# virtual methods
.method public a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;
    .locals 4

    .prologue
    .line 194
    new-instance v0, Lcom/google/android/apps/gmm/map/t/q;

    invoke-direct {v0, p4}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    .line 195
    iput-object p3, v0, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->a:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/co;)V

    .line 197
    new-instance v1, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/v/x;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 198
    new-instance v1, Lcom/google/android/apps/gmm/v/m;

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    .line 200
    return-object v0
.end method

.method final a(Z)Lcom/google/android/apps/gmm/map/t/q;
    .locals 4

    .prologue
    .line 235
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/gmm/d;->F:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const/4 v1, 0x4

    const-string v2, "Accuracy circle fill"

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/d;->A:I

    goto :goto_0
.end method

.method public a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;
    .locals 6

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/v/ar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/v/ao;ZZ)V

    .line 47
    new-instance v1, Lcom/google/android/apps/gmm/mylocation/d/d;

    iget v2, v0, Lcom/google/android/apps/gmm/v/ar;->c:I

    invoke-direct {v1, p4, v2}, Lcom/google/android/apps/gmm/mylocation/d/d;-><init>(Lcom/google/android/apps/gmm/map/t/k;I)V

    .line 48
    invoke-static {v1, p2, p3, v0}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(Lcom/google/android/apps/gmm/mylocation/d/d;Ljava/lang/String;ILcom/google/android/apps/gmm/v/ar;)V

    .line 49
    return-object v1
.end method

.method final b(Z)Lcom/google/android/apps/gmm/map/t/q;
    .locals 4

    .prologue
    .line 243
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/e;->b:Landroid/content/res/Resources;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/gmm/d;->G:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const/4 v1, 0x3

    const-string v2, "Accuracy circle outline"

    sget-object v3, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(IILjava/lang/String;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/d;->B:I

    goto :goto_0
.end method
