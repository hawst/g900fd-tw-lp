.class public Lcom/google/android/apps/gmm/search/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/didyoumean/a/a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/x/o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/g/c;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/q;->a:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->k()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/q;->b:Lcom/google/android/apps/gmm/x/o;

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    .line 38
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 41
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 56
    if-eqz p1, :cond_0

    .line 57
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/q;->b:Lcom/google/android/apps/gmm/x/o;

    const/4 v2, 0x1

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/x/o;Z)V

    .line 63
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    .line 48
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/q;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 51
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
