.class public Lcom/google/android/apps/gmm/navigation/g/j;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/maps/g/a/z;

.field final b:Lcom/google/android/apps/gmm/map/r/a/ae;

.field final c:I

.field final d:I

.field final e:[Lcom/google/maps/g/a/fw;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final f:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/maps/g/a/is;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/g/j;->g:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/navigation/g/k;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->a:Lcom/google/maps/g/a/z;

    const-string v1, "status"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/maps/g/a/z;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->a:Lcom/google/maps/g/a/z;

    .line 77
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    const-string v1, "routes"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 78
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->c:I

    .line 79
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->d:I

    .line 80
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->e:[Lcom/google/maps/g/a/fw;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->e:[Lcom/google/maps/g/a/fw;

    .line 81
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->f:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->f:Lcom/google/n/f;

    .line 82
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/k;->g:Lcom/google/maps/g/a/is;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/j;->h:Lcom/google/maps/g/a/is;

    .line 83
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/z;)Lcom/google/android/apps/gmm/navigation/g/j;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/k;-><init>()V

    .line 141
    iput-object p0, v0, Lcom/google/android/apps/gmm/navigation/g/k;->a:Lcom/google/maps/g/a/z;

    .line 142
    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/j;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/g/j;-><init>(Lcom/google/android/apps/gmm/navigation/g/k;)V

    return-object v1
.end method

.method public static a(Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/map/r/a/e;Landroid/content/Context;ZLcom/google/maps/g/wq;)Lcom/google/android/apps/gmm/navigation/g/j;
    .locals 13

    .prologue
    .line 148
    new-instance v10, Lcom/google/android/apps/gmm/navigation/g/k;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/navigation/g/k;-><init>()V

    .line 149
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/r/b/a/afj;

    if-nez p1, :cond_1

    sget-object v0, Lcom/google/maps/g/a/z;->e:Lcom/google/maps/g/a/z;

    iput-object v0, v10, Lcom/google/android/apps/gmm/navigation/g/k;->a:Lcom/google/maps/g/a/z;

    .line 155
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/j;

    invoke-direct {v0, v10}, Lcom/google/android/apps/gmm/navigation/g/j;-><init>(Lcom/google/android/apps/gmm/navigation/g/k;)V

    return-object v0

    .line 149
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_2
    iput-object v0, v10, Lcom/google/android/apps/gmm/navigation/g/k;->a:Lcom/google/maps/g/a/z;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_3
    sget-object v1, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-eq v0, v1, :cond_6

    sget-object v1, Lcom/google/android/apps/gmm/navigation/g/j;->g:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Response status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->c()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/n/f;->a([B)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, v10, Lcom/google/android/apps/gmm/navigation/g/k;->f:Lcom/google/n/f;

    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/maps/g/a/fw;

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fw;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    :cond_9
    iput-object v2, v10, Lcom/google/android/apps/gmm/navigation/g/k;->e:[Lcom/google/maps/g/a/fw;

    :cond_a
    iget-object v0, v8, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, v8, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    invoke-static {v0, p2}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    const/4 v0, 0x1

    iget-object v1, v8, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    invoke-static {v0, p2}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v5

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_b

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    invoke-static {v1, v4, v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/jm;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    invoke-static {v1, v5, v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/jm;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v5

    :cond_b
    const/4 v0, -0x1

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v1, v1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_f

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_c

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->d:I

    :cond_c
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v1, v1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_10

    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_d

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v1, v1, Lcom/google/r/b/a/afb;->e:I

    iput v1, v10, Lcom/google/android/apps/gmm/navigation/g/k;->c:I

    :cond_d
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v1, v1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_11

    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_e

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v1, v1, Lcom/google/r/b/a/afb;->f:I

    iput v1, v10, Lcom/google/android/apps/gmm/navigation/g/k;->d:I

    :cond_e
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v2, v1, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    if-nez v2, :cond_12

    invoke-static {}, Lcom/google/maps/g/a/is;->d()Lcom/google/maps/g/a/is;

    move-result-object v1

    :goto_7
    iput-object v1, v10, Lcom/google/android/apps/gmm/navigation/g/k;->g:Lcom/google/maps/g/a/is;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v1, v1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v9, v0

    :goto_8
    if-ge v1, v11, :cond_16

    iget-object v0, v8, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v7

    check-cast v7, Lcom/google/r/b/a/afz;

    move-object v0, p1

    move-object v2, p2

    move-object/from16 v3, p4

    move/from16 v6, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/e;ILandroid/content/Context;Lcom/google/maps/g/wq;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZLcom/google/r/b/a/afz;)Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    if-eqz v0, :cond_13

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v9

    :goto_9
    add-int/lit8 v1, v1, 0x1

    move v9, v0

    goto :goto_8

    :cond_f
    const/4 v1, 0x0

    goto :goto_4

    :cond_10
    const/4 v1, 0x0

    goto :goto_5

    :cond_11
    const/4 v1, 0x0

    goto :goto_6

    :cond_12
    iget-object v1, v1, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    goto :goto_7

    :cond_13
    if-ge v1, v9, :cond_14

    add-int/lit8 v0, v9, -0x1

    goto :goto_9

    :cond_14
    if-ne v1, v9, :cond_15

    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/j;->g:Ljava/lang/String;

    const-string v2, "Selected trip returned from the server is not renderable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_15
    move v0, v9

    goto :goto_9

    :cond_16
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-interface {v12, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-static {v9, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    iput-object v0, v10, Lcom/google/android/apps/gmm/navigation/g/k;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    goto/16 :goto_0
.end method
