.class public Lcom/google/android/apps/gmm/suggest/g/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/suggest/d;

.field private final b:Lcom/google/android/apps/gmm/suggest/e/d;

.field private final c:Ljava/lang/CharSequence;

.field private final d:Lcom/google/android/libraries/curvular/aw;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/suggest/d;Lcom/google/android/apps/gmm/suggest/e/d;I)V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/gmm/suggest/g/d;->a:Lcom/google/android/apps/gmm/suggest/d;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/g/d;->b:Lcom/google/android/apps/gmm/suggest/e/d;

    .line 36
    iput p4, p0, Lcom/google/android/apps/gmm/suggest/g/d;->e:I

    .line 38
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 38
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v2, p3, Lcom/google/android/apps/gmm/suggest/e/d;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/google/android/apps/gmm/suggest/e/d;->f:Lcom/google/b/c/cv;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/suggest/e/d;->a(Landroid/text/SpannableStringBuilder;Lcom/google/b/c/cv;Landroid/text/style/ForegroundColorSpan;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/d;->c:Ljava/lang/CharSequence;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/d;->b:Lcom/google/android/apps/gmm/suggest/e/d;

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/e/d;->c:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/d;->b(I)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/d;->d:Lcom/google/android/libraries/curvular/aw;

    .line 41
    return-void

    .line 40
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/d;->ao:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/d;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 76
    iget-object v8, p0, Lcom/google/android/apps/gmm/suggest/g/d;->a:Lcom/google/android/apps/gmm/suggest/d;

    iget-object v9, p0, Lcom/google/android/apps/gmm/suggest/g/d;->b:Lcom/google/android/apps/gmm/suggest/e/d;

    iget v5, p0, Lcom/google/android/apps/gmm/suggest/g/d;->e:I

    iget-object v0, v8, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->o()Lcom/google/b/c/cv;

    move-result-object v4

    iget-object v0, v8, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->gr:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    move-result-object v3

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/b;->a:Lcom/google/android/apps/gmm/suggest/d/b;

    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/d/b;->e:Lcom/google/b/f/t;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/d/b;->f:Lcom/google/b/f/cj;

    if-eqz v2, :cond_0

    iget-object v6, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v6, v2}, Lcom/google/maps/g/ia;->a(Lcom/google/b/f/cj;)Lcom/google/maps/g/ia;

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/maps/g/hy;

    invoke-virtual {v4}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge v5, v0, :cond_2

    invoke-virtual {v4, v5}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v9, v0, :cond_2

    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/a;

    iget-object v2, v8, Lcom/google/android/apps/gmm/suggest/d;->e:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/suggest/d/a;-><init>(Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;Ljava/lang/String;Lcom/google/b/c/cv;I)V

    iget-object v2, v8, Lcom/google/android/apps/gmm/suggest/d;->g:Lcom/google/android/apps/gmm/suggest/d/d;

    iget-object v1, v8, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    iget-object v1, v8, Lcom/google/android/apps/gmm/suggest/d;->h:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/apps/gmm/suggest/d/d;->a(Lcom/google/android/apps/gmm/suggest/d/a;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/suggest/d/e;

    move-result-object v0

    :goto_0
    iget-object v1, v8, Lcom/google/android/apps/gmm/suggest/d;->f:Lcom/google/android/apps/gmm/suggest/a/a;

    if-eqz v1, :cond_1

    iget-object v1, v8, Lcom/google/android/apps/gmm/suggest/d;->f:Lcom/google/android/apps/gmm/suggest/a/a;

    invoke-interface {v1, v9, v6, v0}, Lcom/google/android/apps/gmm/suggest/a/a;->a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V

    .line 77
    :cond_1
    return-object v7

    .line 76
    :cond_2
    const-string v0, "Transient state: displayed suggestions have changed after click"

    const-string v1, "SuggestContentPresenter"

    new-instance v2, Lcom/google/android/apps/gmm/suggest/d/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/suggest/d/c;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v7

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/d;->b:Lcom/google/android/apps/gmm/suggest/e/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/g/d;->d:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method
