.class public Lcom/google/android/apps/gmm/iamhere/c/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/l;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/google/android/apps/gmm/iamhere/c/a;

.field public final b:Lcom/google/android/apps/gmm/iamhere/c/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/iamhere/c/m;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/l;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/a;Lcom/google/android/apps/gmm/iamhere/c/h;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/iamhere/c/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 20
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/iamhere/c/h;

    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 21
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 25
    instance-of v1, p1, Lcom/google/android/apps/gmm/iamhere/c/l;

    if-eqz v1, :cond_0

    .line 26
    check-cast p1, Lcom/google/android/apps/gmm/iamhere/c/l;

    .line 27
    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 28
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/iamhere/c/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 30
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->a:Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/l;->b:Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    return-void
.end method
