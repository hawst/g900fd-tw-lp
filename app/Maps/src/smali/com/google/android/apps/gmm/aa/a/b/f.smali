.class public Lcom/google/android/apps/gmm/aa/a/b/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/aa/a/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private b:F


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/aa/a/b/f;->a(Landroid/content/Intent;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    .line 32
    return-void
.end method

.method private a(Landroid/content/Intent;)Lcom/google/b/c/cv;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/aa/a/b/g;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const v2, 0x3f4ccccd    # 0.8f

    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 79
    if-nez p1, :cond_0

    .line 80
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 128
    :goto_0
    return-object v0

    .line 82
    :cond_0
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 83
    if-nez v4, :cond_1

    .line 84
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_1
    const-string v0, "android.speech.extra.CONFIDENCE_SCORES"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getFloatArrayExtra(Ljava/lang/String;)[F

    move-result-object v0

    .line 87
    if-nez v0, :cond_6

    .line 88
    new-array v0, v3, [F

    move-object v1, v0

    .line 107
    :goto_1
    array-length v0, v1

    if-lez v0, :cond_3

    aget v0, v1, v3

    :goto_2
    iput v0, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->b:F

    .line 108
    iget v0, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->b:F

    cmpg-float v0, v0, v11

    if-gtz v0, :cond_2

    .line 109
    iput v2, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->b:F

    .line 113
    :cond_2
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    move v2, v3

    .line 114
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 115
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    const/high16 v6, 0x3f000000    # 0.5f

    iget v7, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->b:F

    int-to-float v8, v2

    const v9, 0x3dcccccd    # 0.1f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 118
    array-length v7, v1

    if-ge v2, v7, :cond_4

    .line 119
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "populateCandidates candidate: %s confidence: %1.2f => %1.2f"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v0, v9, v3

    aget v10, v1, v2

    .line 121
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v9, v13

    .line 119
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 126
    :goto_4
    new-instance v7, Lcom/google/android/apps/gmm/aa/a/b/g;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0, v6}, Lcom/google/android/apps/gmm/aa/a/b/g;-><init>(Ljava/lang/String;F)V

    invoke-virtual {v5, v7}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 114
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_3
    move v0, v2

    .line 107
    goto :goto_2

    .line 123
    :cond_4
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "populateCandidates candidate: %s confidence default => %1.2f"

    new-array v9, v13, [Ljava/lang/Object;

    aput-object v0, v9, v3

    .line 124
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v9, v12

    .line 123
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_4

    .line 128
    :cond_5
    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 143
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    .line 144
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/b/a/ah;->a:Z

    const-string v1, "candidate"

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    .line 145
    invoke-virtual {v2}, Lcom/google/b/c/cv;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "baseConfidence"

    iget v2, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->b:F

    .line 146
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "top candidate"

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    .line 147
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b/f;->a:Lcom/google/b/c/cv;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/a/b/g;

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
