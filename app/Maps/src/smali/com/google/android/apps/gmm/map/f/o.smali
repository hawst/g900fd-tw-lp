.class public Lcom/google/android/apps/gmm/map/f/o;
.super Lcom/google/android/apps/gmm/v/n;
.source "PG"


# static fields
.field private static final M:F

.field private static final Z:Ljava/util/concurrent/atomic/AtomicLong;

.field static final a:F

.field public static final b:Lcom/google/android/apps/gmm/map/f/a/e;

.field public static final c:Lcom/google/android/apps/gmm/map/f/a/a;


# instance fields
.field public final A:Lcom/google/android/apps/gmm/map/f/j;

.field public B:Lcom/google/android/apps/gmm/v/cp;

.field public C:Lcom/google/android/apps/gmm/map/f/a;

.field public D:Lcom/google/android/apps/gmm/map/f/a/i;

.field private final N:Lcom/google/android/apps/gmm/shared/c/a/p;

.field private O:F

.field private P:Lcom/google/android/apps/gmm/map/b/a/y;

.field private Q:Lcom/google/android/apps/gmm/map/b/a/y;

.field private R:Lcom/google/android/apps/gmm/map/b/a/bb;

.field private S:Z

.field private final T:[D

.field private U:Z

.field private final V:[F

.field private final W:[F

.field private final X:[F

.field private final Y:Lcom/google/android/apps/gmm/map/b/a/y;

.field private aa:Lcom/google/android/apps/gmm/map/f/s;

.field public d:Lcom/google/android/apps/gmm/map/f/a/a;

.field public e:I

.field public f:Lcom/google/android/apps/gmm/map/b/a/y;

.field public g:F

.field public h:F

.field public i:F

.field public volatile j:F

.field public k:Lcom/google/android/apps/gmm/map/f/e;

.field l:Lcom/google/android/apps/gmm/map/b/a/y;

.field public m:F

.field final n:[F

.field final o:[F

.field final p:[F

.field q:Z

.field r:Z

.field s:Z

.field public volatile t:J

.field final u:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/map/f/h;",
            ">;"
        }
    .end annotation
.end field

.field final v:Ljava/lang/Object;

.field w:Lcom/google/android/apps/gmm/map/f/h;

.field public final x:Lcom/google/android/apps/gmm/map/f/q;

.field y:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final z:Lcom/google/android/apps/gmm/v/cp;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 63
    const/high16 v0, 0x48800000    # 262144.0f

    const-wide v2, 0x3ff4f1a6c638d03fL    # 1.3089969389957472

    .line 64
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    sput v0, Lcom/google/android/apps/gmm/map/f/o;->a:F

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x1

    invoke-direct {v0, v4, v4, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    .line 94
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    sput-object v0, Lcom/google/android/apps/gmm/map/f/o;->b:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 102
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v4, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iput v6, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    .line 103
    iput v6, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    sget-object v0, Lcom/google/android/apps/gmm/map/f/o;->b:Lcom/google/android/apps/gmm/map/f/a/e;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/o;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 111
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    const-wide v2, 0x3fd0c152382d7365L    # 0.2617993877991494

    .line 112
    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/map/f/o;->M:F

    .line 210
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/a/a;FLcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/v/ad;I)V
    .locals 9
    .param p5    # Lcom/google/android/apps/gmm/v/ad;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x10

    const/4 v6, 0x1

    .line 412
    const v3, 0x3dcccccd    # 0.1f

    const/high16 v4, 0x41a00000    # 20.0f

    const/high16 v5, 0x41f00000    # 30.0f

    move-object v0, p0

    move-object v1, p4

    move v2, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/n;-><init>(Lcom/google/android/apps/gmm/v/bi;IFFF)V

    .line 152
    sget v0, Lcom/google/android/apps/gmm/map/f/o;->M:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->O:F

    .line 177
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/o;->c()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->R:Lcom/google/android/apps/gmm/map/b/a/bb;

    .line 178
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/f/o;->S:Z

    .line 182
    const/4 v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->T:[D

    .line 183
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/f/o;->U:Z

    .line 186
    new-array v0, v7, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->n:[F

    .line 187
    new-array v0, v7, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    .line 188
    new-array v0, v7, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    .line 189
    new-array v0, v7, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    .line 191
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    .line 194
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/f/o;->r:Z

    .line 195
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    .line 197
    new-array v0, v7, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->W:[F

    .line 201
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->X:[F

    .line 204
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 216
    sget-object v0, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 219
    new-instance v0, Lcom/google/android/apps/gmm/map/f/p;

    const/16 v1, 0xa

    const-string v2, "Camera snapshot pool"

    invoke-direct {v0, p0, v1, v8, v2}, Lcom/google/android/apps/gmm/map/f/p;-><init>(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->u:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 229
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->v:Ljava/lang/Object;

    .line 232
    new-instance v0, Lcom/google/android/apps/gmm/map/f/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->u:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/f/h;-><init>(Lcom/google/android/apps/gmm/map/util/a/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->w:Lcom/google/android/apps/gmm/map/f/h;

    .line 244
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 413
    iput p2, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    .line 414
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/f/o;->N:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 415
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->h()V

    .line 416
    new-instance v0, Lcom/google/android/apps/gmm/map/f/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/f/j;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    .line 417
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v0

    invoke-virtual {p3, v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 418
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 419
    invoke-virtual {p4}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    invoke-virtual {p4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    invoke-virtual {p0, p4, v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/v/bi;II)V

    .line 420
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->l()V

    .line 422
    if-eqz p5, :cond_0

    .line 423
    new-instance v0, Lcom/google/android/apps/gmm/map/f/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/f/q;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, p5, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v0, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 425
    new-instance v0, Lcom/google/android/apps/gmm/v/cr;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/cr;-><init>(Lcom/google/android/apps/gmm/v/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    .line 426
    new-instance v0, Lcom/google/android/apps/gmm/map/f/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->aa:Lcom/google/android/apps/gmm/map/f/s;

    new-instance v0, Lcom/google/android/apps/gmm/v/cr;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->aa:Lcom/google/android/apps/gmm/map/f/s;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/cr;-><init>(Lcom/google/android/apps/gmm/v/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->B:Lcom/google/android/apps/gmm/v/cp;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->aa:Lcom/google/android/apps/gmm/map/f/s;

    iget-object v1, p5, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v0, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 431
    :goto_0
    return-void

    .line 428
    :cond_0
    iput-object v8, p0, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    .line 429
    iput-object v8, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/a/a;IIFLcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 7

    .prologue
    .line 381
    new-instance v4, Lcom/google/android/apps/gmm/map/f/r;

    invoke-direct {v4, p2, p3}, Lcom/google/android/apps/gmm/map/f/r;-><init>(II)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;FLcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/v/ad;I)V

    .line 383
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/16 v4, 0x10

    const/4 v3, 0x1

    .line 348
    new-instance v0, Lcom/google/android/apps/gmm/map/f/r;

    .line 350
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/f/r;-><init>(II)V

    .line 348
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/v/n;-><init>(Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/bi;)V

    .line 152
    sget v0, Lcom/google/android/apps/gmm/map/f/o;->M:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->O:F

    .line 177
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/o;->c()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->R:Lcom/google/android/apps/gmm/map/b/a/bb;

    .line 178
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/f/o;->S:Z

    .line 182
    const/4 v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->T:[D

    .line 183
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/f/o;->U:Z

    .line 186
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->n:[F

    .line 187
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    .line 188
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    .line 189
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    .line 191
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    .line 194
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/f/o;->r:Z

    .line 195
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    .line 197
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->W:[F

    .line 201
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->X:[F

    .line 204
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 216
    sget-object v0, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 219
    new-instance v0, Lcom/google/android/apps/gmm/map/f/p;

    const/16 v1, 0xa

    const-string v2, "Camera snapshot pool"

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/google/android/apps/gmm/map/f/p;-><init>(Lcom/google/android/apps/gmm/map/f/o;ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->u:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 229
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->v:Ljava/lang/Object;

    .line 232
    new-instance v0, Lcom/google/android/apps/gmm/map/f/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->u:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/f/h;-><init>(Lcom/google/android/apps/gmm/map/util/a/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->w:Lcom/google/android/apps/gmm/map/f/h;

    .line 244
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 351
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/f/o;->N:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 352
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    .line 353
    iput-object v5, p0, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    .line 354
    iput-object v5, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    .line 355
    new-instance v0, Lcom/google/android/apps/gmm/map/f/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/f/j;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    .line 356
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->e:I

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->D:Lcom/google/android/apps/gmm/map/f/a/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->D:Lcom/google/android/apps/gmm/map/f/a/i;

    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/f/o;->t:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    iput-object v5, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->g:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->O:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->O:F

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->K:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->j:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->j:F

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/f/o;->U:Z

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->k()V

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    invoke-static {v0, v6, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 357
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto :goto_0
.end method

.method public static a(FIF)F
    .locals 4

    .prologue
    .line 790
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    const/high16 v2, 0x41f00000    # 30.0f

    sub-float/2addr v2, p0

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 794
    int-to-float v1, p1

    const/high16 v2, 0x43800000    # 256.0f

    mul-float/2addr v2, p2

    div-float/2addr v1, v2

    .line 795
    mul-float/2addr v0, v1

    return v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/f/a/a;FFLcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 14

    .prologue
    .line 1492
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/f/a/a;)F

    move-result v4

    .line 1493
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v2, v4

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v3, v5

    div-float/2addr v2, v3

    .line 1494
    mul-float v5, p2, v2

    .line 1495
    move/from16 v0, p3

    neg-float v3, v0

    mul-float/2addr v3, v2

    .line 1497
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1499
    iget v6, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    .line 1500
    float-to-double v2, v3

    float-to-double v6, v4

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 1502
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v8, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    float-to-double v8, v8

    const-wide v10, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v8, v10

    add-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    div-double v2, v6, v2

    double-to-float v2, v2

    mul-float v3, v4, v2

    .line 1511
    mul-float v2, v4, v4

    mul-float v6, v3, v3

    add-float/2addr v2, v6

    float-to-double v6, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v4

    mul-float/2addr v2, v3

    float-to-double v8, v2

    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/high16 v10, 0x42b40000    # 90.0f

    add-float/2addr v2, v10

    float-to-double v10, v2

    const-wide v12, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v10, v12

    .line 1514
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    .line 1511
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v2, v6

    .line 1515
    div-float/2addr v2, v4

    .line 1518
    :cond_0
    iget v4, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    const/high16 v6, 0x42b40000    # 90.0f

    add-float/2addr v4, v6

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/f/a/a;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 1519
    iget v6, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/f/a/a;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    .line 1520
    neg-float v5, v5

    mul-float/2addr v2, v5

    invoke-static {v4, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1521
    neg-float v2, v3

    invoke-static {v6, v2, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1523
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 1524
    move-object/from16 v0, p4

    invoke-static {v2, v6, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1525
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 725
    :cond_0
    :goto_0
    return-void

    .line 698
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 699
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->k()V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_2

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_2

    .line 701
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_3

    .line 702
    :cond_2
    iput-boolean v11, p0, Lcom/google/android/apps/gmm/map/f/o;->U:Z

    .line 704
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 705
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 706
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const/high16 v5, 0x41f00000    # 30.0f

    sub-float v2, v5, v2

    float-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-float v2, v6

    int-to-float v3, v3

    const/high16 v5, 0x43800000    # 256.0f

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/o;->O:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    .line 707
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    if-eq v2, v3, :cond_6

    .line 708
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 709
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    invoke-direct {p0, p1, v10, v3, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FFLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 710
    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    cmpl-float v0, v2, v0

    if-eqz v0, :cond_5

    .line 713
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-virtual {p0, v0, v10}, Lcom/google/android/apps/gmm/map/f/o;->b(FF)V

    .line 718
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v0, :cond_7

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto/16 :goto_0

    .line 716
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_1

    .line 722
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/j;->a()V

    goto/16 :goto_0
.end method

.method private c(Lcom/google/android/apps/gmm/map/f/a/a;)F
    .locals 8

    .prologue
    .line 752
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 753
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    .line 752
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const/high16 v3, 0x41f00000    # 30.0f

    sub-float v0, v3, v0

    float-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v0, v4

    int-to-float v1, v1

    const/high16 v3, 0x43800000    # 256.0f

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 754
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->O:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public static c()Lcom/google/android/apps/gmm/map/b/a/bb;
    .locals 4

    .prologue
    .line 1381
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/bb;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/bb;-><init>([Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    .line 604
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    .line 605
    mul-int/2addr v1, v1

    mul-int/2addr v0, v0

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 606
    const/high16 v1, 0x41f00000    # 30.0f

    .line 607
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(FF)F

    move-result v0

    float-to-double v0, v0

    .line 606
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 611
    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    .line 616
    const/high16 v1, 0x40000000    # 2.0f

    .line 617
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/s;->d(F)F

    move-result v0

    .line 616
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->j:F

    .line 618
    return-void
.end method

.method private i()[D
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide v6, 0x3f91df46a2529d39L    # 0.017453292519943295

    const-wide v4, 0x4076800000000000L    # 360.0

    .line 856
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->U:Z

    if-eqz v0, :cond_1

    .line 863
    const-wide v0, 0x4056800000000000L    # 90.0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    float-to-double v2, v2

    sub-double/2addr v0, v2

    .line 864
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    .line 865
    add-double/2addr v0, v4

    .line 869
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    .line 870
    mul-double/2addr v0, v6

    .line 871
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 872
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/f/o;->T:[D

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v8, v4

    aput-wide v8, v6, v10

    .line 873
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/f/o;->T:[D

    const/4 v7, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    aput-wide v0, v6, v7

    .line 874
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->T:[D

    const/4 v1, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    neg-double v2, v2

    aput-wide v2, v0, v1

    .line 875
    iput-boolean v10, p0, Lcom/google/android/apps/gmm/map/f/o;->U:Z

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->T:[D

    return-object v0

    .line 866
    :cond_2
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_0

    .line 867
    sub-double/2addr v0, v4

    goto :goto_0
.end method

.method private j()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 9

    .prologue
    const-wide v6, 0x3f91df46a2529d39L    # 0.017453292519943295

    const/high16 v8, 0x47800000    # 65536.0f

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->P:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    const-wide v2, 0x4056800000000000L    # 90.0

    iget v1, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    float-to-double v4, v1

    sub-double/2addr v2, v4

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v1, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    mul-float/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-float/2addr v2, v8

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->P:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 926
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->P:Lcom/google/android/apps/gmm/map/b/a/y;

    return-object v0

    .line 924
    :cond_1
    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    float-to-double v4, v0

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v3, v6

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    mul-float/2addr v1, v3

    mul-float/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-float/2addr v2, v3

    mul-float/2addr v2, v8

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v8

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1657
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1658
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->P:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1659
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->Q:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1660
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/f/o;->S:Z

    .line 1661
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->m:F

    .line 1663
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    .line 1664
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/f/o;->r:Z

    .line 1665
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    .line 1666
    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1692
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    invoke-static {v0, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1693
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    .line 1694
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 1697
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    aput v0, v2, v3

    .line 1698
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    const/4 v3, 0x5

    neg-float v4, v1

    aput v4, v2, v3

    .line 1699
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    const/16 v3, 0xa

    aput v5, v2, v3

    .line 1700
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    const/16 v3, 0xf

    aput v5, v2, v3

    .line 1703
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    const/16 v3, 0xc

    aput v0, v2, v3

    .line 1704
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    const/16 v2, 0xd

    aput v1, v0, v2

    .line 1705
    return-void
.end method


# virtual methods
.method public final a(FF)F
    .locals 8

    .prologue
    const/high16 v3, 0x42b40000    # 90.0f

    const-wide v6, 0x3f91df46a2529d39L    # 0.017453292519943295

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 628
    cmpg-float v0, v2, p2

    if-gtz v0, :cond_0

    cmpg-float v0, p2, v3

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 634
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    div-float/2addr v0, v4

    .line 635
    cmpl-float v1, p2, v2

    if-lez v1, :cond_2

    sub-float v1, v3, v0

    sub-float/2addr v1, p2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 636
    float-to-double v2, p2

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 637
    float-to-double v2, v0

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 638
    mul-float/2addr v0, v4

    div-float v2, v4, v1

    sub-float/2addr v0, v2

    div-float v0, p1, v0

    .line 639
    neg-float v2, v0

    div-float v1, v2, v1

    .line 640
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-float p1, v0

    .line 642
    :cond_2
    const/high16 v0, 0x43800000    # 256.0f

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    mul-float/2addr v0, v1

    div-float v0, p1, v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F
    .locals 8

    .prologue
    .line 985
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->i()[D

    move-result-object v0

    .line 986
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 987
    if-eqz p2, :cond_0

    .line 988
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 990
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v2, v1

    const/4 v1, 0x0

    aget-wide v4, v0, v1

    mul-double/2addr v2, v4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v1

    const/4 v1, 0x1

    aget-wide v6, v0, v1

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->Y:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 991
    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-double v4, v1

    const/4 v1, 0x2

    aget-wide v0, v0, v1

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public final a(FFFF)Lcom/google/android/apps/gmm/map/b/a/m;
    .locals 22

    .prologue
    .line 1129
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    if-eqz v2, :cond_0

    .line 1134
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/f/o;->e()V

    .line 1137
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 1138
    iget v8, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 1139
    iget v9, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1140
    iget v10, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v11, v2, v8

    .line 1142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int v12, v2, v9

    .line 1145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->X:[F

    .line 1149
    const/4 v3, 0x0

    aput p1, v2, v3

    .line 1150
    const/4 v3, 0x1

    aput p3, v2, v3

    .line 1151
    const/4 v3, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    .line 1152
    const/4 v3, 0x3

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    .line 1153
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1156
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 1157
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 1158
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 1159
    const/4 v13, 0x6

    aget v13, v2, v13

    mul-float/2addr v3, v13

    int-to-float v13, v10

    div-float/2addr v3, v13

    float-to-double v14, v3

    .line 1161
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v14, v16

    if-ltz v3, :cond_1

    .line 1163
    const/4 v2, 0x0

    .line 1230
    :goto_0
    return-object v2

    .line 1167
    :cond_1
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    sub-double v14, v18, v14

    div-double v14, v16, v14

    .line 1168
    int-to-double v0, v11

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    mul-double/2addr v4, v14

    int-to-double v0, v8

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    .line 1169
    int-to-double v0, v12

    move-wide/from16 v16, v0

    add-double v6, v6, v16

    mul-double/2addr v6, v14

    int-to-double v14, v9

    add-double/2addr v6, v14

    .line 1170
    new-instance v13, Lcom/google/android/apps/gmm/map/b/a/y;

    double-to-int v3, v4

    double-to-int v4, v6

    invoke-direct {v13, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 1176
    const/4 v3, 0x0

    aput p2, v2, v3

    .line 1177
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1178
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 1179
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 1180
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 1181
    const/4 v14, 0x6

    aget v14, v2, v14

    mul-float/2addr v3, v14

    int-to-float v14, v10

    div-float/2addr v3, v14

    float-to-double v14, v3

    .line 1183
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v14, v16

    if-ltz v3, :cond_2

    .line 1184
    const/4 v2, 0x0

    goto :goto_0

    .line 1186
    :cond_2
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    sub-double v14, v18, v14

    div-double v14, v16, v14

    .line 1187
    int-to-double v0, v11

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    mul-double/2addr v4, v14

    int-to-double v0, v8

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    .line 1188
    int-to-double v0, v12

    move-wide/from16 v16, v0

    add-double v6, v6, v16

    mul-double/2addr v6, v14

    int-to-double v14, v9

    add-double/2addr v6, v14

    .line 1189
    new-instance v14, Lcom/google/android/apps/gmm/map/b/a/y;

    double-to-int v3, v4

    double-to-int v4, v6

    invoke-direct {v14, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 1195
    const/4 v3, 0x1

    aput p4, v2, v3

    .line 1196
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1197
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 1198
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 1199
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 1200
    const/4 v15, 0x6

    aget v15, v2, v15

    mul-float/2addr v3, v15

    int-to-float v15, v10

    div-float/2addr v3, v15

    float-to-double v0, v3

    move-wide/from16 v16, v0

    .line 1202
    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v16, v18

    if-ltz v3, :cond_3

    .line 1203
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1205
    :cond_3
    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    sub-double v16, v20, v16

    div-double v16, v18, v16

    .line 1206
    int-to-double v0, v11

    move-wide/from16 v18, v0

    add-double v4, v4, v18

    mul-double v4, v4, v16

    int-to-double v0, v8

    move-wide/from16 v18, v0

    add-double v4, v4, v18

    .line 1207
    int-to-double v0, v12

    move-wide/from16 v18, v0

    add-double v6, v6, v18

    mul-double v6, v6, v16

    int-to-double v0, v9

    move-wide/from16 v16, v0

    add-double v6, v6, v16

    .line 1208
    new-instance v15, Lcom/google/android/apps/gmm/map/b/a/y;

    double-to-int v3, v4

    double-to-int v4, v6

    invoke-direct {v15, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 1214
    const/4 v3, 0x0

    aput p1, v2, v3

    .line 1215
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1216
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 1217
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 1218
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 1219
    const/16 v16, 0x6

    aget v2, v2, v16

    mul-float/2addr v2, v3

    int-to-float v3, v10

    div-float/2addr v2, v3

    float-to-double v2, v2

    .line 1221
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v10, v2, v16

    if-ltz v10, :cond_4

    .line 1222
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1224
    :cond_4
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    sub-double v2, v18, v2

    div-double v2, v16, v2

    .line 1225
    int-to-double v10, v11

    add-double/2addr v4, v10

    mul-double/2addr v4, v2

    int-to-double v10, v8

    add-double/2addr v4, v10

    .line 1226
    int-to-double v10, v12

    add-double/2addr v6, v10

    mul-double/2addr v2, v6

    int-to-double v6, v9

    add-double/2addr v2, v6

    .line 1227
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    double-to-int v4, v4

    double-to-int v2, v2

    invoke-direct {v6, v4, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 1230
    invoke-static {v6, v15, v13, v14}, Lcom/google/android/apps/gmm/map/b/a/m;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/m;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 8

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 890
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->i()[D

    move-result-object v0

    .line 891
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    neg-float v2, v2

    float-to-double v2, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    mul-double/2addr v2, v4

    .line 892
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    neg-float v3, v3

    float-to-double v4, v3

    const/4 v3, 0x1

    aget-wide v6, v0, v3

    mul-double/2addr v4, v6

    .line 893
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    iget v4, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    neg-float v4, v4

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-wide v6, v0, v6

    mul-double/2addr v4, v6

    .line 894
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 895
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 897
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 1445
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/f/a/a;)F

    move-result v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    .line 1446
    mul-float v1, p2, v0

    .line 1447
    neg-float v2, p3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    float-to-double v2, v2

    const-wide v4, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v0, v2

    .line 1449
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->Q:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    const/high16 v3, 0x42b40000    # 90.0f

    add-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/f/a/a;->a(F)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->Q:Lcom/google/android/apps/gmm/map/b/a/y;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->Q:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1450
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->j()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    .line 1451
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v4, v5, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 1452
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v2, v5, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 1453
    invoke-static {v4, v1, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1454
    invoke-static {v2, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1456
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1457
    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1458
    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 1459
    invoke-static {v0, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1460
    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1461
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/f;FFF)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 8

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1557
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    sub-float v6, p4, v0

    .line 1558
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    sub-float v7, p5, v0

    .line 1567
    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 1569
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput p3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 1570
    invoke-virtual {p2, v5}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    .line 1571
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 1572
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 1573
    neg-float v1, v6

    neg-float v2, v7

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/f;FFF)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 8

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1537
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    sub-float v6, p3, v0

    .line 1538
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    mul-float/2addr v0, v1

    sub-float v7, p4, v0

    .line 1540
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {p0, v0, v6, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 1542
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    add-float/2addr v0, p2

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    .line 1543
    invoke-virtual {p1, v5}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    .line 1544
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 1545
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 1546
    neg-float v1, v6

    neg-float v2, v7

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 663
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 664
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    .line 677
    :cond_0
    :goto_0
    return-void

    .line 668
    :cond_1
    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/v/n;->a(F)V

    .line 669
    sget-object v1, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 670
    float-to-double v0, v0

    const-wide v2, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    div-double v0, v4, v0

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    .line 671
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->k()V

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    if-eq v0, v1, :cond_2

    .line 673
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FFLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 676
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0
.end method

.method public a(FLcom/google/android/apps/gmm/map/b/a/bb;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1394
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v0, v0

    .line 1401
    sget v1, Lcom/google/android/apps/gmm/map/f/o;->a:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    const/high16 v1, 0x48800000    # 262144.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x42652ee1

    mul-float/2addr v0, v1

    :goto_0
    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1403
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    sub-float v1, v0, v1

    .line 1404
    const v2, 0x3c8efa35

    mul-float/2addr v1, v2

    .line 1412
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    .line 1413
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    .line 1414
    int-to-float v4, v3

    iget v5, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    mul-float/2addr v4, v5

    float-to-double v6, v1

    .line 1415
    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    double-to-float v1, v6

    mul-float/2addr v1, v4

    .line 1416
    int-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    float-to-double v6, v1

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 1419
    int-to-float v4, v3

    const/4 v5, 0x0

    iget-object v6, p2, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v5, v6, v5

    invoke-virtual {p0, v8, v4, v5}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-eqz v4, :cond_0

    int-to-float v4, v2

    int-to-float v5, v3

    const/4 v6, 0x1

    .line 1421
    iget-object v7, p2, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v6, v7, v6

    .line 1420
    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x3

    .line 1422
    iget-object v5, p2, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v4, v5, v4

    invoke-virtual {p0, v8, v1, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-eqz v4, :cond_0

    int-to-float v4, v2

    const/4 v5, 0x2

    .line 1424
    iget-object v6, p2, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v5, v6, v5

    .line 1423
    invoke-virtual {p0, v4, v1, v5}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1425
    :cond_0
    new-instance v4, Ljava/lang/IllegalStateException;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/f/a/a;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x51

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "pos: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " farAngle: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " size: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " top:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1401
    :cond_1
    const/high16 v0, 0x42960000    # 75.0f

    goto/16 :goto_0

    .line 1429
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/b/a/bb;->a()V

    .line 1430
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 683
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/o;->e:I

    if-eq v0, p1, :cond_0

    .line 684
    iput p1, p0, Lcom/google/android/apps/gmm/map/f/o;->e:I

    .line 685
    sget-object v0, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 688
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/f/r;

    new-instance v1, Lcom/google/android/apps/gmm/v/bj;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/gmm/v/bj;-><init>(II)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/f/r;->h:Lcom/google/android/apps/gmm/v/bj;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/v/bi;II)V

    .line 586
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 526
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 734
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/d;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/map/f/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1874
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v0, :cond_0

    .line 1875
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    if-ne p1, v2, :cond_1

    monitor-exit v1

    .line 1877
    :cond_0
    :goto_0
    return-void

    .line 1875
    :cond_1
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    if-eqz p1, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/ct;->b:Lcom/google/android/apps/gmm/v/ct;

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/ct;->b:Lcom/google/android/apps/gmm/v/ct;

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/gmm/v/g;->b(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/v/bi;II)V
    .locals 8

    .prologue
    .line 553
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/bi;II)V

    .line 561
    sget-object v0, Lcom/google/android/apps/gmm/map/f/o;->Z:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 562
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->h()V

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const/high16 v3, 0x41f00000    # 30.0f

    sub-float v0, v3, v0

    float-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v0, v4

    int-to-float v1, v1

    const/high16 v3, 0x43800000    # 256.0f

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->O:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FFLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 565
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->k()V

    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->l()V

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 570
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->aa:Lcom/google/android/apps/gmm/map/f/s;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->aa:Lcom/google/android/apps/gmm/map/f/s;

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/s;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/s;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0
.end method

.method public final a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 12

    .prologue
    .line 1068
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    if-eqz v0, :cond_0

    .line 1073
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->e()V

    .line 1076
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->X:[F

    .line 1080
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1081
    const/4 v1, 0x1

    aput p2, v0, v1

    .line 1082
    const/4 v1, 0x2

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 1083
    const/4 v1, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 1084
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v4, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1086
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 1087
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 1088
    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 1089
    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1094
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x7

    aget v5, v0, v5

    div-float/2addr v4, v5

    .line 1095
    const/4 v5, 0x4

    aget v5, v0, v5

    mul-float/2addr v5, v4

    float-to-double v6, v5

    .line 1096
    const/4 v5, 0x5

    aget v5, v0, v5

    mul-float/2addr v5, v4

    float-to-double v8, v5

    .line 1097
    const/4 v5, 0x6

    aget v0, v0, v5

    mul-float/2addr v0, v4

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 1099
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_1

    .line 1102
    const/4 v0, 0x0

    .line 1110
    :goto_0
    return v0

    .line 1106
    :cond_1
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double v0, v10, v0

    div-double v0, v4, v0

    .line 1107
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v4, v4

    add-double/2addr v4, v6

    int-to-double v6, v2

    sub-double/2addr v4, v6

    mul-double/2addr v4, v0

    int-to-double v6, v2

    add-double/2addr v4, v6

    .line 1108
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v2

    add-double/2addr v6, v8

    int-to-double v8, v3

    sub-double/2addr v6, v8

    mul-double/2addr v0, v6

    int-to-double v2, v3

    add-double/2addr v0, v2

    .line 1109
    double-to-int v2, v4

    double-to-int v0, v0

    iput v2, p3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, p3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1110
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x7

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1271
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    if-eqz v2, :cond_0

    .line 1272
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->e()V

    .line 1280
    :cond_0
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    const/high16 v3, 0x20000000

    add-int/2addr v2, v3

    const v3, 0x3fffffff    # 1.9999999f

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    sub-int/2addr v2, v3

    .line 1283
    int-to-float v2, v2

    aput v2, p2, v0

    .line 1284
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    aput v2, p2, v1

    .line 1285
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v2, v2

    aput v2, p2, v7

    .line 1288
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    .line 1289
    aget v3, v2, v0

    aget v4, p2, v0

    mul-float/2addr v3, v4

    aget v4, v2, v9

    aget v5, p2, v1

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    const/16 v4, 0x8

    aget v4, v2, v4

    aget v5, p2, v7

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    const/16 v4, 0xc

    aget v4, v2, v4

    add-float/2addr v3, v4

    aput v3, p2, v9

    .line 1290
    const/4 v3, 0x5

    aget v4, v2, v1

    aget v5, p2, v0

    mul-float/2addr v4, v5

    const/4 v5, 0x5

    aget v5, v2, v5

    aget v6, p2, v1

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    const/16 v5, 0x9

    aget v5, v2, v5

    aget v6, p2, v7

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    const/16 v5, 0xd

    aget v5, v2, v5

    add-float/2addr v4, v5

    aput v4, p2, v3

    .line 1291
    const/4 v3, 0x3

    aget v3, v2, v3

    aget v4, p2, v0

    mul-float/2addr v3, v4

    aget v4, v2, v8

    aget v5, p2, v1

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    const/16 v4, 0xb

    aget v4, v2, v4

    aget v5, p2, v7

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    const/16 v4, 0xf

    aget v2, v2, v4

    add-float/2addr v2, v3

    aput v2, p2, v8

    .line 1293
    aget v2, p2, v8

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 1294
    const/high16 v2, 0x7fc00000    # NaNf

    aput v2, p2, v0

    .line 1295
    const/high16 v2, 0x7fc00000    # NaNf

    aput v2, p2, v1

    .line 1303
    :goto_0
    return v0

    .line 1300
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    aget v3, p2, v8

    div-float/2addr v2, v3

    .line 1301
    aget v3, p2, v9

    mul-float/2addr v3, v2

    aput v3, p2, v0

    .line 1302
    const/4 v0, 0x5

    aget v0, p2, v0

    mul-float/2addr v0, v2

    aput v0, p2, v1

    move v0, v1

    .line 1303
    goto :goto_0
.end method

.method public final b(F)F
    .locals 7

    .prologue
    .line 831
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v0, v0

    const-wide v2, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    const-wide v4, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v0, v4

    mul-double/2addr v0, v2

    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v0

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 833
    float-to-double v0, p1

    iget v4, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    float-to-double v4, v4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->b(DDDI)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 5

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1012
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 1016
    :goto_0
    return v0

    .line 1015
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F

    move-result v1

    .line 1016
    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    div-float v0, v2, v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/bb;
    .locals 4

    .prologue
    .line 1311
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->S:Z

    if-eqz v0, :cond_0

    .line 1316
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->R:Lcom/google/android/apps/gmm/map/b/a/bb;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(FLcom/google/android/apps/gmm/map/b/a/bb;)V

    .line 1317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->S:Z

    .line 1319
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->R:Lcom/google/android/apps/gmm/map/b/a/bb;

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 1476
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 1477
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FFLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1478
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final c(Lcom/google/android/apps/gmm/map/b/a/y;)[I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1243
    const/16 v0, 0x8

    new-array v1, v0, [F

    .line 1248
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1249
    const/4 v0, 0x0

    .line 1254
    :goto_0
    return-object v0

    .line 1251
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1252
    aget v2, v1, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v0, v3

    .line 1253
    aget v1, v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    aput v1, v0, v4

    goto :goto_0
.end method

.method d()V
    .locals 14

    .prologue
    .line 1675
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/o;->m:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/o;->m:F

    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/o;->m:F

    .line 1676
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1678
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 1679
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 1681
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 1682
    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    .line 1683
    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 1685
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/f/o;->j()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 1686
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/o;->n:[F

    iget v5, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    iget v6, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v6, v6

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v1, v1

    neg-float v7, v2

    neg-float v8, v3

    neg-float v9, v0

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-static {v7, v8, v9}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v11

    div-float/2addr v10, v11

    mul-float/2addr v7, v10

    mul-float/2addr v8, v10

    mul-float/2addr v9, v10

    mul-float v10, v8, v1

    mul-float v11, v9, v6

    sub-float/2addr v10, v11

    mul-float v11, v9, v5

    mul-float/2addr v1, v7

    sub-float v1, v11, v1

    mul-float/2addr v6, v7

    mul-float/2addr v5, v8

    sub-float v5, v6, v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v10, v1, v5}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v11

    div-float/2addr v6, v11

    mul-float/2addr v10, v6

    mul-float/2addr v1, v6

    mul-float/2addr v5, v6

    mul-float v6, v1, v9

    mul-float v11, v5, v8

    sub-float/2addr v6, v11

    mul-float v11, v5, v7

    mul-float v12, v10, v9

    sub-float/2addr v11, v12

    mul-float v12, v10, v8

    mul-float v13, v1, v7

    sub-float/2addr v12, v13

    const/4 v13, 0x0

    aput v10, v4, v13

    const/4 v10, 0x1

    aput v6, v4, v10

    const/4 v6, 0x2

    neg-float v7, v7

    aput v7, v4, v6

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput v7, v4, v6

    const/4 v6, 0x4

    aput v1, v4, v6

    const/4 v1, 0x5

    aput v11, v4, v1

    const/4 v1, 0x6

    neg-float v6, v8

    aput v6, v4, v1

    const/4 v1, 0x7

    const/4 v6, 0x0

    aput v6, v4, v1

    const/16 v1, 0x8

    aput v5, v4, v1

    const/16 v1, 0x9

    aput v12, v4, v1

    const/16 v1, 0xa

    neg-float v5, v9

    aput v5, v4, v1

    const/16 v1, 0xb

    const/4 v5, 0x0

    aput v5, v4, v1

    const/16 v1, 0xc

    const/4 v5, 0x0

    aput v5, v4, v1

    const/16 v1, 0xd

    const/4 v5, 0x0

    aput v5, v4, v1

    const/16 v1, 0xe

    const/4 v5, 0x0

    aput v5, v4, v1

    const/16 v1, 0xf

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v4, v1

    const/4 v1, 0x0

    neg-float v2, v2

    neg-float v3, v3

    neg-float v0, v0

    invoke-static {v4, v1, v2, v3, v0}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1687
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    .line 1688
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->r:Z

    .line 1689
    return-void
.end method

.method e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1712
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    if-eqz v0, :cond_0

    .line 1713
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->d()V

    .line 1716
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->W:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->V:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/o;->G:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1717
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->W:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/o;->n:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1723
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 1724
    const/16 v0, 0xc

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    const/16 v3, 0x10

    if-ge v0, v3, :cond_1

    .line 1725
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    aget v4, v3, v0

    int-to-float v5, v2

    mul-float/2addr v4, v5

    aput v4, v3, v0

    .line 1724
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1728
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 1729
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    .line 1730
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1605
    if-ne p0, p1, :cond_1

    .line 1622
    :cond_0
    :goto_0
    return v0

    .line 1612
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/f/o;

    if-eqz v2, :cond_3

    .line 1613
    check-cast p1, Lcom/google/android/apps/gmm/map/f/o;

    .line 1614
    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/o;->g:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1615
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/o;->K:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 1617
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    iget-object v3, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 1618
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    iget-object v3, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1622
    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    if-eqz v0, :cond_0

    .line 1898
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->x:Lcom/google/android/apps/gmm/map/f/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1900
    :cond_0
    return-void
.end method

.method public final g()D
    .locals 5

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v0

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1591
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 1594
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    add-int/2addr v0, v1

    .line 1595
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    add-int/2addr v0, v1

    .line 1596
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 1597
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 1598
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1599
    :goto_0
    add-int/2addr v0, v1

    .line 1600
    return v0

    .line 1598
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1599
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 1578
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1579
    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1580
    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    .line 1581
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/o;->g:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1583
    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1584
    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x46

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
