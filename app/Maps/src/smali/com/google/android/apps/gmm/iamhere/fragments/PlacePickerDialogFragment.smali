.class public Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/a/c;
.implements Lcom/google/android/apps/gmm/iamhere/a/a;


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field d:Lcom/google/android/apps/gmm/iamhere/f/f;

.field public e:Landroid/view/View;

.field public f:Lcom/google/android/apps/gmm/iamhere/c/o;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 47
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/iamhere/c/o;Z)Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;-><init>()V

    .line 58
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v2, "iah_state"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 60
    const-string v2, "has_explore_nearby_button"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 62
    return-object v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/iamhere/c/o;)V
    .locals 2

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    new-instance v0, Lcom/google/android/apps/gmm/iamhere/u;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->g:Z

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/gmm/iamhere/u;-><init>(Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;Lcom/google/android/apps/gmm/iamhere/c/o;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->d:Lcom/google/android/apps/gmm/iamhere/f/f;

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/iamhere/fragments/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/iamhere/fragments/a;-><init>(Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    :cond_0
    monitor-exit p0

    return-void

    .line 165
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final F_()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/iamhere/c/o;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->a:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 186
    :goto_1
    return-void

    .line 182
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 185
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->b(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    goto :goto_1
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/google/b/f/t;->cN:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final declared-synchronized i()V
    .locals 1

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/a/b;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_0
    monitor-exit p0

    return-void

    .line 153
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->k:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->b(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    .line 110
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    if-eqz p1, :cond_2

    .line 69
    :goto_0
    if-eqz p1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "iah_state"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 72
    const-string v0, "has_explore_nearby_button"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->g:Z

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    if-nez v0, :cond_1

    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->c:Ljava/lang/String;

    const-string v1, "No state available in onCreate."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    new-instance v1, Lcom/google/android/apps/gmm/iamhere/u;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->g:Z

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/apps/gmm/iamhere/u;-><init>(Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;Lcom/google/android/apps/gmm/iamhere/c/o;Z)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->d:Lcom/google/android/apps/gmm/iamhere/f/f;

    .line 78
    return-void

    .line 68
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 70
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/iamhere/f/i;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->e:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->e:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->d:Lcom/google/android/apps/gmm/iamhere/f/f;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 87
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 88
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 90
    return-object v0

    .line 87
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/iamhere/a/b;->b(Lcom/google/android/apps/gmm/iamhere/a/a;)V

    .line 103
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onPause()V

    .line 104
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->b(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/iamhere/a/a;)V

    .line 98
    return-void

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "iah_state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->f:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 116
    const-string v0, "has_explore_nearby_button"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/iamhere/fragments/PlacePickerDialogFragment;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 115
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
