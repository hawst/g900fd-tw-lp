.class Lcom/google/android/apps/gmm/navigation/navui/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/d/o;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b()V

    .line 316
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/f/a/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    .line 306
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    const-string v2, "betterRouteState"

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/d/m;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/ag;->a(Lcom/google/maps/g/a/hm;)Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v1

    iput-boolean v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v3, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v4, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v4, v3, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/f;->f:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b:Landroid/view/View;

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v3, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/aq;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/aq;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    iget-wide v2, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->g:J

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a:J

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->setDuration(J)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->e:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a:J

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a(J)V

    .line 309
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    sget-object v2, Lcom/google/b/f/b/a/ah;->e:Lcom/google/b/f/b/a/ah;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/d/m;->a(Lcom/google/b/f/b/a/ah;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->d:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/as;->a:Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->c:Lcom/google/android/apps/gmm/navigation/navui/d/m;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 323
    :cond_0
    return-void
.end method
