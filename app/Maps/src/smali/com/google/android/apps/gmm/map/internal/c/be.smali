.class public final Lcom/google/android/apps/gmm/map/internal/c/be;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[F

.field private static final q:[I

.field private static final r:[I

.field private static s:Lcom/google/android/apps/gmm/map/internal/c/be;


# instance fields
.field public final b:I

.field final c:I

.field public final d:[I

.field public final e:[I

.field public final f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

.field public final g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

.field final h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

.field public final i:Lcom/google/android/apps/gmm/map/internal/c/bn;

.field public final j:Lcom/google/android/apps/gmm/map/internal/c/bm;

.field public final k:Lcom/google/android/apps/gmm/map/internal/c/bd;

.field final l:Lcom/google/android/apps/gmm/map/internal/c/p;

.field public final m:I

.field public final n:I

.field public final o:[F

.field public final p:[I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 37
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/be;->q:[I

    .line 38
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/be;->r:[I

    .line 44
    new-array v0, v2, [F

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/be;->a:[F

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, -0x1

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/be;->q:[I

    new-array v4, v2, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 60
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bn;->a()Lcom/google/android/apps/gmm/map/internal/c/bn;

    move-result-object v5

    .line 61
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bm;->a()Lcom/google/android/apps/gmm/map/internal/c/bm;

    move-result-object v6

    .line 62
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a()Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v7

    const/4 v8, 0x0

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/c/be;->a:[F

    move v9, v2

    move v11, v2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(II[I[Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bm;Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/p;I[FI)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/be;->s:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 55
    return-void
.end method

.method public constructor <init>(II[I[Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bm;Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/p;I[FI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    .line 380
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    .line 381
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    .line 382
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    .line 383
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 384
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 385
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 386
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 387
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 388
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 389
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 390
    if-eqz p3, :cond_0

    array-length v0, p3

    if-lez v0, :cond_0

    .line 391
    aget v0, p3, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    .line 395
    :goto_0
    iput p9, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    .line 396
    iput-object p10, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    .line 397
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    invoke-static {v0, p11}, Ljava/util/Arrays;->fill([II)V

    .line 399
    return-void

    .line 393
    :cond_0
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bg;)V
    .locals 1

    .prologue
    .line 1047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    .line 1049
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    .line 1050
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->b:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    .line 1051
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->c:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    .line 1052
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->d:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 1053
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->e:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 1054
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 1055
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->g:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 1056
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->h:Lcom/google/android/apps/gmm/map/internal/c/bm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 1057
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 1058
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->i:Lcom/google/android/apps/gmm/map/internal/c/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 1059
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->j:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    .line 1060
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->k:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    .line 1061
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->l:[F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    .line 1062
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bg;->m:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    .line 1063
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V
    .locals 1

    .prologue
    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    .line 347
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    .line 348
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->c:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    .line 349
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->d:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    .line 350
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->e:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 351
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 352
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 353
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->h:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 354
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->i:Lcom/google/android/apps/gmm/map/internal/c/bm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 355
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->j:Lcom/google/android/apps/gmm/map/internal/c/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 356
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->k:Lcom/google/android/apps/gmm/map/internal/c/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 357
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->l:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    .line 358
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->m:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    .line 359
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->n:[F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    .line 360
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bh;->o:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    .line 361
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/be;->s:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public static a(ILjava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x1

    const/4 v11, 0x0

    .line 148
    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    .line 152
    and-int v1, v2, v0

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    .line 153
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 154
    new-array v3, v4, [I

    move v1, v11

    .line 155
    :goto_1
    if-ge v1, v4, :cond_2

    .line 156
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v5

    aput v5, v3, v1

    .line 155
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v1, v11

    .line 152
    goto :goto_0

    :cond_1
    move-object v3, v8

    .line 162
    :cond_2
    const/4 v1, 0x2

    and-int/2addr v1, v2

    if-eqz v1, :cond_3

    move v1, v0

    :goto_2
    if-eqz v1, :cond_4

    .line 163
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 164
    new-array v4, v5, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    move v1, v11

    .line 165
    :goto_3
    if-ge v1, v5, :cond_5

    .line 166
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v6

    aput-object v6, v4, v1

    .line 165
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    move v1, v11

    .line 162
    goto :goto_2

    :cond_4
    move-object v4, v8

    .line 172
    :cond_5
    const/4 v1, 0x4

    and-int/2addr v1, v2

    if-eqz v1, :cond_8

    move v1, v0

    :goto_4
    if-eqz v1, :cond_10

    .line 173
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/c/bn;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bn;

    move-result-object v5

    .line 178
    :goto_5
    const/16 v1, 0x8

    and-int/2addr v1, v2

    if-eqz v1, :cond_9

    move v1, v0

    :goto_6
    if-eqz v1, :cond_f

    .line 179
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/map/internal/c/bm;->a(Ljava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/bm;

    move-result-object v6

    .line 184
    :goto_7
    const/16 v1, 0x10

    and-int/2addr v1, v2

    if-eqz v1, :cond_a

    move v1, v0

    :goto_8
    if-eqz v1, :cond_e

    .line 185
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v7

    .line 190
    :goto_9
    const/16 v1, 0x20

    and-int/2addr v1, v2

    if-eqz v1, :cond_b

    move v1, v0

    :goto_a
    if-eqz v1, :cond_6

    .line 191
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/map/internal/c/p;->a(Ljava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/p;

    move-result-object v8

    .line 196
    :cond_6
    const/16 v1, 0xc

    if-lt p2, v1, :cond_d

    const/16 v1, 0x40

    and-int/2addr v1, v2

    if-eqz v1, :cond_c

    :goto_b
    if-eqz v0, :cond_d

    .line 197
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v9

    .line 201
    :goto_c
    const/16 v0, 0xd

    if-lt p2, v0, :cond_7

    .line 202
    invoke-static {p1}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v11

    .line 205
    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/be;

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/c/be;->a:[F

    move v1, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(II[I[Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bm;Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/p;I[FI)V

    return-object v0

    :cond_8
    move v1, v11

    .line 172
    goto :goto_4

    :cond_9
    move v1, v11

    .line 178
    goto :goto_6

    :cond_a
    move v1, v11

    .line 184
    goto :goto_8

    :cond_b
    move v1, v11

    .line 190
    goto :goto_a

    :cond_c
    move v0, v11

    .line 196
    goto :goto_b

    :cond_d
    move v9, v11

    goto :goto_c

    :cond_e
    move-object v7, v8

    goto :goto_9

    :cond_f
    move-object v6, v8

    goto :goto_7

    :cond_10
    move-object v5, v8

    goto :goto_5
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/be;I)Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 2

    .prologue
    .line 410
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bh;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/bh;-><init>(Lcom/google/android/apps/gmm/map/internal/c/be;)V

    const/4 v1, 0x6

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->o:[I

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->o:[I

    invoke-static {v1, p1}, Ljava/util/Arrays;->fill([II)V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V

    return-object v1
.end method

.method private static a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 803
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804
    if-nez p1, :cond_0

    .line 805
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 819
    :goto_0
    return-void

    .line 808
    :cond_0
    const-string v0, "["

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    const/4 v0, 0x1

    .line 810
    array-length v3, p1

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    aget v4, p1, v2

    .line 811
    if-eqz v0, :cond_1

    move v0, v1

    .line 816
    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 810
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 814
    :cond_1
    const-string v5, ","

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 818
    :cond_2
    const-string v0, "]"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private a([ILcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bf;)V
    .locals 12

    .prologue
    .line 702
    const/4 v4, 0x0

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 704
    :goto_0
    const/4 v5, 0x0

    .line 705
    const/4 v3, 0x0

    .line 706
    const/4 v1, 0x0

    move v6, v1

    move v1, v5

    :goto_1
    if-gt v6, v0, :cond_4

    move v5, v1

    .line 707
    :goto_2
    array-length v1, p1

    if-ge v5, v1, :cond_1

    aget v1, p1, v5

    if-ne v1, v6, :cond_1

    .line 711
    add-int/lit8 v3, v3, 0x1

    .line 712
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_0

    .line 717
    :cond_1
    if-ge v6, v0, :cond_6

    .line 719
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v1, v1, v6

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v7, v2, v3

    .line 718
    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    iget v8, v7, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    ushr-int/lit8 v9, v2, 0x18

    ushr-int/lit8 v10, v8, 0x18

    sub-int/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    ushr-int/lit8 v10, v2, 0x10

    and-int/lit16 v10, v10, 0xff

    ushr-int/lit8 v11, v8, 0x10

    and-int/lit16 v11, v11, 0xff

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    add-int/2addr v9, v10

    ushr-int/lit8 v10, v2, 0x8

    and-int/lit16 v10, v10, 0xff

    ushr-int/lit8 v11, v8, 0x8

    and-int/lit16 v11, v11, 0xff

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    add-int/2addr v9, v10

    and-int/lit16 v2, v2, 0xff

    and-int/lit16 v8, v8, 0xff

    sub-int/2addr v2, v8

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/2addr v2, v9

    iget-object v8, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    if-eqz v8, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v1, v1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_3
    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    if-eqz v8, :cond_3

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v7, v7

    if-lez v7, :cond_3

    const/4 v7, 0x1

    :goto_4
    if-eq v1, v7, :cond_7

    add-int/lit16 v1, v2, 0x800

    :goto_5
    add-int v2, v4, v1

    .line 720
    add-int/lit8 v1, v3, 0x1

    .line 706
    :goto_6
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v4, v2

    move v3, v1

    move v1, v5

    goto :goto_1

    .line 718
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    const/4 v7, 0x0

    goto :goto_4

    .line 724
    :cond_4
    iget v0, p3, Lcom/google/android/apps/gmm/map/internal/c/bf;->b:I

    if-ge v4, v0, :cond_5

    iput v4, p3, Lcom/google/android/apps/gmm/map/internal/c/bf;->b:I

    const/4 v0, 0x0

    iget-object v1, p3, Lcom/google/android/apps/gmm/map/internal/c/bf;->a:[I

    const/4 v2, 0x0

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 725
    :cond_5
    return-void

    :cond_6
    move v1, v3

    move v2, v4

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_5
.end method

.method public static g()Lcom/google/android/apps/gmm/map/internal/c/bg;
    .locals 1

    .prologue
    .line 930
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bg;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/bg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/be;)Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 626
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v1, :cond_0

    move v6, v0

    .line 627
    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v1, :cond_1

    move v5, v0

    .line 628
    :goto_1
    if-lt v6, v5, :cond_2

    .line 691
    :goto_2
    return-object p0

    .line 626
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v1, v1

    move v6, v1

    goto :goto_0

    .line 627
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v1, v1

    move v5, v1

    goto :goto_1

    .line 635
    :cond_2
    sub-int v7, v5, v6

    .line 636
    new-array v8, v7, [I

    .line 637
    new-instance v10, Lcom/google/android/apps/gmm/map/internal/c/bf;

    invoke-direct {v10, v7}, Lcom/google/android/apps/gmm/map/internal/c/bf;-><init>(I)V

    move v4, v0

    .line 638
    :goto_3
    if-gt v4, v6, :cond_9

    .line 639
    aput v4, v8, v0

    .line 640
    if-le v7, v9, :cond_7

    move v3, v4

    .line 641
    :goto_4
    if-gt v3, v6, :cond_8

    .line 642
    aput v3, v8, v9

    .line 643
    if-le v7, v11, :cond_5

    move v2, v3

    .line 644
    :goto_5
    if-gt v2, v6, :cond_6

    .line 645
    aput v2, v8, v11

    .line 646
    if-le v7, v12, :cond_3

    move v1, v2

    .line 647
    :goto_6
    if-gt v1, v6, :cond_4

    .line 649
    aput v1, v8, v12

    .line 650
    invoke-direct {p0, v8, p1, v10}, Lcom/google/android/apps/gmm/map/internal/c/be;->a([ILcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bf;)V

    .line 648
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 653
    :cond_3
    invoke-direct {p0, v8, p1, v10}, Lcom/google/android/apps/gmm/map/internal/c/be;->a([ILcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bf;)V

    .line 644
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 657
    :cond_5
    invoke-direct {p0, v8, p1, v10}, Lcom/google/android/apps/gmm/map/internal/c/be;->a([ILcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bf;)V

    .line 641
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 661
    :cond_7
    invoke-direct {p0, v8, p1, v10}, Lcom/google/android/apps/gmm/map/internal/c/be;->a([ILcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bf;)V

    .line 638
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 664
    :cond_9
    iget v1, v10, Lcom/google/android/apps/gmm/map/internal/c/bf;->b:I

    const v2, 0x7fffffff

    if-ne v1, v2, :cond_a

    .line 665
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t find best insertion indices"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669
    :cond_a
    new-array v11, v5, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    move v9, v0

    move v1, v0

    .line 672
    :goto_7
    if-gt v9, v6, :cond_d

    move v7, v0

    move v8, v1

    .line 673
    :goto_8
    iget-object v0, v10, Lcom/google/android/apps/gmm/map/internal/c/bf;->a:[I

    array-length v0, v0

    if-ge v8, v0, :cond_b

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/internal/c/bf;->a:[I

    aget v0, v0, v8

    if-ne v0, v9, :cond_b

    .line 676
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v5, v0, v7

    .line 677
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget v1, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/be;->r:[I

    .line 678
    iget v4, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    aput-object v0, v11, v7

    .line 679
    add-int/lit8 v0, v7, 0x1

    .line 680
    add-int/lit8 v1, v8, 0x1

    move v7, v0

    move v8, v1

    .line 681
    goto :goto_8

    .line 684
    :cond_b
    if-ge v9, v6, :cond_c

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, v9

    aput-object v0, v11, v7

    .line 686
    add-int/lit8 v7, v7, 0x1

    .line 672
    :cond_c
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    move v1, v8

    move v0, v7

    goto :goto_7

    .line 691
    :cond_d
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bh;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/bh;-><init>(Lcom/google/android/apps/gmm/map/internal/c/be;)V

    iput-object v11, v0, Lcom/google/android/apps/gmm/map/internal/c/bh;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    new-instance p0, Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V

    goto/16 :goto_2
.end method

.method public final b()Lcom/google/android/apps/gmm/map/internal/c/bh;
    .locals 1

    .prologue
    .line 342
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bh;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/c/bh;-><init>(Lcom/google/android/apps/gmm/map/internal/c/be;)V

    return-object v0
.end method

.method public final c()F
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 417
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 418
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v3, :cond_0

    move v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    .line 419
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 418
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v3, v3

    goto :goto_1

    .line 421
    :cond_1
    return v2
.end method

.method public final d()F
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 428
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_0

    .line 435
    :goto_0
    return v0

    :cond_0
    move v2, v0

    move v0, v1

    .line 432
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v3, :cond_1

    move v3, v1

    :goto_2
    if-ge v0, v3, :cond_2

    .line 433
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 432
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v3, v3

    goto :goto_2

    :cond_2
    move v0, v2

    .line 435
    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 442
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    if-ge v0, v2, :cond_0

    .line 443
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    .line 444
    const/4 v1, 0x1

    .line 447
    :cond_0
    return v1

    .line 442
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 830
    if-ne p0, p1, :cond_1

    .line 901
    :cond_0
    :goto_0
    return v0

    .line 833
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 834
    goto :goto_0

    .line 836
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 837
    goto :goto_0

    .line 839
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 840
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_4

    .line 841
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v2, :cond_5

    move v0, v1

    .line 842
    goto :goto_0

    .line 844
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 845
    goto :goto_0

    .line 847
    :cond_5
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 848
    goto :goto_0

    .line 850
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 851
    goto :goto_0

    .line 853
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 854
    goto :goto_0

    .line 856
    :cond_8
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 857
    goto :goto_0

    .line 859
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 860
    goto :goto_0

    .line 862
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 863
    goto :goto_0

    .line 865
    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 866
    goto :goto_0

    .line 868
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    if-nez v2, :cond_d

    .line 869
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    if-eqz v2, :cond_e

    move v0, v1

    .line 870
    goto :goto_0

    .line 872
    :cond_d
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 873
    goto/16 :goto_0

    .line 875
    :cond_e
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-nez v2, :cond_f

    .line 876
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v2, :cond_10

    move v0, v1

    .line 877
    goto/16 :goto_0

    .line 879
    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 880
    goto/16 :goto_0

    .line 882
    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 883
    goto/16 :goto_0

    .line 885
    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v2, :cond_12

    .line 886
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v2, :cond_13

    move v0, v1

    .line 887
    goto/16 :goto_0

    .line 889
    :cond_12
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 890
    goto/16 :goto_0

    .line 892
    :cond_13
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 893
    goto/16 :goto_0

    .line 895
    :cond_14
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 896
    goto/16 :goto_0

    .line 898
    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 899
    goto/16 :goto_0
.end method

.method public final f()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 905
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    if-nez v2, :cond_1

    move v2, v1

    :goto_1
    add-int v4, v0, v2

    .line 909
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v0, :cond_2

    .line 910
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v6, v5

    move v2, v1

    move v0, v1

    :goto_2
    if-ge v2, v6, :cond_3

    aget-object v3, v5, v2

    .line 911
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v3, v3

    shl-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x18

    add-int/2addr v3, v0

    .line 910
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_2

    .line 905
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    array-length v2, v2

    shl-int/lit8 v2, v2, 0x2

    goto :goto_1

    :cond_2
    move v0, v1

    .line 914
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v2, :cond_4

    .line 915
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v6, v5

    move v2, v1

    :goto_3
    if-ge v2, v6, :cond_4

    aget-object v3, v5, v2

    .line 916
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v3, v3

    shl-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x18

    add-int/2addr v3, v0

    .line 915
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_3

    .line 919
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v2, :cond_5

    .line 920
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v6, v5

    move v2, v1

    :goto_4
    if-ge v2, v6, :cond_5

    aget-object v3, v5, v2

    .line 921
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v3, v3

    shl-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x18

    add-int/2addr v3, v0

    .line 920
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_4

    .line 924
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_6

    .line 925
    :goto_5
    add-int/lit8 v2, v4, 0x54

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    .line 924
    :cond_6
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    array-length v1, v1

    shl-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x18

    goto :goto_5
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 751
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    add-int/lit8 v0, v0, 0x1f

    .line 754
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    add-int/2addr v0, v2

    .line 755
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 756
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 757
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 758
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 759
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 760
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 761
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 762
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 763
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 764
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    add-int/2addr v0, v1

    .line 765
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    add-int/2addr v0, v1

    .line 766
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([F)I

    move-result v1

    add-int/2addr v0, v1

    .line 767
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 768
    return v0

    .line 760
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bn;->hashCode()I

    move-result v0

    goto :goto_0

    .line 761
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bm;->hashCode()I

    move-result v0

    goto :goto_1

    .line 762
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bd;->hashCode()I

    move-result v0

    goto :goto_2

    .line 763
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/p;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 773
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 774
    const-string v0, "Style{id="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 775
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 776
    const-string v0, ", areaFillColors"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->d:[I

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V

    .line 777
    const-string v0, ", volumeFillColors"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(Ljava/lang/String;[ILjava/lang/StringBuilder;)V

    .line 778
    const-string v0, ", components="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->c:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", areaStrokes="

    .line 779
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", lineStrokes="

    .line 780
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", volumeStrokes="

    .line 781
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", textStyle="

    .line 783
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", textBoxStyle="

    .line 784
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", arrowStyle="

    .line 785
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->k:Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", icon="

    .line 786
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", waterColor="

    .line 787
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", blurColor="

    .line 788
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", rasterColorFilter="

    .line 789
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    if-nez v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", zPlanes="

    .line 791
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    .line 792
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 793
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 780
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_1

    .line 781
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->h:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 782
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_2

    .line 789
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->o:[F

    .line 790
    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method
