.class public Lcom/google/android/apps/gmm/map/internal/c/aa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/p;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final e:Lcom/google/android/apps/gmm/map/internal/c/bi;

.field final f:Ljava/lang/String;

.field public final g:F

.field private final h:I


# direct methods
.method public constructor <init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V
    .locals 0

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    .line 334
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 335
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    .line 336
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 337
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->e:Lcom/google/android/apps/gmm/map/internal/c/bi;

    .line 338
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->h:I

    .line 339
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    .line 340
    iput p8, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->g:F

    .line 341
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/p;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 304
    const/4 v1, 0x1

    const/4 v6, -0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    .line 312
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/p;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/gmm/map/internal/c/p;-><init>(Ljava/lang/String;II)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Lcom/google/android/apps/gmm/map/internal/c/p;)V

    .line 301
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 315
    const/4 v1, 0x2

    const/4 v6, -0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    .line 323
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 636
    if-ne p0, p1, :cond_1

    .line 656
    :cond_0
    :goto_0
    return v0

    .line 639
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 640
    goto :goto_0

    .line 642
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 643
    goto :goto_0

    .line 645
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 646
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 647
    goto :goto_0

    .line 649
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/aa;->g:F

    .line 650
    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 651
    goto :goto_0

    .line 655
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_b

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_6
    move v2, v0

    :goto_1
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_c

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_7
    move v2, v0

    :goto_2
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    .line 656
    if-eq v2, v3, :cond_8

    if-eqz v2, :cond_d

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_8
    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-eq v2, v3, :cond_9

    if-eqz v2, :cond_e

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_9
    move v2, v0

    :goto_4
    if-nez v2, :cond_0

    :cond_a
    move v0, v1

    goto :goto_0

    :cond_b
    move v2, v1

    .line 655
    goto :goto_1

    :cond_c
    move v2, v1

    goto :goto_2

    :cond_d
    move v2, v1

    .line 656
    goto :goto_3

    :cond_e
    move v2, v1

    goto :goto_4
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 623
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 626
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 627
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 628
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 629
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 630
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 631
    return v0

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/p;->hashCode()I

    move-result v0

    goto :goto_0

    .line 628
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/be;->hashCode()I

    move-result v0

    goto :goto_1

    .line 629
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 630
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 667
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "components"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    .line 668
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "icon"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 669
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    .line 670
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "style"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 671
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "styleIdIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->h:I

    .line 672
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "styleId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    .line 673
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "horizontalPadding"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/aa;->g:F

    .line 674
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 675
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
