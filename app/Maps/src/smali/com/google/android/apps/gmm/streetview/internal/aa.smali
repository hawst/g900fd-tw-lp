.class public Lcom/google/android/apps/gmm/streetview/internal/aa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/streetview/internal/aa;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:I

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/ab;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/internal/ab;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    .line 44
    invoke-static {p1}, Lcom/google/android/apps/gmm/streetview/internal/ao;->i(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->b:I

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    .line 46
    iput p3, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->d:I

    .line 47
    iput-object p4, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->b:I

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->d:I

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/yd;)V
    .locals 5

    .prologue
    .line 63
    iget v0, p1, Lcom/google/r/b/a/yd;->b:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/streetview/internal/t;->a(I)F

    move-result v3

    iget-object v0, p1, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 64
    :cond_0
    :goto_0
    iget v4, p1, Lcom/google/r/b/a/yd;->d:I

    iget-object v0, p1, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    .line 63
    :goto_1
    invoke-direct {p0, v3, v1, v4, v0}, Lcom/google/android/apps/gmm/streetview/internal/aa;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    .line 67
    return-void

    .line 63
    :cond_1
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v1, p1, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    goto :goto_0

    .line 64
    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v2, p1, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    if-ne p0, p1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 75
    goto :goto_0

    .line 77
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/streetview/internal/aa;

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    .line 79
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/streetview/internal/aa;->d:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    .line 81
    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    .line 82
    invoke-static {v3}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 95
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    return-void
.end method
