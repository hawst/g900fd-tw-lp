.class public abstract Lcom/google/android/apps/gmm/base/l/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/ai;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public b:Ljava/lang/Runnable;

.field public c:Ljava/lang/Runnable;

.field public d:Ljava/lang/String;

.field public e:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;"
        }
    .end annotation
.end field

.field f:Z

.field public g:Lcom/google/android/apps/gmm/base/l/c;

.field private h:Z

.field private final i:Lcom/google/android/apps/gmm/z/b/l;

.field private final j:Lcom/google/android/apps/gmm/search/g;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/b;->h:Z

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->k:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/b;->f:Z

    .line 81
    sget-object v0, Lcom/google/android/apps/gmm/base/l/c;->a:Lcom/google/android/apps/gmm/base/l/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 47
    sget-object v0, Lcom/google/b/f/t;->cy:Lcom/google/b/f/t;

    .line 48
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->i:Lcom/google/android/apps/gmm/z/b/l;

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/search/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/search/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->j:Lcom/google/android/apps/gmm/search/g;

    .line 61
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    .line 187
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 173
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 174
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/b;->h:Z

    if-eq v0, p1, :cond_0

    .line 293
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/l/b;->h:Z

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 296
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/b;->c:Ljava/lang/Runnable;

    .line 301
    return-void
.end method

.method public d()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->O()Lcom/google/android/apps/gmm/aa/c/a/a;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/aa/c/a/a;->a(ZZ)V

    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v3, Lcom/google/android/apps/gmm/base/l/c;->a:Lcom/google/android/apps/gmm/base/l/c;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public h()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 202
    return-object v2
.end method

.method public i()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public j()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->i:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->a:Lcom/google/android/apps/gmm/base/l/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->b:Lcom/google/android/apps/gmm/base/l/c;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->c:Lcom/google/android/apps/gmm/base/l/c;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/b;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->b:Lcom/google/android/apps/gmm/base/l/c;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/l/b;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->a:Lcom/google/android/apps/gmm/base/l/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->c:Lcom/google/android/apps/gmm/base/l/c;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Ljava/lang/Integer;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1
.end method

.method public q()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public t()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 263
    const v0, 0x2000003

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/b;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic v()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    return-object v0
.end method
