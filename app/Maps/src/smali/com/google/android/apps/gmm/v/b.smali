.class public Lcom/google/android/apps/gmm/v/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field final d:I

.field final e:I

.field final f:I

.field g:Z

.field private h:[I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/b;->h:[I

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/b;->g:Z

    .line 213
    iput p1, p0, Lcom/google/android/apps/gmm/v/b;->a:I

    .line 214
    iput p2, p0, Lcom/google/android/apps/gmm/v/b;->b:I

    .line 215
    iput p3, p0, Lcom/google/android/apps/gmm/v/b;->c:I

    .line 216
    iput p4, p0, Lcom/google/android/apps/gmm/v/b;->d:I

    .line 217
    iput p5, p0, Lcom/google/android/apps/gmm/v/b;->e:I

    .line 218
    iput p6, p0, Lcom/google/android/apps/gmm/v/b;->f:I

    .line 219
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/b;)V
    .locals 7

    .prologue
    .line 226
    iget v1, p1, Lcom/google/android/apps/gmm/v/b;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/v/b;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/v/b;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/v/b;->d:I

    iget v5, p1, Lcom/google/android/apps/gmm/v/b;->e:I

    iget v6, p1, Lcom/google/android/apps/gmm/v/b;->f:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/v/b;-><init>(IIIIII)V

    .line 228
    return-void
.end method


# virtual methods
.method final a()[I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x4

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/b;->h:[I

    if-nez v0, :cond_0

    .line 243
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/b;->g:Z

    if-eqz v0, :cond_1

    .line 244
    const/16 v0, 0xf

    new-array v0, v0, [I

    const/16 v1, 0x3024

    aput v1, v0, v2

    iget v1, p0, Lcom/google/android/apps/gmm/v/b;->a:I

    aput v1, v0, v4

    const/16 v1, 0x3023

    aput v1, v0, v5

    iget v1, p0, Lcom/google/android/apps/gmm/v/b;->b:I

    aput v1, v0, v6

    const/16 v1, 0x3022

    aput v1, v0, v3

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->c:I

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->d:I

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->e:I

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->f:I

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3040

    aput v2, v0, v1

    const/16 v1, 0xd

    aput v3, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x3038

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/b;->h:[I

    .line 266
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/b;->h:[I

    return-object v0

    .line 255
    :cond_1
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/16 v1, 0x3024

    aput v1, v0, v2

    iget v1, p0, Lcom/google/android/apps/gmm/v/b;->a:I

    aput v1, v0, v4

    const/16 v1, 0x3023

    aput v1, v0, v5

    iget v1, p0, Lcom/google/android/apps/gmm/v/b;->b:I

    aput v1, v0, v6

    const/16 v1, 0x3022

    aput v1, v0, v3

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->c:I

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->d:I

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->e:I

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/apps/gmm/v/b;->f:I

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3038

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/b;->h:[I

    goto :goto_0
.end method
