.class Lcom/google/android/apps/gmm/map/o/o;
.super Lcom/google/android/apps/gmm/map/util/a/h;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/util/a/h",
        "<",
        "Lcom/google/android/apps/gmm/map/o/d;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(ILcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 1

    .prologue
    .line 15
    const-string v0, "GLLabelCache"

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/map/util/a/h;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    .line 16
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(JI)Lcom/google/android/apps/gmm/map/o/d;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/map/util/a/h;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    invoke-interface {v0, p3}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    monitor-exit p0

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic a(J)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 13
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final synthetic a(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 13
    check-cast p3, Lcom/google/android/apps/gmm/map/o/d;

    const/4 v0, 0x4

    invoke-interface {p3, v0}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 2

    .prologue
    .line 28
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v0

    int-to-long v0, v0

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/h;->a(J)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 30
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v0

    int-to-long v0, v0

    invoke-super {p0, v0, v1, p1}, Lcom/google/android/apps/gmm/map/util/a/h;->b(JLjava/lang/Object;)V

    .line 32
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/map/o/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 65
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v0

    int-to-long v0, v0

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/map/util/a/h;->c(J)Ljava/lang/Object;

    .line 67
    :cond_0
    return-void
.end method
