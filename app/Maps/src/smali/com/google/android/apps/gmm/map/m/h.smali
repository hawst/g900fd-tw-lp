.class Lcom/google/android/apps/gmm/map/m/h;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/m/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/m/g;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    .line 307
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 308
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/m/g;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    .line 311
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 312
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 316
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 333
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/i;->e(Landroid/view/MotionEvent;)V

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 322
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/m/g;->n:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/m/i;->h(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 327
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/g;->m:Z

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/h;->a:Lcom/google/android/apps/gmm/map/m/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
