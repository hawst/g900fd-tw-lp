.class public Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;
.super Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    sget v0, Lcom/google/android/apps/gmm/h;->at:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->addPreferencesFromResource(I)V

    .line 83
    const-string v0, "terms"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->nu:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->a(Lcom/google/android/apps/gmm/shared/b/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "krterm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->hx:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 96
    :goto_0
    const-string v0, "privacy"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->kF:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 99
    const-string v0, "notices"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->hI:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 102
    const-string v0, "open_source"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->jH:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 105
    const-string v0, "web_history"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->oZ:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->nt:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 110
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/h;->ak:I

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 93
    :cond_0
    const-string v0, "krterm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 111
    :cond_1
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 180
    :goto_0
    return v0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 126
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 127
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 128
    const-string v5, "terms"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 129
    sget-object v1, Lcom/google/b/f/t;->eM:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 131
    invoke-static {v4}, Lcom/google/android/apps/gmm/util/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 134
    goto :goto_0

    .line 137
    :cond_1
    const-string v5, "krterm"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    const-string v1, "http://www.google.com/intl/ko/policies/terms/location/"

    .line 139
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 138
    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 142
    goto :goto_0

    .line 145
    :cond_2
    const-string v5, "privacy"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 146
    sget-object v1, Lcom/google/b/f/t;->eI:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 148
    invoke-static {v4}, Lcom/google/android/apps/gmm/util/q;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 151
    goto :goto_0

    .line 154
    :cond_3
    const-string v5, "notices"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 155
    sget-object v1, Lcom/google/b/f/t;->eF:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 160
    invoke-static {v1}, Lcom/google/android/apps/gmm/util/q;->b(Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 158
    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 162
    goto/16 :goto_0

    .line 165
    :cond_4
    const-string v5, "open_source"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 166
    sget-object v1, Lcom/google/b/f/t;->eH:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;-><init>()V

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    move v0, v2

    .line 169
    goto/16 :goto_0

    .line 172
    :cond_5
    const-string v5, "web_history"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 173
    sget-object v1, Lcom/google/b/f/t;->eS:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 175
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    const-string v0, "http://www.google.com/history?hl="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 175
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;->startActivity(Landroid/content/Intent;)V

    move v0, v2

    .line 177
    goto/16 :goto_0

    .line 175
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    move v0, v1

    .line 180
    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 115
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onResume()V

    .line 116
    sget v0, Lcom/google/android/apps/gmm/l;->O:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    return-void
.end method
