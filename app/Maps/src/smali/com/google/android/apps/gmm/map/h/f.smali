.class public final enum Lcom/google/android/apps/gmm/map/h/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/h/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/h/f;

.field public static final enum b:Lcom/google/android/apps/gmm/map/h/f;

.field public static final enum c:Lcom/google/android/apps/gmm/map/h/f;

.field public static final enum d:Lcom/google/android/apps/gmm/map/h/f;

.field private static g:Ljava/lang/Boolean;

.field private static h:Lcom/google/android/apps/gmm/map/h/f;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/map/h/f;


# instance fields
.field public e:Z

.field public f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/h/f;

    const-string v1, "PHONE_PORTRAIT"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/apps/gmm/map/h/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->a:Lcom/google/android/apps/gmm/map/h/f;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/map/h/f;

    const-string v1, "PHONE_LANDSCAPE"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/apps/gmm/map/h/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->b:Lcom/google/android/apps/gmm/map/h/f;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/h/f;

    const-string v1, "TABLET_PORTRAIT"

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/google/android/apps/gmm/map/h/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->c:Lcom/google/android/apps/gmm/map/h/f;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/map/h/f;

    const-string v1, "TABLET_LANDSCAPE"

    invoke-direct {v0, v1, v5, v3, v3}, Lcom/google/android/apps/gmm/map/h/f;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->d:Lcom/google/android/apps/gmm/map/h/f;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/h/f;

    sget-object v1, Lcom/google/android/apps/gmm/map/h/f;->a:Lcom/google/android/apps/gmm/map/h/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/h/f;->b:Lcom/google/android/apps/gmm/map/h/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/h/f;->c:Lcom/google/android/apps/gmm/map/h/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/h/f;->d:Lcom/google/android/apps/gmm/map/h/f;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->i:[Lcom/google/android/apps/gmm/map/h/f;

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->h:Lcom/google/android/apps/gmm/map/h/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    .line 46
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 114
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/res/Configuration;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v1

    .line 115
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v2, :cond_0

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-eqz v1, :cond_0

    .line 117
    const/16 v1, 0x258

    if-lt p1, v1, :cond_0

    const/4 v0, 0x2

    .line 119
    :cond_0
    return v0
.end method

.method public static a(Landroid/content/res/Configuration;)Lcom/google/android/apps/gmm/map/h/f;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    iget v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x258

    if-lt v0, v3, :cond_0

    move v0, v1

    .line 101
    :goto_0
    iget v3, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    iget v4, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    if-le v3, v4, :cond_1

    .line 103
    :goto_1
    invoke-static {}, Lcom/google/android/apps/gmm/map/h/f;->values()[Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v3

    array-length v4, v3

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 104
    iget-boolean v6, v5, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-ne v6, v1, :cond_2

    iget-boolean v6, v5, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-ne v6, v0, :cond_2

    .line 105
    return-object v5

    :cond_0
    move v0, v2

    .line 96
    goto :goto_0

    :cond_1
    move v1, v2

    .line 101
    goto :goto_1

    .line 103
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 109
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/apps/gmm/map/h/f;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/res/Configuration;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->g:Ljava/lang/Boolean;

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/h/f;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/map/h/f;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/res/Configuration;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/h/f;->g:Ljava/lang/Boolean;

    .line 84
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/h/f;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/res/Configuration;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/h/f;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/gmm/map/h/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/h/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/h/f;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/apps/gmm/map/h/f;->i:[Lcom/google/android/apps/gmm/map/h/f;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/h/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/h/f;

    return-object v0
.end method
