.class public Lcom/google/android/apps/gmm/navigation/navui/ba;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/common/api/q;
.implements Lcom/google/android/gms/common/api/r;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/c/a;

.field public final b:Lcom/google/android/gms/common/api/o;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/gmm/navigation/util/b;

.field private final e:Lcom/google/android/gms/wearable/o;

.field private f:Lcom/google/android/apps/gmm/map/r/a/ag;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 117
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->c:Landroid/content/Context;

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->a:Lcom/google/android/apps/gmm/map/c/a;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/navigation/util/b;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/navigation/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 120
    const-string v0, "/guidance"

    invoke-static {v0}, Lcom/google/android/gms/wearable/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wearable/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->e:Lcom/google/android/gms/wearable/o;

    .line 122
    new-instance v0, Lcom/google/android/gms/common/api/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    .line 123
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 124
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wearable/p;->d:Lcom/google/android/gms/common/api/a;

    .line 125
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/g;->a(Landroid/content/Context;)I

    move-result v0

    .line 128
    if-nez v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_1
    const-string v1, "GMMWearableHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Google Play Services unavailable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 325
    const-string v0, "GMMWearableHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConnectionSuspended: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 320
    const-string v0, "GMMWearableHelper"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onConnected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->e:Lcom/google/android/gms/wearable/o;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/o;->b()Lcom/google/android/gms/wearable/h;

    move-result-object v2

    .line 259
    const-string v3, "is_night"

    iget-boolean v4, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Z)V

    .line 261
    const-string v2, "GMMWearableHelper"

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x19

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Night mode changed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->h:Z

    .line 264
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->g:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->h:Z

    if-eqz v2, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    .line 265
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    sget-object v0, Lcom/google/android/gms/wearable/p;->a:Lcom/google/android/gms/wearable/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->e:Lcom/google/android/gms/wearable/o;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/o;->c()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wearable/a;->a(Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/s;

    .line 269
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 264
    goto :goto_0

    .line 267
    :cond_1
    const-string v0, "GMMWearableHelper"

    const-string v2, "Not updating data item yet"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 14
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/16 v3, 0x34

    const-wide/16 v12, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 164
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_2

    move v0, v8

    :goto_1
    if-nez v0, :cond_3

    .line 165
    :cond_1
    iput-boolean v9, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->g:Z

    .line 166
    invoke-virtual {p0, v9}, Lcom/google/android/apps/gmm/navigation/navui/ba;->a(Z)V

    goto :goto_0

    :cond_2
    move v0, v9

    .line 164
    goto :goto_1

    .line 170
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 171
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 172
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/navigation/util/b;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)V

    .line 173
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->e:Lcom/google/android/gms/wearable/o;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/o;->b()Lcom/google/android/gms/wearable/h;

    move-result-object v10

    .line 178
    const-string v5, "title"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->h:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_a

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v5, "text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 180
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_c

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 179
    :goto_3
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v5, "current_step"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 182
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->c:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_e

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 181
    :goto_4
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v5, "distance_to_next_step"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 184
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->d:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_10

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 183
    :goto_5
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v5, "duration_and_distance"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 186
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->e:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_12

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 185
    :goto_6
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v5, "eta"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 188
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->f:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_14

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 187
    :goto_7
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v5, "distance"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 190
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->b:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_16

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 189
    :goto_8
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v5, "abbreviated_text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 192
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->i:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_18

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 191
    :goto_9
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v5, "primary_cue"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 194
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/util/b;->i:Ljava/lang/CharSequence;

    instance-of v6, v2, Landroid/text/Spanned;

    if-eqz v6, :cond_1a

    check-cast v2, Landroid/text/Spanned;

    invoke-static {v2}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v2

    .line 193
    :goto_a
    invoke-virtual {v10, v5, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v5, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v5, v2

    iget-object v5, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 199
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v2, :cond_1c

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v2, v2

    if-le v2, v8, :cond_1c

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v2, v2, v8

    :goto_b
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    .line 200
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 201
    const-string v6, "query"

    invoke-virtual {v10, v6, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_6
    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v2, :cond_1d

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v2, v2

    if-le v2, v8, :cond_1d

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v2, v2, v8

    :goto_c
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 204
    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    cmpl-double v5, v6, v12

    if-nez v5, :cond_7

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpl-double v5, v6, v12

    if-eqz v5, :cond_8

    .line 205
    :cond_7
    const-string v5, "latitude"

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-virtual {v10, v5, v6, v7}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;D)V

    .line 206
    const-string v5, "longitude"

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v10, v5, v6, v7}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;D)V

    .line 208
    :cond_8
    const-string v2, "travel_mode"

    .line 209
    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v5, v5, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v5, v6, v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iget v5, v5, Lcom/google/maps/g/a/hm;->h:I

    .line 208
    invoke-virtual {v10, v2, v5}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;I)V

    .line 211
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/b;->c:Z

    if-eqz v2, :cond_1e

    .line 212
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-nez v2, :cond_1e

    .line 213
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v0

    if-nez v0, :cond_1e

    if-eqz v1, :cond_1e

    move v0, v8

    .line 215
    :goto_d
    const-string v2, "on_route"

    invoke-virtual {v10, v2, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Z)V

    .line 216
    if-eqz v0, :cond_1f

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->f:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v0, v1, :cond_9

    .line 220
    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->f:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->c:Landroid/content/Context;

    .line 223
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/google/android/apps/gmm/d;->aM:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x64

    move v4, v3

    .line 221
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/directions/views/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ag;IIILandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/Asset;->a([B)Lcom/google/android/gms/wearable/Asset;

    move-result-object v0

    .line 226
    const-string v2, "icon"

    invoke-virtual {v10, v2, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/Asset;)V

    .line 227
    const-string v0, "maneuver_type"

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    iget v2, v2, Lcom/google/maps/g/a/ez;->F:I

    invoke-virtual {v10, v0, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;I)V

    .line 228
    const-string v0, "turn_type"

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    iget v2, v2, Lcom/google/maps/g/a/fd;->j:I

    invoke-virtual {v10, v0, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;I)V

    .line 229
    const-string v0, "turn_side"

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    iget v2, v2, Lcom/google/maps/g/a/fb;->d:I

    invoke-virtual {v10, v0, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;I)V

    .line 230
    const-string v0, "step_number"

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    invoke-virtual {v10, v0, v1}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;I)V

    .line 241
    :cond_9
    :goto_e
    const-string v0, "GMMWearableHelper"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->d:Lcom/google/android/apps/gmm/navigation/util/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Nav state changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    iput-boolean v8, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->g:Z

    .line 244
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->g:Z

    if-eqz v0, :cond_20

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->h:Z

    if-eqz v0, :cond_20

    :goto_f
    if-eqz v8, :cond_21

    .line 245
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    sget-object v0, Lcom/google/android/gms/wearable/p;->a:Lcom/google/android/gms/wearable/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->e:Lcom/google/android/gms/wearable/o;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/o;->c()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wearable/a;->a(Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/s;

    goto/16 :goto_0

    .line 178
    :cond_a
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_b

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_b
    const-string v2, ""

    goto/16 :goto_2

    .line 180
    :cond_c
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_d

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_d
    const-string v2, ""

    goto/16 :goto_3

    .line 182
    :cond_e
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_f

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_f
    const-string v2, ""

    goto/16 :goto_4

    .line 184
    :cond_10
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_11

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    :cond_11
    const-string v2, ""

    goto/16 :goto_5

    .line 186
    :cond_12
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_13

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    :cond_13
    const-string v2, ""

    goto/16 :goto_6

    .line 188
    :cond_14
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_15

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    :cond_15
    const-string v2, ""

    goto/16 :goto_7

    .line 190
    :cond_16
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_17

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_17
    const-string v2, ""

    goto/16 :goto_8

    .line 192
    :cond_18
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_19

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    :cond_19
    const-string v2, ""

    goto/16 :goto_9

    .line 194
    :cond_1a
    instance-of v6, v2, Ljava/lang/String;

    if-eqz v6, :cond_1b

    invoke-static {v2}, Landroid/text/Html;->escapeHtml(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a

    :cond_1b
    const-string v2, ""

    goto/16 :goto_a

    :cond_1c
    move-object v2, v4

    .line 199
    goto/16 :goto_b

    :cond_1d
    move-object v2, v4

    .line 203
    goto/16 :goto_c

    :cond_1e
    move v0, v9

    .line 213
    goto/16 :goto_d

    .line 233
    :cond_1f
    iput-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->f:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 234
    const-string v0, "icon"

    invoke-virtual {v10, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    .line 235
    const-string v0, "maneuver_type"

    invoke-virtual {v10, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    .line 236
    const-string v0, "turn_type"

    invoke-virtual {v10, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    .line 237
    const-string v0, "turn_side"

    invoke-virtual {v10, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    .line 238
    const-string v0, "step_number"

    invoke-virtual {v10, v0}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    goto/16 :goto_e

    :cond_20
    move v8, v9

    .line 244
    goto/16 :goto_f

    .line 247
    :cond_21
    const-string v0, "GMMWearableHelper"

    const-string v1, "Not updating data item yet"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 4

    .prologue
    .line 331
    const-string v0, "GMMWearableHelper"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onConnectionFailed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    return-void
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 288
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 289
    sget-object v0, Lcom/google/android/gms/wearable/p;->a:Lcom/google/android/gms/wearable/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ba;->e:Lcom/google/android/gms/wearable/o;

    .line 290
    invoke-virtual {v2}, Lcom/google/android/gms/wearable/o;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wearable/a;->a(Lcom/google/android/gms/common/api/o;Landroid/net/Uri;)Lcom/google/android/gms/common/api/s;

    move-result-object v0

    .line 291
    if-eqz p1, :cond_0

    .line 292
    invoke-interface {v0}, Lcom/google/android/gms/common/api/s;->a()Lcom/google/android/gms/common/api/u;

    .line 294
    :cond_0
    return-void
.end method
