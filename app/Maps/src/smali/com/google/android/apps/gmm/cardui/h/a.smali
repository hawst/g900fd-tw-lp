.class public Lcom/google/android/apps/gmm/cardui/h/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/b;


# instance fields
.field public a:Lcom/google/android/apps/gmm/util/b/h;

.field public b:Z

.field public c:Lcom/google/android/apps/gmm/cardui/g/c;

.field private d:Lcom/google/android/libraries/curvular/ce;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/util/b/h;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->a:Lcom/google/android/apps/gmm/util/b/h;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Lcom/google/android/libraries/curvular/ce;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->d:Lcom/google/android/libraries/curvular/ce;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->c:Lcom/google/android/apps/gmm/cardui/g/c;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->c:Lcom/google/android/apps/gmm/cardui/g/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/cardui/g/c;->c()V

    .line 62
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->a:Lcom/google/android/apps/gmm/util/b/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/h;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->a:Lcom/google/android/apps/gmm/util/b/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/h;->a()Ljava/util/List;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/q;->b()Lcom/google/o/h/a/bw;

    move-result-object v0

    sget-object v3, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/ce;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/cardui/h/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/cardui/h/b;-><init>(Lcom/google/android/apps/gmm/cardui/h/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->d:Lcom/google/android/libraries/curvular/ce;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/a;->d:Lcom/google/android/libraries/curvular/ce;

    return-object v0
.end method
