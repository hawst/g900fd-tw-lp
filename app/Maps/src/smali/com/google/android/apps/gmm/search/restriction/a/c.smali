.class public Lcom/google/android/apps/gmm/search/restriction/a/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/a/b;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/search/restriction/c/k;

.field public b:Lcom/google/maps/g/qm;

.field public c:Lcom/google/r/b/a/alh;

.field public d:Z

.field private final e:Lcom/google/android/apps/gmm/search/e/c;

.field private final f:Lcom/google/android/apps/gmm/base/activities/c;

.field private final g:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;Lcom/google/android/apps/gmm/search/e/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->d:Z

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->f:Lcom/google/android/apps/gmm/base/activities/c;

    .line 42
    iput-object p5, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->g:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    .line 47
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v0

    if-eq p3, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    :cond_0
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->e:Lcom/google/android/apps/gmm/search/e/c;

    .line 50
    new-instance v1, Lcom/google/android/apps/gmm/search/restriction/c/k;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    invoke-direct {v1, p1, v0, p2, v2}, Lcom/google/android/apps/gmm/search/restriction/c/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    .line 52
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/search/restriction/d/b;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->f:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->g:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    :goto_0
    return-object v5

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    .line 139
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;->a(Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)Lcom/google/android/apps/gmm/search/restriction/configuration/RestrictionConfigurationFragment;

    move-result-object v1

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->g:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->h:Ljava/lang/String;

    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "You should use GmmActivityFragment#show(GmmActivity), instead."

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 103
    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 104
    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 105
    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 106
    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    move v0, v2

    :goto_4
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 107
    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_5
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    .line 108
    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_7

    move v0, v2

    :goto_6
    if-eqz v0, :cond_8

    :cond_0
    move v0, v2

    .line 109
    :goto_7
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    iget-object v3, v3, Lcom/google/maps/g/qm;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_9

    :goto_8
    or-int/2addr v0, v2

    .line 112
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    .line 102
    goto :goto_0

    :cond_2
    move v0, v1

    .line 103
    goto :goto_1

    :cond_3
    move v0, v1

    .line 104
    goto :goto_2

    :cond_4
    move v0, v1

    .line 105
    goto :goto_3

    :cond_5
    move v0, v1

    .line 106
    goto :goto_4

    :cond_6
    move v0, v1

    .line 107
    goto :goto_5

    :cond_7
    move v0, v1

    .line 108
    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_7

    :cond_9
    move v2, v1

    .line 109
    goto :goto_8
.end method

.method public final f()Lcom/google/android/apps/gmm/search/e/c;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/a/c;->e:Lcom/google/android/apps/gmm/search/e/c;

    return-object v0
.end method
