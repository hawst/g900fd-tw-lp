.class public Lcom/google/android/apps/gmm/settings/b;
.super Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field b:Z

.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;-><init>()V

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/settings/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/settings/c;-><init>(Lcom/google/android/apps/gmm/settings/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/settings/b;->c:Ljava/lang/Object;

    return-void
.end method

.method public static b()Lcom/google/android/apps/gmm/settings/b;
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/settings/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/settings/b;-><init>()V

    .line 38
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 39
    const-string v2, "allowSideInfoSheet"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    const-string v2, "isNavigating"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 41
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/settings/b;->setArguments(Landroid/os/Bundle;)V

    .line 42
    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 85
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->iN:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/b;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 53
    .line 54
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/b;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    const-string v2, "settings_preference"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 56
    sget v1, Lcom/google/android/apps/gmm/h;->au:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/b;->addPreferencesFromResource(I)V

    .line 58
    if-nez p1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/b;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    .line 61
    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "isNavigating"

    .line 62
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/settings/b;->b:Z

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/b;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 67
    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->n:Z

    if-nez v1, :cond_2

    .line 68
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aO:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 71
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/settings/b;->a(Landroid/preference/PreferenceScreen;)V

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 78
    sget v0, Lcom/google/android/apps/gmm/l;->iN:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/b;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aO:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 80
    :cond_0
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onPause()V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/b;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/b;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 101
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onResume()V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/b;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/settings/b;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 94
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 106
    const-string v0, "isNavigating"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/settings/b;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/b;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 115
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aO:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aO:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/b;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->e()V

    goto :goto_0

    .line 119
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->ai:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0
.end method
