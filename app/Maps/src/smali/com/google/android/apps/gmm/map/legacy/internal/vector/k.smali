.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;
.super Lcom/google/android/apps/gmm/map/m/j;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/m/q;


# static fields
.field private static final d:D


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/m/ac;

.field private final b:F

.field private volatile c:Z

.field private final e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

.field private final f:Lcom/google/android/apps/gmm/map/q/b;

.field private g:F

.field private h:F

.field private i:F

.field private j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 174
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->d:D

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;Lcom/google/android/apps/gmm/map/q/b;F)V
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/j;-><init>()V

    .line 203
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    .line 227
    new-instance v0, Lcom/google/android/apps/gmm/map/m/ac;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/m/ac;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    .line 231
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    .line 232
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    .line 233
    const/16 v0, 0x14

    int-to-float v0, v0

    mul-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->b:F

    .line 234
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/p/l;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 649
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->c:Z

    .line 650
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/s/b;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 582
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/b;->a:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->n:Z

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->i()Lcom/google/android/apps/gmm/map/m/f;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->n:Z

    if-nez v3, :cond_2

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/f;->b:Lcom/google/android/apps/gmm/map/m/n;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/m/n;->n:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/n;->c:Lcom/google/android/apps/gmm/map/m/d;

    instance-of v2, v2, Lcom/google/android/apps/gmm/map/m/x;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/n;->c:Lcom/google/android/apps/gmm/map/m/d;

    check-cast v0, Lcom/google/android/apps/gmm/map/m/x;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/m/x;->e:Z

    .line 584
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 582
    goto :goto_0

    :cond_2
    move v1, v2

    .line 583
    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 271
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v2

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->b:Z

    if-nez v0, :cond_2

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    goto :goto_0

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v0

    .line 281
    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(FF)V

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v1, Lcom/google/r/b/a/a;->l:Lcom/google/r/b/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->d(FF)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/ad;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 448
    iget v2, p1, Lcom/google/android/apps/gmm/map/m/ad;->a:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    .line 449
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->m:Z

    .line 451
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/m/ac;->g:Z

    if-eqz v2, :cond_7

    .line 452
    iget v2, p1, Lcom/google/android/apps/gmm/map/m/ad;->a:I

    if-ne v2, v1, :cond_1

    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 453
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->m:Z

    .line 455
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/ad;->a()F

    move-result v0

    .line 457
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->c:Z

    if-nez v2, :cond_3

    .line 458
    neg-float v0, v0

    .line 460
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->c(F)V

    .line 467
    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v2, Lcom/google/r/b/a/a;->m:Lcom/google/r/b/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    .line 473
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    move v0, v1

    .line 478
    :cond_4
    :goto_2
    return v0

    :cond_5
    move v2, v0

    .line 448
    goto :goto_0

    .line 470
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v2, Lcom/google/r/b/a/a;->n:Lcom/google/r/b/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    goto :goto_1

    .line 476
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v2, :cond_4

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/m/m;->g()V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/u;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 588
    const/4 v0, 0x4

    new-array v3, v0, [F

    .line 589
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/m/u;->a()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    aput v0, v3, v2

    .line 590
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/m/u;->a()Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    aput v0, v3, v1

    .line 591
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/m/u;->b()F

    move-result v0

    aput v0, v3, v7

    .line 592
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/m/u;->c()F

    move-result v0

    aput v0, v3, v8

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    if-ne v0, v4, :cond_6

    move v0, v1

    .line 597
    :goto_0
    if-eqz v0, :cond_7

    .line 598
    aget v4, v3, v1

    .line 599
    aput v6, v3, v2

    .line 600
    aput v6, v3, v1

    .line 601
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    const/high16 v5, 0x40800000    # 4.0f

    mul-float/2addr v4, v5

    aput v4, v3, v7

    .line 602
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->c:Z

    if-eqz v4, :cond_0

    .line 603
    aget v4, v3, v7

    neg-float v4, v4

    aput v4, v3, v7

    .line 613
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 614
    aput v6, v3, v2

    .line 615
    aput v6, v3, v1

    .line 623
    :cond_1
    :goto_2
    aget v2, v3, v7

    cmpl-float v2, v2, v6

    if-eqz v2, :cond_3

    .line 624
    if-eqz v0, :cond_b

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->f:Z

    if-nez v0, :cond_3

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->f()V

    .line 627
    :cond_2
    aput v6, v3, v7

    .line 637
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->h:Z

    if-nez v0, :cond_5

    aget v0, v3, v8

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_5

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v2, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->i()V

    .line 639
    :cond_4
    aput v6, v3, v8

    .line 642
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a([F)V

    .line 643
    return v1

    :cond_6
    move v0, v2

    .line 596
    goto :goto_0

    .line 608
    :cond_7
    aget v4, v3, v2

    neg-float v4, v4

    aput v4, v3, v2

    .line 609
    aget v4, v3, v1

    neg-float v4, v4

    aput v4, v3, v1

    goto :goto_1

    .line 616
    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/m/ac;->b:Z

    if-nez v4, :cond_1

    aget v4, v3, v2

    cmpl-float v4, v4, v6

    if-nez v4, :cond_9

    aget v4, v3, v1

    cmpl-float v4, v4, v6

    if-eqz v4, :cond_1

    .line 618
    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v5, :cond_a

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    .line 619
    :cond_a
    aput v6, v3, v2

    .line 620
    aput v6, v3, v1

    goto :goto_2

    .line 630
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->c:Z

    if-nez v0, :cond_3

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v2, :cond_c

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->b()V

    .line 632
    :cond_c
    aput v6, v3, v7

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/w;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 540
    iget v2, p1, Lcom/google/android/apps/gmm/map/m/w;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 541
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->l:Z

    .line 543
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/m/ac;->h:Z

    if-eqz v2, :cond_5

    .line 544
    iget v2, p1, Lcom/google/android/apps/gmm/map/m/w;->b:I

    if-ne v2, v0, :cond_1

    move v1, v0

    :cond_1
    if-eqz v1, :cond_2

    .line 545
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->l:Z

    .line 547
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/gmm/map/m/w;->a(FF)V

    .line 548
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/w;->c()F

    move-result v1

    .line 549
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 550
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b(F)F

    .line 556
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v2, Lcom/google/r/b/a/a;->s:Lcom/google/r/b/a/a;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    .line 557
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    .line 562
    :goto_2
    return v0

    :cond_3
    move v2, v1

    .line 540
    goto :goto_0

    .line 552
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/w;->a()F

    move-result v2

    .line 553
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/w;->b()F

    move-result v3

    .line 554
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b(FFF)F

    goto :goto_1

    .line 560
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v2, :cond_6

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->h()V

    :cond_6
    move v0, v1

    .line 562
    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/y;)Z
    .locals 12

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 483
    iget v0, p1, Lcom/google/android/apps/gmm/map/m/y;->a:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    .line 484
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->k:Z

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 487
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, v0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    add-float/2addr v5, v7

    div-float/2addr v5, v8

    mul-float v6, v4, v5

    .line 488
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    add-float/2addr v0, v7

    div-float/2addr v0, v8

    mul-float v5, v4, v0

    .line 490
    iget v0, p1, Lcom/google/android/apps/gmm/map/m/y;->a:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_5

    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->e:Z

    if-eqz v0, :cond_6

    .line 492
    iget v0, p1, Lcom/google/android/apps/gmm/map/m/y;->a:I

    if-ne v0, v2, :cond_1

    move v3, v2

    :cond_1
    if-eqz v3, :cond_2

    .line 493
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->k:Z

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v0

    const/high16 v1, -0x40800000    # -1.0f

    const/16 v3, 0x14a

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(FI)F

    move-result v0

    .line 496
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v3, Lcom/google/r/b/a/a;->p:Lcom/google/r/b/a/a;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    .line 497
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v1, v0, v6, v5, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->a(FFFZ)V

    .line 529
    :cond_3
    :goto_2
    return v2

    :cond_4
    move v0, v3

    .line 483
    goto :goto_0

    :cond_5
    move v0, v3

    .line 490
    goto :goto_1

    .line 499
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->d()V

    goto :goto_2

    .line 502
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->c:Z

    if-eqz v0, :cond_11

    .line 503
    iget v0, p1, Lcom/google/android/apps/gmm/map/m/y;->a:I

    if-ne v0, v2, :cond_b

    move v0, v2

    :goto_3
    if-eqz v0, :cond_8

    .line 504
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->k:Z

    .line 506
    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/y;->c()F

    move-result v0

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    sget-wide v10, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->d:D

    div-double/2addr v8, v10

    double-to-float v0, v8

    .line 507
    iget v4, p1, Lcom/google/android/apps/gmm/map/m/y;->a:I

    if-nez v4, :cond_c

    move v4, v2

    :goto_4
    if-eqz v4, :cond_d

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/y;->c()F

    move-result v4

    const v7, 0x3f7fbe77    # 0.999f

    cmpl-float v4, v4, v7

    if-lez v4, :cond_d

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/y;->c()F

    move-result v4

    const v7, 0x3f8020cd    # 1.001001f

    cmpg-float v4, v4, v7

    if-gez v4, :cond_d

    move v4, v2

    :goto_5
    if-eqz v4, :cond_9

    move v0, v1

    .line 511
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a()Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    .line 515
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/y;->a()F

    move-result v6

    .line 516
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/y;->b()F

    move-result v5

    .line 517
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v4

    invoke-virtual {v4, v0, v6, v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(FFF)F

    move-result v4

    .line 519
    :goto_6
    cmpl-float v7, v0, v1

    if-lez v7, :cond_f

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v1, Lcom/google/r/b/a/a;->j:Lcom/google/r/b/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    .line 524
    :cond_a
    :goto_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    iget v0, p1, Lcom/google/android/apps/gmm/map/m/y;->a:I

    const/4 v7, 0x2

    if-ne v0, v7, :cond_10

    move v0, v2

    :goto_8
    invoke-interface {v1, v4, v6, v5, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->a(FFFZ)V

    goto :goto_2

    :cond_b
    move v0, v3

    .line 503
    goto :goto_3

    :cond_c
    move v4, v3

    .line 507
    goto :goto_4

    :cond_d
    move v4, v3

    goto :goto_5

    .line 513
    :cond_e
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(F)F

    move-result v4

    goto :goto_6

    .line 521
    :cond_f
    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v1, Lcom/google/r/b/a/a;->k:Lcom/google/r/b/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    goto :goto_7

    :cond_10
    move v0, v3

    .line 524
    goto :goto_8

    .line 526
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->a()V

    goto/16 :goto_2
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->d()V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->a()V

    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->f()V

    .line 255
    const/4 v0, 0x1

    return v0
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->g()V

    .line 261
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public final g(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->c(FF)Z

    move-result v0

    return v0
.end method

.method public final h(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    if-ne v0, v1, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->i:Z

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->b(FF)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->j()V

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    .line 306
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->g:F

    .line 307
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->h:F

    .line 308
    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    if-ne v0, v1, :cond_1

    .line 337
    :cond_0
    :goto_0
    return v2

    .line 323
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 327
    if-ne v4, v3, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    if-eq v0, v1, :cond_9

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->d:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->c()V

    :cond_2
    move v0, v2

    :goto_1
    or-int/lit8 v2, v0, 0x0

    .line 334
    :goto_2
    if-eq v4, v3, :cond_3

    const/4 v0, 0x3

    if-ne v4, v0, :cond_0

    .line 335
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    goto :goto_0

    .line 329
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->n:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    move v0, v2

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->b:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v5, v0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    add-float/2addr v5, v6

    div-float/2addr v5, v8

    mul-float/2addr v1, v5

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    add-float/2addr v0, v6

    div-float/2addr v0, v8

    mul-float/2addr v0, v5

    :goto_4
    iget-boolean v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->n:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    :goto_5
    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v2

    const/16 v5, 0x14a

    invoke-virtual {v2, v6, v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(FI)F

    move-result v2

    :goto_6
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v6, Lcom/google/r/b/a/a;->o:Lcom/google/r/b/a/a;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v5, v2, v1, v0, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->a(FFFZ)V

    move v0, v3

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_4

    :cond_7
    move v2, v3

    goto :goto_5

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v2

    const/16 v5, 0x14a

    invoke-virtual {v2, v6, v1, v0, v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(FFFI)F

    move-result v2

    goto :goto_6

    .line 331
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    sget-object v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    if-ne v0, v5, :cond_a

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->h:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->b:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->i:F

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    sget-object v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/l;

    if-ne v0, v5, :cond_b

    const/4 v0, 0x2

    if-eq v1, v0, :cond_c

    if-eq v1, v3, :cond_c

    :cond_b
    :goto_7
    or-int/lit8 v2, v2, 0x0

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->f:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_b

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->e()V

    goto :goto_7

    :cond_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->i:F

    sub-float v5, v0, v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    div-float v0, v5, v0

    const/high16 v6, 0x40800000    # 4.0f

    mul-float/2addr v0, v6

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->c:Z

    if-nez v6, :cond_e

    neg-float v0, v0

    :cond_e
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a(F)F

    move-result v0

    if-ne v1, v3, :cond_f

    move v2, v3

    :cond_f
    cmpl-float v1, v5, v7

    if-lez v1, :cond_11

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v5, Lcom/google/r/b/a/a;->r:Lcom/google/r/b/a/a;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    :cond_10
    :goto_8
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->g:F

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->h:F

    invoke-interface {v1, v0, v5, v6, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->a(FFFZ)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->i:F

    move v2, v3

    goto :goto_7

    :cond_11
    cmpg-float v1, v5, v7

    if-gez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->f:Lcom/google/android/apps/gmm/map/q/b;

    sget-object v5, Lcom/google/r/b/a/a;->q:Lcom/google/r/b/a/a;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/map/q/b;->a(Lcom/google/r/b/a/a;)V

    goto :goto_8
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->j:Z

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->e:Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;->a(FF)V

    .line 294
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/m;->k()V

    goto :goto_0
.end method
