.class public Lcom/google/android/apps/gmm/map/t/av;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"


# instance fields
.field public a:F

.field public b:F

.field private final c:[F

.field private final d:[F


# direct methods
.method public constructor <init>(IIZ)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x437f0000    # 255.0f

    .line 30
    if-eqz p3, :cond_0

    const-class v0, Lcom/google/android/apps/gmm/map/t/ax;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->c:[F

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->d:[F

    .line 26
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/av;->a:F

    .line 27
    iput v1, p0, Lcom/google/android/apps/gmm/map/t/av;->b:F

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->c:[F

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    aput v1, v0, v3

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->c:[F

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    aput v1, v0, v4

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->c:[F

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    aput v1, v0, v5

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->d:[F

    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    aput v1, v0, v3

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->d:[F

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    aput v1, v0, v4

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->d:[F

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    aput v1, v0, v5

    .line 37
    return-void

    .line 30
    :cond_0
    const-class v0, Lcom/google/android/apps/gmm/map/t/ay;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 55
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/av;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/aw;

    .line 58
    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    invoke-virtual {p1, v1, p4}, Lcom/google/android/apps/gmm/v/aa;->b(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/ci;

    .line 60
    iget v2, v0, Lcom/google/android/apps/gmm/map/t/aw;->a:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ax;->a:[F

    invoke-static {v2, v5, v3, v1, v3}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    .line 63
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/aw;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/av;->c:[F

    invoke-static {v1, v5, v2, v3}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    .line 64
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/aw;->c:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/av;->d:[F

    invoke-static {v1, v5, v2, v3}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    .line 65
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/aw;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/av;->a:F

    sub-float/2addr v2, v4

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 66
    iget v1, v0, Lcom/google/android/apps/gmm/map/t/aw;->e:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/av;->b:F

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 68
    instance-of v1, v0, Lcom/google/android/apps/gmm/map/t/ay;

    if-eqz v1, :cond_0

    .line 69
    check-cast v0, Lcom/google/android/apps/gmm/map/t/ay;

    .line 70
    iget v0, v0, Lcom/google/android/apps/gmm/map/t/ay;->h:I

    const/high16 v1, 0x3f000000    # 0.5f

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/av;->a:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 72
    :cond_0
    return-void
.end method
