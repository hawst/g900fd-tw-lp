.class public Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;
.super Lcom/google/android/apps/gmm/suggest/SuggestFragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/gmm/shared/net/c;
.implements Lcom/google/android/apps/gmm/suggest/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/suggest/SuggestFragment;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/maps/g/xi;",
        ">;",
        "Lcom/google/android/apps/gmm/suggest/a/a;"
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private f:Landroid/view/View;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->g:Z

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/suggest/k;)Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 50
    new-instance v1, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;-><init>()V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    .line 55
    invoke-static {}, Lcom/google/maps/g/xd;->newBuilder()Lcom/google/maps/g/xg;

    move-result-object v2

    .line 56
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/maps/g/xg;->a(Lcom/google/maps/a/a;)Lcom/google/maps/g/xg;

    move-result-object v2

    .line 57
    invoke-virtual {v2}, Lcom/google/maps/g/xg;->g()Lcom/google/n/t;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 54
    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {v1, v0, p1, v4, v4}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)V

    .line 61
    return-object v1
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 119
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->g:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    move v0, v1

    .line 120
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->f:Landroid/view/View;

    if-eqz v0, :cond_4

    move v3, v2

    :goto_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 121
    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 123
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v1, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->P:Lcom/google/b/f/t;

    aput-object v5, v4, v2

    .line 124
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 125
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    .line 122
    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/gmm/l;->he:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 119
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 120
    :cond_4
    const/16 v3, 0x8

    goto :goto_2
.end method


# virtual methods
.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 81
    sget v0, Lcom/google/android/apps/gmm/g;->g:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->a:Landroid/widget/TextView;

    .line 82
    sget v0, Lcom/google/android/apps/gmm/g;->f:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->f:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->i()V

    .line 85
    return-object v1
.end method

.method public final a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 158
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    .line 159
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->j:Z

    .line 160
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->g:Z

    .line 161
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 3

    .prologue
    .line 39
    check-cast p1, Lcom/google/maps/g/xi;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/maps/g/xi;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/wx;

    iget v0, v0, Lcom/google/maps/g/wx;->b:I

    invoke-static {v0}, Lcom/google/maps/g/xa;->a(I)Lcom/google/maps/g/xa;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/xa;->a:Lcom/google/maps/g/xa;

    :cond_1
    sget-object v2, Lcom/google/maps/g/xa;->i:Lcom/google/maps/g/xa;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->g:Z

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->i()V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 0

    .prologue
    .line 168
    return-void
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b()V

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->i()V

    .line 76
    return-void
.end method

.method protected final c()Lcom/google/android/apps/gmm/suggest/a/a;
    .locals 0
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 97
    return-object p0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 137
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/b/f/t;->P:Lcom/google/b/f/t;

    aput-object v5, v2, v4

    .line 138
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 139
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 136
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/a/a;

    sget-object v1, Lcom/google/maps/g/hs;->i:Lcom/google/maps/g/hs;

    .line 142
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v2

    .line 145
    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/suggest/l;->e()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v5

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/addaplace/a/a;-><init>(Lcom/google/maps/g/hs;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/cc;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->h()Lcom/google/android/apps/gmm/addaplace/a/b;

    move-result-object v2

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/suggest/l;->e()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v3

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v6, v3, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v3, Lcom/google/maps/g/ha;->a:I

    iput-wide v4, v3, Lcom/google/maps/g/ha;->b:D

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v1, v3, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v3, Lcom/google/maps/g/ha;->a:I

    iput-wide v4, v3, Lcom/google/maps/g/ha;->c:D

    invoke-virtual {v3}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/gy;

    .line 148
    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/gmm/addaplace/a/b;->a(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/SuggestOrAddFragment;->e:Lcom/google/android/apps/gmm/suggest/g/c;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/suggest/g/c;->h:Z

    .line 69
    return-object v0
.end method
