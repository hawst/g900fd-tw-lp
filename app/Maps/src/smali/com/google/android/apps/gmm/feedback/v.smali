.class public Lcom/google/android/apps/gmm/feedback/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/a/f;
.implements Lcom/google/android/apps/gmm/map/t/aj;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field volatile b:Z

.field volatile c:Landroid/graphics/Bitmap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Landroid/graphics/Bitmap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/feedback/a/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/feedback/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/a/g;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/feedback/a/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/v;->b:Z

    .line 44
    iput-object v1, p0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    .line 45
    iput-object v1, p0, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/v;->f:Lcom/google/android/apps/gmm/base/activities/c;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/v;->e:Lcom/google/android/apps/gmm/feedback/a/g;

    .line 53
    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/view/View;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 94
    if-nez p0, :cond_0

    .line 104
    :goto_0
    return-object v0

    .line 98
    :cond_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 100
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 101
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 103
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/a/g;)Lcom/google/android/apps/gmm/feedback/a/f;
    .locals 6
    .param p1    # Lcom/google/android/apps/gmm/feedback/a/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 63
    new-instance v1, Lcom/google/android/apps/gmm/feedback/v;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/feedback/v;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/a/g;)V

    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/apps/gmm/feedback/v;->f:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/af;->n:Z

    if-eqz v2, :cond_3

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/map/t/aj;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/feedback/v;->f:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v5, v1, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/v;->a(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/feedback/v;->f:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/v;->a(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    .line 65
    :cond_2
    return-object v1

    .line 64
    :cond_3
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/ai;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/map/t/ai;-><init>(Lcom/google/android/apps/gmm/map/t/af;Lcom/google/android/apps/gmm/map/t/aj;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/v;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 120
    sget-object v0, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/v;->f:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/w;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/feedback/w;-><init>(Lcom/google/android/apps/gmm/feedback/v;Landroid/graphics/Bitmap;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 137
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/v;->b:Z

    return v0
.end method
