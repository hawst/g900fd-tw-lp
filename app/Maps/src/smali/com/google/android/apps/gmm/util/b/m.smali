.class public Lcom/google/android/apps/gmm/util/b/m;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/google/o/h/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/o/h/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:Ljava/lang/String;

.field final e:Lcom/google/r/b/a/tf;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/o/h/a/iv;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    .locals 1
    .param p3    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 66
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/util/b/m;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 67
    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    .locals 8
    .param p3    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/r/b/a/tf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/16 v6, 0x8

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/b/m;->d:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/google/android/apps/gmm/util/b/m;->e:Lcom/google/r/b/a/tf;

    .line 51
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_24

    .line 52
    invoke-static {p1}, Lcom/google/android/apps/gmm/util/b/m;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/m;->a:Ljava/util/List;

    .line 53
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    iget-object v0, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_2

    move v1, v3

    :goto_0
    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/gp;

    iget v1, v1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    move v1, v3

    :goto_1
    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/m;->b:Lcom/google/o/h/a/a;

    .line 54
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    iget-object v0, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_16

    move v1, v3

    :goto_3
    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/gp;

    iget v1, v1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_17

    move v1, v3

    :goto_4
    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    move-object v2, v0

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/gmm/util/b/m;->c:Lcom/google/o/h/a/a;

    .line 55
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    iget-object v0, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_18

    move v1, v3

    :goto_5
    if-eqz v1, :cond_19

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-virtual {v0}, Lcom/google/o/h/a/gp;->d()Ljava/lang/String;

    .line 60
    :cond_1
    :goto_6
    return-void

    :cond_2
    move v1, v4

    .line 53
    goto/16 :goto_0

    :cond_3
    move v1, v4

    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2

    :cond_5
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_6

    move v1, v3

    :goto_7
    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/od;

    iget v1, v1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_7

    move v1, v3

    :goto_8
    if-eqz v1, :cond_8

    iget-object v0, v0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/od;

    iget-object v0, v0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    goto/16 :goto_2

    :cond_6
    move v1, v4

    goto :goto_7

    :cond_7
    move v1, v4

    goto :goto_8

    :cond_8
    move-object v0, v2

    goto/16 :goto_2

    :cond_9
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_a

    move v1, v3

    :goto_9
    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/fd;

    iget v1, v1, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_b

    move v1, v3

    :goto_a
    if-eqz v1, :cond_c

    iget-object v0, v0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    iget-object v0, v0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    goto/16 :goto_2

    :cond_a
    move v1, v4

    goto :goto_9

    :cond_b
    move v1, v4

    goto :goto_a

    :cond_c
    move-object v0, v2

    goto/16 :goto_2

    :cond_d
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v5, 0x40

    if-ne v1, v5, :cond_e

    move v1, v3

    :goto_b
    if-eqz v1, :cond_11

    iget-object v1, v0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/op;

    iget v1, v1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_f

    move v1, v3

    :goto_c
    if-eqz v1, :cond_10

    iget-object v0, v0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/op;

    iget-object v0, v0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    goto/16 :goto_2

    :cond_e
    move v1, v4

    goto :goto_b

    :cond_f
    move v1, v4

    goto :goto_c

    :cond_10
    move-object v0, v2

    goto/16 :goto_2

    :cond_11
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v5, 0x80

    if-ne v1, v5, :cond_12

    move v1, v3

    :goto_d
    if-eqz v1, :cond_15

    iget-object v1, v0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ol;

    iget v1, v1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v5, 0x20

    if-ne v1, v5, :cond_13

    move v1, v3

    :goto_e
    if-eqz v1, :cond_14

    iget-object v0, v0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ol;

    iget-object v0, v0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    goto/16 :goto_2

    :cond_12
    move v1, v4

    goto :goto_d

    :cond_13
    move v1, v4

    goto :goto_e

    :cond_14
    move-object v0, v2

    goto/16 :goto_2

    :cond_15
    move-object v0, v2

    goto/16 :goto_2

    :cond_16
    move v1, v4

    .line 54
    goto/16 :goto_3

    :cond_17
    move v1, v4

    goto/16 :goto_4

    :cond_18
    move v1, v4

    .line 55
    goto/16 :goto_5

    :cond_19
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_1a

    move v1, v3

    :goto_f
    if-eqz v1, :cond_1c

    iget-object v0, v0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/od;

    iget-object v1, v0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1b

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_6

    :cond_1a
    move v1, v4

    goto :goto_f

    :cond_1b
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    goto/16 :goto_6

    :cond_1c
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_1d

    move v1, v3

    :goto_10
    if-eqz v1, :cond_1f

    iget-object v0, v0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    iget-object v1, v0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1e

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_6

    :cond_1d
    move v1, v4

    goto :goto_10

    :cond_1e
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    goto/16 :goto_6

    :cond_1f
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_20

    move v1, v3

    :goto_11
    if-eqz v1, :cond_22

    iget-object v0, v0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/op;

    iget-object v1, v0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_21

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_6

    :cond_20
    move v1, v4

    goto :goto_11

    :cond_21
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    goto/16 :goto_6

    :cond_22
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_23

    move v1, v3

    :goto_12
    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ol;

    invoke-virtual {v0}, Lcom/google/o/h/a/ol;->d()Ljava/lang/String;

    goto/16 :goto_6

    :cond_23
    move v1, v4

    goto :goto_12

    .line 57
    :cond_24
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/b/m;->a:Ljava/util/List;

    .line 58
    iput-object v2, p0, Lcom/google/android/apps/gmm/util/b/m;->b:Lcom/google/o/h/a/a;

    .line 59
    iput-object v2, p0, Lcom/google/android/apps/gmm/util/b/m;->c:Lcom/google/o/h/a/a;

    goto/16 :goto_6
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 184
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v4

    .line 185
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    .line 186
    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/ix;->a(Lcom/google/o/h/a/iv;)Lcom/google/o/h/a/ix;

    move-result-object v6

    .line 187
    iget-object v0, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v1

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/gr;

    move-result-object v0

    iget-object v7, v0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v7, v0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v7, v7, -0x41

    iput v7, v0, Lcom/google/o/h/a/gr;->a:I

    iget-object v7, v0, Lcom/google/o/h/a/gr;->g:Lcom/google/n/ao;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v7, v0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit16 v7, v7, -0x101

    iput v7, v0, Lcom/google/o/h/a/gr;->a:I

    iget-object v7, v1, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/o/h/a/it;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    .line 186
    :cond_0
    :goto_2
    invoke-virtual {v6, v0}, Lcom/google/o/h/a/ix;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/ix;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v0

    .line 186
    invoke-virtual {v4, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto/16 :goto_0

    :cond_1
    move v1, v3

    .line 187
    goto :goto_1

    :cond_2
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v7, 0x4

    if-ne v1, v7, :cond_3

    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v1

    iget-object v0, v0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/od;

    invoke-static {}, Lcom/google/o/h/a/od;->newBuilder()Lcom/google/o/h/a/of;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/o/h/a/of;->a(Lcom/google/o/h/a/od;)Lcom/google/o/h/a/of;

    move-result-object v0

    iget-object v7, v0, Lcom/google/o/h/a/of;->b:Lcom/google/n/ao;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v7, v0, Lcom/google/o/h/a/of;->a:I

    and-int/lit8 v7, v7, -0x5

    iput v7, v0, Lcom/google/o/h/a/of;->a:I

    iget-object v7, v1, Lcom/google/o/h/a/it;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/of;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/o/h/a/it;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_3

    :cond_4
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v7, 0x8

    if-ne v1, v7, :cond_5

    move v1, v2

    :goto_4
    if-eqz v1, :cond_6

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v1

    iget-object v0, v0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    invoke-static {}, Lcom/google/o/h/a/fd;->newBuilder()Lcom/google/o/h/a/ff;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/o/h/a/ff;->a(Lcom/google/o/h/a/fd;)Lcom/google/o/h/a/ff;

    move-result-object v0

    iget-object v7, v0, Lcom/google/o/h/a/ff;->b:Lcom/google/n/ao;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v7, v0, Lcom/google/o/h/a/ff;->a:I

    and-int/lit8 v7, v7, -0x3

    iput v7, v0, Lcom/google/o/h/a/ff;->a:I

    iget-object v7, v1, Lcom/google/o/h/a/it;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/ff;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/o/h/a/it;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    goto/16 :goto_2

    :cond_5
    move v1, v3

    goto :goto_4

    :cond_6
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v7, 0x40

    if-ne v1, v7, :cond_7

    move v1, v2

    :goto_5
    if-eqz v1, :cond_8

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v1

    iget-object v0, v0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/op;

    invoke-static {}, Lcom/google/o/h/a/op;->newBuilder()Lcom/google/o/h/a/or;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/o/h/a/or;->a(Lcom/google/o/h/a/op;)Lcom/google/o/h/a/or;

    move-result-object v0

    iget-object v7, v0, Lcom/google/o/h/a/or;->b:Lcom/google/n/ao;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v7, v0, Lcom/google/o/h/a/or;->a:I

    and-int/lit8 v7, v7, -0x9

    iput v7, v0, Lcom/google/o/h/a/or;->a:I

    iget-object v7, v1, Lcom/google/o/h/a/it;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/or;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, v1, Lcom/google/o/h/a/it;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    goto/16 :goto_2

    :cond_7
    move v1, v3

    goto :goto_5

    :cond_8
    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v7, 0x80

    if-ne v1, v7, :cond_9

    move v1, v2

    :goto_6
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v1

    iget-object v0, v0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ol;

    invoke-static {}, Lcom/google/o/h/a/ol;->newBuilder()Lcom/google/o/h/a/on;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/o/h/a/on;->a(Lcom/google/o/h/a/ol;)Lcom/google/o/h/a/on;

    move-result-object v0

    iget-object v7, v0, Lcom/google/o/h/a/on;->b:Lcom/google/n/ao;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v9, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v7, v0, Lcom/google/o/h/a/on;->a:I

    and-int/lit8 v7, v7, -0x21

    iput v7, v0, Lcom/google/o/h/a/on;->a:I

    iget-object v7, v1, Lcom/google/o/h/a/it;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/on;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v7, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/it;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, v1, Lcom/google/o/h/a/it;->a:I

    invoke-virtual {v1}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    goto/16 :goto_2

    :cond_9
    move v1, v3

    goto :goto_6

    .line 189
    :cond_a
    invoke-virtual {v4}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)Ljava/util/List;
    .locals 6
    .param p2    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/r/b/a/tf;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 73
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 74
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 75
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    .line 76
    iget-boolean v4, v0, Lcom/google/o/h/a/iv;->d:Z

    if-nez v4, :cond_0

    .line 77
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v4

    .line 78
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 79
    new-instance v2, Lcom/google/android/apps/gmm/util/b/m;

    invoke-direct {v2, v4, p1, p2}, Lcom/google/android/apps/gmm/util/b/m;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 83
    :cond_0
    invoke-virtual {v2, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 74
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 87
    new-instance v1, Lcom/google/android/apps/gmm/util/b/m;

    invoke-direct {v1, v0, p1, p2}, Lcom/google/android/apps/gmm/util/b/m;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_2
    return-object v3
.end method
