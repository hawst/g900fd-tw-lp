.class public Lcom/google/android/apps/gmm/directions/f/d/f;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/google/maps/g/a/hm;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 21
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/d/f;->b:Lcom/google/maps/g/a/hm;

    .line 28
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->g:Lcom/google/maps/g/a/hm;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/d/f;->a:Lcom/google/b/c/cv;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/hm;
    .locals 2

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->af:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/d/f;->b:Lcom/google/maps/g/a/hm;

    .line 41
    iget v1, v1, Lcom/google/maps/g/a/hm;->h:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    .line 40
    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/maps/g/a/hm;)V
    .locals 3

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->af:Lcom/google/android/apps/gmm/shared/b/c;

    iget v1, p1, Lcom/google/maps/g/a/hm;->h:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 46
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/hm;)Z
    .locals 1

    .prologue
    .line 35
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/f;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0, p0}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/google/maps/g/a/hm;)I
    .locals 2
    .param p0    # Lcom/google/maps/g/a/hm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 52
    if-nez p0, :cond_0

    .line 53
    sget v0, Lcom/google/android/apps/gmm/f;->cU:I

    .line 65
    :goto_0
    return v0

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/g;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 65
    sget v0, Lcom/google/android/apps/gmm/f;->cU:I

    goto :goto_0

    .line 57
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/f;->cZ:I

    goto :goto_0

    .line 59
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/f;->fs:I

    goto :goto_0

    .line 61
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/f;->cG:I

    goto :goto_0

    .line 63
    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/f;->fx:I

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(Lcom/google/maps/g/a/hm;)Z
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne p0, v0, :cond_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
