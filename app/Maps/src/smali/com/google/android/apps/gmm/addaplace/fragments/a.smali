.class Lcom/google/android/apps/gmm/addaplace/fragments/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 12
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/z;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    .line 49
    iget-object v2, v2, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    sget-object v3, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    .line 48
    invoke-virtual {v1, v2, v3, v10}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->f:Lcom/google/android/apps/gmm/map/b/a;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/b;->a()Lcom/google/android/apps/gmm/shared/net/b;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iput-object v11, v0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/shared/net/b;

    .line 57
    :cond_1
    invoke-static {}, Lcom/google/r/b/a/aka;->newBuilder()Lcom/google/r/b/a/akc;

    move-result-object v0

    .line 58
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    .line 59
    iget-object v2, v2, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v4, v1, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v1, Lcom/google/maps/a/g;->c:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    .line 60
    iget-object v2, v2, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v4, v1, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/maps/a/g;->a:I

    iput-wide v2, v1, Lcom/google/maps/a/g;->b:D

    .line 58
    iget-object v2, v0, Lcom/google/r/b/a/akc;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/r/b/a/akc;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    .line 61
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/r/b/a/akc;->a(Lcom/google/maps/a/a;)Lcom/google/r/b/a/akc;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/d/d;->b:Lcom/google/maps/g/d/d;

    .line 62
    invoke-virtual {v0, v1}, Lcom/google/r/b/a/akc;->a(Lcom/google/maps/g/d/d;)Lcom/google/r/b/a/akc;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/google/r/b/a/akc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aka;

    .line 65
    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/addaplace/fragments/b;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/addaplace/fragments/b;-><init>(Lcom/google/android/apps/gmm/addaplace/fragments/a;)V

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/shared/net/b;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->a(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    invoke-static {v0, v10}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/a;->a:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->c(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V

    goto/16 :goto_0
.end method
