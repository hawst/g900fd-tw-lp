.class public Lcom/google/android/apps/gmm/map/internal/c/cf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/android/apps/gmm/map/internal/c/p;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/maps/g/a/ca;

.field public g:Z

.field public h:Lcom/google/maps/g/a/dn;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/aa;Ljava/lang/String;Lcom/google/maps/g/a/ca;ZLcom/google/maps/g/a/dn;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    .line 37
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    .line 41
    if-nez p5, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 42
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    .line 43
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->f:Lcom/google/maps/g/a/ca;

    .line 44
    iput-boolean p8, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->g:Z

    .line 45
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->h:Lcom/google/maps/g/a/dn;

    .line 46
    return-void

    .line 41
    :cond_0
    iget-object v0, p5, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/cf;
    .locals 11
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 50
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 58
    :goto_2
    return-object v5

    :cond_1
    move v0, v9

    .line 50
    goto :goto_0

    :cond_2
    move v0, v9

    goto :goto_1

    .line 53
    :cond_3
    const/16 v0, 0x13

    invoke-virtual {p0, v1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 54
    const-string v4, ""

    .line 55
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_3
    if-nez v0, :cond_4

    invoke-virtual {p0, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    :cond_4
    move v0, v1

    :goto_4
    if-eqz v0, :cond_5

    .line 56
    const/16 v0, 0x1c

    invoke-virtual {p0, v6, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 58
    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cf;

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v10, v5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/cf;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/aa;Ljava/lang/String;Lcom/google/maps/g/a/ca;ZLcom/google/maps/g/a/dn;)V

    move-object v5, v1

    goto :goto_2

    :cond_6
    move v0, v9

    .line 55
    goto :goto_3

    :cond_7
    move v0, v9

    goto :goto_4
.end method

.method public static a(Lcom/google/maps/b/a/z;)Lcom/google/android/apps/gmm/map/internal/c/cf;
    .locals 11
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    iget-boolean v0, v0, Lcom/google/maps/b/a/db;->c:Z

    if-nez v0, :cond_0

    .line 72
    :goto_0
    return-object v5

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/z;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v0, Lcom/google/maps/b/a/db;->b:J

    .line 70
    invoke-virtual {p0}, Lcom/google/maps/b/a/z;->a()Ljava/lang/String;

    move-result-object v4

    .line 72
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cf;

    const/4 v9, 0x0

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v10, v5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/map/internal/c/cf;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/aa;Ljava/lang/String;Lcom/google/maps/g/a/ca;ZLcom/google/maps/g/a/dn;)V

    move-object v5, v1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    if-ne p0, p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;

    if-nez v2, :cond_2

    move v0, v1

    .line 146
    goto :goto_0

    .line 148
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 149
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    .line 150
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_a

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_3
    move v2, v0

    :goto_1
    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    .line 151
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_b

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_4
    move v2, v0

    :goto_2
    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 152
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_c

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_5
    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    .line 153
    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_d

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_6
    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->f:Lcom/google/maps/g/a/ca;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->f:Lcom/google/maps/g/a/ca;

    .line 154
    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_e

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_7
    move v2, v0

    :goto_5
    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->g:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->g:Z

    if-ne v2, v3, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->h:Lcom/google/maps/g/a/dn;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/cf;->h:Lcom/google/maps/g/a/dn;

    .line 156
    if-eq v2, v3, :cond_8

    if-eqz v2, :cond_f

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_8
    move v2, v0

    :goto_6
    if-nez v2, :cond_0

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    move v2, v1

    .line 150
    goto :goto_1

    :cond_b
    move v2, v1

    .line 151
    goto :goto_2

    :cond_c
    move v2, v1

    .line 152
    goto :goto_3

    :cond_d
    move v2, v1

    .line 153
    goto :goto_4

    :cond_e
    move v2, v1

    .line 154
    goto :goto_5

    :cond_f
    move v2, v1

    .line 156
    goto :goto_6
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 136
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    .line 137
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->f:Lcom/google/maps/g/a/ca;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->g:Z

    .line 138
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->h:Lcom/google/maps/g/a/dn;

    aput-object v2, v0, v1

    .line 136
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 162
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "incidentId"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    .line 163
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "captionText"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    .line 164
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "freetext"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    .line 165
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "mapIcon"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 166
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "infoSheetUrl"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    .line 167
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "type"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->f:Lcom/google/maps/g/a/ca;

    .line 168
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "isAlongTheRoute"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->g:Z

    .line 169
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "attribution"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->h:Lcom/google/maps/g/a/dn;

    .line 170
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 171
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
