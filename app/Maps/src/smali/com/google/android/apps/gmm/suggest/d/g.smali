.class public Lcom/google/android/apps/gmm/suggest/d/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/suggest/d/h;

.field private b:Lcom/google/android/apps/gmm/suggest/e/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private c:Lcom/google/b/c/cv;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private e:Lcom/google/b/f/bj;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:J

.field private m:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/suggest/d/h;)V
    .locals 5

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    if-eq v0, p1, :cond_0

    .line 88
    const-string v0, "SuggestRoundTripLog"

    const-string v1, "Unexpected round trip state: expected <%s> but actually <%s>\n%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :cond_0
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->b:Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/d/h;)V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->h:Z

    .line 123
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->d:Lcom/google/android/apps/gmm/suggest/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->b:Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/d/h;)V

    .line 112
    iput-wide p1, p0, Lcom/google/android/apps/gmm/suggest/d/g;->l:J

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->g:Z

    .line 114
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->c:Lcom/google/android/apps/gmm/suggest/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLcom/google/b/c/cv;Lcom/google/n/f;Lcom/google/b/f/bj;)V
    .locals 1
    .param p4    # Lcom/google/n/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/b/f/bj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;",
            "Lcom/google/n/f;",
            "Lcom/google/b/f/bj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->c:Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/d/h;)V

    .line 134
    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->c:Lcom/google/b/c/cv;

    .line 135
    iput-object p4, p0, Lcom/google/android/apps/gmm/suggest/d/g;->d:Lcom/google/n/f;

    .line 136
    iput-object p5, p0, Lcom/google/android/apps/gmm/suggest/d/g;->e:Lcom/google/b/f/bj;

    .line 137
    iput-wide p1, p0, Lcom/google/android/apps/gmm/suggest/d/g;->m:J

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->i:Z

    .line 139
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->e:Lcom/google/android/apps/gmm/suggest/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    monitor-exit p0

    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/suggest/e/a;)V
    .locals 1

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/d/h;)V

    .line 101
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/d/g;->b:Lcom/google/android/apps/gmm/suggest/e/a;

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->f:Z

    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->b:Lcom/google/android/apps/gmm/suggest/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->e:Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/suggest/d/g;->a(Lcom/google/android/apps/gmm/suggest/d/h;)V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->j:Z

    .line 150
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->f:Lcom/google/android/apps/gmm/suggest/d/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 158
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lcom/google/android/apps/gmm/suggest/e/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->b:Lcom/google/android/apps/gmm/suggest/e/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lcom/google/n/f;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->d:Lcom/google/n/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lcom/google/b/f/bj;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->e:Lcom/google/b/f/bj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Z
    .locals 1

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Z
    .locals 1

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()Z
    .locals 1

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()Z
    .locals 1

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()Z
    .locals 1

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()I
    .locals 4

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->m:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/suggest/d/g;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized n()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 237
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->l:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/gmm/suggest/d/g;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 69
    monitor-enter p0

    :try_start_0
    const-class v0, Lcom/google/android/apps/gmm/suggest/d/g;

    new-instance v4, Lcom/google/b/a/ak;

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "state"

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 70
    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 70
    :cond_0
    :try_start_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "triggeringQuery"

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->b:Lcom/google/android/apps/gmm/suggest/e/a;

    if-nez v3, :cond_1

    move-object v3, v1

    .line 71
    :goto_0
    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 70
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->b:Lcom/google/android/apps/gmm/suggest/e/a;

    .line 71
    iget-object v3, v3, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    goto :goto_0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "fetcherRequestLogged"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->f:Z

    .line 72
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "connectionRequestLogged"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->g:Z

    .line 73
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "connectionRejectedRequestLogged"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->h:Z

    .line 74
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "connectionResponseLogged"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->i:Z

    .line 75
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "outOfSyncResponseLogged"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->j:Z

    .line 76
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "connectionFailureLogged"

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->k:Z

    .line 77
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "roundTripTime"

    iget-wide v6, p0, Lcom/google/android/apps/gmm/suggest/d/g;->m:J

    iget-wide v8, p0, Lcom/google/android/apps/gmm/suggest/d/g;->l:J

    sub-long/2addr v6, v8

    .line 78
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "suggestionCount"

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->c:Lcom/google/b/c/cv;

    if-nez v3, :cond_a

    move v3, v2

    .line 79
    :goto_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 78
    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->c:Lcom/google/b/c/cv;

    .line 79
    invoke-virtual {v3}, Lcom/google/b/c/cv;->size()I

    move-result v3

    goto :goto_1

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "experimentInfoSize"

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/d/g;->d:Lcom/google/n/f;

    if-nez v3, :cond_c

    .line 80
    :goto_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 79
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/d/g;->d:Lcom/google/n/f;

    .line 80
    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    goto :goto_2

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "searchboxExperimentInfo"

    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/d/g;->e:Lcom/google/b/f/bj;

    if-nez v2, :cond_e

    .line 81
    :goto_3
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 80
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/d/g;->e:Lcom/google/b/f/bj;

    .line 82
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 81
    :cond_f
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 83
    invoke-virtual {v4}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method
