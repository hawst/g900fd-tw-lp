.class public Lcom/google/android/apps/gmm/map/internal/c/bj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    .line 31
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;)Lcom/google/android/apps/gmm/map/internal/c/bj;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 60
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bj;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/internal/c/bj;-><init>()V

    .line 63
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    .line 65
    new-array v4, v3, [B

    move v1, v0

    .line 66
    :goto_0
    if-ge v1, v3, :cond_0

    .line 67
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v5

    aput-byte v5, v4, v1

    .line 68
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 71
    :goto_1
    if-ge v0, v1, :cond_1

    .line 72
    invoke-static {p0, p1, v4}, Lcom/google/android/apps/gmm/map/internal/c/bi;->b(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;[B)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v3

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 74
    :cond_1
    return-object v2
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;I)Lcom/google/android/apps/gmm/map/internal/c/bj;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 41
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bj;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/bj;-><init>()V

    .line 42
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    .line 44
    const/4 v3, 0x1

    new-array v3, v3, [B

    int-to-byte v4, p2

    aput-byte v4, v3, v0

    .line 45
    :goto_0
    if-ge v0, v2, :cond_0

    .line 46
    invoke-static {p0, p1, v3}, Lcom/google/android/apps/gmm/map/internal/c/bi;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;[B)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/Iterable;Lcom/google/android/apps/gmm/map/internal/c/bl;Z)Lcom/google/android/apps/gmm/map/internal/c/bj;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/b/a/bg;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/c/bl;",
            "Z)",
            "Lcom/google/android/apps/gmm/map/internal/c/bj;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v12, Lcom/google/android/apps/gmm/map/internal/c/bj;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/map/internal/c/bj;-><init>()V

    .line 91
    invoke-interface/range {p0 .. p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/google/maps/b/a/bg;

    .line 92
    iget-object v2, v8, Lcom/google/maps/b/a/bg;->a:Lcom/google/maps/b/a/db;

    iget-wide v2, v2, Lcom/google/maps/b/a/db;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    .line 93
    iget-object v2, v8, Lcom/google/maps/b/a/bg;->c:Lcom/google/maps/b/a/cs;

    iget v15, v2, Lcom/google/maps/b/a/cs;->b:I

    .line 94
    iget-object v2, v8, Lcom/google/maps/b/a/bg;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_2

    const/4 v2, 0x1

    move v4, v2

    .line 95
    :goto_2
    const/4 v3, 0x0

    .line 96
    const/4 v2, 0x0

    .line 98
    if-lez v15, :cond_20

    if-nez v4, :cond_0

    if-eqz p2, :cond_20

    .line 99
    :cond_0
    new-array v10, v15, [B

    .line 100
    new-array v9, v15, [Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 104
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->g()Lcom/google/android/apps/gmm/map/internal/c/bg;

    move-result-object v16

    .line 105
    const/4 v2, 0x6

    new-array v0, v2, [I

    move-object/from16 v17, v0

    .line 106
    const/4 v2, 0x0

    move v11, v2

    :goto_3
    if-ge v11, v15, :cond_1f

    .line 107
    invoke-virtual {v8, v11}, Lcom/google/maps/b/a/bg;->b(I)Lcom/google/maps/b/a/aj;

    move-result-object v18

    .line 109
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_5

    .line 110
    invoke-virtual/range {v18 .. v18}, Lcom/google/maps/b/a/aj;->a()Lcom/google/maps/b/a/i;

    move-result-object v3

    .line 111
    const/4 v2, 0x0

    iget-object v4, v3, Lcom/google/maps/b/a/i;->c:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    aput v4, v17, v2

    .line 112
    iget-object v2, v3, Lcom/google/maps/b/a/i;->a:Lcom/google/maps/b/a/cs;

    iget v4, v2, Lcom/google/maps/b/a/cs;->b:I

    .line 113
    iget-object v2, v3, Lcom/google/maps/b/a/i;->d:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_4
    if-nez v2, :cond_7

    if-lez v4, :cond_7

    .line 114
    new-array v5, v4, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 115
    const/4 v2, 0x0

    :goto_5
    if-ge v2, v4, :cond_4

    .line 116
    invoke-virtual {v3, v2}, Lcom/google/maps/b/a/i;->c_(I)Lcom/google/maps/b/a/cj;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a(Lcom/google/maps/b/a/cj;)Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v6

    aput-object v6, v5, v2

    .line 115
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 94
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    move v4, v2

    goto :goto_2

    .line 113
    :cond_3
    const/4 v2, 0x0

    goto :goto_4

    .line 118
    :cond_4
    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->d:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 122
    :goto_6
    iget-object v2, v3, Lcom/google/maps/b/a/i;->e:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_7
    if-nez v2, :cond_9

    .line 123
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    iget-object v3, v3, Lcom/google/maps/b/a/i;->b:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    aput v3, v2, v4

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->b:[I

    .line 129
    :cond_5
    :goto_8
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->f:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_6

    .line 130
    invoke-virtual/range {v18 .. v18}, Lcom/google/maps/b/a/aj;->f()Lcom/google/maps/b/a/dk;

    move-result-object v4

    .line 131
    iget-object v2, v4, Lcom/google/maps/b/a/dk;->e:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_9
    if-nez v2, :cond_b

    .line 132
    const/4 v2, 0x4

    iget-object v3, v4, Lcom/google/maps/b/a/dk;->d:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    aput v3, v17, v2

    .line 133
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget-object v5, v4, Lcom/google/maps/b/a/dk;->c:Lcom/google/maps/b/a/cz;

    iget v5, v5, Lcom/google/maps/b/a/cz;->b:I

    aput v5, v2, v3

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->c:[I

    .line 134
    iget-object v2, v4, Lcom/google/maps/b/a/dk;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v2, :cond_6

    .line 135
    const/4 v2, 0x1

    new-array v0, v2, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 136
    iget-object v3, v4, Lcom/google/maps/b/a/dk;->a:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    .line 137
    iget-object v4, v4, Lcom/google/maps/b/a/dk;->b:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v4, v4

    const/high16 v5, 0x41000000    # 8.0f

    div-float/2addr v4, v5

    const/4 v5, 0x0

    new-array v5, v5, [I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    aput-object v2, v19, v20

    .line 135
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/bg;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 147
    :cond_6
    :goto_a
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_e

    .line 148
    invoke-virtual/range {v18 .. v18}, Lcom/google/maps/b/a/aj;->d()Lcom/google/maps/b/a/be;

    move-result-object v3

    .line 149
    const/4 v2, 0x2

    iget-object v4, v3, Lcom/google/maps/b/a/be;->b:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    aput v4, v17, v2

    .line 150
    iget-object v2, v3, Lcom/google/maps/b/a/be;->a:Lcom/google/maps/b/a/cs;

    iget v4, v2, Lcom/google/maps/b/a/cs;->b:I

    .line 151
    iget-object v2, v3, Lcom/google/maps/b/a/be;->c:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_b
    if-nez v2, :cond_14

    if-lez v4, :cond_14

    .line 152
    new-array v5, v4, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 153
    const/4 v2, 0x0

    :goto_c
    if-ge v2, v4, :cond_d

    .line 154
    invoke-virtual {v3, v2}, Lcom/google/maps/b/a/be;->b(I)Lcom/google/maps/b/a/cj;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/internal/c/bd;->a(Lcom/google/maps/b/a/cj;)Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-result-object v6

    aput-object v6, v5, v2

    .line 153
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 120
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->d:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    goto/16 :goto_6

    .line 122
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 125
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->b:[I

    goto/16 :goto_8

    .line 131
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 142
    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->c:[I

    .line 143
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->f:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    goto :goto_a

    .line 151
    :cond_c
    const/4 v2, 0x0

    goto :goto_b

    .line 156
    :cond_d
    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->e:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 162
    :cond_e
    :goto_d
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_11

    .line 163
    invoke-virtual/range {v18 .. v18}, Lcom/google/maps/b/a/aj;->b()Lcom/google/maps/b/a/av;

    move-result-object v3

    .line 164
    iget-object v2, v3, Lcom/google/maps/b/a/av;->f:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_15

    const/4 v2, 0x1

    :goto_e
    if-nez v2, :cond_17

    .line 165
    const/4 v2, 0x1

    iget-object v4, v3, Lcom/google/maps/b/a/av;->e:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    aput v4, v17, v2

    .line 166
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/internal/c/bn;->a(Lcom/google/maps/b/a/av;)Lcom/google/android/apps/gmm/map/internal/c/bn;

    move-result-object v2

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->g:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 167
    const/4 v2, 0x0

    .line 168
    iget-object v4, v3, Lcom/google/maps/b/a/av;->d:Lcom/google/maps/b/a/cr;

    iget-boolean v4, v4, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v4, :cond_16

    .line 169
    invoke-virtual {v3}, Lcom/google/maps/b/a/av;->b()Lcom/google/maps/b/a/ck;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/internal/c/bm;->a(Lcom/google/maps/b/a/ck;)Lcom/google/android/apps/gmm/map/internal/c/bm;

    move-result-object v4

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->h:Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 170
    invoke-virtual {v3}, Lcom/google/maps/b/a/av;->b()Lcom/google/maps/b/a/ck;

    move-result-object v4

    iget-object v4, v4, Lcom/google/maps/b/a/ck;->d:Lcom/google/maps/b/a/cs;

    iget v4, v4, Lcom/google/maps/b/a/cs;->b:I

    if-lez v4, :cond_f

    .line 171
    invoke-virtual {v3}, Lcom/google/maps/b/a/av;->b()Lcom/google/maps/b/a/ck;

    move-result-object v2

    new-instance v4, Lcom/google/maps/b/a/cm;

    invoke-direct {v4, v2}, Lcom/google/maps/b/a/cm;-><init>(Lcom/google/maps/b/a/ck;)V

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/internal/c/p;->a(Ljava/lang/Iterable;)Lcom/google/android/apps/gmm/map/internal/c/p;

    move-result-object v2

    .line 176
    :cond_f
    :goto_f
    iget-object v4, v3, Lcom/google/maps/b/a/av;->c:Lcom/google/maps/b/a/cs;

    iget v4, v4, Lcom/google/maps/b/a/cs;->b:I

    if-lez v4, :cond_10

    .line 177
    new-instance v2, Lcom/google/maps/b/a/ax;

    invoke-direct {v2, v3}, Lcom/google/maps/b/a/ax;-><init>(Lcom/google/maps/b/a/av;)V

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/internal/c/p;->a(Ljava/lang/Iterable;)Lcom/google/android/apps/gmm/map/internal/c/p;

    move-result-object v2

    .line 179
    :cond_10
    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->i:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 187
    :cond_11
    :goto_10
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_12

    .line 188
    invoke-virtual/range {v18 .. v18}, Lcom/google/maps/b/a/aj;->e()Lcom/google/maps/b/a/cc;

    move-result-object v3

    .line 189
    iget-object v2, v3, Lcom/google/maps/b/a/cc;->c:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_18

    const/4 v2, 0x1

    :goto_11
    if-nez v2, :cond_19

    iget-object v2, v3, Lcom/google/maps/b/a/cc;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_19

    .line 190
    const/4 v2, 0x3

    iget-object v4, v3, Lcom/google/maps/b/a/cc;->b:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    aput v4, v17, v2

    .line 191
    invoke-virtual {v3}, Lcom/google/maps/b/a/cc;->a()Lcom/google/maps/b/a/dl;

    move-result-object v2

    iget-object v2, v2, Lcom/google/maps/b/a/dl;->a:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->k:I

    .line 192
    invoke-virtual {v3}, Lcom/google/maps/b/a/cc;->a()Lcom/google/maps/b/a/dl;

    move-result-object v2

    iget-object v2, v2, Lcom/google/maps/b/a/dl;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->j:I

    .line 199
    :cond_12
    :goto_12
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->g:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_1c

    .line 200
    invoke-virtual/range {v18 .. v18}, Lcom/google/maps/b/a/aj;->g()Lcom/google/maps/b/a/by;

    move-result-object v3

    .line 201
    iget-object v2, v3, Lcom/google/maps/b/a/by;->c:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v2, :cond_1a

    const/4 v2, 0x1

    :goto_13
    if-nez v2, :cond_1e

    .line 202
    const/4 v2, 0x5

    iget-object v4, v3, Lcom/google/maps/b/a/by;->b:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    aput v4, v17, v2

    .line 203
    iget-object v2, v3, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    iget v4, v2, Lcom/google/maps/b/a/cw;->b:I

    .line 204
    const/16 v2, 0x14

    if-ne v4, v2, :cond_1d

    .line 205
    new-array v5, v4, [F

    .line 206
    const/4 v2, 0x0

    :goto_14
    if-ge v2, v4, :cond_1b

    .line 207
    iget-object v6, v3, Lcom/google/maps/b/a/by;->a:Lcom/google/maps/b/a/cw;

    iget-object v6, v6, Lcom/google/maps/b/a/cw;->a:[F

    aget v6, v6, v2

    aput v6, v5, v2

    .line 208
    rem-int/lit8 v6, v2, 0x5

    const/4 v7, 0x4

    if-ne v6, v7, :cond_13

    .line 209
    aget v6, v5, v2

    const/high16 v7, 0x437f0000    # 255.0f

    mul-float/2addr v6, v7

    aput v6, v5, v2

    .line 206
    :cond_13
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    .line 158
    :cond_14
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->e:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    goto/16 :goto_d

    .line 164
    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_e

    .line 174
    :cond_16
    const/4 v4, 0x0

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->h:Lcom/google/android/apps/gmm/map/internal/c/bm;

    goto/16 :goto_f

    .line 181
    :cond_17
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->g:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 182
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->h:Lcom/google/android/apps/gmm/map/internal/c/bm;

    .line 183
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->i:Lcom/google/android/apps/gmm/map/internal/c/p;

    goto/16 :goto_10

    .line 189
    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_11

    .line 194
    :cond_19
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->k:I

    .line 195
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->j:I

    goto :goto_12

    .line 201
    :cond_1a
    const/4 v2, 0x0

    goto :goto_13

    .line 212
    :cond_1b
    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->l:[F

    .line 221
    :cond_1c
    :goto_15
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->a:I

    .line 222
    const/4 v2, 0x6

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->m:[I

    .line 223
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/be;

    move-object/from16 v0, v16

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bg;)V

    aput-object v2, v9, v11

    .line 224
    aget-object v2, v9, v11

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/maps/b/a/aj;->a:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    int-to-byte v2, v2

    aput-byte v2, v10, v11

    .line 106
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto/16 :goto_3

    .line 214
    :cond_1d
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/be;->a:[F

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->l:[F

    goto :goto_15

    .line 217
    :cond_1e
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bg;->l:[F

    goto :goto_15

    :cond_1f
    move-object v2, v9

    move-object v3, v10

    .line 228
    :cond_20
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bi;

    invoke-direct {v4, v14, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bi;-><init>(Ljava/lang/String;[Lcom/google/android/apps/gmm/map/internal/c/be;[B)V

    iget-object v2, v12, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 230
    :cond_21
    return-object v12
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 1

    .prologue
    .line 238
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 240
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bi;->a()Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v0

    .line 242
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bi;

    goto :goto_0
.end method
