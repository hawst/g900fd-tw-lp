.class public Lcom/google/android/apps/gmm/login/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Lcom/google/android/apps/gmm/login/a/a;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Landroid/accounts/AccountManager;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field e:Z

.field final f:Ljava/lang/Object;

.field final g:Ljava/lang/Object;

.field final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/login/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/android/apps/gmm/shared/net/p;

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/shared/net/q;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/google/android/apps/gmm/login/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Landroid/accounts/AccountManager;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/a;->j:Ljava/util/Map;

    .line 144
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    .line 158
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/a;->g:Ljava/lang/Object;

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 165
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/a;->h:Ljava/util/Map;

    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/login/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/login/c;-><init>(Lcom/google/android/apps/gmm/login/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/login/a;->k:Ljava/lang/Object;

    .line 178
    iput-object p1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    .line 179
    iput-object p2, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    .line 180
    iput-object p3, p0, Lcom/google/android/apps/gmm/login/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 181
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 483
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 487
    :goto_1
    return-object v0

    .line 482
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 487
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/a;)V
    .locals 3

    .prologue
    .line 94
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 95
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/gmm/l;->cf:I

    .line 96
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->jE:I

    .line 97
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/login/b;

    invoke-direct {v2, p1, p0}, Lcom/google/android/apps/gmm/login/b;-><init>(Lcom/google/android/apps/gmm/login/a/a;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;Z)V
    .locals 3

    .prologue
    .line 71
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/login/h;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/gmm/login/h;-><init>(Lcom/google/android/apps/gmm/login/a;ZLcom/google/android/apps/gmm/login/a/b;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/shared/net/p;)V
    .locals 0

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/shared/net/p;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/shared/net/p;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 327
    iget-object v2, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    .line 329
    if-nez p1, :cond_3

    .line 330
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 331
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->g()Landroid/accounts/Account;

    move-result-object v3

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_4

    .line 332
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    :cond_2
    :goto_1
    return-void

    .line 329
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    goto :goto_0

    .line 334
    :cond_4
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    .line 335
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 336
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->j:Ljava/util/Map;

    const-string v3, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    invoke-interface {v1, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/login/a;->e:Z

    .line 338
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/q;)V

    .line 339
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    if-eqz v0, :cond_2

    .line 341
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/a/a;->a:[Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/gmm/login/f;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/gmm/login/f;-><init>(Lcom/google/android/apps/gmm/login/a;Landroid/accounts/Account;)V

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/gms/common/a/a;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    .line 342
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/login/k;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/login/k;-><init>(Lcom/google/android/apps/gmm/login/a;Landroid/accounts/Account;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_1

    .line 339
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 341
    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/login/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/login/e;-><init>(Lcom/google/android/apps/gmm/login/a;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->k:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/login/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/login/d;-><init>(Lcom/google/android/apps/gmm/login/a;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 202
    return-void
.end method

.method public final a(Landroid/app/Activity;ILandroid/content/Intent;)V
    .locals 4
    .param p3    # Landroid/content/Intent;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 550
    if-eqz p3, :cond_0

    .line 551
    const-string v0, "callerExtras"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 552
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 554
    :goto_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_2

    .line 555
    const-string v1, "authAccount"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 556
    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/apps/gmm/login/a;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/gmm/login/a/b;)V

    .line 561
    :cond_0
    :goto_1
    return-void

    .line 552
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->h:Ljava/util/Map;

    const-string v2, "callbackId"

    .line 553
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/login/a/b;

    goto :goto_0

    .line 557
    :cond_2
    if-nez p2, :cond_0

    .line 558
    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/login/h;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/apps/gmm/login/h;-><init>(Lcom/google/android/apps/gmm/login/a;ZLcom/google/android/apps/gmm/login/a/b;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_1
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V
    .locals 8
    .param p2    # Lcom/google/android/apps/gmm/login/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 354
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 355
    const-string v0, "allowSkip"

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    const-string v2, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    new-instance v6, Lcom/google/android/apps/gmm/login/j;

    invoke-direct {v6, p0, p2}, Lcom/google/android/apps/gmm/login/j;-><init>(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;)V

    move-object v5, p1

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 363
    return-void
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 657
    new-instance v0, Lcom/google/android/apps/gmm/login/i;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/login/i;-><init>(Lcom/google/android/apps/gmm/login/a;Ljava/lang/Runnable;)V

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V

    .line 666
    return-void
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/gmm/login/a/b;)V
    .locals 3
    .param p3    # Lcom/google/android/apps/gmm/login/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 493
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/login/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 494
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    if-nez v1, :cond_2

    .line 496
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->oi:I

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 497
    const/4 v0, 0x0

    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/login/h;

    invoke-direct {v2, p0, v0, p3}, Lcom/google/android/apps/gmm/login/h;-><init>(Lcom/google/android/apps/gmm/login/a;ZLcom/google/android/apps/gmm/login/a/b;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 546
    :cond_1
    :goto_0
    return-void

    .line 500
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/login/g;

    invoke-direct {v2, p0, v0, p1, p3}, Lcom/google/android/apps/gmm/login/g;-><init>(Lcom/google/android/apps/gmm/login/a;Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method a(Lcom/google/android/apps/gmm/shared/net/p;)V
    .locals 5

    .prologue
    .line 401
    if-nez p1, :cond_1

    const-string v0, "*"

    .line 402
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 403
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/login/a;->b(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 405
    iget-object v2, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->f:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 406
    :cond_0
    if-eqz p1, :cond_2

    .line 407
    const/4 v2, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/login/a;->a(Ljava/lang/String;I)V

    .line 411
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 401
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    .line 409
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->b()V

    goto :goto_1

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/login/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 248
    if-nez v0, :cond_0

    .line 249
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 254
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/e/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    invoke-direct {v2, v0, p2, v3}, Lcom/google/android/apps/gmm/base/e/c;-><init>(Landroid/accounts/Account;IZ)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method b()V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/e/c;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/base/e/c;-><init>(Landroid/accounts/Account;Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 233
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/login/a;->a(Ljava/lang/String;I)V

    .line 240
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 291
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 292
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/p;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 298
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 299
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    return v0
.end method

.method public final g()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 311
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/p;->b:Landroid/accounts/Account;

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 318
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 319
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->g()Landroid/accounts/Account;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method i()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 448
    .line 449
    iget-object v3, p0, Lcom/google/android/apps/gmm/login/a;->g:Ljava/lang/Object;

    monitor-enter v3

    .line 451
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    if-eqz v1, :cond_4

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->f:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 453
    :goto_0
    if-eqz v2, :cond_3

    const-string v1, "*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 454
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/login/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 457
    :goto_1
    const-string v4, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    if-nez v1, :cond_1

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/login/a;->b(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 458
    monitor-exit v3

    .line 459
    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_0
    move-object v2, v0

    .line 452
    goto :goto_0

    .line 457
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/p;

    iget-object v5, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/login/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v5, v6, v1, v4}, Lcom/google/android/apps/gmm/shared/net/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_2

    .line 458
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 459
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    move-object v1, v0

    goto :goto_1

    :cond_4
    move-object v1, v0

    move-object v2, v0

    goto :goto_1
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 465
    return-void
.end method

.method public final k()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 469
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 471
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 473
    :cond_0
    return-object v1
.end method

.method public final l()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method m()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 584
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    if-eqz v1, :cond_0

    .line 585
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    aget-object v1, v1, v0

    .line 586
    const-string v2, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 587
    const/4 v0, 0x1

    .line 590
    :cond_0
    return v0

    .line 586
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/p;

    iget-object v3, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/login/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/google/android/apps/gmm/shared/net/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 611
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 612
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->i:Lcom/google/android/apps/gmm/shared/net/p;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/login/a;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 613
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method o()V
    .locals 5

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->k()Ljava/util/List;

    move-result-object v1

    instance-of v0, v1, Ljava/util/Collection;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/HashSet;

    invoke-static {v1}, Lcom/google/b/c/an;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v1, v0

    .line 674
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "#"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_0

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 673
    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/jp;->a(Ljava/util/Iterator;)Ljava/util/HashSet;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 674
    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 675
    return-void
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 266
    iget-object v2, p0, Lcom/google/android/apps/gmm/login/a;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 267
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->h()Ljava/lang/String;

    move-result-object v3

    .line 268
    if-nez v3, :cond_0

    .line 269
    monitor-exit v2

    .line 287
    :goto_0
    return-void

    .line 272
    :cond_0
    array-length v4, p1

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, p1, v1

    .line 273
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 274
    const/4 v0, 0x1

    .line 278
    :cond_1
    if-nez v0, :cond_2

    .line 280
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 282
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/login/a;->o()V

    goto :goto_0

    .line 272
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 282
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
