.class public Lcom/google/android/apps/gmm/aa/d/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:[B


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/gmm/aa/d/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/d/p;->b:Ljava/lang/String;

    .line 30
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/gmm/aa/d/p;->c:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/p;->a:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public static a(Ljava/io/File;)Z
    .locals 8

    .prologue
    const/16 v6, 0x46

    const/16 v5, 0x2c

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_9

    .line 54
    const/16 v2, 0x2c

    :try_start_1
    new-array v2, v2, [B

    .line 55
    invoke-virtual {v3, v2}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eq v4, v5, :cond_0

    .line 56
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 93
    :goto_0
    return v0

    .line 59
    :cond_0
    const/4 v4, 0x0

    :try_start_3
    aget-byte v4, v2, v4

    const/16 v5, 0x52

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    aget-byte v4, v2, v4

    const/16 v5, 0x49

    if-ne v4, v5, :cond_1

    const/4 v4, 0x2

    aget-byte v4, v2, v4

    if-ne v4, v6, :cond_1

    const/4 v4, 0x3

    aget-byte v4, v2, v4

    if-ne v4, v6, :cond_1

    const/16 v4, 0x8

    aget-byte v4, v2, v4

    const/16 v5, 0x57

    if-ne v4, v5, :cond_1

    const/16 v4, 0x9

    aget-byte v4, v2, v4

    const/16 v5, 0x41

    if-ne v4, v5, :cond_1

    const/16 v4, 0xa

    aget-byte v4, v2, v4

    const/16 v5, 0x56

    if-ne v4, v5, :cond_1

    const/16 v4, 0xb

    aget-byte v4, v2, v4

    const/16 v5, 0x45

    if-eq v4, v5, :cond_3

    .line 61
    :cond_1
    sget-object v4, Lcom/google/android/apps/gmm/aa/d/p;->b:Ljava/lang/String;

    const-string v5, "File doesn\'t look like a WAV file. Assuming it\'s correct: "

    .line 62
    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    .line 61
    invoke-static {v4, v2, v5}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    :goto_2
    move v0, v1

    .line 91
    goto :goto_0

    .line 62
    :cond_2
    :try_start_5
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 85
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 66
    :cond_3
    const/16 v4, 0x28

    :try_start_7
    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    const/16 v5, 0x29

    aget-byte v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/16 v5, 0x2a

    aget-byte v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/16 v5, 0x2b

    aget-byte v2, v2, v5

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v2, v4

    .line 69
    if-lez v2, :cond_4

    add-int/lit8 v4, v2, 0x2c

    int-to-long v4, v4

    invoke-virtual {p0}, Ljava/io/File;->length()J
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 70
    :cond_4
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto/16 :goto_0

    .line 75
    :cond_5
    :try_start_9
    sget-object v4, Lcom/google/android/apps/gmm/aa/d/p;->c:[B

    const/high16 v4, 0x10000

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 76
    sget-object v4, Lcom/google/android/apps/gmm/aa/d/p;->c:[B

    const/high16 v4, 0x10000

    new-array v4, v4, [B

    .line 77
    invoke-virtual {v3, v4}, Ljava/io/FileInputStream;->read([B)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v5

    if-eq v5, v2, :cond_6

    .line 78
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v1

    goto/16 :goto_0

    .line 81
    :cond_6
    :try_start_b
    sget-object v2, Lcom/google/android/apps/gmm/aa/d/p;->c:[B

    invoke-static {v4, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result v2

    if-eqz v2, :cond_7

    .line 82
    :try_start_c
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto/16 :goto_0

    .line 88
    :cond_7
    :try_start_d
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    :goto_3
    move v0, v1

    .line 93
    goto/16 :goto_0

    .line 87
    :catchall_0
    move-exception v0

    .line 88
    :try_start_e
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    .line 91
    :goto_4
    throw v0

    :catch_5
    move-exception v1

    goto/16 :goto_0

    :catch_6
    move-exception v0

    goto :goto_2

    .line 92
    :catch_7
    move-exception v0

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_4

    .line 51
    :catch_9
    move-exception v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 127
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/p;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    if-nez v1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    .line 136
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v4, v1

    .line 137
    mul-long/2addr v2, v4

    .line 138
    const-wide/32 v4, 0x80000

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 130
    :catch_0
    move-exception v1

    goto :goto_0
.end method
