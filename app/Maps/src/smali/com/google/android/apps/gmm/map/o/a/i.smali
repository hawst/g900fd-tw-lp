.class public Lcom/google/android/apps/gmm/map/o/a/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/a/a;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/o/b/a;

.field private final b:[F

.field private final c:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final d:Lcom/google/android/apps/gmm/map/o/a/c;

.field private final e:Lcom/google/android/apps/gmm/map/o/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/a/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/i;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 23
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/i;->b:[F

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/i;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/map/o/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/i;->e:Lcom/google/android/apps/gmm/map/o/a/b;

    .line 49
    const-string v0, "placedCallouts"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/o/a/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/a/i;->d:Lcom/google/android/apps/gmm/map/o/a/c;

    .line 50
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/b/a/ab;ILcom/google/android/apps/gmm/map/f/o;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 160
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    invoke-static {p3, v2}, Lcom/google/b/a/aq;->a(II)I

    .line 161
    add-int/lit8 v2, p3, -0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 162
    add-int/lit8 v3, p3, 0x2

    .line 163
    iget-object v4, p2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 165
    :goto_0
    if-ge v2, v3, :cond_1

    .line 166
    invoke-virtual {p2, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 167
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/i;->b:[F

    invoke-virtual {p4, v4, v5}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/a/i;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 172
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/i;->b:[F

    aget v5, v5, v1

    float-to-int v5, v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/a/i;->b:[F

    aget v6, v6, v0

    float-to-int v6, v6

    int-to-float v6, v6

    iput v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v6, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 173
    invoke-virtual {p1, v4}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177
    :goto_1
    return v0

    .line 165
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 177
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 18

    .prologue
    .line 54
    new-instance v9, Lcom/google/android/apps/gmm/map/o/b/a;

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 56
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    .line 57
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    invoke-direct {v9, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    .line 59
    new-instance v10, Lcom/google/android/apps/gmm/map/o/a/j;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/map/o/a/j;-><init>()V

    .line 60
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/i;->e:Lcom/google/android/apps/gmm/map/o/a/b;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/o/a/b;->a(Lcom/google/android/apps/gmm/map/f/o;)F

    move-result v2

    iput v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->e:F

    .line 61
    new-instance v11, Lcom/google/android/apps/gmm/map/o/a/j;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/map/o/a/j;-><init>()V

    .line 64
    new-instance v12, Lcom/google/android/apps/gmm/map/o/a/d;

    .line 65
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b:Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v12, v9, v2}, Lcom/google/android/apps/gmm/map/o/a/d;-><init>(Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/b/a/ab;)V

    .line 66
    iget-object v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 67
    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v2}, Lcom/google/android/apps/gmm/map/o/a/d;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ay;)I

    move-result v2

    iput v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->c:I

    .line 69
    iget v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->c:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 70
    const/4 v2, 0x0

    .line 114
    :cond_0
    :goto_0
    return v2

    .line 75
    :cond_1
    iget-object v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/o/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 77
    const/4 v2, 0x0

    goto :goto_0

    .line 80
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/o/b/f;->values()[Lcom/google/android/apps/gmm/map/o/b/f;

    move-result-object v13

    array-length v14, v13

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v14, :cond_7

    aget-object v2, v13, v8

    .line 81
    iput-object v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/i;->e:Lcom/google/android/apps/gmm/map/o/a/b;

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    iget v4, v10, Lcom/google/android/apps/gmm/map/o/a/j;->e:F

    .line 86
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v6, v10, Lcom/google/android/apps/gmm/map/o/a/j;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 85
    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/o/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/i;->d:Lcom/google/android/apps/gmm/map/o/a/c;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/o/a/c;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 90
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/o/a/i;->a:Lcom/google/android/apps/gmm/map/o/b/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/i;->e:Lcom/google/android/apps/gmm/map/o/a/b;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v4, v10, Lcom/google/android/apps/gmm/map/o/a/j;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    iget v5, v10, Lcom/google/android/apps/gmm/map/o/a/j;->e:F

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/o/a/b;->b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)V

    const/4 v2, 0x0

    invoke-virtual {v9, v7}, Lcom/google/android/apps/gmm/map/o/b/a;->b(Lcom/google/android/apps/gmm/map/o/b/a;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v9, v7}, Lcom/google/android/apps/gmm/map/o/a/b;->a(Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)F

    move-result v2

    float-to-double v4, v2

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    cmpg-double v3, v4, v16

    if-gez v3, :cond_5

    const/16 v2, 0x3e8

    :cond_3
    :goto_2
    iput v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    .line 94
    iget v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    iget v3, v11, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    if-ge v2, v3, :cond_4

    .line 95
    iget-object v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->b:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v4, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    iput-object v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    iget v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->c:I

    iput v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->c:I

    iget v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->e:F

    iput v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->e:F

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/o/a/j;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    iget v4, v3, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v5, v3, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v6, v3, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-virtual {v2, v4, v5, v6, v3}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    iget v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    iput v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    .line 98
    iget v2, v10, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    if-eqz v2, :cond_7

    .line 99
    :cond_4
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_1

    .line 90
    :cond_5
    const/4 v3, 0x0

    const/high16 v4, 0x42200000    # 40.0f

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v2, v5, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    :cond_6
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b:Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v4, v10, Lcom/google/android/apps/gmm/map/o/a/j;->c:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v7, v3, v4, v1}, Lcom/google/android/apps/gmm/map/o/a/i;->a(Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/b/a/ab;ILcom/google/android/apps/gmm/map/f/o;)Z

    move-result v3

    if-eqz v3, :cond_3

    add-int/lit8 v2, v2, 0x5

    goto :goto_2

    .line 104
    :cond_7
    iget v2, v11, Lcom/google/android/apps/gmm/map/o/a/j;->g:I

    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_8

    const/4 v2, 0x1

    .line 106
    :goto_3
    if-eqz v2, :cond_0

    .line 107
    new-instance v3, Lcom/google/android/apps/gmm/map/o/a/k;

    iget v4, v11, Lcom/google/android/apps/gmm/map/o/a/j;->c:I

    invoke-direct {v3, v12, v4}, Lcom/google/android/apps/gmm/map/o/a/k;-><init>(Lcom/google/android/apps/gmm/map/o/a/d;I)V

    .line 109
    iget-object v4, v11, Lcom/google/android/apps/gmm/map/o/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, v11, Lcom/google/android/apps/gmm/map/o/a/j;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    iget v6, v11, Lcom/google/android/apps/gmm/map/o/a/j;->e:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/n;)V

    .line 111
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/i;->d:Lcom/google/android/apps/gmm/map/o/a/c;

    iget-object v4, v11, Lcom/google/android/apps/gmm/map/o/a/j;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/a/c;->a:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 104
    :cond_8
    const/4 v2, 0x0

    goto :goto_3
.end method
