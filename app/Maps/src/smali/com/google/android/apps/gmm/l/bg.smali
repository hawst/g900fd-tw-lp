.class public Lcom/google/android/apps/gmm/l/bg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/ab;


# instance fields
.field private a:Lcom/google/android/apps/gmm/l/ae;

.field private b:Lcom/google/android/apps/gmm/aa/a/a/a;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 90
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 93
    :goto_0
    return v0

    .line 92
    :catch_0
    move-exception v0

    const-string v0, "Unparseable value: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 93
    :goto_1
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 92
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static c(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    const-string v1, "google.maps"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "google.maps:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 54
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/bg;->c(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 59
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/bg;->c(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 61
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 62
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    .line 85
    :goto_1
    return-object v0

    :cond_2
    move v2, v1

    .line 62
    goto :goto_0

    .line 65
    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/l/bf;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/l/bf;-><init>()V

    .line 67
    invoke-virtual {v2, v3}, Landroid/net/UrlQuerySanitizer;->parseUrl(Ljava/lang/String;)V

    .line 68
    const-string v3, "act"

    invoke-virtual {v2, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 70
    if-nez v3, :cond_4

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto :goto_1

    .line 75
    :cond_4
    invoke-static {v3}, Lcom/google/android/apps/gmm/l/bg;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/aa/a/a/a;->a(I)Lcom/google/android/apps/gmm/aa/a/a/a;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/l/bg;->b:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 78
    const-string v3, "entry"

    invoke-virtual {v2, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/l/ae;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/l/bg;->a:Lcom/google/android/apps/gmm/l/ae;

    .line 81
    const-string v3, "notts"

    invoke-virtual {v2, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/l/bg;->a(Ljava/lang/String;)I

    move-result v2

    .line 82
    if-eq v2, v0, :cond_5

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/l/bg;->c:Z

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/l/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/l/z;-><init>()V

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->l:Lcom/google/android/apps/gmm/l/y;

    iput-object v1, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/bg;->b:Lcom/google/android/apps/gmm/aa/a/a/a;

    iput-object v1, v0, Lcom/google/android/apps/gmm/l/z;->x:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/l/bg;->c:Z

    .line 85
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/l/z;->y:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/bg;->a:Lcom/google/android/apps/gmm/l/ae;

    iput-object v1, v0, Lcom/google/android/apps/gmm/l/z;->v:Lcom/google/android/apps/gmm/l/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 82
    goto :goto_2
.end method
