.class public Lcom/google/android/apps/gmm/navigation/a/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/g;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final c:Lcom/google/android/apps/gmm/shared/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/z;->a:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/z;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 28
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/z;->c:Lcom/google/android/apps/gmm/shared/b/a;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/z;->c:Lcom/google/android/apps/gmm/shared/b/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/navigation/a/c;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/z;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/z;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/a/aa;->a:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/a/b/j;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget v2, Lcom/google/android/apps/gmm/k;->p:I

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/a/al;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/google/android/apps/gmm/k;->b:I

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/a/al;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    sget v2, Lcom/google/android/apps/gmm/k;->c:I

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/a/al;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
