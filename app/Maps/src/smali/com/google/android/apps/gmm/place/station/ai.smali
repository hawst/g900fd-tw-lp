.class public Lcom/google/android/apps/gmm/place/station/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/c;


# static fields
.field private static final t:Landroid/view/View$OnClickListener;


# instance fields
.field final a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/b/a/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/map/b/a/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final f:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation
.end field

.field final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final h:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final i:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final j:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final k:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final l:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field m:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final n:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final o:Ljava/lang/Boolean;

.field private final p:Lcom/google/android/apps/gmm/place/station/b/d;

.field private final q:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final r:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/google/android/libraries/curvular/cg;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/place/station/aj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/aj;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ai;->t:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/vk;Ljava/util/List;)V
    .locals 12
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p10    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p11    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p12    # Lcom/google/android/libraries/curvular/cg;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;",
            "Lcom/google/maps/g/vk;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-eqz p12, :cond_0

    move-object/from16 v0, p12

    instance-of v1, v0, Ljava/io/Serializable;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const-string v2, "onTimeTableClickListener should be either null or Serializable."

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 113
    :cond_2
    if-eqz p14, :cond_3

    move-object/from16 v0, p14

    instance-of v1, v0, Ljava/io/Serializable;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    const-string v2, "notices should be non-null and Serializable."

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 117
    :cond_4
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/station/ai;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 118
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/station/ai;->d:Ljava/lang/String;

    .line 119
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 120
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->f:Ljava/util/List;

    .line 121
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->g:Ljava/lang/String;

    .line 122
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->h:Lcom/google/android/libraries/curvular/a;

    .line 123
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->i:Lcom/google/android/libraries/curvular/a;

    .line 124
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->j:Lcom/google/android/libraries/curvular/a;

    .line 125
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->k:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 126
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 127
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->r:Ljava/util/List;

    .line 128
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->s:Lcom/google/android/libraries/curvular/cg;

    .line 130
    invoke-virtual/range {p13 .. p13}, Lcom/google/maps/g/vk;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->a:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p13

    .line 131
    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/gmm/place/station/ai;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/maps/g/vk;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    .line 143
    new-instance v1, Lcom/google/android/apps/gmm/place/station/am;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/place/station/am;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->p:Lcom/google/android/apps/gmm/place/station/b/d;

    .line 145
    const/4 v2, 0x0

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/place/station/b/d;

    .line 147
    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/station/b/d;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 148
    const/4 v1, 0x1

    .line 152
    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->o:Ljava/lang/Boolean;

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/v;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->n:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->n:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_8

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 158
    invoke-static {p0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    .line 157
    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/v;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->q:Ljava/lang/String;

    .line 160
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->m:Ljava/lang/Boolean;

    .line 161
    return-void

    .line 155
    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    goto :goto_4

    :cond_9
    move v1, v2

    goto :goto_2
.end method

.method static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/maps/g/vk;)Lcom/google/b/c/cv;
    .locals 15
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Lcom/google/maps/g/vk;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v13

    .line 285
    invoke-virtual/range {p10 .. p10}, Lcom/google/maps/g/vk;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/maps/g/vf;

    .line 286
    new-instance v0, Lcom/google/android/apps/gmm/place/station/am;

    .line 298
    invoke-virtual/range {p10 .. p10}, Lcom/google/maps/g/vk;->d()Ljava/lang/String;

    move-result-object v11

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/gmm/place/station/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Ljava/lang/String;Lcom/google/maps/g/vf;)V

    .line 286
    invoke-virtual {v13, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 301
    :cond_0
    invoke-virtual {v13}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 165
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    .line 166
    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/ai;->d:Ljava/lang/String;

    .line 167
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ai;->t:Landroid/view/View$OnClickListener;

    .line 168
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 169
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    return-object v1
.end method

.method public final a(I)Lcom/google/android/apps/gmm/place/station/b/d;
    .locals 1

    .prologue
    .line 242
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    .line 243
    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/d;

    :goto_1
    return-object v0

    .line 242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->p:Lcom/google/android/apps/gmm/place/station/b/d;

    goto :goto_1
.end method

.method public final b(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 249
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->h:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->i:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->j:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->k:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->b:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->s:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->m:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->r:Ljava/util/List;

    return-object v0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ai;->o:Ljava/lang/Boolean;

    return-object v0
.end method
