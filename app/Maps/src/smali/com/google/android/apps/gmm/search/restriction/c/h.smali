.class public Lcom/google/android/apps/gmm/search/restriction/c/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;
.implements Lcom/google/android/apps/gmm/search/restriction/d/d;


# instance fields
.field private a:Lcom/google/android/apps/gmm/search/restriction/c/i;

.field private final b:Landroid/content/res/Resources;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    sget-object v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 83
    iget v0, p2, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->c:Ljava/lang/Boolean;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->b:Landroid/content/res/Resources;

    .line 86
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/search/restriction/c/h;->a(Lcom/google/r/b/a/alh;)V

    .line 87
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 3

    .prologue
    .line 99
    invoke-static {}, Lcom/google/maps/g/sj;->d()Lcom/google/maps/g/sj;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/qo;->e:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p1, Lcom/google/maps/g/qo;->a:I

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/i;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->e:Lcom/google/maps/g/pz;

    invoke-virtual {p1, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/pz;)Lcom/google/r/b/a/alm;

    move-result-object p1

    .line 94
    :cond_0
    return-object p1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v1

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 161
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/restriction/c/i;->ordinal()I

    move-result v1

    if-ne v1, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 1

    .prologue
    .line 109
    iget v0, p1, Lcom/google/r/b/a/alh;->h:I

    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/search/restriction/c/i;->a(Lcom/google/maps/g/pz;)Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 110
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->d:Ljava/lang/Runnable;

    .line 115
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->g:I

    .line 134
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/restriction/c/i;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/i;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 170
    const-string v0, ""

    .line 173
    :goto_0
    return-object v0

    .line 172
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/c;->e:I

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->b:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->a:Lcom/google/android/apps/gmm/search/restriction/c/i;

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 193
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->lR:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 203
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 204
    const/4 v0, 0x0

    .line 206
    :goto_0
    return-object v0

    .line 207
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/restriction/c/i;->f:Lcom/google/b/f/t;

    .line 206
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->lF:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 178
    invoke-static {}, Lcom/google/android/apps/gmm/search/restriction/c/i;->values()[Lcom/google/android/apps/gmm/search/restriction/c/i;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 179
    const-string v0, ""

    .line 182
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/h;->b:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->f:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final g(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 212
    if-lez p1, :cond_0

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
