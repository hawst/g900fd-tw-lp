.class public Lcom/google/android/apps/gmm/streetview/internal/w;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/v/g;

.field private final c:Lcom/google/android/apps/gmm/streetview/internal/x;

.field private d:F

.field private e:F

.field private f:I


# direct methods
.method public constructor <init>(Ljava/util/Collection;FFILcom/google/android/apps/gmm/streetview/internal/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;FFI",
            "Lcom/google/android/apps/gmm/streetview/internal/x;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    .line 50
    iput p2, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->d:F

    .line 51
    sub-float v0, p2, p3

    int-to-float v1, p4

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->e:F

    .line 52
    iput p4, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->f:I

    .line 53
    iput-object p5, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->c:Lcom/google/android/apps/gmm/streetview/internal/x;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->b:Lcom/google/android/apps/gmm/v/g;

    .line 59
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 93
    if-eqz v0, :cond_0

    .line 94
    sget-object v4, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    if-ne v4, v1, :cond_3

    const v3, 0xffff

    :cond_1
    move v4, v5

    .line 95
    :goto_0
    const/16 v1, 0x10

    if-gt v4, v1, :cond_0

    .line 96
    and-int v1, v4, v3

    if-eqz v1, :cond_2

    .line 97
    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/r;

    .line 98
    iget v7, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->d:F

    invoke-interface {v1, v7}, Lcom/google/android/apps/gmm/v/r;->a(F)V

    .line 95
    :cond_2
    shl-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_3
    move v1, v2

    move v3, v2

    .line 94
    :goto_1
    sget v7, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v1, v7, :cond_1

    iget-object v7, v0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v7, v7, v1

    iget v8, v4, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v7, v7, v8

    if-eqz v7, :cond_4

    shl-int v7, v5, v1

    or-int/2addr v3, v7

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 103
    :cond_5
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->b:Lcom/google/android/apps/gmm/v/g;

    if-nez v1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->d:F

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->e:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->d:F

    .line 78
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->f:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->f:I

    if-lez v1, :cond_2

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 80
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->c:Lcom/google/android/apps/gmm/streetview/internal/x;

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/w;->c:Lcom/google/android/apps/gmm/streetview/internal/x;

    invoke-interface {v1, p0}, Lcom/google/android/apps/gmm/streetview/internal/x;->a(Lcom/google/android/apps/gmm/streetview/internal/w;)V

    goto :goto_0
.end method
