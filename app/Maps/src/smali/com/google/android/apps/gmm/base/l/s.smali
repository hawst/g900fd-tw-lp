.class public Lcom/google/android/apps/gmm/base/l/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/c/a/d;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/aw;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/o/h/a/gx;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/o/h/a/gx;Z)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 23
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/s;->c:Lcom/google/o/h/a/gx;

    .line 24
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 25
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->a:Lcom/google/android/libraries/curvular/aw;

    .line 26
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/base/l/s;->d:Z

    .line 27
    return-void

    .line 25
    :cond_0
    iget-object v0, p2, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/d;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->c:Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->a:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public c()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/startpage/a/e;->b(Z)V

    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/startpage/a/e;->b(Z)V

    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/b/f/t;->T:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/b/f/t;->U:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->c:Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/s;->c:Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/s;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
