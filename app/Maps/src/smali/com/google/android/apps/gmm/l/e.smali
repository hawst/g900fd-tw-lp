.class public Lcom/google/android/apps/gmm/l/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field b:Lcom/google/android/apps/gmm/l/y;

.field c:Lcom/google/android/apps/gmm/map/r/a/ap;

.field d:[Lcom/google/android/apps/gmm/map/r/a/ap;

.field e:Lcom/google/maps/g/a/hm;

.field f:Ljava/lang/String;

.field g:I

.field h:Lcom/google/android/apps/gmm/l/aa;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    const-string v0, "+"

    const-string v1, "%20"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/e;->a:Ljava/lang/String;

    .line 172
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/l/e;->g:I

    .line 173
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/aa;)Landroid/net/Uri;
    .locals 10
    .param p0    # Lcom/google/maps/g/a/hm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/l/aa;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/16 v9, 0x31

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 856
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 858
    :cond_0
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 859
    const/4 v0, 0x0

    .line 899
    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    .line 858
    goto :goto_0

    .line 862
    :cond_2
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "google.navigation"

    .line 863
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "/"

    .line 864
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 867
    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->b:Lcom/google/android/apps/gmm/l/aa;

    if-ne p3, v0, :cond_7

    .line 868
    const-string v0, "target"

    const-string v4, "d"

    invoke-virtual {v3, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 876
    :cond_3
    :goto_2
    invoke-static {p0}, Lcom/google/android/apps/gmm/l/a/a;->a(Lcom/google/maps/g/a/hm;)Ljava/lang/String;

    move-result-object v0

    .line 877
    const-string v4, "mode"

    if-eqz v0, :cond_9

    :goto_3
    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 882
    if-eqz p1, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    .line 883
    const-string v0, "sll"

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 884
    iget-object v6, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 883
    invoke-virtual {v3, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 888
    :cond_4
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 889
    const-string v0, "q"

    iget-object v4, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 892
    :cond_5
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_b

    move v0, v1

    :goto_5
    if-eqz v0, :cond_6

    .line 894
    const-string v0, "ll"

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 895
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 894
    invoke-virtual {v3, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 899
    :cond_6
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    .line 869
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    if-ne p3, v0, :cond_8

    .line 870
    const-string v0, "target"

    const-string v4, "n"

    invoke-virtual {v3, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_2

    .line 871
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->a:Lcom/google/android/apps/gmm/l/aa;

    if-ne p3, v0, :cond_3

    .line 872
    const-string v0, "target"

    const-string v4, "c"

    invoke-virtual {v3, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto/16 :goto_2

    .line 877
    :cond_9
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 879
    invoke-static {v0}, Lcom/google/android/apps/gmm/l/a/a;->a(Lcom/google/maps/g/a/hm;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_a
    move v0, v2

    .line 882
    goto/16 :goto_4

    :cond_b
    move v0, v2

    .line 892
    goto :goto_5
.end method

.method static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/n;
    .locals 8

    .prologue
    const-wide v6, 0x412e848000000000L    # 1000000.0

    const/4 v0, 0x0

    .line 419
    if-nez p0, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-object v0

    .line 422
    :cond_1
    const-string v1, ","

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 424
    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 426
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 427
    const/4 v4, 0x1

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 428
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/n;

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/n;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 430
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 373
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/e;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/n;

    move-result-object v1

    .line 376
    if-nez p0, :cond_0

    if-nez v1, :cond_0

    .line 409
    :goto_0
    return-object v0

    .line 386
    :cond_0
    if-eqz p0, :cond_1

    if-eqz v1, :cond_1

    if-nez p2, :cond_1

    move-object p2, p0

    move-object p0, v0

    .line 391
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 392
    if-eqz v1, :cond_2

    .line 393
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/n;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 395
    :cond_2
    if-eqz p0, :cond_3

    .line 396
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 398
    :cond_3
    if-eqz p2, :cond_5

    .line 399
    iput-object p2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 406
    :goto_1
    if-eqz p3, :cond_4

    .line 407
    invoke-static {p3, v0}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/r/a/aq;)V

    .line 409
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_0

    .line 401
    :cond_5
    if-eqz p0, :cond_6

    .line 402
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 404
    :cond_6
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    goto :goto_1
.end method

.method static a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 309
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_0

    .line 323
    :goto_0
    return-object v0

    .line 314
    :cond_0
    invoke-virtual {p0, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    if-nez v0, :cond_1

    move-object v0, v1

    .line 316
    goto :goto_0

    .line 320
    :cond_1
    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 321
    invoke-static {v0}, Lcom/google/android/apps/gmm/l/ac;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 323
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method
