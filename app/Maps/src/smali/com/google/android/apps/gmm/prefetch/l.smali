.class public Lcom/google/android/apps/gmm/prefetch/l;
.super Lcom/google/android/apps/gmm/prefetch/m;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/apps/gmm/prefetch/m;-><init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/l;->c:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    sget-object v4, Lcom/google/android/apps/gmm/prefetch/k;->a:Lcom/google/android/apps/gmm/prefetch/k;

    if-eq v3, v4, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    sget-object v3, Lcom/google/android/apps/gmm/prefetch/k;->b:Lcom/google/android/apps/gmm/prefetch/k;

    if-ne v0, v3, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    move v0, v1

    .line 29
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/l;->b:Lcom/google/android/apps/gmm/map/internal/d/be;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/d/be;->f()Z

    move-result v4

    .line 30
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/l;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v3, :cond_5

    move v3, v2

    .line 32
    :goto_2
    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    if-nez v3, :cond_2

    .line 33
    :cond_1
    const-string v5, "PrefetcherService"

    const-string v6, "Force prefetch not started: isDiskCacheReady = %b, isNetworkConnected = %b, isPrefetcherAvailable = %b"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    .line 35
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v1

    const/4 v1, 0x2

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v7, v1

    .line 33
    invoke-static {v5, v6, v7}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 39
    :cond_2
    return v1

    :cond_3
    move v0, v2

    .line 28
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    .line 30
    :cond_5
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    goto :goto_2
.end method

.method protected final b()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/l;->b:Lcom/google/android/apps/gmm/map/internal/d/be;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/be;->f()Z

    move-result v3

    .line 45
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/l;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v2, :cond_2

    move v2, v1

    .line 47
    :goto_0
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 48
    :cond_0
    const-string v4, "PrefetcherService"

    const-string v5, "Force prefetch suspended: isDiskCacheReady = %b, isNetworkConnected = %b"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    .line 50
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v6, v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v6, v0

    .line 48
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 53
    :cond_1
    return v0

    .line 45
    :cond_2
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    goto :goto_0
.end method
