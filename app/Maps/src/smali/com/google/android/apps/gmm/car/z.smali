.class public Lcom/google/android/apps/gmm/car/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/y;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/base/a;

.field private final c:Lcom/google/android/apps/gmm/map/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/car/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/base/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/z;->b:Lcom/google/android/apps/gmm/base/a;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/z;->c:Lcom/google/android/apps/gmm/map/t;

    .line 41
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/z;->d:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/f/c;)V
    .locals 14
    .param p4    # Lcom/google/android/apps/gmm/directions/f/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 47
    sget-object v2, Lcom/google/android/apps/gmm/car/z;->a:Ljava/lang/String;

    .line 49
    new-instance v5, Lcom/google/android/apps/gmm/directions/aa;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/z;->b:Lcom/google/android/apps/gmm/base/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/z;->d:Landroid/content/Context;

    .line 50
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v5, v2, v3}, Lcom/google/android/apps/gmm/directions/aa;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;)V

    .line 51
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v2

    iget v3, v2, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v2, Lcom/google/maps/g/ia;->a:I

    move/from16 v0, p3

    iput-boolean v0, v2, Lcom/google/maps/g/ia;->f:Z

    invoke-virtual {v2}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/hy;

    .line 53
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/z;->c:Lcom/google/android/apps/gmm/map/t;

    if-nez v3, :cond_3

    const/4 v3, 0x0

    .line 54
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v6

    .line 55
    iget-object v4, p0, Lcom/google/android/apps/gmm/car/z;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;

    move-result-object v7

    .line 56
    sget-object v8, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    .line 57
    iget-object v4, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_0
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_6

    .line 58
    iget-object v4, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v4

    if-nez v4, :cond_5

    :cond_1
    const/4 v4, 0x0

    :goto_2
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/f;->e:Lcom/google/r/b/a/afz;

    .line 61
    :goto_3
    if-eqz p4, :cond_2

    .line 63
    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    .line 62
    invoke-static {v8, v4}, Lcom/google/android/apps/gmm/directions/f/b/a;->a(Ljava/util/EnumMap;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;

    move-result-object v4

    .line 65
    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/gmm/car/z;->d:Landroid/content/Context;

    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v10

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v8

    .line 67
    new-instance v9, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    .line 68
    iput-object v3, v9, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    .line 69
    iput-object v6, v9, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 70
    iput-object v3, v9, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    .line 71
    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v3, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v6, v9, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iput-object v4, v9, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    .line 74
    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/f/b;->h:Lcom/google/maps/g/hy;

    .line 75
    iput-object v7, v9, Lcom/google/android/apps/gmm/directions/f/b;->f:Lcom/google/maps/g/a/al;

    const/4 v2, 0x1

    .line 76
    iput-boolean v2, v9, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    .line 77
    invoke-virtual {v9}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v2

    .line 78
    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Lcom/google/android/apps/gmm/directions/aa;->a(Lcom/google/android/apps/gmm/directions/f/a;Ljava/lang/String;)V

    .line 79
    return-void

    .line 53
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/z;->c:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v3

    goto :goto_0

    .line 57
    :cond_4
    iget-object v4, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v4

    goto :goto_1

    .line 58
    :cond_5
    iget-object v4, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v4

    goto :goto_2

    :cond_6
    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iget-object v9, p0, Lcom/google/android/apps/gmm/car/z;->b:Lcom/google/android/apps/gmm/base/a;

    .line 60
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v9

    .line 59
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v10

    invoke-static {v4, v8, v9, v10}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;

    move-result-object v4

    goto :goto_3
.end method
