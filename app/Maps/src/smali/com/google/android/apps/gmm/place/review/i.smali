.class Lcom/google/android/apps/gmm/place/review/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/review/h;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/review/h;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 487
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->e:Lcom/google/android/apps/gmm/z/a/b;

    sget-object v1, Lcom/google/b/f/t;->fF:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/review/h;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget v3, v3, Lcom/google/android/apps/gmm/place/review/h;->e:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v4, v4, Lcom/google/android/apps/gmm/place/review/h;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v5, v5, Lcom/google/android/apps/gmm/place/review/h;->c:Ljava/lang/String;

    .line 491
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/review/l;

    move-result-object v1

    .line 490
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 496
    :cond_0
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    .line 498
    return-void

    .line 490
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 492
    :cond_2
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/i;->a:Lcom/google/android/apps/gmm/place/review/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->e:Lcom/google/android/apps/gmm/z/a/b;

    sget-object v1, Lcom/google/b/f/t;->fE:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1
.end method
