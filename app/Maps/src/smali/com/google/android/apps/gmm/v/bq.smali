.class public Lcom/google/android/apps/gmm/v/bq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/be;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:J

.field private E:J

.field private F:J

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/br;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:I

.field private d:[D

.field private e:J

.field private f:J

.field private g:I

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:J

.field private s:J

.field private t:J

.field private u:J

.field private v:J

.field private w:J

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/bq;->b:Z

    .line 40
    iput v1, p0, Lcom/google/android/apps/gmm/v/bq;->c:I

    .line 45
    const/16 v0, 0xa

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bq;->d:[D

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/bq;->h:Z

    .line 70
    iput v1, p0, Lcom/google/android/apps/gmm/v/bq;->i:I

    .line 75
    iput v1, p0, Lcom/google/android/apps/gmm/v/bq;->j:I

    .line 80
    iput v1, p0, Lcom/google/android/apps/gmm/v/bq;->k:I

    .line 85
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bq;->l:Ljava/util/Set;

    .line 90
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bq;->m:Ljava/util/Set;

    .line 135
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bq;->a:Ljava/util/List;

    .line 184
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->F:J

    .line 400
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->n:J

    .line 191
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bq;->h:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->f:J

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/bq;->h:Z

    .line 195
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 250
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->j:I

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bq;->l:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 252
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/bh;)V
    .locals 0

    .prologue
    .line 326
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/f;)V
    .locals 0

    .prologue
    .line 316
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 201
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/v/bq;->n:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->t:J

    .line 202
    iget-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->o:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/v/bq;->t:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->o:J

    .line 203
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->u:J

    .line 204
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 255
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->k:I

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bq;->m:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 257
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/bh;)V
    .locals 0

    .prologue
    .line 331
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/f;)V
    .locals 0

    .prologue
    .line 321
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 210
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/v/bq;->u:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->s:J

    .line 211
    iget-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->p:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/v/bq;->s:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->p:J

    .line 212
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->u:J

    .line 213
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->x:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->x:I

    .line 261
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->y:I

    .line 262
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->q:J

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/v/bq;->u:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->q:J

    .line 220
    iget-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->r:J

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/v/bq;->n:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->r:J

    .line 221
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->g:I

    .line 222
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->e:J

    .line 223
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->z:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->z:I

    .line 266
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->A:I

    .line 267
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->i:I

    .line 230
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->B:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->B:I

    .line 271
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/google/android/apps/gmm/v/bq;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/bq;->i:I

    .line 237
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 274
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->D:J

    .line 275
    return-void
.end method

.method public final h()V
    .locals 32

    .prologue
    .line 278
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->D:J

    sub-long/2addr v2, v4

    .line 279
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->v:J

    add-long/2addr v4, v2

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->v:J

    .line 280
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->w:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->w:J

    .line 282
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->e:J

    const-wide/16 v4, 0x5dc

    sub-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->f:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    .line 283
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->r:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->v:J

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->r:J

    .line 284
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/v/bq;->g:I

    int-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/v/bq;->e:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/v/bq;->f:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    div-double/2addr v2, v4

    .line 285
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/bq;->d:[D

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/v/bq;->c:I

    aput-wide v2, v4, v5

    .line 286
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/v/bq;->c:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->c:I

    .line 287
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/v/bq;->c:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/bq;->d:[D

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 288
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->c:I

    .line 289
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/v/bq;->b:Z

    .line 292
    :cond_0
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/v/bq;->b:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bq;->d:[D

    array-length v2, v2

    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/v/bq;->d:[D

    aget-wide v6, v6, v3

    add-double/2addr v4, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/v/bq;->c:I

    goto :goto_0

    :cond_2
    int-to-double v2, v2

    div-double v8, v4, v2

    new-instance v3, Lcom/google/android/apps/gmm/v/bs;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/v/bq;->C:I

    add-int/lit8 v2, v4, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->C:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/v/bq;->g:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/v/bq;->g:I

    int-to-double v6, v2

    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/apps/gmm/v/bq;->e:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/apps/gmm/v/bq;->f:J

    sub-long/2addr v10, v12

    long-to-double v10, v10

    div-double/2addr v6, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/apps/gmm/v/bq;->r:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/apps/gmm/v/bq;->o:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/apps/gmm/v/bq;->p:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/v/bq;->q:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->i:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bq;->l:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->j:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->B:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/v/bq;->v:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/v/bq;->w:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->x:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->y:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->z:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->A:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bq;->m:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/v/bq;->k:I

    move/from16 v31, v0

    invoke-direct/range {v3 .. v31}, Lcom/google/android/apps/gmm/v/bs;-><init>(IIDDJJJJIIIIJJIIIIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/v/br;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/v/br;->a(Lcom/google/android/apps/gmm/v/bs;)V

    goto :goto_2

    .line 293
    :cond_3
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->o:J

    .line 294
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->p:J

    .line 295
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->q:J

    .line 296
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->r:J

    .line 297
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->j:I

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bq;->l:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 299
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->k:I

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/bq;->m:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 301
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->g:I

    .line 302
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->v:J

    .line 303
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/v/bq;->w:J

    .line 304
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/v/bq;->h:Z

    .line 305
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->x:I

    .line 306
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->y:I

    .line 307
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->z:I

    .line 308
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->A:I

    .line 309
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/v/bq;->B:I

    .line 311
    :cond_4
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 335
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->E:J

    .line 336
    return-void
.end method

.method public final j()V
    .locals 6

    .prologue
    .line 340
    iget-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->F:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/v/bq;->E:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/v/bq;->F:J

    .line 342
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 346
    return-void
.end method
