.class public Lcom/google/android/apps/gmm/base/placelists/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/place/l;

.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field c:Lcom/google/android/apps/gmm/map/b/a;

.field public d:Z

.field e:Z

.field f:Lcom/google/android/apps/gmm/util/a/e;

.field g:Lcom/google/android/apps/gmm/map/p;

.field private final h:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field private final i:Lcom/google/android/apps/gmm/map/d/a;

.field private final j:Lcom/google/android/apps/gmm/base/placelists/o;

.field private k:Lcom/google/android/apps/gmm/map/b/a/q;

.field private l:Lcom/google/android/apps/gmm/map/f/o;

.field private m:Lcom/google/android/apps/gmm/map/b/a/q;

.field private n:Z

.field private o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field private p:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/place/l;Lcom/google/android/apps/gmm/base/placelists/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 79
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->e:Z

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/util/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->f:Lcom/google/android/apps/gmm/util/a/e;

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/q;-><init>(Lcom/google/android/apps/gmm/base/placelists/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->g:Lcom/google/android/apps/gmm/map/p;

    .line 118
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 119
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    .line 120
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->h:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 121
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/placelists/p;->j:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 123
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 124
    if-eqz v0, :cond_0

    .line 125
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    .line 128
    :cond_0
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/s;

    .line 130
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v2

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/placelists/s;-><init>(Lcom/google/android/apps/gmm/base/placelists/p;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->i:Lcom/google/android/apps/gmm/map/d/a;

    .line 194
    return-void

    :cond_1
    move v0, v2

    .line 125
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;)V
    .locals 6

    .prologue
    .line 511
    .line 512
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v5, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    .line 511
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V

    .line 513
    return-void

    .line 512
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V
    .locals 9

    .prologue
    .line 488
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/ah;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 490
    :cond_0
    iget-object v0, p2, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f()Landroid/graphics/Rect;

    move-result-object v4

    .line 491
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gt v0, v1, :cond_2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 494
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    .line 496
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v5, :cond_1

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/ah;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    .line 498
    :goto_0
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g()Landroid/graphics/Rect;

    move-result-object v2

    new-instance v5, Landroid/graphics/Point;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    invoke-direct {v5, v6, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v2, p1

    move v6, p5

    move v7, p3

    move-object v8, p4

    .line 492
    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;FILcom/google/android/apps/gmm/map/p;)V

    .line 507
    :goto_1
    return-void

    .line 496
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 504
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 505
    invoke-static {p1, p5}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;F)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    iput p3, v1, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 504
    const/4 v2, 0x1

    invoke-virtual {v0, v1, p4, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 321
    if-eqz v0, :cond_2

    .line 322
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    if-eqz v1, :cond_1

    .line 323
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/b/a;)V

    .line 324
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->i:Lcom/google/android/apps/gmm/map/d/a;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/d/a;->g:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 325
    iput-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    .line 326
    iput-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 328
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ag;)V

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->j:Lcom/google/android/apps/gmm/base/placelists/o;

    iput-object v5, v0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    .line 332
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;IZ)V
    .locals 7

    .prologue
    .line 215
    const/4 v5, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    :goto_0
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/b/a/q;IZZZ)V

    .line 216
    return-void

    .line 215
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/b/a/q;IZZZ)V
    .locals 14

    .prologue
    .line 223
    if-eqz p1, :cond_12

    if-eqz p2, :cond_12

    .line 225
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_4

    .line 226
    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->b:Lcom/google/android/apps/gmm/map/ag;

    .line 236
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v8, v3, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v8, :cond_d

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    if-nez v3, :cond_8

    const/4 v3, 0x1

    :goto_2
    iget-object v4, v8, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v2, v3}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz p1, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v2, v2, Lcom/google/r/b/a/ads;->p:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ajk;->d()Lcom/google/r/b/a/ajk;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/ajk;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/base/i/f;->a(Lcom/google/r/b/a/ajk;Lcom/google/android/apps/gmm/map/h/f;)Lcom/google/r/b/a/aje;

    move-result-object v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_3
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->p:Z

    if-eqz p5, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->i:Lcom/google/android/apps/gmm/map/d/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/d/a;->g:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v2, 0x0

    if-eqz p5, :cond_2

    if-eqz p6, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->A()Lcom/google/maps/a/a;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->A()Lcom/google/maps/a/a;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/maps/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    :cond_1
    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v4, v3, :cond_a

    sget v3, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    :goto_4
    move/from16 v0, p4

    invoke-virtual {p0, v4, v3, v0, v2}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;IZLjava/lang/Float;)V

    :cond_2
    if-eqz p1, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->j:Lcom/google/android/apps/gmm/base/placelists/o;

    iput-object p1, v2, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    move/from16 v0, p3

    iput v0, v2, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-nez v2, :cond_b

    const/4 v2, 0x0

    :goto_5
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    const/4 v3, 0x0

    if-eqz v2, :cond_e

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    if-nez v2, :cond_c

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 225
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 227
    :cond_4
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v2, v2, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v2, v2, Lcom/google/r/b/a/ads;->F:Z

    if-eqz v2, :cond_5

    .line 228
    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->f:Lcom/google/android/apps/gmm/map/ag;

    goto/16 :goto_1

    .line 229
    :cond_5
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v2, v2, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v2, :cond_6

    .line 230
    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->e:Lcom/google/android/apps/gmm/map/ag;

    goto/16 :goto_1

    .line 231
    :cond_6
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v2, v2, Lcom/google/r/b/a/ads;->F:Z

    if-eqz v2, :cond_7

    .line 232
    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->d:Lcom/google/android/apps/gmm/map/ag;

    goto/16 :goto_1

    .line 234
    :cond_7
    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    goto/16 :goto_1

    .line 236
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v3, -0x1

    goto :goto_4

    :cond_b
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    goto :goto_5

    :cond_c
    new-instance v3, Lcom/google/android/apps/gmm/map/o/ai;

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const-string v6, "http://mt0.google.com/vt/icon/name=icons/spotlight/measle_ad_monroe_spotlight_L.png&scale=4"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;I)V

    invoke-direct {v3, v4, v5, v2}, Lcom/google/android/apps/gmm/map/o/ai;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/c/c;)V

    move-object v2, v3

    :goto_6
    iget-object v3, v8, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ag;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    instance-of v2, v2, Lcom/google/android/apps/gmm/map/t/p;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    check-cast v2, Lcom/google/android/apps/gmm/map/t/p;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    .line 241
    :cond_d
    :goto_7
    return-void

    .line 236
    :cond_e
    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_13

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/base/g/c;->h:Z

    if-nez v2, :cond_f

    const/4 v5, 0x1

    :goto_8
    iget-wide v6, v4, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    if-eqz v5, :cond_10

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const-string v2, "http://mt0.google.com/vt/icon/name=icons/spotlight/measle_spotlight_L.png&scale=4"

    const/4 v3, 0x4

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;I)V

    :goto_9
    new-instance v2, Lcom/google/android/apps/gmm/map/o/ah;

    invoke-static {v10, v11, v12, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/o/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/aa;ZJ)V

    goto :goto_6

    :cond_f
    const/4 v5, 0x0

    goto :goto_8

    :cond_10
    const/4 v4, 0x0

    goto :goto_9

    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->j:Lcom/google/android/apps/gmm/base/placelists/o;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/base/placelists/o;->c:I

    goto :goto_7

    .line 239
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/p;->a()V

    goto :goto_7

    :cond_13
    move-object v2, v3

    goto :goto_6
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 3

    .prologue
    .line 453
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Place sheet dragging started: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 454
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->n:Z

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/base/placelists/u;->a()Lcom/google/android/apps/gmm/base/placelists/u;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 457
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 2

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->n:Z

    if-eqz v0, :cond_0

    .line 447
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_1

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/place/l;->a(Z)V

    .line 449
    :cond_0
    return-void

    .line 447
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 430
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->n:Z

    .line 431
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_4

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    if-eq v3, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/place/l;->a(Z)V

    .line 432
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->e:Z

    if-eqz v0, :cond_1

    .line 433
    sget v0, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    const/4 v3, 0x0

    invoke-virtual {p0, p3, v0, v2, v3}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;IZLjava/lang/Float;)V

    .line 438
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p3, v0, :cond_3

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->j:Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/placelists/o;->a:Lcom/google/android/apps/gmm/z/a/b;

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    sget-object v4, Lcom/google/r/b/a/ru;->d:Lcom/google/r/b/a/ru;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/o;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/z/d;->a(Lcom/google/r/b/a/ru;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/z/d;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-interface {v3, v1}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 442
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 431
    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;IZLjava/lang/Float;)V
    .locals 10
    .param p4    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->h:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p1, v0, :cond_2

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/l;->ad_()V

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->c:Lcom/google/android/apps/gmm/map/b/a;

    if-eqz v0, :cond_0

    .line 353
    if-eqz p3, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/placelists/p;->g:Lcom/google/android/apps/gmm/map/p;

    .line 354
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 355
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v9

    :goto_2
    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 356
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->p:Z

    if-eqz v1, :cond_6

    .line 358
    new-instance v1, Lcom/google/android/apps/gmm/map/f/o;

    .line 359
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->l:Lcom/google/android/apps/gmm/map/f/o;

    .line 362
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 363
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 370
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f()Landroid/graphics/Rect;

    move-result-object v1

    .line 371
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 372
    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/map/c;->b(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput p2, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 371
    const/4 v1, 0x1

    invoke-virtual {v2, v0, v8, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 374
    iput-object v9, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_0

    :cond_3
    move-object v8, v9

    .line 353
    goto :goto_1

    .line 355
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_2

    .line 368
    :cond_5
    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_3

    .line 375
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v0, :cond_b

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->l:Lcom/google/android/apps/gmm/map/f/o;

    if-eqz v0, :cond_8

    .line 378
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->l:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 381
    iget-object v3, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v5, :cond_7

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/ah;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    :goto_4
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 382
    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f()Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 383
    iget-object v5, v5, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->g()Landroid/graphics/Rect;

    move-result-object v6

    new-instance v5, Landroid/graphics/Point;

    invoke-virtual {v6}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    invoke-direct {v5, v7, v6}, Landroid/graphics/Point;-><init>(II)V

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/placelists/p;->l:Lcom/google/android/apps/gmm/map/f/o;

    .line 384
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v6, v6, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move v7, p2

    .line 377
    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Point;FILcom/google/android/apps/gmm/map/p;)V

    .line 387
    iput-object v9, p0, Lcom/google/android/apps/gmm/base/placelists/p;->l:Lcom/google/android/apps/gmm/map/f/o;

    .line 388
    iput-object v9, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto/16 :goto_0

    :cond_7
    move-object v3, v9

    .line 381
    goto :goto_4

    .line 390
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    if-nez p4, :cond_a

    .line 392
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_9

    :goto_5
    iget v9, v9, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move v7, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V

    .line 398
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto/16 :goto_0

    .line 392
    :cond_9
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_5

    .line 395
    :cond_a
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 396
    invoke-virtual {p4}, Ljava/lang/Float;->floatValue()F

    move-result v9

    move v7, p2

    .line 395
    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V

    goto :goto_6

    .line 401
    :cond_b
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v0, :cond_d

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_c

    :goto_7
    iget v9, v9, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move v7, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto/16 :goto_0

    .line 403
    :cond_c
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_7

    .line 406
    :cond_d
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 407
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 409
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/placelists/p;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_e

    :goto_8
    iget v9, v9, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move v7, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/map/MapFragment;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/base/activities/c;ILcom/google/android/apps/gmm/map/p;F)V

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto/16 :goto_0

    .line 409
    :cond_e
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_8
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 461
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x20

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Place sheet dragging ended: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " -> "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-nez v2, :cond_1

    .line 463
    const-string v2, "PlacemarkPinHelper"

    const-string v3, "onDragStarted() should be called before onDragEnded().  PlacemarkPinHelper may have been created after the drag started (http://b/9432190)."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    :goto_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 470
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->n:Z

    .line 472
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/place/l;->a(Z)V

    .line 473
    :cond_0
    return-void

    .line 466
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/p;->j:Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/p;->o:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v2, v3, p2}, Lcom/google/android/apps/gmm/base/placelists/o;->c(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 472
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
