.class public abstract Lcom/google/android/apps/gmm/base/f/am;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/cardui/g/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method

.method public static varargs a(Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/aq;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 10

    .prologue
    const v9, 0xffffff

    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 136
    const/16 v0, 0xb

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 137
    invoke-static {p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v5

    const/4 v0, -0x2

    .line 139
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v6

    const/4 v0, -0x1

    .line 140
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v7

    const/4 v0, 0x3

    const/16 v2, 0x11

    .line 143
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const v3, 0x800003

    .line 144
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    .line 142
    invoke-static {p4, v2, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    .line 147
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const v3, 0x7fffffff

    .line 148
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    .line 146
    invoke-static {p2, v2, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 149
    sget-object v3, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    .line 150
    new-array v2, v7, [Lcom/google/android/libraries/curvular/cu;

    sget v3, Lcom/google/android/apps/gmm/m;->l:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v5

    sget v3, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v2}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x7

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 151
    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v9

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->aw:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    .line 152
    invoke-static {p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 153
    sget v2, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    .line 154
    sget-object v3, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    .line 152
    invoke-static {v0, v2, v3}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v8

    const/16 v0, 0x9

    .line 155
    sget-object v2, Lcom/google/android/libraries/curvular/g;->e:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, p3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xa

    .line 156
    sget-object v2, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 136
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.TextView"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    .line 157
    invoke-virtual {v0, p5}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 151
    :cond_0
    const-wide/high16 v6, 0x4060000000000000L    # 128.0

    mul-double/2addr v4, v6

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v9

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x12

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/libraries/curvular/ar;Z)Lcom/google/android/libraries/curvular/cs;
    .locals 32

    .prologue
    .line 55
    .line 56
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/cardui/g/e;->a()Ljava/lang/CharSequence;

    move-result-object v11

    .line 57
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/cardui/g/e;->k()Ljava/lang/CharSequence;

    move-result-object v6

    .line 58
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/cardui/g/e;->e()Ljava/lang/Boolean;

    move-result-object v8

    .line 59
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/cardui/g/e;->v()Ljava/lang/Boolean;

    move-result-object v12

    .line 60
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/cardui/g/e;->w()Lcom/google/android/libraries/curvular/cf;

    move-result-object v13

    .line 61
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/cardui/g/e;->q()Lcom/google/android/libraries/curvular/aq;

    move-result-object v14

    .line 62
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/cardui/g/e;->r()Lcom/google/android/libraries/curvular/aq;

    move-result-object v7

    .line 63
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v9}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/libraries/curvular/ce;

    check-cast v9, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/cardui/g/e;->d()Ljava/lang/Boolean;

    move-result-object v15

    .line 64
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v9}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/libraries/curvular/ce;

    check-cast v9, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/cardui/g/e;->g()Ljava/lang/Boolean;

    move-result-object v9

    .line 65
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v10}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/libraries/curvular/ce;

    check-cast v10, Lcom/google/android/apps/gmm/cardui/g/e;

    invoke-interface {v10}, Lcom/google/android/apps/gmm/cardui/g/e;->u()Lcom/google/android/libraries/curvular/aq;

    move-result-object v16

    .line 55
    const/16 v10, 0xc

    new-array v0, v10, [Lcom/google/android/libraries/curvular/cu;

    move-object/from16 v17, v0

    const/4 v10, 0x0

    const/16 v18, -0x2

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    aput-object v18, v17, v10

    const/4 v10, 0x1

    const/16 v18, -0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    aput-object v18, v17, v10

    const/16 v18, 0x2

    const-wide/high16 v20, 0x4030000000000000L    # 16.0

    const-wide/high16 v22, 0x4030000000000000L    # 16.0

    const-wide/high16 v24, 0x4054000000000000L    # 80.0

    const-wide/high16 v26, 0x4038000000000000L    # 24.0

    new-instance v19, Lcom/google/android/libraries/curvular/as;

    new-instance v28, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v20 .. v21}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_1

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_0
    move-object/from16 v0, v28

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v20, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v22 .. v23}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_2

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v22, 0xffffff

    and-int v21, v21, v22

    shl-int/lit8 v21, v21, 0x8

    or-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_1
    move-object/from16 v0, v20

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v21, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v24 .. v25}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_3

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v22, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v23, 0xffffff

    and-int v22, v22, v23

    shl-int/lit8 v22, v22, 0x8

    or-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_2
    move-object/from16 v0, v21

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v22, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v26 .. v27}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_4

    move-wide/from16 v0, v26

    double-to-int v0, v0

    move/from16 v23, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v24, 0xffffff

    and-int v23, v23, v24

    shl-int/lit8 v23, v23, 0x8

    or-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_3
    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v17, v18

    const/16 v18, 0x3

    const-wide/high16 v20, 0x4030000000000000L    # 16.0

    const-wide/high16 v22, 0x4030000000000000L    # 16.0

    const-wide/high16 v24, 0x4054000000000000L    # 80.0

    const-wide/high16 v26, 0x4038000000000000L    # 24.0

    new-instance v19, Lcom/google/android/libraries/curvular/as;

    new-instance v28, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v20 .. v21}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_5

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_4
    move-object/from16 v0, v28

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v20, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v22 .. v23}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_6

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v22, 0xffffff

    and-int v21, v21, v22

    shl-int/lit8 v21, v21, 0x8

    or-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_5
    move-object/from16 v0, v20

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v21, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v24 .. v25}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_7

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v22, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v23, 0xffffff

    and-int v22, v22, v23

    shl-int/lit8 v22, v22, 0x8

    or-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_6
    move-object/from16 v0, v21

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v22, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v26 .. v27}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_8

    move-wide/from16 v0, v26

    double-to-int v0, v0

    move/from16 v23, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v24, 0xffffff

    and-int v23, v23, v24

    shl-int/lit8 v23, v23, 0x8

    or-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_7
    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v17, v18

    const/16 v18, 0x4

    const-wide/high16 v20, 0x4030000000000000L    # 16.0

    new-instance v19, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v20 .. v21}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_9

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_8
    move-object/from16 v0, v19

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v17, v18

    const/16 v18, 0x5

    const-wide/high16 v20, 0x4030000000000000L    # 16.0

    new-instance v19, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v20 .. v21}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_a

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    :goto_9
    move-object/from16 v0, v19

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v17, v18

    const/4 v10, 0x6

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    aput-object v18, v17, v10

    const/4 v10, 0x7

    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    const v19, 0x106000d

    invoke-static/range {v19 .. v19}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v19

    sget-object v20, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v19

    sget-object v20, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v17, v10

    const/16 v16, 0x8

    const/4 v10, 0x0

    if-eqz v13, :cond_0

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-static {v13, v10}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v13

    new-instance v10, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v10, v13}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v13, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v17, v16

    const/16 v10, 0x9

    sget-object v13, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v17, v10

    const/16 v10, 0xa

    const/16 v12, 0x8

    new-array v12, v12, [Lcom/google/android/libraries/curvular/cu;

    const/4 v13, 0x0

    invoke-static {v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    const/16 v18, 0x8

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    sget-object v20, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v12, v13

    const/4 v13, 0x1

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v18, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v12, v13

    const/4 v13, 0x2

    const/16 v16, -0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v18, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v12, v13

    const/4 v13, 0x3

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    const/16 v18, 0x11

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    const v19, 0x800003

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    sget-object v20, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v12, v13

    const/4 v13, 0x4

    aput-object p1, v12, v13

    const/4 v13, 0x5

    invoke-static {v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    sget v18, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static/range {v18 .. v18}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v18

    sget-object v19, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v19

    invoke-static {v0, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v14}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, Lcom/google/android/libraries/curvular/g;->e:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    aput-object v11, v12, v13

    new-instance v11, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v11, v12}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v12, "android.widget.TextView"

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v11

    aput-object v11, v17, v10

    const/16 v13, 0xb

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Lcom/google/android/libraries/curvular/cu;

    const/4 v14, 0x0

    const-wide/high16 v18, 0x4026000000000000L    # 11.0

    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v12

    if-eqz v12, :cond_b

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v12, Landroid/util/TypedValue;

    invoke-direct {v12}, Landroid/util/TypedValue;-><init>()V

    const v18, 0xffffff

    and-int v16, v16, v18

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v12, Landroid/util/TypedValue;->data:I

    :goto_a
    invoke-direct {v15, v12}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v12, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v14

    invoke-static/range {v6 .. v11}, Lcom/google/android/apps/gmm/base/f/am;->a(Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/aq;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    aput-object v6, v17, v13

    new-instance v6, Lcom/google/android/libraries/curvular/cs;

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.LinearLayout"

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    return-object v6

    :cond_1
    const-wide/high16 v30, 0x4060000000000000L    # 128.0

    mul-double v20, v20, v30

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v20

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x11

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_2
    const-wide/high16 v30, 0x4060000000000000L    # 128.0

    mul-double v22, v22, v30

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v21

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v22, 0xffffff

    and-int v21, v21, v22

    shl-int/lit8 v21, v21, 0x8

    or-int/lit8 v21, v21, 0x11

    move/from16 v0, v21

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_3
    const-wide/high16 v22, 0x4060000000000000L    # 128.0

    mul-double v22, v22, v24

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v22

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v23, 0xffffff

    and-int v22, v22, v23

    shl-int/lit8 v22, v22, 0x8

    or-int/lit8 v22, v22, 0x11

    move/from16 v0, v22

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_4
    const-wide/high16 v24, 0x4060000000000000L    # 128.0

    mul-double v24, v24, v26

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v24

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v23

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v24, 0xffffff

    and-int v23, v23, v24

    shl-int/lit8 v23, v23, 0x8

    or-int/lit8 v23, v23, 0x11

    move/from16 v0, v23

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    :cond_5
    const-wide/high16 v30, 0x4060000000000000L    # 128.0

    mul-double v20, v20, v30

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v20

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x11

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    :cond_6
    const-wide/high16 v30, 0x4060000000000000L    # 128.0

    mul-double v22, v22, v30

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v21

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v22, 0xffffff

    and-int v21, v21, v22

    shl-int/lit8 v21, v21, 0x8

    or-int/lit8 v21, v21, 0x11

    move/from16 v0, v21

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    :cond_7
    const-wide/high16 v22, 0x4060000000000000L    # 128.0

    mul-double v22, v22, v24

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v22

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v23, 0xffffff

    and-int v22, v22, v23

    shl-int/lit8 v22, v22, 0x8

    or-int/lit8 v22, v22, 0x11

    move/from16 v0, v22

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6

    :cond_8
    const-wide/high16 v24, 0x4060000000000000L    # 128.0

    mul-double v24, v24, v26

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v24

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v23

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v24, 0xffffff

    and-int v23, v23, v24

    shl-int/lit8 v23, v23, 0x8

    or-int/lit8 v23, v23, 0x11

    move/from16 v0, v23

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_7

    :cond_9
    const-wide/high16 v22, 0x4060000000000000L    # 128.0

    mul-double v20, v20, v22

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v20

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x11

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_8

    :cond_a
    const-wide/high16 v22, 0x4060000000000000L    # 128.0

    mul-double v20, v20, v22

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v20

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v21, 0xffffff

    and-int v20, v20, v21

    shl-int/lit8 v20, v20, 0x8

    or-int/lit8 v20, v20, 0x11

    move/from16 v0, v20

    iput v0, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_9

    :cond_b
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v12, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v12}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v12, Landroid/util/TypedValue;

    invoke-direct {v12}, Landroid/util/TypedValue;-><init>()V

    const v18, 0xffffff

    and-int v16, v16, v18

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v12, Landroid/util/TypedValue;->data:I

    goto/16 :goto_a
.end method
