.class public Lcom/google/android/apps/gmm/shared/c/c/h;
.super Lcom/google/android/apps/gmm/shared/c/c/i;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/shared/c/c/j;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    .line 380
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 381
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 481
    instance-of v0, p2, Lcom/google/android/apps/gmm/shared/c/c/i;

    if-eqz v0, :cond_0

    .line 482
    check-cast p2, Lcom/google/android/apps/gmm/shared/c/c/i;

    .line 483
    iget v0, p2, Lcom/google/android/apps/gmm/shared/c/c/i;->e:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->e:I

    .line 484
    invoke-super {p2, p1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object p2

    .line 488
    :goto_0
    return-object p2

    .line 485
    :cond_0
    const-string v0, "%s"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p2, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 486
    check-cast p2, Ljava/lang/CharSequence;

    goto :goto_0

    .line 488
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method private a(Landroid/text/SpannableStringBuilder;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 517
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Lcom/google/android/apps/gmm/shared/c/c/f;

    invoke-virtual {p1, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/shared/c/c/f;

    .line 497
    array-length v1, v0

    new-array v3, v1, [I

    .line 498
    array-length v1, v0

    new-array v4, v1, [I

    move v1, v2

    .line 499
    :goto_1
    array-length v5, v0

    if-ge v1, v5, :cond_2

    .line 500
    aget-object v5, v0, v1

    invoke-virtual {p1, v5}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    aput v5, v3, v1

    .line 501
    aget-object v5, v0, v1

    invoke-virtual {p1, v5}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    aput v5, v4, v1

    .line 499
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 504
    :cond_2
    invoke-static {v3}, Ljava/util/Arrays;->sort([I)V

    .line 505
    invoke-static {v4}, Ljava/util/Arrays;->sort([I)V

    move v0, v2

    .line 508
    :goto_2
    array-length v1, v3

    if-ge v2, v1, :cond_4

    .line 509
    aget v1, v3, v2

    if-le v1, v0, :cond_3

    .line 510
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget v5, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->e:I

    aget v6, v3, v2

    invoke-static {v1, p1, v5, v0, v6}, Lcom/google/android/apps/gmm/shared/c/c/j;->a(Lcom/google/android/apps/gmm/shared/c/c/j;Landroid/text/SpannableStringBuilder;III)V

    .line 512
    :cond_3
    aget v0, v4, v2

    .line 508
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 514
    :cond_4
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget v2, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->e:I

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-static {v1, p1, v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/c/j;->a(Lcom/google/android/apps/gmm/shared/c/c/j;Landroid/text/SpannableStringBuilder;III)V

    goto :goto_0
.end method

.method private varargs a(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 426
    move v0, v1

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 427
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 428
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 429
    if-gez v3, :cond_0

    .line 430
    new-instance v0, Ljava/util/MissingFormatArgumentException;

    invoke-direct {v0, v2}, Ljava/util/MissingFormatArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :cond_0
    const-string v4, "%s"

    aget-object v5, p2, v0

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/gmm/shared/c/c/h;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 433
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v3

    invoke-virtual {p1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 434
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/f;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/shared/c/c/f;-><init>()V

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {p1, v2, v3, v4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 426
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 436
    :cond_1
    return-void
.end method

.method private varargs b(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 444
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/c/e;->b:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    move v0, v1

    move v2, v1

    .line 447
    :goto_0
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 449
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    .line 450
    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 453
    if-eqz v3, :cond_1

    .line 455
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 456
    const-string v7, "%"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v5, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    move v10, v4

    move-object v4, v3

    move v3, v10

    .line 463
    :goto_2
    array-length v7, p2

    if-lt v3, v7, :cond_2

    .line 464
    new-instance v0, Ljava/util/MissingFormatArgumentException;

    invoke-direct {v0, v5}, Ljava/util/MissingFormatArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move v3, v0

    move-object v4, v5

    .line 460
    goto :goto_2

    .line 466
    :cond_2
    aget-object v3, p2, v3

    .line 469
    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 470
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {p1, v4, v7, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 471
    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/f;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/shared/c/c/f;-><init>()V

    .line 472
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v8

    add-int/2addr v8, v2

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v9

    add-int/2addr v8, v9

    .line 471
    invoke-virtual {p1, v4, v7, v8, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 475
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 476
    add-int/lit8 v0, v0, 0x1

    .line 477
    goto/16 :goto_0

    .line 478
    :cond_3
    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 403
    new-instance v2, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 404
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "{0}"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V

    .line 409
    :goto_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a(Landroid/text/SpannableStringBuilder;)V

    .line 414
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v3, Lcom/google/android/apps/gmm/shared/c/c/f;

    invoke-virtual {v2, v1, v0, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/shared/c/c/f;

    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 415
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 414
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 407
    :cond_0
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/h;->b(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V

    goto :goto_0

    .line 417
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/shared/c/c/h;->b:Ljava/lang/Object;

    .line 418
    return-object p0
.end method
