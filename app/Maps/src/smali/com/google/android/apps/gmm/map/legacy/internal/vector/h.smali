.class Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;
.super Lcom/google/android/apps/gmm/map/f/t;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/f/o;

.field a:F

.field private b:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:Z

.field private final s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

.field private t:Z

.field private final u:[F

.field private final v:[F

.field private final w:[F

.field private x:[F

.field private final y:Lcom/google/android/apps/gmm/map/f/f;

.field private z:Lcom/google/android/apps/gmm/map/f/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Landroid/content/Context;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1131
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 1100
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->u:[F

    .line 1105
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->v:[F

    .line 1110
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->w:[F

    .line 1119
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->x:[F

    .line 1132
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    .line 1133
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1134
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->y:Lcom/google/android/apps/gmm/map/f/f;

    .line 1135
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    .line 1136
    return-void
.end method

.method private declared-synchronized h()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1307
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->w:[F

    iget v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I

    if-nez v3, :cond_0

    const/4 v1, 0x0

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1309
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->w:[F

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 1310
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->x:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->w:[F

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->v:[F

    aget v3, v3, v0

    sub-float/2addr v2, v3

    aput v2, v1, v0

    .line 1311
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->v:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->w:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 1309
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1307
    :cond_0
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    iput v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->i:I

    iget v3, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->i:I

    int-to-float v3, v3

    iget v4, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->d:F

    invoke-static {v5, v6, v3}, Lcom/google/android/apps/gmm/shared/c/s;->b(FFF)F

    move-result v5

    aput v5, v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget v6, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->e:F

    invoke-static {v5, v6, v3}, Lcom/google/android/apps/gmm/shared/c/s;->b(FFF)F

    move-result v5

    aput v5, v2, v4

    const/4 v4, 0x2

    const/4 v5, 0x0

    iget v6, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->f:F

    invoke-static {v5, v6, v3}, Lcom/google/android/apps/gmm/shared/c/s;->b(FFF)F

    move-result v5

    aput v5, v2, v4

    const/4 v4, 0x3

    const/4 v5, 0x0

    iget v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->g:F

    invoke-static {v5, v1, v3}, Lcom/google/android/apps/gmm/shared/c/s;->b(FFF)F

    move-result v1

    aput v1, v2, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1313
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized W_()I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1161
    monitor-enter p0

    .line 1162
    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_c

    .line 1164
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    or-int/lit8 v0, v0, 0x0

    .line 1166
    :goto_0
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_2

    .line 1167
    sget v3, Lcom/google/android/apps/gmm/map/f/a/a;->c:I

    or-int/2addr v0, v3

    .line 1169
    :cond_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_3

    .line 1170
    sget v3, Lcom/google/android/apps/gmm/map/f/a/a;->d:I

    or-int/2addr v0, v3

    .line 1172
    :cond_3
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_4

    .line 1173
    sget v3, Lcom/google/android/apps/gmm/map/f/a/a;->e:I

    or-int/2addr v0, v3

    .line 1175
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->t:Z

    if-eqz v3, :cond_8

    .line 1176
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    iget v4, v3, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->d:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_5

    iget v3, v3, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->e:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_9

    :cond_5
    move v3, v1

    :goto_1
    if-eqz v3, :cond_6

    .line 1177
    sget v3, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    or-int/2addr v0, v3

    .line 1179
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    iget v3, v3, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->f:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_a

    move v3, v1

    :goto_2
    if-eqz v3, :cond_7

    .line 1180
    sget v3, Lcom/google/android/apps/gmm/map/f/a/a;->c:I

    or-int/2addr v0, v3

    .line 1182
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    iget v3, v3, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->g:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_b

    :goto_3
    if-eqz v1, :cond_8

    .line 1183
    sget v1, Lcom/google/android/apps/gmm/map/f/a/a;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    or-int/2addr v0, v1

    .line 1186
    :cond_8
    monitor-exit p0

    return v0

    :cond_9
    move v3, v2

    .line 1176
    goto :goto_1

    :cond_a
    move v3, v2

    .line 1179
    goto :goto_2

    :cond_b
    move v1, v2

    .line 1182
    goto :goto_3

    .line 1161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_c
    move v0, v2

    goto :goto_0
.end method

.method final declared-synchronized a(F)F
    .locals 3

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    .line 1219
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    add-float/2addr v1, p1

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    .line 1220
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-eqz v1, :cond_0

    .line 1221
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    .line 1223
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v2, 0x41a80000    # 21.0f

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1219
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(FFF)F
    .locals 3

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    .line 1237
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    add-float/2addr v1, p1

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    .line 1242
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-eqz v1, :cond_0

    .line 1243
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->p:F

    .line 1244
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->q:F

    .line 1245
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    .line 1247
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v2, 0x41a80000    # 21.0f

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(J)I
    .locals 13

    .prologue
    .line 1319
    monitor-enter p0

    .line 1320
    :try_start_0
    iget v10, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->p:F

    .line 1321
    iget v11, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->q:F

    .line 1322
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->t:Z

    if-eqz v0, :cond_1

    .line 1323
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->h()V

    .line 1326
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->x:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    add-float/2addr v1, v0

    .line 1327
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->x:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    add-float/2addr v0, v2

    .line 1328
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->x:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    add-float v4, v2, v3

    .line 1329
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->x:[F

    const/4 v5, 0x3

    aget v3, v3, v5

    add-float/2addr v3, v2

    .line 1330
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    .line 1331
    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    move v8, v2

    move v5, v3

    move v9, v4

    move v4, v1

    move v3, v0

    .line 1368
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->e:I

    if-eqz v0, :cond_0

    .line 1369
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    cmpg-double v0, v0, v6

    if-gez v0, :cond_5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    cmpg-double v0, v0, v6

    if-gez v0, :cond_5

    .line 1370
    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v6, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v0, v0, v6

    if-gez v0, :cond_5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v6

    if-gez v0, :cond_5

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    .line 1371
    iget v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->i:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I

    if-ne v1, v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    .line 1373
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    .line 1374
    const/4 v0, 0x0

    monitor-exit p0

    .line 1432
    :goto_2
    return v0

    .line 1333
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    .line 1334
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    .line 1335
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    .line 1342
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 1343
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    mul-float/2addr v4, v5

    const/high16 v5, -0x3ee00000    # -10.0f

    mul-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 1353
    :goto_3
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    const/4 v5, 0x0

    cmpg-float v3, v3, v5

    if-gez v3, :cond_3

    .line 1354
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    mul-float/2addr v5, v6

    const v6, -0x42333333    # -0.1f

    mul-float/2addr v5, v6

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 1360
    :goto_4
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    sub-float/2addr v5, v4

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    .line 1361
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    sub-float/2addr v5, v3

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    .line 1362
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    sub-float/2addr v5, v1

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    .line 1363
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    sub-float/2addr v5, v0

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    .line 1364
    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    sub-float/2addr v5, v2

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    move v8, v2

    move v5, v3

    move v9, v4

    move v4, v1

    move v3, v0

    goto/16 :goto_0

    .line 1345
    :cond_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->b:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto :goto_3

    .line 1356
    :cond_3
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    iget v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    mul-float/2addr v5, v6

    const v6, 0x3dcccccd    # 0.1f

    mul-float/2addr v5, v6

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_4

    .line 1371
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1376
    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1379
    const/4 v0, 0x0

    cmpl-float v0, v4, v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_d

    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->e:I

    sget v1, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    move v2, v0

    .line 1381
    :goto_5
    const/4 v0, 0x0

    cmpl-float v0, v5, v0

    if-eqz v0, :cond_e

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->e:I

    sget v1, Lcom/google/android/apps/gmm/map/f/a/a;->e:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    move v1, v0

    .line 1383
    :goto_6
    const/4 v0, 0x0

    cmpl-float v0, v9, v0

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->e:I

    sget v6, Lcom/google/android/apps/gmm/map/f/a/a;->c:I

    and-int/2addr v0, v6

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    move v7, v0

    .line 1385
    :goto_7
    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_10

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->e:I

    sget v6, Lcom/google/android/apps/gmm/map/f/a/a;->d:I

    and-int/2addr v0, v6

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    move v6, v0

    .line 1389
    :goto_8
    if-eqz v2, :cond_8

    .line 1390
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v2, v4, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1393
    if-nez v1, :cond_7

    if-nez v7, :cond_7

    if-eqz v6, :cond_8

    .line 1394
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 1399
    :cond_8
    if-eqz v1, :cond_a

    .line 1400
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    if-eqz v0, :cond_11

    .line 1402
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->y:Lcom/google/android/apps/gmm/map/f/f;

    invoke-virtual {v0, v1, v5, v10, v11}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/f;FFF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1408
    :goto_9
    if-nez v7, :cond_9

    if-eqz v6, :cond_a

    .line 1409
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 1414
    :cond_a
    if-eqz v7, :cond_b

    .line 1415
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    add-float v3, v0, v9

    .line 1416
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    if-eqz v0, :cond_12

    .line 1417
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->y:Lcom/google/android/apps/gmm/map/f/f;

    move v4, v10

    move v5, v11

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/f;FFF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1423
    :goto_a
    if-eqz v6, :cond_b

    .line 1424
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->A:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 1428
    :cond_b
    if-eqz v6, :cond_c

    .line 1429
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    add-float/2addr v0, v8

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1431
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->y:Lcom/google/android/apps/gmm/map/f/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1432
    const/4 v0, 0x2

    goto/16 :goto_2

    .line 1376
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1379
    :cond_d
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_5

    .line 1381
    :cond_e
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_6

    .line 1383
    :cond_f
    const/4 v0, 0x0

    move v7, v0

    goto/16 :goto_7

    .line 1385
    :cond_10
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_8

    .line 1404
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    add-float/2addr v0, v5

    iput v0, v12, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v12, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v12, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v12, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v12, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v12, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    goto/16 :goto_9

    .line 1419
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    goto/16 :goto_a
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final declared-synchronized a()V
    .locals 2

    .prologue
    .line 1193
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->t:Z

    .line 1194
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    iget v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195
    monitor-exit p0

    return-void

    .line 1193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(FF)V
    .locals 1

    .prologue
    .line 1288
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->n:F

    .line 1289
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->o:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1290
    monitor-exit p0

    return-void

    .line 1288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1156
    iput p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->e:I

    .line 1157
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/c;)V
    .locals 1

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 1147
    return-void
.end method

.method final declared-synchronized a([F)V
    .locals 11

    .prologue
    const/high16 v10, 0x40800000    # 4.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 1201
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->t:Z

    .line 1202
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->u:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->u:[F

    const/4 v4, 0x4

    invoke-static {p1, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1203
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->u:[F

    const/4 v2, 0x0

    aget v4, v1, v2

    const/4 v2, 0x1

    aget v3, v1, v2

    const/4 v2, 0x2

    aget v2, v1, v2

    const/4 v5, 0x3

    aget v1, v1, v5

    cmpl-float v5, v2, v0

    if-nez v5, :cond_1

    move v5, v6

    :goto_0
    cmpg-float v6, v5, v6

    if-gez v6, :cond_0

    mul-float/2addr v4, v5

    mul-float/2addr v3, v5

    mul-float/2addr v2, v5

    mul-float/2addr v1, v5

    :cond_0
    mul-float v5, v4, v4

    mul-float v6, v3, v3

    add-float/2addr v5, v6

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v5, v8

    iget v6, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a:F

    mul-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v6, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->b:F

    mul-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v6

    const v1, 0x7fffffff

    invoke-virtual {v7, v5, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a(II)I

    move-result v8

    if-nez v5, :cond_2

    move v1, v0

    :goto_1
    iput v1, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->d:F

    if-nez v5, :cond_3

    :goto_2
    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->e:F

    iget v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a:F

    mul-float/2addr v0, v10

    float-to-int v0, v0

    invoke-virtual {v7, v2, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a(II)I

    move-result v0

    int-to-float v0, v0

    iget v1, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a:F

    div-float/2addr v0, v1

    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->f:F

    const v0, 0x7fffffff

    invoke-virtual {v7, v6, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a(II)I

    move-result v0

    int-to-float v0, v0

    iget v1, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->b:F

    div-float/2addr v0, v1

    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->g:F

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ne v2, v5, :cond_4

    const v0, 0x7fffffff

    invoke-virtual {v7, v5, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a(II)I

    move-result v0

    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I

    :goto_3
    const/4 v0, 0x0

    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->i:I

    .line 1204
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->v:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1205
    monitor-exit p0

    return-void

    .line 1203
    :cond_1
    const/high16 v5, 0x41700000    # 15.0f

    div-float/2addr v5, v2

    :try_start_1
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    goto :goto_0

    :cond_2
    int-to-float v1, v8

    mul-float/2addr v1, v4

    int-to-float v4, v5

    div-float/2addr v1, v4

    goto :goto_1

    :cond_3
    int-to-float v0, v8

    mul-float/2addr v0, v3

    int-to-float v1, v5

    div-float/2addr v0, v1

    goto :goto_2

    :cond_4
    if-ne v2, v0, :cond_5

    iget v1, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a:F

    mul-float/2addr v1, v10

    float-to-int v1, v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a(II)I

    move-result v0

    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1203
    :cond_5
    const v0, 0x7fffffff

    :try_start_2
    invoke-virtual {v7, v1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a(II)I

    move-result v0

    iput v0, v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->h:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 1

    .prologue
    .line 1140
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1141
    const/4 v0, 0x1

    return v0
.end method

.method final declared-synchronized b(F)F
    .locals 2

    .prologue
    .line 1256
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    .line 1257
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 1258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    .line 1260
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    add-float/2addr v0, v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/f;->a(F)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 1256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(FFF)F
    .locals 2

    .prologue
    .line 1273
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    .line 1276
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 1277
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->p:F

    .line 1278
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->q:F

    .line 1279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->r:Z

    .line 1281
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->z:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->m:F

    add-float/2addr v0, v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/f;->a(F)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 1273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1443
    const/4 v0, 0x0

    return-object v0
.end method

.method final declared-synchronized c(F)V
    .locals 1

    .prologue
    .line 1296
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->a:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1297
    monitor-exit p0

    return-void

    .line 1296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1437
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1448
    const/4 v0, 0x1

    return v0
.end method

.method final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 1208
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/h;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
