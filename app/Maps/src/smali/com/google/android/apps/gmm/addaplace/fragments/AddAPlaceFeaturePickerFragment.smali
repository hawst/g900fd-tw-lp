.class public Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;
.super Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/aka;",
            "Lcom/google/r/b/a/ake;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/google/android/apps/gmm/map/b/a/q;

.field c:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/fragments/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/addaplace/fragments/a;-><init>(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 108
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 110
    if-eqz p1, :cond_0

    .line 111
    const-string v1, "placemark"

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 113
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;-><init>()V

    .line 114
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 115
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/q;->o()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->a(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->o()V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 120
    sget v0, Lcom/google/android/apps/gmm/l;->i:I

    return v0
.end method

.method protected final a(Z)I
    .locals 1

    .prologue
    .line 154
    const/16 v0, 0x9

    return v0
.end method

.method protected final a_()V
    .locals 5

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->l()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v4, v1, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v1, Lcom/google/maps/g/ha;->b:D

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v0, v1, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v1, Lcom/google/maps/g/ha;->c:D

    invoke-virtual {v1}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->l:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 145
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 125
    sget v0, Lcom/google/android/apps/gmm/l;->gF:I

    return v0
.end method

.method protected final b_()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    .line 161
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    .line 163
    :cond_0
    return-void
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 130
    sget v0, Lcom/google/android/apps/gmm/d;->aM:I

    return v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 135
    sget v0, Lcom/google/android/apps/gmm/l;->fW:I

    return v0
.end method

.method protected final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFeaturePickerFragment;->c:Ljava/lang/Object;

    return-object v0
.end method
