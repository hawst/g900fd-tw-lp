.class public Lcom/google/android/apps/gmm/map/internal/c/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bq;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final f:I

.field private final g:I

.field private final h:D

.field private final i:D

.field private j:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private k:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private l:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;II)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 59
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->a:I

    .line 61
    if-ge p4, v0, :cond_0

    move p4, v0

    .line 66
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 65
    invoke-static {p3, v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    .line 68
    iget v1, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 67
    invoke-static {p3, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    .line 70
    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    .line 71
    iget v3, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v3, v4

    .line 72
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 73
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 75
    if-gt v5, v4, :cond_1

    .line 76
    iput v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->f:I

    .line 77
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->g:I

    .line 78
    iput v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->c:I

    .line 79
    iput v7, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->d:I

    .line 80
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->b:I

    .line 88
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->b:I

    if-nez v0, :cond_2

    .line 89
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->i:D

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->h:D

    .line 95
    :goto_1
    iput v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->l:I

    .line 96
    return-void

    .line 82
    :cond_1
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->f:I

    .line 83
    iput v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->g:I

    .line 84
    iput v7, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->c:I

    .line 85
    iput v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->d:I

    .line 86
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->b:I

    goto :goto_0

    .line 91
    :cond_2
    int-to-double v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->b:I

    int-to-double v4, v2

    div-double/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->h:D

    .line 92
    int-to-double v0, v3

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->b:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->i:D

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 112
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->k:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 113
    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->k:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-ge v1, v2, :cond_2

    .line 115
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 116
    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->c:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 117
    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->d:I

    add-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 136
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    :cond_1
    return-object v0

    .line 118
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->l:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->b:I

    if-gt v1, v2, :cond_1

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v2, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->l:I

    int-to-double v4, v1

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->h:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v1, v2

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->l:I

    int-to-double v4, v4

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->i:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 123
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->a:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->f:I

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->g:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v2

    invoke-static {v3, v4, v5, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->j:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 129
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->a:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->f:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->c:I

    sub-int/2addr v1, v4

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->g:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->d:I

    add-int/2addr v2, v4

    invoke-static {v3, v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(IIILcom/google/android/apps/gmm/map/internal/c/cd;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->k:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 132
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ag;->l:I

    goto :goto_0
.end method
