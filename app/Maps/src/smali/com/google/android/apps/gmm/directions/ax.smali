.class public Lcom/google/android/apps/gmm/directions/ax;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/a/f;


# instance fields
.field a:Lcom/google/android/apps/gmm/directions/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 289
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/directions/ax;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/directions/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/util/List;)Lcom/google/android/apps/gmm/directions/h/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/gk;",
            ">;)",
            "Lcom/google/android/apps/gmm/directions/h/a;"
        }
    .end annotation

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/a;

    .line 335
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/directions/i/a;->b(Landroid/content/Context;Ljava/util/List;)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/directions/i/a;-><init>(Landroid/content/Context;Lcom/google/b/c/cv;)V

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/a/gk;Landroid/content/Context;)Lcom/google/android/apps/gmm/directions/h/n;
    .locals 1

    .prologue
    .line 328
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/al;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/directions/i/al;-><init>(Lcom/google/maps/g/a/gk;Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 6

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/directions/e;

    .line 66
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 67
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    new-instance v4, Lcom/google/android/apps/gmm/directions/az;

    invoke-direct {v4, p1}, Lcom/google/android/apps/gmm/directions/az;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 68
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/c;->g()Lcom/google/android/apps/gmm/map/x;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/e;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/e;Lcom/google/android/apps/gmm/map/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    .line 69
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/directions/ax;->a(Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;Z)V

    .line 209
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;Z)V
    .locals 3

    .prologue
    .line 222
    invoke-interface {p1}, Lcom/google/android/apps/gmm/directions/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/directions/a/c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/gmm/directions/a/c;->e()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 225
    :cond_3
    invoke-static {p1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/av;

    move-result-object v1

    .line 226
    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 227
    invoke-virtual {v1, p3}, Lcom/google/android/apps/gmm/directions/av;->a(Z)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/ay;

    invoke-direct {v2, p0, v1, p1}, Lcom/google/android/apps/gmm/directions/ay;-><init>(Lcom/google/android/apps/gmm/directions/ax;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/directions/a/c;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 229
    return-void
.end method

.method public final a(Lcom/google/b/c/cv;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 284
    new-instance v1, Lcom/google/android/apps/gmm/directions/i/a;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/gmm/directions/i/a;-><init>(Landroid/content/Context;Lcom/google/b/c/cv;)V

    .line 285
    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/QuAgencyInfoFragment;->a(Lcom/google/android/apps/gmm/directions/h/a;)Lcom/google/android/apps/gmm/directions/QuAgencyInfoFragment;

    move-result-object v1

    .line 284
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 287
    return-void
.end method

.method public final a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/google/maps/g/a/hm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/r/b/a/afz;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 155
    new-instance v1, Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/av;-><init>()V

    .line 157
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 158
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    if-nez v0, :cond_1

    .line 159
    :cond_0
    const-string v0, "DirectionsVeneerImpl"

    const-string v1, "Cannot start directions if not attached to GmmActivity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    :goto_0
    return-void

    .line 163
    :cond_1
    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/gmm/directions/f/d/f;->a(Lcom/google/maps/g/a/hm;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/f;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/hm;

    move-result-object p1

    .line 166
    :cond_3
    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/a/hm;)V

    .line 168
    if-nez p2, :cond_4

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/directions/e;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object p2

    .line 171
    :cond_4
    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 173
    if-nez p3, :cond_5

    .line 174
    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object p3

    .line 176
    :cond_5
    invoke-virtual {v1, p3}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 178
    if-eqz p4, :cond_6

    .line 179
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-static {p1, v0, p4}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;

    move-result-object v0

    .line 183
    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/r/b/a/afz;)V

    .line 185
    invoke-virtual {v1, p5}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 188
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;

    move-result-object v0

    .line 187
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/a/al;)V

    .line 190
    invoke-virtual {v1, p6}, Lcom/google/android/apps/gmm/directions/av;->a(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v1, p7}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/hy;)V

    .line 194
    invoke-virtual {v1, p8}, Lcom/google/android/apps/gmm/directions/av;->b(Ljava/lang/String;)V

    .line 196
    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/directions/ay;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/apps/gmm/directions/ay;-><init>(Lcom/google/android/apps/gmm/directions/ax;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/directions/a/c;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0

    .line 181
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    sget-object v3, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {p1, v3, v0, v4}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()Lcom/google/android/apps/gmm/directions/a/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/f;->a:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    aput-object v3, v2, v1

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    aput-object v3, v2, v0

    const/4 v3, 0x2

    sget-object v4, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    aput-object v4, v2, v3

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v3, v2

    if-ltz v3, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x5

    int-to-long v4, v3

    add-long/2addr v0, v4

    div-int/lit8 v3, v3, 0xa

    int-to-long v4, v3

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v0, v4

    if-lez v3, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    if-nez v0, :cond_5

    move-object v0, v1

    .line 122
    :goto_2
    return-object v0

    .line 103
    :cond_3
    const-wide/32 v4, -0x80000000

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v0

    goto :goto_1

    .line 109
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_6

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->b:Z

    if-nez v0, :cond_6

    .line 119
    sget-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_6
    move-object v0, v1

    .line 122
    goto :goto_2
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 74
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 75
    return-void
.end method

.method public final i()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 131
    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->c:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 132
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/ax;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    .line 135
    return-void
.end method
