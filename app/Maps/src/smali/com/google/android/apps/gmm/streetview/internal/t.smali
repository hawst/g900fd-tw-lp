.class public Lcom/google/android/apps/gmm/streetview/internal/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/streetview/internal/t;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:J

.field public a:Z

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Lcom/google/android/apps/gmm/map/b/a/u;

.field public j:I

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:I

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/streetview/internal/z;",
            ">;"
        }
    .end annotation
.end field

.field public q:F

.field public r:F

.field public s:F

.field public t:F

.field public u:F

.field public v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

.field public w:Lcom/google/android/apps/gmm/streetview/internal/d;

.field public x:F

.field public y:F

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 513
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/u;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/internal/u;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/t;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->A:J

    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v2, 0x6

    if-eq v0, v2, :cond_0

    .line 251
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->a:Z

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->b:I

    .line 257
    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->c:I

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    .line 260
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 265
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->j:I

    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->k:Ljava/lang/String;

    .line 268
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->l:Ljava/lang/String;

    .line 269
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->m:Ljava/lang/String;

    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->n:I

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    .line 275
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    .line 276
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    .line 277
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/aa;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/streetview/internal/aa;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    .line 278
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->A:J

    .line 279
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 280
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 282
    :try_start_0
    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/d;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/d;-><init>([B[B)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/t;->b()V

    .line 287
    return-void

    :cond_1
    move v0, v1

    .line 253
    goto/16 :goto_0

    .line 284
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/r/b/a/xy;)V
    .locals 13

    .prologue
    const v7, -0x55d4a80

    const/4 v12, 0x0

    const v11, 0x358637bd    # 1.0E-6f

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->A:J

    .line 294
    iget-object v0, p1, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    move-object v1, v0

    .line 295
    :goto_0
    iget-object v0, p1, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/yt;->d()Lcom/google/r/b/a/yt;

    move-result-object v0

    move-object v2, v0

    .line 296
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v0

    move-object v3, v0

    .line 297
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/r/b/a/yp;->d()Lcom/google/r/b/a/yp;

    move-result-object v0

    move-object v4, v0

    .line 300
    :goto_3
    iget-boolean v0, v1, Lcom/google/r/b/a/yj;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->a:Z

    .line 303
    iget v0, v1, Lcom/google/r/b/a/yj;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->b:I

    .line 304
    iget v0, v1, Lcom/google/r/b/a/yj;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->c:I

    .line 305
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->b:I

    if-eqz v0, :cond_0

    .line 306
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->b:I

    iget v5, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->c:I

    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0x2c

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "infoLevel="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ", infoValue="

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 309
    :cond_0
    iget v0, v1, Lcom/google/r/b/a/yj;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    .line 310
    iget v0, v1, Lcom/google/r/b/a/yj;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    .line 311
    iget v0, v1, Lcom/google/r/b/a/yj;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    .line 312
    iget v0, v1, Lcom/google/r/b/a/yj;->h:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    .line 313
    iget-object v0, v1, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_b

    check-cast v0, Ljava/lang/String;

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    .line 314
    iget v5, v1, Lcom/google/r/b/a/yj;->j:I

    const/16 v0, 0xe

    if-ge v5, v6, :cond_d

    move v0, v6

    :cond_1
    :goto_5
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->j:I

    .line 315
    iget-object v0, v1, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v0

    .line 316
    :goto_6
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/u;

    iget v9, v0, Lcom/google/r/b/a/ia;->b:I

    iget v0, v0, Lcom/google/r/b/a/ia;->c:I

    invoke-direct {v5, v9, v0}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 318
    iget-object v0, v1, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_f

    check-cast v0, Ljava/lang/String;

    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->k:Ljava/lang/String;

    .line 319
    iget-object v0, v1, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_11

    check-cast v0, Ljava/lang/String;

    :goto_8
    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->l:Ljava/lang/String;

    .line 320
    iget-object v0, v1, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_13

    check-cast v0, Ljava/lang/String;

    :goto_9
    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->m:Ljava/lang/String;

    .line 321
    iget v0, v1, Lcom/google/r/b/a/yj;->r:I

    invoke-static {v0}, Lcom/google/r/b/a/yz;->a(I)Lcom/google/r/b/a/yz;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/yz;->a:Lcom/google/r/b/a/yz;

    :cond_2
    iget v0, v0, Lcom/google/r/b/a/yz;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->n:I

    .line 326
    iget v0, v2, Lcom/google/r/b/a/yt;->b:I

    const v1, 0x15752a00

    rem-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, v11

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    .line 327
    iget v0, v2, Lcom/google/r/b/a/yt;->c:I

    const v1, 0x15752a00

    rem-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, v11

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    .line 328
    iget v0, v2, Lcom/google/r/b/a/yt;->d:I

    int-to-float v0, v0

    mul-float v5, v0, v11

    const v0, -0x3d4c3333    # -89.9f

    const v1, 0x42b3cccd    # 89.9f

    cmpg-float v9, v5, v0

    if-gez v9, :cond_15

    :goto_a
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    .line 333
    iput v8, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    .line 334
    iget v0, v2, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_17

    move v0, v6

    :goto_b
    if-eqz v0, :cond_4

    .line 335
    iget v0, v2, Lcom/google/r/b/a/yt;->f:I

    invoke-static {v0}, Lcom/google/r/b/a/yx;->a(I)Lcom/google/r/b/a/yx;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/r/b/a/yx;->a:Lcom/google/r/b/a/yx;

    .line 336
    :cond_3
    sget-object v1, Lcom/google/r/b/a/yx;->a:Lcom/google/r/b/a/yx;

    if-ne v0, v1, :cond_18

    .line 337
    iput v8, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    .line 346
    :cond_4
    :goto_c
    iget v1, v2, Lcom/google/r/b/a/yt;->h:I

    const v0, 0x55d4a80

    if-ge v1, v7, :cond_19

    move v0, v7

    :cond_5
    :goto_d
    int-to-float v0, v0

    mul-float/2addr v0, v11

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    .line 348
    iget v1, v2, Lcom/google/r/b/a/yt;->i:I

    const v0, 0x55d4a80

    if-ge v1, v7, :cond_1a

    :goto_e
    int-to-float v0, v7

    mul-float/2addr v0, v11

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    .line 351
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_1c

    const v0, -0x335456b0    # -9.0E7f

    :goto_f
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    .line 353
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_1d

    const v0, 0x4caba950    # 9.0E7f

    :goto_10
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    .line 357
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 358
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    .line 359
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    .line 360
    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    .line 368
    :cond_6
    iget-object v0, v3, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 369
    new-array v0, v2, [Lcom/google/android/apps/gmm/streetview/internal/aa;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    move v1, v8

    .line 370
    :goto_11
    if-ge v1, v2, :cond_1e

    .line 371
    iget-object v0, v3, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yd;

    .line 372
    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    new-instance v7, Lcom/google/android/apps/gmm/streetview/internal/aa;

    invoke-direct {v7, v0}, Lcom/google/android/apps/gmm/streetview/internal/aa;-><init>(Lcom/google/r/b/a/yd;)V

    aput-object v7, v5, v1

    .line 370
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 294
    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    move-object v1, v0

    goto/16 :goto_0

    .line 295
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    move-object v2, v0

    goto/16 :goto_1

    .line 296
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    move-object v3, v0

    goto/16 :goto_2

    .line 297
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    move-object v4, v0

    goto/16 :goto_3

    .line 313
    :cond_b
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    iput-object v5, v1, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    :cond_c
    move-object v0, v5

    goto/16 :goto_4

    .line 314
    :cond_d
    if-gt v5, v0, :cond_1

    move v0, v5

    goto/16 :goto_5

    .line 315
    :cond_e
    iget-object v0, v1, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    goto/16 :goto_6

    .line 318
    :cond_f
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_10

    iput-object v5, v1, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    :cond_10
    move-object v0, v5

    goto/16 :goto_7

    .line 319
    :cond_11
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    iput-object v5, v1, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    :cond_12
    move-object v0, v5

    goto/16 :goto_8

    .line 320
    :cond_13
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_14

    iput-object v5, v1, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    :cond_14
    move-object v0, v5

    goto/16 :goto_9

    .line 328
    :cond_15
    cmpl-float v0, v5, v1

    if-lez v0, :cond_16

    move v0, v1

    goto/16 :goto_a

    :cond_16
    move v0, v5

    goto/16 :goto_a

    :cond_17
    move v0, v8

    .line 334
    goto/16 :goto_b

    .line 338
    :cond_18
    sget-object v1, Lcom/google/r/b/a/yx;->b:Lcom/google/r/b/a/yx;

    if-ne v0, v1, :cond_4

    .line 339
    iput v6, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    goto/16 :goto_c

    .line 346
    :cond_19
    if-gt v1, v0, :cond_5

    move v0, v1

    goto/16 :goto_d

    .line 348
    :cond_1a
    if-le v1, v0, :cond_1b

    move v7, v0

    goto/16 :goto_e

    :cond_1b
    move v7, v1

    goto/16 :goto_e

    .line 351
    :cond_1c
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    goto/16 :goto_f

    .line 353
    :cond_1d
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    goto/16 :goto_10

    .line 378
    :cond_1e
    if-eqz v4, :cond_1f

    iget v0, v4, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_20

    move v0, v6

    :goto_12
    if-eqz v0, :cond_1f

    iget v0, v4, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_21

    :goto_13
    if-eqz v6, :cond_1f

    .line 380
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/d;

    iget-object v1, v4, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->c()[B

    move-result-object v1

    iget-object v2, v4, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->c()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/d;-><init>([B[B)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_1f
    :goto_14
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/t;->b()V

    .line 387
    return-void

    :cond_20
    move v0, v8

    .line 378
    goto :goto_12

    :cond_21
    move v6, v8

    goto :goto_13

    .line 382
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    goto :goto_14
.end method

.method public static a(I)F
    .locals 2

    .prologue
    .line 620
    const v0, 0x15752a00

    rem-int v0, p0, v0

    int-to-float v0, v0

    const v1, 0x358637bd    # 1.0E-6f

    mul-float/2addr v0, v1

    return v0
.end method

.method static a()J
    .locals 2

    .prologue
    .line 452
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/android/apps/gmm/streetview/internal/t;
    .locals 2

    .prologue
    .line 221
    invoke-static {p0}, Lcom/google/r/b/a/xy;->a(Ljava/io/InputStream;)Lcom/google/r/b/a/xy;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/t;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/t;-><init>(Lcom/google/r/b/a/xy;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 447
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "config_"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 395
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    invoke-static {v0}, Lcom/google/android/apps/gmm/streetview/internal/ao;->h(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->x:F

    .line 396
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->x:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 397
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->x:F

    const v1, 0x40c90fdb

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->x:F

    .line 399
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    invoke-static {v0}, Lcom/google/android/apps/gmm/streetview/internal/ao;->h(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->y:F

    .line 403
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    .line 404
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(IIII)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->z:I

    .line 408
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    .line 409
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    packed-switch v0, :pswitch_data_0

    .line 422
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unrecognized projection type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    :goto_0
    return-void

    .line 414
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->p:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/z;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v4, v4}, Lcom/google/android/apps/gmm/streetview/internal/z;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " text=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 474
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 475
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 476
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 479
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 480
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 481
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 482
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 486
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 490
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 491
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 492
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 493
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 494
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 495
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->t:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 496
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->u:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->v:[Lcom/google/android/apps/gmm/streetview/internal/aa;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 498
    iget-wide v2, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->A:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/d;->a:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/d;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 506
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 475
    goto/16 :goto_0

    .line 503
    :cond_1
    new-array v0, v1, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 504
    new-array v0, v1, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_1
.end method
