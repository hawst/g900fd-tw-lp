.class Lcom/google/android/apps/gmm/replay/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/replay/a;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final b:Lcom/google/android/apps/gmm/replay/d;

.field private final c:Lcom/google/android/apps/gmm/shared/c/f;

.field private final d:Lcom/google/android/apps/gmm/base/a;

.field private final e:Ljava/lang/String;

.field private f:Lcom/google/android/apps/gmm/replay/k;

.field private g:J

.field private h:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/replay/d;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/gmm/replay/e;->g:J

    .line 71
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/gmm/replay/e;->e:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/google/android/apps/gmm/replay/e;->d:Lcom/google/android/apps/gmm/base/a;

    .line 73
    invoke-interface {p2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 74
    invoke-interface {p2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 75
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p3, Lcom/google/android/apps/gmm/replay/d;

    iput-object p3, p0, Lcom/google/android/apps/gmm/replay/e;->b:Lcom/google/android/apps/gmm/replay/d;

    .line 76
    return-void
.end method

.method private a(J)J
    .locals 7

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v0

    .line 160
    iget-wide v2, p0, Lcom/google/android/apps/gmm/replay/e;->g:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 161
    iput-wide p1, p0, Lcom/google/android/apps/gmm/replay/e;->g:J

    .line 162
    iput-wide v0, p0, Lcom/google/android/apps/gmm/replay/e;->h:J

    .line 163
    const-wide/16 v0, 0x0

    .line 167
    :goto_0
    return-wide v0

    .line 165
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/gmm/replay/e;->h:J

    sub-long/2addr v0, v2

    .line 166
    iget-wide v2, p0, Lcom/google/android/apps/gmm/replay/e;->g:J

    sub-long v2, p1, v2

    .line 167
    sub-long v0, v2, v0

    goto :goto_0
.end method

.method private a(Lorg/w3c/dom/Document;)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 193
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 194
    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "event-track"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 195
    const-string v1, "EventTrackPlayer"

    const-string v3, "Event track must start with <%s> tag, not <%s>"

    new-array v4, v13, [Ljava/lang/Object;

    const-string v5, "event-track"

    aput-object v5, v4, v2

    .line 196
    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    .line 195
    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    :cond_0
    invoke-interface {v0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v6

    move v1, v2

    .line 199
    :goto_0
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 200
    invoke-interface {v6, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 201
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v3

    if-ne v3, v12, :cond_6

    .line 202
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    .line 205
    const-string v4, "event"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 206
    const-string v4, "EventTrackPlayer"

    const-string v5, "Expected <%s> tag, found <%s>"

    new-array v7, v13, [Ljava/lang/Object;

    const-string v8, "event"

    aput-object v8, v7, v2

    aput-object v3, v7, v12

    invoke-static {v4, v5, v7}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v3

    const-string v4, "time"

    invoke-interface {v3, v4}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 211
    if-eqz v3, :cond_2

    .line 212
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 213
    invoke-direct {p0, v8, v9}, Lcom/google/android/apps/gmm/replay/e;->a(J)J

    move-result-wide v4

    :goto_1
    const-wide/16 v10, 0x0

    cmp-long v3, v4, v10

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/replay/e;->f:Lcom/google/android/apps/gmm/replay/k;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/gmm/replay/k;->a(J)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/replay/e;->f:Lcom/google/android/apps/gmm/replay/k;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/replay/k;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v8, v9}, Lcom/google/android/apps/gmm/replay/e;->a(J)J

    move-result-wide v4

    goto :goto_1

    .line 215
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/replay/e;->f:Lcom/google/android/apps/gmm/replay/k;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/replay/k;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 216
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    move v3, v2

    :goto_2
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v3, v0, :cond_6

    invoke-interface {v5, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    if-ne v0, v12, :cond_5

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->b:Lcom/google/android/apps/gmm/replay/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/replay/d;->a:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/replay/b;

    if-nez v0, :cond_3

    const-string v8, "EventProfiles"

    const-string v9, "No event defined for %s."

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v7, v10, v2

    invoke-static {v8, v9, v10}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v7

    new-instance v8, Ljava/util/HashMap;

    invoke-interface {v7}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v4

    invoke-direct {v8, v4}, Ljava/util/HashMap;-><init>(I)V

    move v4, v2

    :goto_3
    invoke-interface {v7}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v9

    if-ge v4, v9, :cond_4

    invoke-interface {v7, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v10, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v4

    const-string v7, "EventTrackPlayer"

    const-string v8, "Playing <%s> tag: %s"

    new-array v9, v13, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    aput-object v0, v9, v2

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v12

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->a(Ljava/lang/Object;)V

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 199
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 220
    :cond_7
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/replay/k;)V
    .locals 5

    .prologue
    .line 83
    const-string v0, "EventTrackPlayer"

    const-string v1, "Playing track %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/replay/e;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/replay/k;

    iput-object p1, p0, Lcom/google/android/apps/gmm/replay/e;->f:Lcom/google/android/apps/gmm/replay/k;

    .line 85
    const/4 v2, 0x0

    .line 87
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->e:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/gmm/replay/f; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 88
    :try_start_1
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/replay/e;->a(Lorg/w3c/dom/Document;)V
    :try_end_1
    .catch Lcom/google/android/apps/gmm/replay/f; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 104
    :cond_1
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 91
    :goto_1
    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 96
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 98
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 100
    :cond_2
    :goto_3
    throw v0

    .line 93
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/e;->d:Lcom/google/android/apps/gmm/base/a;

    const-string v2, "Error replaying event track"

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 96
    if-eqz v1, :cond_1

    .line 98
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 100
    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v1

    goto :goto_3

    .line 96
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 93
    :catch_5
    move-exception v0

    goto :goto_4

    .line 89
    :catch_6
    move-exception v0

    goto :goto_1
.end method
