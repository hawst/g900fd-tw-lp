.class public Lcom/google/android/apps/gmm/streetview/internal/az;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# instance fields
.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 148
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nuniform mat3 uDepthMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uTextureMatrix * vec3(aTextureCoord, 1.0)).xy;\n  vColor = aColor;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nvoid main() {\n  vec4 c = texture2D(sTexture0, vTextureCoord);\n  gl_FragColor = vColor * c;\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 159
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/az;->c:I

    .line 160
    const-string v0, "uTextureMatrix"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/streetview/internal/az;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/az;->d:I

    .line 161
    return-void
.end method
