.class Lcom/google/android/apps/gmm/map/o/aw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/gmm/map/o/d;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 241
    check-cast p1, Lcom/google/android/apps/gmm/map/o/d;

    check-cast p2, Lcom/google/android/apps/gmm/map/o/d;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->b()I

    move-result v3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->c()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/m;->d()I

    move-result v0

    :goto_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/m;->b()I

    move-result v8

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/m;->c()I

    move-result v7

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/m;->d()I

    move-result v6

    :goto_1
    if-eq v3, v8, :cond_1

    sub-int v1, v3, v8

    :cond_0
    :goto_2
    return v1

    :cond_1
    if-eq v2, v7, :cond_2

    sub-int v1, v2, v7

    goto :goto_2

    :cond_2
    if-eq v0, v6, :cond_3

    sub-int v1, v0, v6

    goto :goto_2

    :cond_3
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v2

    if-eq v0, v2, :cond_4

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->d()I

    move-result v1

    sub-int v1, v0, v1

    goto :goto_2

    :cond_4
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v2

    if-eq v0, v2, :cond_6

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/o/d;->f()I

    move-result v2

    if-ge v0, v2, :cond_5

    move v1, v4

    goto :goto_2

    :cond_5
    if-le v0, v2, :cond_0

    move v1, v5

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    if-ge v0, v2, :cond_7

    move v1, v4

    goto :goto_2

    :cond_7
    if-le v0, v2, :cond_0

    move v1, v5

    goto :goto_2

    :cond_8
    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_0
.end method
