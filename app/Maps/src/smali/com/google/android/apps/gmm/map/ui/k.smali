.class public Lcom/google/android/apps/gmm/map/ui/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/apps/gmm/map/ui/j;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/ui/j;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iput p2, p0, Lcom/google/android/apps/gmm/map/ui/k;->a:I

    iput p3, p0, Lcom/google/android/apps/gmm/map/ui/k;->b:I

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/ui/k;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    iget v1, p0, Lcom/google/android/apps/gmm/map/ui/k;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    iget v2, p0, Lcom/google/android/apps/gmm/map/ui/k;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 100
    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    .line 102
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 103
    new-instance v0, Lcom/google/android/apps/gmm/map/ui/l;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/ui/l;-><init>(IFLandroid/graphics/Paint;)V

    .line 104
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/k;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 105
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/ui/k;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/k;->d:Lcom/google/android/apps/gmm/map/ui/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method
