.class public final Lcom/google/android/apps/gmm/base/views/c/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cw;


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/CharSequence;

.field public final c:Lcom/google/android/libraries/curvular/aw;

.field public final d:Ljava/lang/Runnable;

.field public final e:I

.field public final f:Z

.field public final g:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/c/f;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/e;->a:Ljava/lang/CharSequence;

    .line 30
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/e;->b:Ljava/lang/CharSequence;

    .line 31
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/e;->c:Lcom/google/android/libraries/curvular/aw;

    .line 32
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/c/e;->d:Ljava/lang/Runnable;

    .line 33
    iget v2, p1, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/c/e;->e:I

    .line 34
    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/views/c/f;->f:Z

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/c/f;->g:Ljava/util/concurrent/Callable;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-ne v3, v2, :cond_1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 35
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/c/f;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/c/e;->f:Z

    .line 36
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/f;->g:Ljava/util/concurrent/Callable;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/e;->g:Ljava/util/concurrent/Callable;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/e;->h:Ljava/lang/Runnable;

    .line 38
    return-void
.end method
