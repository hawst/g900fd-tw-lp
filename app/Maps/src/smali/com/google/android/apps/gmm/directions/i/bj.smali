.class public Lcom/google/android/apps/gmm/directions/i/bj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/y;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/gmm/map/r/a/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/bj;->a:Landroid/content/Context;

    .line 22
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/bj;->b:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bj;->b:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/h;->e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;

    move-result-object v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 31
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/bj;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/be;Lcom/google/android/apps/gmm/shared/c/c/m;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 32
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 37
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/i/bj;->b:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_0

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v4, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_1
    iget-object v0, v0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iget v0, v0, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_3

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_3
    iget-object v3, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v3, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_4
    iget-object v0, v0, Lcom/google/maps/g/a/au;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    iget v0, v0, Lcom/google/maps/g/a/be;->b:I

    .line 38
    :goto_5
    if-gez v0, :cond_6

    .line 39
    const/4 v0, 0x0

    .line 41
    :goto_6
    return-object v0

    .line 37
    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_3

    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_4

    :cond_5
    const/4 v0, -0x1

    goto :goto_5

    .line 41
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/i/bj;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->fm:I

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {v3, v0, v5}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method public final c()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 46
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/bj;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/i/bj;->b:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_0

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v4, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_1
    iget v0, v0, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_4
    iget-object v3, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v3, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_5
    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/shared/c/c/c;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;)V

    invoke-virtual {v3, v0, v1, v1}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/maps/g/a/ai;ZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_4

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_5
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method
