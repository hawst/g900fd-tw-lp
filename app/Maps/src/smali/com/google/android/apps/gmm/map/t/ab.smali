.class public Lcom/google/android/apps/gmm/map/t/ab;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_PICKER:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/lang/Object;

.field d:Z

.field final synthetic e:Lcom/google/android/apps/gmm/map/t/z;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/z;)V
    .locals 1

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    .line 217
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/z;->g:Lcom/google/android/apps/gmm/map/c/a/a;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ab;->a:Ljava/util/List;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ab;->b:Ljava/util/List;

    .line 208
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t/ab;->setDaemon(Z)V

    .line 219
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 267
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->b:Ljava/util/List;

    monitor-enter v1

    .line 269
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ab;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 271
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 272
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 272
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public run()V
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 224
    invoke-static {v6}, Landroid/os/Process;->setThreadPriority(I)V

    .line 228
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 230
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->d:Z

    if-eqz v1, :cond_0

    .line 231
    monitor-exit v2

    return-void

    .line 233
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 236
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/z;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 238
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/t/ab;->a:Ljava/util/List;

    monitor-enter v8
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    move-object v4, v0

    new-instance v10, Lcom/google/android/apps/gmm/map/t/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget v1, v1, Lcom/google/android/apps/gmm/map/t/z;->a:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t/z;->c:Lcom/google/android/apps/gmm/map/f/o;

    invoke-direct {v10, v4, v1, v2}, Lcom/google/android/apps/gmm/map/t/ac;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/f/o;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/z;->b:Ljava/util/SortedSet;

    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/t/p;

    iget-byte v1, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    sget-object v3, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    iget v3, v3, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v1, v1, v3

    check-cast v1, Lcom/google/android/apps/gmm/v/bb;

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/t/y;

    move-object v3, v0

    iget-boolean v1, v3, Lcom/google/android/apps/gmm/map/t/y;->a:Z

    if-eqz v1, :cond_2

    instance-of v1, v2, Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v1, :cond_3

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/map/t/q;

    move-object v1, v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_2
    iget-object v12, v3, Lcom/google/android/apps/gmm/map/t/y;->c:Lcom/google/android/apps/gmm/map/t/x;

    invoke-interface {v12, v10, v1}, Lcom/google/android/apps/gmm/map/t/x;->a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/gmm/map/t/y;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move v1, v5

    :goto_3
    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/z;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    new-instance v2, Lcom/google/android/apps/gmm/map/j/z;

    const/4 v3, 0x0

    sget-object v10, Lcom/google/android/apps/gmm/map/j/r;->a:Lcom/google/android/apps/gmm/map/j/r;

    invoke-direct {v2, v3, v4, v10}, Lcom/google/android/apps/gmm/map/j/z;-><init>(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 245
    :catch_0
    move-exception v1

    goto/16 :goto_0

    .line 233
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v1
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_3
    move-object v1, v7

    .line 238
    goto :goto_2

    :cond_4
    :try_start_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 239
    :try_start_8
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/t/ab;->b:Ljava/util/List;

    monitor-enter v8
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0

    :try_start_9
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v10, Lcom/google/android/apps/gmm/map/t/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t/z;->c:Lcom/google/android/apps/gmm/map/f/o;

    invoke-direct {v10, v1, v2}, Lcom/google/android/apps/gmm/map/t/ad;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/f/o;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t/z;->b:Ljava/util/SortedSet;

    invoke-interface {v2}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/t/p;

    iget-byte v3, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-eqz v3, :cond_6

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    sget-object v4, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    iget v4, v4, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v3, v3, v4

    check-cast v3, Lcom/google/android/apps/gmm/v/bb;

    check-cast v3, Lcom/google/android/apps/gmm/map/t/y;

    iget-boolean v4, v3, Lcom/google/android/apps/gmm/map/t/y;->b:Z

    if-eqz v4, :cond_6

    instance-of v4, v2, Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v4, :cond_7

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/map/t/q;

    move-object v4, v0

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_5
    iget-object v12, v3, Lcom/google/android/apps/gmm/map/t/y;->c:Lcom/google/android/apps/gmm/map/t/x;

    invoke-interface {v12, v10, v4}, Lcom/google/android/apps/gmm/map/t/x;->a(Lcom/google/android/apps/gmm/map/t/ad;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v4, Lcom/google/android/apps/gmm/map/j/r;->b:Lcom/google/android/apps/gmm/map/j/r;

    invoke-virtual {v3, v2, v1, v4}, Lcom/google/android/apps/gmm/map/t/y;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    move v2, v5

    :goto_6
    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t/z;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    new-instance v3, Lcom/google/android/apps/gmm/map/j/p;

    const/4 v4, 0x0

    sget-object v10, Lcom/google/android/apps/gmm/map/j/r;->b:Lcom/google/android/apps/gmm/map/j/r;

    invoke-direct {v3, v4, v1, v10}, Lcom/google/android/apps/gmm/map/j/p;-><init>(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    goto :goto_4

    :catchall_2
    move-exception v1

    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v1
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0

    :cond_7
    move-object v4, v7

    goto :goto_5

    :cond_8
    :try_start_b
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 242
    :try_start_c
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ab;->e:Lcom/google/android/apps/gmm/map/t/z;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/z;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_0

    :cond_9
    move v2, v6

    goto :goto_6

    :cond_a
    move v1, v6

    goto/16 :goto_3
.end method
