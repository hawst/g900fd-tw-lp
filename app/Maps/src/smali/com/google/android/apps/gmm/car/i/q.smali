.class Lcom/google/android/apps/gmm/car/i/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/i/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/car/i/k",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/gmm/car/bm;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/i/o;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/i/o;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/o;->e()V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    const-class v2, Lcom/google/android/apps/gmm/car/i/a/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->bM:I

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 275
    return-void

    .line 271
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->bL:I

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 226
    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/o;->e()V

    move v1, v5

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bm;

    new-instance v2, Lcom/google/android/apps/gmm/car/i/v;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/car/i/v;-><init>(Lcom/google/android/apps/gmm/car/i/l;I)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/i/o;->e:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/gmm/car/w;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    const-class v1, Lcom/google/android/apps/gmm/car/i/a/b;

    sget v2, Lcom/google/android/apps/gmm/l;->bs:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v8, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    new-instance v0, Lcom/google/android/apps/gmm/car/f/d;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/i/o;->e:Lcom/google/android/apps/gmm/car/w;

    sget-object v6, Lcom/google/android/apps/gmm/car/f/o;->a:Lcom/google/android/apps/gmm/car/f/o;

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/car/f/d;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/w;ZZLcom/google/android/apps/gmm/car/f/o;Z)V

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bm;

    new-instance v3, Lcom/google/android/apps/gmm/car/i/b/d;

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v6, v6, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v7, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v7, v7, Lcom/google/android/apps/gmm/car/i/o;->j:Lcom/google/android/apps/gmm/car/i/b/e;

    invoke-direct {v3, v6, v0, v7}, Lcom/google/android/apps/gmm/car/i/b/d;-><init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/i/b/e;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    const-class v2, Lcom/google/android/apps/gmm/car/i/a/e;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    goto :goto_1

    :cond_3
    iget-object v1, v8, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/q;->a:Lcom/google/android/apps/gmm/car/i/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/car/i/r;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/car/i/r;-><init>(Lcom/google/android/apps/gmm/car/i/q;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/i/q;->a()V

    .line 280
    return-void
.end method
