.class Lcom/google/android/apps/gmm/navigation/base/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/apps/gmm/base/a;

.field final synthetic c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/base/NavigationService;Landroid/content/Intent;Lcom/google/android/apps/gmm/base/a;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/base/h;->c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/base/h;->a:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/base/h;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/h;->a:Landroid/content/Intent;

    .line 266
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/h;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/navigation/b/d;->a(Landroid/net/Uri;Lcom/google/android/apps/gmm/x/a;)Lcom/google/android/apps/gmm/navigation/b/d;

    move-result-object v1

    .line 267
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/i;->a:[I

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/b/e;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 281
    :goto_0
    return-void

    .line 269
    :pswitch_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/b/d;->b:Lcom/google/android/apps/gmm/map/r/a/f;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/b/d;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/h;->c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->a()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v4

    if-eq v0, v4, :cond_0

    const/4 v4, 0x0

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->I:Lcom/google/n/f;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 275
    :catch_0
    move-exception v0

    .line 277
    const-string v1, "NavigationService"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/h;->c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 269
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/h;->c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iput-object v0, v2, Lcom/google/android/apps/gmm/navigation/g/c;->i:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/h;->c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/base/a/a;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/navigation/base/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/ae;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 272
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/h;->c:Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/base/a/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/b/d;->d:Lcom/google/android/apps/gmm/navigation/b/a;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/navigation/base/a/b;-><init>(Lcom/google/android/apps/gmm/navigation/b/a;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 267
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
