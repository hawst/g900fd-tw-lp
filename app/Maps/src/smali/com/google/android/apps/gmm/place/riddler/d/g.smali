.class public Lcom/google/android/apps/gmm/place/riddler/d/g;
.super Lcom/google/android/apps/gmm/place/riddler/d/d;
.source "PG"


# static fields
.field static final g:Ljava/lang/String;


# instance fields
.field final h:Lcom/google/android/apps/gmm/place/riddler/a;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/riddler/c/c;",
            ">;"
        }
    .end annotation
.end field

.field j:Z

.field k:Z

.field l:J

.field m:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field n:Z

.field private final o:Lcom/google/android/apps/gmm/place/riddler/d;

.field private final p:I

.field private q:I

.field private r:Ljava/lang/String;

.field private s:J

.field private t:Lcom/google/r/b/a/apt;

.field private u:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/gmm/place/riddler/d/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/riddler/d/g;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 96
    new-instance v1, Lcom/google/android/apps/gmm/place/riddler/d;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/riddler/d;-><init>(Lcom/google/android/apps/gmm/shared/net/r;)V

    .line 97
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->w()Lcom/google/android/apps/gmm/place/riddler/a;

    move-result-object v0

    .line 96
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/gmm/place/riddler/d/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/riddler/d;Lcom/google/android/apps/gmm/place/riddler/a;)V

    .line 98
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/riddler/d;Lcom/google/android/apps/gmm/place/riddler/a;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/d;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 74
    iput v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->q:I

    .line 75
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->j:Z

    .line 76
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->k:Z

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->l:J

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/d/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/riddler/d/h;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->u:Ljava/lang/Object;

    .line 104
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/apq;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->p:I

    .line 105
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->o:Lcom/google/android/apps/gmm/place/riddler/d;

    .line 106
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->h:Lcom/google/android/apps/gmm/place/riddler/a;

    .line 107
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v0

    .line 108
    iget v0, v0, Lcom/google/r/b/a/apq;->k:I

    invoke-static {v0}, Lcom/google/r/b/a/apt;->a(I)Lcom/google/r/b/a/apt;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/apt;->e:Lcom/google/r/b/a/apt;

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->t:Lcom/google/r/b/a/apt;

    .line 109
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/place/riddler/c/c;)V
    .locals 3

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v1

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/place/riddler/d/l;

    invoke-direct {v2, p0, v1, p1}, Lcom/google/android/apps/gmm/place/riddler/d/l;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/g;Ljava/lang/String;Lcom/google/android/apps/gmm/place/riddler/c/c;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 521
    return-void
.end method

.method private b(I)Lcom/google/android/apps/gmm/place/riddler/c/b;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 600
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 601
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/riddler/c/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 602
    :cond_0
    const/4 v0, 0x0

    .line 604
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, p1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/c/b;

    goto :goto_0
.end method

.method private o()Lcom/google/android/apps/gmm/place/riddler/c/a;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->i:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 590
    :goto_0
    return-object v0

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/c/c;

    .line 579
    instance-of v3, v0, Lcom/google/android/apps/gmm/place/riddler/c/a;

    if-eqz v3, :cond_1

    .line 583
    iget-object v3, v0, Lcom/google/android/apps/gmm/place/riddler/c/c;->f:Lcom/google/android/apps/gmm/place/riddler/c/d;

    sget-object v4, Lcom/google/android/apps/gmm/place/riddler/c/d;->b:Lcom/google/android/apps/gmm/place/riddler/c/d;

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->n:Z

    if-eqz v3, :cond_1

    .line 585
    :cond_2
    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/c/a;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 590
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/place/riddler/d/g;->b(I)Lcom/google/android/apps/gmm/place/riddler/c/b;

    move-result-object v1

    .line 146
    if-nez v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/place/riddler/c/b;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 547
    iget v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->d:I

    if-eq p1, v0, :cond_0

    .line 548
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->f:Lcom/google/android/libraries/curvular/h;

    .line 550
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/d;->a(I)V

    .line 551
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 408
    iput-object v5, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->i:Ljava/util/List;

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->m:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->m:Lcom/google/android/apps/gmm/x/o;

    .line 412
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 413
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->n:Z

    .line 416
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->m:Lcom/google/android/apps/gmm/x/o;

    .line 418
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->aj()Lcom/google/android/apps/gmm/place/riddler/c/e;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/place/riddler/d/j;

    invoke-direct {v2, p0, p2, v1}, Lcom/google/android/apps/gmm/place/riddler/d/j;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/g;Lcom/google/android/apps/gmm/x/o;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 420
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 421
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->r:Ljava/lang/String;

    .line 431
    :cond_2
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->s:J

    .line 432
    iget-wide v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->s:J

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->G()Lcom/google/android/apps/gmm/place/riddler/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/riddler/a/a;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->c:Z

    .line 441
    :goto_1
    iput v4, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->q:I

    .line 442
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->j:Z

    .line 443
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/place/riddler/d/d;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 444
    return-void

    .line 423
    :cond_3
    iput-object v5, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->r:Ljava/lang/String;

    goto :goto_0

    .line 435
    :cond_4
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->c:Z

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->G()Lcom/google/android/apps/gmm/place/riddler/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/riddler/a/a;->d()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->u:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 529
    return-void
.end method

.method protected final b(Lcom/google/android/apps/gmm/place/riddler/d/c;)Lcom/google/android/libraries/curvular/h;
    .locals 1

    .prologue
    .line 653
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/d/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/m;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/g;Lcom/google/android/apps/gmm/place/riddler/d/c;)V

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Integer;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->b(I)Lcom/google/android/apps/gmm/place/riddler/c/b;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/b;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->u:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 537
    return-void
.end method

.method public final c(Ljava/lang/Integer;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 194
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/place/riddler/d/g;->b(I)Lcom/google/android/apps/gmm/place/riddler/c/b;

    move-result-object v1

    .line 195
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 196
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->dJ:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 197
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->r:Ljava/lang/String;

    .line 198
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 199
    iget-object v1, v1, Lcom/google/android/apps/gmm/place/riddler/c/b;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 200
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/cf;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 246
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_6

    .line 248
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->a(Lcom/google/android/apps/gmm/place/riddler/c/c;)V

    .line 250
    iget-object v5, v0, Lcom/google/android/apps/gmm/place/riddler/c/c;->c:Lcom/google/n/f;

    .line 251
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->b(I)Lcom/google/android/apps/gmm/place/riddler/c/b;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 252
    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->o:Lcom/google/android/apps/gmm/place/riddler/d;

    invoke-static {}, Lcom/google/maps/g/is;->newBuilder()Lcom/google/maps/g/iu;

    move-result-object v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 251
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/b;->a:Lcom/google/n/f;

    goto :goto_0

    .line 252
    :cond_1
    iget v7, v1, Lcom/google/maps/g/iu;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v1, Lcom/google/maps/g/iu;->a:I

    iput-object v0, v1, Lcom/google/maps/g/iu;->b:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/maps/g/iu;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/is;

    invoke-static {}, Lcom/google/maps/g/ba;->newBuilder()Lcom/google/maps/g/bg;

    move-result-object v7

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, v7, Lcom/google/maps/g/bg;->a:I

    if-eq v1, v8, :cond_3

    new-instance v1, Lcom/google/n/ao;

    invoke-direct {v1}, Lcom/google/n/ao;-><init>()V

    iput-object v1, v7, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    iput v8, v7, Lcom/google/maps/g/bg;->a:I

    :cond_3
    iget-object v1, v7, Lcom/google/maps/g/bg;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v8, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v3, v1, Lcom/google/n/ao;->d:Z

    sget-object v0, Lcom/google/maps/g/bd;->b:Lcom/google/maps/g/bd;

    invoke-virtual {v7, v0}, Lcom/google/maps/g/bg;->a(Lcom/google/maps/g/bd;)Lcom/google/maps/g/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/bg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ba;

    invoke-virtual {v6, v5, v0}, Lcom/google/android/apps/gmm/place/riddler/d;->a(Lcom/google/n/f;Lcom/google/maps/g/ba;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 255
    iget v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->q:I

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ba:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 257
    iget v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->q:I

    iget v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->p:I

    if-lt v0, v1, :cond_4

    .line 258
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->c:Z

    .line 260
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/d;->c:Z

    if-nez v0, :cond_7

    move v0, v3

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    .line 261
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->j:Z

    .line 262
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->k:Z

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->l:J

    .line 264
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->f:Lcom/google/android/libraries/curvular/h;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/riddler/d/i;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/riddler/d/i;-><init>(Lcom/google/android/apps/gmm/place/riddler/d/g;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    const-wide/16 v4, 0x2134

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 291
    :cond_5
    :goto_2
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 293
    :cond_6
    return-object v2

    :cond_7
    move v0, v4

    .line 260
    goto :goto_1

    .line 287
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {}, Lcom/google/android/apps/gmm/place/riddler/RiddlerTutorialDialogFragment;->i()Lcom/google/android/apps/gmm/place/riddler/RiddlerTutorialDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ba:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_2
.end method

.method public final synthetic d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->c:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/a;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_0

    .line 177
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->gj:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 178
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->r:Ljava/lang/String;

    .line 179
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 180
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/c;->e:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 181
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    .line 214
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->dK:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 215
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->r:Ljava/lang/String;

    .line 216
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 217
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/riddler/c/c;->e:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 218
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 230
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_0

    .line 232
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dN:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 233
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 234
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->o()Lcom/google/android/apps/gmm/place/riddler/c/a;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_0

    .line 304
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/place/riddler/d/g;->a(Lcom/google/android/apps/gmm/place/riddler/c/c;)V

    .line 306
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/riddler/c/c;->c:Lcom/google/n/f;

    .line 307
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->o:Lcom/google/android/apps/gmm/place/riddler/d;

    invoke-static {}, Lcom/google/maps/g/ba;->newBuilder()Lcom/google/maps/g/bg;

    move-result-object v0

    sget-object v3, Lcom/google/maps/g/bd;->c:Lcom/google/maps/g/bd;

    invoke-virtual {v0, v3}, Lcom/google/maps/g/bg;->a(Lcom/google/maps/g/bd;)Lcom/google/maps/g/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/bg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ba;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/place/riddler/d;->a(Lcom/google/n/f;Lcom/google/maps/g/ba;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 312
    iget-wide v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->s:J

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->G()Lcom/google/android/apps/gmm/place/riddler/a/a;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/gmm/place/riddler/a/a;->a(J)V

    .line 314
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->h()Lcom/google/android/libraries/curvular/cf;

    .line 316
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 325
    iput v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->q:I

    .line 326
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->c:Z

    .line 327
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->j:Z

    .line 328
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 329
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 2

    .prologue
    .line 337
    sget-object v0, Lcom/google/android/apps/gmm/place/riddler/d/n;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->t:Lcom/google/r/b/a/apt;

    invoke-virtual {v1}, Lcom/google/r/b/a/apt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 349
    sget v0, Lcom/google/android/apps/gmm/d;->ap:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    :goto_0
    return-object v0

    .line 340
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/d;->aA:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 342
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/d;->R:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 344
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/d;->aa:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 346
    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/d;->al:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 337
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final l()Lcom/google/android/libraries/curvular/aq;
    .locals 2

    .prologue
    .line 358
    sget-object v0, Lcom/google/android/apps/gmm/place/riddler/d/n;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->t:Lcom/google/r/b/a/apt;

    invoke-virtual {v1}, Lcom/google/r/b/a/apt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 366
    sget v0, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    :goto_0
    return-object v0

    .line 362
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 358
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final m()Lcom/google/android/libraries/curvular/aq;
    .locals 2

    .prologue
    .line 375
    sget-object v0, Lcom/google/android/apps/gmm/place/riddler/d/n;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->t:Lcom/google/r/b/a/apt;

    invoke-virtual {v1}, Lcom/google/r/b/a/apt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 384
    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    :goto_0
    return-object v0

    .line 377
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/d;->ag:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 380
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0

    .line 375
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 393
    sget-object v0, Lcom/google/android/apps/gmm/place/riddler/d/n;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d/g;->t:Lcom/google/r/b/a/apt;

    invoke-virtual {v1}, Lcom/google/r/b/a/apt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 401
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    .line 397
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 393
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
