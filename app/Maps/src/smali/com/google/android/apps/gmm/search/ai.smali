.class public Lcom/google/android/apps/gmm/search/ai;
.super Lcom/google/android/apps/gmm/map/v/a;
.source "PG"


# static fields
.field static final c:Lcom/google/maps/a/a;


# instance fields
.field a:Z

.field transient b:Lcom/google/android/apps/gmm/map/internal/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/n/v;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    sput-object v0, Lcom/google/android/apps/gmm/search/ai;->c:Lcom/google/maps/a/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/v/a;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ai;->a:Z

    .line 616
    return-void
.end method

.method public constructor <init>(Lcom/google/e/a/a/a/b;)V
    .locals 1

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/v/a;-><init>(Lcom/google/e/a/a/a/b;)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/ai;->a:Z

    .line 639
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/qm;
    .locals 3

    .prologue
    .line 703
    .line 704
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x1e

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 705
    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v1

    .line 703
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/maps/g/qm;

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method protected final b()Lcom/google/e/a/a/a/d;
    .locals 1

    .prologue
    .line 643
    sget-object v0, Lcom/google/r/b/a/b/bd;->a:Lcom/google/e/a/a/a/d;

    return-object v0
.end method

.method public final c()Lcom/google/r/b/a/alh;
    .locals 3

    .prologue
    .line 713
    .line 714
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x19

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 715
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    .line 713
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Lcom/google/r/b/a/alh;

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/place/ck;
    .locals 9

    .prologue
    const/16 v3, 0xd

    const/4 v8, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 739
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 740
    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    move v0, v4

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {v1, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v4

    :goto_1
    if-nez v0, :cond_3

    .line 741
    const/4 v0, 0x0

    .line 745
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 740
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 744
    :cond_3
    const/16 v0, 0x1a

    invoke-virtual {v1, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 743
    new-instance v3, Lcom/google/android/apps/gmm/place/cl;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/place/cl;-><init>()V

    if-nez v0, :cond_4

    move-object v0, v3

    .line 745
    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/cl;->a()Lcom/google/android/apps/gmm/place/ck;

    move-result-object v0

    goto :goto_2

    .line 743
    :cond_4
    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_7

    move v1, v4

    :goto_4
    if-eqz v1, :cond_5

    const/16 v1, 0x15

    invoke-virtual {v0, v4, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v1, v6

    invoke-static {v1}, Lcom/google/android/apps/gmm/place/cm;->a(I)Lcom/google/android/apps/gmm/place/cm;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/apps/gmm/place/cl;->b:Lcom/google/android/apps/gmm/place/cm;

    :cond_5
    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_8

    move v1, v4

    :goto_5
    if-eqz v1, :cond_6

    const/16 v1, 0x18

    invoke-virtual {v0, v5, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/apps/gmm/place/cl;->c:Ljava/lang/Boolean;

    :cond_6
    :goto_6
    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-ge v2, v1, :cond_9

    const/16 v1, 0x1c

    invoke-virtual {v0, v8, v2, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/apps/gmm/place/cl;->a:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5

    :cond_9
    move-object v0, v3

    goto :goto_3
.end method

.method public final e()Lcom/google/maps/g/hy;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x13

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 749
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 750
    iget-object v3, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_2

    move v3, v2

    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {v4, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    if-nez v1, :cond_3

    .line 753
    :goto_1
    return-object v0

    :cond_2
    move v3, v1

    .line 750
    goto :goto_0

    .line 754
    :cond_3
    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v1

    .line 753
    if-eqz v4, :cond_4

    const/16 v0, 0x19

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :cond_4
    if-eqz v0, :cond_5

    :goto_2
    check-cast v0, Lcom/google/maps/g/hy;

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public final f()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/akl;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x1c

    .line 799
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    .line 800
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    .line 801
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v0

    .line 802
    :goto_1
    if-ge v2, v3, :cond_1

    .line 804
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v4, 0x15

    invoke-virtual {v0, v6, v2, v4}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    .line 803
    invoke-static {v0}, Lcom/google/r/b/a/akl;->a(I)Lcom/google/r/b/a/akl;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 802
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v1, v0

    .line 799
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 808
    :goto_2
    return-object v0

    :cond_2
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_2
.end method

.method public final g()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/akl;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x1d

    .line 812
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    .line 813
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    .line 814
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v0

    .line 815
    :goto_1
    if-ge v2, v3, :cond_1

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v4, 0x15

    invoke-virtual {v0, v6, v2, v4}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    .line 816
    invoke-static {v0}, Lcom/google/r/b/a/akl;->a(I)Lcom/google/r/b/a/akl;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 815
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v1, v0

    .line 812
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 821
    :goto_2
    return-object v0

    :cond_2
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_2
.end method
