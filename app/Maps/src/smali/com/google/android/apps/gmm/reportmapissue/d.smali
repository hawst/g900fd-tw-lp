.class public Lcom/google/android/apps/gmm/reportmapissue/d;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:[B

.field private final c:Lcom/google/android/apps/gmm/reportmapissue/c;

.field private d:Lcom/google/android/apps/gmm/reportmapissue/e;

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/c;Lcom/google/android/apps/gmm/reportmapissue/e;)V
    .locals 2

    .prologue
    .line 92
    sget-object v0, Lcom/google/r/b/a/el;->aA:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/ag;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 93
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    .line 94
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->d:Lcom/google/android/apps/gmm/reportmapissue/e;

    .line 95
    return-void
.end method


# virtual methods
.method protected final M_()J
    .locals 2

    .prologue
    .line 208
    const-wide/16 v0, 0xbb8

    return-wide v0
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    .line 176
    invoke-static {p1, v1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->e:I

    .line 180
    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 182
    const/16 v0, 0x1a

    invoke-virtual {p1, v4, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 184
    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    .line 189
    :cond_2
    const/4 v0, 0x0

    return-object v0

    :cond_3
    move v2, v0

    .line 180
    goto :goto_0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 10

    .prologue
    const/4 v7, 0x5

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 111
    new-instance v3, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/ag;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v3, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 112
    const/16 v2, 0xd

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 113
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 115
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->h:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v8, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 117
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->i:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 118
    const/16 v2, 0xe

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->i:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 120
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->j:Z

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_0
    iget-object v4, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v9, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 124
    const/16 v2, 0x8

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->l:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 126
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_2
    move v2, v1

    :goto_1
    if-nez v2, :cond_6

    .line 127
    const/16 v2, 0xb

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->c:Ljava/lang/String;

    if-gez v2, :cond_5

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 121
    :cond_3
    sget-object v2, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_0

    :cond_4
    move v2, v0

    .line 126
    goto :goto_1

    .line 127
    :cond_5
    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 131
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 132
    const/4 v4, 0x4

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->d:Lcom/google/android/apps/gmm/map/b/a/w;

    if-eqz v2, :cond_7

    .line 136
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->d:Lcom/google/android/apps/gmm/map/b/a/w;

    .line 137
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/w;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->e:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v5, v5, Lcom/google/android/apps/gmm/reportmapissue/c;->f:I

    iget-object v6, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->d:Lcom/google/android/apps/gmm/map/b/a/w;

    .line 138
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/b/a/w;->b:Lcom/google/android/apps/gmm/map/b/a/be;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/be;->b:I

    .line 137
    invoke-static {v2, v4, v5, v6}, Lcom/google/android/apps/gmm/map/b/a/v;->a(Lcom/google/android/apps/gmm/map/b/a/u;III)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 139
    iget-object v4, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v7, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 143
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    .line 144
    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/ag;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 151
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    if-eq v2, v7, :cond_8

    const/4 v5, 0x6

    if-eq v2, v5, :cond_8

    const/4 v5, 0x7

    if-ne v2, v5, :cond_f

    :cond_8
    move v2, v1

    :goto_2
    if-eqz v2, :cond_10

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->n:I

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->m:I

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v9, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 154
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v8, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 158
    :cond_9
    :goto_3
    iget-object v2, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_11

    move v2, v1

    :goto_4
    if-nez v2, :cond_a

    invoke-virtual {v4, v8}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_12

    :cond_a
    move v2, v1

    :goto_5
    if-eqz v2, :cond_b

    .line 159
    const/4 v2, 0x6

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 162
    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->b:[B

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->b:[B

    array-length v2, v2

    if-lez v2, :cond_c

    .line 163
    const/16 v2, 0x9

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->b:[B

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v2, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 166
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->a:Ljava/lang/String;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_e

    :cond_d
    move v0, v1

    :cond_e
    if-nez v0, :cond_14

    .line 167
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->a:Ljava/lang/String;

    if-gez v0, :cond_13

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_f
    move v2, v0

    .line 151
    goto :goto_2

    .line 155
    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    const/16 v5, 0x9

    if-ne v2, v5, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->c:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v8, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    goto :goto_3

    :cond_11
    move v2, v0

    .line 158
    goto :goto_4

    :cond_12
    move v2, v0

    goto :goto_5

    .line 167
    :cond_13
    iget-object v2, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 170
    :cond_14
    return-object v3
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->d:Lcom/google/android/apps/gmm/reportmapissue/e;

    if-eqz v0, :cond_1

    .line 197
    if-nez p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->e:I

    if-ne v0, v1, :cond_2

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->d:Lcom/google/android/apps/gmm/reportmapissue/e;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/e;->a(I)V

    .line 204
    :cond_1
    :goto_0
    return-void

    .line 200
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->e:I

    if-nez v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d;->d:Lcom/google/android/apps/gmm/reportmapissue/e;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/e;->a(I)V

    goto :goto_0
.end method
