.class public final enum Lcom/google/android/apps/gmm/directions/f/c/e;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/f/c/e;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/f/c/e;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/f/c/e;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/directions/f/c/e;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c/e;

    const-string v1, "NEAR_POLYLINE"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/directions/f/c/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->a:Lcom/google/android/apps/gmm/directions/f/c/e;

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c/e;

    const-string v1, "NEAR_START"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/directions/f/c/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->b:Lcom/google/android/apps/gmm/directions/f/c/e;

    .line 147
    new-array v0, v4, [Lcom/google/android/apps/gmm/directions/f/c/e;

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/c/e;->a:Lcom/google/android/apps/gmm/directions/f/c/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/directions/f/c/e;->b:Lcom/google/android/apps/gmm/directions/f/c/e;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->d:[Lcom/google/android/apps/gmm/directions/f/c/e;

    .line 186
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/c/f;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 196
    iput p3, p0, Lcom/google/android/apps/gmm/directions/f/c/e;->c:I

    .line 197
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/directions/f/c/e;
    .locals 1

    .prologue
    .line 174
    packed-switch p0, :pswitch_data_0

    .line 177
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 175
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->a:Lcom/google/android/apps/gmm/directions/f/c/e;

    goto :goto_0

    .line 176
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->b:Lcom/google/android/apps/gmm/directions/f/c/e;

    goto :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/f/c/e;
    .locals 1

    .prologue
    .line 147
    const-class v0, Lcom/google/android/apps/gmm/directions/f/c/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/c/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/f/c/e;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/c/e;->d:[Lcom/google/android/apps/gmm/directions/f/c/e;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/f/c/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/f/c/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/google/android/apps/gmm/directions/f/c/e;->c:I

    return v0
.end method
