.class public Lcom/google/android/apps/gmm/navigation/base/NavigationService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/g/c;

.field private b:Lcom/google/android/apps/gmm/navigation/logging/d;

.field private c:Lcom/google/android/apps/gmm/shared/c/a/a;

.field private d:Lcom/google/android/apps/gmm/shared/c/a/a;

.field private e:Lcom/google/android/apps/gmm/navigation/h/a;

.field private f:Lcom/google/android/apps/gmm/navigation/g/h;

.field private g:Z

.field private h:Lcom/google/android/apps/gmm/navigation/g/m;

.field private i:Lcom/google/android/apps/gmm/navigation/g/b;

.field private j:Lcom/google/android/apps/gmm/navigation/a/ao;

.field private k:Lcom/google/android/apps/gmm/navigation/navui/z;

.field private l:Lcom/google/android/apps/gmm/navigation/base/a;

.field private m:Lcom/google/android/apps/gmm/navigation/navui/ba;

.field private final n:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->g:Z

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/navigation/base/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/base/j;-><init>(Lcom/google/android/apps/gmm/navigation/base/NavigationService;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->n:Landroid/os/IBinder;

    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 351
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 352
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 353
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->n:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x1

    .line 147
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 150
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v11

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/android/apps/gmm/base/a;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/navigation/logging/d;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/a/d;->d:Lcom/google/android/apps/gmm/navigation/logging/m;

    .line 158
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->k()Lcom/google/android/apps/gmm/replay/a/a;

    move-result-object v4

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/logging/d;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/navigation/logging/m;Lcom/google/android/apps/gmm/replay/a/a;Lcom/google/android/apps/gmm/shared/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->b:Lcom/google/android/apps/gmm/navigation/logging/d;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->b:Lcom/google/android/apps/gmm/navigation/logging/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/logging/d;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 167
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v1

    .line 168
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    .line 166
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/shared/c/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->c:Lcom/google/android/apps/gmm/shared/c/a/a;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->c:Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/shared/c/a/a;->setPriority(I)V

    .line 171
    new-instance v0, Lcom/google/android/apps/gmm/navigation/h/a;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    invoke-direct {v0, v9, v1}, Lcom/google/android/apps/gmm/navigation/h/a;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/internal/d/bd;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->e:Lcom/google/android/apps/gmm/navigation/h/a;

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->e:Lcom/google/android/apps/gmm/navigation/h/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/h/a;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/h/d;->g:Lcom/google/android/apps/gmm/map/internal/d/ai;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/ai;->a(Lcom/google/android/apps/gmm/map/internal/d/at;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/h/d;->l:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/h/a;->b:Lcom/google/android/apps/gmm/navigation/h/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/h/d;->g:Lcom/google/android/apps/gmm/map/internal/d/ai;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/ai;->a(Lcom/google/android/apps/gmm/map/internal/d/at;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/h/d;->l:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 174
    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/h;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    .line 175
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->k()Lcom/google/android/apps/gmm/replay/a/a;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/navigation/g/h;-><init>(Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/replay/a/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->f:Lcom/google/android/apps/gmm/navigation/g/h;

    .line 178
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/m;

    .line 181
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    .line 182
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->f:Lcom/google/android/apps/gmm/navigation/g/h;

    .line 184
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v2

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->k:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v1, :cond_0

    invoke-virtual {v2, v6}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v6

    .line 186
    :goto_0
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v8

    move-object v1, v10

    move-object v2, v11

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/navigation/g/m;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/car/a/g;Lcom/google/android/apps/gmm/navigation/g/h;Lcom/google/android/apps/gmm/map/internal/d/as;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->h:Lcom/google/android/apps/gmm/navigation/g/m;

    .line 188
    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/b;

    .line 191
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v4

    .line 192
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->f:Lcom/google/android/apps/gmm/navigation/g/h;

    .line 195
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v8

    move-object v3, v11

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/navigation/g/b;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/navigation/b/c;Lcom/google/android/apps/gmm/car/a/g;Lcom/google/android/apps/gmm/navigation/g/h;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->i:Lcom/google/android/apps/gmm/navigation/g/b;

    .line 198
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "quitquitquit"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 198
    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/z;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/z;-><init>(Landroid/app/Service;Landroid/content/Intent;Lcom/google/android/apps/gmm/base/a;)V

    invoke-virtual {p0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->k:Lcom/google/android/apps/gmm/navigation/navui/z;

    .line 201
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 202
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v1

    .line 203
    invoke-interface {v11}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    .line 201
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/shared/c/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->d:Lcom/google/android/apps/gmm/shared/c/a/a;

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->d:Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/shared/c/a/a;->setPriority(I)V

    .line 205
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ba;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/ba;-><init>(Landroid/content/Context;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/ba;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->m:Lcom/google/android/apps/gmm/navigation/navui/ba;

    .line 207
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    .line 210
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v1

    .line 211
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v2

    .line 209
    invoke-virtual {v0, v1, v2, v9}, Lcom/google/android/apps/gmm/navigation/g/c;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/navigation/b/c;Lcom/google/android/apps/gmm/shared/net/ad;)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/h/c;->b()V

    .line 216
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/ao;

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/navigation/a/ao;-><init>(Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/b/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->j:Lcom/google/android/apps/gmm/navigation/a/ao;

    .line 218
    return-void

    :cond_0
    move-object v6, v1

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 15

    .prologue
    const/high16 v14, 0x400000

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 362
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 363
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    sget-object v4, Lcom/google/r/b/a/av;->o:Lcom/google/r/b/a/av;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/h/c;->a(Lcom/google/r/b/a/av;)V

    .line 367
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v4

    new-array v5, v1, [Lcom/google/android/apps/gmm/z/b/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->b:Lcom/google/android/apps/gmm/navigation/logging/d;

    new-instance v7, Lcom/google/android/apps/gmm/navigation/logging/a/a;

    invoke-static {}, Lcom/google/b/f/b/a/bi;->newBuilder()Lcom/google/b/f/b/a/bk;

    move-result-object v8

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v10

    iget-wide v12, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->h:J

    sub-long/2addr v10, v12

    long-to-int v0, v10

    div-int/lit16 v0, v0, 0x3e8

    iput v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->i:I

    iget v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->i:I

    iget v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v9, v9, 0x200

    iput v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, v8, Lcom/google/b/f/b/a/bk;->l:I

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->l:Z

    iget v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x10000

    or-int/2addr v9, v10

    iput v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput-boolean v0, v8, Lcom/google/b/f/b/a/bk;->s:Z

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->j:Z

    iget v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x80000

    or-int/2addr v9, v10

    iput v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput-boolean v0, v8, Lcom/google/b/f/b/a/bk;->v:Z

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/navigation/logging/k;->a(Lcom/google/b/f/b/a/bk;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->d:Lcom/google/android/apps/gmm/navigation/logging/l;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/navigation/logging/l;->a(Lcom/google/b/f/b/a/bk;)V

    iget-object v9, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->e:Lcom/google/android/apps/gmm/navigation/logging/a;

    sget-boolean v0, Lcom/google/android/apps/gmm/navigation/logging/a;->h:Z

    if-nez v0, :cond_1

    iget v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    and-int/2addr v0, v14

    if-ne v0, v14, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->a:Lcom/google/b/c/hz;

    invoke-interface {v0}, Lcom/google/b/c/hz;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v8, v0}, Lcom/google/b/f/b/a/bk;->a(Lcom/google/b/f/b/a/bl;)Lcom/google/b/f/b/a/bk;

    :cond_2
    iget-object v0, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->b:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v10

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x1

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->b:I

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x2

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    iget v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v11, v11, 0x4

    iput v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, v10, Lcom/google/b/f/b/a/aq;->d:F

    invoke-virtual {v10}, Lcom/google/b/f/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ao;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/navigation/logging/b;

    invoke-direct {v0, v9}, Lcom/google/android/apps/gmm/navigation/logging/b;-><init>(Lcom/google/android/apps/gmm/navigation/logging/a;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/logging/b;->c()Lcom/google/b/c/io;

    move-result-object v0

    iget-object v10, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->a:Lcom/google/b/c/hz;

    invoke-interface {v10}, Lcom/google/b/c/hz;->d()Ljava/util/Set;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/google/b/c/io;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v10, "network"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    sget-object v0, Lcom/google/b/f/b/a/bl;->b:Lcom/google/b/f/b/a/bl;

    goto :goto_1

    :cond_4
    const-string v10, "gps"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    sget-object v0, Lcom/google/b/f/b/a/bl;->a:Lcom/google/b/f/b/a/bl;

    goto :goto_1

    :cond_5
    const-string v10, "fused"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/b/f/b/a/bl;->c:Lcom/google/b/f/b/a/bl;

    goto :goto_1

    :cond_6
    move-object v0, v3

    goto :goto_1

    :cond_7
    iget-object v10, v8, Lcom/google/b/f/b/a/bk;->w:Lcom/google/n/ao;

    iget-object v11, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v10, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v10, Lcom/google/n/ao;->d:Z

    iget v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x800000

    or-int/2addr v0, v10

    iput v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const-string v0, "LocationStats"

    iget-object v10, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->b:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0xd

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "snapDistance "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-array v11, v2, [Ljava/lang/Object;

    invoke-static {v0, v10, v11}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->c:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v10

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x1

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->b:I

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x2

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    iget v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v11, v11, 0x4

    iput v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, v10, Lcom/google/b/f/b/a/aq;->d:F

    invoke-virtual {v10}, Lcom/google/b/f/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ao;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v10, v8, Lcom/google/b/f/b/a/bk;->x:Lcom/google/n/ao;

    iget-object v11, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v10, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v10, Lcom/google/n/ao;->d:Z

    iget v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x1000000

    or-int/2addr v0, v10

    iput v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const-string v0, "LocationStats"

    iget-object v10, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->c:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x9

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "accuracy "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-array v11, v2, [Ljava/lang/Object;

    invoke-static {v0, v10, v11}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->d:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v10

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x1

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->b:I

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x2

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    iget v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v11, v11, 0x4

    iput v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, v10, Lcom/google/b/f/b/a/aq;->d:F

    invoke-virtual {v10}, Lcom/google/b/f/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ao;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget-object v10, v8, Lcom/google/b/f/b/a/bk;->y:Lcom/google/n/ao;

    iget-object v11, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v10, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v10, Lcom/google/n/ao;->d:Z

    iget v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x2000000

    or-int/2addr v0, v10

    iput v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const-string v0, "LocationStats"

    iget-object v10, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->d:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x10

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "expecationDelta "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-array v11, v2, [Ljava/lang/Object;

    invoke-static {v0, v10, v11}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->e:F

    float-to-int v0, v0

    iget v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    const v11, 0x8000

    or-int/2addr v10, v11

    iput v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, v8, Lcom/google/b/f/b/a/bk;->r:I

    const-string v0, "LocationStats"

    iget v9, v9, Lcom/google/android/apps/gmm/navigation/logging/a;->e:F

    new-instance v10, Ljava/lang/StringBuilder;

    const/16 v11, 0x20

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "distanceTraveled "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-array v10, v2, [Ljava/lang/Object;

    invoke-static {v0, v9, v10}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->f:Lcom/google/android/apps/gmm/navigation/logging/m;

    iget-boolean v9, v0, Lcom/google/android/apps/gmm/navigation/logging/m;->e:Z

    if-eqz v9, :cond_c

    iget-object v9, v0, Lcom/google/android/apps/gmm/navigation/logging/m;->a:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v11, 0x40000000    # 2.0f

    or-int/2addr v10, v11

    iput v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput-object v9, v8, Lcom/google/b/f/b/a/bk;->D:Ljava/lang/Object;

    iget-object v9, v0, Lcom/google/android/apps/gmm/navigation/logging/m;->b:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v11, -0x80000000

    or-int/2addr v10, v11

    iput v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput-object v9, v8, Lcom/google/b/f/b/a/bk;->E:Ljava/lang/Object;

    iget v9, v0, Lcom/google/android/apps/gmm/navigation/logging/m;->c:I

    iget v10, v8, Lcom/google/b/f/b/a/bk;->b:I

    or-int/lit8 v10, v10, 0x1

    iput v10, v8, Lcom/google/b/f/b/a/bk;->b:I

    iput v9, v8, Lcom/google/b/f/b/a/bk;->F:I

    iget v9, v0, Lcom/google/android/apps/gmm/navigation/logging/m;->d:I

    iget v10, v8, Lcom/google/b/f/b/a/bk;->b:I

    or-int/lit8 v10, v10, 0x2

    iput v10, v8, Lcom/google/b/f/b/a/bk;->b:I

    iput v9, v8, Lcom/google/b/f/b/a/bk;->G:I

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/logging/m;->f:I

    iget v9, v8, Lcom/google/b/f/b/a/bk;->b:I

    or-int/lit8 v9, v9, 0x4

    iput v9, v8, Lcom/google/b/f/b/a/bk;->b:I

    iput v0, v8, Lcom/google/b/f/b/a/bk;->H:I

    :cond_c
    iget-object v9, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->g:Lcom/google/android/apps/gmm/navigation/logging/j;

    iget-object v0, v9, Lcom/google/android/apps/gmm/navigation/logging/j;->a:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v10

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x1

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->b:I

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x2

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    iget v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v11, v11, 0x4

    iput v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, v10, Lcom/google/b/f/b/a/aq;->d:F

    invoke-virtual {v10}, Lcom/google/b/f/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ao;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget-object v10, v8, Lcom/google/b/f/b/a/bk;->z:Lcom/google/n/ao;

    iget-object v11, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v10, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v10, Lcom/google/n/ao;->d:Z

    iget v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x4000000

    or-int/2addr v0, v10

    iput v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    iget-object v0, v9, Lcom/google/android/apps/gmm/navigation/logging/j;->b:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v10

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x1

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->b:I

    iget v11, v0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v12, v12, 0x2

    iput v12, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v11, v10, Lcom/google/b/f/b/a/aq;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    iget v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v11, v11, 0x4

    iput v11, v10, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, v10, Lcom/google/b/f/b/a/aq;->d:F

    invoke-virtual {v10}, Lcom/google/b/f/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ao;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    iget-object v10, v8, Lcom/google/b/f/b/a/bk;->A:Lcom/google/n/ao;

    iget-object v11, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v10, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v10, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v10, Lcom/google/n/ao;->d:Z

    iget v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x8000000

    or-int/2addr v0, v10

    iput v0, v8, Lcom/google/b/f/b/a/bk;->a:I

    iget v0, v9, Lcom/google/android/apps/gmm/navigation/logging/j;->c:I

    iget v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v11, 0x10000000

    or-int/2addr v10, v11

    iput v10, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, v8, Lcom/google/b/f/b/a/bk;->B:I

    iget v0, v9, Lcom/google/android/apps/gmm/navigation/logging/j;->d:I

    iget v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v10, 0x20000000

    or-int/2addr v9, v10

    iput v9, v8, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, v8, Lcom/google/b/f/b/a/bk;->C:I

    invoke-virtual {v8}, Lcom/google/b/f/b/a/bk;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/bi;

    iget-object v8, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v7, v0, v8}, Lcom/google/android/apps/gmm/navigation/logging/a/a;-><init>(Lcom/google/b/f/b/a/bi;Lcom/google/android/apps/gmm/shared/c/f;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_f

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/logging/d;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/apps/gmm/z/b/a;->d:Lcom/google/o/b/a/v;

    :cond_f
    aput-object v7, v5, v2

    invoke-interface {v4, v5}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->b:Lcom/google/android/apps/gmm/navigation/logging/d;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/logging/d;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/h/c;->c()V

    .line 385
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->g:Z

    if-eqz v0, :cond_14

    .line 386
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->g:Z

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/g;->f:Z

    if-nez v0, :cond_11

    move v0, v1

    .line 395
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->j:Lcom/google/android/apps/gmm/navigation/a/ao;

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Z)V

    iget-boolean v0, v4, Lcom/google/android/apps/gmm/navigation/a/ao;->c:Z

    if-eqz v0, :cond_10

    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/a/ao;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/navigation/a/ao;->c:Z

    .line 397
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->i:Lcom/google/android/apps/gmm/navigation/g/b;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/g/b;->b:Z

    if-nez v4, :cond_12

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_11
    move v0, v2

    .line 391
    goto :goto_2

    .line 397
    :cond_12
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/b;->b:Z

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->h:Lcom/google/android/apps/gmm/navigation/g/m;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->g:Z

    if-nez v4, :cond_13

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_13
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->a:Lcom/google/android/apps/gmm/navigation/g/v;

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/g/v;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v5, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/m;->g:Z

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/m;->f:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/navigation/g/o;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/navigation/g/o;-><init>(Lcom/google/android/apps/gmm/navigation/g/m;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v4, v5, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 403
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->f:Lcom/google/android/apps/gmm/navigation/g/h;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    if-eqz v4, :cond_15

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/h;->a:Lcom/google/android/apps/gmm/replay/a/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/replay/a/a;->a()V

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    .line 404
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->e:Lcom/google/android/apps/gmm/navigation/h/a;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/h/a;->a:Lcom/google/android/apps/gmm/navigation/h/d;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/navigation/h/d;->b()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/h/a;->b:Lcom/google/android/apps/gmm/navigation/h/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/h/d;->b()V

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->l:Lcom/google/android/apps/gmm/navigation/base/a;

    if-eqz v0, :cond_16

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->l:Lcom/google/android/apps/gmm/navigation/base/a;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/base/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 408
    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->l:Lcom/google/android/apps/gmm/navigation/base/a;

    .line 411
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->k:Lcom/google/android/apps/gmm/navigation/navui/z;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    invoke-virtual {v4, v1}, Landroid/app/Service;->stopForeground(Z)V

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/z;->e:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/z;->b:Landroid/app/NotificationManager;

    const v2, 0x25f40b4

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/z;->c:Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->a()V

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->m:Lcom/google/android/apps/gmm/navigation/navui/ba;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/ba;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/ba;->b:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v1

    if-eqz v1, :cond_17

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/ba;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/bb;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/navui/bb;-><init>(Lcom/google/android/apps/gmm/navigation/navui/ba;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->WEARABLE_DATA:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 414
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->d:Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->quit()Z

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->a:Lcom/google/android/apps/gmm/navigation/g/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->j:Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/c;->k:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->g:Lcom/google/android/apps/gmm/navigation/g/f;

    if-eqz v1, :cond_18

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/c;->g:Lcom/google/android/apps/gmm/navigation/g/f;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/c;->g:Lcom/google/android/apps/gmm/navigation/g/f;

    :cond_18
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->h:Lcom/google/android/apps/gmm/navigation/g/e;

    if-eqz v1, :cond_19

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/c;->h:Lcom/google/android/apps/gmm/navigation/g/e;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/c;->h:Lcom/google/android/apps/gmm/navigation/g/e;

    :cond_19
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/j/a/b;

    invoke-direct {v1, v3, v3}, Lcom/google/android/apps/gmm/navigation/j/a/b;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/f;Lcom/google/android/apps/gmm/navigation/g/b/d;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->c:Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->quit()Z

    .line 421
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 222
    const-string v0, "quitquitquit"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->stopSelf()V

    .line 229
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 225
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onStartInternal. Intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    sget-object v3, Lcom/google/r/b/a/av;->n:Lcom/google/r/b/a/av;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/h/c;->a(Lcom/google/r/b/a/av;)V

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->g:Z

    if-nez v0, :cond_5

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->g:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->h:Lcom/google/android/apps/gmm/navigation/g/m;

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/navigation/g/m;->g:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/m;->a:Lcom/google/android/apps/gmm/navigation/g/v;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/v;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/m;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/navigation/g/m;->g:Z

    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/m;->f:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/navigation/g/n;

    invoke-direct {v4, v3}, Lcom/google/android/apps/gmm/navigation/g/n;-><init>(Lcom/google/android/apps/gmm/navigation/g/m;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v4, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->i:Lcom/google/android/apps/gmm/navigation/g/b;

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/navigation/g/b;->b:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/navigation/g/b;->b:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->j:Lcom/google/android/apps/gmm/navigation/a/ao;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/ao;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/a/ao;->c:Z

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->l:Lcom/google/android/apps/gmm/navigation/base/a;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->l:Lcom/google/android/apps/gmm/navigation/base/a;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/base/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    :cond_6
    new-instance v1, Lcom/google/android/apps/gmm/navigation/base/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/base/a;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationService;->l:Lcom/google/android/apps/gmm/navigation/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/navigation/base/h;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/gmm/navigation/base/h;-><init>(Lcom/google/android/apps/gmm/navigation/base/NavigationService;Landroid/content/Intent;Lcom/google/android/apps/gmm/base/a;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto/16 :goto_0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 429
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/gmm/navigation/base/NavigationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 430
    return-void
.end method
