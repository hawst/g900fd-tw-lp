.class public Lcom/google/android/apps/gmm/base/views/QuHeaderView;
.super Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/l/a/ab;

.field public b:Landroid/view/View;

.field private c:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/a/ab;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->a:Lcom/google/android/apps/gmm/base/l/a/ab;

    .line 33
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 34
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/n;

    .line 35
    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 36
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->b:Landroid/view/View;

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->a:Lcom/google/android/apps/gmm/base/l/a/ab;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 38
    sget v0, Lcom/google/android/apps/gmm/g;->cA:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->setId(I)V

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Z)Landroid/view/View;
    .locals 5

    .prologue
    .line 127
    if-eqz p2, :cond_1

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/gmm/f;->gs:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    .line 130
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/gmm/g;->cA:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    sget-object v1, Lcom/google/android/apps/gmm/base/support/c;->b:Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v1

    neg-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->c:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method public final setFragment(Landroid/app/Fragment;)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public final setTitle(I)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 58
    return-void
.end method
