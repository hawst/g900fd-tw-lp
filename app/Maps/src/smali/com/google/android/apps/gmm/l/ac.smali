.class public Lcom/google/android/apps/gmm/l/ac;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/gmm/l/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    .line 81
    const-string v0, ","

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/ac;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    return-void
.end method

.method static a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    if-nez v0, :cond_0

    .line 120
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 122
    :cond_0
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 123
    if-ltz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, v1, 0x1

    if-le v2, v3, :cond_1

    .line 124
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 137
    if-nez p0, :cond_0

    move-object v0, v1

    .line 167
    :goto_0
    return-object v0

    .line 140
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 141
    const-string v3, "loc:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 142
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "()"

    invoke-static {v3}, Lcom/google/b/a/f;->a(Ljava/lang/CharSequence;)Lcom/google/b/a/f;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/b/a/f;->d(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 146
    :goto_1
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/l/ac;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v2

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 147
    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 148
    if-ltz v0, :cond_1

    .line 152
    :cond_2
    if-eq v0, v2, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_5

    .line 153
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    const-string v0, "Malformed latlng string, expected comma: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move-object v0, v1

    .line 154
    goto :goto_0

    .line 153
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 165
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    const-string v0, "Malformed latlng string, expected double: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_3
    move-object v0, v1

    .line 167
    goto :goto_0

    .line 156
    :cond_5
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v3, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 157
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object v0, v1

    .line 159
    goto/16 :goto_0

    .line 161
    :cond_6
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/u;->b(Ljava/lang/String;)I

    move-result v2

    .line 162
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/u;->b(Ljava/lang/String;)I

    move-result v0

    .line 163
    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 165
    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    move-object v3, v0

    goto/16 :goto_1
.end method

.method static a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 243
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 244
    const/4 v0, 0x0

    .line 265
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 243
    goto :goto_0

    .line 247
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 248
    invoke-static {v2}, Lcom/google/android/apps/gmm/l/ac;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/af;

    move-result-object v3

    .line 249
    if-nez v3, :cond_4

    .line 251
    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 252
    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 253
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    .line 262
    :goto_2
    if-eqz p2, :cond_3

    .line 263
    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/r/a/aq;)V

    .line 265
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_1

    .line 255
    :cond_4
    iget-object v2, v3, Lcom/google/android/apps/gmm/l/af;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 256
    iget-object v1, v3, Lcom/google/android/apps/gmm/l/af;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 260
    :goto_3
    iget-object v1, v3, Lcom/google/android/apps/gmm/l/af;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_2

    .line 258
    :cond_5
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    goto :goto_3
.end method

.method static a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;FF)Ljava/lang/Float;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 188
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 189
    if-nez v1, :cond_0

    .line 200
    :goto_0
    return-object v0

    .line 195
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 196
    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 197
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    const-string v1, "Malformed maps.google.com url, expected float: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 497
    invoke-static {}, Lcom/google/o/f/b;->newBuilder()Lcom/google/o/f/d;

    move-result-object v0

    .line 498
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v1, v0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/o/f/d;->a:I

    iput-wide v2, v0, Lcom/google/o/f/d;->d:J

    .line 499
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v1, v0, Lcom/google/o/f/d;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Lcom/google/o/f/d;->a:I

    iput-wide v2, v0, Lcom/google/o/f/d;->e:J

    .line 500
    invoke-virtual {v0}, Lcom/google/o/f/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/f/b;

    .line 501
    invoke-static {}, Lcom/google/b/e/a;->d()Lcom/google/b/e/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/o/f/b;->l()[B

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, v1

    check-cast v0, [B

    const/4 v3, 0x0

    array-length v1, v1

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/b/e/a;->a([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/r/a/aq;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 275
    :try_start_0
    invoke-static {}, Lcom/google/b/e/a;->d()Lcom/google/b/e/a;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/b/e/a;->a(Ljava/lang/CharSequence;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/o/f/b;->a([B)Lcom/google/o/f/b;

    move-result-object v3

    .line 276
    iget v2, v3, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    iget v2, v3, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    .line 277
    iget v2, v3, Lcom/google/o/f/b;->c:I

    iget v4, v3, Lcom/google/o/f/b;->d:I

    invoke-static {v2, v4}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    iput-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 279
    :cond_0
    iget v2, v3, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    iget v2, v3, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_5

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 280
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-wide v4, v3, Lcom/google/o/f/b;->f:J

    iget-wide v2, v3, Lcom/google/o/f/b;->g:J

    invoke-direct {v0, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    iput-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 287
    :cond_1
    :goto_4
    return-void

    :cond_2
    move v2, v1

    .line 276
    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v2, v1

    .line 279
    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_3

    .line 281
    :cond_6
    iget v2, v3, Lcom/google/o/f/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_7

    :goto_5
    if-eqz v0, :cond_1

    .line 282
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-wide v2, v3, Lcom/google/o/f/b;->e:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(J)V

    iput-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 284
    :catch_0
    move-exception v0

    .line 285
    sget-object v2, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Error parsing geocode parameter:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_7
    move v0, v1

    .line 281
    goto :goto_5
.end method

.method static b(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/af;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/16 v4, 0x29

    const/16 v1, 0x28

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 335
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 361
    :cond_1
    :goto_1
    return-object v2

    :cond_2
    move v0, v3

    .line 335
    goto :goto_0

    .line 341
    :cond_3
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_5

    .line 342
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_5

    .line 343
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-ltz v0, :cond_8

    if-ltz v1, :cond_8

    if-ge v0, v1, :cond_8

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    if-nez v4, :cond_4

    move-object v0, v2

    .line 344
    :goto_3
    if-eqz v0, :cond_5

    move-object v2, v0

    .line 345
    goto :goto_1

    .line 343
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/l/af;

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/l/af;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)V

    goto :goto_3

    .line 351
    :cond_5
    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 352
    const/4 v0, -0x1

    if-eq v1, v0, :cond_7

    .line 354
    if-nez v1, :cond_6

    move-object v0, v2

    .line 355
    :goto_4
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 357
    :goto_5
    invoke-static {p0}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    .line 358
    if-eqz v1, :cond_1

    .line 361
    new-instance v2, Lcom/google/android/apps/gmm/l/af;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/l/af;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)V

    goto :goto_1

    .line 354
    :cond_6
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_7
    move-object v0, v2

    goto :goto_5

    :cond_8
    move-object v0, p0

    move-object v1, v2

    goto :goto_2
.end method

.method static b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 218
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method static b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 451
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 452
    if-nez v0, :cond_0

    .line 453
    const/4 v0, 0x0

    .line 455
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static c(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ag;
    .locals 2

    .prologue
    .line 307
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 308
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 309
    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->c:Lcom/google/android/apps/gmm/l/ag;

    .line 318
    :goto_1
    return-object v0

    .line 308
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 311
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 312
    const-string v1, "saddr"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 313
    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->a:Lcom/google/android/apps/gmm/l/ag;

    goto :goto_1

    .line 315
    :cond_3
    const-string v1, "daddr"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 316
    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->b:Lcom/google/android/apps/gmm/l/ag;

    goto :goto_1

    .line 318
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->c:Lcom/google/android/apps/gmm/l/ag;

    goto :goto_1
.end method

.method static c(Ljava/lang/String;)Lcom/google/r/b/a/a/s;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 681
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    invoke-static {}, Lcom/google/r/b/a/a/s;->d()Lcom/google/r/b/a/a/s;

    move-result-object v0

    .line 694
    :goto_0
    return-object v0

    .line 686
    :cond_0
    const/16 v0, 0xb

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 688
    invoke-static {v0}, Lcom/google/r/b/a/a/s;->a([B)Lcom/google/r/b/a/a/s;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 689
    :catch_0
    move-exception v0

    .line 690
    sget-object v1, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x34

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "IllegalArgumentException while parsing gmm options: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    invoke-static {}, Lcom/google/r/b/a/a/s;->d()Lcom/google/r/b/a/a/s;

    move-result-object v0

    goto :goto_0

    .line 692
    :catch_1
    move-exception v0

    .line 693
    sget-object v1, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "IllegalProtocolBufferException while parsing gmm options: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    invoke-static {}, Lcom/google/r/b/a/a/s;->d()Lcom/google/r/b/a/a/s;

    move-result-object v0

    goto :goto_0
.end method

.method static d(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ad;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 406
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 407
    if-nez v1, :cond_0

    move-object v0, v2

    .line 438
    :goto_0
    return-object v0

    .line 412
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v5, v4

    move v3, v0

    move-object v1, v2

    :goto_1
    if-ge v3, v5, :cond_1

    aget-char v6, v4, v3

    .line 413
    sparse-switch v6, :sswitch_data_0

    .line 430
    sget-object v6, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    .line 412
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 415
    :sswitch_0
    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    goto :goto_2

    .line 418
    :sswitch_1
    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    goto :goto_2

    .line 421
    :sswitch_2
    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    goto :goto_2

    .line 424
    :sswitch_3
    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    goto :goto_2

    .line 427
    :sswitch_4
    const/4 v0, 0x1

    .line 428
    goto :goto_2

    .line 434
    :cond_1
    if-nez v1, :cond_2

    .line 435
    sget-object v0, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    move-object v0, v2

    .line 436
    goto :goto_0

    .line 438
    :cond_2
    new-instance v2, Lcom/google/android/apps/gmm/l/ad;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/l/ad;-><init>(Lcom/google/maps/g/a/hm;Z)V

    move-object v0, v2

    goto :goto_0

    .line 413
    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_1
        0x64 -> :sswitch_0
        0x69 -> :sswitch_4
        0x72 -> :sswitch_2
        0x77 -> :sswitch_3
    .end sparse-switch
.end method

.method static e(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 474
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 475
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    const-string v1, ","

    .line 476
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 489
    :cond_1
    :goto_1
    return-object v0

    .line 475
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 481
    :cond_3
    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 482
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    .line 485
    :try_start_0
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 486
    :catch_0
    move-exception v1

    .line 487
    sget-object v2, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "IllegalArgumentException in getQueryParameterFeatureId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static f(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 516
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 517
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v1}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    goto :goto_1
.end method

.method public static g(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/streetview/b/a;
    .locals 9

    .prologue
    const/high16 v8, 0x42b40000    # 90.0f

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 532
    const-string v3, ","

    .line 533
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const/4 v3, 0x0

    move-object v5, v3

    .line 534
    :goto_0
    if-eqz v5, :cond_0

    array-length v3, v5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    .line 536
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>()V

    .line 555
    :goto_1
    return-object v0

    .line 533
    :cond_1
    invoke-static {v3}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, -0x1

    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    goto :goto_0

    .line 540
    :cond_2
    const/4 v3, 0x1

    :try_start_0
    aget-object v3, v5, v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    :cond_3
    move v3, v2

    :goto_2
    if-eqz v3, :cond_8

    move v4, v0

    .line 543
    :goto_3
    const/4 v3, 0x4

    aget-object v3, v5, v3

    .line 544
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_9

    :cond_4
    move v3, v2

    :goto_4
    if-eqz v3, :cond_a

    move v3, v0

    .line 545
    :goto_5
    const/high16 v6, -0x3d4c0000    # -90.0f

    const/high16 v7, 0x42b40000    # 90.0f

    .line 543
    invoke-static {v6, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v7, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    neg-float v3, v3

    .line 547
    const/4 v6, 0x3

    aget-object v6, v5, v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_6

    :cond_5
    move v1, v2

    :cond_6
    if-eqz v1, :cond_b

    .line 550
    :goto_6
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    float-to-double v0, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->pow(DD)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    double-to-float v0, v0

    div-float v1, v8, v0

    .line 555
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0, v4, v3, v1}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>(FFF)V

    goto :goto_1

    :cond_7
    move v3, v1

    .line 540
    goto :goto_2

    :cond_8
    const/4 v3, 0x1

    :try_start_1
    aget-object v3, v5, v3

    .line 541
    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    move v4, v3

    goto :goto_3

    :cond_9
    move v3, v1

    .line 544
    goto :goto_4

    :cond_a
    const/4 v3, 0x4

    aget-object v3, v5, v3

    .line 545
    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto :goto_5

    .line 547
    :cond_b
    const/4 v0, 0x3

    aget-object v0, v5, v0

    .line 548
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_6

    .line 551
    :catch_0
    move-exception v0

    .line 552
    sget-object v1, Lcom/google/android/apps/gmm/l/ac;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x37

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "NumberFormatException while parsing street view state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>()V

    goto/16 :goto_1
.end method

.method static h(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 659
    invoke-virtual {p0, p1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/l/ae;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;

    move-result-object v0

    return-object v0
.end method
