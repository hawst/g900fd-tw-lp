.class public Lcom/google/android/apps/gmm/search/d/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/e/d;


# instance fields
.field a:Ljava/lang/Boolean;

.field private final b:Lcom/google/android/apps/gmm/search/aq;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/apps/gmm/search/al;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/search/aq;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->a:Ljava/lang/Boolean;

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/d/i;->b:Lcom/google/android/apps/gmm/search/aq;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->c:Ljava/lang/String;

    .line 47
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 104
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/d/i;->d:Lcom/google/android/apps/gmm/search/al;

    .line 106
    if-nez p1, :cond_1

    .line 107
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->a:Ljava/lang/Boolean;

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 112
    if-eqz v1, :cond_0

    .line 113
    iget-object v2, v1, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    .line 114
    iget-object v1, v1, Lcom/google/android/apps/gmm/search/ap;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/d/i;->c:Ljava/lang/String;

    .line 115
    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/i;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->a:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->d:Lcom/google/android/apps/gmm/search/al;

    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-object v5

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/d/i;->d:Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    .line 79
    :try_start_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    new-instance v1, Lcom/google/e/a/a/a/b;

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v0, v0

    const/4 v3, 0x1

    new-instance v4, Lcom/google/e/a/a/a/c;

    invoke-direct {v4}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    const/4 v2, 0x7

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/search/ai;-><init>(Lcom/google/e/a/a/a/b;)V

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/d/i;->b:Lcom/google/android/apps/gmm/search/aq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/d/i;->d:Lcom/google/android/apps/gmm/search/al;

    .line 89
    iget-object v2, v2, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    .line 88
    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    const-string v1, "SpellingCorrectionItemViewModel"

    const-string v2, "Exception while cloning TactileSearchRequestProto."

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 60
    const-string v0, ""

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lcom/google/b/f/t;->gq:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/d;->b(I)Ljava/lang/Integer;

    move-result-object v0

    .line 55
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/d;->ao:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0
.end method
