.class public Lcom/google/android/apps/gmm/base/b/l;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/b/j;


# instance fields
.field private a:Landroid/view/View;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/b/l;-><init>(Landroid/view/View;Z)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/b/l;->a:Landroid/view/View;

    .line 27
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/base/b/l;->b:Z

    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/b/l;->a()V

    .line 29
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/l;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 85
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/b/l;->c:Z

    if-eq v0, v1, :cond_0

    .line 86
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/b/l;->c:Z

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/l;->notifyDataSetChanged()V

    .line 89
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/b/l;->c:Z

    .line 90
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)I
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/b/l;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/b/l;->a()V

    .line 34
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/b/l;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/l;->a:Landroid/view/View;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 49
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 39
    const/4 v0, -0x1

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/l;->a:Landroid/view/View;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method
