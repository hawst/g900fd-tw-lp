.class public final enum Lcom/google/android/apps/gmm/iamhere/d/at;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/d/at;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/d/at;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/d/at;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/d/at;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/iamhere/d/at;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/at;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/d/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/at;->a:Lcom/google/android/apps/gmm/iamhere/d/at;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/at;

    const-string v1, "ACTION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/d/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/at;->b:Lcom/google/android/apps/gmm/iamhere/d/at;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/at;

    const-string v1, "DISAMBIGUATION"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/iamhere/d/at;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/at;->c:Lcom/google/android/apps/gmm/iamhere/d/at;

    .line 43
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/d/at;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/at;->a:Lcom/google/android/apps/gmm/iamhere/d/at;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/at;->b:Lcom/google/android/apps/gmm/iamhere/d/at;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/at;->c:Lcom/google/android/apps/gmm/iamhere/d/at;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/at;->d:[Lcom/google/android/apps/gmm/iamhere/d/at;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/d/at;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/at;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/d/at;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/d/at;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/at;->d:[Lcom/google/android/apps/gmm/iamhere/d/at;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/d/at;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/d/at;

    return-object v0
.end method
