.class public Lcom/google/android/apps/gmm/car/h/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/h/k;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/h/o;

.field public final b:Lcom/google/android/apps/gmm/car/h/p;

.field public final c:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field final d:[Lcom/google/android/apps/gmm/car/h/c;

.field e:Z

.field public final f:Lcom/google/android/apps/gmm/car/h/q;

.field public final g:Ljava/lang/Object;

.field private final h:Z

.field private final i:Lcom/google/android/apps/gmm/car/k/h;

.field private final j:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/h/o;Lcom/google/android/apps/gmm/car/h/p;ZLcom/google/android/apps/gmm/car/k/h;ZLcom/google/android/apps/gmm/map/util/b/a/a;ZLcom/google/android/apps/gmm/shared/c/c/c;Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/l;->e:Z

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/car/h/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/m;-><init>(Lcom/google/android/apps/gmm/car/h/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->f:Lcom/google/android/apps/gmm/car/h/q;

    .line 120
    new-instance v0, Lcom/google/android/apps/gmm/car/h/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/n;-><init>(Lcom/google/android/apps/gmm/car/h/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->g:Ljava/lang/Object;

    .line 51
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/h/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->a:Lcom/google/android/apps/gmm/car/h/o;

    .line 52
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/car/h/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->b:Lcom/google/android/apps/gmm/car/h/p;

    .line 53
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/car/h/l;->h:Z

    .line 54
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/car/k/h;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/h/l;->i:Lcom/google/android/apps/gmm/car/k/h;

    .line 55
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/car/h/l;->j:Z

    .line 56
    if-nez p6, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move-object v0, p6

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->c:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/car/h/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->d:[Lcom/google/android/apps/gmm/car/h/c;

    .line 59
    const/4 v2, 0x0

    :goto_0
    const/4 v0, 0x3

    if-ge v2, v0, :cond_4

    .line 60
    iget-object v8, p0, Lcom/google/android/apps/gmm/car/h/l;->d:[Lcom/google/android/apps/gmm/car/h/c;

    new-instance v0, Lcom/google/android/apps/gmm/car/h/c;

    move-object v1, p2

    move-object v3, p1

    move v4, p5

    move/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/car/h/c;-><init>(Lcom/google/android/apps/gmm/car/h/p;ILcom/google/android/apps/gmm/car/h/o;ZZLcom/google/android/apps/gmm/shared/c/c/c;Landroid/content/Context;)V

    aput-object v0, v8, v2

    .line 59
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->f:Lcom/google/android/apps/gmm/car/h/q;

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/car/h/p;->a(Lcom/google/android/apps/gmm/car/h/q;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->g:Ljava/lang/Object;

    invoke-interface {p6, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 65
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/car/h/b;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->d:[Lcom/google/android/apps/gmm/car/h/c;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/l;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/l;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/l;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->a:Lcom/google/android/apps/gmm/car/h/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/h/o;->a()V

    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/l;->e:Z

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->a:Lcom/google/android/apps/gmm/car/h/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/h/o;->c()V

    .line 100
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/l;->i:Lcom/google/android/apps/gmm/car/k/h;

    sget v1, Lcom/google/android/apps/gmm/l;->bk:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/k/h;->a(I)V

    goto :goto_0
.end method
