.class Lcom/google/android/apps/gmm/reportmapissue/d/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/n;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/reportmapissue/d/h;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/d/h;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->lh:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 318
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->s:Z

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->c:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 319
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->d:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 320
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->n:Lcom/google/android/apps/gmm/reportmapissue/d/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/f;->a:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 321
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->e:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 322
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->e:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_5

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 323
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->f:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 324
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->f:Lcom/google/android/apps/gmm/reportmapissue/d/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_1
    move v2, v0

    :goto_2
    if-nez v2, :cond_7

    move v2, v0

    :goto_3
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 325
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->g:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v2, v0

    :goto_4
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 326
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->k:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v0

    :goto_5
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 327
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->j:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v2, v0

    :goto_6
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 328
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->i:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v2, v0

    :goto_7
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 329
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->h:Lcom/google/android/apps/gmm/reportmapissue/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v2, v0

    :goto_8
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 330
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->l:Lcom/google/android/apps/gmm/reportmapissue/d/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/b;->b:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v2, v0

    :goto_9
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 331
    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->q:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/c;->n:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_e

    :cond_2
    move v2, v0

    :goto_a
    if-nez v2, :cond_f

    .line 318
    :cond_3
    :goto_b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_4
    move v2, v1

    .line 322
    goto/16 :goto_0

    :cond_5
    move v2, v1

    goto/16 :goto_1

    :cond_6
    move v2, v1

    .line 324
    goto/16 :goto_2

    :cond_7
    move v2, v1

    goto/16 :goto_3

    :cond_8
    move v2, v1

    .line 325
    goto/16 :goto_4

    :cond_9
    move v2, v1

    .line 326
    goto :goto_5

    :cond_a
    move v2, v1

    .line 327
    goto :goto_6

    :cond_b
    move v2, v1

    .line 328
    goto :goto_7

    :cond_c
    move v2, v1

    .line 329
    goto :goto_8

    :cond_d
    move v2, v1

    .line 330
    goto :goto_9

    :cond_e
    move v2, v1

    .line 331
    goto :goto_a

    :cond_f
    move v0, v1

    goto :goto_b
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 342
    :goto_0
    return-object v1

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356
    :goto_0
    return-object v7

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->s:Z

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->s:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->t:Lcom/google/android/apps/gmm/base/l/a/n;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->p:Lcom/google/android/apps/gmm/reportmapissue/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/d/h;->q:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 353
    iget-object v3, v3, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 354
    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/d/h;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 355
    iget-object v5, v5, Lcom/google/android/apps/gmm/reportmapissue/d/h;->a:Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    iget-object v6, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 352
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Lcom/google/android/apps/gmm/reportmapissue/a/c;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/reportmapissue/h;Z)V

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->s:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/i;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->s:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    return-object v0
.end method
