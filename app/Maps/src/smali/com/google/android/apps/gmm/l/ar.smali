.class public Lcom/google/android/apps/gmm/l/ar;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/base/activities/c;

.field public final c:Lcom/google/android/apps/gmm/l/an;

.field public final d:Landroid/os/PowerManager$WakeLock;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/l/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/ar;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 4

    .prologue
    .line 48
    new-instance v1, Lcom/google/android/apps/gmm/l/an;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/l/an;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "power"

    .line 50
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v2, 0x30000006

    sget-object v3, Lcom/google/android/apps/gmm/l/ar;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 48
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/gmm/l/ar;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/an;Landroid/os/PowerManager$WakeLock;)V

    .line 53
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/an;Landroid/os/PowerManager$WakeLock;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/gmm/l/ar;->c:Lcom/google/android/apps/gmm/l/an;

    .line 60
    iput-object p3, p0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 64
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/ae;->e:Z

    if-eqz v2, :cond_3

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 173
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/b/f;->a()Z

    move-result v2

    .line 174
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/h/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/l/ar;->c:Lcom/google/android/apps/gmm/l/an;

    iget v2, v3, Lcom/google/android/apps/gmm/l/an;->c:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_5

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    iget v2, v3, Lcom/google/android/apps/gmm/l/an;->c:F

    const/high16 v4, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_6

    iget v2, v3, Lcom/google/android/apps/gmm/l/an;->c:F

    iget v3, v3, Lcom/google/android/apps/gmm/l/an;->e:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_6

    :cond_0
    :goto_1
    if-eqz v0, :cond_2

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v2, 0x680080

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->e:Z

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ar;->c:Lcom/google/android/apps/gmm/l/an;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/l/an;->d:Z

    if-eqz v2, :cond_4

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/l/an;->d:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/an;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 181
    :cond_4
    return-void

    :cond_5
    move v2, v1

    .line 174
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method
