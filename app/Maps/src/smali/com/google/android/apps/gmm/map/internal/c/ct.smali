.class public Lcom/google/android/apps/gmm/map/internal/c/ct;
.super Lcom/google/android/apps/gmm/map/internal/c/bt;
.source "PG"


# instance fields
.field final i:I

.field private final j:B


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJIBII)V
    .locals 13

    .prologue
    .line 44
    const/4 v10, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move/from16 v11, p10

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZI)V

    .line 46
    move/from16 v0, p8

    iput-byte v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->j:B

    .line 47
    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->i:I

    .line 48
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;[BIIJJI)V
    .locals 10

    .prologue
    .line 69
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    move/from16 v9, p10

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZI)V

    .line 72
    invoke-static {p2, p3, p4, p5}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BII)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v0

    .line 74
    iget-byte v1, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->f:B

    iput-byte v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->j:B

    .line 75
    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->a:I

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->i:I

    .line 76
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;[BIJJIII)V
    .locals 13

    .prologue
    .line 82
    const/4 v10, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move/from16 v11, p11

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJZI)V

    .line 84
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/cr;

    move/from16 v0, p9

    move/from16 v1, p10

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cr;-><init>(II)V

    .line 85
    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/b/a/ai;[BILcom/google/android/apps/gmm/map/internal/c/cr;)Lcom/google/android/apps/gmm/map/internal/c/cs;

    move-result-object v2

    .line 87
    iget-byte v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cs;->f:B

    iput-byte v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->j:B

    .line 88
    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cs;->a:I

    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->i:I

    .line 89
    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/cs;->b:I

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    .line 90
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/net/ad;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 104
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    .line 105
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget-boolean v2, v2, Lcom/google/r/b/a/aqg;->i:Z

    if-eqz v2, :cond_4

    .line 107
    iget-byte v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->j:B

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-byte v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->j:B

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 111
    :cond_1
    :goto_2
    return v0

    :cond_2
    move v2, v0

    .line 107
    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    .line 111
    :cond_4
    iget-byte v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ct;->j:B

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_5

    move v2, v1

    :goto_3
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3
.end method
