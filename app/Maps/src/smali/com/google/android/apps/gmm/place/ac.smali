.class public Lcom/google/android/apps/gmm/place/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/an;
.implements Lcom/google/android/apps/gmm/place/review/b;


# instance fields
.field public a:Lcom/google/android/apps/gmm/search/views/b;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Landroid/app/Fragment;

.field private final d:Lcom/google/android/apps/gmm/z/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/app/Fragment;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/ac;->c:Landroid/app/Fragment;

    .line 71
    new-instance v1, Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/a;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->d:Lcom/google/android/apps/gmm/z/a;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->d:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->a()V

    .line 73
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 5

    .prologue
    .line 147
    sget-object v1, Lcom/google/r/b/a/ru;->b:Lcom/google/r/b/a/ru;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v3, 0x0

    invoke-static {v1, p2}, Lcom/google/android/apps/gmm/z/d;->a(Lcom/google/r/b/a/ru;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/z/d;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 150
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    const-string v3, "tel: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 156
    return-void

    .line 150
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 346
    invoke-static {v0}, Lcom/google/android/apps/gmm/util/q;->f(Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;

    move-result-object v0

    .line 347
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    .line 348
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 347
    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 350
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->d:Lcom/google/android/apps/gmm/z/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a;Landroid/view/View;)Lcom/google/android/apps/gmm/z/b/l;

    .line 355
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->c:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 114
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->n()Ljava/lang/String;

    move-result-object v3

    .line 115
    const-string v4, "call clicked, [%s]"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v3, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 116
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    if-nez v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v4, Lcom/google/b/f/t;->dc:Lcom/google/b/f/t;

    invoke-interface {v1, v0, v2, v4}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 120
    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/gmm/place/ac;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;F)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->b(Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/place/review/j;

    move-result-object v1

    .line 258
    if-eqz p1, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v3, "placemarkref"

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    iput-object p1, v1, Lcom/google/android/apps/gmm/place/review/j;->c:Lcom/google/android/apps/gmm/x/o;

    .line 259
    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v2, "fivestarrating"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 260
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/review/j;->a()V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->cZ:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    .line 264
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v2, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v3, Lcom/google/b/f/t;->cZ:Lcom/google/b/f/t;

    .line 263
    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 266
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    if-eqz p3, :cond_2

    .line 299
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 300
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    .line 301
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez p3, :cond_1

    const-string v0, ""

    :goto_0
    iput-object v0, v2, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    const/4 v0, 0x0

    .line 302
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 303
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 304
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v1

    invoke-interface {v1, v0, v3, v6, v6}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 320
    :cond_0
    :goto_1
    return-void

    .line 301
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 313
    :cond_2
    sget-object v0, Lcom/google/b/f/t;->ds:Lcom/google/b/f/t;

    .line 314
    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v2, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/aq;

    new-instance v2, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 316
    if-eqz v3, :cond_3

    const-string v4, "\\s+"

    const-string v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    :cond_3
    iput-object v0, v2, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 315
    invoke-virtual {v1, v2, v6}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Review "

    if-eqz p1, :cond_0

    const-string v0, "deleted"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 272
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 273
    return-void

    .line 270
    :cond_0
    const-string v0, "deletion failure"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/x/o;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->c:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 130
    iget-object v6, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    .line 131
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/a;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v6, Lcom/google/android/apps/gmm/base/g/a;->s:Ljava/lang/String;

    move-object v5, v4

    .line 132
    :goto_1
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/a;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, v6, Lcom/google/android/apps/gmm/base/g/a;->r:Ljava/lang/String;

    move-object v4, v3

    .line 133
    :goto_2
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_9

    :cond_2
    move v3, v2

    :goto_3
    if-nez v3, :cond_0

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    move v1, v2

    :cond_4
    if-nez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v3, Lcom/google/b/f/t;->cX:Lcom/google/b/f/t;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 139
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/gmm/place/ac;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    goto :goto_0

    .line 131
    :cond_5
    iget-object v4, v6, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    move-object v5, v4

    goto :goto_1

    :cond_6
    move-object v5, v3

    goto :goto_1

    .line 132
    :cond_7
    iget-object v3, v6, Lcom/google/android/apps/gmm/base/g/a;->n:Ljava/lang/String;

    move-object v4, v3

    goto :goto_2

    :cond_8
    move-object v4, v3

    goto :goto_2

    :cond_9
    move v3, v1

    .line 133
    goto :goto_3
.end method

.method public final c(Lcom/google/android/apps/gmm/x/o;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->c:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v1, "save clicked, [%s]"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/j/d/a/w;->g:Lcom/google/j/d/a/w;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->x()Lcom/google/android/apps/gmm/myplaces/a/e;

    move-result-object v0

    .line 169
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/myplaces/a/e;->a(Lcom/google/android/apps/gmm/x/o;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/apps/gmm/x/o;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->c:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 182
    const-string v1, "share clicked, [%s]"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    .line 186
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    new-array v1, v6, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->dy:Lcom/google/b/f/t;

    aput-object v5, v1, v7

    .line 187
    iput-object v1, v4, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 188
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 185
    invoke-interface {v3, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    sget-object v3, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v4, Lcom/google/b/f/t;->dy:Lcom/google/b/f/t;

    invoke-interface {v1, v0, v3, v4}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->I()Lcom/google/android/apps/gmm/share/a/b;

    move-result-object v1

    .line 198
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/g/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-array v4, v6, [Lcom/google/android/apps/gmm/share/a/a;

    new-instance v5, Lcom/google/android/apps/gmm/base/placelists/n;

    sget-object v6, Lcom/google/r/b/a/ru;->c:Lcom/google/r/b/a/ru;

    invoke-direct {v5, v6, p1}, Lcom/google/android/apps/gmm/base/placelists/n;-><init>(Lcom/google/r/b/a/ru;Lcom/google/android/apps/gmm/x/o;)V

    aput-object v5, v4, v7

    .line 196
    invoke-interface {v1, v3, v2, v0, v4}, Lcom/google/android/apps/gmm/share/a/b;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;[Lcom/google/android/apps/gmm/share/a/a;)V

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 188
    goto :goto_1
.end method

.method public final e(Lcom/google/android/apps/gmm/x/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->c:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    .line 210
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/b/f/cq;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/b/f/t;->dt:Lcom/google/b/f/t;

    aput-object v5, v3, v4

    .line 211
    iput-object v3, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 212
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 209
    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v2

    .line 214
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v3, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v4, Lcom/google/b/f/t;->dt:Lcom/google/b/f/t;

    .line 213
    invoke-interface {v2, v1, v3, v4}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v1

    sget-object v2, Lcom/google/maps/g/hs;->b:Lcom/google/maps/g/hs;

    sget-object v3, Lcom/google/maps/g/hq;->b:Lcom/google/maps/g/hq;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;Lcom/google/maps/g/hq;)V

    goto :goto_0
.end method

.method public final f(Lcom/google/android/apps/gmm/x/o;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 237
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 239
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v5

    new-array v1, v4, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->O:Lcom/google/b/f/t;

    aput-object v6, v1, v3

    .line 240
    iput-object v1, v5, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 241
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_0
    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 242
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v5

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v5

    .line 245
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v6, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v7, Lcom/google/b/f/t;->O:Lcom/google/b/f/t;

    .line 244
    invoke-interface {v5, v1, v6, v7}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 247
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    .line 248
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/a;->j:Ljava/lang/String;

    .line 249
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    move v0, v4

    :goto_1
    if-nez v0, :cond_2

    .line 250
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 253
    :cond_2
    return-void

    :cond_3
    move-object v1, v2

    .line 241
    goto :goto_0

    :cond_4
    move v0, v3

    .line 249
    goto :goto_1
.end method

.method public final g(Lcom/google/android/apps/gmm/x/o;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->a:Lcom/google/android/apps/gmm/search/views/b;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 278
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ac;->a:Lcom/google/android/apps/gmm/search/views/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 279
    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 280
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 281
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    .line 282
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    .line 278
    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/search/views/b;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Landroid/app/FragmentManager;)V

    .line 284
    :cond_0
    return-void
.end method

.method public final h(Lcom/google/android/apps/gmm/x/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 337
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 338
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 339
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v2

    .line 340
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;Ljava/lang/String;)V

    .line 341
    return-void

    .line 336
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0
.end method

.method public final i(Lcom/google/android/apps/gmm/x/o;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 360
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v2

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v8

    .line 361
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v1, Lcom/google/android/apps/gmm/place/ad;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/maps/g/om;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    const-string v3, ""

    .line 362
    :goto_1
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/a/a;

    sget-object v1, Lcom/google/maps/g/hs;->b:Lcom/google/maps/g/hs;

    .line 367
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v5

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/addaplace/a/a;-><init>(Lcom/google/maps/g/hs;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/cc;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->h()Lcom/google/android/apps/gmm/addaplace/a/b;

    move-result-object v1

    invoke-interface {v1, v0, v8}, Lcom/google/android/apps/gmm/addaplace/a/b;->a(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;)V

    .line 371
    return-void

    .line 360
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0

    .line 361
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ag()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ab()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ah()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final j(Lcom/google/android/apps/gmm/x/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v2, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->dA:Lcom/google/b/f/t;

    aput-object v5, v4, v1

    .line 376
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 377
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    .line 375
    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v3

    .line 379
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v4, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v5, Lcom/google/b/f/t;->dA:Lcom/google/b/f/t;

    .line 378
    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 381
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ai()V

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->v:Ljava/lang/String;

    .line 385
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    invoke-static {v3}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 386
    invoke-static {v3}, Landroid/webkit/URLUtil;->isHttpUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3}, Landroid/webkit/URLUtil;->isHttpsUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 387
    :cond_1
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 388
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ac;->b:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 394
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 385
    goto :goto_0

    .line 389
    :cond_3
    if-nez v3, :cond_4

    .line 390
    const-string v0, "PlacePageViewListenerImpl"

    const-string v2, "Invalid URL for a third party note: null"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 392
    :cond_4
    const-string v0, "PlacePageViewListenerImpl"

    const-string v4, "Invalid URL for a third party note: \"%s\""

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    invoke-static {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
