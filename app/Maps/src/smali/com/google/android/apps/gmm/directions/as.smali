.class Lcom/google/android/apps/gmm/directions/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V
    .locals 0

    .prologue
    .line 917
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 4

    .prologue
    .line 928
    .line 929
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/se;

    .line 930
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->s()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v0

    .line 931
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->p()Z

    move-result v1

    if-nez v1, :cond_0

    .line 932
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 933
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 934
    sget-object v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    const-string v2, "Map Point Picker is opened while DSPF is background, but waypoint is not selected"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 938
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    sget-object v3, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 941
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/suggest/e/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    sget v3, Lcom/google/android/apps/gmm/l;->fj:I

    .line 942
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 939
    :goto_0
    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;->a(Landroid/app/Fragment;Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/MapPointPickerFragment;

    move-result-object v0

    .line 938
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 944
    return-void

    .line 942
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/as;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    sget v3, Lcom/google/android/apps/gmm/l;->fi:I

    .line 943
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 924
    sget-object v0, Lcom/google/o/h/a/g;->x:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 925
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 920
    iget-object v0, p1, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/se;

    iget-boolean v0, v0, Lcom/google/o/h/a/se;->c:Z

    return v0
.end method
