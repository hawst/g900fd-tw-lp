.class public Lcom/google/android/apps/gmm/l/u;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final i:Lcom/google/android/apps/gmm/l/u;


# instance fields
.field private final A:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final B:[B
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final C:Z

.field private final D:Lcom/google/r/b/a/a/s;

.field public final a:Lcom/google/android/apps/gmm/l/y;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/google/android/apps/gmm/map/r/a/ap;

.field public final d:Lcom/google/android/apps/gmm/l/aa;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/j;

.field final f:Lcom/google/android/apps/gmm/l/ae;

.field public final g:Lcom/google/android/apps/gmm/aa/a/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final h:Z

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final n:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final o:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final p:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final q:Ljava/lang/Float;

.field private final r:Lcom/google/android/apps/gmm/map/r/a/ap;

.field private final s:Lcom/google/android/apps/gmm/l/ag;

.field private final t:Lcom/google/android/apps/gmm/l/ad;

.field private final u:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final v:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final w:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final x:Ljava/lang/String;

.field private final y:Lcom/google/android/apps/gmm/streetview/b/a;

.field private final z:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 30

    .prologue
    .line 259
    new-instance v0, Lcom/google/android/apps/gmm/l/u;

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->i:Lcom/google/android/apps/gmm/l/y;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 261
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    sget-object v20, Lcom/google/android/apps/gmm/l/ag;->c:Lcom/google/android/apps/gmm/l/ag;

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    invoke-direct/range {v0 .. v29}, Lcom/google/android/apps/gmm/l/u;-><init>(Lcom/google/android/apps/gmm/l/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/Float;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/ad;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/gmm/l/aa;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Lcom/google/android/apps/gmm/l/ag;Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/l/ae;Ljava/lang/String;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/aa/a/a/a;Z[BZLcom/google/r/b/a/a/s;)V

    sput-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    .line 259
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/l/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/Float;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/ad;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/gmm/l/aa;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Lcom/google/android/apps/gmm/l/ag;Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/l/ae;Ljava/lang/String;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/aa/a/a/a;Z[BZLcom/google/r/b/a/a/s;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p10    # Lcom/google/android/apps/gmm/l/ad;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p13    # Lcom/google/android/apps/gmm/l/aa;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p14    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p16    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p17    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p18    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p19    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p22    # Lcom/google/android/apps/gmm/l/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p23    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p24    # Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p25    # Lcom/google/android/apps/gmm/aa/a/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p27    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    iput-object p2, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    .line 280
    iput-object p3, p0, Lcom/google/android/apps/gmm/l/u;->k:Ljava/lang/String;

    .line 281
    iput-object p4, p0, Lcom/google/android/apps/gmm/l/u;->l:Ljava/lang/String;

    .line 282
    iput-object p1, p0, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    .line 283
    iput-object p5, p0, Lcom/google/android/apps/gmm/l/u;->o:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 284
    iput-object p6, p0, Lcom/google/android/apps/gmm/l/u;->q:Ljava/lang/Float;

    .line 285
    iput-object p7, p0, Lcom/google/android/apps/gmm/l/u;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 286
    iput-object p8, p0, Lcom/google/android/apps/gmm/l/u;->r:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 287
    iput-object p9, p0, Lcom/google/android/apps/gmm/l/u;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 288
    iput-object p10, p0, Lcom/google/android/apps/gmm/l/u;->t:Lcom/google/android/apps/gmm/l/ad;

    .line 289
    iput-object p11, p0, Lcom/google/android/apps/gmm/l/u;->u:Ljava/lang/Boolean;

    .line 290
    iput-object p12, p0, Lcom/google/android/apps/gmm/l/u;->v:Ljava/lang/Boolean;

    .line 291
    iput-object p13, p0, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    .line 292
    iput-object p14, p0, Lcom/google/android/apps/gmm/l/u;->j:Ljava/lang/String;

    .line 293
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 294
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->n:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 295
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->s:Lcom/google/android/apps/gmm/l/ag;

    .line 296
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 297
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->w:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 298
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->x:Ljava/lang/String;

    .line 299
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->y:Lcom/google/android/apps/gmm/streetview/b/a;

    .line 300
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->f:Lcom/google/android/apps/gmm/l/ae;

    .line 301
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->z:Ljava/lang/String;

    .line 302
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->A:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 303
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 304
    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/l/u;->h:Z

    .line 305
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->B:[B

    .line 306
    move/from16 v0, p28

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/l/u;->C:Z

    .line 307
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    .line 308
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->o:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->o:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 918
    :goto_0
    if-eqz v0, :cond_2

    .line 928
    :goto_1
    return-object v0

    .line 917
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 922
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    .line 923
    invoke-interface {p2}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v1

    .line 924
    if-eqz v1, :cond_3

    .line 925
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_1

    .line 927
    :cond_3
    iget-object v0, v0, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    .line 928
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/maps/a/e;->c:D

    iget-wide v4, v0, Lcom/google/maps/a/e;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v0, v1

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 7

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->o:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->o:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 578
    :goto_0
    if-nez v0, :cond_2

    .line 598
    :goto_1
    return-void

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 581
    :cond_2
    iget-object v6, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 582
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 583
    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 584
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 585
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    .line 584
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/l/u;->b(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 586
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 588
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v2

    .line 589
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/l/v;

    invoke-direct {v3, p0, v6, v0, v2}, Lcom/google/android/apps/gmm/l/v;-><init>(Lcom/google/android/apps/gmm/l/u;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/mylocation/b/f;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v3, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_1
.end method

.method private b(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)D
    .locals 6

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object v4, v0

    .line 939
    :goto_0
    if-eqz v4, :cond_1

    .line 940
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/r;

    .line 941
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-wide v2, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;DD)V

    .line 943
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v1

    iget-object v1, v1, Lcom/google/maps/a/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/m;->d()Lcom/google/maps/a/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/a/m;

    .line 944
    iget v2, v1, Lcom/google/maps/a/m;->c:I

    iget v1, v1, Lcom/google/maps/a/m;->b:I

    .line 945
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v3

    .line 944
    invoke-static {v0, v2, v1, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/r;IID)D

    move-result-wide v0

    .line 950
    :goto_1
    return-wide v0

    .line 938
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->n:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object v4, v0

    goto :goto_0

    .line 947
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->q:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 948
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->q:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    goto :goto_1

    .line 950
    :cond_2
    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    goto :goto_1
.end method

.method public static b()Lcom/google/android/apps/gmm/l/z;
    .locals 1

    .prologue
    .line 1029
    new-instance v0, Lcom/google/android/apps/gmm/l/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/l/z;-><init>()V

    return-object v0
.end method

.method private b(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 784
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 788
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 789
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v4, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    .line 791
    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 792
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 793
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v4

    .line 795
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v5

    const/4 v6, 0x0

    .line 800
    iget-object v7, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v7, :cond_7

    iget v0, v7, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/a;->d()Lcom/google/maps/g/g/a;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/a;

    iget v0, v0, Lcom/google/maps/g/g/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/a;->d()Lcom/google/maps/g/g/a;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/a;

    iget-object v0, v0, Lcom/google/maps/g/g/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/d;->d()Lcom/google/maps/g/g/d;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/d;

    iget v0, v0, Lcom/google/maps/g/g/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/a;->d()Lcom/google/maps/g/g/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/a;

    iget-object v0, v0, Lcom/google/maps/g/g/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/d;->d()Lcom/google/maps/g/g/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/d;

    iget v0, v0, Lcom/google/maps/g/g/d;->b:I

    invoke-static {v0}, Lcom/google/maps/g/g/g;->a(I)Lcom/google/maps/g/g/g;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/g/g;->a:Lcom/google/maps/g/g/g;

    :cond_0
    sget-object v1, Lcom/google/maps/g/g/g;->a:Lcom/google/maps/g/g/g;

    if-ne v0, v1, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 796
    :goto_4
    invoke-interface {v5, v4, v3, v6, v0}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 801
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/l/u;->C:Z

    if-eqz v0, :cond_1

    .line 804
    :cond_1
    return-void

    .line 790
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 800
    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto :goto_4
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/maps/a/a;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 870
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/a/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/m;->d()Lcom/google/maps/a/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/maps/a/m;

    .line 871
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v8

    .line 873
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/l/u;->b(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)D

    move-result-wide v0

    iget-wide v2, v8, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    .line 876
    iget v6, v7, Lcom/google/maps/a/m;->c:I

    .line 872
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->a(DDDI)D

    move-result-wide v0

    .line 878
    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v2

    .line 879
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v3

    iget-wide v4, v8, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 880
    iget v6, v3, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v3, Lcom/google/maps/a/g;->a:I

    iput-wide v4, v3, Lcom/google/maps/a/g;->c:D

    iget-wide v4, v8, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 881
    iget v6, v3, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v3, Lcom/google/maps/a/g;->a:I

    iput-wide v4, v3, Lcom/google/maps/a/g;->b:D

    .line 882
    iget v4, v3, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v3, Lcom/google/maps/a/g;->a:I

    iput-wide v0, v3, Lcom/google/maps/a/g;->d:D

    .line 879
    iget-object v0, v2, Lcom/google/maps/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v0, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    .line 883
    invoke-static {}, Lcom/google/maps/a/i;->newBuilder()Lcom/google/maps/a/k;

    move-result-object v0

    .line 884
    iget v1, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/k;->a:I

    iput v9, v0, Lcom/google/maps/a/k;->b:F

    .line 885
    iget v1, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/a/k;->a:I

    iput v9, v0, Lcom/google/maps/a/k;->c:F

    .line 886
    iget v1, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/a/k;->a:I

    iput v9, v0, Lcom/google/maps/a/k;->d:F

    .line 883
    iget-object v1, v2, Lcom/google/maps/a/c;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/a/k;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    .line 887
    if-nez v7, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v2, Lcom/google/maps/a/c;->d:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v0, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    const/high16 v0, 0x41f00000    # 30.0f

    .line 888
    iget v1, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v2, Lcom/google/maps/a/c;->a:I

    iput v0, v2, Lcom/google/maps/a/c;->e:F

    .line 889
    invoke-virtual {v2}, Lcom/google/maps/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 312
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    new-instance v1, Lcom/google/b/a/ak;

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 313
    const-string v0, "query"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 314
    const-string v0, "hiddenQuery"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->k:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 315
    const-string v0, "hiddenNear"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->l:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 316
    const-string v0, "actionType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 317
    const-string v0, "sll"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->o:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 318
    const-string v0, "zoom"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->q:Ljava/lang/Float;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 319
    const-string v0, "ll"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->m:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 320
    const-string v0, "startWaypoint"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->r:Lcom/google/android/apps/gmm/map/r/a/ap;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 321
    const-string v0, "destinationWaypoint"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 322
    const-string v0, "directionsFlag"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->t:Lcom/google/android/apps/gmm/l/ad;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 323
    const-string v0, "enableTrafficOverlay"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->u:Ljava/lang/Boolean;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 324
    const-string v0, "enableSatelliteMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->v:Ljava/lang/Boolean;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 325
    const-string v0, "targetMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 326
    const-string v0, "thirdPartyLabel"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->j:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 327
    const-string v0, "placeFeatureId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 328
    const-string v0, "latLngSpan"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->n:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 329
    const-string v0, "myLocationSpec"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->s:Lcom/google/android/apps/gmm/l/ag;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 330
    const-string v0, "searchSpan"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 331
    const-string v0, "streetViewLatLng"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->w:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 332
    const-string v0, "streetViewPanoId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->x:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 333
    const-string v0, "streetViewUserOrientation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->y:Lcom/google/android/apps/gmm/streetview/b/a;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 334
    const-string v0, "entryPoint"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->f:Lcom/google/android/apps/gmm/l/ae;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 335
    const-string v0, "preferredTransitPattern"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->z:Ljava/lang/String;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 336
    const-string v0, "fragment"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->A:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 340
    const-string v0, "playConfirmationTts"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/l/u;->h:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_18
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 341
    const-string v0, "intentExtension"

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 342
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 497
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 498
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    .line 504
    iget-object v0, v0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/a;->d()Lcom/google/maps/g/g/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/g/a;

    iget-boolean v0, v0, Lcom/google/maps/g/g/a;->d:Z

    if-eqz v0, :cond_2

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "setOneBackTapBackground: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iput-boolean v7, p1, Lcom/google/android/apps/gmm/base/activities/c;->n:Z

    .line 509
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Switching on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    sget-object v0, Lcom/google/android/apps/gmm/l/x;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/l/y;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 562
    :goto_1
    :pswitch_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v0

    .line 563
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->u:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 564
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->u:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    .line 566
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->v:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 567
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->v:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->d(Z)Z

    goto :goto_0

    .line 513
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v7

    :goto_2
    if-eqz v0, :cond_6

    const-string v0, "IntentAction"

    const-string v1, "An intent with empty query should not handled as SEARCH or SEARCH_LIST. See isValidIntentAction() for more details."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_2

    :cond_6
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    new-instance v5, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->c:Lcom/google/android/apps/gmm/l/y;

    if-ne v0, v1, :cond_b

    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/h;->b:Lcom/google/android/apps/gmm/base/placelists/a/h;

    :goto_3
    iput-object v0, v5, Lcom/google/android/apps/gmm/base/placelists/a/e;->d:Lcom/google/android/apps/gmm/base/placelists/a/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_c

    :cond_7
    move v0, v7

    :goto_4
    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->j:Ljava/lang/String;

    iput-object v0, v5, Lcom/google/android/apps/gmm/base/placelists/a/e;->c:Ljava/lang/String;

    :cond_8
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    new-instance v8, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v2, "\\s+"

    const-string v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    :cond_9
    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    sget-object v2, Lcom/google/b/f/bc;->b:Lcom/google/b/f/bc;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v1}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hy;

    iput-object v1, v8, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    iget v1, v1, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v11, :cond_d

    move v1, v7

    :goto_5
    if-eqz v1, :cond_13

    invoke-static {}, Lcom/google/r/b/a/alh;->newBuilder()Lcom/google/r/b/a/alm;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    iget-object v1, v1, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/k;->d()Lcom/google/maps/g/g/k;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/g/k;

    new-instance v2, Lcom/google/n/ai;

    iget-object v3, v1, Lcom/google/maps/g/g/k;->b:Ljava/util/List;

    sget-object v4, Lcom/google/maps/g/g/k;->c:Lcom/google/n/aj;

    invoke-direct {v2, v3, v4}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-virtual {v9, v2}, Lcom/google/r/b/a/alm;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;

    invoke-static {}, Lcom/google/maps/g/sp;->newBuilder()Lcom/google/maps/g/sr;

    move-result-object v3

    iget v2, v1, Lcom/google/maps/g/g/k;->d:I

    invoke-static {v2}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v2

    if-nez v2, :cond_a

    sget-object v2, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_a
    if-nez v2, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/h;->a:Lcom/google/android/apps/gmm/base/placelists/a/h;

    goto/16 :goto_3

    :cond_c
    move v0, v6

    goto :goto_4

    :cond_d
    move v1, v6

    goto :goto_5

    :cond_e
    iget v4, v3, Lcom/google/maps/g/sr;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/maps/g/sr;->a:I

    iget v2, v2, Lcom/google/maps/g/qb;->d:I

    iput v2, v3, Lcom/google/maps/g/sr;->b:I

    invoke-virtual {v3}, Lcom/google/maps/g/sr;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/sp;

    invoke-virtual {v9, v2}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/sp;)Lcom/google/r/b/a/alm;

    iget v2, v1, Lcom/google/maps/g/g/k;->g:I

    invoke-static {v2}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v2

    if-nez v2, :cond_f

    sget-object v2, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_f
    invoke-virtual {v9, v2}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/pz;)Lcom/google/r/b/a/alm;

    new-instance v2, Lcom/google/n/ai;

    iget-object v3, v1, Lcom/google/maps/g/g/k;->e:Ljava/util/List;

    sget-object v4, Lcom/google/maps/g/g/k;->f:Lcom/google/n/aj;

    invoke-direct {v2, v3, v4}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    invoke-virtual {v9, v2}, Lcom/google/r/b/a/alm;->b(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;

    iget-object v2, v1, Lcom/google/maps/g/g/k;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/g/p;->d()Lcom/google/maps/g/g/p;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/g/p;

    invoke-static {}, Lcom/google/maps/g/gn;->newBuilder()Lcom/google/maps/g/gp;

    move-result-object v10

    iget-object v3, v2, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v3, Ljava/lang/String;

    :goto_6
    if-nez v3, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    check-cast v3, Lcom/google/n/f;

    invoke-virtual {v3}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/n/f;->e()Z

    move-result v3

    if-eqz v3, :cond_11

    iput-object v4, v2, Lcom/google/maps/g/g/p;->b:Ljava/lang/Object;

    :cond_11
    move-object v3, v4

    goto :goto_6

    :cond_12
    iget v4, v10, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v10, Lcom/google/maps/g/gp;->a:I

    iput-object v3, v10, Lcom/google/maps/g/gp;->b:Ljava/lang/Object;

    iget v2, v2, Lcom/google/maps/g/g/p;->c:I

    iget v3, v10, Lcom/google/maps/g/gp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v10, Lcom/google/maps/g/gp;->a:I

    iput v2, v10, Lcom/google/maps/g/gp;->c:I

    invoke-virtual {v10}, Lcom/google/maps/g/gp;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/gn;

    invoke-virtual {v9, v2}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/gn;)Lcom/google/r/b/a/alm;

    iget-object v1, v1, Lcom/google/maps/g/g/k;->i:Lcom/google/n/aq;

    invoke-virtual {v9, v1}, Lcom/google/r/b/a/alm;->c(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;

    invoke-virtual {v9}, Lcom/google/r/b/a/alm;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/alh;

    iput-object v1, v8, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    :cond_13
    invoke-static {}, Lcom/google/r/b/a/alc;->newBuilder()Lcom/google/r/b/a/ale;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->k:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_15

    :cond_14
    move v1, v7

    :goto_7
    if-nez v1, :cond_17

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->k:Ljava/lang/String;

    if-nez v1, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    move v1, v6

    goto :goto_7

    :cond_16
    iget v3, v2, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/r/b/a/ale;->a:I

    iput-object v1, v2, Lcom/google/r/b/a/ale;->b:Ljava/lang/Object;

    :cond_17
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->l:Ljava/lang/String;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1b

    :cond_18
    move v1, v7

    :goto_8
    if-nez v1, :cond_1a

    iget-object v3, p0, Lcom/google/android/apps/gmm/l/u;->l:Ljava/lang/String;

    if-eqz v3, :cond_19

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1c

    :cond_19
    move v1, v7

    :goto_9
    if-eqz v1, :cond_1d

    :goto_a
    if-eqz v6, :cond_1a

    invoke-virtual {v2}, Lcom/google/r/b/a/ale;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/alc;

    iput-object v1, v8, Lcom/google/android/apps/gmm/search/aj;->s:Lcom/google/r/b/a/alc;

    :cond_1a
    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;)V

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/maps/a/a;

    move-result-object v1

    iput-object v1, v8, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto/16 :goto_1

    :cond_1b
    move v1, v6

    goto :goto_8

    :cond_1c
    move v1, v6

    goto :goto_9

    :cond_1d
    const/16 v1, 0x2c

    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_21

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    if-eqz v1, :cond_1f

    if-nez v3, :cond_1e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1e
    iget v1, v2, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v2, Lcom/google/r/b/a/ale;->a:I

    iput-object v3, v2, Lcom/google/r/b/a/ale;->d:Ljava/lang/Object;

    :goto_b
    move v6, v7

    goto :goto_a

    :cond_1f
    if-nez v3, :cond_20

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_20
    iget v1, v2, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v2, Lcom/google/r/b/a/ale;->a:I

    iput-object v3, v2, Lcom/google/r/b/a/ale;->c:Ljava/lang/Object;

    goto :goto_b

    :cond_21
    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v6

    if-eqz v6, :cond_24

    if-nez v4, :cond_22

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_22
    iget v6, v2, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, v2, Lcom/google/r/b/a/ale;->a:I

    iput-object v4, v2, Lcom/google/r/b/a/ale;->d:Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_23

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_23
    iget v3, v2, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/google/r/b/a/ale;->a:I

    iput-object v1, v2, Lcom/google/r/b/a/ale;->c:Ljava/lang/Object;

    goto :goto_b

    :cond_24
    if-nez v3, :cond_25

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_25
    iget v1, v2, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v2, Lcom/google/r/b/a/ale;->a:I

    iput-object v3, v2, Lcom/google/r/b/a/ale;->c:Ljava/lang/Object;

    goto :goto_b

    .line 517
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->s:Lcom/google/android/apps/gmm/l/ag;

    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->a:Lcom/google/android/apps/gmm/l/ag;

    if-eq v1, v0, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->r:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    sget-object v2, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    if-ne v0, v2, :cond_2a

    move v0, v7

    :goto_c
    if-eqz v0, :cond_2b

    :cond_26
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    :goto_d
    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->b:Lcom/google/android/apps/gmm/l/ag;

    if-ne v1, v0, :cond_2c

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    :goto_e
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    if-nez v1, :cond_2d

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    :goto_f
    iget-object v4, p0, Lcom/google/android/apps/gmm/l/u;->D:Lcom/google/r/b/a/a/s;

    iget-object v1, v4, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/o;->d()Lcom/google/r/b/a/a/o;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/a/o;

    iget-object v1, v1, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/w;->d()Lcom/google/r/b/a/a/w;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/a/w;

    iget v1, v1, Lcom/google/r/b/a/a/w;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/he;->a(I)Lcom/google/maps/g/a/he;

    move-result-object v1

    if-nez v1, :cond_27

    sget-object v1, Lcom/google/maps/g/a/he;->a:Lcom/google/maps/g/a/he;

    :cond_27
    sget-object v9, Lcom/google/maps/g/a/he;->a:Lcom/google/maps/g/a/he;

    if-eq v1, v9, :cond_37

    iget-object v1, v4, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/o;->d()Lcom/google/r/b/a/a/o;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/a/o;

    iget-object v1, v1, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/k;->d()Lcom/google/r/b/a/a/k;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/a/k;

    iget-object v4, v4, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/o;->d()Lcom/google/r/b/a/a/o;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/r/b/a/a/o;

    iget-object v4, v4, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/w;->d()Lcom/google/r/b/a/a/w;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/r/b/a/a/w;

    iget v4, v4, Lcom/google/r/b/a/a/w;->b:I

    invoke-static {v4}, Lcom/google/maps/g/a/he;->a(I)Lcom/google/maps/g/a/he;

    move-result-object v4

    if-nez v4, :cond_28

    sget-object v4, Lcom/google/maps/g/a/he;->a:Lcom/google/maps/g/a/he;

    :cond_28
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/gmm/l/x;->c:[I

    invoke-virtual {v4}, Lcom/google/maps/g/a/he;->ordinal()I

    move-result v4

    aget v4, v10, v4

    packed-switch v4, :pswitch_data_1

    iget v4, v1, Lcom/google/r/b/a/a/k;->b:I

    invoke-static {v4}, Lcom/google/maps/g/a/fr;->a(I)Lcom/google/maps/g/a/fr;

    move-result-object v4

    if-nez v4, :cond_29

    sget-object v4, Lcom/google/maps/g/a/fr;->a:Lcom/google/maps/g/a/fr;

    :cond_29
    sget-object v10, Lcom/google/maps/g/a/fr;->a:Lcom/google/maps/g/a/fr;

    if-ne v4, v10, :cond_30

    sget-object v4, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    :goto_10
    if-nez v4, :cond_31

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2a
    move v0, v6

    goto/16 :goto_c

    :cond_2b
    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->r:Lcom/google/android/apps/gmm/map/r/a/ap;

    goto/16 :goto_d

    :cond_2c
    iget-object v3, p0, Lcom/google/android/apps/gmm/l/u;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    goto/16 :goto_e

    :cond_2d
    sget-object v1, Lcom/google/android/apps/gmm/l/x;->b:[I

    iget-object v4, p0, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/l/aa;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_2

    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    goto/16 :goto_f

    :pswitch_3
    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->b:Lcom/google/android/apps/gmm/directions/a/g;

    goto/16 :goto_f

    :pswitch_4
    sget-object v5, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    goto/16 :goto_f

    :pswitch_5
    sget-object v4, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    if-nez v4, :cond_2e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2e
    iget v10, v9, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v10, v10, 0x1

    iput v10, v9, Lcom/google/r/b/a/agw;->a:I

    iget v4, v4, Lcom/google/maps/g/a/hc;->i:I

    iput v4, v9, Lcom/google/r/b/a/agw;->b:I

    :goto_11
    iget v4, v1, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v10, 0x2

    if-ne v4, v10, :cond_32

    move v4, v7

    :goto_12
    if-eqz v4, :cond_34

    iget v4, v1, Lcom/google/r/b/a/a/k;->c:I

    invoke-static {v4}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v4

    if-nez v4, :cond_2f

    sget-object v4, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    :cond_2f
    if-nez v4, :cond_33

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_30
    sget-object v4, Lcom/google/maps/g/a/hc;->c:Lcom/google/maps/g/a/hc;

    goto :goto_10

    :cond_31
    iget v10, v9, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v10, v10, 0x1

    iput v10, v9, Lcom/google/r/b/a/agw;->a:I

    iget v4, v4, Lcom/google/maps/g/a/hc;->i:I

    iput v4, v9, Lcom/google/r/b/a/agw;->b:I

    goto :goto_11

    :cond_32
    move v4, v6

    goto :goto_12

    :cond_33
    iget v10, v9, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v10, v10, 0x2

    iput v10, v9, Lcom/google/r/b/a/agw;->a:I

    iget v4, v4, Lcom/google/maps/g/a/fu;->c:I

    iput v4, v9, Lcom/google/r/b/a/agw;->c:I

    :cond_34
    iget v4, v1, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v11, :cond_35

    move v6, v7

    :cond_35
    if-eqz v6, :cond_36

    iget-wide v10, v1, Lcom/google/r/b/a/a/k;->d:J

    iget v1, v9, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v9, Lcom/google/r/b/a/agw;->a:I

    iput-wide v10, v9, Lcom/google/r/b/a/agw;->d:J

    :cond_36
    invoke-static {}, Lcom/google/r/b/a/afz;->newBuilder()Lcom/google/r/b/a/agb;

    move-result-object v1

    iget-object v4, v1, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    invoke-virtual {v9}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v6

    iget-object v9, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v4, Lcom/google/n/ao;->d:Z

    iget v4, v1, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/r/b/a/agb;->a:I

    invoke-virtual {v1}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/afz;

    move-object v4, v1

    :goto_13
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->t:Lcom/google/android/apps/gmm/l/ad;

    if-eqz v1, :cond_3b

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->t:Lcom/google/android/apps/gmm/l/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/l/ad;->a:Lcom/google/maps/g/a/hm;

    :goto_14
    iget-object v6, p0, Lcom/google/android/apps/gmm/l/u;->z:Ljava/lang/String;

    sget-object v7, Lcom/google/b/f/bc;->a:Lcom/google/b/f/bc;

    new-instance v9, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v9, v7}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v7}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v7

    check-cast v7, Lcom/google/maps/g/hy;

    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/directions/a/g;Ljava/lang/String;Lcom/google/maps/g/hy;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_37
    move-object v4, v8

    goto :goto_13

    .line 525
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/u;->b(Lcom/google/android/apps/gmm/base/activities/c;)V

    goto/16 :goto_1

    .line 529
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    if-eqz v0, :cond_38

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_39

    :cond_38
    move v0, v7

    :goto_15
    if-eqz v0, :cond_3a

    move-object v1, v8

    :goto_16
    iget-object v4, p0, Lcom/google/android/apps/gmm/l/u;->y:Lcom/google/android/apps/gmm/streetview/b/a;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->L()Lcom/google/android/apps/gmm/streetview/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/u;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/l/u;->w:Lcom/google/android/apps/gmm/map/b/a/q;

    move v5, v6

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/streetview/b/a;Z)V

    goto/16 :goto_1

    :cond_39
    move v0, v6

    goto :goto_15

    :cond_3a
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    goto :goto_16

    .line 533
    :pswitch_8
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->L()Lcom/google/android/apps/gmm/streetview/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/streetview/a/a;->c()V

    goto/16 :goto_1

    .line 537
    :pswitch_9
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/apps/gmm/l/u;->b(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)D

    move-result-wide v2

    double-to-float v0, v2

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;F)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/l/w;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/apps/gmm/l/w;-><init>(Lcom/google/android/apps/gmm/l/u;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/a;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto/16 :goto_1

    .line 541
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->A:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_1

    .line 545
    :pswitch_b
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/u;->b(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->A:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_1

    .line 553
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/u;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Intent action received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_3b
    move-object v1, v8

    goto/16 :goto_14

    .line 510
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_9
        :pswitch_7
        :pswitch_6
        :pswitch_1
        :pswitch_1
        :pswitch_c
        :pswitch_8
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 517
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
