.class public Lcom/google/android/apps/gmm/reportmapissue/o;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/e/a/a/a/b;

.field private final b:Lcom/google/e/a/a/a/b;

.field private final c:Lcom/google/android/apps/gmm/reportmapissue/p;

.field private d:Lcom/google/e/a/a/a/b;


# direct methods
.method private constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/reportmapissue/p;)V
    .locals 2

    .prologue
    .line 64
    sget-object v0, Lcom/google/r/b/a/el;->bU:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/bf;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->a:Lcom/google/e/a/a/a/b;

    .line 67
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->b:Lcom/google/e/a/a/a/b;

    .line 68
    iput-object p3, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->c:Lcom/google/android/apps/gmm/reportmapissue/p;

    .line 69
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/reportmapissue/p;)Lcom/google/android/apps/gmm/reportmapissue/o;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 59
    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/reportmapissue/o;

    invoke-direct {v2, v0, v1, p1}, Lcom/google/android/apps/gmm/reportmapissue/o;-><init>(Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/reportmapissue/p;)V

    return-object v2

    .line 58
    :cond_0
    sget-object v2, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->S_()Lcom/google/b/a/ak;

    move-result-object v1

    const-string v0, "camera"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->a:Lcom/google/e/a/a/a/b;

    .line 112
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v1
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->d:Lcom/google/e/a/a/a/b;

    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public final ak_()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bf;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 80
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->a:Lcom/google/e/a/a/a/b;

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->b:Lcom/google/e/a/a/a/b;

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 83
    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->c:Lcom/google/android/apps/gmm/reportmapissue/p;

    if-eqz v0, :cond_0

    .line 96
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->d:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->c:Lcom/google/android/apps/gmm/reportmapissue/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->d:Lcom/google/e/a/a/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/p;->a_(Lcom/google/e/a/a/a/b;)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/o;->c:Lcom/google/android/apps/gmm/reportmapissue/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/reportmapissue/p;->c_()V

    goto :goto_0
.end method
