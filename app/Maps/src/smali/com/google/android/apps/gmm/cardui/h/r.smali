.class public Lcom/google/android/apps/gmm/cardui/h/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/g;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/util/b/ag;

.field private final b:Lcom/google/o/h/a/ol;

.field private final c:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/ol;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/h/r;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/r;->b:Lcom/google/o/h/a/ol;

    .line 26
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    .line 27
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/o/h/a/ol;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 28
    iget-object v0, p2, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget v0, v0, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 29
    new-array v0, v1, [Lcom/google/b/f/cq;

    sget-object v1, Lcom/google/b/f/t;->an:Lcom/google/b/f/t;

    aput-object v1, v0, v2

    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 32
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    .line 33
    if-eqz v0, :cond_1

    .line 34
    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    .line 36
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/r;->c:Lcom/google/android/apps/gmm/z/b/l;

    .line 37
    return-void

    :cond_2
    move v0, v2

    .line 28
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Float;)Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/r;->b:Lcom/google/o/h/a/ol;

    iget-object v0, v0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/r;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/r;->b:Lcom/google/o/h/a/ol;

    .line 64
    iget-object v0, v0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/r;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 65
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/cardui/h/r;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v4, v4, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;FLjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 63
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 68
    :cond_0
    return-object v5

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/r;->b:Lcom/google/o/h/a/ol;

    iget v0, v0, Lcom/google/o/h/a/ol;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/r;->c:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 51
    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/r;->b:Lcom/google/o/h/a/ol;

    iget-object v0, v2, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
