.class public Lcom/google/android/apps/gmm/map/t/ai;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/t/aj;

.field final synthetic b:Lcom/google/android/apps/gmm/map/t/af;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/af;Lcom/google/android/apps/gmm/map/t/aj;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/ai;->b:Lcom/google/android/apps/gmm/map/t/af;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/t/ai;->a:Lcom/google/android/apps/gmm/map/t/aj;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 384
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ai;->b:Lcom/google/android/apps/gmm/map/t/af;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/t/ai;->a:Lcom/google/android/apps/gmm/map/t/aj;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t/af;->f()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t/af;->g()I

    move-result v3

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    :cond_0
    invoke-interface {v8, v7}, Lcom/google/android/apps/gmm/map/t/aj;->a(Landroid/graphics/Bitmap;)V

    .line 389
    :goto_0
    return-void

    .line 388
    :cond_1
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    mul-int v0, v2, v3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v4, 0x1908

    const/16 v5, 0x1401

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    const-string v0, "GmmRenderTarget"

    const-string v1, "glReadPixels"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v9, v6}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, v9

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    invoke-interface {v8, v0}, Lcom/google/android/apps/gmm/map/t/aj;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v7

    goto :goto_1
.end method
