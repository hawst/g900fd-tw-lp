.class Lcom/google/android/apps/gmm/navigation/a/at;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/a/as;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/a/as;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/navigation/a/a;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eq p1, v0, :cond_0

    .line 313
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 314
    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected completion of: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but was: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    .line 313
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->b()V

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->g:Lcom/google/android/apps/gmm/navigation/a/f;

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->g:Lcom/google/android/apps/gmm/navigation/a/f;

    invoke-interface {v0, v6}, Lcom/google/android/apps/gmm/navigation/a/f;->a(Z)V

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->h:Z

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/at;->a:Lcom/google/android/apps/gmm/navigation/a/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/as;->a:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/bs;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/bu;->a:Lcom/google/android/apps/gmm/navigation/a/bu;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/a/bs;-><init>(Lcom/google/android/apps/gmm/navigation/a/bu;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 323
    :cond_2
    return-void
.end method
