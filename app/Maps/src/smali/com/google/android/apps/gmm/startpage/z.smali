.class Lcom/google/android/apps/gmm/startpage/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/startpage/aa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/z;->a:Ljava/util/Queue;

    .line 331
    return-void
.end method


# virtual methods
.method final declared-synchronized a(Lcom/google/android/apps/gmm/startpage/aa;)Z
    .locals 11

    .prologue
    const/16 v10, 0x80

    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 344
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/z;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/aa;

    .line 345
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->c:Lcom/google/android/apps/gmm/startpage/ad;

    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/startpage/ad;->d:Lcom/google/android/apps/gmm/startpage/ad;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/startpage/ad;->c:Lcom/google/android/apps/gmm/startpage/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_17

    :cond_1
    move v0, v2

    :goto_1
    if-eqz v0, :cond_0

    move v0, v2

    .line 349
    :goto_2
    monitor-exit p0

    return v0

    .line 345
    :cond_2
    :try_start_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v1, v1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v1, v1, 0x80

    if-ne v1, v10, :cond_3

    move v1, v2

    :goto_3
    if-eqz v1, :cond_1a

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v1, v1, Lcom/google/o/b/a/v;->i:F

    move v4, v1

    :goto_4
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v1, v1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v1, v1, 0x80

    if-ne v1, v10, :cond_4

    move v1, v2

    :goto_5
    if-eqz v1, :cond_19

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget v1, v1, Lcom/google/o/b/a/v;->i:F

    :goto_6
    cmpg-float v1, v4, v1

    if-gez v1, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->a:Lcom/google/android/apps/gmm/startpage/ad;

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_5

    :cond_5
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    if-nez v1, :cond_6

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_7

    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto :goto_0

    :cond_7
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    if-eqz v1, :cond_8

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    invoke-static {v1, v4, v8, v9}, Lcom/google/android/apps/gmm/startpage/aa;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;D)Z

    move-result v1

    if-nez v1, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto :goto_0

    :cond_8
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    if-eqz v1, :cond_9

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    invoke-virtual {v1, v4}, Lcom/google/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto :goto_0

    :cond_9
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_a

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    invoke-static {v1, v4, v8, v9}, Lcom/google/android/apps/gmm/startpage/aa;->a(Lcom/google/o/b/a/v;Lcom/google/o/b/a/v;D)Z

    move-result v1

    if-nez v1, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto/16 :goto_0

    :cond_a
    iget-object v7, p1, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    iget-object v8, v0, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    if-ne v7, v8, :cond_b

    move v1, v2

    :goto_7
    if-nez v1, :cond_14

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto/16 :goto_0

    :cond_b
    if-eqz v7, :cond_c

    if-nez v8, :cond_d

    :cond_c
    move v1, v3

    goto :goto_7

    :cond_d
    iget-object v1, v7, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/et;->d()Lcom/google/o/h/a/et;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/et;

    iget v1, v1, Lcom/google/o/h/a/et;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_10

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    move-object v4, v1

    :goto_8
    iget-object v1, v8, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/et;->d()Lcom/google/o/h/a/et;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/et;

    iget v1, v1, Lcom/google/o/h/a/et;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_e

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_e
    if-ne v4, v1, :cond_f

    invoke-virtual {v7}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v8}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eq v1, v4, :cond_11

    :cond_f
    move v1, v3

    goto :goto_7

    :cond_10
    move-object v4, v1

    goto :goto_8

    :cond_11
    move v4, v3

    :goto_9
    invoke-virtual {v7}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_13

    invoke-virtual {v7}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/sx;

    invoke-static {v1}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/o/h/a/sx;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v9

    invoke-virtual {v8}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/sx;

    invoke-static {v1}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/o/h/a/sx;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)Z

    move-result v1

    if-nez v1, :cond_12

    move v1, v3

    goto :goto_7

    :cond_12
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_9

    :cond_13
    move v1, v2

    goto/16 :goto_7

    :cond_14
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    if-eq v1, v4, :cond_15

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto/16 :goto_0

    :cond_15
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->b:Lcom/google/android/apps/gmm/startpage/ad;

    goto/16 :goto_0

    :cond_16
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ad;->d:Lcom/google/android/apps/gmm/startpage/ad;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_17
    move v0, v3

    goto/16 :goto_1

    :cond_18
    move v0, v3

    .line 349
    goto/16 :goto_2

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_19
    move v1, v5

    goto/16 :goto_6

    :cond_1a
    move v4, v5

    goto/16 :goto_4
.end method

.method declared-synchronized b(Lcom/google/android/apps/gmm/startpage/aa;)V
    .locals 1

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/z;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    monitor-exit p0

    return-void

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c(Lcom/google/android/apps/gmm/startpage/aa;)Z
    .locals 1

    .prologue
    .line 358
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/z;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
