.class public Lcom/google/android/apps/gmm/v/ce;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# instance fields
.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 242
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 243
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ce;->c:I

    .line 244
    const-string v0, "ShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget v0, p0, Lcom/google/android/apps/gmm/v/ce;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 246
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    const-string v0, "uTextureMatrix"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/v/ce;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/ce;->d:I

    .line 250
    return-void
.end method
