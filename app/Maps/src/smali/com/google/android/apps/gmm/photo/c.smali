.class Lcom/google/android/apps/gmm/photo/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/aw;


# instance fields
.field final a:Lcom/google/android/apps/gmm/photo/d;

.field private b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/photo/d;)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/c;->a:Lcom/google/android/apps/gmm/photo/d;

    .line 157
    return-void
.end method


# virtual methods
.method public final d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/c;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 164
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 165
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 166
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 167
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/c;->a:Lcom/google/android/apps/gmm/photo/d;

    if-lez v2, :cond_0

    if-lez v2, :cond_0

    if-lez v3, :cond_0

    if-ge v2, v2, :cond_3

    :cond_0
    move-object v1, v0

    .line 170
    :cond_1
    :goto_0
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/photo/c;->b:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/c;->b:Landroid/graphics/drawable/Drawable;

    :goto_1
    return-object v0

    .line 169
    :cond_3
    :try_start_1
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/photo/d;->a(I)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_4

    move-object v1, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v4, v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    int-to-float v2, v2

    div-float v2, v5, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v3, v2, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    div-int/lit8 v5, v2, 0x2

    sub-int/2addr v4, v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v1, v3, v4, v5, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v2

    goto :goto_0

    .line 172
    :catch_0
    move-exception v1

    goto :goto_1
.end method
