.class public Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# static fields
.field private static final d:Lcom/google/o/h/a/dq;


# instance fields
.field c:Lcom/google/android/apps/gmm/startpage/m;

.field private final e:Lcom/google/android/apps/gmm/startpage/d/d;

.field private f:Ljava/lang/String;

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    sput-object v0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->d:Lcom/google/o/h/a/dq;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/myprofile/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/myprofile/b;-><init>(Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->g:Ljava/lang/Object;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    sget-object v1, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->d:Lcom/google/o/h/a/dq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->c(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->e(Z)V

    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83
    const-string v1, "profile_page_odelay_state"

    iget-object v2, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 84
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 85
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->f:Ljava/lang/String;

    .line 189
    if-eqz p1, :cond_0

    .line 191
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 192
    return-void

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->iJ:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->B()Lcom/google/android/apps/gmm/u/a/a;

    move-result-object v1

    .line 184
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 183
    :goto_1
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/u/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_1
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->pl:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/google/b/f/t;->fo:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onAttach(Landroid/app/Activity;)V

    .line 91
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 95
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    const-string v1, "profile_page_odelay_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 100
    instance-of v1, v0, Lcom/google/android/apps/gmm/startpage/d/d;

    if-eqz v1, :cond_1

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 109
    :cond_0
    :goto_0
    new-instance v3, Lcom/google/android/apps/gmm/myprofile/c;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/myprofile/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/startpage/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 112
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/m;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->c:Lcom/google/android/apps/gmm/startpage/m;

    .line 114
    return-void

    .line 103
    :cond_1
    if-eqz v0, :cond_0

    .line 104
    const-string v0, "ProfileStartPageFragment"

    const-string v1, "OdelayContentState class cast exception"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 162
    const-class v1, Lcom/google/android/apps/gmm/myprofile/d;

    const/4 v2, 0x0

    .line 163
    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->c:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/libraries/curvular/ag;)V

    .line 165
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 171
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroy()V

    .line 172
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->c:Lcom/google/android/apps/gmm/startpage/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->c:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 155
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 156
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 118
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 126
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 127
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 128
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 129
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 131
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->c:Lcom/google/android/apps/gmm/startpage/m;

    .line 133
    iget-object v1, v1, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 132
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 134
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->a(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->c:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/ProfileStartPageFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/myprofile/d;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    const/16 v2, 0xfa

    .line 145
    invoke-static {v1, v0, v4, v2, v4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;IIZ)V

    .line 148
    return-void
.end method
