.class Lcom/google/android/apps/gmm/car/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/e/s;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/ae;

.field private final b:Lcom/google/android/apps/gmm/car/e/ak;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/ae;)V
    .locals 1

    .prologue
    .line 790
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 818
    new-instance v0, Lcom/google/android/apps/gmm/car/am;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/am;-><init>(Lcom/google/android/apps/gmm/car/al;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/al;->b:Lcom/google/android/apps/gmm/car/e/ak;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 794
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/l;->d()Lcom/google/android/apps/gmm/car/m/h;

    goto :goto_0

    .line 797
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 798
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/car/bm;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/gmm/car/bm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 802
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iput-boolean v7, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 804
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 805
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    if-eqz v0, :cond_1

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    goto :goto_0

    .line 808
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    new-instance v0, Lcom/google/android/apps/gmm/car/e/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    .line 809
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ae;->d:Lcom/google/android/apps/gmm/car/d/q;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/al;->b:Lcom/google/android/apps/gmm/car/e/ak;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/e/x;-><init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/m/l;Lcom/google/android/apps/gmm/car/d/q;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/e/ak;)V

    .line 808
    iput-object v0, v8, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    .line 810
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 811
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 813
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/ae;->g:Z

    if-eqz v0, :cond_7

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/al;->a:Lcom/google/android/apps/gmm/car/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_4
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-nez v0, :cond_5

    move v0, v6

    :goto_1
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_5
    move v0, v7

    goto :goto_1

    :cond_6
    iput-boolean v6, v1, Lcom/google/android/apps/gmm/car/bi;->c:Z

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 816
    :cond_7
    return-void
.end method
