.class public Lcom/google/android/apps/gmm/base/d/d;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field public a:I

.field b:I

.field c:I

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:Landroid/graphics/drawable/Drawable;

.field public f:Z

.field private g:[I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 50
    iput v1, p0, Lcom/google/android/apps/gmm/base/d/d;->b:I

    .line 51
    iput v1, p0, Lcom/google/android/apps/gmm/base/d/d;->c:I

    .line 68
    iput v0, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    .line 71
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/d/d;->f:Z

    .line 79
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/d/d;->d:Landroid/graphics/drawable/Drawable;

    .line 80
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/d/d;->e:Landroid/graphics/drawable/Drawable;

    .line 82
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    .line 83
    iget v0, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 87
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    .line 89
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/d;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/d/d;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/base/d/d;->b:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/d/d;->g:[I

    array-length v3, v3

    mul-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 116
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/d/d;->g:[I

    array-length v2, v2

    div-int v3, v1, v2

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/d/d;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/d/d;->e:Landroid/graphics/drawable/Drawable;

    new-instance v2, Landroid/graphics/Rect;

    iget v4, p0, Lcom/google/android/apps/gmm/base/d/d;->b:I

    iget v5, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/d;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    iget v7, p0, Lcom/google/android/apps/gmm/base/d/d;->c:I

    sub-int/2addr v6, v7

    invoke-direct {v2, v0, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 121
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/d/d;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 127
    :cond_0
    iget v2, p0, Lcom/google/android/apps/gmm/base/d/d;->b:I

    .line 128
    add-int v1, v2, v3

    .line 131
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/d/d;->g:[I

    array-length v5, v4

    :goto_0
    if-ge v0, v5, :cond_1

    aget v6, v4, v0

    .line 132
    iget-object v7, p0, Lcom/google/android/apps/gmm/base/d/d;->d:Landroid/graphics/drawable/Drawable;

    new-instance v8, Landroid/graphics/Rect;

    iget v9, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    iget v10, p0, Lcom/google/android/apps/gmm/base/d/d;->h:I

    .line 133
    int-to-float v6, v6

    iget v11, p0, Lcom/google/android/apps/gmm/base/d/d;->a:I

    int-to-float v11, v11

    div-float/2addr v6, v11

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/d;->getBounds()Landroid/graphics/Rect;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int v10, v11, v10

    add-int/lit8 v10, v10, -0x1

    int-to-float v10, v10

    mul-float/2addr v6, v10

    float-to-double v10, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-int v6, v10

    invoke-direct {v8, v9, v2, v6, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 132
    invoke-virtual {v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 134
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/d/d;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 136
    iget v2, p0, Lcom/google/android/apps/gmm/base/d/d;->c:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/base/d/d;->b:I

    add-int/2addr v2, v1

    .line 137
    add-int v1, v2, v3

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_1
    return-void
.end method


# virtual methods
.method public final a([I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 171
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/d/d;->g:[I

    .line 174
    array-length v3, p1

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, p1, v2

    .line 175
    if-le v0, v1, :cond_1

    .line 174
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 180
    :cond_0
    iput v1, p0, Lcom/google/android/apps/gmm/base/d/d;->a:I

    .line 181
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/d;->g:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/d;->g:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/d/d;->f:Z

    if-eqz v0, :cond_2

    .line 99
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 101
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/d;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/d/d;->a(Landroid/graphics/Canvas;)V

    .line 103
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 105
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/d/d;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 155
    const/16 v0, 0xff

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 164
    return-void
.end method
