.class public Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;
.super Lcom/google/android/apps/gmm/share/IntentHandlerDialog;
.source "PG"


# instance fields
.field h:Ljava/lang/String;

.field i:Z

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 114
    :goto_0
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    .line 113
    goto :goto_0

    .line 115
    :cond_1
    const-string v1, "isShortened"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->i:Z

    .line 116
    const-string v1, "urlFormatMessageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->j:I

    .line 117
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    .line 118
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->d:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_1

    .line 153
    iget v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->j:I

    if-lez v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->d:Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    .line 155
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->j:I

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->d:Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "com.android.bluetooth"

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "com.google.glass.companion"

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->e:Ljava/util/HashMap;

    .line 167
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 168
    return-void

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->d:Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 160
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->j:I

    if-lez v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->d:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->j:I

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->d:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 123
    invoke-super {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->onResume()V

    .line 124
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->i:Z

    if-nez v0, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 126
    invoke-static {}, Lcom/google/r/b/a/xb;->newBuilder()Lcom/google/r/b/a/xd;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    .line 127
    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v3, v1, Lcom/google/r/b/a/xd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/r/b/a/xd;->a:I

    iput-object v2, v1, Lcom/google/r/b/a/xd;->b:Ljava/lang/Object;

    const/4 v2, 0x0

    .line 128
    iget v3, v1, Lcom/google/r/b/a/xd;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Lcom/google/r/b/a/xd;->a:I

    iput v2, v1, Lcom/google/r/b/a/xd;->c:I

    .line 129
    invoke-virtual {v1}, Lcom/google/r/b/a/xd;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/xb;

    .line 130
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/share/j;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/share/j;-><init>(Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 139
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 144
    const-string v0, "isShortened"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    const-string v0, "urlFormatMessageId"

    iget v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    const-string v0, "url"

    iget-object v1, p0, Lcom/google/android/apps/gmm/share/ShortUrlIntentHandlerDialog;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    return-void
.end method
