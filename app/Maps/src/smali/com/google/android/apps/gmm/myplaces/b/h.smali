.class Lcom/google/android/apps/gmm/myplaces/b/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/apps/gmm/base/g/c;

.field final synthetic c:Lcom/google/android/apps/gmm/myplaces/b/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/b/a;ZLcom/google/android/apps/gmm/base/g/c;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->c:Lcom/google/android/apps/gmm/myplaces/b/a;

    iput-boolean p2, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->a:Z

    iput-object p3, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const-wide v6, 0x412e848000000000L    # 1000000.0

    .line 227
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->a:Z

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/myplaces/c/k;->a(Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/myplaces/c/k;

    move-result-object v0

    .line 229
    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->a(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/e/a/a/a/b;

    move-result-object v8

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->c:Lcom/google/android/apps/gmm/myplaces/b/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 231
    iget-object v3, v0, Lcom/google/android/apps/gmm/myplaces/c/f;->e:Lcom/google/android/apps/gmm/myplaces/c/h;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/myplaces/c/f;->f:J

    iget-wide v6, v0, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    .line 230
    iget-object v9, v1, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    new-instance v0, Lcom/google/android/apps/gmm/myplaces/b/o;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/myplaces/b/o;-><init>(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/android/apps/gmm/myplaces/c/h;JJLcom/google/e/a/a/a/b;)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ae;)Ljava/lang/Object;

    .line 241
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->c:Lcom/google/android/apps/gmm/myplaces/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->e:Lcom/google/android/apps/gmm/myplaces/c/j;

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-static {v2}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/myplaces/c/i;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/myplaces/c/i;-><init>(Lcom/google/android/apps/gmm/myplaces/c/j;Ljava/util/Set;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/b/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->c:Lcom/google/android/apps/gmm/myplaces/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->b:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Ljava/util/List;Ljava/util/List;)V

    .line 243
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/u;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->c:Lcom/google/android/apps/gmm/myplaces/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 236
    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    .line 235
    iget-object v4, v0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    new-instance v5, Lcom/google/android/apps/gmm/myplaces/b/q;

    invoke-direct {v5, v0, v2, v3, v1}, Lcom/google/android/apps/gmm/myplaces/b/q;-><init>(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;II)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ae;)Ljava/lang/Object;

    goto :goto_0

    .line 233
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/h;->c:Lcom/google/android/apps/gmm/myplaces/b/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v4, v5}, Lcom/google/b/h/c;->a(J)Lcom/google/b/h/c;

    move-result-object v0

    iget-object v3, v1, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    new-instance v4, Lcom/google/android/apps/gmm/myplaces/b/p;

    invoke-direct {v4, v1, v2, v0}, Lcom/google/android/apps/gmm/myplaces/b/p;-><init>(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/b/h/c;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ae;)Ljava/lang/Object;

    goto/16 :goto_0
.end method
