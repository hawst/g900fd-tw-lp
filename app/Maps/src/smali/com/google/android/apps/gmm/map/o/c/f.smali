.class public Lcom/google/android/apps/gmm/map/o/c/f;
.super Lcom/google/android/apps/gmm/map/o/c/g;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/apps/gmm/map/internal/c/be;

.field private final d:F

.field private g:F

.field private h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;F)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/c/g;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/c/f;->b:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 40
    iput p4, p0, Lcom/google/android/apps/gmm/map/o/c/f;->d:F

    .line 41
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->i:F

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;
    .locals 8

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/o/an;->a(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I

    move-result v6

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/o/an;->b(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I

    move-result v5

    .line 111
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->m:I

    if-eqz v0, :cond_1

    .line 112
    const/4 v6, 0x0

    .line 117
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    if-ne p1, v0, :cond_1

    .line 118
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->m:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/o/an;->a(I)I

    move-result v5

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/c/f;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 122
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    :goto_0
    iget v4, p0, Lcom/google/android/apps/gmm/map/o/c/f;->g:F

    iget v7, p0, Lcom/google/android/apps/gmm/map/o/c/f;->m:I

    .line 121
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FIII)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    .line 126
    return-object v0

    .line 122
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 45
    const/high16 v6, 0x3f800000    # 1.0f

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    if-eqz v0, :cond_1

    move v0, v5

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 47
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bm;->a:I

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->m:I

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 51
    if-eqz v3, :cond_0

    .line 52
    iget v6, v3, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->c:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->d:F

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/gmm/map/o/an;->a(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/o/ap;F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->g:F

    .line 55
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/ap;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->a:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/c/f;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget v4, p0, Lcom/google/android/apps/gmm/map/o/c/f;->g:F

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;Lcom/google/android/apps/gmm/map/internal/c/bn;FZF)[F

    move-result-object v0

    .line 59
    aget v1, v0, v7

    iput v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->i:F

    .line 60
    aget v1, v0, v5

    iput v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->j:F

    .line 61
    const/4 v1, 0x2

    aget v1, v0, v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->k:F

    .line 62
    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->l:F

    .line 63
    return v5

    :cond_1
    move v0, v7

    .line 46
    goto :goto_0

    :cond_2
    move v0, v7

    .line 47
    goto :goto_1
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->j:F

    return v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->k:F

    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->l:F

    return v0
.end method

.method public final e()F
    .locals 2

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/c/f;->j:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->k:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/c/f;->l:F

    sub-float/2addr v0, v1

    return v0
.end method
