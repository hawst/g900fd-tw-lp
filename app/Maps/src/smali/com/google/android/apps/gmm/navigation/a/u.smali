.class public Lcom/google/android/apps/gmm/navigation/a/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/s;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field b:Ljava/lang/String;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/navigation/a/x;",
            "Lcom/google/android/apps/gmm/navigation/a/w;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/navigation/a/t;

.field private e:Lcom/google/android/apps/gmm/navigation/a/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 3

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    .line 230
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->a:Lcom/google/android/apps/gmm/base/a;

    .line 231
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/h;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 232
    const-string v0, "/voice/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->b:Ljava/lang/String;

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/u;->b()V

    .line 236
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/u;->c()V

    .line 237
    return-void

    .line 232
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected static a(Lcom/google/maps/g/a/al;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 520
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/v;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/al;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 527
    const-string v0, "voice_instructions.zip"

    :goto_0
    return-object v0

    .line 522
    :pswitch_0
    const-string v0, "voice_instructions_imperial.zip"

    goto :goto_0

    .line 524
    :pswitch_1
    const-string v0, "voice_instructions_yards.zip"

    goto :goto_0

    .line 520
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/w;)V
    .locals 6

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/x;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/x;-><init>(Ljava/lang/String;Lcom/google/maps/g/a/al;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/w;

    .line 265
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/x;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/a/x;-><init>(Ljava/lang/String;Lcom/google/maps/g/a/al;)V

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/navigation/a/w;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/navigation/a/w;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 267
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/w;)V

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/u;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    monitor-exit p0

    return-void

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b()V
    .locals 14

    .prologue
    const/16 v13, 0x10

    const/4 v11, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aP:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v0, ""

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 241
    :cond_0
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    move v0, v11

    .line 243
    :goto_0
    array-length v1, v12

    if-ge v0, v1, :cond_4

    .line 244
    aget-object v1, v12, v0

    iget-object v10, p0, Lcom/google/android/apps/gmm/navigation/a/u;->b:Ljava/lang/String;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v2, v8

    const/4 v3, 0x5

    if-eq v2, v3, :cond_3

    const-string v2, "CannedSpeechManager"

    const-string v3, "Unable to parse voice bundle description: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 245
    :goto_2
    if-eqz v1, :cond_1

    .line 246
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/a/x;

    iget-object v4, v1, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/navigation/a/x;-><init>(Ljava/lang/String;Lcom/google/maps/g/a/al;)V

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/w;

    aget-object v2, v8, v11

    const/4 v3, 0x1

    aget-object v3, v8, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/directions/f/d/b;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, v8, v4

    invoke-static {v4, v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v4

    const/4 v6, 0x3

    aget-object v6, v8, v6

    invoke-static {v6, v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v6

    const/4 v9, 0x4

    aget-object v8, v8, v9

    invoke-static {v8, v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/gmm/navigation/a/w;-><init>(Ljava/lang/String;Lcom/google/maps/g/a/al;JJJLjava/lang/String;)V

    goto :goto_2

    .line 249
    :cond_4
    return-void
.end method

.method private static b(Lcom/google/android/apps/gmm/navigation/a/w;)V
    .locals 5

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/w;->a()Ljava/io/File;

    move-result-object v1

    .line 286
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 287
    if-nez v2, :cond_0

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 291
    new-instance v3, Ljava/io/File;

    aget-object v4, v2, v0

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 293
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private c()V
    .locals 10

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 274
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/w;

    .line 276
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v0, Lcom/google/android/apps/gmm/navigation/a/w;->d:J

    const-wide v8, 0x9a7ec800L

    add-long/2addr v6, v8

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 277
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/w;)V

    .line 278
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 276
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 281
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/u;->a()V

    .line 282
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/Locale;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/navigation/a/t;)Lcom/google/android/apps/gmm/navigation/a/n;
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/a/x;

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/gmm/navigation/a/x;-><init>(Ljava/lang/String;Lcom/google/maps/g/a/al;)V

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/w;

    move-object v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    if-eqz v3, :cond_5

    .line 322
    :try_start_1
    new-instance v2, Ljava/io/File;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/a/w;->a()Ljava/io/File;

    move-result-object v4

    iget-object v6, v3, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-static {v6}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Lcom/google/maps/g/a/al;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 323
    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/a/n;->a(Ljava/io/File;)Lcom/google/android/apps/gmm/navigation/a/n;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 324
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/google/android/apps/gmm/navigation/a/w;->d:J

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/u;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v8, v4

    .line 332
    :goto_0
    if-eqz v3, :cond_0

    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v10, v3, Lcom/google/android/apps/gmm/navigation/a/w;->e:J

    const-wide/32 v12, 0x5265c00

    add-long/2addr v10, v12

    cmp-long v2, v6, v10

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_1

    :cond_0
    if-nez v8, :cond_2

    .line 333
    :cond_1
    const-string v6, "CannedSpeechManager#loadBundle()"

    .line 334
    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/y;

    if-eqz v8, :cond_4

    move-object v7, v3

    :goto_2
    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/navigation/a/y;-><init>(Lcom/google/android/apps/gmm/navigation/a/u;Ljava/util/Locale;Lcom/google/maps/g/a/al;Ljava/lang/String;Lcom/google/android/apps/gmm/navigation/a/w;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;

    .line 335
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    .line 336
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/u;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 339
    :cond_2
    monitor-exit p0

    return-object v8

    .line 327
    :catch_0
    move-exception v2

    move-object v4, v5

    .line 328
    :goto_3
    :try_start_4
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1f

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Unable to parse speech bundle: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v8, v4

    goto :goto_0

    .line 332
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    move-object v7, v5

    .line 334
    goto :goto_2

    .line 318
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 327
    :catch_1
    move-exception v2

    goto :goto_3

    :cond_5
    move-object v8, v5

    goto :goto_0
.end method

.method declared-synchronized a()V
    .locals 4

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/w;

    .line 254
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 255
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/w;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 259
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aP:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    :cond_2
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/y;)V
    .locals 3

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/y;->b:Lcom/google/maps/g/a/al;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/v;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/al;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    .line 478
    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/a/y;->b:Lcom/google/maps/g/a/al;

    if-ne v0, v1, :cond_1

    .line 479
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 476
    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/google/maps/g/a/al;->b:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 484
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/a/y;->a:Ljava/util/Locale;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Ljava/util/Locale;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/navigation/a/t;)Lcom/google/android/apps/gmm/navigation/a/n;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    if-eqz v1, :cond_0

    .line 487
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/navigation/a/t;->a(Lcom/google/android/apps/gmm/navigation/a/n;)V

    .line 488
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    .line 489
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/y;Lcom/google/android/apps/gmm/navigation/a/w;[B)V
    .locals 5

    .prologue
    .line 431
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/navigation/a/w;->a()Ljava/io/File;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/apps/gmm/navigation/a/w;->b:Lcom/google/maps/g/a/al;

    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Lcom/google/maps/g/a/al;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 432
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 435
    invoke-virtual {v0, p3}, Ljava/io/FileOutputStream;->write([B)V

    .line 436
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    :try_start_2
    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/a/n;->a(Ljava/io/File;)Lcom/google/android/apps/gmm/navigation/a/n;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 462
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    if-eqz v1, :cond_0

    .line 463
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/navigation/a/t;->a(Lcom/google/android/apps/gmm/navigation/a/n;)V

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    .line 466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;

    .line 469
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/navigation/a/u;->a(Lcom/google/android/apps/gmm/navigation/a/w;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 470
    :goto_0
    monitor-exit p0

    return-void

    .line 437
    :catch_0
    move-exception v0

    .line 440
    :try_start_4
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot save bundle to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 441
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/y;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 431
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 443
    :catch_1
    move-exception v0

    .line 445
    :try_start_5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x16

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot save bundle to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 446
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 447
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/y;)V

    goto :goto_0

    .line 454
    :catch_2
    move-exception v2

    .line 456
    const-string v3, "Cannot parse bundle "

    iget-object v0, p2, Lcom/google/android/apps/gmm/navigation/a/w;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 457
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 458
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/navigation/a/u;->b(Lcom/google/android/apps/gmm/navigation/a/y;)V

    goto :goto_0

    .line 456
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method declared-synchronized b(Lcom/google/android/apps/gmm/navigation/a/y;)V
    .locals 1

    .prologue
    .line 494
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/t;->a()V

    .line 496
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->d:Lcom/google/android/apps/gmm/navigation/a/t;

    .line 497
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/u;->e:Lcom/google/android/apps/gmm/navigation/a/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    :cond_0
    monitor-exit p0

    return-void

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
