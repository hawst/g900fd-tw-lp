.class public Lcom/google/android/apps/gmm/search/e;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/b;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/e;->b:Ljava/util/ArrayList;

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/e;->a:Ljava/util/List;

    .line 67
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/am;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/am;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/e;->b:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 80
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 15

    .prologue
    .line 84
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/o/p;->d()Lcom/google/android/apps/gmm/map/o/z;

    move-result-object v13

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/e;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/google/android/apps/gmm/base/g/c;

    .line 86
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x37

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot draw a measle without latlng: featureId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", title="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/gmm/map/g/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/g/a;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/c;Lcom/google/android/apps/gmm/map/internal/c/bb;Ljava/lang/Integer;Lcom/google/android/apps/gmm/map/indoor/d/g;Lcom/google/android/apps/gmm/map/b/a/j;ZZZ)V

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    double-to-float v1, v4

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->hashCode()I

    move-result v4

    xor-int/2addr v3, v4

    xor-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const-string v1, "https://mts0.google.com/vt/icon/name=icons/spotlight/measle_5px.png&scale=4"

    const/4 v3, 0x4

    invoke-direct {v5, v1, v3}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;I)V

    const/high16 v7, 0x3f800000    # 1.0f

    sget-object v10, Lcom/google/android/apps/gmm/map/t/l;->v:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v3, v13, Lcom/google/android/apps/gmm/map/o/z;->i:Lcom/google/android/apps/gmm/map/o/bj;

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/o/z;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    iget-object v12, v13, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    move-object v8, v2

    move-object v9, p0

    move-object v11, v0

    invoke-virtual/range {v3 .. v12}, Lcom/google/android/apps/gmm/map/o/bj;->a(ILcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/g/b;Lcom/google/android/apps/gmm/map/o/h;)Lcom/google/android/apps/gmm/map/o/bi;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(I)V

    sget-object v1, Lcom/google/b/f/t;->gi:Lcom/google/b/f/t;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->s:Lcom/google/b/f/cq;

    new-instance v1, Lcom/google/android/apps/gmm/map/o/as;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/o/d;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 88
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 4

    .prologue
    .line 94
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/as;->a:Lcom/google/android/apps/gmm/map/o/d;

    const/16 v3, 0x40

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    .line 94
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 98
    return-void
.end method
