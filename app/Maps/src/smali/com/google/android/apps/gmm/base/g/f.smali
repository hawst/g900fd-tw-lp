.class public final enum Lcom/google/android/apps/gmm/base/g/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/g/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/g/f;

.field public static final enum b:Lcom/google/android/apps/gmm/base/g/f;

.field public static final enum c:Lcom/google/android/apps/gmm/base/g/f;

.field public static final enum d:Lcom/google/android/apps/gmm/base/g/f;

.field public static final enum e:Lcom/google/android/apps/gmm/base/g/f;

.field public static final enum f:Lcom/google/android/apps/gmm/base/g/f;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/base/g/f;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/base/g/f;

    const-string v1, "GEOCODE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/base/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/base/g/f;

    const-string v1, "BUSINESS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/base/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->b:Lcom/google/android/apps/gmm/base/g/f;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/base/g/f;

    const-string v1, "AD"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/base/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->c:Lcom/google/android/apps/gmm/base/g/f;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/base/g/f;

    const-string v1, "STATION"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/base/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/base/g/f;

    const-string v1, "MY_MAPS_FEATURE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/base/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->e:Lcom/google/android/apps/gmm/base/g/f;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/base/g/f;

    const-string v1, "UNRESOLVED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->f:Lcom/google/android/apps/gmm/base/g/f;

    .line 128
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/g/f;

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->b:Lcom/google/android/apps/gmm/base/g/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->c:Lcom/google/android/apps/gmm/base/g/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->e:Lcom/google/android/apps/gmm/base/g/f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/base/g/f;->f:Lcom/google/android/apps/gmm/base/g/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/base/g/f;->g:[Lcom/google/android/apps/gmm/base/g/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/g/f;
    .locals 1

    .prologue
    .line 128
    const-class v0, Lcom/google/android/apps/gmm/base/g/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/g/f;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->g:[Lcom/google/android/apps/gmm/base/g/f;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/g/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/g/f;

    return-object v0
.end method
