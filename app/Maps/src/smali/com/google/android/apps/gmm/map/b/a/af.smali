.class public abstract Lcom/google/android/apps/gmm/map/b/a/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/b/a/d;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/gmm/map/b/a/af;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/af;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 109
    invoke-virtual {p0, v10}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 111
    invoke-virtual {p0, v10}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move v3, v0

    move v4, v2

    move v5, v2

    move v2, v0

    move v0, v1

    .line 113
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 114
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 115
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 116
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 117
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {p1, v10}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v6, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 122
    invoke-virtual {p1, v10}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    move v7, v0

    move v8, v6

    move v9, v6

    move v6, v0

    move v0, v1

    .line 124
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v11

    if-ge v0, v11, :cond_1

    .line 125
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 126
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v8, v11}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 127
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v7, v11}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 128
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v6, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 131
    :cond_1
    if-gt v5, v8, :cond_2

    if-gt v3, v6, :cond_2

    if-lt v4, v9, :cond_2

    if-lt v2, v7, :cond_2

    :goto_2
    return v1

    :cond_2
    move v1, v10

    goto :goto_2
.end method

.method private d(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v8

    .line 143
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v9

    .line 144
    if-eqz v8, :cond_0

    if-nez v9, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 147
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/af;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 148
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    move v5, v0

    move-object v6, v1

    .line 149
    :goto_1
    if-ge v5, v8, :cond_0

    .line 150
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    move v1, v0

    move-object v2, v3

    .line 152
    :goto_2
    if-ge v1, v9, :cond_3

    .line 153
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 154
    invoke-static {v6, v7, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 157
    const/4 v0, 0x1

    goto :goto_0

    .line 152
    :cond_2
    add-int/lit8 v1, v1, 0x1

    move-object v2, v4

    goto :goto_2

    .line 149
    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v6, v7

    goto :goto_1
.end method


# virtual methods
.method public a()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(I)Lcom/google/android/apps/gmm/map/b/a/y;
.end method

.method public a(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 91
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    if-nez v1, :cond_2

    .line 92
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/af;->a:Ljava/lang/String;

    const-string v2, "Tried to intersect null region, or region with null vertex."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    :cond_1
    :goto_0
    return v0

    .line 96
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/af;->c(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 98
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 99
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/af;->d(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
.end method

.method public abstract b()I
.end method

.method public b(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/af;->c(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 71
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 72
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 76
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/af;->d(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public c()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 40
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 43
    const/4 v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/af;->b()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 44
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 45
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 46
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 47
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/af;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5, v4, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-direct {v0, v5, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v0
.end method
