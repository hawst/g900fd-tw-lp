.class public Lcom/google/android/apps/gmm/base/g/a;
.super Lcom/google/android/apps/gmm/map/v/a;
.source "PG"


# static fields
.field private static final v:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/internal/c/c;

.field b:Lcom/google/android/apps/gmm/map/b/a/j;

.field c:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final d:Z

.field public transient e:Ljava/lang/String;

.field public transient f:Ljava/lang/String;

.field public transient g:Ljava/lang/String;

.field public transient h:Ljava/lang/String;

.field public transient i:Ljava/lang/String;

.field public transient j:Ljava/lang/String;

.field public transient k:Z

.field transient l:Ljava/lang/String;

.field transient m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public transient n:Ljava/lang/String;

.field transient o:Ljava/lang/String;

.field public transient p:Lcom/google/android/apps/gmm/z/b/l;

.field transient q:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/ade;",
            ">;"
        }
    .end annotation
.end field

.field public transient r:Ljava/lang/String;

.field public transient s:Ljava/lang/String;

.field transient t:Lcom/google/r/b/a/aje;

.field private transient w:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/android/apps/gmm/base/g/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/g/a;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/g/a;Lcom/google/e/a/a/a/b;Z)V
    .locals 8
    .param p1    # Lcom/google/android/apps/gmm/map/g/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 85
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/v/a;-><init>(Lcom/google/e/a/a/a/b;)V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/g/a;->w:I

    .line 86
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 88
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/a;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 89
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/g/d;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 90
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v2

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 92
    :cond_0
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/base/g/a;->d:Z

    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/g/a;->c()V

    .line 94
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;Z)[Lcom/google/e/a/a/a/b;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 355
    if-nez p0, :cond_1

    move-object v0, v1

    .line 371
    :cond_0
    :goto_0
    return-object v0

    .line 358
    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x3

    :goto_1
    const/16 v2, 0x1a

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 361
    if-nez v0, :cond_3

    .line 362
    sget-object v0, Lcom/google/android/apps/gmm/base/g/a;->v:Ljava/lang/String;

    const-string v2, "TactileAdsResponseProto was missing a text ad set"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 363
    goto :goto_0

    .line 358
    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    .line 365
    :cond_3
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 367
    array-length v2, v0

    if-nez v2, :cond_0

    .line 368
    sget-object v0, Lcom/google/android/apps/gmm/base/g/a;->v:Ljava/lang/String;

    const-string v2, "TactileTextAdSetProto has no text ads"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 369
    goto :goto_0
.end method

.method public static b(Lcom/google/e/a/a/a/b;Z)Lcom/google/e/a/a/a/b;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 381
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/base/g/a;->a(Lcom/google/e/a/a/a/b;Z)[Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 383
    :cond_0
    const/4 v0, 0x0

    .line 385
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private c()V
    .locals 14

    .prologue
    const/4 v5, -0x1

    const/4 v13, 0x2

    const/16 v12, 0x1a

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 97
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 102
    if-nez v6, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-static {v6, v13}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->e:Ljava/lang/String;

    .line 108
    const/4 v0, 0x3

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->f:Ljava/lang/String;

    .line 109
    const/4 v0, 0x4

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->g:Ljava/lang/String;

    .line 110
    const/4 v0, 0x5

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->h:Ljava/lang/String;

    .line 111
    const/4 v0, 0x6

    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    .line 112
    const/16 v0, 0xa

    .line 113
    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->j:Ljava/lang/String;

    .line 115
    const/16 v0, 0x9

    invoke-static {v6, v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/g/a;->k:Z

    .line 117
    const/4 v0, 0x7

    iget-object v1, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/g/a;->w:I

    .line 120
    iget v0, p0, Lcom/google/android/apps/gmm/base/g/a;->w:I

    if-lez v0, :cond_3

    .line 121
    const/4 v0, 0x7

    invoke-virtual {v6, v0, v4, v12}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 122
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    .line 124
    if-nez v1, :cond_8

    .line 125
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/g/a;->d:Z

    if-eqz v1, :cond_7

    move v1, v3

    .line 126
    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/c;

    invoke-direct {v2, v5, v5, v5, v1}, Lcom/google/android/apps/gmm/map/internal/c/c;-><init>(IIII)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 134
    :goto_2
    const/16 v1, 0x8

    .line 145
    invoke-static {}, Lcom/google/r/b/a/ade;->d()Lcom/google/r/b/a/ade;

    move-result-object v2

    .line 142
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;ILcom/google/n/at;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->q:Lcom/google/b/c/cv;

    .line 147
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    .line 148
    if-eqz v1, :cond_2

    .line 149
    const/4 v2, 0x3

    const/16 v5, 0x13

    invoke-virtual {v1, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v8

    .line 150
    const/16 v2, 0x13

    invoke-virtual {v1, v13, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v10

    .line 151
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v1, v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 154
    :cond_2
    invoke-static {v0, v13}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->l:Ljava/lang/String;

    .line 156
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->n:Ljava/lang/String;

    .line 158
    const/4 v7, 0x4

    if-nez v0, :cond_9

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_3
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->m:Ljava/util/List;

    .line 160
    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->o:Ljava/lang/String;

    .line 162
    const/16 v1, 0x9

    .line 163
    invoke-virtual {v0, v1, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 164
    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v1

    .line 162
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_10

    :goto_4
    check-cast v0, Lcom/google/r/b/a/aje;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->t:Lcom/google/r/b/a/aje;

    .line 167
    :cond_3
    const/16 v0, 0xb

    invoke-virtual {v6, v0, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 168
    if-eqz v0, :cond_4

    .line 169
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->r:Ljava/lang/String;

    .line 171
    invoke-static {v0, v13}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->s:Ljava/lang/String;

    .line 178
    :cond_4
    iget-object v0, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_11

    move v0, v3

    :goto_5
    if-nez v0, :cond_5

    invoke-virtual {v6, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_5
    move v4, v3

    :cond_6
    if-eqz v4, :cond_0

    .line 179
    new-instance v2, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    .line 181
    invoke-virtual {v6, v3, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 182
    invoke-static {}, Lcom/google/r/b/a/acm;->h()Lcom/google/r/b/a/acm;

    move-result-object v1

    .line 180
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_12

    :goto_6
    check-cast v0, Lcom/google/r/b/a/acm;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Lcom/google/r/b/a/acm;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    .line 183
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/a;->p:Lcom/google/android/apps/gmm/z/b/l;

    goto/16 :goto_0

    :cond_7
    move v1, v4

    .line 125
    goto/16 :goto_1

    .line 132
    :cond_8
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/c/c;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/c;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    goto/16 :goto_2

    .line 158
    :cond_9
    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v8

    if-ltz v8, :cond_a

    move v1, v3

    :goto_7
    if-nez v1, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_a
    move v1, v4

    goto :goto_7

    :cond_b
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v4

    :goto_8
    if-ge v5, v8, :cond_f

    const/16 v1, 0x1c

    invoke-virtual {v0, v7, v5, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_e

    :cond_c
    move v1, v3

    :goto_9
    if-nez v1, :cond_d

    const/16 v1, 0x1c

    invoke-virtual {v0, v7, v5, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_8

    :cond_e
    move v1, v4

    goto :goto_9

    :cond_f
    move-object v1, v2

    goto/16 :goto_3

    :cond_10
    move-object v0, v1

    .line 162
    goto/16 :goto_4

    :cond_11
    move v0, v4

    .line 178
    goto/16 :goto_5

    :cond_12
    move-object v0, v1

    .line 180
    goto :goto_6
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0

    .prologue
    .line 338
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/g/a;->c()V

    .line 340
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0

    .prologue
    .line 334
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 335
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/a;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/g/a;->s:Ljava/lang/String;

    .line 259
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    move v2, v0

    :goto_1
    if-nez v2, :cond_4

    :goto_2
    return v0

    :cond_2
    move v2, v1

    .line 258
    goto :goto_0

    :cond_3
    move v2, v1

    .line 259
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method protected final b()Lcom/google/e/a/a/a/d;
    .locals 1

    .prologue
    .line 344
    sget-object v0, Lcom/google/r/b/a/b/at;->f:Lcom/google/e/a/a/a/d;

    return-object v0
.end method
