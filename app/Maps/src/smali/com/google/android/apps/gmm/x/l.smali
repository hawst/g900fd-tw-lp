.class public final Lcom/google/android/apps/gmm/x/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/x/l",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/x/l;-><init>(Ljava/lang/String;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    .line 79
    iput p2, p0, Lcom/google/android/apps/gmm/x/l;->b:I

    .line 80
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/x/l;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 89
    if-nez v0, :cond_0

    .line 90
    iget v0, p0, Lcom/google/android/apps/gmm/x/l;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/x/l;->b:I

    if-ge v0, v1, :cond_1

    const/4 v0, -0x1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/x/l;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/x/l;->b:I

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/apps/gmm/x/l;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/x/l;->a(Lcom/google/android/apps/gmm/x/l;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 102
    instance-of v1, p1, Lcom/google/android/apps/gmm/x/l;

    if-nez v1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/x/l;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/x/l;->a(Lcom/google/android/apps/gmm/x/l;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 110
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/x/l;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/x/l;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
