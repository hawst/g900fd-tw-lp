.class public Lcom/google/android/apps/gmm/place/station/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/b;


# static fields
.field static final a:Ljava/lang/String;

.field static final b:Lcom/google/android/libraries/curvular/cg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:J

.field private static final k:J

.field private static final l:Lcom/google/b/a/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/ar",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Lcom/google/b/a/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/ar",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:Lcom/google/android/apps/gmm/directions/h/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final e:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation
.end field

.field final i:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    const-class v0, Lcom/google/android/apps/gmm/place/station/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/station/z;->a:Ljava/lang/String;

    .line 61
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/place/station/z;->j:J

    .line 73
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/place/station/z;->k:J

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/place/station/aa;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/aa;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/z;->b:Lcom/google/android/libraries/curvular/cg;

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ab;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/ab;-><init>()V

    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ac;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/ac;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/z;->l:Lcom/google/b/a/ar;

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ad;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/ad;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/z;->m:Lcom/google/b/a/ar;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->c:Ljava/lang/String;

    .line 128
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->d:Lcom/google/android/apps/gmm/directions/h/a;

    .line 129
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->f:Lcom/google/b/c/cv;

    .line 130
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->e:Lcom/google/b/c/cv;

    .line 131
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    .line 132
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->g:Lcom/google/b/c/cv;

    .line 133
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->h:Lcom/google/b/c/cv;

    .line 134
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->i:Lcom/google/b/c/cv;

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/wb;J)V
    .locals 4
    .param p2    # Lcom/google/android/libraries/curvular/cg;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;",
            "Lcom/google/maps/g/wb;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    invoke-virtual {p3}, Lcom/google/maps/g/wb;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->c:Ljava/lang/String;

    .line 155
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/place/station/z;->a(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/wb;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->f:Lcom/google/b/c/cv;

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->f:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/z;->l:Lcom/google/b/a/ar;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    new-instance v2, Lcom/google/b/c/ee;

    invoke-direct {v2, v0, v1}, Lcom/google/b/c/ee;-><init>(Ljava/lang/Iterable;Lcom/google/b/a/ar;)V

    invoke-static {v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Iterable;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->e:Lcom/google/b/c/cv;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->f:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/z;->m:Lcom/google/b/a/ar;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    new-instance v2, Lcom/google/b/c/ee;

    invoke-direct {v2, v0, v1}, Lcom/google/b/c/ee;-><init>(Ljava/lang/Iterable;Lcom/google/b/a/ar;)V

    invoke-static {v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Iterable;)Lcom/google/b/c/cv;

    .line 158
    invoke-static {p1, p3}, Lcom/google/android/apps/gmm/place/station/z;->a(Landroid/content/Context;Lcom/google/maps/g/wb;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->g:Lcom/google/b/c/cv;

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/z;->g:Lcom/google/b/c/cv;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->e:Lcom/google/b/c/cv;

    .line 161
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 162
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 159
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/place/station/z;->a(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->h:Lcom/google/b/c/cv;

    .line 163
    invoke-static {p3}, Lcom/google/android/apps/gmm/place/station/z;->a(Lcom/google/maps/g/wb;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->i:Lcom/google/b/c/cv;

    .line 164
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 165
    invoke-virtual {p3}, Lcom/google/maps/g/wb;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/directions/a/f;->a(Landroid/content/Context;Ljava/util/List;)Lcom/google/android/apps/gmm/directions/h/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->d:Lcom/google/android/apps/gmm/directions/h/a;

    .line 166
    return-void

    .line 161
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/wb;)Lcom/google/b/c/cv;
    .locals 11
    .param p1    # Lcom/google/android/libraries/curvular/cg;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;",
            "Lcom/google/maps/g/wb;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    const/4 v2, 0x0

    .line 237
    iget v0, p2, Lcom/google/maps/g/wb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {p2}, Lcom/google/maps/g/wb;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    .line 240
    :cond_0
    invoke-virtual {p2}, Lcom/google/maps/g/wb;->d()Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v8

    .line 242
    invoke-virtual {p2}, Lcom/google/maps/g/wb;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vp;

    .line 243
    iget v1, v0, Lcom/google/maps/g/vp;->j:I

    invoke-static {v1}, Lcom/google/maps/g/vs;->a(I)Lcom/google/maps/g/vs;

    move-result-object v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/google/maps/g/vs;->a:Lcom/google/maps/g/vs;

    .line 245
    :cond_2
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Lcom/google/maps/g/vp;)Lcom/google/android/apps/gmm/base/views/c/b;

    move-result-object v5

    .line 246
    invoke-virtual {v0}, Lcom/google/maps/g/vp;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/maps/g/vx;

    .line 247
    iget-object v0, v7, Lcom/google/maps/g/vx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/place/station/an;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/place/station/an;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/maps/g/vs;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/libraries/curvular/cg;Lcom/google/maps/g/vx;)V

    invoke-virtual {v8, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 237
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 260
    :cond_5
    invoke-virtual {v8}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/maps/g/wb;)Lcom/google/b/c/cv;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/maps/g/wb;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    invoke-virtual/range {p1 .. p1}, Lcom/google/maps/g/wb;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v3

    .line 266
    invoke-virtual/range {p1 .. p1}, Lcom/google/maps/g/wb;->d()Ljava/lang/String;

    move-result-object v4

    .line 268
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 269
    invoke-virtual/range {p1 .. p1}, Lcom/google/maps/g/wb;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/vp;

    .line 270
    iget v2, v1, Lcom/google/maps/g/vp;->j:I

    invoke-static {v2}, Lcom/google/maps/g/vs;->a(I)Lcom/google/maps/g/vs;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/vs;->a:Lcom/google/maps/g/vs;

    :cond_1
    sget-object v5, Lcom/google/maps/g/vs;->b:Lcom/google/maps/g/vs;

    if-ne v2, v5, :cond_0

    .line 274
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Lcom/google/maps/g/vp;)Lcom/google/android/apps/gmm/base/views/c/b;

    move-result-object v11

    .line 275
    invoke-virtual {v1}, Lcom/google/maps/g/vp;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/vx;

    .line 276
    invoke-virtual {v1}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v5

    .line 277
    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/v;->a(Lcom/google/maps/g/vx;)Ljava/lang/String;

    move-result-object v6

    .line 278
    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/v;->b(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;

    move-result-object v7

    .line 280
    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/v;->c(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;

    move-result-object v8

    .line 281
    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/v;->d(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;

    move-result-object v9

    .line 282
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Lcom/google/maps/g/vx;)Lcom/google/android/apps/gmm/base/views/c/b;

    move-result-object v10

    .line 283
    invoke-virtual {v1}, Lcom/google/maps/g/vx;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/google/maps/g/vk;

    .line 284
    invoke-virtual {v15}, Lcom/google/maps/g/vk;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/maps/g/vf;

    .line 286
    iget-object v1, v13, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/google/maps/g/a/fo;

    new-instance v1, Lcom/google/android/apps/gmm/place/station/am;

    .line 298
    invoke-virtual {v15}, Lcom/google/maps/g/vk;->d()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/place/station/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Ljava/lang/String;Lcom/google/maps/g/vf;)V

    .line 285
    new-instance v2, Lcom/google/b/a/an;

    invoke-direct {v2, v14, v1}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 306
    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/place/station/ae;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/station/ae;-><init>()V

    move-object/from16 v0, v16

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 315
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v2

    .line 316
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/b/a/an;

    .line 317
    iget-object v1, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 319
    :cond_5
    invoke-virtual {v2}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    return-object v1
.end method

.method private static a(Lcom/google/maps/g/wb;)Lcom/google/b/c/cv;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/wb;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 361
    invoke-virtual {p0}, Lcom/google/maps/g/wb;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/vp;

    .line 362
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/maps/g/vp;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 363
    new-instance v4, Lcom/google/android/apps/gmm/directions/i/af;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/directions/i/af;-><init>(Lcom/google/maps/g/a/da;)V

    invoke-virtual {v1, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 366
    :cond_2
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/google/b/c/cv;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    .line 334
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-wide v0, Lcom/google/android/apps/gmm/place/station/z;->k:J

    move-wide v2, v0

    .line 338
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x14

    move v1, v0

    .line 342
    :goto_1
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    move v4, v0

    :goto_2
    if-ge v4, v6, :cond_3

    .line 343
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 344
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/d;->v()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 343
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    .line 348
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v10, v2

    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    if-ge v4, v1, :cond_3

    .line 349
    :cond_0
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 342
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 334
    :cond_1
    sget-wide v0, Lcom/google/android/apps/gmm/place/station/z;->j:J

    move-wide v2, v0

    goto :goto_0

    .line 338
    :cond_2
    const/4 v0, 0x4

    move v1, v0

    goto :goto_1

    .line 355
    :cond_3
    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->e:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->f:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->g:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->h:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    sget-object v0, Lcom/google/android/apps/gmm/place/station/z;->b:Lcom/google/android/libraries/curvular/cg;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/directions/h/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->d:Lcom/google/android/apps/gmm/directions/h/a;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/z;->i:Lcom/google/b/c/cv;

    return-object v0
.end method
