.class public Lcom/google/android/apps/gmm/map/internal/c/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final b:I

.field public final c:F

.field final d:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final e:F

.field private final f:F

.field private final g:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;IFLcom/google/android/apps/gmm/map/b/a/y;FFF)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 46
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    .line 47
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    .line 48
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 49
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->e:F

    .line 50
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->f:F

    .line 51
    iput p7, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->g:F

    .line 52
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 192
    if-ne p0, p1, :cond_1

    .line 228
    :cond_0
    :goto_0
    return v0

    .line 195
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 196
    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 199
    goto :goto_0

    .line 201
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 202
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_4

    .line 203
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_5

    move v0, v1

    .line 204
    goto :goto_0

    .line 206
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 207
    goto :goto_0

    .line 209
    :cond_5
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->f:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 210
    goto :goto_0

    .line 212
    :cond_6
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->e:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_7
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->g:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_8
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 219
    goto :goto_0

    .line 221
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_a

    .line 222
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_b

    move v0, v1

    .line 223
    goto :goto_0

    .line 225
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 226
    goto :goto_0

    .line 228
    :cond_b
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 181
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->f:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 182
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 184
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    add-int/2addr v0, v2

    .line 185
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 186
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    return v0

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->hashCode()I

    move-result v0

    goto :goto_0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AbsolutePosition{mPosition = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mComponents = "

    .line 240
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRotation = "

    .line 241
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBoundCenter = "

    .line 242
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBoundRotation = "

    .line 243
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBoundHeight = "

    .line 244
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->f:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBoundWidth = "

    .line 245
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/a;->g:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    .line 246
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
