.class public Lcom/google/android/apps/gmm/directions/bt;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/gmm/directions/cz;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/google/android/apps/gmm/directions/ck;

.field b:Landroid/view/View$OnClickListener;

.field c:Lcom/google/android/apps/gmm/directions/cj;

.field private d:I

.field private e:Lcom/google/android/apps/gmm/directions/cm;

.field private f:[Lcom/google/android/apps/gmm/directions/views/a;

.field private final g:Landroid/graphics/Typeface;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Landroid/graphics/Typeface;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Landroid/graphics/Typeface;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 214
    sget v0, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    .line 215
    sget v0, Lcom/google/android/apps/gmm/k;->aa:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->h:Landroid/graphics/Typeface;

    .line 216
    sget v0, Lcom/google/android/apps/gmm/k;->X:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->i:Landroid/graphics/Typeface;

    .line 217
    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 24

    .prologue
    .line 688
    if-nez p2, :cond_13

    .line 689
    sget v5, Lcom/google/android/apps/gmm/h;->B:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v4, v5, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 690
    new-instance v5, Lcom/google/android/apps/gmm/directions/cl;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/directions/cl;-><init>()V

    .line 691
    sget v4, Lcom/google/android/apps/gmm/g;->Z:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->a:Landroid/widget/TextView;

    .line 692
    sget v4, Lcom/google/android/apps/gmm/g;->at:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->b:Landroid/widget/TextView;

    .line 693
    sget v4, Lcom/google/android/apps/gmm/g;->cZ:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->c:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    .line 694
    sget v4, Lcom/google/android/apps/gmm/g;->aT:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->d:Landroid/widget/ImageView;

    .line 695
    sget v4, Lcom/google/android/apps/gmm/g;->ea:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->e:Landroid/view/View;

    .line 696
    sget v4, Lcom/google/android/apps/gmm/g;->dY:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->f:Landroid/widget/ImageView;

    .line 697
    sget v4, Lcom/google/android/apps/gmm/g;->bV:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->g:Landroid/view/ViewGroup;

    .line 698
    sget v4, Lcom/google/android/apps/gmm/g;->O:I

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->h:Landroid/view/View;

    .line 699
    iget-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 700
    iget-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->a:Landroid/widget/TextView;

    sget v6, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 701
    iget-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 702
    iget-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->b:Landroid/widget/TextView;

    sget v6, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 703
    iget-object v4, v5, Lcom/google/android/apps/gmm/directions/cl;->e:Landroid/view/View;

    sget-object v6, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    .line 704
    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/base/h/c;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 703
    invoke-virtual {v4, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 707
    invoke-virtual {v10, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v12, v5

    :goto_0
    move-object v4, v10

    .line 712
    check-cast v4, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    sget v6, Lcom/google/android/apps/gmm/g;->ej:I

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 714
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lcom/google/android/apps/gmm/directions/db;

    .line 715
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/activities/c;

    .line 720
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->l:Lcom/google/maps/g/a/hm;

    sget-object v6, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v5, v6, :cond_17

    .line 721
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->e:Ljava/lang/String;

    if-nez v5, :cond_0

    const-string v5, ""

    .line 722
    :cond_0
    iget-object v6, v11, Lcom/google/android/apps/gmm/directions/db;->g:Ljava/lang/String;

    if-nez v6, :cond_1

    const-string v6, ""

    .line 723
    :cond_1
    iget-object v7, v11, Lcom/google/android/apps/gmm/directions/db;->f:Ljava/lang/String;

    if-nez v7, :cond_2

    const-string v7, ""

    .line 724
    :cond_2
    iget-object v8, v11, Lcom/google/android/apps/gmm/directions/db;->b:Ljava/lang/Integer;

    iget-object v9, v11, Lcom/google/android/apps/gmm/directions/db;->c:Ljava/lang/Integer;

    iget-object v13, v11, Lcom/google/android/apps/gmm/directions/db;->d:Ljava/lang/Integer;

    iget-object v14, v12, Lcom/google/android/apps/gmm/directions/cl;->a:Landroid/widget/TextView;

    .line 726
    invoke-virtual {v14}, Landroid/widget/TextView;->getTextSize()F

    move-result v14

    float-to-int v14, v14

    .line 724
    new-instance v15, Landroid/text/SpannableStringBuilder;

    invoke-direct {v15}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz v13, :cond_3

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v15}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v16

    const/16 v17, 0x20

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v15}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v17

    new-instance v18, Landroid/graphics/drawable/ColorDrawable;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/4 v13, 0x0

    const/16 v19, 0x0

    int-to-double v0, v14

    move-wide/from16 v20, v0

    const-wide v22, 0x3fdaaaaaaaaaaaabL    # 0.4166666666666667

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v13, v1, v2, v14}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    new-instance v13, Landroid/text/style/ImageSpan;

    const/4 v14, 0x1

    move-object/from16 v0, v18

    invoke-direct {v13, v0, v14}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    const/16 v14, 0x11

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v13, v0, v1, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const/16 v13, 0x20

    invoke-virtual {v15, v13}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_3
    if-eqz v8, :cond_14

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v15}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v13

    const/16 v14, 0x20

    invoke-virtual {v15, v14}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v15, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v5, 0x20

    invoke-virtual {v15, v5}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v15}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    new-instance v14, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v14, v8}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/16 v8, 0x11

    invoke-virtual {v15, v14, v13, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    if-eqz v9, :cond_4

    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v9, 0x11

    invoke-virtual {v15, v8, v13, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    :goto_1
    const/16 v5, 0x20

    invoke-virtual {v15, v5}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_15

    :cond_5
    const/4 v5, 0x1

    :goto_2
    if-nez v5, :cond_6

    invoke-virtual {v15, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v5, 0x20

    invoke-virtual {v15, v5}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_6
    invoke-virtual {v15, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 727
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->a:Landroid/widget/TextView;

    if-eqz v5, :cond_7

    if-eqz v15, :cond_16

    invoke-virtual {v5, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 741
    :cond_7
    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 742
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->l:Lcom/google/maps/g/a/hm;

    sget-object v7, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v5, v7, :cond_1a

    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_1b

    .line 744
    iget v5, v11, Lcom/google/android/apps/gmm/directions/db;->m:I

    add-int/lit8 v5, v5, 0x1

    .line 746
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v5

    .line 745
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    const/16 v5, 0x20

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 759
    :cond_8
    :goto_5
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->h:Lcom/google/maps/g/a/be;

    if-eqz v5, :cond_9

    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->h:Lcom/google/maps/g/a/be;

    iget v5, v5, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v5, v5, 0x1

    const/4 v7, 0x1

    if-ne v5, v7, :cond_1c

    const/4 v5, 0x1

    :goto_6
    if-eqz v5, :cond_9

    .line 760
    const/16 v5, 0x28

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 761
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->h:Lcom/google/maps/g/a/be;

    iget v5, v5, Lcom/google/maps/g/a/be;->b:I

    .line 762
    sget-object v7, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    .line 763
    invoke-static {v4, v5, v7}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v4

    .line 762
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 764
    const/16 v4, 0x29

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 767
    :cond_9
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->b:Landroid/widget/TextView;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_a

    if-eqz v5, :cond_1d

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 768
    :cond_a
    :goto_7
    iget v4, v11, Lcom/google/android/apps/gmm/directions/db;->m:I

    if-lez v4, :cond_b

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_b

    .line 769
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->b:Landroid/widget/TextView;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 770
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v7, Lcom/google/android/apps/gmm/l;->U:I

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 771
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-boolean v4, v11, Lcom/google/android/apps/gmm/directions/db;->q:Z

    if-eqz v4, :cond_1e

    sget v4, Lcom/google/android/apps/gmm/l;->ay:I

    :goto_8
    invoke-virtual {v8, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v9, v13

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v9, v13

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 769
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 776
    :cond_b
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->c:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    iget v5, v11, Lcom/google/android/apps/gmm/directions/db;->a:I

    iput v5, v4, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->invalidate()V

    .line 777
    const/4 v4, 0x0

    .line 778
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->l:Lcom/google/maps/g/a/hm;

    sget-object v6, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v5, v6, :cond_c

    .line 779
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->a(I)Lcom/google/android/apps/gmm/directions/views/a;

    move-result-object v4

    .line 781
    :cond_c
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->c:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    move/from16 v0, p1

    invoke-virtual {v5, v0, v4}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->setDottedLineController(ILcom/google/android/apps/gmm/directions/views/a;)V

    .line 784
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->f:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 785
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->e:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setClickable(Z)V

    .line 786
    iget v4, v11, Lcom/google/android/apps/gmm/directions/db;->m:I

    if-lez v4, :cond_d

    .line 787
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->f:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 788
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->f:Landroid/widget/ImageView;

    iget-boolean v4, v11, Lcom/google/android/apps/gmm/directions/db;->q:Z

    if-eqz v4, :cond_1f

    sget v4, Lcom/google/android/apps/gmm/f;->gz:I

    :goto_9
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 791
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->e:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/gmm/directions/bx;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v11}, Lcom/google/android/apps/gmm/directions/bx;-><init>(Lcom/google/android/apps/gmm/directions/bt;Lcom/google/android/apps/gmm/directions/db;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 801
    :cond_d
    const/4 v4, 0x0

    .line 802
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->j:Ljava/lang/String;

    if-eqz v5, :cond_e

    .line 803
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v8

    .line 804
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v4

    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->j:Ljava/lang/String;

    sget-object v6, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    .line 807
    invoke-static {v8}, Lcom/google/android/apps/gmm/map/i/b/a;->a(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    .line 808
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/4 v9, 0x0

    .line 804
    invoke-interface/range {v4 .. v9}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/lang/String;Lcom/google/r/b/a/acy;FLandroid/content/res/Resources;Lcom/google/android/apps/gmm/map/i/a/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 811
    :cond_e
    if-eqz v4, :cond_20

    .line 812
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->d:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 813
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->d:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 818
    :goto_a
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->h:Landroid/view/View;

    iget-boolean v4, v11, Lcom/google/android/apps/gmm/directions/db;->k:Z

    if-eqz v5, :cond_f

    if-eqz v4, :cond_21

    const/4 v4, 0x0

    :goto_b
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 822
    :cond_f
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->g:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 823
    iget-object v4, v11, Lcom/google/android/apps/gmm/directions/db;->r:Ljava/util/List;

    if-eqz v4, :cond_27

    .line 824
    iget-object v4, v11, Lcom/google/android/apps/gmm/directions/db;->r:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_27

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/google/android/apps/gmm/directions/cy;

    .line 825
    iget-object v9, v12, Lcom/google/android/apps/gmm/directions/cl;->g:Landroid/view/ViewGroup;

    iget-object v11, v12, Lcom/google/android/apps/gmm/directions/cl;->g:Landroid/view/ViewGroup;

    .line 826
    iget-object v13, v6, Lcom/google/android/apps/gmm/directions/cy;->a:Lcom/google/maps/g/a/da;

    sget v5, Lcom/google/android/apps/gmm/h;->A:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    sget v4, Lcom/google/android/apps/gmm/g;->bW:I

    invoke-virtual {v14, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    sget v5, Lcom/google/android/apps/gmm/f;->fO:I

    iget v7, v13, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v7, v7, 0x1

    const/4 v15, 0x1

    if-ne v7, v15, :cond_22

    const/4 v7, 0x1

    :goto_d
    if-eqz v7, :cond_11

    iget v5, v13, Lcom/google/maps/g/a/da;->b:I

    invoke-static {v5}, Lcom/google/maps/g/a/dd;->a(I)Lcom/google/maps/g/a/dd;

    move-result-object v5

    if-nez v5, :cond_10

    sget-object v5, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    :cond_10
    invoke-static {v5}, Lcom/google/android/apps/gmm/map/r/a/n;->a(Lcom/google/maps/g/a/dd;)I

    move-result v5

    :cond_11
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget v4, v13, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_23

    const/4 v4, 0x1

    :goto_e
    if-eqz v4, :cond_12

    sget v4, Lcom/google/android/apps/gmm/g;->dD:I

    invoke-virtual {v14, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget v5, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v5, v7}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v13}, Lcom/google/maps/g/a/da;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_12
    sget v4, Lcom/google/android/apps/gmm/g;->dZ:I

    invoke-virtual {v14, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    sget v4, Lcom/google/android/apps/gmm/g;->dY:I

    invoke-virtual {v14, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    sget v5, Lcom/google/android/apps/gmm/g;->ae:I

    invoke-virtual {v14, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    sget-object v7, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/base/h/c;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v15, v7}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget v7, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-interface {v7, v11}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget v7, v13, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v7, v7, 0x10

    const/16 v11, 0x10

    if-ne v7, v11, :cond_24

    const/4 v7, 0x1

    :goto_f
    if-eqz v7, :cond_26

    invoke-virtual {v13}, Lcom/google/maps/g/a/da;->h()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v7, v6, Lcom/google/android/apps/gmm/directions/cy;->b:Z

    if-eqz v7, :cond_25

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    sget v5, Lcom/google/android/apps/gmm/f;->gz:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_10
    new-instance v4, Lcom/google/android/apps/gmm/directions/bw;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v6}, Lcom/google/android/apps/gmm/directions/bw;-><init>(Lcom/google/android/apps/gmm/directions/bt;Lcom/google/android/apps/gmm/directions/cy;)V

    invoke-virtual {v15, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 825
    :goto_11
    invoke-virtual {v9, v14}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_c

    .line 709
    :cond_13
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/directions/cl;

    move-object v12, v4

    move-object/from16 v10, p2

    goto/16 :goto_0

    .line 724
    :cond_14
    invoke-virtual {v15, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1

    :cond_15
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 727
    :cond_16
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 729
    :cond_17
    sget v5, Lcom/google/android/apps/gmm/l;->ob:I

    .line 730
    iget-object v6, v11, Lcom/google/android/apps/gmm/directions/db;->l:Lcom/google/maps/g/a/hm;

    sget-object v7, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v6, v7, :cond_18

    .line 731
    sget v5, Lcom/google/android/apps/gmm/l;->nG:I

    .line 733
    :cond_18
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 734
    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 735
    iget-object v5, v12, Lcom/google/android/apps/gmm/directions/cl;->a:Landroid/widget/TextView;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_7

    if-eqz v6, :cond_19

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_19
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 742
    :cond_1a
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 748
    :cond_1b
    iget-object v5, v11, Lcom/google/android/apps/gmm/directions/db;->i:Lcom/google/maps/g/a/ai;

    if-eqz v5, :cond_8

    .line 750
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v5

    iget-object v7, v11, Lcom/google/android/apps/gmm/directions/db;->i:Lcom/google/maps/g/a/ai;

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v5, v7, v8, v9}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/maps/g/a/ai;ZZ)Ljava/lang/String;

    move-result-object v5

    .line 752
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    const/16 v5, 0x20

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 759
    :cond_1c
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 767
    :cond_1d
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 771
    :cond_1e
    sget v4, Lcom/google/android/apps/gmm/l;->ax:I

    goto/16 :goto_8

    .line 788
    :cond_1f
    sget v4, Lcom/google/android/apps/gmm/f;->gA:I

    goto/16 :goto_9

    .line 815
    :cond_20
    iget-object v4, v12, Lcom/google/android/apps/gmm/directions/cl;->d:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 818
    :cond_21
    const/16 v4, 0x8

    goto/16 :goto_b

    .line 826
    :cond_22
    const/4 v7, 0x0

    goto/16 :goto_d

    :cond_23
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_24
    const/4 v7, 0x0

    goto/16 :goto_f

    :cond_25
    const/16 v7, 0x8

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    sget v5, Lcom/google/android/apps/gmm/f;->gA:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_10

    :cond_26
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v5, 0x0

    invoke-virtual {v15, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v5, 0x0

    invoke-virtual {v15, v5}, Landroid/view/View;->setClickable(Z)V

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_11

    .line 830
    :cond_27
    return-object v10
.end method

.method private a(I)Lcom/google/android/apps/gmm/directions/views/a;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 342
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    .line 343
    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->g:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v1, :cond_1

    move-object v0, v3

    .line 371
    :cond_0
    :goto_0
    return-object v0

    .line 346
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->c:Lcom/google/android/apps/gmm/directions/da;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->f:Lcom/google/android/apps/gmm/directions/da;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->h:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v1, :cond_3

    :cond_2
    move v0, v4

    :goto_1
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v0, v5

    goto :goto_1

    .line 349
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->f:[Lcom/google/android/apps/gmm/directions/views/a;

    aget-object v0, v0, p1

    if-eqz v0, :cond_5

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->f:[Lcom/google/android/apps/gmm/directions/views/a;

    aget-object v0, v0, p1

    goto :goto_0

    :cond_5
    move v1, p1

    .line 353
    :goto_2
    if-ltz v1, :cond_9

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    sget-object v6, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v6, :cond_8

    .line 354
    :goto_3
    add-int/lit8 v6, p1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getCount()I

    move-result v7

    :goto_4
    if-ge v6, v7, :cond_6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    sget-object v8, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v8, :cond_a

    move v2, v6

    .line 356
    :cond_6
    if-ltz v1, :cond_7

    if-gez v2, :cond_b

    :cond_7
    move-object v0, v3

    .line 357
    goto :goto_0

    .line 353
    :cond_8
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_3

    .line 354
    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 360
    :cond_b
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v3, :cond_c

    move v0, v4

    :goto_5
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_c
    move v0, v5

    goto :goto_5

    .line 361
    :cond_d
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v3, :cond_e

    move v0, v4

    :goto_6
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_e
    move v0, v5

    goto :goto_6

    .line 363
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/e;->ac:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 365
    new-instance v0, Lcom/google/android/apps/gmm/directions/views/a;

    sub-int v4, v2, v1

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/apps/gmm/directions/views/a;-><init>(III)V

    .line 368
    :goto_7
    if-ge v1, v2, :cond_0

    .line 369
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/bt;->f:[Lcom/google/android/apps/gmm/directions/views/a;

    aput-object v0, v3, v1

    .line 368
    add-int/lit8 v1, v1, 0x1

    goto :goto_7
.end method

.method private b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 913
    if-nez p2, :cond_4

    .line 915
    sget v3, Lcom/google/android/apps/gmm/h;->w:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 917
    new-instance v9, Lcom/google/android/apps/gmm/directions/cf;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/directions/cf;-><init>()V

    .line 918
    sget v2, Lcom/google/android/apps/gmm/g;->i:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cf;->a:Landroid/widget/TextView;

    .line 919
    sget v2, Lcom/google/android/apps/gmm/g;->Y:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cf;->b:Landroid/view/View;

    .line 920
    sget v2, Lcom/google/android/apps/gmm/g;->cZ:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cf;->c:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    .line 921
    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cf;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 922
    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cf;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 924
    iget-object v10, v9, Lcom/google/android/apps/gmm/directions/cf;->b:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    .line 925
    sget v3, Lcom/google/android/apps/gmm/d;->L:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    const-wide/16 v6, 0x0

    .line 926
    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v5

    if-eqz v5, :cond_0

    double-to-int v6, v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v8

    if-eqz v8, :cond_1

    double-to-int v7, v6

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v5, v6}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v12, 0x0

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v7

    if-eqz v7, :cond_2

    double-to-int v8, v12

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v8, v11

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v7, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v12, 0x0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v8

    if-eqz v8, :cond_3

    double-to-int v11, v12

    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v8, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v7, v8}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    .line 925
    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/base/k/c;->a(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    .line 926
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 924
    invoke-virtual {v10, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 929
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v9

    move-object/from16 v3, p2

    :goto_4
    move-object v2, v3

    .line 934
    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    sget v6, Lcom/google/android/apps/gmm/g;->ej:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 936
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cr;

    .line 937
    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/cf;->a:Landroid/widget/TextView;

    iget-object v6, v2, Lcom/google/android/apps/gmm/directions/cr;->b:Lcom/google/maps/g/a/da;

    invoke-virtual {v6}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 938
    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/cf;->c:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    iget v2, v2, Lcom/google/android/apps/gmm/directions/cr;->a:I

    iput v2, v5, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->invalidate()V

    .line 939
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->a(I)Lcom/google/android/apps/gmm/directions/views/a;

    move-result-object v2

    .line 940
    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/cf;->c:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    move/from16 v0, p1

    invoke-virtual {v4, v0, v2}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->setDottedLineController(ILcom/google/android/apps/gmm/directions/views/a;)V

    .line 942
    return-object v3

    .line 926
    :cond_0
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v5, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v5}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v5, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_1
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v8, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v8}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v6, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_2
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v7, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v7}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v8, v11

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v7, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_3
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v8, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v8}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v8, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 931
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cf;

    move-object v4, v2

    move-object/from16 v3, p2

    goto/16 :goto_4
.end method

.method private c(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 947
    if-nez p2, :cond_6

    .line 948
    sget v3, Lcom/google/android/apps/gmm/h;->z:I

    .line 949
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 950
    new-instance v9, Lcom/google/android/apps/gmm/directions/ci;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/directions/ci;-><init>()V

    .line 951
    sget v2, Lcom/google/android/apps/gmm/g;->Z:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/ci;->a:Landroid/widget/TextView;

    .line 952
    sget v2, Lcom/google/android/apps/gmm/g;->cZ:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/ci;->b:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    .line 953
    sget v2, Lcom/google/android/apps/gmm/g;->Y:I

    .line 954
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/ci;->c:Landroid/view/ViewGroup;

    .line 955
    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/ci;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 956
    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/ci;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 958
    iget-object v10, v9, Lcom/google/android/apps/gmm/directions/ci;->c:Landroid/view/ViewGroup;

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    .line 959
    sget v3, Lcom/google/android/apps/gmm/d;->L:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    const-wide/16 v6, 0x0

    .line 960
    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v5

    if-eqz v5, :cond_2

    double-to-int v6, v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v8

    if-eqz v8, :cond_3

    double-to-int v7, v6

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v5, v6}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v12, 0x0

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v7

    if-eqz v7, :cond_4

    double-to-int v8, v12

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v8, v11

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v7, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v12, 0x0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v8

    if-eqz v8, :cond_5

    double-to-int v11, v12

    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v8, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v7, v8}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    .line 959
    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/base/k/c;->a(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    .line 960
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 958
    invoke-virtual {v10, v2}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 963
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v9

    move-object/from16 v3, p2

    :goto_4
    move-object v2, v3

    .line 968
    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    sget v6, Lcom/google/android/apps/gmm/g;->ej:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 970
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cx;

    .line 971
    iget-object v6, v4, Lcom/google/android/apps/gmm/directions/ci;->a:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/google/android/apps/gmm/directions/cx;->b:Ljava/lang/String;

    if-nez v5, :cond_0

    const-string v5, ""

    :cond_0
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 972
    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/ci;->b:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    iget v6, v2, Lcom/google/android/apps/gmm/directions/cx;->a:I

    iput v6, v5, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->invalidate()V

    .line 973
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->a(I)Lcom/google/android/apps/gmm/directions/views/a;

    move-result-object v5

    .line 974
    iget-object v6, v4, Lcom/google/android/apps/gmm/directions/ci;->b:Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    move/from16 v0, p1

    invoke-virtual {v6, v0, v5}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->setDottedLineController(ILcom/google/android/apps/gmm/directions/views/a;)V

    .line 977
    iget-object v5, v2, Lcom/google/android/apps/gmm/directions/cx;->o:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v5, :cond_1

    .line 978
    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/ci;->c:Landroid/view/ViewGroup;

    new-instance v5, Lcom/google/android/apps/gmm/directions/by;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v2}, Lcom/google/android/apps/gmm/directions/by;-><init>(Lcom/google/android/apps/gmm/directions/bt;Lcom/google/android/apps/gmm/directions/cx;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 988
    :cond_1
    return-object v3

    .line 960
    :cond_2
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v5, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v5}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v5, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_3
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v8, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v8}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v6, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_4
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v7, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v7}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v8, v11

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v7, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_5
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v8, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v8}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v8, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 965
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/ci;

    move-object v4, v2

    move-object/from16 v3, p2

    goto/16 :goto_4
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->clear()V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->e:Lcom/google/android/apps/gmm/directions/cm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/cm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    .line 222
    sget-object v1, Lcom/google/android/apps/gmm/directions/cb;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/da;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 242
    const-string v1, "TransitDetailsAdapter"

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x16

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported row type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 229
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/bt;->add(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    move-object v1, v0

    .line 235
    check-cast v1, Lcom/google/android/apps/gmm/directions/cs;

    .line 236
    iget-boolean v1, v1, Lcom/google/android/apps/gmm/directions/cs;->c:Z

    if-eqz v1, :cond_0

    .line 237
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/bt;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 245
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getCount()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/views/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->f:[Lcom/google/android/apps/gmm/directions/views/a;

    .line 246
    return-void

    .line 222
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/cm;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 249
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bt;->e:Lcom/google/android/apps/gmm/directions/cm;

    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->a()V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getCount()I

    move-result v9

    sget v1, Lcom/google/android/apps/gmm/h;->C:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x28

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v4

    move v7, v8

    :goto_0
    if-ge v7, v9, :cond_6

    invoke-virtual {p0, v7}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    if-ne v2, v3, :cond_7

    check-cast v1, Lcom/google/android/apps/gmm/directions/cv;

    sget v2, Lcom/google/android/apps/gmm/g;->n:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/gmm/g;->X:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/bt;->i:Landroid/graphics/Typeface;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/bt;->i:Landroid/graphics/Typeface;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v5, v1, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v10, v1, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    invoke-static {v5, v10}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v5, :cond_3

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    if-eqz v3, :cond_1

    if-eqz v1, :cond_5

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    :goto_4
    invoke-virtual {v0, v8, v8}, Landroid/view/ViewGroup;->measure(II)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    if-le v1, v4, :cond_7

    :goto_5
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v4, v1

    goto :goto_0

    :cond_2
    move-object v5, v6

    goto :goto_1

    :cond_3
    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move-object v1, v6

    goto :goto_3

    :cond_5
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_6
    iput v4, p0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bt;->notifyDataSetChanged()V

    .line 253
    return-void

    :cond_7
    move v1, v4

    goto :goto_5
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/da;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 22

    .prologue
    .line 1049
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cz;

    .line 1050
    sget-object v3, Lcom/google/android/apps/gmm/directions/cb;->a:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/da;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1079
    const-string v3, "TransitDetailsAdapter"

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x16

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unsupported row type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v2, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1080
    const/16 p2, 0x0

    :cond_0
    :goto_0
    return-object p2

    .line 1052
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v12

    if-nez p2, :cond_13

    sget v3, Lcom/google/android/apps/gmm/h;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/gmm/directions/ch;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/ch;-><init>()V

    sget v2, Lcom/google/android/apps/gmm/g;->n:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->a:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->X:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->b:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->dt:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->c:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->Z:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->d:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->cD:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->e:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->cC:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->f:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->cZ:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    sget v2, Lcom/google/android/apps/gmm/g;->Y:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->i:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->a:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-interface {v4, v12}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->i:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->b:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-interface {v4, v12}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->c:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->c:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-interface {v4, v12}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->d:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->d:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-interface {v4, v12}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->f:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->f:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-interface {v4, v12}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->e:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/ch;->e:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-interface {v4, v12}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v8, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v10, v3

    :goto_1
    move-object v2, v8

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    sget v4, Lcom/google/android/apps/gmm/g;->ej:I

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/google/android/apps/gmm/directions/cv;

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    if-eqz v2, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    if-eqz v3, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v9, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v3

    :goto_3
    iget-object v4, v9, Lcom/google/android/apps/gmm/directions/cv;->j:Lcom/google/maps/g/a/be;

    if-nez v4, :cond_16

    const/4 v4, 0x0

    :goto_4
    iget-object v5, v9, Lcom/google/android/apps/gmm/directions/cv;->m:Ljava/lang/String;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_18

    :cond_1
    const/4 v5, 0x1

    :goto_5
    if-nez v5, :cond_19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/gmm/l;->nF:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v13, v9, Lcom/google/android/apps/gmm/directions/cv;->m:Ljava/lang/String;

    aput-object v13, v7, v11

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    :goto_6
    if-eqz v4, :cond_1d

    if-eqz v6, :cond_1d

    const/4 v5, 0x1

    :goto_7
    iget-object v7, v10, Lcom/google/android/apps/gmm/directions/ch;->a:Landroid/widget/TextView;

    if-eqz v7, :cond_2

    if-eqz v2, :cond_1e

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_8
    iget-object v7, v10, Lcom/google/android/apps/gmm/directions/ch;->b:Landroid/widget/TextView;

    if-eqz v7, :cond_3

    if-eqz v3, :cond_1f

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    :goto_9
    iget-object v7, v10, Lcom/google/android/apps/gmm/directions/ch;->e:Landroid/widget/TextView;

    if-eqz v7, :cond_4

    if-eqz v4, :cond_20

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    :goto_a
    iget-object v4, v10, Lcom/google/android/apps/gmm/directions/ch;->d:Landroid/widget/TextView;

    if-eqz v4, :cond_5

    if-eqz v6, :cond_21

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    :goto_b
    iget-object v7, v10, Lcom/google/android/apps/gmm/directions/ch;->c:Landroid/widget/TextView;

    iget-object v4, v9, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    if-nez v4, :cond_6

    const-string v4, ""

    :cond_6
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, v10, Lcom/google/android/apps/gmm/directions/ch;->f:Landroid/widget/TextView;

    if-eqz v5, :cond_22

    const/4 v4, 0x0

    :goto_c
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-direct {v4, v12}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    if-eqz v2, :cond_23

    sget v5, Lcom/google/android/apps/gmm/l;->q:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v2, v7, v11

    invoke-virtual {v12, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_d
    if-eqz v2, :cond_7

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_24

    :cond_7
    move-object v2, v4

    :goto_e
    if-eqz v3, :cond_25

    sget v5, Lcom/google/android/apps/gmm/l;->t:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v3, v7, v11

    invoke-virtual {v12, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_f
    if-eqz v3, :cond_8

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_26

    :cond_8
    :goto_10
    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    if-eqz v3, :cond_9

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_27

    :cond_9
    :goto_11
    if-eqz v6, :cond_a

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_28

    :cond_a
    :goto_12
    if-eqz p3, :cond_b

    iget-object v2, v4, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_b
    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iget v3, v9, Lcom/google/android/apps/gmm/directions/cv;->b:I

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->f:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iget v3, v9, Lcom/google/android/apps/gmm/directions/cv;->c:I

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->g:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iget v3, v9, Lcom/google/android/apps/gmm/directions/cv;->d:I

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->h:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iget v3, v9, Lcom/google/android/apps/gmm/directions/cv;->e:I

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->i:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-lez p1, :cond_c

    iget-object v4, v9, Lcom/google/android/apps/gmm/directions/cv;->f:Lcom/google/maps/g/a/hm;

    sget-object v5, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v4, v5, :cond_c

    add-int/lit8 v2, p1, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/directions/bt;->a(I)Lcom/google/android/apps/gmm/directions/views/a;

    move-result-object v2

    :cond_c
    iget-object v4, v9, Lcom/google/android/apps/gmm/directions/cv;->g:Lcom/google/maps/g/a/hm;

    sget-object v5, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v4, v5, :cond_d

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->a(I)Lcom/google/android/apps/gmm/directions/views/a;

    move-result-object v3

    :cond_d
    iget-object v4, v10, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->j:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v5, :cond_e

    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->j:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)I

    move-result v6

    if-ltz v6, :cond_29

    iget-object v5, v5, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v5, v5, v6

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    const/high16 v6, -0x40800000    # -1.0f

    iput v6, v5, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    const/high16 v6, -0x40800000    # -1.0f

    iput v6, v5, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    :cond_e
    :goto_13
    iput-object v2, v4, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->j:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v2, :cond_f

    move/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/apps/gmm/directions/views/a;->a(ILcom/google/android/apps/gmm/directions/views/BaseSchematicView;)V

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V

    :cond_f
    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->g:Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->k:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v4, :cond_10

    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->k:Lcom/google/android/apps/gmm/directions/views/a;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;)I

    move-result v5

    if-ltz v5, :cond_2a

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/views/a;->b:[Lcom/google/android/apps/gmm/directions/views/b;

    aget-object v4, v4, v5

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/google/android/apps/gmm/directions/views/b;->a:Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;

    const/high16 v5, -0x40800000    # -1.0f

    iput v5, v4, Lcom/google/android/apps/gmm/directions/views/b;->b:F

    const/high16 v5, -0x40800000    # -1.0f

    iput v5, v4, Lcom/google/android/apps/gmm/directions/views/b;->c:F

    :cond_10
    :goto_14
    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->k:Lcom/google/android/apps/gmm/directions/views/a;

    if-eqz v3, :cond_11

    move/from16 v0, p1

    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/gmm/directions/views/a;->a(ILcom/google/android/apps/gmm/directions/views/BaseSchematicView;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/gmm/directions/views/a;->a(Lcom/google/android/apps/gmm/directions/views/BaseSchematicView;F)V

    :cond_11
    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v13

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v14

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v15

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v16

    sget-object v2, Lcom/google/android/apps/gmm/directions/cb;->b:[I

    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/cv;->a:Lcom/google/android/apps/gmm/directions/cw;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/cw;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    const-string v2, "TransitDetailsAdapter"

    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/cv;->a:Lcom/google/android/apps/gmm/directions/cw;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unsupported node position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_15
    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    move/from16 v0, v16

    invoke-virtual {v2, v13, v15, v14, v0}, Landroid/view/View;->setPadding(IIII)V

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cv;->o:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_12

    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/gmm/directions/bu;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v9}, Lcom/google/android/apps/gmm/directions/bu;-><init>(Lcom/google/android/apps/gmm/directions/bt;Lcom/google/android/apps/gmm/directions/cv;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_12
    move-object/from16 p2, v8

    goto/16 :goto_0

    :cond_13
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/ch;

    move-object v10, v2

    move-object/from16 v8, p2

    goto/16 :goto_1

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_15
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v4, v9, Lcom/google/android/apps/gmm/directions/cv;->j:Lcom/google/maps/g/a/be;

    iget v4, v4, Lcom/google/maps/g/a/be;->b:I

    if-gez v4, :cond_17

    sget v4, Lcom/google/android/apps/gmm/l;->oa:I

    :goto_16
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v11, v9, Lcom/google/android/apps/gmm/directions/cv;->j:Lcom/google/maps/g/a/be;

    invoke-virtual {v11}, Lcom/google/maps/g/a/be;->d()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v6, v7

    invoke-virtual {v5, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    :cond_17
    sget v4, Lcom/google/android/apps/gmm/l;->nZ:I

    goto :goto_16

    :cond_18
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_19
    iget-object v5, v9, Lcom/google/android/apps/gmm/directions/cv;->l:Ljava/lang/String;

    if-eqz v5, :cond_1a

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1b

    :cond_1a
    const/4 v5, 0x1

    :goto_17
    if-nez v5, :cond_1c

    iget-object v5, v9, Lcom/google/android/apps/gmm/directions/cv;->l:Ljava/lang/String;

    move-object v6, v5

    goto/16 :goto_6

    :cond_1b
    const/4 v5, 0x0

    goto :goto_17

    :cond_1c
    const/4 v5, 0x0

    move-object v6, v5

    goto/16 :goto_6

    :cond_1d
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_1e
    const/16 v11, 0x8

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    :cond_1f
    const/16 v11, 0x8

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_9

    :cond_20
    const/16 v4, 0x8

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    :cond_21
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_b

    :cond_22
    const/16 v4, 0x8

    goto/16 :goto_c

    :cond_23
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_24
    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    move-object v2, v4

    goto/16 :goto_e

    :cond_25
    const/4 v3, 0x0

    goto/16 :goto_f

    :cond_26
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_10

    :cond_27
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_11

    :cond_28
    invoke-virtual {v2, v6}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_12

    :cond_29
    sget-object v5, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    goto/16 :goto_13

    :cond_2a
    sget-object v4, Lcom/google/android/apps/gmm/directions/views/a;->a:Ljava/lang/String;

    goto/16 :goto_14

    :pswitch_1
    iget-object v0, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    move-object/from16 v17, v0

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    sget v3, Lcom/google/android/apps/gmm/d;->L:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    const-wide/16 v6, 0x0

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v5

    if-eqz v5, :cond_2b

    double-to-int v6, v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Landroid/util/TypedValue;->data:I

    :goto_18
    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v6, 0x0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v11

    if-eqz v11, :cond_2c

    double-to-int v7, v6

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v7, v11

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Landroid/util/TypedValue;->data:I

    :goto_19
    invoke-direct {v5, v6}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v18, 0x0

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v7

    if-eqz v7, :cond_2d

    move-wide/from16 v0, v18

    double-to-int v11, v0

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v18, 0xffffff

    and-int v11, v11, v18

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v7, Landroid/util/TypedValue;->data:I

    :goto_1a
    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v11

    if-eqz v11, :cond_2e

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    new-instance v11, Landroid/util/TypedValue;

    invoke-direct {v11}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    iput v0, v11, Landroid/util/TypedValue;->data:I

    :goto_1b
    invoke-direct {v7, v11}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/base/k/c;->a(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-interface {v2, v12}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_15

    :cond_2b
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v18

    sget-object v5, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v5}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v5, Landroid/util/TypedValue;->data:I

    goto/16 :goto_18

    :cond_2c
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v18

    sget-object v11, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v11}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v7, v11

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v6, Landroid/util/TypedValue;->data:I

    goto/16 :goto_19

    :cond_2d
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v7, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v7}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v18, 0xffffff

    and-int v11, v11, v18

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v7, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1a

    :cond_2e
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v11, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v11}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v18

    new-instance v11, Landroid/util/TypedValue;

    invoke-direct {v11}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x11

    move/from16 v0, v18

    iput v0, v11, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1b

    :pswitch_2
    iget-object v2, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    sget-object v3, Lcom/google/android/apps/gmm/base/h/c;->j:Lcom/google/android/apps/gmm/base/h/c;

    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/base/h/c;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_15

    :pswitch_3
    iget-object v0, v10, Lcom/google/android/apps/gmm/directions/ch;->h:Landroid/view/View;

    move-object/from16 v17, v0

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    sget v3, Lcom/google/android/apps/gmm/d;->L:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    const-wide/16 v6, 0x0

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v5

    if-eqz v5, :cond_2f

    double-to-int v6, v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Landroid/util/TypedValue;->data:I

    :goto_1c
    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v11

    if-eqz v11, :cond_30

    double-to-int v7, v6

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v7, v11

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Landroid/util/TypedValue;->data:I

    :goto_1d
    invoke-direct {v5, v6}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v18, 0x0

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v7

    if-eqz v7, :cond_31

    move-wide/from16 v0, v18

    double-to-int v11, v0

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v18, 0xffffff

    and-int v11, v11, v18

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v7, Landroid/util/TypedValue;->data:I

    :goto_1e
    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v18, 0x0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v11

    if-eqz v11, :cond_32

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    new-instance v11, Landroid/util/TypedValue;

    invoke-direct {v11}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    iput v0, v11, Landroid/util/TypedValue;->data:I

    :goto_1f
    invoke-direct {v7, v11}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/base/k/c;->a(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-interface {v2, v12}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_15

    :cond_2f
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v18

    sget-object v5, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v5}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v5, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1c

    :cond_30
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v18

    sget-object v11, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v11}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v7, v11

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v6, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1d

    :cond_31
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v7, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v7}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v18, 0xffffff

    and-int v11, v11, v18

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v7, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1e

    :cond_32
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v11, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v11}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v18

    new-instance v11, Landroid/util/TypedValue;

    invoke-direct {v11}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x11

    move/from16 v0, v18

    iput v0, v11, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1f

    .line 1055
    :pswitch_4
    if-nez p2, :cond_3a

    sget v3, Lcom/google/android/apps/gmm/h;->v:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v9, Lcom/google/android/apps/gmm/directions/cd;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/directions/cd;-><init>()V

    sget v2, Lcom/google/android/apps/gmm/g;->dt:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->a:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->Z:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->b:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->cZ:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->c:Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;

    sget v2, Lcom/google/android/apps/gmm/g;->Y:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->d:Landroid/view/View;

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->b:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/cd;->b:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v10, v9, Lcom/google/android/apps/gmm/directions/cd;->d:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/base/h/c;->g:Lcom/google/android/apps/gmm/base/h/c;

    sget v3, Lcom/google/android/apps/gmm/d;->L:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    const-wide/16 v6, 0x0

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v5

    if-eqz v5, :cond_36

    double-to-int v6, v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Landroid/util/TypedValue;->data:I

    :goto_20
    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v8

    if-eqz v8, :cond_37

    double-to-int v7, v6

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Landroid/util/TypedValue;->data:I

    :goto_21
    invoke-direct {v5, v6}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/16 v12, 0x0

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v7

    if-eqz v7, :cond_38

    double-to-int v8, v12

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v8, v11

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v7, Landroid/util/TypedValue;->data:I

    :goto_22
    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v8

    if-eqz v8, :cond_39

    double-to-int v11, v12

    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x1

    iput v11, v8, Landroid/util/TypedValue;->data:I

    :goto_23
    invoke-direct {v7, v8}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/base/k/c;->a(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v9

    move-object/from16 v3, p2

    :goto_24
    move-object v2, v3

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    sget v6, Lcom/google/android/apps/gmm/g;->ej:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/co;

    iget-object v6, v4, Lcom/google/android/apps/gmm/directions/cd;->a:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/google/android/apps/gmm/directions/co;->k:Ljava/lang/String;

    if-nez v5, :cond_33

    const-string v5, ""

    :cond_33
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/cd;->b:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/google/android/apps/gmm/l;->nD:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v5, :cond_34

    if-eqz v6, :cond_3b

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_34
    :goto_25
    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/cd;->c:Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;

    iget v6, v2, Lcom/google/android/apps/gmm/directions/co;->b:I

    iput v6, v5, Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;->f:I

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;->invalidate()V

    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/cd;->c:Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;

    iget v6, v2, Lcom/google/android/apps/gmm/directions/co;->c:I

    iput v6, v5, Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;->g:I

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/views/BlockTransferSchematicView;->invalidate()V

    iget-object v5, v2, Lcom/google/android/apps/gmm/directions/co;->o:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v5, :cond_35

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/cd;->d:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/gmm/directions/bv;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v2}, Lcom/google/android/apps/gmm/directions/bv;-><init>(Lcom/google/android/apps/gmm/directions/bt;Lcom/google/android/apps/gmm/directions/co;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_35
    move-object/from16 p2, v3

    goto/16 :goto_0

    :cond_36
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v5, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v5}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v5, Landroid/util/TypedValue;->data:I

    goto/16 :goto_20

    :cond_37
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v8, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v8}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v6, Landroid/util/TypedValue;->data:I

    goto/16 :goto_21

    :cond_38
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v7, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v7}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    const v11, 0xffffff

    and-int/2addr v8, v11

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v7, Landroid/util/TypedValue;->data:I

    goto/16 :goto_22

    :cond_39
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v8, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v8}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v11

    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v11, v12

    shl-int/lit8 v11, v11, 0x8

    or-int/lit8 v11, v11, 0x11

    iput v11, v8, Landroid/util/TypedValue;->data:I

    goto/16 :goto_23

    :cond_3a
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cd;

    move-object v4, v2

    move-object/from16 v3, p2

    goto/16 :goto_24

    :cond_3b
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_25

    .line 1058
    :pswitch_5
    invoke-direct/range {p0 .. p3}, Lcom/google/android/apps/gmm/directions/bt;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 1061
    :pswitch_6
    if-nez p2, :cond_3d

    sget v3, Lcom/google/android/apps/gmm/h;->x:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v3, Lcom/google/android/apps/gmm/directions/cg;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/cg;-><init>()V

    sget v2, Lcom/google/android/apps/gmm/g;->du:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/cg;->a:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/gmm/g;->cZ:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/cg;->b:Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/cg;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/cg;->a:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v3

    move-object/from16 v3, p2

    :goto_26
    move-object v2, v3

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/directions/bt;->d:I

    sget v6, Lcom/google/android/apps/gmm/g;->ej:I

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/ct;

    iget-object v6, v4, Lcom/google/android/apps/gmm/directions/cg;->a:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/google/android/apps/gmm/directions/ct;->b:Ljava/lang/String;

    if-nez v5, :cond_3c

    const-string v5, ""

    :cond_3c
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/cg;->b:Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;

    iget v2, v2, Lcom/google/android/apps/gmm/directions/ct;->a:I

    iput v2, v4, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->g:I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->invalidate()V

    move-object/from16 p2, v3

    goto/16 :goto_0

    :cond_3d
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cg;

    move-object v4, v2

    move-object/from16 v3, p2

    goto :goto_26

    .line 1064
    :pswitch_7
    invoke-direct/range {p0 .. p3}, Lcom/google/android/apps/gmm/directions/bt;->c(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 1067
    :pswitch_8
    invoke-direct/range {p0 .. p3}, Lcom/google/android/apps/gmm/directions/bt;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 1070
    :pswitch_9
    if-nez p2, :cond_3f

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v3, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v2, :cond_3e

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3e
    check-cast v2, Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/directions/c/ae;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    move-object/from16 p2, v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/bz;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/directions/bz;-><init>(Lcom/google/android/apps/gmm/directions/bt;)V

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    new-instance v3, Lcom/google/android/apps/gmm/directions/cc;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/cc;-><init>()V

    sget v2, Lcom/google/android/apps/gmm/g;->j:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/cc;->a:Landroid/widget/TextView;

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/cc;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/bt;->h:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/cc;->a:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/cc;->a:Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/base/h/c;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v3

    :goto_27
    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/cc;->a:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/bt;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_3f
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cc;

    goto :goto_27

    .line 1073
    :pswitch_a
    if-nez p2, :cond_41

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v3, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v2, :cond_40

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_40
    check-cast v2, Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/directions/c/af;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/gmm/directions/ca;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/directions/ca;-><init>(Lcom/google/android/apps/gmm/directions/bt;)V

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    new-instance v4, Lcom/google/android/apps/gmm/directions/ce;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/directions/ce;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v4, Lcom/google/android/apps/gmm/directions/ce;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 p2, v3

    move-object v3, v4

    :goto_28
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/gmm/directions/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/cq;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/ce;->a:Landroid/widget/TextView;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/cq;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_41
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/directions/ce;

    move-object v3, v2

    goto :goto_28

    .line 1076
    :pswitch_b
    if-nez p2, :cond_0

    new-instance p2, Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/directions/bt;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x8

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1050
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_9
        :pswitch_4
        :pswitch_a
        :pswitch_b
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1052
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 380
    sget v0, Lcom/google/android/apps/gmm/directions/cm;->a:I

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 1098
    const/4 v0, 0x0

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 1086
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1088
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->c:Lcom/google/android/apps/gmm/directions/cj;

    if-eqz v0, :cond_0

    .line 1089
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bt;->c:Lcom/google/android/apps/gmm/directions/cj;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/cj;->a()V

    .line 1091
    :cond_0
    return-void
.end method
