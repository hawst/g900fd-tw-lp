.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/o/b/g;

.field final b:F

.field final c:F

.field public d:Lcom/google/android/apps/gmm/map/o/b/f;

.field public e:Lcom/google/android/apps/gmm/map/b/a/i;

.field public f:Lcom/google/android/apps/gmm/map/b/a/i;

.field private final g:F

.field private final h:Lcom/google/android/apps/gmm/map/b/a/i;

.field private final i:Lcom/google/android/apps/gmm/map/b/a/i;


# direct methods
.method public constructor <init>(FFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/o/b/g;F)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    .line 59
    iput p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b:F

    .line 60
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->c:F

    .line 61
    iput p5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/i;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b:F

    iget v2, p4, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v2, v5

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    add-float/2addr v2, v3

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    add-float/2addr v2, v3

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    add-float/2addr v2, v3

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    add-float/2addr v2, v3

    mul-float/2addr v2, p5

    add-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->c:F

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v3, v5

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    add-float/2addr v3, v4

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    add-float/2addr v3, v4

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    mul-float/2addr v3, p5

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/i;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/i;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/i;

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b:F

    iget v2, p4, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v2, v5

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    add-float/2addr v2, v3

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    add-float/2addr v2, v3

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    mul-float/2addr v2, p5

    add-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->c:F

    iget v3, p4, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v3, v5

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    add-float/2addr v3, v4

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    add-float/2addr v3, v4

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    add-float/2addr v3, v4

    iget v4, p4, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    add-float/2addr v3, v4

    mul-float/2addr v3, p5

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/i;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->i:Lcom/google/android/apps/gmm/map/b/a/i;

    .line 78
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a(Lcom/google/android/apps/gmm/map/o/b/f;)V

    .line 79
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/o/b/f;)Lcom/google/android/apps/gmm/map/b/a/i;
    .locals 2

    .prologue
    .line 265
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/b;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 275
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 270
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->i:Lcom/google/android/apps/gmm/map/b/a/i;

    goto :goto_0

    .line 273
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/i;

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 181
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    div-float/2addr v0, v4

    mul-float/2addr v0, p3

    add-float/2addr v0, p1

    .line 182
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    div-float/2addr v1, v4

    mul-float/2addr v1, p3

    add-float/2addr v1, p2

    .line 184
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/b;->a:[I

    invoke-virtual {p4}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 201
    :goto_0
    iget v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v2, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object v0, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v0

    return-object v0

    .line 187
    :pswitch_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    mul-float/2addr v2, p3

    add-float/2addr v1, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 191
    :pswitch_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    mul-float/2addr v2, p3

    sub-float/2addr v1, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 194
    :pswitch_2
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    mul-float/2addr v2, p3

    add-float/2addr v0, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 197
    :pswitch_3
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    mul-float/2addr v2, p3

    sub-float/2addr v0, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/o/b/a;)Lcom/google/android/apps/gmm/map/o/b/a;
    .locals 7

    .prologue
    .line 154
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    .line 155
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(Lcom/google/android/apps/gmm/map/o/b/f;)Lcom/google/android/apps/gmm/map/b/a/i;

    move-result-object v6

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    .line 156
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 159
    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v1, v6, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, p3

    sub-float/2addr v0, v1

    .line 160
    iget v1, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v6, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v2, p3

    sub-float/2addr v1, v2

    .line 161
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v6, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v3, p3

    add-float/2addr v2, v3

    .line 162
    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v4, v6, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v4, p3

    add-float/2addr v3, v4

    .line 158
    invoke-virtual {p5, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 164
    return-object p5
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/b/f;)V
    .locals 6

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->n:Lcom/google/b/c/dn;

    invoke-virtual {v0, p1}, Lcom/google/b/c/dn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(Lcom/google/android/apps/gmm/map/o/b/f;)Lcom/google/android/apps/gmm/map/b/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    .line 89
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(Lcom/google/android/apps/gmm/map/o/b/f;)Lcom/google/android/apps/gmm/map/b/a/i;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/b;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/i;

    .line 90
    return-void

    .line 89
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/i;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v1, v1

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    float-to-int v1, v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/i;-><init>(II)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/i;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v1, v1

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    float-to-int v1, v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/i;-><init>(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 218
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 224
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->b(Lcom/google/android/apps/gmm/map/o/b/f;)Lcom/google/android/apps/gmm/map/b/a/i;

    move-result-object v1

    .line 226
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/b;->a:[I

    invoke-virtual {p4}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 255
    :goto_0
    iget v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v0, p3

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, p3

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 256
    iget v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v0, p1

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v0, p2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 258
    return-object p5

    .line 228
    :pswitch_0
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v0, v2, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 233
    :pswitch_1
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v2, v2, 0x2

    neg-int v2, v2

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 238
    :pswitch_2
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v0, v2, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 243
    :pswitch_3
    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v2, v2, 0x2

    neg-int v2, v2

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v1, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto :goto_0

    .line 248
    :pswitch_4
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v4, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto/16 :goto_0

    .line 251
    :pswitch_5
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->g:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v4, p5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    goto/16 :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
