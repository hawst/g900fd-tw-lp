.class public Lcom/google/android/apps/gmm/z/c/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/z/a/b;


# static fields
.field static final b:J


# instance fields
.field final c:Lcom/google/android/apps/gmm/base/a;

.field public final d:Lcom/google/android/apps/gmm/z/h;

.field e:Lcom/google/android/apps/gmm/z/o;

.field final f:Lcom/google/android/apps/gmm/z/c/b;

.field volatile g:Z

.field private final h:Lcom/google/android/apps/gmm/z/c/j;

.field private final i:Lcom/google/android/apps/gmm/z/f;

.field private final j:Landroid/content/SharedPreferences;

.field private final k:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final l:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private n:J

.field private o:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/r/b/a/tf;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 82
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/z/c/f;->b:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 3

    .prologue
    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/z/f;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/f;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    new-instance v1, Lcom/google/android/apps/gmm/z/h;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/z/h;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    new-instance v2, Lcom/google/android/apps/gmm/z/c/b;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/z/c/b;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/z/c/f;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/z/f;Lcom/google/android/apps/gmm/z/h;Lcom/google/android/apps/gmm/z/c/b;)V

    .line 155
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/z/f;Lcom/google/android/apps/gmm/z/h;Lcom/google/android/apps/gmm/z/c/b;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/z/c/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/z/c/j;-><init>(Lcom/google/android/apps/gmm/z/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->h:Lcom/google/android/apps/gmm/z/c/j;

    .line 120
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/z/c/f;->g:Z

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->m:Ljava/util/List;

    .line 161
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 162
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/c/f;->i:Lcom/google/android/apps/gmm/z/f;

    .line 163
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/c/f;->d:Lcom/google/android/apps/gmm/z/h;

    .line 164
    iput-object p4, p0, Lcom/google/android/apps/gmm/z/c/f;->f:Lcom/google/android/apps/gmm/z/c/b;

    .line 166
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ue3Preference"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->j:Landroid/content/SharedPreferences;

    .line 168
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->j:Landroid/content/SharedPreferences;

    const-string v2, "activationId"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 169
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->j:Landroid/content/SharedPreferences;

    const-string v2, "sequenceId"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->j:Landroid/content/SharedPreferences;

    const-string v1, "previousClientEventId"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->q:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->j:Landroid/content/SharedPreferences;

    const-string v1, "baseEventId"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->r:Ljava/lang/String;

    .line 172
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v1, 0x14

    const-string v2, "prefetch upgrades"

    .line 173
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->o:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 174
    return-void
.end method

.method private varargs declared-synchronized a(Z[Lcom/google/android/apps/gmm/z/b/a;)V
    .locals 8

    .prologue
    .line 579
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->m:Ljava/util/List;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 580
    if-nez p1, :cond_1

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/z/c/f;->n:J

    .line 582
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget v5, v5, Lcom/google/r/b/a/jn;->b:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 602
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 587
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->f:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->m:Ljava/util/List;

    .line 592
    iget-object v2, v0, Lcom/google/android/apps/gmm/z/c/b;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/c/d;->c()V

    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/z/c/e;

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/c/b;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/z/c/k;->a(Lcom/google/android/apps/gmm/login/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/apps/gmm/z/c/e;-><init>(Lcom/google/android/apps/gmm/z/c/b;Ljava/util/List;Ljava/lang/String;)V

    .line 593
    invoke-static {}, Lcom/google/r/b/a/api;->newBuilder()Lcom/google/r/b/a/apk;

    move-result-object v1

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/a;

    .line 595
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/a;->d()Lcom/google/r/b/a/apd;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 595
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Lcom/google/r/b/a/apk;->c()V

    iget-object v4, v1, Lcom/google/r/b/a/apk;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    invoke-direct {v5}, Lcom/google/n/ao;-><init>()V

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/n/ao;->d:Z

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 597
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v4

    iget v0, v1, Lcom/google/r/b/a/apk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/r/b/a/apk;->a:I

    iput-wide v4, v1, Lcom/google/r/b/a/apk;->c:J

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    .line 600
    invoke-virtual {v1}, Lcom/google/r/b/a/apk;->g()Lcom/google/n/t;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 599
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/z/c/f;->n:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 643
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 644
    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_6

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_3
    :goto_2
    return v0

    :cond_4
    move v2, v1

    .line 643
    goto :goto_0

    :cond_5
    move v2, v1

    .line 644
    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private b(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 280
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/z/o;->b(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_0

    move-object p2, v0

    .line 287
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/z/b/l;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid Ue3LoggingCommonParams: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/String;)V

    .line 291
    const/4 v0, 0x0

    .line 298
    :goto_0
    return-object v0

    .line 294
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/z/b/a;

    new-instance v1, Lcom/google/android/apps/gmm/z/l;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/l;-><init>()V

    .line 295
    iput-object p1, v1, Lcom/google/android/apps/gmm/z/l;->a:Lcom/google/android/apps/gmm/z/b/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 296
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/l;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 297
    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/z/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/l;

    move-result-object v1

    .line 298
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/l;->a()Lcom/google/android/apps/gmm/z/k;

    move-result-object v1

    aput-object v1, v0, v3

    .line 294
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    move-result-object v0

    .line 298
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 3

    .prologue
    .line 570
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->j:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sequenceId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 571
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "activationId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 572
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "previousClientEventId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->q:Ljava/lang/String;

    .line 573
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "baseEventId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->r:Ljava/lang/String;

    .line 574
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 575
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 576
    monitor-exit p0

    return-void

    .line 570
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 660
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 661
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget-boolean v0, v0, Lcom/google/r/b/a/jn;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 665
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 666
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget-boolean v0, v0, Lcom/google/r/b/a/jn;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/z/c/f;->b(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/av;)Ljava/lang/String;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 381
    invoke-virtual {p0, p1, v0, v0}, Lcom/google/android/apps/gmm/z/c/f;->a(Lcom/google/r/b/a/av;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/av;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 387
    invoke-static {}, Lcom/google/b/f/b/a/e;->newBuilder()Lcom/google/b/f/b/a/g;

    move-result-object v0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/b/f/b/a/g;->a:I

    iget v1, p1, Lcom/google/r/b/a/av;->v:I

    iput v1, v0, Lcom/google/b/f/b/a/g;->b:I

    .line 388
    invoke-static {p2, p3}, Lcom/google/android/apps/gmm/z/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->p:Ljava/lang/String;

    invoke-static {v1, p3}, Lcom/google/android/apps/gmm/z/c/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 389
    invoke-static {}, Lcom/google/b/f/b/a/p;->newBuilder()Lcom/google/b/f/b/a/r;

    move-result-object v1

    .line 390
    if-eqz p2, :cond_2

    .line 391
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/b/f/b/a/r;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/b/f/b/a/r;->a:I

    iput-object p2, v1, Lcom/google/b/f/b/a/r;->b:Ljava/lang/Object;

    .line 393
    :cond_2
    if-eqz p3, :cond_4

    .line 394
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v2, v1, Lcom/google/b/f/b/a/r;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/b/f/b/a/r;->a:I

    iput-object p3, v1, Lcom/google/b/f/b/a/r;->c:Ljava/lang/Object;

    .line 396
    :cond_4
    iget-object v2, v0, Lcom/google/b/f/b/a/g;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/b/f/b/a/r;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/b/f/b/a/g;->a:I

    .line 397
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/c/f;->p:Ljava/lang/String;

    .line 399
    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/z/b/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 400
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-virtual {v0}, Lcom/google/b/f/b/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/e;)Lcom/google/android/apps/gmm/z/b/e;

    move-result-object v1

    .line 402
    sget-object v0, Lcom/google/android/apps/gmm/z/c/i;->a:[I

    invoke-virtual {p1}, Lcom/google/r/b/a/av;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 423
    :goto_0
    new-array v0, v7, [Lcom/google/android/apps/gmm/z/b/a;

    aput-object v1, v0, v6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 407
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/c/h;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/z/c/h;-><init>(Lcom/google/android/apps/gmm/z/c/f;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    sget-wide v4, Lcom/google/android/apps/gmm/z/c/f;->b:J

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    goto :goto_0

    .line 418
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/content/Context;)Lcom/google/b/f/b/a/w;

    move-result-object v2

    iget-object v0, v1, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    iget-object v0, v0, Lcom/google/b/f/b/a/bc;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/aa;->d()Lcom/google/b/f/b/a/aa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aa;

    invoke-static {v0}, Lcom/google/b/f/b/a/aa;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/ac;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/ac;

    iget-object v2, v1, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/ac;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aa;

    invoke-virtual {v2, v0}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/bc;

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->h:Lcom/google/android/apps/gmm/z/c/j;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget v3, v3, Lcom/google/r/b/a/jn;->c:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/z/c/j;->a(J)V

    goto :goto_0

    .line 402
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/google/r/b/a/gb;Lcom/google/r/b/a/av;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 429
    invoke-static {}, Lcom/google/b/f/b/a/h;->newBuilder()Lcom/google/b/f/b/a/j;

    move-result-object v3

    .line 430
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, v3, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/b/f/b/a/j;->a:I

    iput-object p1, v3, Lcom/google/b/f/b/a/j;->b:Ljava/lang/Object;

    .line 431
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v0, v3, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v3, Lcom/google/b/f/b/a/j;->a:I

    iget v0, p2, Lcom/google/r/b/a/gb;->h:I

    iput v0, v3, Lcom/google/b/f/b/a/j;->d:I

    .line 432
    iget v0, v3, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v3, Lcom/google/b/f/b/a/j;->a:I

    iput-boolean p5, v3, Lcom/google/b/f/b/a/j;->e:Z

    .line 433
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    :goto_0
    if-nez v0, :cond_5

    .line 434
    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v0, v1

    .line 433
    goto :goto_0

    .line 434
    :cond_4
    iget v0, v3, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lcom/google/b/f/b/a/j;->a:I

    iput-object p4, v3, Lcom/google/b/f/b/a/j;->c:Ljava/lang/Object;

    .line 436
    :cond_5
    invoke-static {}, Lcom/google/b/f/b/a/e;->newBuilder()Lcom/google/b/f/b/a/g;

    move-result-object v0

    .line 437
    if-nez p3, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v4, v0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/b/f/b/a/g;->a:I

    iget v4, p3, Lcom/google/r/b/a/av;->v:I

    iput v4, v0, Lcom/google/b/f/b/a/g;->b:I

    .line 438
    iget-object v4, v0, Lcom/google/b/f/b/a/g;->c:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/b/f/b/a/j;->g()Lcom/google/n/t;

    move-result-object v3

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v3, 0x0

    iput-object v3, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    iget v3, v0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/b/f/b/a/g;->a:I

    .line 439
    new-array v2, v2, [Lcom/google/android/apps/gmm/z/b/a;

    new-instance v3, Lcom/google/android/apps/gmm/z/b/e;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-virtual {v0}, Lcom/google/b/f/b/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/e;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/e;)Lcom/google/android/apps/gmm/z/b/e;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    move-result-object v0

    .line 440
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final varargs a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 505
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 506
    array-length v10, p1

    move v8, v6

    :goto_0
    if-ge v8, v10, :cond_c

    aget-object v1, p1, v8

    .line 507
    const/4 v2, 0x0

    .line 508
    instance-of v0, v1, Lcom/google/android/apps/gmm/z/b/e;

    if-eqz v0, :cond_e

    move-object v0, v1

    .line 509
    check-cast v0, Lcom/google/android/apps/gmm/z/b/e;

    .line 513
    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    iget v2, v2, Lcom/google/b/f/b/a/bc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    move v2, v7

    :goto_1
    if-nez v2, :cond_0

    .line 514
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    .line 515
    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    iget v4, v3, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v3, Lcom/google/b/f/b/a/bc;->a:I

    iput v2, v3, Lcom/google/b/f/b/a/bc;->b:I

    .line 518
    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/a;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v2, v7

    :goto_2
    if-eqz v2, :cond_5

    .line 519
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/a;->f:Ljava/lang/String;

    move-object v2, v0

    .line 525
    :goto_3
    iget-object v3, v1, Lcom/google/android/apps/gmm/z/b/a;->e:Lcom/google/maps/g/hy;

    .line 526
    if-eqz v3, :cond_1

    iget v0, v3, Lcom/google/maps/g/hy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_6

    move v0, v7

    :goto_4
    if-nez v0, :cond_2

    .line 527
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/z/b/f;-><init>(Lcom/google/maps/g/hy;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/f;->q:Ljava/lang/String;

    .line 528
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 529
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 527
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/a;->e:Lcom/google/maps/g/hy;

    .line 531
    :cond_2
    iput-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->q:Ljava/lang/String;

    move-object v3, v2

    .line 535
    :goto_5
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->g()Z

    move-result v0

    if-nez v0, :cond_b

    .line 539
    instance-of v0, v1, Lcom/google/android/apps/gmm/z/m;

    if-eqz v0, :cond_b

    .line 540
    sget-object v0, Lcom/google/b/f/t;->cF:Lcom/google/b/f/t;

    .line 541
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/o;->a()Lcom/google/android/apps/gmm/z/b/o;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/o;->a()Lcom/google/android/apps/gmm/z/b/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/b/o;->f_()Lcom/google/b/f/t;

    move-result-object v0

    move-object v4, v0

    :goto_6
    move-object v0, v1

    .line 544
    check-cast v0, Lcom/google/android/apps/gmm/z/m;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/m;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v5, v6

    :goto_7
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/z/g;

    iget v2, v2, Lcom/google/android/apps/gmm/z/g;->c:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v5, v2

    goto :goto_7

    :cond_3
    move v2, v6

    .line 513
    goto/16 :goto_1

    :cond_4
    move v2, v6

    .line 518
    goto :goto_2

    .line 521
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->r:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    iget v0, v0, Lcom/google/b/f/b/a/bc;->b:I

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 522
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/a;->f:Ljava/lang/String;

    move-object v2, v0

    goto/16 :goto_3

    :cond_6
    move v0, v6

    .line 526
    goto :goto_4

    :cond_7
    move v2, v6

    .line 544
    :goto_8
    if-ge v2, v5, :cond_8

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_8
    if-lez v5, :cond_a

    invoke-static {v4}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/t;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/b/f/co;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/g;

    iget v12, v0, Lcom/google/android/apps/gmm/z/g;->c:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/t;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/b/f/co;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v2

    check-cast v2, Lcom/google/b/f/cm;

    invoke-interface {v11, v12, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget v0, v0, Lcom/google/android/apps/gmm/z/g;->c:I

    invoke-virtual {v4}, Lcom/google/b/f/co;->c()V

    iget-object v2, v4, Lcom/google/b/f/co;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_9
    invoke-virtual {v4}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iput-object v11, v1, Lcom/google/android/apps/gmm/z/b/a;->g:Ljava/util/List;

    .line 548
    :cond_b
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 506
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_0

    .line 550
    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->e()V

    .line 551
    invoke-direct {p0, v6, p1}, Lcom/google/android/apps/gmm/z/c/f;->a(Z[Lcom/google/android/apps/gmm/z/b/a;)V

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->d:Lcom/google/android/apps/gmm/z/h;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/h;->a(Ljava/util/List;)V

    .line 566
    return-object v9

    :cond_d
    move-object v4, v0

    goto/16 :goto_6

    :cond_e
    move-object v3, v2

    goto/16 :goto_5
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->i:Lcom/google/android/apps/gmm/z/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/f;->c()Lcom/google/android/apps/gmm/z/m;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_0

    .line 254
    new-array v1, v3, [Lcom/google/android/apps/gmm/z/b/a;

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 256
    :cond_0
    new-array v0, v2, [Lcom/google/android/apps/gmm/z/b/a;

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/gmm/z/c/f;->a(Z[Lcom/google/android/apps/gmm/z/b/a;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/r;->b()V

    .line 258
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 187
    const v0, 0x1020002

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 188
    new-instance v1, Lcom/google/android/apps/gmm/z/o;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/o;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    .line 189
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/z/c/g;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/z/c/g;-><init>(Lcom/google/android/apps/gmm/z/c/f;)V

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 198
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/z/a/a;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 606
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/a/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/a/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->r:Ljava/lang/String;

    .line 609
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 202
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/z/b/l;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid Ue3LoggingCommonParams: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/String;)V

    .line 244
    :cond_1
    :goto_0
    return-void

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->i:Lcom/google/android/apps/gmm/z/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/z/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/g;

    move-result-object v0

    .line 211
    if-eqz v0, :cond_1

    .line 217
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/o;->a()Lcom/google/android/apps/gmm/z/b/o;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    .line 226
    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/z/o;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 225
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/o;->b(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 227
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/t;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/b/f/co;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    .line 228
    new-instance v2, Lcom/google/android/apps/gmm/z/b/r;

    const/4 v3, 0x0

    .line 229
    new-instance v4, Lcom/google/android/apps/gmm/z/b/s;

    sget-object v5, Lcom/google/b/f/aa;->d:Lcom/google/b/f/aa;

    invoke-direct {v4, v1, v5, v3}, Lcom/google/android/apps/gmm/z/b/s;-><init>(Lcom/google/android/apps/gmm/z/b/l;Lcom/google/b/f/aa;Lcom/google/android/apps/gmm/z/b/l;)V

    invoke-static {v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 230
    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/z/b/r;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 231
    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/apps/gmm/z/b/a;->g:Ljava/util/List;

    .line 232
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 233
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v2, p1, v1, v6, v0}, Lcom/google/android/apps/gmm/z/o;->a(Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/String;ILcom/google/b/f/cm;)V

    goto :goto_0

    .line 239
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/f;->i:Lcom/google/android/apps/gmm/z/f;

    .line 240
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/f;->b()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/google/android/apps/gmm/z/g;->c:I

    .line 241
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/t;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/b/f/co;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/f/co;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    .line 239
    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/google/android/apps/gmm/z/o;->a(Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/String;ILcom/google/b/f/cm;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/z/b/o;)V
    .locals 13

    .prologue
    .line 331
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 332
    sget-object v0, Lcom/google/android/apps/gmm/z/b/o;->z:Lcom/google/android/apps/gmm/z/b/o;

    if-ne p1, v0, :cond_1

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/z/b/o;->g()Ljava/lang/Integer;

    move-result-object v0

    .line 338
    if-nez v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p1, v1}, Lcom/google/android/apps/gmm/z/b/o;->a_(I)V

    .line 342
    :cond_2
    new-instance v6, Lcom/google/android/apps/gmm/z/c;

    .line 343
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/t;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/gmm/z/b/o;->f_()Lcom/google/b/f/t;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/gmm/z/c;-><init>(ILcom/google/android/apps/gmm/shared/c/f;[Lcom/google/b/f/t;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->z()Lcom/google/android/apps/gmm/map/t/d;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->z()Lcom/google/android/apps/gmm/map/t/d;

    move-result-object v7

    iget-boolean v1, v7, Lcom/google/android/apps/gmm/map/t/d;->c:Z

    iget v0, v7, Lcom/google/android/apps/gmm/map/t/d;->d:I

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 346
    :goto_1
    if-eqz v0, :cond_8

    .line 347
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v1

    .line 348
    invoke-static {}, Lcom/google/b/f/b/a/cy;->newBuilder()Lcom/google/b/f/b/a/da;

    move-result-object v2

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 345
    :cond_3
    if-eqz v1, :cond_4

    const/16 v0, 0x64

    :cond_4
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    move v12, v1

    move v1, v4

    move-wide v4, v2

    move v2, v12

    :goto_2
    if-ge v2, v0, :cond_6

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/t/d;->a:[J

    aget-wide v8, v3, v2

    add-long/2addr v4, v8

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/t/d;->a:[J

    aget-wide v8, v3, v2

    const-wide/16 v10, 0x32

    cmp-long v3, v8, v10

    if-lez v3, :cond_5

    add-int/lit8 v1, v1, 0x1

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    int-to-long v2, v0

    div-long v2, v4, v2

    invoke-static {}, Lcom/google/b/f/b/a/as;->newBuilder()Lcom/google/b/f/b/a/au;

    move-result-object v4

    iget v5, v4, Lcom/google/b/f/b/a/au;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/b/f/b/a/au;->a:I

    iput v0, v4, Lcom/google/b/f/b/a/au;->b:I

    long-to-int v0, v2

    iget v2, v4, Lcom/google/b/f/b/a/au;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v4, Lcom/google/b/f/b/a/au;->a:I

    iput v0, v4, Lcom/google/b/f/b/a/au;->c:I

    iget v0, v4, Lcom/google/b/f/b/a/au;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v4, Lcom/google/b/f/b/a/au;->a:I

    iput v1, v4, Lcom/google/b/f/b/a/au;->d:I

    const/4 v0, 0x0

    iput v0, v7, Lcom/google/android/apps/gmm/map/t/d;->d:I

    const/4 v0, 0x0

    iput-boolean v0, v7, Lcom/google/android/apps/gmm/map/t/d;->c:Z

    invoke-virtual {v4}, Lcom/google/b/f/b/a/au;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/as;

    goto :goto_1

    .line 348
    :cond_7
    iget-object v3, v2, Lcom/google/b/f/b/a/da;->d:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/da;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v2, Lcom/google/b/f/b/a/da;->a:I

    invoke-virtual {v2}, Lcom/google/b/f/b/a/da;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cy;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/cy;)Lcom/google/b/f/b/a/ac;

    move-result-object v0

    .line 349
    invoke-virtual {v0}, Lcom/google/b/f/b/a/ac;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aa;

    .line 347
    iget-object v1, v6, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/bc;

    .line 353
    :cond_8
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 354
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 356
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->g()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/z/o;->a(Ljava/util/List;Ljava/util/List;)V

    .line 358
    iput-object v5, v6, Lcom/google/android/apps/gmm/z/b/a;->g:Ljava/util/List;

    .line 361
    :cond_9
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v1, 0x0

    aput-object v6, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 363
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/z/o;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 366
    invoke-direct {p0}, Lcom/google/android/apps/gmm/z/c/f;->g()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 367
    const/4 v1, 0x0

    move v3, v1

    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_a

    .line 368
    iget-object v6, p0, Lcom/google/android/apps/gmm/z/c/f;->e:Lcom/google/android/apps/gmm/z/o;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/z/b/l;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/b/f/cm;

    invoke-virtual {v6, v1, v0, v3, v2}, Lcom/google/android/apps/gmm/z/o;->a(Lcom/google/android/apps/gmm/z/b/l;Ljava/lang/String;ILcom/google/b/f/cm;)V

    .line 367
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 372
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->i:Lcom/google/android/apps/gmm/z/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/f;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 374
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->i:Lcom/google/android/apps/gmm/z/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/z/f;->a(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/b/f/b/a/t;)V
    .locals 2

    .prologue
    .line 499
    sget-object v0, Lcom/google/r/b/a/av;->s:Lcom/google/r/b/a/av;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/apps/gmm/z/c/f;->a(Lcom/google/r/b/a/av;Lcom/google/b/f/b/a/cy;Lcom/google/b/f/b/a/t;)V

    .line 501
    return-void
.end method

.method public final a(Lcom/google/maps/a/a;Lcom/google/maps/g/f/a;Lcom/google/maps/g/hy;)V
    .locals 3

    .prologue
    .line 323
    new-instance v0, Lcom/google/android/apps/gmm/z/b/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 324
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/gmm/z/b/q;-><init>(Lcom/google/maps/a/a;Lcom/google/maps/g/f/a;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 325
    iput-object p3, v0, Lcom/google/android/apps/gmm/z/b/a;->e:Lcom/google/maps/g/hy;

    .line 326
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 327
    return-void
.end method

.method a(Lcom/google/r/b/a/av;Lcom/google/b/f/b/a/cy;Lcom/google/b/f/b/a/t;)V
    .locals 6
    .param p2    # Lcom/google/b/f/b/a/cy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/b/f/b/a/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 465
    invoke-static {}, Lcom/google/b/f/b/a/e;->newBuilder()Lcom/google/b/f/b/a/g;

    move-result-object v0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/b/f/b/a/g;->a:I

    iget v1, p1, Lcom/google/r/b/a/av;->v:I

    iput v1, v0, Lcom/google/b/f/b/a/g;->b:I

    .line 466
    if-eqz p3, :cond_1

    .line 467
    invoke-virtual {v0, p3}, Lcom/google/b/f/b/a/g;->a(Lcom/google/b/f/b/a/t;)Lcom/google/b/f/b/a/g;

    .line 469
    :cond_1
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v1

    .line 472
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v2, v1, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/b/f/b/a/ac;->a:I

    iget v2, p1, Lcom/google/r/b/a/av;->v:I

    iput v2, v1, Lcom/google/b/f/b/a/ac;->b:I

    .line 473
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 474
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    .line 473
    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/content/Context;)Lcom/google/b/f/b/a/w;

    move-result-object v2

    .line 480
    if-eqz v2, :cond_3

    .line 481
    invoke-virtual {v1, v2}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/ac;

    .line 483
    :cond_3
    if-eqz p2, :cond_4

    .line 484
    invoke-virtual {v1, p2}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/cy;)Lcom/google/b/f/b/a/ac;

    .line 486
    :cond_4
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/gmm/z/b/e;

    iget-object v5, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-virtual {v0}, Lcom/google/b/f/b/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/e;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/e;)Lcom/google/android/apps/gmm/z/b/e;

    move-result-object v4

    .line 487
    invoke-virtual {v1}, Lcom/google/b/f/b/a/ac;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aa;

    iget-object v1, v4, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/bc;

    aput-object v4, v2, v3

    .line 486
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 488
    return-void
.end method

.method public final a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->o:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->o:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p2, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 309
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/g;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-direct {v2, p1, p2, v3}, Lcom/google/android/apps/gmm/z/b/g;-><init>(Lcom/google/r/b/a/tf;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 445
    invoke-static {}, Lcom/google/b/f/b/a/h;->newBuilder()Lcom/google/b/f/b/a/j;

    move-result-object v0

    .line 446
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/b/f/b/a/j;->a:I

    iput-object p1, v0, Lcom/google/b/f/b/a/j;->b:Ljava/lang/Object;

    .line 447
    iget v1, v0, Lcom/google/b/f/b/a/j;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/b/f/b/a/j;->a:I

    iput-boolean v4, v0, Lcom/google/b/f/b/a/j;->f:Z

    .line 448
    invoke-static {}, Lcom/google/b/f/b/a/e;->newBuilder()Lcom/google/b/f/b/a/g;

    move-result-object v1

    .line 449
    iget-object v2, v1, Lcom/google/b/f/b/a/g;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/j;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/b/f/b/a/g;->a:I

    sget-object v0, Lcom/google/r/b/a/av;->u:Lcom/google/r/b/a/av;

    .line 450
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/b/f/b/a/g;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/b/f/b/a/g;->a:I

    iget v0, v0, Lcom/google/r/b/a/av;->v:I

    iput v0, v1, Lcom/google/b/f/b/a/g;->b:I

    .line 451
    invoke-virtual {v1}, Lcom/google/b/f/b/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/e;

    .line 452
    new-array v1, v4, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/z/b/e;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/z/b/e;->a(Lcom/google/b/f/b/a/e;)Lcom/google/android/apps/gmm/z/b/e;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 453
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 314
    if-nez p1, :cond_0

    .line 315
    const-string v0, "gcsResponse was null"

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 319
    :goto_0
    return-void

    .line 318
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-direct {v2, p1, p2, p3, v3}, Lcom/google/android/apps/gmm/z/b/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/c/f;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 274
    new-instance v0, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v1, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/z/c/f;->b(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->f:Lcom/google/android/apps/gmm/z/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/b;->b:Lcom/google/android/apps/gmm/z/c/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/k;->e()V

    .line 263
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 492
    sget-object v0, Lcom/google/r/b/a/av;->r:Lcom/google/r/b/a/av;

    .line 493
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/app/Activity;)Lcom/google/b/f/b/a/cy;

    move-result-object v1

    const/4 v2, 0x0

    .line 492
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/z/c/f;->a(Lcom/google/r/b/a/av;Lcom/google/b/f/b/a/cy;Lcom/google/b/f/b/a/t;)V

    .line 495
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 620
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/f;->g:Z

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->h:Lcom/google/android/apps/gmm/z/c/j;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget v2, v2, Lcom/google/r/b/a/jn;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/z/c/j;->a(J)V

    .line 622
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 613
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/z/c/f;->g:Z

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 615
    return-void
.end method
