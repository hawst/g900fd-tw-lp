.class public Lcom/google/android/apps/gmm/map/offline/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/a/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/offline/i;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    .line 76
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/offline/i;->b:Ljava/lang/String;

    .line 77
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 100
    if-nez p1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 103
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 106
    check-cast p1, Lcom/google/android/apps/gmm/map/offline/i;

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/offline/i;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/offline/i;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/offline/i;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/offline/i;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/offline/i;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/offline/i;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
