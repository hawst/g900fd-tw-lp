.class public Lcom/google/android/apps/gmm/g/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/location/Location;

.field c:Landroid/location/Location;

.field d:J

.field e:Lcom/google/android/apps/gmm/z/b/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/z/b/h",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/z/b/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/z/b/h",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/android/apps/gmm/z/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/g/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/g/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/z/b/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/b;->e:Lcom/google/android/apps/gmm/z/b/h;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/z/b/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/b;->f:Lcom/google/android/apps/gmm/z/b/h;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/z/b/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/b;->g:Lcom/google/android/apps/gmm/z/b/d;

    .line 41
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/z/b/h;)Lcom/google/b/f/b/a/ak;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/z/b/h",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/google/b/f/b/a/ak;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {}, Lcom/google/b/f/b/a/ak;->newBuilder()Lcom/google/b/f/b/a/am;

    move-result-object v1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v2, v1, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/b/f/b/a/am;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/am;->b:I

    const/16 v0, 0x32

    .line 94
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/b/h;->a(I)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v2, v1, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/b/f/b/a/am;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/am;->c:F

    const/16 v0, 0x4b

    .line 95
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/b/h;->a(I)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v2, v1, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/b/f/b/a/am;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/am;->d:F

    const/16 v0, 0x5a

    .line 96
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/z/b/h;->a(I)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v2, v1, Lcom/google/b/f/b/a/am;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v1, Lcom/google/b/f/b/a/am;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/am;->e:F

    .line 97
    invoke-virtual {v1}, Lcom/google/b/f/b/a/am;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ak;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 62
    .line 63
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "accuracyTracker"

    iget-object v2, p0, Lcom/google/android/apps/gmm/g/b;->e:Lcom/google/android/apps/gmm/z/b/h;

    .line 64
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "locationFixAgeTracker"

    iget-object v2, p0, Lcom/google/android/apps/gmm/g/b;->f:Lcom/google/android/apps/gmm/z/b/h;

    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "locationDistanceTracker"

    iget-object v2, p0, Lcom/google/android/apps/gmm/g/b;->g:Lcom/google/android/apps/gmm/z/b/d;

    .line 66
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
