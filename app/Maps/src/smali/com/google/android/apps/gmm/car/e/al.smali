.class public Lcom/google/android/apps/gmm/car/e/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/navui/h;

.field final b:Lcom/google/android/apps/gmm/car/ad;

.field c:Lcom/google/android/apps/gmm/car/h/l;

.field d:Landroid/view/View;

.field private final e:Lcom/google/android/apps/gmm/car/e/ao;

.field private final f:Lcom/google/android/apps/gmm/car/e/b;

.field private final g:Lcom/google/android/apps/gmm/car/e/i;

.field private final h:Lcom/google/android/apps/gmm/car/ax;

.field private final i:Lcom/google/android/apps/gmm/z/b/j;

.field private final j:Lcom/google/android/apps/gmm/car/h/o;

.field private final k:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/e/ao;Lcom/google/android/apps/gmm/car/e/b;Lcom/google/android/apps/gmm/car/e/i;)V
    .locals 4

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ad:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->i:Lcom/google/android/apps/gmm/z/b/j;

    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/car/e/am;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/am;-><init>(Lcom/google/android/apps/gmm/car/e/al;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->j:Lcom/google/android/apps/gmm/car/h/o;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/car/e/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/an;-><init>(Lcom/google/android/apps/gmm/car/e/al;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->k:Ljava/lang/Object;

    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/navui/h;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/al;->a:Lcom/google/android/apps/gmm/navigation/navui/h;

    .line 45
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 46
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/e/ao;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/e/al;->e:Lcom/google/android/apps/gmm/car/e/ao;

    .line 47
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/car/e/b;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/e/al;->f:Lcom/google/android/apps/gmm/car/e/b;

    .line 48
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p5, Lcom/google/android/apps/gmm/car/e/i;

    iput-object p5, p0, Lcom/google/android/apps/gmm/car/e/al;->g:Lcom/google/android/apps/gmm/car/e/i;

    .line 50
    iget-object v0, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget-object v1, Lcom/google/android/apps/gmm/car/n/b;->e:Lcom/google/android/libraries/curvular/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    .line 52
    new-instance v1, Lcom/google/android/apps/gmm/car/ax;

    iget-object v2, p2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 53
    iget-object v3, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->h:Lcom/google/android/apps/gmm/car/ax;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->g:Lcom/google/android/apps/gmm/car/e/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->h:Lcom/google/android/apps/gmm/car/ax;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/e/i;->a(Lcom/google/android/apps/gmm/car/ax;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->h:Lcom/google/android/apps/gmm/car/ax;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->f:Lcom/google/android/apps/gmm/car/e/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/e/b;->a(Z)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->k:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->c:Lcom/google/android/apps/gmm/car/h/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->c:Lcom/google/android/apps/gmm/car/h/l;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->i:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 10

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/h/d;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->d:Landroid/view/View;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/car/h/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->j:Lcom/google/android/apps/gmm/car/h/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/al;->e:Lcom/google/android/apps/gmm/car/e/ao;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 63
    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->r:Lcom/google/android/apps/gmm/car/k/h;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 64
    iget-boolean v5, v5, Lcom/google/android/apps/gmm/car/ad;->o:Z

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 65
    iget-object v6, v6, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v6

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 67
    iget-object v8, v8, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 68
    iget-object v9, v9, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v9}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/car/h/l;-><init>(Lcom/google/android/apps/gmm/car/h/o;Lcom/google/android/apps/gmm/car/h/p;ZLcom/google/android/apps/gmm/car/k/h;ZLcom/google/android/apps/gmm/map/util/b/a/a;ZLcom/google/android/apps/gmm/shared/c/c/c;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->c:Lcom/google/android/apps/gmm/car/h/l;

    .line 69
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/al;->k:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->f:Lcom/google/android/apps/gmm/car/e/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/e/b;->a(Z)V

    .line 86
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/al;->c:Lcom/google/android/apps/gmm/car/h/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/h/l;->c:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/h/l;->g:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/h/l;->b:Lcom/google/android/apps/gmm/car/h/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/h/l;->f:Lcom/google/android/apps/gmm/car/h/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/h/p;->b(Lcom/google/android/apps/gmm/car/h/q;)V

    .line 91
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/e/al;->c:Lcom/google/android/apps/gmm/car/h/l;

    .line 92
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/e/al;->d:Landroid/view/View;

    .line 93
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
