.class public Lcom/google/android/apps/gmm/map/m/x;
.super Lcom/google/android/apps/gmm/map/m/d;
.source "PG"


# instance fields
.field public e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/m/r;FF)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/m/d;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/x;->e:Z

    .line 71
    return-void
.end method


# virtual methods
.method public final a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")",
            "Lcom/google/android/apps/gmm/map/m/e;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    .line 85
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    .line 86
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/d;

    .line 87
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/m/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    .line 182
    :goto_1
    return-object v0

    .line 85
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 101
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->b:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_1

    .line 105
    :cond_2
    const/4 v1, 0x0

    .line 106
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v3

    :goto_2
    if-ge v2, v3, :cond_3

    .line 107
    invoke-virtual {p3, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    .line 108
    iget v4, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-le v4, v6, :cond_4

    move-object v1, v0

    .line 113
    :cond_3
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    .line 114
    if-nez v1, :cond_5

    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_1

    .line 106
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 120
    :cond_5
    iget v2, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-gt v2, v6, :cond_6

    .line 124
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_1

    .line 127
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/m/x;->e:Z

    if-nez v2, :cond_7

    .line 128
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->c:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_1

    .line 132
    :cond_7
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    const v2, 0x3db2b8c2

    .line 136
    :goto_3
    iget v3, v1, Lcom/google/android/apps/gmm/map/m/k;->b:F

    .line 137
    iget v4, v0, Lcom/google/android/apps/gmm/map/m/k;->b:F

    .line 138
    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/map/m/x;->a(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 139
    cmpg-float v2, v3, v2

    if-gez v2, :cond_9

    .line 144
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_1

    .line 132
    :cond_8
    const v2, 0x3e32b8c2

    goto :goto_3

    .line 148
    :cond_9
    iget v2, p0, Lcom/google/android/apps/gmm/map/m/x;->c:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/x;->d:F

    add-float/2addr v2, v4

    mul-float/2addr v2, v5

    .line 149
    iget v0, v0, Lcom/google/android/apps/gmm/map/m/k;->c:F

    div-float/2addr v0, v2

    .line 150
    const/high16 v4, 0x3f400000    # 0.75f

    cmpg-float v4, v0, v4

    if-gez v4, :cond_a

    .line 155
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_1

    .line 160
    :cond_a
    iget v1, v1, Lcom/google/android/apps/gmm/map/m/k;->c:F

    div-float/2addr v1, v2

    .line 161
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 162
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_c

    .line 163
    div-float v0, v3, v0

    .line 164
    cmpg-float v1, v0, v5

    if-gez v1, :cond_b

    .line 168
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_1

    .line 169
    :cond_b
    const v1, 0x3f666666    # 0.9f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_c

    .line 173
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->b:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_1

    .line 182
    :cond_c
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->c:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_1
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/x;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->e(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method

.method protected final d(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/x;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->f(Lcom/google/android/apps/gmm/map/m/n;)V

    .line 194
    return-void
.end method

.method protected final f(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/x;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->d(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method
