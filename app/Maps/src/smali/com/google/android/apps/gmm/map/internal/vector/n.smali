.class Lcom/google/android/apps/gmm/map/internal/vector/n;
.super Lcom/google/android/apps/gmm/map/internal/vector/m;
.source "PG"


# instance fields
.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field private i:[I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;IIIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 719
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3024

    aput v2, v0, v1

    aput p2, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x3022

    aput v2, v0, v1

    const/4 v1, 0x5

    aput p4, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    aput p5, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    aput p6, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    aput p7, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3038

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/m;-><init>(Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;[I)V

    .line 728
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    .line 729
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->c:I

    .line 730
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->d:I

    .line 731
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->e:I

    .line 732
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->f:I

    .line 733
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->g:I

    .line 734
    iput p7, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->h:I

    .line 735
    return-void
.end method


# virtual methods
.method public final a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 740
    array-length v7, p3

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_7

    aget-object v5, p3, v6

    .line 741
    const/16 v0, 0x3025

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    invoke-interface {p1, p2, v5, v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    aget v0, v0, v1

    .line 743
    :goto_1
    const/16 v2, 0x3026

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    invoke-interface {p1, p2, v5, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    aget v2, v2, v1

    .line 745
    :goto_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->g:I

    if-lt v0, v3, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->h:I

    if-lt v2, v0, :cond_6

    .line 746
    const/16 v0, 0x3024

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    invoke-interface {p1, p2, v5, v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    aget v0, v0, v1

    .line 748
    :goto_3
    const/16 v2, 0x3023

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    invoke-interface {p1, p2, v5, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    aget v2, v2, v1

    .line 750
    :goto_4
    const/16 v3, 0x3022

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    invoke-interface {p1, p2, v5, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    aget v3, v3, v1

    .line 752
    :goto_5
    const/16 v4, 0x3021

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    invoke-interface {p1, p2, v5, v4, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->i:[I

    aget v4, v4, v1

    .line 754
    :goto_6
    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->c:I

    if-ne v0, v8, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->d:I

    if-ne v2, v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->e:I

    if-ne v3, v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/n;->f:I

    if-ne v4, v0, :cond_6

    move-object v0, v5

    .line 760
    :goto_7
    return-object v0

    :cond_0
    move v0, v1

    .line 741
    goto :goto_1

    :cond_1
    move v2, v1

    .line 743
    goto :goto_2

    :cond_2
    move v0, v1

    .line 746
    goto :goto_3

    :cond_3
    move v2, v1

    .line 748
    goto :goto_4

    :cond_4
    move v3, v1

    .line 750
    goto :goto_5

    :cond_5
    move v4, v1

    .line 752
    goto :goto_6

    .line 740
    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 760
    :cond_7
    const/4 v0, 0x0

    goto :goto_7
.end method
