.class public Lcom/google/android/apps/gmm/map/m/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final g:I

.field static final h:I

.field static final i:I


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field final j:Landroid/os/Handler;

.field final k:Lcom/google/android/apps/gmm/map/m/i;

.field l:Landroid/view/GestureDetector$OnDoubleTapListener;

.field m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:Landroid/view/MotionEvent;

.field r:Landroid/view/MotionEvent;

.field s:Z

.field t:F

.field u:F

.field v:F

.field w:F

.field x:Z

.field y:Landroid/view/VelocityTracker;

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 252
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/m/g;->g:I

    .line 253
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/m/g;->h:I

    .line 254
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/m/g;->i:I

    .line 255
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/i;)V
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/m/g;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/i;Landroid/os/Handler;)V

    .line 389
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/i;Landroid/os/Handler;)V
    .locals 5

    .prologue
    .line 404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405
    if-eqz p3, :cond_1

    .line 406
    new-instance v0, Lcom/google/android/apps/gmm/map/m/h;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/gmm/map/m/h;-><init>(Lcom/google/android/apps/gmm/map/m/g;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    .line 410
    :goto_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    .line 411
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    .line 412
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 415
    :cond_0
    if-nez p1, :cond_2

    .line 416
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    .line 417
    :goto_1
    mul-int/2addr v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/g;->z:I

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/m/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/m/h;-><init>(Lcom/google/android/apps/gmm/map/m/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    goto :goto_0

    .line 416
    :cond_2
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    goto :goto_1

    .line 419
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->x:Z

    if-nez p1, :cond_4

    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    shl-int/lit8 v1, v0, 0x1

    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/m/g;->e:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/m/g;->f:I

    move v2, v0

    :goto_2
    iput v2, p0, Lcom/google/android/apps/gmm/map/m/g;->a:I

    mul-int/2addr v2, v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/m/g;->b:I

    mul-int/2addr v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/g;->c:I

    mul-int v0, v1, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/g;->d:I

    .line 420
    return-void

    .line 419
    :cond_4
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/gmm/map/m/g;->e:I

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/gmm/map/m/g;->f:I

    goto :goto_2
.end method


# virtual methods
.method a(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 804
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    .line 805
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    const/4 v4, 0x3

    .line 806
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v8

    .line 807
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v11

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v12

    .line 808
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v13

    .line 805
    invoke-static/range {v0 .. v13}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v0

    .line 809
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v1, v0}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    .line 811
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->s:Z

    .line 812
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->o:Z

    .line 813
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->p:Z

    .line 814
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->n:Z

    if-eqz v0, :cond_1

    .line 815
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/g;->n:Z

    .line 817
    :cond_1
    return-void
.end method
