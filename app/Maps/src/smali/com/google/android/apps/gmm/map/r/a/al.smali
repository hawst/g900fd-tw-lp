.class public Lcom/google/android/apps/gmm/map/r/a/al;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/maps/g/a/ff;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/a/ff;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    .line 21
    return-void
.end method


# virtual methods
.method public final a(II)Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 53
    if-ltz p1, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 54
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v2, v2, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 61
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    .line 62
    iget-object v0, v0, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/eu;->d()Lcom/google/maps/g/a/eu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eu;

    .line 61
    invoke-static {v0, v4, v3, v3, v4}, Lcom/google/android/apps/gmm/map/r/a/aj;->a(Lcom/google/maps/g/a/eu;Lcom/google/android/apps/gmm/map/b/a/y;FFLcom/google/android/apps/gmm/map/r/a/ap;)Lcom/google/android/apps/gmm/map/r/a/aj;

    move-result-object v0

    .line 64
    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->i:I

    iget v2, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->j:I

    .line 63
    invoke-static {v0, p2, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v0

    return-object v0
.end method
