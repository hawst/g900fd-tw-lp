.class public Lcom/google/android/apps/gmm/map/i/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/r/a/r;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field private final b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

.field private final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/m;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/map/r/a/t;

.field private final f:Z

.field private final g:Z

.field private h:Lcom/google/android/apps/gmm/map/r/a/w;

.field private final i:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/i/p;)V
    .locals 6

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 80
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 81
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/aq;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/internal/c/aq;-><init>()V

    aput-object v2, v0, v1

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v0, v0, v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    aget-byte v2, v2, v1

    iput-byte v2, v0, Lcom/google/android/apps/gmm/map/internal/c/aq;->b:B

    .line 85
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->j:Lcom/google/android/apps/gmm/map/i/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/q;->c:Ljava/util/TreeMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 88
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/by;

    invoke-virtual {v0}, Lcom/google/maps/g/by;->d()Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v2, v2, v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/aq;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 81
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 94
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 95
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->k:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 96
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ar;

    .line 97
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    aget-object v4, v4, v2

    iget v5, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ar;->c:I

    or-int/2addr v0, v5

    iput v0, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    goto :goto_1

    .line 101
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->c:Lcom/google/b/c/cv;

    .line 102
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->d:Lcom/google/b/c/cv;

    .line 103
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->g:Lcom/google/android/apps/gmm/map/r/a/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->e:Lcom/google/android/apps/gmm/map/r/a/t;

    .line 104
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/i/p;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/n;->f:Z

    .line 105
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/i/p;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/n;->g:Z

    .line 106
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    const-string v1, "route"

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->h:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 107
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->i:Ljava/lang/String;

    .line 108
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/i/p;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->j:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/r;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->c()[Lcom/google/android/apps/gmm/map/internal/c/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    .line 113
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->d()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->c:Lcom/google/b/c/cv;

    .line 114
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 115
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/n;->f:Z

    .line 116
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->e()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->d:Lcom/google/b/c/cv;

    .line 117
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->f()Lcom/google/android/apps/gmm/map/r/a/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->e:Lcom/google/android/apps/gmm/map/r/a/t;

    .line 118
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/n;->g:Z

    .line 119
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->i()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->h:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 120
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->i:Ljava/lang/String;

    .line 121
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->j:Ljava/lang/String;

    .line 122
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/gmm/map/c/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 225
    if-nez p1, :cond_1

    .line 242
    :cond_0
    return-void

    .line 230
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v1

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 232
    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 233
    if-eqz v5, :cond_2

    .line 234
    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/b/a/l;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 237
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/c/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/ar;

    iget v6, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/ar;->c:I

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v5, v6

    iput v5, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    .line 231
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_3
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/c/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/ar;

    iget v6, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/ar;->c:I

    or-int/2addr v5, v6

    iput v5, v4, Lcom/google/android/apps/gmm/map/internal/c/aq;->a:I

    goto :goto_1
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 136
    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 139
    :goto_0
    return-object v0

    .line 140
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 139
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/bc;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()[Lcom/google/android/apps/gmm/map/internal/c/aq;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->b:[Lcom/google/android/apps/gmm/map/internal/c/aq;

    return-object v0
.end method

.method public final d()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->c:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final e()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->d:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/map/r/a/t;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->e:Lcom/google/android/apps/gmm/map/r/a/t;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/n;->f:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/i/n;->g:Z

    return v0
.end method

.method public final i()Lcom/google/android/apps/gmm/map/r/a/w;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->h:Lcom/google/android/apps/gmm/map/r/a/w;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/n;->j:Ljava/lang/String;

    return-object v0
.end method
