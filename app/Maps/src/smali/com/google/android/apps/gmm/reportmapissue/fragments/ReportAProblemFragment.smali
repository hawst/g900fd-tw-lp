.class public Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/fragments/a/b;
.implements Lcom/google/android/apps/gmm/reportmapissue/h;


# instance fields
.field a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

.field b:Landroid/view/View;

.field private c:Lcom/google/android/apps/gmm/reportmapissue/a/c;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/fragments/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/a;-><init>(Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->d:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/reportmapissue/a/c;)Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    const-string v1, "MODEL_KEY"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 59
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;-><init>()V

    .line 60
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->setArguments(Landroid/os/Bundle;)V

    .line 61
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/r/b/a/ajt;)V
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->r:Landroid/app/ProgressDialog;

    .line 139
    :cond_1
    if-eqz p1, :cond_3

    iget v0, p1, Lcom/google/r/b/a/ajt;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/ajw;->a(I)Lcom/google/r/b/a/ajw;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/ajw;->a:Lcom/google/r/b/a/ajw;

    :cond_2
    sget-object v1, Lcom/google/r/b/a/ajw;->b:Lcom/google/r/b/a/ajw;

    if-eq v0, v1, :cond_4

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/fragments/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/c;-><init>(Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;)V

    new-instance v2, Lcom/google/android/apps/gmm/reportmapissue/fragments/d;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/d;-><init>(Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/f/b;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 142
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    const-class v2, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->c()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 127
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/d/h;->n:Lcom/google/android/apps/gmm/reportmapissue/d/f;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/reportmapissue/c/d;->a(Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 130
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/google/b/f/t;->dP:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    if-eqz p1, :cond_0

    .line 69
    :goto_0
    const-string v0, "MODEL_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->c:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/fragments/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/b;-><init>(Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;)V

    .line 79
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/d/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->c:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->d:Ljava/lang/Runnable;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/apps/gmm/reportmapissue/d/h;-><init>(Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;Lcom/google/android/apps/gmm/reportmapissue/a/c;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    .line 81
    return-void

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 103
    const-class v1, Lcom/google/android/apps/gmm/reportmapissue/b/b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->b:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->b:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 85
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/d/h;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 89
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 90
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    const/4 v1, 0x0

    .line 91
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 93
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 94
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 95
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 97
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 110
    const-string v0, "MODEL_KEY"

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/fragments/ReportAProblemFragment;->c:Lcom/google/android/apps/gmm/reportmapissue/a/c;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 111
    return-void
.end method
