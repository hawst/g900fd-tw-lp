.class public Lcom/google/android/apps/gmm/directions/i/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/util/b/p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/util/b/p",
        "<",
        "Lcom/google/android/apps/gmm/directions/h/w;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/directions/i/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/iv;Lcom/google/o/h/a/jf;Lcom/google/android/libraries/curvular/ag;)Lcom/google/android/libraries/curvular/ce;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 24
    iget-object v0, p3, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    iget-object v0, v0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v1

    :goto_0
    iget-object v3, v1, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v3, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v1

    :goto_1
    iget-object v1, v1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    :goto_2
    if-nez v1, :cond_5

    move-object v0, v2

    :goto_3
    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_0

    :cond_1
    iget-object v1, v1, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_1

    :cond_2
    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v1

    :goto_4
    iget-object v4, v1, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v4, :cond_4

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v1

    :goto_5
    iget-object v1, v1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hu;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    move-object v1, v3

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_4

    :cond_4
    iget-object v1, v1, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_5

    :cond_5
    iget-object v0, p3, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    iget-object v0, v0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    new-instance v3, Lcom/google/android/apps/gmm/directions/i/a/b;

    invoke-direct {v3, p0, p2, v0}, Lcom/google/android/apps/gmm/directions/i/a/b;-><init>(Lcom/google/android/apps/gmm/directions/i/a/a;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/a;)V

    sget-object v4, Lcom/google/android/apps/gmm/directions/i/a/c;->a:[I

    iget v0, p4, Lcom/google/o/h/a/jf;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/o/h/a/ji;->a:Lcom/google/o/h/a/ji;

    :cond_6
    invoke-virtual {v0}, Lcom/google/o/h/a/ji;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/i/a/a;->a:Ljava/lang/String;

    const-string v1, "Unknown style for directions item"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_3

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->d:Lcom/google/android/apps/gmm/directions/h/x;

    :goto_6
    new-instance v2, Lcom/google/android/apps/gmm/directions/i/be;

    invoke-direct {v2, p1, v1, v5}, Lcom/google/android/apps/gmm/directions/i/be;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;I)V

    iput-object v0, v2, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/i/be;->f:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/i/be;->a()Lcom/google/android/apps/gmm/directions/h/w;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->e:Lcom/google/android/apps/gmm/directions/h/x;

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
