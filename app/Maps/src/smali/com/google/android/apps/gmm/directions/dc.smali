.class Lcom/google/android/apps/gmm/directions/dc;
.super Lcom/google/android/apps/gmm/directions/v;
.source "PG"


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field private final k:Z

.field private final l:Lcom/google/android/apps/gmm/directions/h/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/directions/dc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/dc;->j:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;ILcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Z)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 45
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/v;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Z)V

    .line 48
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_1

    move v0, v6

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-boolean v0, v0, Lcom/google/r/b/a/afb;->j:Z

    if-eqz v0, :cond_2

    move v0, v6

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/dc;->k:Z

    .line 49
    new-instance v1, Lcom/google/android/apps/gmm/directions/i/be;

    if-ltz p3, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, p3, :cond_3

    :cond_0
    const/4 v0, 0x0

    :goto_2
    invoke-direct {v1, p1, v0, p3}, Lcom/google/android/apps/gmm/directions/i/be;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;I)V

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/dc;->k:Z

    .line 50
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/directions/i/be;->c:Z

    if-eqz p6, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    .line 51
    :goto_3
    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/i/be;->a:Lcom/google/android/apps/gmm/directions/h/x;

    .line 54
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/i/be;->a()Lcom/google/android/apps/gmm/directions/h/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/dc;->l:Lcom/google/android/apps/gmm/directions/h/w;

    .line 55
    return-void

    :cond_1
    move v0, v7

    .line 48
    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_1

    .line 49
    :cond_3
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p3

    if-nez v0, :cond_4

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v3, v0, p2}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v3, v2, p3

    :cond_4
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, p3

    goto :goto_2

    .line 50
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->c:Lcom/google/android/apps/gmm/directions/h/x;

    goto :goto_3
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 59
    sget v0, Lcom/google/android/apps/gmm/h;->D:I

    return v0
.end method

.method protected final a(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)Landroid/widget/ListView;
    .locals 29

    .prologue
    .line 85
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    :cond_0
    check-cast v3, Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 86
    new-instance v23, Lcom/google/android/apps/gmm/directions/cp;

    invoke-direct/range {v23 .. v23}, Lcom/google/android/apps/gmm/directions/cp;-><init>()V

    .line 87
    move-object/from16 v0, p3

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-nez v4, :cond_1

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    :cond_1
    check-cast v4, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v4, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/dc;->c:Lcom/google/maps/g/a/ja;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/dc;->d:Lcom/google/maps/g/a/ja;

    .line 88
    move-object/from16 v0, v23

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cp;->b:Lcom/google/maps/g/a/ja;

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/google/android/apps/gmm/directions/cp;->c:Lcom/google/maps/g/a/ja;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/dc;->e:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/dc;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 89
    move-object/from16 v0, v23

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cp;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/google/android/apps/gmm/directions/cp;->e:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 90
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v3, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    :goto_0
    iget-object v4, v3, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    if-nez v4, :cond_3

    invoke-static {}, Lcom/google/maps/g/a/gy;->d()Lcom/google/maps/g/a/gy;

    move-result-object v3

    :goto_1
    iget-object v3, v3, Lcom/google/maps/g/a/gy;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, v23

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/directions/cp;->f:Z

    .line 91
    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v3, :cond_5

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 90
    :cond_2
    iget-object v3, v3, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_3
    iget-object v3, v3, Lcom/google/maps/g/a/fk;->g:Lcom/google/maps/g/a/gy;

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 91
    :cond_5
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    new-instance v22, Lcom/google/android/apps/gmm/directions/cv;

    invoke-direct/range {v22 .. v22}, Lcom/google/android/apps/gmm/directions/cv;-><init>()V

    new-instance v3, Lcom/google/android/apps/gmm/directions/cu;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/cu;-><init>()V

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v3, Lcom/google/android/apps/gmm/directions/cz;->n:I

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    const/4 v4, 0x0

    new-instance v25, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v3, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/ea;

    move-object/from16 v0, v25

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v3, v3, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v26

    const/4 v3, 0x0

    move/from16 v21, v3

    move-object/from16 v18, v22

    :goto_3
    move/from16 v0, v21

    move/from16 v1, v26

    if-ge v0, v1, :cond_39

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v27

    if-eqz v18, :cond_6

    const/4 v3, 0x1

    :goto_4
    if-nez v3, :cond_7

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    throw v3

    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    :cond_7
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget v3, v3, Lcom/google/maps/g/a/ff;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    const/4 v3, 0x1

    :goto_5
    if-nez v3, :cond_9

    :goto_6
    add-int/lit8 v3, v21, 0x1

    move/from16 v21, v3

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    :cond_9
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/fk;

    new-instance v20, Lcom/google/android/apps/gmm/directions/cv;

    invoke-direct/range {v20 .. v20}, Lcom/google/android/apps/gmm/directions/cv;-><init>()V

    new-instance v6, Lcom/google/android/apps/gmm/directions/db;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/directions/db;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    sget-object v4, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    iget v5, v3, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v5, v5, 0x1

    const/4 v8, 0x1

    if-ne v5, v8, :cond_c

    const/4 v5, 0x1

    :goto_7
    if-eqz v5, :cond_a

    iget v4, v3, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v4}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v4

    if-nez v4, :cond_a

    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_a
    iput-object v4, v6, Lcom/google/android/apps/gmm/directions/db;->l:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->g:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->f:Lcom/google/maps/g/a/hm;

    invoke-virtual {v3}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v6, Lcom/google/android/apps/gmm/directions/db;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v6, Lcom/google/android/apps/gmm/directions/db;->j:Ljava/lang/String;

    iget v5, v3, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v5, v5, 0x8

    const/16 v8, 0x8

    if-ne v5, v8, :cond_d

    const/4 v5, 0x1

    :goto_8
    if-eqz v5, :cond_b

    iget-object v5, v3, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v5, :cond_e

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v3

    :goto_9
    iput-object v3, v6, Lcom/google/android/apps/gmm/directions/db;->h:Lcom/google/maps/g/a/be;

    :cond_b
    const/4 v3, 0x0

    const/16 v5, -0x3039

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v5}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, v18

    iput v3, v0, Lcom/google/android/apps/gmm/directions/cv;->p:I

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v4, v3, :cond_38

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    move-object v14, v3

    check-cast v14, Lcom/google/maps/g/a/hg;

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/fk;

    iget-boolean v11, v3, Lcom/google/maps/g/a/fk;->i:Z

    const/4 v3, 0x0

    move/from16 v17, v3

    move-object/from16 v16, v18

    :goto_a
    iget-object v3, v14, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v17

    if-ge v0, v3, :cond_2b

    if-nez v17, :cond_f

    const/4 v3, 0x1

    move v13, v3

    :goto_b
    iget-object v3, v14, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v17

    if-ne v0, v3, :cond_10

    const/4 v3, 0x1

    move/from16 v19, v3

    :goto_c
    iget-object v3, v14, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/e;->d()Lcom/google/maps/g/a/e;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    move-object v15, v3

    check-cast v15, Lcom/google/maps/g/a/e;

    iget-object v3, v15, Lcom/google/maps/g/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/go;

    new-instance v10, Ljava/util/ArrayList;

    iget-object v4, v3, Lcom/google/maps/g/a/go;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v10, v4}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, v3, Lcom/google/maps/g/a/go;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ee;->d()Lcom/google/maps/g/a/ee;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/ee;

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_8

    :cond_e
    iget-object v3, v3, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_9

    :cond_f
    const/4 v3, 0x0

    move v13, v3

    goto :goto_b

    :cond_10
    const/4 v3, 0x0

    move/from16 v19, v3

    goto :goto_c

    :cond_11
    iget-object v3, v15, Lcom/google/maps/g/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/go;

    invoke-virtual {v3}, Lcom/google/maps/g/a/go;->g()Ljava/lang/String;

    move-result-object v3

    const v4, -0x996601

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_12

    const v3, -0x996601

    :cond_12
    iget v4, v15, Lcom/google/maps/g/a/e;->d:I

    if-gtz v4, :cond_1b

    iget-object v4, v14, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/gu;

    move-object v5, v4

    :goto_e
    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/apps/gmm/directions/cv;->c:I

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/apps/gmm/directions/cv;->d:I

    sget-object v4, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->g:Lcom/google/maps/g/a/hm;

    iget v4, v5, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v6, 0x20

    if-ne v4, v6, :cond_1c

    const/4 v4, 0x1

    :goto_f
    if-eqz v4, :cond_1d

    iget-object v4, v5, Lcom/google/maps/g/a/gu;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/fo;

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    :cond_13
    :goto_10
    if-eqz v13, :cond_15

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    if-nez v4, :cond_14

    invoke-virtual {v5}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    :cond_14
    iget v4, v5, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v6, 0x80

    if-ne v4, v6, :cond_1f

    const/4 v4, 0x1

    :goto_11
    if-eqz v4, :cond_15

    iget-object v4, v5, Lcom/google/maps/g/a/gu;->i:Ljava/lang/Object;

    instance-of v6, v4, Ljava/lang/String;

    if-eqz v6, :cond_20

    check-cast v4, Ljava/lang/String;

    :goto_12
    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->m:Ljava/lang/String;

    :cond_15
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->c(Ljava/lang/Iterable;)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->d(Ljava/lang/Iterable;)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->e(Ljava/lang/Iterable;)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->b(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->f(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    iget v9, v15, Lcom/google/maps/g/a/e;->a:I

    and-int/lit8 v9, v9, 0x2

    const/4 v12, 0x2

    if-ne v9, v12, :cond_22

    const/4 v9, 0x1

    :goto_13
    if-eqz v9, :cond_23

    iget-object v9, v15, Lcom/google/maps/g/a/e;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v9

    check-cast v9, Lcom/google/maps/g/a/be;

    :goto_14
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_25

    iget v12, v14, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v12, v12, 0x20

    const/16 v28, 0x20

    move/from16 v0, v28

    if-ne v12, v0, :cond_24

    const/4 v12, 0x1

    :goto_15
    if-eqz v12, :cond_25

    iget-object v12, v14, Lcom/google/maps/g/a/hg;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/bm;->h()Lcom/google/maps/g/a/bm;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v12

    check-cast v12, Lcom/google/maps/g/a/bm;

    :goto_16
    if-eqz v13, :cond_26

    move-object/from16 v0, v27

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v13, v13, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v13, v0}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v13

    check-cast v13, Lcom/google/maps/g/a/fk;

    iget-object v13, v13, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    :goto_17
    invoke-static/range {v3 .. v13}, Lcom/google/android/apps/gmm/directions/cp;->a(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/a/be;Ljava/lang/String;ZLcom/google/maps/g/a/bm;Ljava/util/List;)Lcom/google/android/apps/gmm/directions/db;

    move-result-object v3

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/directions/cp;->a(Lcom/google/maps/g/a/hg;Lcom/google/maps/g/a/e;)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/apps/gmm/directions/cp;->a(Ljava/util/List;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/db;Ljava/util/List;)V

    if-nez v19, :cond_3f

    new-instance v4, Lcom/google/android/apps/gmm/directions/co;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/directions/co;-><init>()V

    iget-object v3, v15, Lcom/google/maps/g/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/go;

    invoke-virtual {v3}, Lcom/google/maps/g/a/go;->g()Ljava/lang/String;

    move-result-object v3

    const v5, -0x996601

    invoke-static {v3, v5}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;I)I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_16

    const v3, -0x996601

    :cond_16
    iput v3, v4, Lcom/google/android/apps/gmm/directions/co;->b:I

    const/4 v3, -0x1

    iput v3, v4, Lcom/google/android/apps/gmm/directions/co;->e:I

    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    iput-object v3, v4, Lcom/google/android/apps/gmm/directions/co;->f:Lcom/google/maps/g/a/hm;

    iget v3, v15, Lcom/google/maps/g/a/e;->e:I

    if-lez v3, :cond_17

    iget v3, v15, Lcom/google/maps/g/a/e;->e:I

    iget-object v5, v14, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v3, v5, :cond_27

    :cond_17
    const/4 v3, 0x0

    move-object v5, v3

    :goto_18
    if-eqz v5, :cond_1a

    iget v3, v5, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v6, 0x4

    if-ne v3, v6, :cond_28

    const/4 v3, 0x1

    :goto_19
    if-eqz v3, :cond_18

    iget-object v3, v5, Lcom/google/maps/g/a/gu;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/fo;

    iput-object v3, v4, Lcom/google/android/apps/gmm/directions/co;->h:Lcom/google/maps/g/a/fo;

    :cond_18
    iget v3, v5, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v6, 0x8

    if-ne v3, v6, :cond_29

    const/4 v3, 0x1

    :goto_1a
    if-eqz v3, :cond_19

    iget-object v3, v5, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/fo;

    iput-object v3, v4, Lcom/google/android/apps/gmm/directions/co;->i:Lcom/google/maps/g/a/fo;

    :cond_19
    iget v3, v5, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v6, 0x1

    if-ne v3, v6, :cond_2a

    const/4 v3, 0x1

    :goto_1b
    if-eqz v3, :cond_1a

    invoke-virtual {v5}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/google/android/apps/gmm/directions/co;->k:Ljava/lang/String;

    :cond_1a
    move-object v3, v4

    :goto_1c
    add-int/lit8 v4, v17, 0x1

    move/from16 v17, v4

    move-object/from16 v16, v3

    goto/16 :goto_a

    :cond_1b
    iget-object v4, v14, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iget v5, v15, Lcom/google/maps/g/a/e;->d:I

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, v14, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/gu;

    move-object v5, v4

    goto/16 :goto_e

    :cond_1c
    const/4 v4, 0x0

    goto/16 :goto_f

    :cond_1d
    iget v4, v5, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_1e

    const/4 v4, 0x1

    :goto_1d
    if-eqz v4, :cond_13

    iget-object v4, v5, Lcom/google/maps/g/a/gu;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/fo;

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    goto/16 :goto_10

    :cond_1e
    const/4 v4, 0x0

    goto :goto_1d

    :cond_1f
    const/4 v4, 0x0

    goto/16 :goto_11

    :cond_20
    check-cast v4, Lcom/google/n/f;

    invoke-virtual {v4}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/n/f;->e()Z

    move-result v4

    if-eqz v4, :cond_21

    iput-object v6, v5, Lcom/google/maps/g/a/gu;->i:Ljava/lang/Object;

    :cond_21
    move-object v4, v6

    goto/16 :goto_12

    :cond_22
    const/4 v9, 0x0

    goto/16 :goto_13

    :cond_23
    const/4 v9, 0x0

    goto/16 :goto_14

    :cond_24
    const/4 v12, 0x0

    goto/16 :goto_15

    :cond_25
    const/4 v12, 0x0

    goto/16 :goto_16

    :cond_26
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v13

    goto/16 :goto_17

    :cond_27
    iget v3, v15, Lcom/google/maps/g/a/e;->e:I

    add-int/lit8 v3, v3, -0x1

    iget-object v5, v14, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/gu;

    move-object v5, v3

    goto/16 :goto_18

    :cond_28
    const/4 v3, 0x0

    goto/16 :goto_19

    :cond_29
    const/4 v3, 0x0

    goto/16 :goto_1a

    :cond_2a
    const/4 v3, 0x0

    goto/16 :goto_1b

    :cond_2b
    iget-object v3, v14, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2f

    const-string v3, "TransitDetailsModel"

    const-string v4, "Expected block transfer legs."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const v3, -0x996601

    move v4, v3

    :goto_1e
    iget-object v3, v14, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/gu;

    move-object/from16 v0, v20

    iput v4, v0, Lcom/google/android/apps/gmm/directions/cv;->b:I

    const/4 v4, -0x1

    move-object/from16 v0, v20

    iput v4, v0, Lcom/google/android/apps/gmm/directions/cv;->e:I

    sget-object v4, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->f:Lcom/google/maps/g/a/hm;

    iget v4, v3, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_31

    const/4 v4, 0x1

    :goto_1f
    if-eqz v4, :cond_32

    iget-object v4, v3, Lcom/google/maps/g/a/gu;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/fo;

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    :cond_2c
    :goto_20
    iget v4, v3, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v4, v4, 0x1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_34

    const/4 v4, 0x1

    :goto_21
    if-eqz v4, :cond_2d

    invoke-virtual {v3}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    :cond_2d
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/fk;

    iget-object v4, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v4, :cond_35

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v3

    :goto_22
    iget v3, v3, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_36

    const/4 v3, 0x1

    :goto_23
    if-eqz v3, :cond_2e

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v3, v3, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/fk;

    iget-object v4, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v4, :cond_37

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v3

    :goto_24
    iget-object v3, v3, Lcom/google/maps/g/a/ek;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/be;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/cv;->j:Lcom/google/maps/g/a/be;

    :cond_2e
    :goto_25
    move-object/from16 v18, v20

    goto/16 :goto_6

    :cond_2f
    iget-object v3, v14, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iget-object v4, v14, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/e;->d()Lcom/google/maps/g/a/e;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/e;

    iget-object v3, v3, Lcom/google/maps/g/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/a/go;

    invoke-virtual {v3}, Lcom/google/maps/g/a/go;->g()Ljava/lang/String;

    move-result-object v3

    const v4, -0x996601

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_30

    const v3, -0x996601

    :cond_30
    move v4, v3

    goto/16 :goto_1e

    :cond_31
    const/4 v4, 0x0

    goto/16 :goto_1f

    :cond_32
    iget v4, v3, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_33

    const/4 v4, 0x1

    :goto_26
    if-eqz v4, :cond_2c

    iget-object v4, v3, Lcom/google/maps/g/a/gu;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/a/fo;

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    goto/16 :goto_20

    :cond_33
    const/4 v4, 0x0

    goto :goto_26

    :cond_34
    const/4 v4, 0x0

    goto/16 :goto_21

    :cond_35
    iget-object v3, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto/16 :goto_22

    :cond_36
    const/4 v3, 0x0

    goto/16 :goto_23

    :cond_37
    iget-object v3, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto :goto_24

    :cond_38
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v27

    invoke-static {v0, v1, v6, v7, v2}, Lcom/google/android/apps/gmm/directions/cp;->a(Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/db;Ljava/util/List;Lcom/google/android/apps/gmm/map/r/a/al;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/apps/gmm/directions/cp;->a(Ljava/util/List;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/db;Ljava/util/List;)V

    goto :goto_25

    :cond_39
    if-eqz v18, :cond_3a

    const/4 v3, 0x1

    :goto_27
    if-nez v3, :cond_3b

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    throw v3

    :cond_3a
    const/4 v3, 0x0

    goto :goto_27

    :cond_3b
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, v18

    iput v3, v0, Lcom/google/android/apps/gmm/directions/cz;->n:I

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v3, Lcom/google/android/apps/gmm/directions/cw;->a:Lcom/google/android/apps/gmm/directions/cw;

    move-object/from16 v0, v22

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/cv;->a:Lcom/google/android/apps/gmm/directions/cw;

    sget-object v3, Lcom/google/android/apps/gmm/directions/cw;->c:Lcom/google/android/apps/gmm/directions/cw;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/cv;->a:Lcom/google/android/apps/gmm/directions/cw;

    const/4 v3, 0x0

    move-object/from16 v0, v22

    iput v3, v0, Lcom/google/android/apps/gmm/directions/cv;->p:I

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v18

    iput v3, v0, Lcom/google/android/apps/gmm/directions/cv;->p:I

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/cp;->b:Lcom/google/maps/g/a/ja;

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/google/android/apps/gmm/directions/cp;->c:Lcom/google/maps/g/a/ja;

    move-object/from16 v0, v23

    iget-object v7, v0, Lcom/google/android/apps/gmm/directions/cp;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, v23

    iget-object v8, v0, Lcom/google/android/apps/gmm/directions/cp;->e:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, v23

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/directions/cp;->f:Z

    move-object/from16 v4, v24

    move-object/from16 v9, v22

    move-object/from16 v10, v18

    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/gmm/directions/cp;->a(Lcom/google/android/apps/gmm/map/r/a/w;Ljava/util/List;Lcom/google/maps/g/a/ja;Lcom/google/maps/g/a/ja;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/cv;)V

    move-object/from16 v0, v23

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/directions/cp;->f:Z

    if-eqz v3, :cond_3c

    new-instance v3, Lcom/google/android/apps/gmm/directions/cn;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/cn;-><init>()V

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v3, Lcom/google/android/apps/gmm/directions/cz;->n:I

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3c
    const/4 v3, 0x0

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v4, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v5

    move v4, v3

    :goto_28
    if-ge v4, v5, :cond_3d

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/cp;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v3, Lcom/google/maps/g/a/hu;->h:Lcom/google/n/aq;

    invoke-interface {v3, v4}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v6, Lcom/google/android/apps/gmm/directions/cq;

    invoke-direct {v6, v3}, Lcom/google/android/apps/gmm/directions/cq;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v6, Lcom/google/android/apps/gmm/directions/cz;->n:I

    move-object/from16 v0, v24

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_28

    :cond_3d
    new-instance v3, Lcom/google/android/apps/gmm/directions/cu;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/cu;-><init>()V

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v3, Lcom/google/android/apps/gmm/directions/cz;->n:I

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/gmm/directions/cm;

    move-object/from16 v0, v24

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/directions/cm;-><init>(Ljava/util/List;)V

    .line 93
    new-instance v4, Lcom/google/android/apps/gmm/directions/bt;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/directions/dc;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/directions/bt;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 94
    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/directions/bt;->a(Lcom/google/android/apps/gmm/directions/cm;)V

    .line 95
    new-instance v5, Lcom/google/android/apps/gmm/directions/dd;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v3, v4}, Lcom/google/android/apps/gmm/directions/dd;-><init>(Lcom/google/android/apps/gmm/directions/dc;Lcom/google/android/apps/gmm/directions/cm;Lcom/google/android/apps/gmm/directions/bt;)V

    iput-object v5, v4, Lcom/google/android/apps/gmm/directions/bt;->a:Lcom/google/android/apps/gmm/directions/ck;

    .line 106
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/dc;->i:Landroid/view/View$OnClickListener;

    iput-object v3, v4, Lcom/google/android/apps/gmm/directions/bt;->b:Landroid/view/View$OnClickListener;

    .line 107
    new-instance v3, Lcom/google/android/apps/gmm/directions/de;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/directions/de;-><init>(Lcom/google/android/apps/gmm/directions/dc;Landroid/view/ViewGroup;)V

    iput-object v3, v4, Lcom/google/android/apps/gmm/directions/bt;->c:Lcom/google/android/apps/gmm/directions/cj;

    .line 123
    sget v3, Lcom/google/android/apps/gmm/g;->ac:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 124
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/dc;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v6, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v3, :cond_3e

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3e
    check-cast v3, Lcom/google/android/libraries/curvular/bd;

    const-class v6, Lcom/google/android/apps/gmm/directions/c/ad;

    invoke-virtual {v3, v6, v5}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 127
    sget v3, Lcom/google/android/apps/gmm/g;->ab:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 128
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/directions/dc;->l:Lcom/google/android/apps/gmm/directions/h/w;

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 129
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 131
    return-object v3

    :cond_3f
    move-object/from16 v3, v16

    goto/16 :goto_1c
.end method

.method final a(Lcom/google/android/apps/gmm/map/r/a/w;)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 78
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/dc;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dc;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 79
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 78
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    move v0, v4

    goto :goto_0

    :cond_2
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget v0, v1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    move v0, v3

    :goto_3
    if-eqz v0, :cond_0

    iget v0, v1, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_3
    sget-object v8, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v0, v8, :cond_e

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/f/d/e;->a(Lcom/google/android/apps/gmm/map/r/a/p;)Lcom/google/maps/g/a/fk;

    move-result-object v6

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-direct {v2, v5}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    if-eqz v6, :cond_4

    invoke-static {v6}, Lcom/google/android/apps/gmm/directions/f/d/e;->a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/directions/f/d/e;->a(Landroid/content/Context;Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_9

    :goto_4
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_b

    :cond_4
    :goto_5
    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/d/e;->b(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_c

    :cond_5
    move-object v0, v2

    :goto_6
    invoke-static {v5, v1}, Lcom/google/android/apps/gmm/directions/f/d/e;->b(Landroid/content/Context;Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_d

    :cond_6
    :goto_7
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/c/a;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_7
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v1, v0

    goto :goto_2

    :cond_8
    move v0, v4

    goto :goto_3

    :cond_9
    if-nez v0, :cond_a

    sget v0, Lcom/google/android/apps/gmm/l;->ab:I

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v6, v7, v4

    invoke-virtual {v5, v0, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_a
    sget v7, Lcom/google/android/apps/gmm/l;->ac:I

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v4

    aput-object v6, v8, v3

    invoke-virtual {v5, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_b
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_5

    :cond_c
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    move-object v0, v2

    goto :goto_6

    :cond_d
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_7

    :cond_e
    sget-object v5, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v0, v5, :cond_12

    iget v0, v1, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_f

    move v0, v3

    :goto_8
    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v0

    :goto_9
    iget v1, v0, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_11

    move v1, v3

    :goto_a
    if-eqz v1, :cond_0

    invoke-virtual {v6, v0, v3, v3}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/maps/g/a/ai;ZZ)Ljava/lang/String;

    move-result-object v0

    :goto_b
    move-object v2, v0

    goto/16 :goto_1

    :cond_f
    move v0, v4

    goto :goto_8

    :cond_10
    iget-object v0, v1, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_9

    :cond_11
    move v1, v4

    goto :goto_a

    :cond_12
    move-object v0, v2

    goto :goto_b
.end method

.method protected final a(Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 3

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/directions/v;->a(Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)V

    .line 71
    sget v0, Lcom/google/android/apps/gmm/g;->dp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dc;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/directions/c/aq;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 74
    return-void
.end method

.method protected final a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 136
    sget v0, Lcom/google/android/apps/gmm/g;->dp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 137
    if-nez v1, :cond_0

    .line 138
    sget-object v0, Lcom/google/android/apps/gmm/directions/dc;->j:Ljava/lang/String;

    const-string v1, "Could not find sheet header content view."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dc;->l:Lcom/google/android/apps/gmm/directions/h/w;

    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/directions/h/w;->a(Lcom/google/android/apps/gmm/directions/h/x;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dc;->l:Lcom/google/android/apps/gmm/directions/h/w;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 142
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->c:Lcom/google/android/apps/gmm/directions/h/x;

    goto :goto_1
.end method
