.class public Lcom/google/android/apps/gmm/directions/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/a/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final c:Lcom/google/maps/g/a/a;

.field private final d:Lcom/google/android/apps/gmm/suggest/e/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/a/ja;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iget-object v0, p2, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    invoke-virtual {v0}, Lcom/google/maps/g/a/je;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->a:Ljava/lang/String;

    .line 44
    iget-object v0, p2, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    iget v0, v0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 45
    iget-object v0, p2, Lcom/google/maps/g/a/ja;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    iget-object v0, v0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 49
    :goto_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/z;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 50
    iget-object v0, p2, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/a;->d()Lcom/google/maps/g/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    .line 51
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/maps/g/gy;->b:D

    iget-wide v4, v0, Lcom/google/maps/g/gy;->c:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v0, v1

    goto :goto_1

    .line 47
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/z;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    .line 79
    iget-object v0, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 98
    if-eqz p1, :cond_0

    .line 99
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/b/f/cq;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/b/f/t;->gp:Lcom/google/b/f/t;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/b/f/t;->aF:Lcom/google/b/f/t;

    aput-object v5, v3, v4

    .line 102
    iput-object v3, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 104
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 101
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 105
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/b/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/b/f;-><init>(Lcom/google/android/apps/gmm/directions/a/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 108
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    .line 89
    iget-object v0, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->c:Lcom/google/maps/g/a/a;

    iget-object v0, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 92
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/z;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    return-object v0
.end method
