.class public Lcom/google/android/apps/gmm/map/legacy/internal/b/l;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/c;
.source "PG"


# instance fields
.field public d:Z

.field volatile e:Z

.field final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 1

    .prologue
    .line 1553
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 1554
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/c;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    .line 1555
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->f:Ljava/util/Set;

    .line 1556
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/Set;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1582
    move v1, v2

    move v3, v2

    .line 1585
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1586
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 1587
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1588
    sget-object v3, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget v5, v5, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->v:F

    invoke-interface {v0, v3, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/map/t/b;F)Z

    move v3, v4

    .line 1585
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1600
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1601
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 1608
    instance-of v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 1609
    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    .line 1610
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    if-eq v6, v7, :cond_2

    .line 1611
    iget-object v6, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    if-eq v6, v7, :cond_2

    .line 1612
    iget-object v6, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    if-eq v6, v7, :cond_2

    .line 1613
    const-string v0, "applyTileChanges ignoring tile, want legend="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 1614
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1615
    iget-object v7, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1616
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x21

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " got legend="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " coords="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tileOverlay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 1621
    :cond_2
    invoke-interface {v0, p1, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    .line 1622
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget v6, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->v:F

    invoke-interface {v0, v1, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/map/t/b;F)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1623
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->f:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 1631
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(F)V

    .line 1632
    or-int/lit8 v3, v3, 0x1

    .line 1634
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1637
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1638
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1639
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1640
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1642
    if-eqz v3, :cond_6

    .line 1643
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 1648
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/k;

    .line 1649
    if-eqz v0, :cond_5

    .line 1650
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/k;->a()V

    .line 1655
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->x:Lcom/google/android/apps/gmm/map/o/p;

    .line 1656
    if-eqz v0, :cond_6

    .line 1657
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o/p;->c(Lcom/google/android/apps/gmm/map/o/ak;)V

    .line 1661
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    monitor-enter v1

    .line 1662
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->k:Ljava/lang/Runnable;

    if-eqz v0, :cond_7

    .line 1663
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->k:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1664
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    const/4 v5, 0x0

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->k:Ljava/lang/Runnable;

    .line 1666
    :cond_7
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1670
    if-nez v3, :cond_9

    :goto_2
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->d:Z

    .line 1673
    if-eqz v3, :cond_8

    .line 1674
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1676
    :cond_8
    return-void

    .line 1640
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1666
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_9
    move v4, v2

    .line 1670
    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 3

    .prologue
    .line 1560
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    .line 1561
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z

    if-eqz v1, :cond_0

    .line 1562
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->f:Ljava/util/Set;

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/Set;)V

    .line 1572
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z

    if-nez v0, :cond_1

    .line 1574
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/Set;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1579
    :goto_0
    monitor-exit p0

    return-void

    .line 1576
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->f:Ljava/util/Set;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/Set;)V

    .line 1577
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 0

    .prologue
    .line 1536
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/c;->a(Lcom/google/android/apps/gmm/v/g;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 1680
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/c;->b(Lcom/google/android/apps/gmm/v/g;)V

    .line 1682
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->e:Z

    if-eqz v0, :cond_0

    .line 1683
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->e:Z

    .line 1684
    sget-object v0, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->b(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1685
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->b(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1687
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1706
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 1707
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->i:Z

    .line 1708
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1710
    :cond_0
    return-void
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 1728
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V

    .line 1729
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1730
    monitor-exit p0

    return-void

    .line 1728
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
