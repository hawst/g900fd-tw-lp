.class public Lcom/google/android/apps/gmm/map/u/a/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/u/a/f;

.field static final b:[Lcom/google/android/apps/gmm/map/u/a/a;


# instance fields
.field public final c:I

.field public final d:[Lcom/google/android/apps/gmm/map/u/a/f;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public f:[Lcom/google/android/apps/gmm/map/u/a/a;

.field public final g:I

.field public final h:I

.field private final i:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/u/a/f;

    const-string v1, "Unknown Road"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/u/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Lcom/google/android/apps/gmm/map/u/a/e;->a:Lcom/google/android/apps/gmm/map/u/a/f;

    .line 24
    new-array v0, v3, [Lcom/google/android/apps/gmm/map/u/a/a;

    sput-object v0, Lcom/google/android/apps/gmm/map/u/a/e;->b:[Lcom/google/android/apps/gmm/map/u/a/a;

    return-void
.end method

.method public constructor <init>(J[Lcom/google/android/apps/gmm/map/u/a/f;Lcom/google/android/apps/gmm/map/b/a/ab;IIII)V
    .locals 3

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    array-length v0, p3

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Segments must have at least one name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_0
    iput p5, p0, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    .line 159
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->i:J

    .line 160
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    .line 161
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 162
    sget-object v0, Lcom/google/android/apps/gmm/map/u/a/e;->b:[Lcom/google/android/apps/gmm/map/u/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    .line 164
    iput p7, p0, Lcom/google/android/apps/gmm/map/u/a/e;->g:I

    .line 165
    iput p8, p0, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    .line 166
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)J
    .locals 5

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    int-to-long v0, v0

    const/16 v2, 0x30

    shl-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, -0x1

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v1
.end method

.method public final a(ILcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 238
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 314
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/u/a/e;

    if-nez v1, :cond_1

    .line 317
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/u/a/e;->i:J

    check-cast p1, Lcom/google/android/apps/gmm/map/u/a/e;

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/u/a/e;->i:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 307
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->i:J

    const/16 v2, 0x30

    ushr-long/2addr v0, v2

    and-long/2addr v0, v6

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/u/a/e;->i:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/u/a/e;->i:J

    const-wide/32 v4, 0xffff

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 322
    new-instance v4, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    aget-object v1, v1, v3

    .line 323
    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "unroutable"

    .line 324
    iget v1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "leaves-region"

    .line 325
    iget v1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/b/a/al;

    invoke-direct {v5}, Lcom/google/b/a/al;-><init>()V

    iget-object v6, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v5, v6, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v5, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "enters-region"

    .line 326
    iget v1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    :goto_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "num-points"

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "first-point"

    .line 328
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "last-point"

    .line 329
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "num-arcs"

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    array-length v1, v1

    .line 330
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 331
    invoke-virtual {v4}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
