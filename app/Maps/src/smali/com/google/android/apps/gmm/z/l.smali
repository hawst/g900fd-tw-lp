.class public Lcom/google/android/apps/gmm/z/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/z/b/n;

.field public b:Lcom/google/b/f/cq;

.field public c:Lcom/google/b/f/b/a/dj;

.field public d:Lcom/google/android/apps/gmm/shared/c/f;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:[Lcom/google/b/f/cq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/z/k;
    .locals 8

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/l;->a:Lcom/google/android/apps/gmm/z/b/n;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/l;->d:Lcom/google/android/apps/gmm/shared/c/f;

    if-nez v0, :cond_1

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/l;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 113
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/z/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/l;->a:Lcom/google/android/apps/gmm/z/b/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/l;->d:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/l;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/z/l;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/gmm/z/l;->b:Lcom/google/b/f/cq;

    iget-object v6, p0, Lcom/google/android/apps/gmm/z/l;->g:[Lcom/google/b/f/cq;

    iget-object v7, p0, Lcom/google/android/apps/gmm/z/l;->c:Lcom/google/b/f/b/a/dj;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/z/k;-><init>(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/shared/c/f;Ljava/lang/String;Ljava/lang/String;Lcom/google/b/f/cq;[Lcom/google/b/f/cq;Lcom/google/b/f/b/a/dj;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/l;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 93
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/l;->e:Ljava/lang/String;

    .line 94
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/l;->f:Ljava/lang/String;

    .line 95
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v0, v0

    if-gtz v0, :cond_2

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/z/l;->b:Lcom/google/b/f/cq;

    .line 97
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v0, v0

    if-gt v0, v4, :cond_3

    :cond_1
    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/z/l;->g:[Lcom/google/b/f/cq;

    .line 98
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->d:Lcom/google/b/f/b/a/dj;

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/l;->c:Lcom/google/b/f/b/a/dj;

    .line 99
    return-object p0

    .line 95
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aget-object v0, v0, v3

    goto :goto_0

    .line 97
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v0, v0

    add-int/lit8 v1, v0, -0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    iget-object v2, p1, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v4, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method
