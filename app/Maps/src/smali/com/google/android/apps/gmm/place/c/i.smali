.class public Lcom/google/android/apps/gmm/place/c/i;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/ae;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 29
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 30
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v8

    .line 31
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v9

    .line 33
    invoke-static {}, Lcom/google/android/apps/gmm/place/c/c;->a()Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v10

    const/4 v2, 0x3

    const-class v3, Lcom/google/android/apps/gmm/base/f/aq;

    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->C()Lcom/google/android/apps/gmm/base/l/a/o;

    move-result-object v0

    new-instance v4, Lcom/google/android/libraries/curvular/ao;

    const/4 v5, 0x0

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-direct {v4, v3, v0}, Lcom/google/android/libraries/curvular/ao;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ah;)V

    aput-object v4, v1, v2

    const/4 v0, 0x4

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/i;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Lcom/google/android/libraries/curvular/cu;

    sget-object v4, Lcom/google/android/apps/gmm/place/t;->a:Lcom/google/android/libraries/curvular/bk;

    .line 39
    sget-object v5, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v8

    .line 41
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v9

    .line 42
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x3

    sget v5, Lcom/google/android/apps/gmm/d;->aR:I

    .line 43
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v3, v4

    .line 36
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/base/k/ao;->a(Lcom/google/android/libraries/curvular/am;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v1, v0

    .line 29
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.FrameLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 5

    .prologue
    .line 25
    check-cast p2, Lcom/google/android/apps/gmm/place/ae;

    new-instance v0, Lcom/google/android/apps/gmm/place/c/a;

    invoke-direct {v0, p2, p4, p3}, Lcom/google/android/apps/gmm/place/c/a;-><init>(Lcom/google/android/apps/gmm/place/ae;Lcom/google/android/libraries/curvular/bc;Landroid/content/Context;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->u()Lcom/google/android/apps/gmm/place/i/f;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/j;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->u()Lcom/google/android/apps/gmm/place/i/f;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/ae;->G()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->f()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->b()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->i()Lcom/google/android/apps/gmm/place/i/c;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->i()Lcom/google/android/apps/gmm/place/i/c;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/i/c;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/d/a/i;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->i()Lcom/google/android/apps/gmm/place/i/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/r;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->i()Lcom/google/android/apps/gmm/place/i/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/d/a/g;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->i()Lcom/google/android/apps/gmm/place/i/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/c/a;->a(Z)Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->E()Lcom/google/android/apps/gmm/place/e/b/a;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->E()Lcom/google/android/apps/gmm/place/e/b/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/e/b/a;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/e/a/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->E()Lcom/google/android/apps/gmm/place/e/b/a;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->F()Lcom/google/android/apps/gmm/place/e/b/a;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->F()Lcom/google/android/apps/gmm/place/e/b/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/e/b/a;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/e/a/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->F()Lcom/google/android/apps/gmm/place/e/b/a;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->d()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/base/f/av;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/ae;->p()Lcom/google/android/apps/gmm/base/l/v;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->g()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/c/a;->h()Lcom/google/android/apps/gmm/place/c/a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->f()Lcom/google/android/apps/gmm/place/x;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/x;->b()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v3, Lcom/google/android/apps/gmm/place/c/m;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_5
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->I()Lcom/google/android/apps/gmm/place/j;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v3, Lcom/google/android/apps/gmm/place/i;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/j;->a()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/h;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/j;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    :cond_6
    :goto_0
    return-void

    :cond_7
    iget-object v1, v0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method
