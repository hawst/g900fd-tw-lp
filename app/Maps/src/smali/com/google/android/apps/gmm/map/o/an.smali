.class public Lcom/google/android/apps/gmm/map/o/an;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const v0, -0x9197a9

    sput v0, Lcom/google/android/apps/gmm/map/o/an;->a:I

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/be;FIIF)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    .line 112
    :cond_0
    int-to-float v0, v0

    mul-float/2addr v0, p1

    .line 113
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 114
    mul-float/2addr v0, p4

    return v0

    :cond_1
    move v1, v0

    .line 111
    goto :goto_0
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 67
    const/16 v0, 0xff

    ushr-int/lit8 v1, p0, 0x18

    mul-int/2addr v0, v1

    div-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 70
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/o/an;->b(I)I

    move-result v1

    .line 74
    const/16 v2, 0xc0

    if-lt v1, v2, :cond_0

    .line 77
    :goto_0
    return v0

    :cond_0
    const v1, 0xffffff

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/o/ap;F)I
    .locals 3

    .prologue
    .line 149
    iget v0, p1, Lcom/google/android/apps/gmm/map/o/ap;->d:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/ap;->e:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/o/ap;->f:I

    invoke-static {p0, v0, v1, v2, p2}, Lcom/google/android/apps/gmm/map/o/an;->a(Lcom/google/android/apps/gmm/map/internal/c/be;FIIF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ao;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/t/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    .line 36
    :goto_1
    return v0

    .line 30
    :pswitch_0
    const/high16 v0, -0x60000000

    goto :goto_1

    .line 32
    :pswitch_1
    const/high16 v0, -0x80000000

    goto :goto_1

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/map/o/an;->b(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I

    move-result v0

    const/16 v1, 0xa0

    ushr-int/lit8 v2, v0, 0x18

    mul-int/2addr v1, v2

    div-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/o/an;->b(I)I

    move-result v0

    const/16 v2, 0xc0

    if-lt v0, v2, :cond_2

    const v0, 0x808080

    or-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const v0, 0xffffff

    or-int/2addr v0, v1

    goto :goto_1

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(I)I
    .locals 2

    .prologue
    .line 94
    ushr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x4d

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    mul-int/lit16 v1, v1, 0x97

    add-int/2addr v0, v1

    and-int/lit16 v1, p0, 0xff

    mul-int/lit8 v1, v1, 0x1c

    add-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    .line 98
    return v0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/t/b;)I
    .locals 2

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ao;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/t/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    .line 135
    :goto_1
    if-nez v0, :cond_0

    .line 136
    sget v0, Lcom/google/android/apps/gmm/map/o/an;->a:I

    .line 138
    :cond_0
    :goto_2
    return v0

    .line 124
    :pswitch_0
    const/4 v0, -0x1

    goto :goto_2

    .line 126
    :pswitch_1
    const v0, -0x3f3f40

    goto :goto_2

    .line 129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 130
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/map/o/an;->a:I

    goto :goto_1

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
