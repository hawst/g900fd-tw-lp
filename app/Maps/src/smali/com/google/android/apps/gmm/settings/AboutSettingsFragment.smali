.class public Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;
.super Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->k:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 41
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 43
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 44
    sget v3, Lcom/google/android/apps/gmm/l;->aY:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 45
    sget v3, Lcom/google/android/apps/gmm/l;->cc:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 46
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 48
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 49
    sget v3, Lcom/google/android/apps/gmm/l;->ou:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 50
    sget v3, Lcom/google/android/apps/gmm/l;->l:I

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "{0}"

    sget-object v5, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "{1}"

    sget-object v5, Lcom/google/android/apps/gmm/d/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 53
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 54
    sget v3, Lcom/google/android/apps/gmm/l;->nt:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/settings/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/settings/a;-><init>(Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;)V

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 67
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 70
    invoke-static {v1}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->a(Landroid/preference/PreferenceScreen;)V

    .line 134
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 154
    const-string v2, "DEBUG_COOKIE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {p2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 156
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 143
    sget v0, Lcom/google/android/apps/gmm/l;->L:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    invoke-super {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onResume()V

    .line 145
    return-void
.end method
