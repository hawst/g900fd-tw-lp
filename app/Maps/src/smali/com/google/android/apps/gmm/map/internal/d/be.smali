.class public Lcom/google/android/apps/gmm/map/internal/d/be;
.super Lcom/google/android/apps/gmm/map/internal/d/a;
.source "PG"


# static fields
.field private static final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-array v0, v3, [Ljava/lang/Integer;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/be;->q:Ljava/util/List;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0xe

    .line 47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/be;->r:Ljava/util/List;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;IFLjava/util/Locale;ZLjava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V
    .locals 17

    .prologue
    .line 70
    .line 71
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/be;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/ac;)Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x100

    .line 75
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v2

    .line 76
    iget-boolean v2, v2, Lcom/google/android/apps/gmm/shared/net/a/h;->q:Z

    if-nez v2, :cond_0

    .line 77
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->aa:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    sget-object v7, Lcom/google/android/apps/gmm/map/internal/d/be;->q:Ljava/util/List;

    :goto_0
    const/4 v8, 0x1

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move/from16 v9, p3

    move/from16 v10, p4

    move-object/from16 v12, p5

    move/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    move-object/from16 v16, p9

    .line 70
    invoke-direct/range {v2 .. v16}, Lcom/google/android/apps/gmm/map/internal/d/a;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/ai;ILjava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)V

    .line 88
    return-void

    .line 77
    :cond_1
    sget-object v7, Lcom/google/android/apps/gmm/map/internal/d/be;->r:Ljava/util/List;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/d/ac;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->A:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->o()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "vts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v2, "_"

    const/16 v3, 0x3a

    const/16 v4, 0x5f

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected b(Z)Lcom/google/android/apps/gmm/map/internal/d/i;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/bf;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/bf;-><init>(Lcom/google/android/apps/gmm/map/internal/d/be;Z)V

    return-object v0
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 263
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/be;->q:Ljava/util/List;

    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->b:I

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/a;->a(Ljava/util/Collection;I)I

    move-result v2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/be;->r:Ljava/util/List;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/a;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/google/android/apps/gmm/map/util/h;->a()V

    .line 120
    :try_start_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/internal/d/a;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    invoke-static {}, Lcom/google/android/apps/gmm/map/util/h;->b()V

    .line 123
    return-void

    .line 122
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/gmm/map/util/h;->b()V

    throw v0
.end method
