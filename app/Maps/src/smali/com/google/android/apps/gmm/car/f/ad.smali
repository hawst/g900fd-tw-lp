.class public Lcom/google/android/apps/gmm/car/f/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/f/ac;


# instance fields
.field a:Lcom/google/android/apps/gmm/car/bm;

.field private final b:Lcom/google/android/libraries/curvular/bd;

.field private final c:Lcom/google/android/apps/gmm/car/f/ae;

.field private final d:Lcom/google/android/apps/gmm/q/a;

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Lcom/google/android/apps/gmm/car/f/p;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/q/a;Lcom/google/android/apps/gmm/car/f/ae;ZZZLcom/google/android/apps/gmm/car/f/p;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/bm;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    .line 47
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/libraries/curvular/bd;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/f/ad;->b:Lcom/google/android/libraries/curvular/bd;

    .line 48
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/f/ad;->d:Lcom/google/android/apps/gmm/q/a;

    .line 49
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/car/f/ae;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/f/ad;->c:Lcom/google/android/apps/gmm/car/f/ae;

    .line 50
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/car/f/ad;->e:Z

    .line 51
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/car/f/ad;->f:Z

    .line 52
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/car/f/ad;->g:Z

    .line 53
    if-nez p8, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p8, Lcom/google/android/apps/gmm/car/f/p;

    iput-object p8, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    .line 54
    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->c:Lcom/google/android/apps/gmm/car/f/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/f/ae;->d()V

    .line 253
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->b:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->hM:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 78
    if-nez v1, :cond_0

    .line 79
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    if-eq v2, v3, :cond_1

    .line 82
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 81
    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 88
    if-nez v4, :cond_0

    .line 89
    const-string v0, ""

    .line 98
    :goto_0
    return-object v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->d:Lcom/google/android/apps/gmm/q/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->b:Lcom/google/android/libraries/curvular/bd;

    .line 92
    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/d;->n:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 96
    :goto_1
    new-instance v5, Lcom/google/android/apps/gmm/car/f/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/f/ad;->b:Lcom/google/android/libraries/curvular/bd;

    .line 97
    iget-object v6, v6, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/gmm/car/f/a;-><init>(Landroid/content/Context;I)V

    .line 99
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->B()Lcom/google/android/apps/gmm/y/f;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v4

    .line 98
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz v0, :cond_3

    iget-object v0, v5, Lcom/google/android/apps/gmm/car/f/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->ku:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v5, Lcom/google/android/apps/gmm/car/f/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    :goto_2
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->b:Lcom/google/android/libraries/curvular/bd;

    .line 94
    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/d;->o:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_1

    .line 98
    :cond_3
    if-eqz v4, :cond_4

    iget-object v0, v5, Lcom/google/android/apps/gmm/car/f/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->kw:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v5, Lcom/google/android/apps/gmm/car/f/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    :cond_4
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/y/f;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v5, Lcom/google/android/apps/gmm/car/f/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->jG:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_5
    iget-object v0, v6, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v0, :cond_9

    iget-object v0, v6, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    iget-object v0, v6, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_6
    :goto_4
    if-eqz v0, :cond_1

    sget-object v4, Lcom/google/android/apps/gmm/car/f/b;->a:[I

    iget-object v0, v6, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v0, :cond_b

    iget-object v0, v6, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_a

    move v0, v2

    :goto_5
    if-eqz v0, :cond_b

    iget-object v0, v6, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_7
    :goto_6
    invoke-virtual {v0}, Lcom/google/maps/g/jq;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    :pswitch_0
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_2

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_4

    :cond_a
    move v0, v3

    goto :goto_5

    :cond_b
    move-object v0, v1

    goto :goto_6

    :pswitch_1
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/y/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_2

    :pswitch_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/car/f/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->km:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v5, Lcom/google/android/apps/gmm/car/f/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    :pswitch_3
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/y/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, v5, Lcom/google/android/apps/gmm/car/f/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->kl:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, v5, Lcom/google/android/apps/gmm/car/f/a;->b:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v0, v3, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/ad;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/ad;->g()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/Float;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 114
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-object v0

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v1

    .line 118
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/ad;->g()Ljava/lang/Float;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    const-string v0, "  \u2022  "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    .line 141
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 140
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 141
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v2, :cond_0

    .line 147
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 154
    :goto_0
    return-object v0

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/ad;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 154
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_4

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->j()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->k()Lcom/google/android/libraries/curvular/aq;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    .line 170
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 170
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 171
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/ad;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->f()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->a()Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->b()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->b()I

    move-result v0

    .line 197
    if-nez v0, :cond_0

    .line 200
    sget v0, Lcom/google/android/apps/gmm/l;->bm:I

    .line 202
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final w()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->c:Lcom/google/android/apps/gmm/car/f/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/f/ae;->a()V

    .line 228
    const/4 v0, 0x0

    return-object v0
.end method

.method public final x()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->h:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/f/y;->e()Lcom/google/android/apps/gmm/car/f/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/f/p;->a(Lcom/google/android/apps/gmm/car/f/y;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->c:Lcom/google/android/apps/gmm/car/f/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/f/ae;->e()V

    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public final y()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->c:Lcom/google/android/apps/gmm/car/f/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/f/ae;->b()V

    .line 241
    const/4 v0, 0x0

    return-object v0
.end method

.method public final z()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/ad;->c:Lcom/google/android/apps/gmm/car/f/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/f/ae;->c()V

    .line 247
    const/4 v0, 0x0

    return-object v0
.end method
