.class public final enum Lcom/google/android/apps/gmm/car/f/o;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/car/f/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/car/f/o;

.field public static final enum b:Lcom/google/android/apps/gmm/car/f/o;

.field public static final enum c:Lcom/google/android/apps/gmm/car/f/o;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/car/f/o;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/car/f/o;

    const-string v1, "NOTHING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/car/f/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/f/o;->a:Lcom/google/android/apps/gmm/car/f/o;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/car/f/o;

    const-string v1, "SHOW_ROUTES"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/car/f/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/f/o;->b:Lcom/google/android/apps/gmm/car/f/o;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/car/f/o;

    const-string v1, "NAVIGATE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/car/f/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/f/o;->c:Lcom/google/android/apps/gmm/car/f/o;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/car/f/o;

    sget-object v1, Lcom/google/android/apps/gmm/car/f/o;->a:Lcom/google/android/apps/gmm/car/f/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/car/f/o;->b:Lcom/google/android/apps/gmm/car/f/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/car/f/o;->c:Lcom/google/android/apps/gmm/car/f/o;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/car/f/o;->d:[Lcom/google/android/apps/gmm/car/f/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/car/f/o;
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/apps/gmm/car/f/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/f/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/car/f/o;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/car/f/o;->d:[Lcom/google/android/apps/gmm/car/f/o;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/car/f/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/car/f/o;

    return-object v0
.end method
