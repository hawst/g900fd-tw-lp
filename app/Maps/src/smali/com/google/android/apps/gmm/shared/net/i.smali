.class public abstract Lcom/google/android/apps/gmm/shared/net/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static g:Lcom/google/android/apps/gmm/shared/c/f;

.field public static final p:J


# instance fields
.field private a:Lcom/google/android/apps/gmm/shared/net/l;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/m;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private final d:Lcom/google/android/apps/gmm/shared/c/f;

.field private e:I

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final q:Lcom/google/r/b/a/el;

.field r:Landroid/accounts/Account;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field s:Lcom/google/android/apps/gmm/p/d/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 46
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/shared/net/i;->p:J

    .line 336
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/i;->g:Lcom/google/android/apps/gmm/shared/c/f;

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/i;->g:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(ILcom/google/android/apps/gmm/shared/c/f;)V

    .line 319
    return-void
.end method

.method private constructor <init>(ILcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 322
    invoke-static {p1}, Lcom/google/r/b/a/el;->a(I)Lcom/google/r/b/a/el;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 323
    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/el;)V
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/i;->g:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/shared/net/i;-><init>(Lcom/google/r/b/a/el;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 315
    return-void
.end method

.method private constructor <init>(Lcom/google/r/b/a/el;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->c:J

    .line 307
    iput v2, p0, Lcom/google/android/apps/gmm/shared/net/i;->e:I

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->f:Ljava/util/Map;

    .line 326
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/i;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 327
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->b:Ljava/util/List;

    .line 328
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->a:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 329
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/i;->q:Lcom/google/r/b/a/el;

    .line 330
    return-void
.end method

.method private static a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/shared/net/i;",
            ">;)",
            "Lcom/google/android/apps/gmm/shared/c/a/p;"
        }
    .end annotation

    .prologue
    .line 533
    :goto_0
    const-class v0, Lcom/google/android/apps/gmm/shared/net/i;

    if-eq p0, v0, :cond_0

    .line 535
    :try_start_0
    const-string v0, "onComplete"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/a/o;->a(Ljava/lang/reflect/Method;)Lcom/google/android/apps/gmm/shared/c/a/p;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 542
    :goto_1
    return-object v0

    .line 536
    :catch_0
    move-exception v0

    .line 537
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 533
    :catch_1
    move-exception v0

    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_0

    .line 542
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    goto :goto_1
.end method

.method private g()Lcom/google/android/apps/gmm/shared/net/l;
    .locals 2

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/i;->b:Ljava/util/List;

    monitor-enter v1

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->a:Lcom/google/android/apps/gmm/shared/net/l;

    monitor-exit v1

    return-object v0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public M_()J
    .locals 2

    .prologue
    .line 463
    sget-wide v0, Lcom/google/android/apps/gmm/shared/net/i;->p:J

    return-wide v0
.end method

.method public R_()I
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x2

    return v0
.end method

.method public S_()Lcom/google/b/a/ak;
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    .line 587
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Lcom/google/b/a/ak;

    invoke-direct {v6, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "currentState"

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/i;->a:Lcom/google/android/apps/gmm/shared/net/l;

    .line 588
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v6, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v6, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v1, "stateHistory"

    .line 589
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-wide v2, v4

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/m;

    cmp-long v9, v2, v4

    if-nez v9, :cond_1

    iget-wide v2, v0, Lcom/google/android/apps/gmm/shared/net/m;->a:J

    :goto_1
    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/m;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string v0, "]"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v6, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v6, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v6
.end method

.method public abstract a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
.end method

.method final a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 515
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/i;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    .line 516
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-ne v0, v1, :cond_0

    .line 517
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/shared/net/i;->b(Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 526
    :goto_0
    return-void

    .line 519
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/shared/net/j;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/gmm/shared/net/j;-><init>(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/shared/net/k;)V

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method public final varargs a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 134
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/i;->b:Ljava/util/List;

    monitor-enter v1

    .line 136
    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/i;->a:Lcom/google/android/apps/gmm/shared/net/l;

    .line 145
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    if-ne p1, v0, :cond_1

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->M_()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 150
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->c:J

    .line 155
    :cond_1
    :goto_0
    return-void

    .line 145
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->M_()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->c:J

    goto :goto_0
.end method

.method public abstract a(Ljava/io/DataOutput;)V
.end method

.method public a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 480
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/shared/net/k;->p:Z

    if-eqz v1, :cond_1

    .line 486
    :cond_0
    :goto_0
    return v0

    .line 483
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/shared/net/i;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/gmm/shared/net/i;->e:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->R_()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 486
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public ak_()Z
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x1

    return v0
.end method

.method public al_()Z
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    return v0
.end method

.method final declared-synchronized b(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 500
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Dropped canceled request:"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/p;->a([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 512
    :goto_0
    monitor-exit p0

    return-void

    .line 505
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->f:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 506
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/shared/net/i;->onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 507
    if-nez p1, :cond_1

    .line 508
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->g:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 510
    :cond_1
    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->h:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "err="

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/net/k;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized f()V
    .locals 2

    .prologue
    .line 437
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->i:Lcom/google/android/apps/gmm/shared/net/l;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    monitor-exit p0

    return-void

    .line 437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final i()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->a:Lcom/google/android/apps/gmm/shared/net/l;

    .line 205
    iput v2, p0, Lcom/google/android/apps/gmm/shared/net/i;->e:I

    .line 206
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->c:J

    .line 207
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->a:Lcom/google/android/apps/gmm/shared/net/l;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/i;->a(Lcom/google/android/apps/gmm/shared/net/l;[Ljava/lang/Object;)V

    .line 208
    return-void
.end method

.method public j()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->r:Landroid/accounts/Account;

    return-object v0
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method public final declared-synchronized l()Z
    .locals 2

    .prologue
    .line 442
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/i;->g()Lcom/google/android/apps/gmm/shared/net/l;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->i:Lcom/google/android/apps/gmm/shared/net/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method m()Z
    .locals 4

    .prologue
    .line 529
    iget-wide v0, p0, Lcom/google/android/apps/gmm/shared/net/i;->c:J

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/i;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 555
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 556
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 561
    :goto_1
    return-object v0

    :cond_1
    move v2, v0

    .line 555
    goto :goto_0

    .line 558
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    if-nez v0, :cond_5

    .line 559
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 561
    :cond_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 497
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 582
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/i;->S_()Lcom/google/b/a/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
