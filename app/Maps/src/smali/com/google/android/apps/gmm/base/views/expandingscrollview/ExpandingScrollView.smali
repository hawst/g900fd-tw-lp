.class public Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;
.super Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;
.source "PG"


# static fields
.field public static final a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public static final b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public static final c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public static final d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public static final e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public static final f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public static final g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;


# instance fields
.field private A:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field private B:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field private C:Z

.field public h:I

.field public i:I

.field public j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public final l:[F

.field public final m:[I

.field public final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/base/views/expandingscrollview/q;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/view/View;

.field public q:Landroid/graphics/drawable/Drawable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final v:I

.field private w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

.field private x:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field private y:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field private z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/o;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/n;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/n;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/p;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/p;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/f;-><init>()V

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/i;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/g;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/m;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 345
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;-><init>(Landroid/content/Context;)V

    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 287
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->x:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 291
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->y:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 295
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 326
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    .line 329
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    .line 332
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    .line 336
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->o:Ljava/util/Set;

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 358
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Landroid/content/res/Configuration;Z)V

    .line 360
    new-instance v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    new-instance v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/b;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    new-instance v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/c;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/c;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    .line 376
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 377
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->v:I

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d()V

    .line 382
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setClipChildren(Z)V

    .line 346
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 349
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 287
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->x:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 291
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->y:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 295
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 326
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    .line 329
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    .line 332
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    .line 336
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->o:Ljava/util/Set;

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 358
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Landroid/content/res/Configuration;Z)V

    .line 360
    new-instance v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    new-instance v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/b;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    new-instance v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/c;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/c;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    .line 376
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 377
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->v:I

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d()V

    .line 382
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setClipChildren(Z)V

    .line 350
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 353
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 287
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->x:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 291
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->y:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 295
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 326
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    .line 329
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    .line 332
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    .line 336
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->o:Ljava/util/Set;

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 358
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Landroid/content/res/Configuration;Z)V

    .line 360
    new-instance v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    new-instance v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/b;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    new-instance v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/c;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/c;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;-><init>(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/w;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    .line 376
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 377
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->v:I

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d()V

    .line 382
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setClipChildren(Z)V

    .line 354
    return-void
.end method

.method private a(Landroid/content/res/Configuration;Z)V
    .locals 2

    .prologue
    .line 510
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->y:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 515
    return-void

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->x:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_0
.end method

.method private a(Ljava/lang/Iterable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1087
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v4

    .line 1088
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v2

    aget-object v2, v2, v0

    .line 1089
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v5

    array-length v6, v5

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v3, v5, v0

    .line 1090
    iget-object v7, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v8

    aget v7, v7, v8

    if-lt v4, v7, :cond_0

    .line 1089
    add-int/lit8 v0, v0, 0x1

    move-object v2, v3

    goto :goto_0

    .line 1097
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v0, v0, v3

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    if-ne v0, v3, :cond_1

    .line 1098
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 1099
    invoke-interface {v0, p0, v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    goto :goto_1

    .line 1102
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v3, v0, v3

    .line 1103
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v2, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1106
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v0

    aget v0, v5, v0

    .line 1108
    if-eq v0, v3, :cond_2

    .line 1109
    int-to-float v1, v4

    int-to-float v4, v3

    sub-float/2addr v1, v4

    sub-int/2addr v0, v3

    int-to-float v0, v0

    div-float v0, v1, v0

    move v1, v0

    .line 1112
    :cond_2
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 1113
    invoke-interface {v0, p0, v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    goto :goto_3

    .line 1105
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v5, v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    goto :goto_2

    .line 1116
    :cond_4
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 5

    .prologue
    .line 857
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 858
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 859
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e()V

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v2, :cond_1

    .line 864
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->C:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;

    move-object v1, v0

    .line 865
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 866
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {v0, p0, v2, v4, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V

    goto :goto_1

    .line 864
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;

    move-object v1, v0

    goto :goto_0

    .line 869
    :cond_1
    return-void
.end method

.method private c(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 3

    .prologue
    .line 894
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v2

    aget v1, v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 895
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExposurePixelsInternal(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;I)V

    .line 896
    return-void
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 425
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 426
    iget v6, v5, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v7, "percentage may not be negative"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v7

    aput v6, v0, v7

    invoke-direct {p0, v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 425
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 428
    :cond_2
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_0

    .line 560
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v0

    aget v0, v1, v0

    .line 561
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aput v0, v1, v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aput v0, v1, v4

    .line 568
    :goto_0
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a:Lcom/google/b/c/cv;

    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 566
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a:Lcom/google/b/c/cv;

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 567
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v0

    aget v0, v2, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aput v0, v2, v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aput v1, v0, v4

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 680
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/d;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 681
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v1, :cond_0

    .line 682
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 684
    :cond_0
    return-void

    .line 680
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(F)V
    .locals 8

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_0

    .line 734
    :goto_0
    return-void

    .line 710
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->v:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 713
    const v0, 0x3e99999a    # 0.3f

    mul-float/2addr v0, p1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v4, v0

    .line 714
    const/4 v3, 0x0

    const v2, 0x7fffffff

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v6

    aget v1, v1, v6

    sub-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v2, :cond_4

    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_2
    move v2, v0

    move-object v3, v1

    goto :goto_1

    .line 718
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 719
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 720
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    .line 722
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v1, :cond_2

    .line 723
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    .line 724
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v4

    aget v2, v2, v4

    .line 725
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v4

    sub-int/2addr v4, v1

    int-to-float v4, v4

    sub-int v1, v2, v1

    int-to-float v1, v1

    div-float v1, v4, v1

    .line 727
    const v2, 0x3e4ccccd    # 0.2f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    move-object v3, v0

    .line 733
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    goto/16 :goto_0

    .line 720
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 721
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    goto :goto_3

    :cond_4
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V
    .locals 1

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1008
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->B:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v0, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->B:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1012
    :cond_0
    invoke-static {p1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Ljava/lang/Iterable;)V

    .line 1013
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 1

    .prologue
    .line 799
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 800
    return-void
.end method

.method final b()V
    .locals 3

    .prologue
    .line 1043
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b()V

    .line 1044
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->C:Z

    .line 1045
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->B:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1046
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 1047
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {v0, p0, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0

    .line 1049
    :cond_0
    return-void
.end method

.method final c()V
    .locals 3

    .prologue
    .line 1053
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->c()V

    .line 1054
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->B:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1055
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->C:Z

    .line 1056
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 1057
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {v0, p0, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0

    .line 1059
    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 633
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 634
    return-void

    .line 629
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 519
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 525
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Landroid/content/res/Configuration;Z)V

    .line 529
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v1, v0, :cond_1

    .line 530
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->A:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 533
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a:Lcom/google/b/c/cv;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->A:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_2

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 537
    :cond_2
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 538
    iput-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->A:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 778
    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 649
    const/4 v0, 0x1

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 572
    const/4 v0, 0x0

    .line 573
    sub-int v1, p5, p3

    div-int/lit8 v1, v1, 0x2

    .line 574
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    if-eq v2, v1, :cond_9

    .line 575
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    .line 576
    const/4 v0, 0x1

    move v1, v0

    .line 579
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->q:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {v0, p2, v3, p4, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 585
    :cond_0
    sub-int v4, p4, p2

    .line 586
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 587
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 589
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    .line 590
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getPaddingRight()I

    move-result v6

    sub-int v6, v4, v6

    .line 591
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getPaddingLeft()I

    move-result v7

    invoke-virtual {v5, v7, v2, v6, v3}, Landroid/view/View;->layout(IIII)V

    .line 586
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    .line 595
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v2

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/h;

    .line 596
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/h;->ac_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 597
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->i:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    .line 598
    :goto_3
    if-lez v0, :cond_3

    .line 599
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v4

    const/high16 v5, -0x40800000    # -1.0f

    aput v5, v3, v4

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExposurePixelsInternal(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;I)V

    .line 603
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e()V

    .line 605
    if-eqz v1, :cond_8

    .line 607
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 608
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_4

    .line 609
    iget v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v6

    aget v5, v5, v6

    mul-float/2addr v4, v5

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExposurePixelsInternal(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;I)V

    .line 607
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 595
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 597
    :cond_6
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_3

    .line 618
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 619
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    .line 622
    :cond_8
    return-void

    :cond_9
    move v1, v0

    goto/16 :goto_0
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 544
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 546
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    move v1, v0

    .line 547
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 548
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1, v3}, Landroid/view/View;->measure(II)V

    .line 549
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 547
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 554
    :cond_0
    mul-int/lit8 v0, v2, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setMeasuredDimension(II)V

    .line 555
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 757
    check-cast p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;

    .line 758
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 759
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 760
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 761
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->b:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 762
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->c:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 760
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 764
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 751
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 752
    new-instance v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->l:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;-><init>(Landroid/os/Parcelable;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;[F[I)V

    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/high16 v12, -0x40800000    # -1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 654
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget v3, v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    iget-object v5, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getScrollY()I

    move-result v5

    sub-int v5, v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    sget-object v7, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v7, :cond_2

    int-to-float v0, v5

    cmpg-float v0, v6, v0

    if-gez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->g:F

    sub-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a:I

    int-to-float v7, v7

    cmpg-float v0, v0, v7

    if-gez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->h:F

    sub-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a:I

    int-to-float v7, v7

    cmpg-float v0, v0, v7

    if-gez v0, :cond_2

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v7, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0, v7, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/q;->a()V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    iput v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->g:F

    iput v6, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->h:F

    int-to-float v0, v5

    sub-float v0, v6, v0

    iput v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->i:F

    iput v12, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->l:Z

    :cond_3
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getScrollY()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    if-nez v0, :cond_b

    int-to-float v0, v3

    cmpg-float v0, v8, v0

    if-gez v0, :cond_8

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    if-nez v0, :cond_7

    move v0, v2

    :goto_2
    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    :goto_3
    iput-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    if-eq v0, v3, :cond_0

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    if-eqz v0, :cond_10

    move v0, v1

    :goto_4
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    if-nez v3, :cond_11

    move v3, v2

    :goto_5
    if-eqz v3, :cond_18

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a()V

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    sget-object v7, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    if-ne v3, v7, :cond_17

    iget v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    cmpl-float v3, v3, v12

    if-eqz v3, :cond_17

    iget v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    sub-float/2addr v3, v6

    invoke-virtual {p1, v13, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    :goto_6
    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    move v2, v1

    :cond_4
    if-eqz v2, :cond_5

    iput-boolean v1, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->l:Z

    :cond_5
    iget v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    :cond_6
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a(Landroid/view/MotionEvent;)Z

    :goto_7
    iput v6, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->j:F

    move v2, v1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget-object v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getScrollX()I

    move-result v7

    add-int/2addr v0, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v7, v7

    iget-object v9, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getScrollY()I

    move-result v9

    add-int/2addr v7, v9

    iget-object v9, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->n:Landroid/graphics/Rect;

    iget-object v10, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v10

    iget-object v11, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    invoke-virtual {v9, v2, v2, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v9, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->n:Landroid/graphics/Rect;

    iget-object v10, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTranslationX()F

    move-result v10

    float-to-int v10, v10

    iget-object v11, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getTranslationY()F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Rect;->offset(II)V

    iget-object v9, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v10, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    iget-object v11, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->n:Landroid/graphics/Rect;

    invoke-virtual {v9, v10, v11}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v9, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->n:Landroid/graphics/Rect;

    invoke-virtual {v9, v0, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    goto/16 :goto_2

    :cond_8
    int-to-float v0, v3

    cmpl-float v0, v8, v0

    if-gez v0, :cond_9

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v3, :cond_a

    :cond_9
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    goto/16 :goto_3

    :cond_a
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    goto/16 :goto_3

    :cond_b
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    if-ne v0, v3, :cond_f

    iget v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->g:F

    sub-float v0, v7, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->h:F

    sub-float v3, v8, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->b:I

    int-to-float v7, v7

    cmpl-float v0, v0, v7

    if-lez v0, :cond_c

    move v0, v1

    :goto_8
    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a:I

    int-to-float v7, v7

    cmpl-float v3, v3, v7

    if-lez v3, :cond_d

    move v3, v1

    :goto_9
    if-eqz v3, :cond_e

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    goto/16 :goto_3

    :cond_c
    move v0, v2

    goto :goto_8

    :cond_d
    move v3, v2

    goto :goto_9

    :cond_e
    if-eqz v0, :cond_f

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    goto/16 :goto_3

    :cond_f
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    goto/16 :goto_3

    :cond_10
    move v0, v2

    goto/16 :goto_4

    :cond_11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    packed-switch v3, :pswitch_data_0

    :cond_12
    move v3, v0

    goto/16 :goto_5

    :pswitch_0
    move v3, v1

    goto/16 :goto_5

    :pswitch_1
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    sget-object v8, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    if-eq v3, v8, :cond_13

    move v3, v0

    goto/16 :goto_5

    :cond_13
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v3

    iget-object v8, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    sget-object v9, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v8, v8, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v9

    aget v8, v8, v9

    if-ge v3, v8, :cond_14

    move v3, v2

    goto/16 :goto_5

    :cond_14
    iget v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->j:F

    cmpg-float v3, v7, v3

    if-gez v3, :cond_15

    move v3, v1

    goto/16 :goto_5

    :cond_15
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    iget v7, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->g:F

    float-to-int v7, v7

    iget v8, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->i:F

    float-to-int v8, v8

    invoke-static {v3, v7, v8}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->a(Landroid/view/View;II)Z

    move-result v3

    goto/16 :goto_5

    :pswitch_2
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    sget-object v7, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    if-ne v3, v7, :cond_16

    move v3, v1

    goto/16 :goto_5

    :cond_16
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    sget-object v7, Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/u;

    if-ne v3, v7, :cond_12

    iget-boolean v3, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->l:Z

    if-nez v3, :cond_12

    move v3, v2

    goto/16 :goto_5

    :cond_17
    neg-int v3, v5

    int-to-float v3, v3

    invoke-virtual {p1, v13, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto/16 :goto_6

    :cond_18
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a()V

    iput v12, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->k:F

    iput-boolean v2, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->l:Z

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a(Landroid/view/MotionEvent;)Z

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public scrollTo(II)V
    .locals 1

    .prologue
    .line 1030
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->scrollTo(II)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1031
    :goto_0
    return-void

    .line 1030
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Ljava/lang/Iterable;)V

    goto :goto_0
.end method

.method public final setContent(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->removeAllViews()V

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/a;->a:Landroid/view/MotionEvent;

    .line 411
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    .line 412
    if-eqz p1, :cond_1

    .line 413
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->addView(Landroid/view/View;)V

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->w:Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;

    iput-object p2, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/t;->m:Landroid/view/View;

    .line 416
    return-void
.end method

.method public final setExpandingState(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 813
    if-eqz p2, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->t:I

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v2

    aget v2, v3, v2

    if-lez v0, :cond_1

    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(IZI)V

    .line 814
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 813
    goto :goto_0

    :cond_1
    invoke-super {p0, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_1
.end method

.method public final setExpandingStateTransition(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 476
    const/4 v0, 0x1

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->x:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->y:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Landroid/content/res/Configuration;Z)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->z:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->A:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->requestLayout()V

    .line 478
    return-void
.end method

.method public setExposurePixelsInternal(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;I)V
    .locals 4

    .prologue
    .line 915
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    .line 916
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    aget v0, v0, v1

    if-ne v0, p2, :cond_1

    .line 956
    :cond_0
    :goto_0
    return-void

    .line 920
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    aput p2, v0, v1

    .line 924
    add-int/lit8 v0, v1, -0x1

    :goto_1
    if-ltz v0, :cond_3

    .line 925
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    aget v2, v2, v0

    if-le v2, p2, :cond_2

    .line 926
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    aput p2, v2, v0

    .line 924
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 930
    :cond_3
    add-int/lit8 v0, v1, 0x1

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 931
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    aget v1, v1, v0

    if-ge v1, p2, :cond_4

    .line 932
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    aput p2, v1, v0

    .line 930
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 936
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e()V

    .line 937
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->s:Z

    if-eqz v0, :cond_7

    .line 942
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v0

    .line 943
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 944
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v2

    if-eq v1, v2, :cond_6

    .line 945
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_3

    .line 947
    :cond_6
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 948
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 949
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_4

    .line 952
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, p1, :cond_0

    .line 953
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->t:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(IZI)V

    goto/16 :goto_0
.end method
