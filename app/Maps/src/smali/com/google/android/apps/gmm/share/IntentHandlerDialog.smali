.class public Lcom/google/android/apps/gmm/share/IntentHandlerDialog;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/support/v7/internal/widget/k;

.field public c:Landroid/content/Context;

.field public d:Landroid/content/Intent;

.field public e:Ljava/util/HashMap;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field f:I

.field g:[Lcom/google/android/apps/gmm/share/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 74
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 91
    const-string v0, "Intent"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 92
    :cond_0
    const-string v0, "Title"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 93
    :cond_1
    const-string v0, "Callback"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 95
    :cond_2
    const-string v0, "Intent"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->d:Landroid/content/Intent;

    .line 96
    const-string v0, "Title"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->f:I

    .line 101
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v2, "Callback"

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 102
    array-length v2, v0

    const-class v5, [Lcom/google/android/apps/gmm/share/a/a;

    invoke-static {v0, v2, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/share/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->g:[Lcom/google/android/apps/gmm/share/a/a;

    .line 104
    const-string v0, "share_history.xml"

    .line 105
    invoke-static {v3, v0}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->b:Landroid/support/v7/internal/widget/k;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->b:Landroid/support/v7/internal/widget/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->d:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/Intent;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->b:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/k;->a()I

    move-result v2

    .line 108
    if-ltz v2, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->a:Ljava/util/List;

    move v0, v1

    .line 109
    :goto_1
    if-ge v0, v2, :cond_5

    .line 110
    iget-object v5, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->a:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->b:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v6, v0}, Landroid/support/v7/internal/widget/k;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 113
    :cond_5
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v2

    const-string v0, "PackageSpecificIntentsPackageNames"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v0, "PackageSpecificIntents"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v5, :cond_6

    if-eqz v4, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v0, v6, :cond_7

    :cond_6
    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->e:Ljava/util/HashMap;

    .line 115
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->f:I

    .line 116
    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 117
    iput-object v3, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->c:Landroid/content/Context;

    new-instance v1, Landroid/widget/ListView;

    invoke-direct {v1, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/gmm/share/h;

    iget-object v4, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->a:Ljava/util/List;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/share/h;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v1

    .line 113
    :goto_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_8

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    move-object v0, v2

    goto :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->b:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v0, p3}, Landroid/support/v7/internal/widget/k;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 129
    iget-object v2, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->e:Ljava/util/HashMap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 131
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->g:[Lcom/google/android/apps/gmm/share/a/a;

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->b:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v0, p3}, Landroid/support/v7/internal/widget/k;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 137
    iget-object v3, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->g:[Lcom/google/android/apps/gmm/share/a/a;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 138
    iget-object v6, p0, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->c:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v5, v6, v0, v2}, Lcom/google/android/apps/gmm/share/a/a;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Landroid/content/pm/ResolveInfo;)V

    .line 137
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 143
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/share/IntentHandlerDialog;->dismiss()V

    goto :goto_0
.end method
