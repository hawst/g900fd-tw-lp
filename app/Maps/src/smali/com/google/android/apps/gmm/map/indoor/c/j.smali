.class public Lcom/google/android/apps/gmm/map/indoor/c/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/util/a/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/l",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/c;",
            ">;>;"
        }
    .end annotation
.end field

.field final b:Lcom/google/android/apps/gmm/map/util/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/j",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/l;",
            "Lcom/google/android/apps/gmm/map/indoor/d/a;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/util/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/j",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/l;",
            "Lcom/google/android/apps/gmm/map/indoor/d/e;",
            ">;"
        }
    .end annotation
.end field

.field final d:Lcom/google/android/apps/gmm/map/internal/d/bd;

.field final e:Lcom/google/android/apps/gmm/shared/c/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/gmm/map/indoor/c/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 5

    .prologue
    const/16 v4, 0x400

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 64
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->d:Lcom/google/android/apps/gmm/map/internal/d/bd;

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/l;

    const/16 v1, 0x32

    const-string v2, "Indoor Metadata Cache"

    .line 66
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/l;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->a:Lcom/google/android/apps/gmm/map/util/a/l;

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/map/util/a/l;

    const-string v2, "Indoor Building Cache"

    .line 68
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v3

    invoke-direct {v1, v4, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/l;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/j;-><init>(Lcom/google/android/apps/gmm/map/util/a/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->b:Lcom/google/android/apps/gmm/map/util/a/j;

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/map/util/a/l;

    const-string v2, "Indoor Level Cache"

    .line 70
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v3

    invoke-direct {v1, v4, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/l;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/j;-><init>(Lcom/google/android/apps/gmm/map/util/a/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->c:Lcom/google/android/apps/gmm/map/util/a/j;

    .line 71
    return-void
.end method


# virtual methods
.method a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    if-nez p1, :cond_1

    .line 244
    :cond_0
    return-void

    .line 239
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 240
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    .line 241
    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    if-nez v2, :cond_4

    .line 242
    :cond_3
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->c:Lcom/google/android/apps/gmm/map/util/a/j;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/gmm/map/util/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 241
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/j;->b:Lcom/google/android/apps/gmm/map/util/a/j;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/gmm/map/util/a/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
