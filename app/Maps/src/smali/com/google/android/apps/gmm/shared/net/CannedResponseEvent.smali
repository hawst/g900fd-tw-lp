.class public Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "canned-response"
    b = .enum Lcom/google/android/apps/gmm/util/replay/d;->LOW:Lcom/google/android/apps/gmm/util/replay/d;
.end annotation


# instance fields
.field private final requestId:Lcom/google/r/b/a/el;

.field private final responseEncoded:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "request"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "response"
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lcom/google/r/b/a/el;->a(I)Lcom/google/r/b/a/el;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/r/b/a/el;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->requestId:Lcom/google/r/b/a/el;

    .line 47
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->responseEncoded:Ljava/lang/String;

    .line 48
    return-void
.end method

.method constructor <init>(Lcom/google/r/b/a/el;[B)V
    .locals 2
    .param p2    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 34
    iget v1, p1, Lcom/google/r/b/a/el;->cS:I

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;-><init>(ILjava/lang/String;)V

    .line 35
    return-void

    .line 34
    :cond_0
    invoke-static {p2}, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->writeProtoToBase64String([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static writeProtoToBase64String([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getRequestId()I
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "request"
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->requestId:Lcom/google/r/b/a/el;

    iget v0, v0, Lcom/google/r/b/a/el;->cS:I

    return v0
.end method

.method public getResponse()[B
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->responseEncoded:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->responseEncoded:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public getResponseEncoded()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "response"
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->responseEncoded:Ljava/lang/String;

    return-object v0
.end method

.method public hasResponse()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "response"
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/CannedResponseEvent;->responseEncoded:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
