.class Lcom/google/android/apps/gmm/myplaces/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/a/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/a;->a:Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/a;->a:Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    sget v1, Lcom/google/android/apps/gmm/l;->iI:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/a;->a:Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/a;->a:Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 112
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/myplaces/c/e;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/a;->a:Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    iput-object p2, v0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->b:Lcom/google/android/apps/gmm/myplaces/c/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a:Landroid/app/AlertDialog;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 104
    return-void
.end method
