.class public Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;
.super Lcom/google/android/apps/gmm/cardui/CardUiListFragment;
.source "PG"


# instance fields
.field f:Lcom/google/android/apps/gmm/mapsactivity/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/r/b/a/ks;Ljava/lang/String;)Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;-><init>()V

    .line 30
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 31
    const-string v2, "arg_key_maps_activity_request"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 32
    const-string v2, "arg_key_maps_activity_page_title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 34
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;Z)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/apps/gmm/cardui/g/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 131
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/f;

    return-object v0
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 3

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "arg_key_maps_activity_page_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method protected final i()Lcom/google/android/apps/gmm/cardui/b/b;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/cardui/b/b;->a:Lcom/google/android/apps/gmm/cardui/b/b;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    new-instance v1, Lcom/google/android/apps/gmm/mapsactivity/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "arg_key_maps_activity_request"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ks;

    new-instance v3, Lcom/google/android/apps/gmm/mapsactivity/e;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/mapsactivity/e;-><init>(Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/mapsactivity/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/r/b/a/ks;Lcom/google/android/apps/gmm/mapsactivity/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->f:Lcom/google/android/apps/gmm/mapsactivity/c;

    .line 62
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->f:Lcom/google/android/apps/gmm/mapsactivity/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v3, v0, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/android/apps/gmm/cardui/c;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    :cond_0
    iput-object v2, v0, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    .line 71
    if-eqz p3, :cond_1

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->f:Lcom/google/android/apps/gmm/mapsactivity/c;

    iget-object v0, v2, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/cardui/c;->b(Landroid/os/Bundle;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    const-string v0, "arg_key_maps_activity_request"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ks;

    iput-object v0, v2, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->f:Lcom/google/android/apps/gmm/mapsactivity/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mapsactivity/c;->a()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "arg_key_maps_activity_page_title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 80
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 86
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onResume()V

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 89
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 90
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 92
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    .line 94
    iget-object v1, v1, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 93
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 96
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/TodoListFragment;->f:Lcom/google/android/apps/gmm/mapsactivity/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mapsactivity/c;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/cardui/c;->a(Landroid/os/Bundle;)V

    const-string v1, "arg_key_maps_activity_request"

    iget-object v0, v0, Lcom/google/android/apps/gmm/mapsactivity/c;->b:Lcom/google/r/b/a/ks;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 102
    return-void
.end method
