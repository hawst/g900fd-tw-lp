.class public final enum Lcom/google/android/apps/gmm/place/w;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/place/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/place/w;

.field public static final enum b:Lcom/google/android/apps/gmm/place/w;

.field private static final synthetic c:[Lcom/google/android/apps/gmm/place/w;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/place/w;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/place/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/w;->a:Lcom/google/android/apps/gmm/place/w;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/place/w;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/place/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/w;->b:Lcom/google/android/apps/gmm/place/w;

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/place/w;

    sget-object v1, Lcom/google/android/apps/gmm/place/w;->a:Lcom/google/android/apps/gmm/place/w;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/place/w;->b:Lcom/google/android/apps/gmm/place/w;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/place/w;->c:[Lcom/google/android/apps/gmm/place/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/place/w;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/place/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/w;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/place/w;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/apps/gmm/place/w;->c:[Lcom/google/android/apps/gmm/place/w;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/place/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/place/w;

    return-object v0
.end method
