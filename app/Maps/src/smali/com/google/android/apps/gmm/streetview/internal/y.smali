.class Lcom/google/android/apps/gmm/streetview/internal/y;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field final b:Lcom/google/android/apps/gmm/streetview/internal/t;

.field final c:[Lcom/google/android/apps/gmm/streetview/internal/n;

.field final d:[[Lcom/google/android/apps/gmm/v/aa;

.field final e:Lcom/google/android/apps/gmm/v/cj;

.field final f:Lcom/google/android/apps/gmm/v/cj;

.field final g:[F

.field final h:Lcom/google/android/apps/gmm/v/cn;

.field private i:I

.field private final j:Lcom/google/android/apps/gmm/v/m;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v1, v2}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/y;->a:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/t;IILcom/google/android/apps/gmm/v/cn;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->f:Lcom/google/android/apps/gmm/v/cj;

    .line 40
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->g:[F

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    .line 51
    iput p2, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->i:I

    .line 52
    iput-object p4, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/v/m;

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/v/m;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->j:Lcom/google/android/apps/gmm/v/m;

    .line 54
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    iget v1, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->s:F

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF)Lcom/google/android/apps/gmm/v/cj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->e:Lcom/google/android/apps/gmm/v/cj;

    .line 56
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/internal/t;->z:I

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 57
    new-array v1, v0, [Lcom/google/android/apps/gmm/streetview/internal/n;

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    .line 58
    new-array v1, v0, [[Lcom/google/android/apps/gmm/v/aa;

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    .line 59
    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/gmm/streetview/internal/y;->a(II)V

    .line 60
    return-void
.end method

.method private a(II)V
    .locals 13

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->o:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 72
    :cond_1
    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, p2, :cond_4

    .line 73
    add-int/lit8 v0, p1, -0x1

    sub-int v1, v0, v9

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/internal/t;->d:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v3, v3, Lcom/google/android/apps/gmm/streetview/internal/t;->e:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v4, v4, Lcom/google/android/apps/gmm/streetview/internal/t;->f:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v5, v5, Lcom/google/android/apps/gmm/streetview/internal/t;->g:I

    const/high16 v6, 0x3f800000    # 1.0f

    iget-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v7, v7, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/streetview/internal/n;-><init>(IIIIIFLcom/google/android/apps/gmm/streetview/internal/d;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aput-object v0, v1, v9

    .line 82
    iget v1, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    mul-int/2addr v0, v1

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/aa;

    aput-object v0, v1, v9

    .line 84
    const/4 v1, 0x0

    .line 85
    const/4 v0, 0x0

    move v8, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    if-ge v8, v0, :cond_3

    .line 86
    const/4 v0, 0x0

    move v6, v0

    move v7, v1

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    if-ge v6, v0, :cond_2

    .line 87
    new-instance v10, Lcom/google/android/apps/gmm/v/aa;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/v/aa;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v9

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->f:[Lcom/google/android/apps/gmm/v/av;

    array-length v1, v1

    invoke-static {v8, v1}, Lcom/google/b/a/aq;->a(II)I

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->f:[Lcom/google/android/apps/gmm/v/av;

    aget-object v0, v0, v8

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->i:I

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/co;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->j:Lcom/google/android/apps/gmm/v/m;

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->i:I

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    int-to-float v1, v6

    mul-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    new-instance v11, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->e:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v2, 0x0

    iget-object v3, v11, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->h:Lcom/google/android/apps/gmm/v/cn;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, v11, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    invoke-static {v4, v5, v1, v2, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/y;->a:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->b:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    neg-float v1, v1

    sub-float v2, v1, v0

    iget-object v0, v11, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v12, 0x1

    aget v4, v4, v12

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v12, 0x2

    aget v5, v5, v12

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    invoke-virtual {v10, v11}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v0, v0, v9

    aput-object v10, v0, v7

    .line 86
    add-int/lit8 v0, v6, 0x1

    add-int/lit8 v1, v7, 0x1

    move v6, v0

    move v7, v1

    goto/16 :goto_3

    .line 85
    :cond_2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v1, v7

    goto/16 :goto_2

    .line 72
    :cond_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_1

    .line 92
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(III)Lcom/google/android/apps/gmm/v/aa;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->d:[[Lcom/google/android/apps/gmm/v/aa;

    aget-object v0, v0, p3

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/y;->c:[Lcom/google/android/apps/gmm/streetview/internal/n;

    aget-object v1, v1, p3

    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    mul-int/2addr v1, p2

    add-int/2addr v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method
