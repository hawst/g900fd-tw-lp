.class public abstract Lcom/google/android/apps/gmm/mylocation/a/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/a/i;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/r/b/a;

.field public final b:Lcom/google/android/apps/gmm/mylocation/g/a;

.field private final c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

.field private d:F

.field private e:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/internal/a/a;

    const v2, 0x3f7eb852    # 0.995f

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/a/a;-><init>(F)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/c;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/internal/a/c;->setDuration(J)V

    .line 60
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)I
    .locals 3

    .prologue
    .line 69
    monitor-enter p0

    const/4 v1, 0x0

    .line 70
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/mylocation/a/j;->b(J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 74
    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->d:F

    .line 147
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->e:F

    .line 148
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)Z
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 81
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/mylocation/a/j;->b(Lcom/google/android/apps/gmm/map/r/b/a;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/g/a;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v6, 0x41400000    # 12.0f

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 113
    iget v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 114
    iget v4, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 112
    if-nez v2, :cond_2

    move-object v0, v1

    :goto_0
    iput-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iput v4, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 116
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->d:F

    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->e:F

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4, v6, v7, v0}, Lcom/google/android/apps/gmm/shared/c/s;->a(FFFFF)F

    move-result v3

    const v4, 0x3ecccccd    # 0.4f

    const v5, 0x3f266666    # 0.65f

    invoke-static {v4, v5, v6, v7, v0}, Lcom/google/android/apps/gmm/shared/c/s;->a(FFFFF)F

    move-result v0

    const/4 v4, 0x0

    const/high16 v5, 0x425c0000    # 55.0f

    invoke-static {v0, v3, v4, v5, v2}, Lcom/google/android/apps/gmm/shared/c/s;->a(FFFFF)F

    move-result v0

    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/e;->d:Ljava/util/List;

    :cond_0
    iput-object v1, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->c:Ljava/util/List;

    .line 121
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 112
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_0
.end method

.method protected abstract b(J)I
.end method

.method public final b()Lcom/google/android/apps/gmm/mylocation/b/g;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/g;->a:Lcom/google/android/apps/gmm/mylocation/b/g;

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/mylocation/g/a;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 152
    iget-object v2, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 160
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v0

    .line 152
    goto :goto_0

    .line 155
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/legacy/internal/a/c;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 156
    iget-object v2, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/legacy/internal/a/b;->b:Ljava/lang/Object;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    if-nez v0, :cond_0

    .line 157
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    iget-object v1, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/a/c;->a(Ljava/lang/Object;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/j;->c:Lcom/google/android/apps/gmm/map/legacy/internal/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/a/c;->start()V

    goto :goto_1
.end method

.method public abstract b(Lcom/google/android/apps/gmm/map/r/b/a;)Z
.end method
