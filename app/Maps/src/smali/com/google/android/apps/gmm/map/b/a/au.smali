.class public Lcom/google/android/apps/gmm/map/b/a/au;
.super Lcom/google/android/apps/gmm/map/b/a/ai;
.source "PG"


# instance fields
.field private final I:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/av;)V
    .locals 1

    .prologue
    .line 582
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/ai;-><init>(Lcom/google/android/apps/gmm/map/b/a/al;)V

    .line 583
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/b/a/av;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/au;->I:Z

    .line 584
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/shared/net/a/b;)J
    .locals 4

    .prologue
    .line 576
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/net/a/b;->d()Lcom/google/android/apps/gmm/shared/net/a/t;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/t;->a:Lcom/google/r/b/a/aqg;

    iget v0, v0, Lcom/google/r/b/a/aqg;->e:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 567
    .line 568
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/b/a/au;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v2

    .line 569
    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 571
    :goto_0
    return-wide v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 562
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/au;->I:Z

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 553
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ai;

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    sub-int/2addr v0, v1

    return v0
.end method
