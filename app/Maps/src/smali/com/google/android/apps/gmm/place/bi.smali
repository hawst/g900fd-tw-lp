.class public Lcom/google/android/apps/gmm/place/bi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/h;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lcom/google/android/apps/gmm/y/d;

.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Landroid/support/v4/f/a;

.field private final c:Lcom/google/android/apps/gmm/map/h/d;

.field private d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/place/an;

.field private f:Z

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private l:Lcom/google/android/libraries/curvular/aq;

.field private m:Lcom/google/android/libraries/curvular/aw;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Lcom/google/android/apps/gmm/z/b/l;

.field private s:Lcom/google/android/apps/gmm/z/b/l;

.field private t:Lcom/google/android/apps/gmm/z/b/l;

.field private u:Lcom/google/android/apps/gmm/z/b/l;

.field private v:Lcom/google/android/apps/gmm/z/b/l;

.field private w:Lcom/google/android/apps/gmm/z/b/l;

.field private x:Lcom/google/android/apps/gmm/base/l/a/y;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/support/v4/f/a;Lcom/google/android/apps/gmm/map/h/d;)V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->h:Ljava/util/List;

    .line 87
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 88
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/bi;->b:Landroid/support/v4/f/a;

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/bi;->c:Lcom/google/android/apps/gmm/map/h/d;

    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/y/d;

    .line 92
    sget v1, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v1

    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/y/d;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->C:Lcom/google/android/apps/gmm/y/d;

    .line 94
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 453
    new-instance v0, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    .line 454
    invoke-static {v3, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    .line 455
    invoke-static {v3, p2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/al;->b:Lcom/google/android/libraries/curvular/ah;

    .line 456
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/l/al;->a(Lcom/google/android/libraries/curvular/ce;)Lcom/google/android/apps/gmm/base/l/al;

    move-result-object v0

    .line 457
    if-eqz p3, :cond_0

    .line 458
    sget v1, Lcom/google/android/apps/gmm/f;->cL:I

    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/al;->g:Lcom/google/android/libraries/curvular/ah;

    .line 460
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->am:I

    new-array v2, v2, [Ljava/lang/Object;

    .line 277
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 276
    goto :goto_0

    .line 277
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final B()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 284
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->F:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v1, :cond_1

    :cond_0
    sget-object v1, Lcom/google/b/f/t;->dw:Lcom/google/b/f/t;

    .line 287
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v2

    sget-object v3, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 289
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v2

    .line 290
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/place/bi;->a(Lcom/google/maps/g/hg;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    .line 289
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 291
    const/4 v0, 0x0

    return-object v0

    .line 284
    :cond_1
    sget-object v1, Lcom/google/b/f/t;->dF:Lcom/google/b/f/t;

    goto :goto_0
.end method

.method public final C()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->B:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final D()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/an;->e(Lcom/google/android/apps/gmm/x/o;)V

    .line 306
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final E()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->A:Z

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ku:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 470
    :goto_0
    return-object v0

    .line 467
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->z:Z

    if-eqz v0, :cond_1

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->kw:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 470
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final F()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->z:Z

    if-eqz v0, :cond_0

    .line 477
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 478
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->E()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->F()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 477
    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/place/an;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;)V

    .line 480
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final G()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 485
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->z:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final H()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->A:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final I()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->E()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final J()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->r:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final K()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->s:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final L()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->t:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final M()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->u:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final N()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->v:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final O()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->w:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method a(Lcom/google/maps/g/hg;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 444
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 446
    invoke-virtual {p1}, Lcom/google/maps/g/hg;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    aput-object p2, v0, v2

    .line 447
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 448
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 445
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 312
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 313
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->B()Lcom/google/android/apps/gmm/y/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->B()Lcom/google/android/apps/gmm/y/f;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/y/g;->values()[Lcom/google/android/apps/gmm/y/g;

    move-result-object v0

    array-length v2, v0

    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {}, Lcom/google/android/apps/gmm/y/g;->values()[Lcom/google/android/apps/gmm/y/g;

    move-result-object v2

    array-length v4, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_4

    aget-object v5, v2, v0

    iget-object v6, v1, Lcom/google/android/apps/gmm/y/f;->a:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    move v2, v0

    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/y/c;->b:Lcom/google/android/apps/gmm/y/g;

    iget v0, v0, Lcom/google/android/apps/gmm/y/g;->h:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/c;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v6, Ljava/lang/String;

    const-string v1, "\n"

    new-instance v7, Lcom/google/b/a/ab;

    invoke-direct {v7, v1}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/y/c;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/y/c;->a()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->h:Ljava/util/List;

    if-nez v2, :cond_5

    const/4 v0, 0x1

    :goto_4
    invoke-direct {p0, v4, v6, v0}, Lcom/google/android/apps/gmm/place/bi;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    invoke-static {v4}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v0, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v5, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v7, 0x11

    invoke-virtual {v1, v0, v5, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-static {v6}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v0, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v5, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x11

    invoke-virtual {v4, v0, v5, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/bi;->h:Ljava/util/List;

    if-nez v2, :cond_7

    const/4 v0, 0x1

    :goto_6
    invoke-direct {p0, v1, v4, v0}, Lcom/google/android/apps/gmm/place/bi;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    .line 316
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->m:Lcom/google/maps/g/hg;

    if-nez v1, :cond_9

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    iget-object v1, v1, Lcom/google/r/b/a/acq;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_11

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    const/4 v2, 0x0

    iget-object v1, v1, Lcom/google/r/b/a/acq;->l:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hg;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->m:Lcom/google/maps/g/hg;

    :cond_9
    :goto_7
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->m:Lcom/google/maps/g/hg;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_12

    :cond_a
    const/4 v0, 0x1

    :goto_8
    if-nez v0, :cond_14

    new-instance v0, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->ky:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/l/al;->a(Lcom/google/android/libraries/curvular/ce;)Lcom/google/android/apps/gmm/base/l/al;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/place/bj;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/place/bj;-><init>(Lcom/google/android/apps/gmm/place/bi;Lcom/google/maps/g/hg;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/l/al;->a(Ljava/lang/Runnable;)Lcom/google/android/apps/gmm/base/l/al;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_13

    :cond_b
    const/4 v0, 0x1

    :goto_9
    if-nez v0, :cond_c

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/l/al;->b:Lcom/google/android/libraries/curvular/ah;

    :cond_c
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->x:Lcom/google/android/apps/gmm/base/l/a/y;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->y:Z

    .line 318
    :goto_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->K()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 320
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->i:Ljava/lang/CharSequence;

    .line 325
    :goto_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->C:Lcom/google/android/apps/gmm/y/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 327
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->B()Lcom/google/android/apps/gmm/y/f;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->A:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/bi;->z:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 326
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz v0, :cond_17

    iget-object v0, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->ku:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v2, v2, Lcom/google/android/apps/gmm/y/d;->b:I

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v1

    :goto_c
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v1

    .line 333
    if-eqz v1, :cond_22

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    :goto_d
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->l:Lcom/google/android/libraries/curvular/aq;

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->m:Lcom/google/android/libraries/curvular/aw;

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    .line 337
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 338
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->F:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v0, :cond_e

    .line 339
    :cond_d
    const-string v0, "g.co/santatracker"

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->kA:I

    .line 340
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 341
    :goto_e
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    .line 342
    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->l:Lcom/google/android/libraries/curvular/aq;

    .line 343
    sget v0, Lcom/google/android/apps/gmm/f;->bJ:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->m:Lcom/google/android/libraries/curvular/aw;

    .line 346
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->H()Lcom/google/maps/g/hg;

    move-result-object v1

    .line 347
    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    :goto_f
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->o:Ljava/lang/String;

    .line 348
    if-eqz v1, :cond_25

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    :goto_10
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->p:Ljava/lang/String;

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->o()Ljava/lang/String;

    move-result-object v0

    .line 350
    if-nez v0, :cond_26

    const-string v0, ""

    :goto_11
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->n:Ljava/lang/String;

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->z:Z

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->A:Z

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->f:Z

    if-nez v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 356
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->b:Lcom/google/android/apps/gmm/base/g/f;

    if-ne v0, v1, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 357
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->w:Z

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    :goto_12
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->B:Z

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_28

    const/4 v0, 0x1

    move v1, v0

    .line 360
    :goto_13
    if-eqz v1, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    if-nez v0, :cond_29

    :cond_f
    const/4 v0, 0x0

    .line 361
    :goto_14
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->q:Ljava/lang/String;

    .line 362
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->q:Ljava/lang/String;

    .line 363
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->dk:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 364
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 365
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->r:Lcom/google/android/apps/gmm/z/b/l;

    .line 366
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->q:Ljava/lang/String;

    .line 367
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->ds:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 368
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 369
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->s:Lcom/google/android/apps/gmm/z/b/l;

    .line 370
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->dp:Lcom/google/b/f/t;

    aput-object v4, v0, v3

    .line 371
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    if-eqz v1, :cond_2b

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 372
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v3, :cond_2a

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_15
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->q:Ljava/lang/String;

    .line 373
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 374
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->u:Lcom/google/android/apps/gmm/z/b/l;

    .line 376
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v0, 0x1

    new-array v3, v0, [Lcom/google/b/f/cq;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 379
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->F:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v0, :cond_2c

    :cond_10
    sget-object v0, Lcom/google/b/f/t;->dw:Lcom/google/b/f/t;

    :goto_16
    aput-object v0, v3, v4

    .line 377
    iput-object v3, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 382
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v3, :cond_2d

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_17
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->q:Ljava/lang/String;

    .line 383
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    if-eqz v1, :cond_2e

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 384
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 385
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->x()Lcom/google/maps/g/hg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->d()Ljava/lang/String;

    move-result-object v0

    .line 384
    :goto_18
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 387
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->v:Lcom/google/android/apps/gmm/z/b/l;

    .line 388
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->dl:Lcom/google/b/f/t;

    aput-object v4, v0, v3

    .line 389
    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    if-eqz v1, :cond_30

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 390
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_2f

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_19
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->t:Lcom/google/android/apps/gmm/z/b/l;

    .line 392
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->q:Ljava/lang/String;

    .line 393
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dt:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 394
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 395
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->w:Lcom/google/android/apps/gmm/z/b/l;

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->K()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->f:Z

    .line 399
    return-void

    .line 316
    :cond_11
    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->m:Lcom/google/maps/g/hg;

    goto/16 :goto_7

    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_8

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_9

    :cond_14
    new-instance v0, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->x:Lcom/google/android/apps/gmm/base/l/a/y;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->y:Z

    goto/16 :goto_a

    .line 322
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    if-nez v1, :cond_16

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    invoke-virtual {v1}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    :cond_16
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->i:Ljava/lang/CharSequence;

    goto/16 :goto_b

    .line 326
    :cond_17
    if-eqz v4, :cond_18

    iget-object v0, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->kw:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v2, v2, Lcom/google/android/apps/gmm/y/d;->b:I

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v1

    goto/16 :goto_c

    :cond_18
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/y/f;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->jG:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v0, v1

    goto/16 :goto_c

    :cond_19
    iget-object v0, v3, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v0, :cond_1e

    iget-object v0, v3, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_1d

    const/4 v0, 0x1

    :goto_1a
    if-eqz v0, :cond_1e

    iget-object v0, v3, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_1a

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_1a
    :goto_1b
    if-eqz v0, :cond_1c

    sget-object v4, Lcom/google/android/apps/gmm/y/e;->a:[I

    iget-object v0, v3, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    if-eqz v0, :cond_20

    iget-object v0, v3, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v5, 0x4

    if-ne v0, v5, :cond_1f

    const/4 v0, 0x1

    :goto_1c
    if-eqz v0, :cond_20

    iget-object v0, v3, Lcom/google/android/apps/gmm/y/f;->b:Lcom/google/maps/g/jl;

    iget v0, v0, Lcom/google/maps/g/jl;->d:I

    invoke-static {v0}, Lcom/google/maps/g/jq;->a(I)Lcom/google/maps/g/jq;

    move-result-object v0

    if-nez v0, :cond_1b

    sget-object v0, Lcom/google/maps/g/jq;->a:Lcom/google/maps/g/jq;

    :cond_1b
    :goto_1d
    invoke-virtual {v0}, Lcom/google/maps/g/jq;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :cond_1c
    :goto_1e
    move-object v0, v1

    goto/16 :goto_c

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1a

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1b

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1c

    :cond_20
    const/4 v0, 0x0

    goto :goto_1d

    :pswitch_0
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/y/d;->a(Lcom/google/android/apps/gmm/y/f;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_c

    :pswitch_1
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/y/f;->c()Z

    move-result v0

    if-eqz v0, :cond_21

    const-string v0, " "

    new-instance v4, Lcom/google/b/a/ab;

    invoke-direct {v4, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/y/f;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->kn:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v4

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget v2, v2, Lcom/google/android/apps/gmm/y/d;->c:I

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v4, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v1

    goto/16 :goto_c

    :cond_21
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/y/d;->a(Lcom/google/android/apps/gmm/y/f;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_c

    :pswitch_2
    iget-object v0, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->km:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v0, " \u00b7 "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v3, v2, Lcom/google/android/apps/gmm/y/d;->b:I

    invoke-direct {v0, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v3, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->hd:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget v2, v2, Lcom/google/android/apps/gmm/y/d;->c:I

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v0, v1

    goto/16 :goto_c

    :pswitch_3
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/y/d;->a(Lcom/google/android/apps/gmm/y/f;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_c

    :pswitch_4
    iget-object v0, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->kl:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v0, " \u00b7 "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v3, v2, Lcom/google/android/apps/gmm/y/d;->b:I

    invoke-direct {v0, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v3, v2, Lcom/google/android/apps/gmm/y/d;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->hd:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget v2, v2, Lcom/google/android/apps/gmm/y/d;->c:I

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1e

    .line 333
    :cond_22
    const/4 v0, 0x0

    goto/16 :goto_d

    .line 341
    :cond_23
    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_e

    .line 347
    :cond_24
    const/4 v0, 0x0

    goto/16 :goto_f

    .line 348
    :cond_25
    const/4 v0, 0x0

    goto/16 :goto_10

    .line 350
    :cond_26
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->b:Landroid/support/v4/f/a;

    invoke-virtual {v1, v0}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_11

    .line 357
    :cond_27
    const/4 v0, 0x0

    goto/16 :goto_12

    .line 359
    :cond_28
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_13

    .line 360
    :cond_29
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 361
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    goto/16 :goto_14

    .line 372
    :cond_2a
    const/4 v0, 0x0

    goto/16 :goto_15

    :cond_2b
    const/4 v0, 0x0

    goto/16 :goto_15

    .line 379
    :cond_2c
    sget-object v0, Lcom/google/b/f/t;->dF:Lcom/google/b/f/t;

    goto/16 :goto_16

    .line 382
    :cond_2d
    const/4 v0, 0x0

    goto/16 :goto_17

    .line 385
    :cond_2e
    const/4 v0, 0x0

    goto/16 :goto_18

    .line 390
    :cond_2f
    const/4 v0, 0x0

    goto/16 :goto_19

    :cond_30
    const/4 v0, 0x0

    goto/16 :goto_19

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ag:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/bi;->i:Ljava/lang/CharSequence;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/an;->g(Lcom/google/android/apps/gmm/x/o;)V

    .line 111
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 117
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v2, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v3, Lcom/google/b/f/t;->gm:Lcom/google/b/f/t;

    .line 116
    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 120
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->H()Lcom/google/maps/g/hg;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->gm:Lcom/google/b/f/t;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/place/bi;->a(Lcom/google/maps/g/hg;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    .line 119
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->o:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->x:Lcom/google/android/apps/gmm/base/l/a/y;

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->y:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    .line 148
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v2, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v3, Lcom/google/b/f/t;->dl:Lcom/google/b/f/t;

    .line 147
    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->f:Z

    .line 151
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 163
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 162
    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->aj:I

    new-array v2, v2, [Ljava/lang/Object;

    .line 163
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->j:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->g:Z

    .line 175
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 176
    const/4 v0, 0x0

    return-object v0

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->h:Ljava/util/List;

    return-object v0
.end method

.method public final p()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->n:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->c:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bi;->q()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ak:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 239
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/bi;->n:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->e:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bi;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/an;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method public final v()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final w()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->l:Lcom/google/android/libraries/curvular/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->l:Lcom/google/android/libraries/curvular/aq;

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final x()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->m:Lcom/google/android/libraries/curvular/aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bi;->m:Lcom/google/android/libraries/curvular/aw;

    .line 261
    :goto_0
    return-object v0

    .line 260
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/f;->fz:I

    .line 261
    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    goto :goto_0
.end method

.method public final y()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 266
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bi;->k:Ljava/lang/CharSequence;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final z()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bi;->y()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bi;->q()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bi;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
