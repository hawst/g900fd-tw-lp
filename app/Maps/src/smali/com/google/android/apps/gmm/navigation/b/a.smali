.class public Lcom/google/android/apps/gmm/navigation/b/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Lcom/google/maps/g/a/hm;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/b/b;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/b/b;->a:Lcom/google/maps/g/a/hm;

    const-string v1, "travelMode"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/b/a;->a:Lcom/google/maps/g/a/hm;

    .line 39
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/b/b;->b:Ljava/util/List;

    const-string v1, "destinations"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Ljava/util/Collection;

    .line 38
    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/b/a;->b:Ljava/util/List;

    .line 40
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/b/b;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/b/a;->c:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/google/maps/g/a/hm;)V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/navigation/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/b/b;-><init>()V

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/b/b;->a:Lcom/google/maps/g/a/hm;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/b/a;-><init>(Lcom/google/android/apps/gmm/navigation/b/b;)V

    .line 49
    return-void
.end method
