.class public final enum Lcom/google/android/apps/gmm/l/y;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/l/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/l/y;

.field public static final enum b:Lcom/google/android/apps/gmm/l/y;

.field public static final enum c:Lcom/google/android/apps/gmm/l/y;

.field public static final enum d:Lcom/google/android/apps/gmm/l/y;

.field public static final enum e:Lcom/google/android/apps/gmm/l/y;

.field public static final enum f:Lcom/google/android/apps/gmm/l/y;

.field public static final enum g:Lcom/google/android/apps/gmm/l/y;

.field public static final enum h:Lcom/google/android/apps/gmm/l/y;

.field public static final enum i:Lcom/google/android/apps/gmm/l/y;

.field public static final enum j:Lcom/google/android/apps/gmm/l/y;

.field public static final enum k:Lcom/google/android/apps/gmm/l/y;

.field public static final enum l:Lcom/google/android/apps/gmm/l/y;

.field private static final synthetic m:[Lcom/google/android/apps/gmm/l/y;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "DIRECTIONS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->a:Lcom/google/android/apps/gmm/l/y;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "FNAV"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->b:Lcom/google/android/apps/gmm/l/y;

    .line 113
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->c:Lcom/google/android/apps/gmm/l/y;

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "SEARCH_LIST"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->d:Lcom/google/android/apps/gmm/l/y;

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->e:Lcom/google/android/apps/gmm/l/y;

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "MAP_ONLY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->f:Lcom/google/android/apps/gmm/l/y;

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "STREET_VIEW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->g:Lcom/google/android/apps/gmm/l/y;

    .line 118
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "STREET_VIEW_STEREO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->h:Lcom/google/android/apps/gmm/l/y;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "INVALID"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->i:Lcom/google/android/apps/gmm/l/y;

    .line 120
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "FRAGMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->j:Lcom/google/android/apps/gmm/l/y;

    .line 123
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "PLACE_AND_FRAGMENT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->k:Lcom/google/android/apps/gmm/l/y;

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/l/y;

    const-string v1, "VOICE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->l:Lcom/google/android/apps/gmm/l/y;

    .line 109
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/apps/gmm/l/y;

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->a:Lcom/google/android/apps/gmm/l/y;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->b:Lcom/google/android/apps/gmm/l/y;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->c:Lcom/google/android/apps/gmm/l/y;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->d:Lcom/google/android/apps/gmm/l/y;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->e:Lcom/google/android/apps/gmm/l/y;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->f:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->g:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->h:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->i:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->j:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->k:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->l:Lcom/google/android/apps/gmm/l/y;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/l/y;->m:[Lcom/google/android/apps/gmm/l/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/y;
    .locals 1

    .prologue
    .line 109
    const-class v0, Lcom/google/android/apps/gmm/l/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/y;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/l/y;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/android/apps/gmm/l/y;->m:[Lcom/google/android/apps/gmm/l/y;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/l/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/l/y;

    return-object v0
.end method
