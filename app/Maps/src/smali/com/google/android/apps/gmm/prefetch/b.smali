.class public abstract Lcom/google/android/apps/gmm/prefetch/b;
.super Landroid/app/Service;
.source "PG"


# static fields
.field static final a:J

.field private static j:I

.field private static final k:I


# instance fields
.field b:Lcom/google/android/apps/gmm/prefetch/k;

.field c:Lcom/google/android/apps/gmm/prefetch/j;

.field d:Lcom/google/android/apps/gmm/map/h/d;

.field public e:Lcom/google/android/apps/gmm/prefetch/s;

.field f:Landroid/net/wifi/WifiManager$WifiLock;

.field public volatile g:Z

.field final h:Ljava/util/concurrent/Semaphore;

.field volatile i:Z

.field private final l:Landroid/os/IBinder;

.field private m:J

.field private n:J

.field private o:Landroid/os/Looper;

.field private p:Landroid/os/PowerManager$WakeLock;

.field private final q:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 73
    const/16 v0, 0x20

    sput v0, Lcom/google/android/apps/gmm/prefetch/b;->j:I

    .line 87
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/prefetch/b;->a:J

    .line 112
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    sget v2, Lcom/google/android/apps/gmm/prefetch/a/a;->b:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/prefetch/b;->k:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 129
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/k;->c:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/prefetch/e;-><init>(Lcom/google/android/apps/gmm/prefetch/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->l:Landroid/os/IBinder;

    .line 155
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->g:Z

    .line 161
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->h:Ljava/util/concurrent/Semaphore;

    .line 166
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->i:Z

    .line 169
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/prefetch/c;-><init>(Lcom/google/android/apps/gmm/prefetch/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->q:Ljava/lang/Object;

    .line 912
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/prefetch/b;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/prefetch/j;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/prefetch/j;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->c()Lcom/google/android/apps/gmm/shared/net/a/r;

    move-result-object v1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/r;->a:Lcom/google/r/b/a/ti;

    iget v1, v1, Lcom/google/r/b/a/ti;->d:I

    int-to-long v6, v1

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/gmm/prefetch/b;->m:J

    sub-long/2addr v2, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->p:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendEmptyMessage(I)Z

    :goto_2
    return-void

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/h/a;->a(Landroid/content/Context;)Z

    move-result v0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/prefetch/h;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v1, p1, Lcom/google/android/apps/gmm/prefetch/h;->a:Lcom/google/android/apps/gmm/prefetch/q;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->f()Lcom/google/android/apps/gmm/map/internal/d/be;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/m;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/internal/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    const-wide/16 v4, 0x190

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/be;->a(J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/m;->c()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/k;->c:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    iget-object v0, p1, Lcom/google/android/apps/gmm/prefetch/h;->e:Lcom/google/android/apps/gmm/prefetch/a/c;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->i:Lcom/google/android/apps/gmm/prefetch/a/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/prefetch/m;->e:Z

    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->a:Lcom/google/android/apps/gmm/prefetch/a/e;

    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/prefetch/h;->e:Lcom/google/android/apps/gmm/prefetch/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    :cond_3
    :goto_2
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->d:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v1, :cond_5

    :goto_3
    if-nez v2, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->h:Lcom/google/android/apps/gmm/prefetch/a/e;

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    goto :goto_3

    :cond_6
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget v0, p1, Lcom/google/android/apps/gmm/prefetch/h;->b:I

    if-lez v0, :cond_a

    :cond_7
    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    sget v5, Lcom/google/android/apps/gmm/prefetch/b;->j:I

    if-ge v0, v5, :cond_a

    invoke-interface {v1}, Lcom/google/android/apps/gmm/prefetch/q;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v5

    if-eqz v5, :cond_a

    iget-object v0, p1, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/m;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    if-eqz v0, :cond_9

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/internal/d/d;->h:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->c:Lcom/google/android/apps/gmm/map/internal/d/b/s;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z

    move-result v0

    :goto_5
    if-nez v0, :cond_7

    :cond_8
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_5

    :cond_a
    new-instance v5, Lcom/google/android/apps/gmm/prefetch/i;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, p0, v0, p1}, Lcom/google/android/apps/gmm/prefetch/i;-><init>(Lcom/google/android/apps/gmm/prefetch/b;ILcom/google/android/apps/gmm/prefetch/h;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move v1, v2

    :goto_6
    if-ge v1, v6, :cond_b

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v7, p1, Lcom/google/android/apps/gmm/prefetch/h;->d:Lcom/google/android/apps/gmm/prefetch/m;

    iget-object v7, v7, Lcom/google/android/apps/gmm/prefetch/m;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {v3, v0, v5, v7, v2}, Lcom/google/android/apps/gmm/map/internal/d/be;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Z)Lcom/google/android/apps/gmm/map/internal/d/ak;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_b
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/prefetch/k;->c:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->m:J

    new-instance v0, Lcom/google/android/apps/gmm/m/g;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    const-string v1, "LAST_PREFECHED_FINISHED"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/prefetch/b;->m:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/m/d;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/prefetch/q;Lcom/google/android/apps/gmm/prefetch/m;Lcom/google/android/apps/gmm/prefetch/a/c;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/prefetch/b;->a(Lcom/google/android/apps/gmm/prefetch/q;Lcom/google/android/apps/gmm/prefetch/m;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/prefetch/q;Lcom/google/android/apps/gmm/prefetch/m;Lcom/google/android/apps/gmm/prefetch/a/c;)V
    .locals 8
    .param p3    # Lcom/google/android/apps/gmm/prefetch/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 590
    if-eqz p3, :cond_0

    .line 591
    invoke-interface {p3, p2}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/b;)V

    .line 594
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/prefetch/m;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 595
    if-eqz p3, :cond_2

    .line 596
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->i:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 598
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->d:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 599
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/e;->h:Lcom/google/android/apps/gmm/prefetch/a/e;

    .line 601
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    invoke-interface {p3, v1, v0}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 603
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    .line 647
    :goto_1
    return-void

    .line 598
    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    goto :goto_0

    .line 607
    :cond_4
    iget-object v0, p2, Lcom/google/android/apps/gmm/prefetch/m;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 612
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->i:Z

    if-nez v0, :cond_5

    .line 615
    if-eqz v2, :cond_6

    const-wide/16 v0, 0xa

    .line 619
    :goto_2
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/b;->h:Ljava/util/concurrent/Semaphore;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1, v4}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 624
    if-eqz v0, :cond_7

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->h:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 635
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->e:Lcom/google/android/apps/gmm/prefetch/s;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetch/s;->a()V

    .line 639
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/k;->a:Lcom/google/android/apps/gmm/prefetch/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    .line 641
    if-eqz v2, :cond_8

    sget v2, Lcom/google/android/apps/gmm/prefetch/b;->k:I

    .line 645
    :goto_3
    const/4 v7, 0x3

    new-instance v0, Lcom/google/android/apps/gmm/prefetch/h;

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/prefetch/h;-><init>(Lcom/google/android/apps/gmm/prefetch/q;ILcom/google/android/apps/gmm/prefetch/m;JLcom/google/android/apps/gmm/prefetch/a/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v1, v7, v0}, Lcom/google/android/apps/gmm/prefetch/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/prefetch/j;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 615
    :cond_6
    const-wide/16 v0, 0x3c

    goto :goto_2

    .line 620
    :catch_0
    move-exception v0

    .line 621
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 628
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/a/d;->a:Lcom/google/android/apps/gmm/prefetch/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/a/e;->i:Lcom/google/android/apps/gmm/prefetch/a/e;

    invoke-interface {p3, v0, v1}, Lcom/google/android/apps/gmm/prefetch/a/c;->a(Lcom/google/android/apps/gmm/prefetch/a/d;Lcom/google/android/apps/gmm/prefetch/a/e;)V

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    goto :goto_1

    .line 643
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->c()Lcom/google/android/apps/gmm/shared/net/a/r;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/r;->a:Lcom/google/r/b/a/ti;

    iget v2, v0, Lcom/google/r/b/a/ti;->c:I

    goto :goto_3
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/prefetch/b;)V
    .locals 6

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->d:Lcom/google/android/apps/gmm/map/h/d;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->f()Lcom/google/android/apps/gmm/map/internal/d/be;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/b;->f:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/c;->e:Lcom/google/android/apps/gmm/map/internal/d/c;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/prefetch/a;-><init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/b;->e:Lcom/google/android/apps/gmm/prefetch/s;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/prefetch/s;->b()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->d()V

    :goto_0
    return-void

    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/prefetch/w;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/prefetch/w;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/c/a;)V

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/gmm/prefetch/b;->a(Lcom/google/android/apps/gmm/prefetch/q;Lcom/google/android/apps/gmm/prefetch/m;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/prefetch/b;)Lcom/google/android/apps/gmm/shared/net/a/r;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->c()Lcom/google/android/apps/gmm/shared/net/a/r;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract b()V
.end method

.method protected final c()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->d:Lcom/google/android/apps/gmm/map/h/d;

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->d:Lcom/google/android/apps/gmm/map/h/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/h/d;->a()V

    .line 249
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/u;->a:Lcom/google/android/apps/gmm/prefetch/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->e:Lcom/google/android/apps/gmm/prefetch/s;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->e:Lcom/google/android/apps/gmm/prefetch/s;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/prefetch/s;->a(Lcom/google/android/apps/gmm/map/c/a;)V

    .line 253
    new-instance v0, Lcom/google/android/apps/gmm/m/g;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    const-string v1, "LAST_PREFECHED_FINISHED"

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    :goto_1
    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->m:J

    .line 254
    iget-wide v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->m:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Last prefetch finished: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 257
    new-instance v0, Lcom/google/android/apps/gmm/m/g;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    const-string v1, "LAST_PREFETCH_NOT_START_CONDITION_LOG_TIME"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_2
    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->n:J

    .line 258
    iget-wide v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->n:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x42

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Last prefetch not started condition log time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 262
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/prefetch/b;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    const-string v1, "PrefetcherService"

    invoke-virtual {v0, v7, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->f:Landroid/net/wifi/WifiManager$WifiLock;

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v6}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 266
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/prefetch/b;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "PrefetcherService"

    invoke-virtual {v0, v7, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->p:Landroid/os/PowerManager$WakeLock;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->p:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->a()V

    .line 270
    return-void

    :cond_0
    move-wide v0, v2

    .line 253
    goto/16 :goto_0

    :cond_1
    move-wide v0, v2

    goto/16 :goto_1

    :cond_2
    move-wide v0, v2

    .line 257
    goto :goto_2
.end method

.method d()V
    .locals 2

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->f:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 797
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->p:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 798
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->p:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/k;->a:Lcom/google/android/apps/gmm/prefetch/k;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/k;->b:Lcom/google/android/apps/gmm/prefetch/k;

    if-ne v0, v1, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    .line 806
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendEmptyMessage(I)Z

    .line 808
    :cond_3
    return-void

    .line 805
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->n:J

    .line 851
    new-instance v0, Lcom/google/android/apps/gmm/m/g;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    const-string v1, "LAST_PREFETCH_NOT_START_CONDITION_LOG_TIME"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/prefetch/b;->n:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;J)V

    .line 854
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/m/d;->a()V

    .line 855
    return-void
.end method

.method protected final f()Lcom/google/android/apps/gmm/map/internal/d/be;
    .locals 3

    .prologue
    .line 862
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/be;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->l:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/a;

    const-string v1, "PrefetcherService"

    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/a;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 228
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->start()V

    .line 229
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->o:Landroid/os/Looper;

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->o:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 233
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->o:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/prefetch/j;-><init>(Lcom/google/android/apps/gmm/prefetch/b;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->q:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendEmptyMessage(I)Z

    .line 237
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/b;->q:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->o:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 377
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/prefetch/d;-><init>(Lcom/google/android/apps/gmm/prefetch/b;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 378
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendEmptyMessage(I)Z

    .line 351
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendEmptyMessage(I)Z

    .line 358
    return v1
.end method
