.class public Lcom/google/android/apps/gmm/car/ao;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/gms/car/support/CarAppLayout;

.field public final b:Lcom/google/android/gms/car/a/c;

.field public final c:Ljava/lang/String;

.field final d:Lcom/google/android/apps/gmm/map/util/b/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public e:Z

.field f:Z

.field public g:Z

.field public h:Z

.field public i:Lcom/google/android/apps/gmm/car/aq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final l:Ljava/lang/Object;

.field private final m:Lcom/google/android/gms/car/support/p;

.field private final n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/view/View$OnClickListener;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private p:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/support/CarAppLayout;Lcom/google/android/gms/car/a/c;Lcom/google/android/gms/car/support/p;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/map/util/b/a/a;)V
    .locals 1
    .param p6    # Lcom/google/android/apps/gmm/map/util/b/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    new-instance v0, Lcom/google/android/apps/gmm/car/ap;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/ap;-><init>(Lcom/google/android/apps/gmm/car/ao;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->l:Ljava/lang/Object;

    .line 83
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/car/support/CarAppLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    .line 84
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/gms/car/a/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/ao;->b:Lcom/google/android/gms/car/a/c;

    .line 85
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/gms/car/support/p;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/ao;->m:Lcom/google/android/gms/car/support/p;

    .line 86
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move-object v0, p4

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->c:Ljava/lang/String;

    .line 87
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p5, Landroid/graphics/drawable/Drawable;

    iput-object p5, p0, Lcom/google/android/apps/gmm/car/ao;->n:Landroid/graphics/drawable/Drawable;

    .line 88
    iput-object p6, p0, Lcom/google/android/apps/gmm/car/ao;->d:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 90
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/ao;->j:Ljava/lang/String;

    .line 91
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/ao;->k:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a()V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 97
    if-eqz p6, :cond_5

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->l:Ljava/lang/Object;

    invoke-interface {p6, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 101
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->c()V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setMenuClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setTitleClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->p:Z

    .line 207
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/ao;->o:Landroid/view/View$OnClickListener;

    .line 183
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->p:Z

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setMenuClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setTitleClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->h:Z

    if-eq v0, p1, :cond_0

    .line 259
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/car/ao;->h:Z

    .line 260
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->h:Z

    if-nez v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->a()V

    .line 267
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 270
    :cond_0
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->b()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->k:Ljava/lang/String;

    .line 274
    :goto_0
    if-nez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->e()V

    .line 280
    :goto_1
    return-void

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->j:Ljava/lang/String;

    goto :goto_0

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->setTitle(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->d()V

    goto :goto_1
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setMenuDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->setMenuClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->setTitleClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->p:Z

    .line 197
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->c:Lcom/google/android/apps/gmm/car/aq;

    if-ne v0, v1, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->e:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->f:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->g:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ao;->h:Z

    if-nez v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->b:Lcom/google/android/apps/gmm/car/aq;

    if-eq v0, v1, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ao;->m:Lcom/google/android/gms/car/support/p;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setSearchBoxModeSmall(Lcom/google/android/gms/car/support/p;)V

    .line 289
    sget-object v0, Lcom/google/android/apps/gmm/car/aq;->b:Lcom/google/android/apps/gmm/car/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    goto :goto_0

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->a:Lcom/google/android/apps/gmm/car/aq;

    if-eq v0, v1, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->setSearchBoxModeNone()V

    .line 294
    sget-object v0, Lcom/google/android/apps/gmm/car/aq;->a:Lcom/google/android/apps/gmm/car/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    goto :goto_0
.end method
