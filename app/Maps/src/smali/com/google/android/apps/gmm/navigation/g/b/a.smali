.class public Lcom/google/android/apps/gmm/navigation/g/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/g/b/k;)V
    .locals 6

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 43
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 51
    :cond_2
    iget v0, p2, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->a:I

    .line 52
    iget-object v0, p3, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    .line 53
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v4

    mul-double/2addr v2, v4

    .line 52
    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->b:I

    .line 54
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->c:I

    .line 60
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->b:I

    if-ltz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->c:I

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/a;->d:Z

    .line 62
    return-void

    .line 60
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
