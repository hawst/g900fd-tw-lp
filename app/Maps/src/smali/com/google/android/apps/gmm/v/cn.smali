.class public Lcom/google/android/apps/gmm/v/cn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    .line 23
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    .line 29
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/cn;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    .line 36
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/cn;)F
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v0, v0, v2

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v2

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v3

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v4

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v4

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final a()Lcom/google/android/apps/gmm/v/cn;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v0, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v3

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v4

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v4

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v5

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v5

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 62
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v3

    div-float/2addr v2, v0

    aput v2, v1, v3

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v4

    div-float/2addr v2, v0

    aput v2, v1, v4

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v2, v2, v5

    div-float v0, v2, v0

    aput v0, v1, v5

    .line 66
    return-object p0
.end method

.method public final a(FFF)Lcom/google/android/apps/gmm/v/cn;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v2

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v2

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v3

    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v3

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v4

    invoke-static {v1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v4

    .line 152
    return-object p0
.end method

.method public final b(FFF)Lcom/google/android/apps/gmm/v/cn;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v2

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v2

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v3

    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v3

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v1, v1, v4

    invoke-static {v1, p3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v4

    .line 166
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 188
    if-ne p0, p1, :cond_0

    .line 189
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    .line 191
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/v/cn;

    if-eqz v0, :cond_1

    .line 192
    check-cast p1, Lcom/google/android/apps/gmm/v/cn;

    .line 193
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    goto :goto_0

    .line 195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x33

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
