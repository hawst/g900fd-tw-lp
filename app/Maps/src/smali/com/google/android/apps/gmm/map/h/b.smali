.class public Lcom/google/android/apps/gmm/map/h/b;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/h/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/h/c;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/h/b;->a:Lcom/google/android/apps/gmm/map/h/c;

    .line 21
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 31
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 32
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/h/b;->a:Lcom/google/android/apps/gmm/map/h/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/h/c;->d()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/h/d;->a()V

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/h/b;->a:Lcom/google/android/apps/gmm/map/h/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/h/c;->a()V

    .line 41
    :goto_0
    return-void

    .line 38
    :cond_0
    const-string v0, "ConnectivityChangeReceiver"

    const-string v1, "ConnectivityChangeReceiver should be only registered to CONNECTIVITY_ACTION!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
