.class public Lcom/google/android/apps/gmm/navigation/i/d;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/i/u;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 60
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p0, v9}, Lcom/google/android/apps/gmm/navigation/i/d;->a(Lcom/google/android/apps/gmm/navigation/i/u;Ljava/util/List;)V

    invoke-virtual {v9}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v3, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v2, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    if-ne v0, v2, :cond_0

    invoke-virtual {v9}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_0
    invoke-virtual {v9}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v3, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-ne v2, v3, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v2, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    if-ne v0, v2, :cond_1

    invoke-virtual {v9}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v3, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    if-ne v2, v3, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v3, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    if-ne v2, v3, :cond_1

    const/16 v2, 0x3c

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->g:I

    iput v1, v0, Lcom/google/android/apps/gmm/navigation/i/s;->f:I

    :cond_1
    invoke-static {v9}, Lcom/google/android/apps/gmm/navigation/i/d;->a(Ljava/util/List;)V

    invoke-static {v9}, Lcom/google/android/apps/gmm/navigation/i/d;->b(Ljava/util/List;)V

    invoke-static {v9}, Lcom/google/android/apps/gmm/navigation/i/d;->c(Ljava/util/List;)V

    invoke-static {v9}, Lcom/google/android/apps/gmm/navigation/i/d;->d(Ljava/util/List;)V

    .line 61
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/apps/gmm/navigation/i/s;

    .line 62
    iget-object v0, v8, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v1, v8, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v11, v0, v1

    iget-object v0, v11, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v4

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v1, v8, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    iget v2, v8, Lcom/google/android/apps/gmm/navigation/i/s;->e:I

    int-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iget v3, v8, Lcom/google/android/apps/gmm/navigation/i/s;->f:I

    iget v6, v8, Lcom/google/android/apps/gmm/navigation/i/s;->g:I

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget-boolean v5, v8, Lcom/google/android/apps/gmm/navigation/i/s;->d:Z

    iget-object v6, v8, Lcom/google/android/apps/gmm/navigation/i/s;->h:Ljava/lang/String;

    const/4 v7, -0x1

    iget v8, v8, Lcom/google/android/apps/gmm/navigation/i/s;->i:I

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/a/am;-><init>(Lcom/google/maps/g/a/bx;IIIZLjava/lang/String;II)V

    iget-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v11, v0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_0

    .line 64
    :cond_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    .line 66
    :goto_1
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/google/android/apps/gmm/navigation/i/u;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/navigation/i/u;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v9, 0x64

    const/4 v4, 0x0

    .line 116
    move v3, v4

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    if-ge v3, v0, :cond_0

    .line 117
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/s;

    sget-object v1, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    const/16 v5, 0x1e

    const/16 v6, 0xc8

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/i/s;-><init>(Lcom/google/maps/g/a/bx;Lcom/google/android/apps/gmm/navigation/i/u;IIII)V

    .line 124
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/s;

    sget-object v1, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    const/16 v5, 0xa

    move-object v2, p0

    move v6, v9

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/i/s;-><init>(Lcom/google/maps/g/a/bx;Lcom/google/android/apps/gmm/navigation/i/u;IIII)V

    .line 133
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/s;

    sget-object v6, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    const/16 v11, 0x63

    move-object v7, p0

    move v8, v3

    move v10, v4

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/gmm/navigation/i/s;-><init>(Lcom/google/maps/g/a/bx;Lcom/google/android/apps/gmm/navigation/i/u;IIII)V

    .line 142
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 201
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v7

    .line 202
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    move-object v1, v0

    .line 203
    :goto_0
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    :goto_1
    move-object v3, v1

    move-object v1, v0

    .line 204
    :goto_2
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 205
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    .line 206
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v8, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v4, v8, :cond_c

    .line 207
    iget-object v4, v1, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v8, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-ne v4, v8, :cond_c

    .line 208
    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v8, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    if-ne v4, v8, :cond_c

    .line 209
    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v8, v3, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v4, v4, v8

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    iget v4, v3, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v9, v3, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget-object v9, v9, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    if-ne v4, v9, :cond_4

    move-object v4, v2

    :goto_3
    if-nez v4, :cond_5

    move v4, v5

    :goto_4
    if-eqz v4, :cond_c

    .line 212
    const-string v4, ""

    iput-object v4, v1, Lcom/google/android/apps/gmm/navigation/i/s;->h:Ljava/lang/String;

    .line 213
    invoke-interface {v7}, Ljava/util/ListIterator;->remove()V

    .line 218
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v8, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v4, v4, v8

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v8, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    if-eq v4, v8, :cond_0

    .line 219
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v4, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v4, Lcom/google/maps/g/a/ez;->l:Lcom/google/maps/g/a/ez;

    if-eq v0, v4, :cond_0

    .line 220
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v4, v1, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v4, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    if-ne v0, v4, :cond_1

    .line 221
    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v4, v1, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v4, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    if-eq v0, v4, :cond_1

    .line 222
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v4, v1, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v4, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    if-ne v0, v4, :cond_b

    .line 223
    :cond_1
    iput-boolean v6, v3, Lcom/google/android/apps/gmm/navigation/i/s;->d:Z

    goto/16 :goto_2

    :cond_2
    move-object v1, v2

    .line 202
    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    .line 203
    goto/16 :goto_1

    .line 209
    :cond_4
    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v9, v3, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    add-int/lit8 v9, v9, 0x1

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v4, v4, v9

    goto :goto_3

    :cond_5
    sget-object v9, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    if-ne v8, v9, :cond_8

    iget v8, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    const/16 v9, 0xa

    if-le v8, v9, :cond_6

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    const/16 v8, 0x64

    if-gt v4, v8, :cond_7

    :cond_6
    move v4, v6

    goto :goto_4

    :cond_7
    move v4, v5

    goto :goto_4

    :cond_8
    iget v8, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    const/16 v9, 0x1e

    if-le v8, v9, :cond_9

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    const/16 v8, 0xc8

    if-gt v4, v8, :cond_a

    :cond_9
    move v4, v6

    goto/16 :goto_4

    :cond_a
    move v4, v5

    goto/16 :goto_4

    :cond_b
    move-object v0, v1

    move-object v1, v3

    :cond_c
    move-object v3, v1

    move-object v1, v0

    .line 229
    goto/16 :goto_2

    .line 230
    :cond_d
    return-void
.end method

.method private static b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 243
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    :goto_0
    move-object v1, v0

    .line 244
    :goto_1
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    .line 246
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v4, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v3, v4, :cond_0

    .line 247
    iget-object v3, v1, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v4, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-ne v3, v4, :cond_0

    .line 248
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v4, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v3, v3, v4

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    const/16 v4, 0x320

    if-ge v3, v4, :cond_0

    .line 249
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v4, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v3, v3, v4

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->g:I

    .line 250
    const-string v3, ""

    iput-object v3, v1, Lcom/google/android/apps/gmm/navigation/i/s;->h:Ljava/lang/String;

    :cond_0
    move-object v1, v0

    .line 253
    goto :goto_1

    .line 243
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 254
    :cond_2
    return-void
.end method

.method private static c(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 267
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/s;

    .line 269
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v3, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    if-ne v2, v3, :cond_0

    .line 270
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->d:Z

    if-nez v2, :cond_0

    .line 271
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v3, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    if-eq v2, v3, :cond_1

    .line 272
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v3, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v3, Lcom/google/maps/g/a/ez;->l:Lcom/google/maps/g/a/ez;

    if-eq v2, v3, :cond_1

    .line 273
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v2, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->d:Lcom/google/maps/g/a/ez;

    if-ne v0, v2, :cond_0

    .line 274
    :cond_1
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 277
    :cond_2
    return-void
.end method

.method private static d(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v7

    .line 287
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/apps/gmm/navigation/i/s;

    .line 292
    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    sget-object v1, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v0, v1, :cond_0

    .line 293
    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v1, v3, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v1, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    if-ne v0, v1, :cond_0

    .line 294
    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    iget v1, v3, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    const/16 v1, 0xa28

    if-le v0, v1, :cond_0

    .line 296
    invoke-interface {v7}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 298
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/s;

    sget-object v1, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    .line 299
    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    .line 300
    iget v3, v3, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    const/16 v4, -0x960

    const/16 v5, 0x1e

    const/16 v6, 0xc8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/i/s;-><init>(Lcom/google/maps/g/a/bx;Lcom/google/android/apps/gmm/navigation/i/u;IIII)V

    .line 298
    invoke-interface {v7, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 304
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 307
    :cond_1
    return-void
.end method
