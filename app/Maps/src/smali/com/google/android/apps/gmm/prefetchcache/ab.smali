.class public Lcom/google/android/apps/gmm/prefetchcache/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetchcache/api/f;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/a;

.field private b:Lcom/google/android/apps/gmm/prefetchcache/r;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->c:Z

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->a:Lcom/google/android/apps/gmm/base/a;

    .line 36
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/accounts/Account;[Landroid/accounts/Account;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    monitor-enter p0

    if-eqz p1, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 103
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    if-eqz p1, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eq p1, v2, :cond_10

    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v2, :cond_4

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->a()V

    :cond_4
    iput-object p1, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    iput-object p2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->c:[Landroid/accounts/Account;

    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v2, :cond_5

    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->h:Lcom/google/android/apps/gmm/map/internal/c/cd;

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/b;

    iget-object v5, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/b;-><init>(Landroid/accounts/Account;)V

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    :cond_5
    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-eqz v2, :cond_6

    :goto_2
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->f()Lcom/google/r/b/a/a/ab;

    move-result-object v0

    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    const/4 v2, 0x0

    iput-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->g()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ab;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    sget-object v5, Lcom/google/android/apps/gmm/prefetchcache/s;->a:[I

    iget v2, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v2

    if-nez v2, :cond_8

    sget-object v2, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_8
    invoke-virtual {v2}, Lcom/google/r/b/a/a/ai;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    iget v0, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_9
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2b

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Area was not loaded due to unknown status: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v0

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    if-eqz v0, :cond_a

    :try_start_5
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    :cond_a
    monitor-enter v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-eqz v0, :cond_12

    iget-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v2, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :pswitch_0
    :try_start_8
    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/t;

    iget-object v5, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v2, v0, v5}, Lcom/google/android/apps/gmm/prefetchcache/t;-><init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V

    if-nez v1, :cond_b

    invoke-interface {v2}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->e()Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_b
    invoke-interface {v2}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->j()Lcom/google/android/apps/gmm/prefetchcache/api/c;

    move-result-object v0

    :goto_4
    iget-object v2, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_1
    iput-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    goto/16 :goto_3

    :cond_c
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v1, :cond_d

    :try_start_9
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    :cond_d
    monitor-enter v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    iget-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    if-eqz v0, :cond_f

    iget-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    if-nez v1, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :catchall_4
    move-exception v0

    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_e
    :try_start_c
    iget v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v1, v1, Lcom/google/r/b/a/a/ai;->f:I

    iput v1, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->e()V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    :cond_f
    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 104
    :cond_10
    monitor-exit p0

    return-void

    .line 103
    :cond_11
    :try_start_d
    iget v4, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/a/ah;->a:I

    iget v2, v2, Lcom/google/r/b/a/a/ai;->f:I

    iput v2, v0, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    iput-object v0, v3, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->e()V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    :cond_12
    monitor-exit v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_13
    move-object v0, v2

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/d/b/a;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/apps/gmm/prefetchcache/r;->g:Landroid/accounts/Account;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/prefetchcache/r;->f()Lcom/google/r/b/a/a/ab;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/prefetchcache/r;->g()Z

    move-result v1

    if-nez v1, :cond_1

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_4
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ab;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    sget-object v4, Lcom/google/android/apps/gmm/prefetchcache/s;->a:[I

    iget v1, v0, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v1}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_2
    invoke-virtual {v1}, Lcom/google/r/b/a/a/ai;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/t;

    iget-object v4, v2, Lcom/google/android/apps/gmm/prefetchcache/r;->e:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/gmm/prefetchcache/t;-><init>(Lcom/google/r/b/a/a/af;Lcom/google/android/apps/gmm/map/c/a;)V

    invoke-interface {v1}, Lcom/google/android/apps/gmm/prefetchcache/api/c;->j()Lcom/google/android/apps/gmm/prefetchcache/api/c;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/prefetchcache/r;->c()V

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/prefetch/a/f;)V
    .locals 1

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->a:Lcom/google/android/apps/gmm/base/a;

    .line 47
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(Lcom/google/android/apps/gmm/prefetch/a/f;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/prefetchcache/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit p0

    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/prefetchcache/api/d;)V
    .locals 4

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    const-string v1, "OfflineDataUpdateListener should not be null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "removeDataChangeListener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/r/b/a/a/af;)V
    .locals 2

    .prologue
    .line 52
    monitor-enter p0

    .line 53
    :try_start_0
    iget v0, p1, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_0
    sget-object v1, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 52
    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 53
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 54
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/r;->b(Lcom/google/r/b/a/a/af;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/r/b/a/a/af;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    .line 96
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 96
    :cond_0
    :try_start_1
    iget v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/r/b/a/a/ah;->a:I

    iput-object p2, v0, Lcom/google/r/b/a/a/ah;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    .line 95
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->a(Lcom/google/r/b/a/a/af;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/api/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->b:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/prefetchcache/api/d;)V
    .locals 4

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    const-string v1, "OfflineDataUpdateListener should not be null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/r;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "addDataChangeListener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/r/b/a/a/af;)V
    .locals 1

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/r;->b(Lcom/google/r/b/a/a/af;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit p0

    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/r/b/a/a/af;)V
    .locals 1

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/prefetchcache/r;->c(Lcom/google/r/b/a/a/af;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    monitor-exit p0

    return-void

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetchcache/r;->d:Lcom/google/r/b/a/a/af;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->c:Z

    .line 120
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return v0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ab;->b:Lcom/google/android/apps/gmm/prefetchcache/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/r;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
