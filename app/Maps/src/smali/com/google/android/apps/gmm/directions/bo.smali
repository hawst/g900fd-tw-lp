.class Lcom/google/android/apps/gmm/directions/bo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bo;->a:Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/directions/b/c;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bo;->a:Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bo;->a:Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->clear()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/directions/bs;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1, v4, v4, v4}, Ljava/util/Calendar;->set(III)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/bs;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/apps/gmm/directions/bs;->b:J

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;->i()V

    .line 138
    :cond_0
    return-void
.end method
