.class public Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final SATELLITE_BUNDLE_STRING:Ljava/lang/String; = "satellites"


# instance fields
.field public final location:Landroid/location/Location;


# direct methods
.method public constructor <init>(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    .line 27
    return-void
.end method

.method public static buildLocation(Ljava/lang/String;DDLjava/lang/Long;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;)Landroid/location/Location;
    .locals 5
    .param p5    # Ljava/lang/Long;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Double;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 169
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    .line 170
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/r/b/c;->g:Ljava/lang/String;

    .line 171
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    .line 172
    if-eqz p5, :cond_5

    .line 173
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->i:J

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    .line 177
    :goto_0
    if-eqz p6, :cond_0

    .line 178
    invoke-virtual {p6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->b:D

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->r:Z

    .line 180
    :cond_0
    if-eqz p7, :cond_1

    .line 181
    invoke-virtual {p7}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    .line 183
    :cond_1
    if-eqz p8, :cond_2

    .line 184
    invoke-virtual {p8}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    .line 186
    :cond_2
    if-eqz p9, :cond_3

    .line 187
    invoke-virtual {p9}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    .line 189
    :cond_3
    if-eqz p10, :cond_4

    .line 190
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 191
    const-string v2, "satellites"

    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 192
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->d:Landroid/os/Bundle;

    .line 194
    :cond_4
    new-instance v1, Landroid/location/Location;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->i:J

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    goto :goto_0

    .line 194
    :cond_6
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 122
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;

    if-nez v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    if-eq v1, v2, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAccuracy()Ljava/lang/Float;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "accuracy"
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAltitude()Ljava/lang/Double;
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "altitude"
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getBearing()Ljava/lang/Float;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "bearing"
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getLatitude()D
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "lat"
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    return-object v0
.end method

.method public getLongitude()D
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "lng"
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getNumSatellites()Ljava/lang/Integer;
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "numSatellites"
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->hasNumSatellites()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 110
    :goto_2
    return-object v0

    .line 107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 108
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "satellites"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "provider"
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpeed()Ljava/lang/Float;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "speed"
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTime()Ljava/lang/Long;
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "time"
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public hasAccuracy()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "accuracy"
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    return v0
.end method

.method public hasAltitude()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "altitude"
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    return v0
.end method

.method public hasBearing()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "bearing"
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    return v0
.end method

.method public hasNumSatellites()Z
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "numSatellites"
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    .line 99
    :goto_1
    return v0

    :cond_0
    move v0, v2

    .line 93
    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "satellites"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 97
    goto :goto_1

    :cond_2
    move v0, v2

    .line 99
    goto :goto_1
.end method

.method public hasSpeed()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "speed"
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 131
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->location:Landroid/location/Location;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 136
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "provider"

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getProvider()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "lat"

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "lng"

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "time"

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getTime()Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 142
    const-string v0, "altitude"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getAltitude()Ljava/lang/Double;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 144
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 145
    const-string v0, "bearing"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getBearing()Ljava/lang/Float;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 147
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 148
    const-string v0, "speed"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getSpeed()Ljava/lang/Float;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 150
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 151
    const-string v0, "accuracy"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getAccuracy()Ljava/lang/Float;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 153
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->hasNumSatellites()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 154
    const-string v0, "numSatellites"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;->getNumSatellites()Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 156
    :cond_d
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
