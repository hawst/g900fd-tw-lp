.class public Lcom/google/android/apps/gmm/base/views/FloorPickerView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/LinearLayout;",
        "Lcom/google/android/libraries/curvular/cl",
        "<",
        "Lcom/google/android/apps/gmm/base/l/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/q;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/base/l/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloorPickerView;->b:Ljava/util/Set;

    .line 33
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 34
    const-class v1, Lcom/google/android/apps/gmm/base/f/ac;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloorPickerView;->a:Lcom/google/android/libraries/curvular/ae;

    .line 35
    return-void
.end method


# virtual methods
.method public final synthetic setViewModel(Lcom/google/android/libraries/curvular/ce;)V
    .locals 2

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/apps/gmm/base/l/q;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/l/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloorPickerView;->c:Lcom/google/android/apps/gmm/base/l/r;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloorPickerView;->c:Lcom/google/android/apps/gmm/base/l/r;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloorPickerView;->b:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/l/r;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloorPickerView;->a:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    return-void
.end method
