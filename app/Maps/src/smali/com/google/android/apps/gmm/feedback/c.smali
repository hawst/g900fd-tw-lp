.class Lcom/google/android/apps/gmm/feedback/c;
.super Landroid/app/DialogFragment;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/c;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 29
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    const-class v2, Lcom/google/android/apps/gmm/feedback/b/a;

    const/4 v3, 0x0

    .line 30
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    .line 31
    iget-object v2, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    new-instance v3, Lcom/google/android/apps/gmm/feedback/d;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/feedback/d;-><init>(Lcom/google/android/apps/gmm/feedback/c;)V

    invoke-interface {v2, v3}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 46
    new-instance v2, Landroid/app/Dialog;

    invoke-direct {v2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 48
    iget-object v0, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 49
    return-object v2
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/c;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 61
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 62
    return-void
.end method
