.class public Lcom/google/android/apps/gmm/place/reservation/ah;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:J


# instance fields
.field public final b:Lcom/google/android/apps/gmm/shared/c/f;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/place/reservation/ag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/place/reservation/ah;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/ah;->b:Lcom/google/android/apps/gmm/shared/c/f;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 72
    if-nez p1, :cond_1

    .line 85
    :cond_0
    return-void

    .line 76
    :cond_1
    const-string v0, "reservation_widget_saved_states"

    .line 77
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/reservation/ag;

    .line 80
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/ag;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ah;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->e:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/google/android/apps/gmm/place/reservation/ah;->a:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/reservation/ag;->a:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 80
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
