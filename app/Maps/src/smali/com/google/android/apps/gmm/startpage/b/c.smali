.class public Lcom/google/android/apps/gmm/startpage/b/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Lcom/google/android/apps/gmm/startpage/d/e;

.field public final e:Lcom/google/o/h/a/dq;

.field public final f:Z

.field public final g:Lcom/google/o/h/a/eq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final h:Z

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Lcom/google/android/apps/gmm/shared/net/k;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;ZLcom/google/o/h/a/eq;ZLcom/google/android/apps/gmm/shared/net/k;Z)V
    .locals 1
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/o/h/a/eq;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Lcom/google/o/h/a/dq;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/o/h/a/eq;",
            "Z",
            "Lcom/google/android/apps/gmm/shared/net/k;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/b/c;->a:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/b/c;->b:Ljava/lang/String;

    .line 59
    iput-wide p3, p0, Lcom/google/android/apps/gmm/startpage/b/c;->c:J

    .line 60
    iput-object p5, p0, Lcom/google/android/apps/gmm/startpage/b/c;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 61
    iput-object p6, p0, Lcom/google/android/apps/gmm/startpage/b/c;->e:Lcom/google/o/h/a/dq;

    .line 62
    iput-object p7, p0, Lcom/google/android/apps/gmm/startpage/b/c;->i:Ljava/lang/String;

    .line 63
    iput-boolean p8, p0, Lcom/google/android/apps/gmm/startpage/b/c;->f:Z

    .line 64
    iput-object p9, p0, Lcom/google/android/apps/gmm/startpage/b/c;->g:Lcom/google/o/h/a/eq;

    .line 65
    iput-boolean p10, p0, Lcom/google/android/apps/gmm/startpage/b/c;->h:Z

    .line 66
    iput-object p11, p0, Lcom/google/android/apps/gmm/startpage/b/c;->k:Lcom/google/android/apps/gmm/shared/net/k;

    .line 67
    iput-boolean p12, p0, Lcom/google/android/apps/gmm/startpage/b/c;->j:Z

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;ZLcom/google/o/h/a/eq;ZZ)V
    .locals 15
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/o/h/a/eq;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Lcom/google/o/h/a/dq;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/o/h/a/eq;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 80
    const/4 v12, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move/from16 v13, p11

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/gmm/startpage/b/c;-><init>(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;ZLcom/google/o/h/a/eq;ZLcom/google/android/apps/gmm/shared/net/k;Z)V

    .line 84
    return-void
.end method
